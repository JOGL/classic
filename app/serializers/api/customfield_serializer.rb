# frozen_string_literal: true

module Api
  class CustomfieldSerializer < ActiveModel::Serializer
    attributes  :id,
                :name,
                :description,
                :field_type
  end
end
