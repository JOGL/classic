# frozen_string_literal: true

module Api
  class SourceSerializer < ActiveModel::Serializer
    attributes :id, :title, :url
  end
end
