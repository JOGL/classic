# frozen_string_literal: true

module Api
  class MembersSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes  :id,
                :first_name,
                :last_name,
                :nickname,
                :status,
                :skills,
                :ressources,
                :interests,
                :can_contact,
                :current_sign_in_at,
                :logo_url,
                :logo_url_sm,
                :short_bio,
                :owner,
                :admin,
                :member,
                :affiliation,
                :reviewer,
                :pending,
                :has_followed,
                :has_clapped,
                :has_saved,
                :geoloc,
                :stats

    def owner
      object.has_role?(:owner, @instance_options[:parent])
    end

    def admin
      object.has_role?(:admin, @instance_options[:parent])
    end

    def member
      return true if object.has_role?(:member, @instance_options[:parent])

      @instance_options[:parent].user_is_member?(object)
    end

    def reviewer
      object.has_role?(:reviewer, @instance_options[:parent])
    end

    def pending
      object.has_role?(:pending, @instance_options[:parent])
    end

    def mutual_count
      if defined?(current_user).nil? || current_user.nil?
        0
      else
        object.follow_mutual_count(current_user)
      end
    end

    def stats
      {
        mutual_count: mutual_count,
        followers_count: object.followers_count,
        projects_count: object.projects_count,
        spaces_count: object.spaces_count
      }
    end
  end
end
