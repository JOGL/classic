# frozen_string_literal: true

module Api
  class ReviewedSerializer < ActiveModel::Serializer
    has_many :projects do
      object.owned_relations.reviewed.of_type('Project').includes(:resource).map(&:resource).compact
    end

    has_many :users do
      object.owned_relations.reviewed.of_type('User').includes(:resource).map(&:resource).compact
    end

    has_many :needs do
      object.owned_relations.reviewed.of_type('Need').includes(:resource).map(&:resource).compact
    end

    has_many :challenges do
      object.owned_relations.reviewed.of_type('Challenge').includes(:resource).map(&:resource).compact
    end

    has_many :workgroups do
      object.owned_relations.reviewed.of_type('Workgroup').includes(:resource).map(&:resource).compact
    end

    has_many :programs do
      object.owned_relations.reviewed.of_type('Program').includes(:resource).map(&:resource).compact
    end
  end
end
