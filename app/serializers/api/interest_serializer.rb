# frozen_string_literal: true

module Api
  class InterestSerializer < ActiveModel::Serializer
    attributes :id
  end
end
