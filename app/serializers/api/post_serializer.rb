# frozen_string_literal: true

module Api
  class PostSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :content,
               :category,
               :media,
               :creator,
               :mentions,
               :from,
               :comments,
               :created_at,
               :claps_count,
               :saves_count,
               :clappers

    attribute :documents, if: :show_objects?
    attribute :has_clapped, unless: :scope?
    attribute :has_saved, unless: :scope?

    has_many :comments
    has_many :mentions

    def comments
      object.comments.collect do |comment|
        Api::CommentSerializer.new(comment).attributes
      end
    end

    def from
      if object.from_object.nil?
        {
          object_type: 'impossible',
          object_id: 0,
          object_name: 'Bollocks'
        }
      else
        {
          object_type: object.from_object.downcase,
          object_id: object.from_id,
          object_name: object.from_name,
          object_image: object.from_image,
          object_need_proj_id: object.from_need_project_id
        }
      end
    end

    def show_objects?
      @instance_options[:show_objects]
    end
  end
end
