# frozen_string_literal: true

module Api
  class FollowingSerializer < ActiveModel::Serializer
    has_many :projects do
      object.following.where(resource_type: 'Project').select { |project| Project.exists? project.resource_id }.map do |project|
        @obj = Project.find(project.resource_id)
      end
    end

    has_many :workgroups do
      object.following.where(resource_type: 'Workgroup').select { |workgroup| Workgroup.exists? workgroup.resource_id }.map do |workgroup|
        @obj = Workgroup.find(workgroup.resource_id)
      end
    end

    has_many :users do
      object.following.where(resource_type: 'User').select { |user| User.exists? user.resource_id }.map do |user|
        @obj = User.find(user.resource_id)
      end
    end

    has_many :challenges do
      object.following.where(resource_type: 'Challenge').select { |challenge| Challenge.exists? challenge.resource_id }.map do |challenge|
        @obj = Challenge.find(challenge.resource_id)
      end
    end

    has_many :programs do
      object.following.where(resource_type: 'Program').select { |program| Program.exists? program.resource_id }.map do |program|
        @obj = Program.find(program.resource_id)
      end
    end

    has_many :needs do
      object.following.where(resource_type: 'Need').select { |need| Need.exists? need.resource_id }.map do |need|
        @obj = Need.find(need.resource_id)
      end
    end

    has_many :spaces do
      object.following.where(resource_type: 'Space').select { |space| Space.exists? space.resource_id }.map do |space|
        @obj = Space.find(space.resource_id)
      end
    end
  end
end
