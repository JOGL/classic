# frozen_string_literal: true

module Api
  class SharedProjectSerializer < Panko::Serializer
    @@helper = Api::SerializerHelper.new

    attributes  :id,
                :title,
                :short_title,
                :banner_url,
                :banner_url_sm,
                :short_description,
                :status,
                :creator,
                :challenges,
                :affiliated_spaces,
                :skills,
                :interests,
                :geoloc,
                :grant_info,
                :is_private,
                :is_owner,
                :is_admin,
                :is_member,
                :is_reviewer,
                :is_reviewed,
                :users_sm,
                :has_followed,
                :has_saved,
                :followers_count,
                :needs_count,
                :members_count,
                :posts_count,
                :reviews_count

    delegate    :banner_url,
                :banner_url_sm,
                :followers_count,
                :needs_count,
                :posts_count,
                :reviews_count,
                :users_sm,
                to: :object

    def challenges
      if !context[:challenge_id].nil?
        ChallengesProject
          .where(project_id: object.id)
          .where(challenge_id: context[:challenge_id])
          .select(:challenge_id, :project_status)
          .as_json({ except: :id })
      elsif !context[:program_id].nil?
        ChallengesProject
          .joins(:challenge)
          .where(project_id: object.id)
          .where('challenges.program_id = ?', context[:program_id])
          .select(:challenge_id, :project_status)
          .as_json({ except: :id })
      else
        ChallengesProject
          .where(project_id: object.id)
          .select(:challenge_id, :project_status)
          .as_json({ except: :id })
      end
    end

    def affiliated_spaces
      if !context[:space_id].nil?
        ::Affiliation
          .where(affiliate_id: object.id)
          .where(parent_id: context[:space_id])
          .select(:parent_type,
                  :parent_id,
                  :status,
                  :id)
          .as_json
      else
        ::Affiliation
          .where(affiliate_id: object.id)
          .select(:parent_type,
                  :parent_id,
                  :status,
                  :id)
          .as_json
      end
    end

    def creator
      @@helper.creator(object)
    end

    def geoloc
      @@helper.geoloc(object)
    end

    def has_followed
      @@helper.context = context
      @@helper.has_followed(object)
    end

    def has_saved
      @@helper.context = context
      @@helper.has_saved(object)
    end

    def interests
      @@helper.interests(object)
    end

    def is_admin
      @@helper.is_admin(object)
    end

    def is_member
      @@helper.is_member(object)
    end

    def is_owner
      @@helper.is_owner(object)
    end

    def is_reviewer
      @@helper.is_reviewer(object)
    end

    def members_count
      object.members_count
    end

    def skills
      @@helper.skills(object)
    end
  end
end
