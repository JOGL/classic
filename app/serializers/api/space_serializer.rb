# frozen_string_literal: true

module Api
  class SpaceSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :available_tabs,
               :banner_url_sm,
               :banner_url,
               :challenges_count,
               :code_of_conduct,
               :contact_email,
               :end_date,
               :feed_id,
               :followers_count,
               :home_header,
               :home_info,
               :is_pending,
               :launch_date,
               :logo_url_sm,
               :logo_url,
               :meeting_information,
               :members_count,
               :needs_count,
               :onboarding_steps,
               :posts_count,
               :projects_count,
               :ressources,
               :saves_count,
               :selected_tabs,
               :short_description_fr,
               :short_description,
               :short_title_fr,
               :short_title,
               :show_featured,
               :space_type,
               :status,
               :title_fr,
               :title,
               :users_sm

    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :is_owner, unless: :scope?
    attribute :is_pending, unless: :scope?

    attribute :description, if: :show_objects?
    attribute :description_fr, if: :show_objects?
    attribute :documents, if: :show_objects?
    attribute :enablers, if: :show_objects?
    attribute :enablers_fr, if: :show_objects?

    def show_objects?
      @instance_options[:show_objects]
    end

    def ressources
      object.ressources
    end
  end
end
