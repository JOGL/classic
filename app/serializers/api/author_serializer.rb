# frozen_string_literal: true

module Api
  class AuthorSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper

    attributes :id,
               :first_name,
               :last_name,
               :nickname,
               :logo_url,
               :logo_url_sm,
               :short_bio

    attribute :has_clapped, unless: :scope?
    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?

    def scope?
      defined?(current_user).nil?
    end
  end
end
