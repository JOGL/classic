# frozen_string_literal: true

module Api
  class TagSerializer < ActiveModel::Serializer
    attributes :name
  end
end
