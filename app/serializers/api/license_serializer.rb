# frozen_string_literal: true

module Api
  class LicenseSerializer < ActiveModel::Serializer
    attributes :id,
               :title,
               :short_title,
               :version,
               :attr,
               :logo_url,
               :url,
               :url_legal
  end
end
