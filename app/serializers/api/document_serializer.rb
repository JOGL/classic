# frozen_string_literal: true

module Api
  class DocumentSerializer < ActiveModel::Serializer
    attributes :id,
               :title,
               :content

    has_many :users, :serializer => Api::AuthorSerializer
  end
end
