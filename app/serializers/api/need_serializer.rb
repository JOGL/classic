# frozen_string_literal: true

module Api
  class NeedSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :title,
               :content,
               :creator,
               :status,
               :feed_id,
               :users_sm,
               :end_date,
               :skills,
               :ressources,
               :project,
               :followers_count,
               :members_count,
               :saves_count,
               :posts_count,
               :created_at,
               :updated_at,
               :is_urgent

    attribute :documents, if: :show_objects?
    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :is_owner, unless: :scope?

    def project
      @project = object.project
      if @project.nil?
        {
          id: -1,
          title: object.title,
          banner_url: ""
        }
      else
        {
          id: @project.id,
          title: @project.title,
          banner_url: @project.banner_url
        }
      end
    end

    def show_objects?
      @instance_options[:show_objects]
    end
  end
end
