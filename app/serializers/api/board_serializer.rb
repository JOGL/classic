# frozen_string_literal: true

module Api
  class BoardSerializer < ActiveModel::Serializer
    attributes :id,
               :title,
               :description

    has_many :users
  end
end
