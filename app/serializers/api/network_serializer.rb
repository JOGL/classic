# frozen_string_literal: true

module Api
  class NetworkSerializer < ActiveModel::Serializer
    attributes :id,
               :edges,
               :created_at,
               :nodes,
               :updated_at

    def nodes
      Rails.application.routes.url_helpers.rails_blob_url(object.node_list)
    end

    def edges
      Rails.application.routes.url_helpers.rails_blob_url(object.edge_list)
    end
  end
end
