# frozen_string_literal: true

module Api
  class ChallengeSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :title,
               :title_fr,
               :banner_url,
               :banner_url_sm,
               :logo_url,
               :logo_url_sm,
               :short_title,
               :short_description,
               :short_description_fr,
               :custom_type,
               :rules,
               :rules_fr,
               :faq,
               :faq_fr,
               :status,
               :skills,
               :interests,
               :geoloc,
               :country,
               :city,
               :address,
               :feed_id,
               :users_sm,
               :program,
               :space,
               :launch_date,
               :end_date,
               :final_date,
               :followers_count,
               :members_count,
               :saves_count,
               :needs_count,
               :projects_count,
               :created_at,
               :updated_at,
               :posts_count

    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :is_owner, unless: :scope?
    # Show following attributes only if show_objects is true (set in controller, usually true only for :show api call)
    attribute :description, if: :show_objects?
    attribute :description_fr, if: :show_objects?
    attribute :documents, if: :show_objects?
    attribute :faq, if: :show_objects?
    attribute :faq_fr, if: :show_objects?
    attribute :rules, if: :show_objects?
    attribute :rules_fr, if: :show_objects?

    def program
      @program = object.program
      if @program.nil?
        {
          id: -1
        }
      else
        {
          id: @program.id,
          title: @program.title,
          title_fr: @program.title_fr,
          short_title: @program.short_title,
          custom_challenge_name: @program.custom_challenge_name
        }
      end
    end

    def space
      @space = object.space
      if @space.nil?
        {
          id: -1
        }
      else
        {
          id: @space.id,
          title: @space.title,
          title_fr: @space.title_fr,
          short_title: @space.short_title,
          custom_challenge_name: @space.custom_challenge_name
        }
      end
    end

    def show_objects?
      @instance_options[:show_objects]
    end
  end
end
