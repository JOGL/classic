# frozen_string_literal: true

module Api
  class CustomdataSerializer < ActiveModel::Serializer
    attributes  :id,
                :customfield_id,
                :user_id,
                :value
  end
end
