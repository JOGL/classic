# frozen_string_literal: true

module Api
  class WorkgroupSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :title,
               :short_title,
               :banner_url,
               :banner_url_sm,
               :short_description,
               :creator,
               :status,
               :skills,
               :ressources,
               :interests,
               :geoloc,
               :country,
               :city,
               :address,
               :feed_id,
               :is_private,
               :users_sm,
               :followers_count,
               :saves_count,
               :members_count,
               :created_at,
               :updated_at,
               :posts_count

    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :is_pending, unless: :scope?
    attribute :is_owner, unless: :scope?
    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    # Show following attributes only if show_objects is true (set in controller, usually true only for :show api call)
    attribute :description, if: :show_objects?

    def show_objects?
      @instance_options[:show_objects]
    end
  end
end
