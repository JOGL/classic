# frozen_string_literal: true

module Api
  class UserSerializerWithPrivateFields < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :active_status,
               :affiliation,
               :age,
               :badges,
               :bio,
               :birth_date,
               :can_contact,
               :category,
               :city,
               :confirmed_at,
               :country,
               :current_sign_in_at,
               :feed_id,
               :first_name,
               :gender,
               :id,
               :interests,
               :last_name,
               :logo_url_sm,
               :logo_url,
               :mail_newsletter,
               :mail_weekly,
               :nickname,
               :ressources,
               :short_bio,
               :skills,
               :stats,
               :status

    attribute :geoloc, unless: :scope?
    attribute :has_clapped, unless: :scope?
    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    attribute :is_admin, unless: :scope?

    def scope?
      defined?(current_user).nil?
    end

    def mutual_count
      if defined?(current_user).nil? || current_user.nil?
        0
      else
        object.follow_mutual_count(current_user)
      end
    end

    def stats
      {
        saves_count: object.saves_count,
        claps_count: object.claps_count,
        followers_count: object.followers_count,
        following_count: object.following_count,
        mutual_count: mutual_count,
        projects_count: object.projects_count,
        needs_count: object.needs_count,
        workgroups_count: object.workgroups_count,
        challenges_count: object.challenges_count,
        programs_count: object.programs_count,
        spaces_count: object.spaces_count
      }
    end
  end
end
