# frozen_string_literal: true

module Api
  class SharedChallengeSerializer < Panko::Serializer
    @@helper = Api::SerializerHelper.new

    attributes  :id,
                :title,
                :title_fr,
                :short_title,
                :short_description,
                :short_description_fr,
                :banner_url,
                :banner_url_sm,
                :logo_url,
                :logo_url_sm,
                :geoloc,
                :interests,
                :is_owner,
                :is_admin,
                :is_member,
                :has_followed,
                :has_saved,
                :followers_count,
                :members_count,
                :needs_count,
                :posts_count,
                :program,
                :affiliated_spaces,
                :projects_count,
                :status,
                :skills,
                :users_sm

    delegate    :banner_url,
                :banner_url_sm,
                :followers_count,
                :logo_url,
                :logo_url_sm,
                :needs_count,
                :projects_count,
                :posts_count,
                :users_sm,
                to: :object

    def program
      if !object.program.nil?
        {
          id: object.program.id,
          short_title: object.program.short_title,
          title: object.program.title,
          title_fr: object.program.title_fr
        }
      end
    end

    def affiliated_spaces
      if !context[:space_id].nil?
        ::Affiliation
          .where(affiliate_id: object.id)
          .where(parent_id: context[:space_id])
          .select(:parent_type,
                  :parent_id,
                  :status,
                  :id)
          .as_json
      else
        ::Affiliation
          .where(affiliate_id: object.id)
          .select(:parent_type,
                  :parent_id,
                  :status,
                  :id)
          .as_json
      end
    end

    def creator
      @@helper.creator(object)
    end

    def geoloc
      @@helper.geoloc(object)
    end

    def has_followed
      @@helper.context = context
      @@helper.has_followed(object)
    end

    def has_saved
      @@helper.context = context
      @@helper.has_saved(object)
    end

    def interests
      @@helper.interests(object)
    end

    def is_admin
      @@helper.is_admin(object)
    end

    def is_member
      @@helper.is_member(object)
    end

    def is_owner
      @@helper.is_owner(object)
    end

    def members_count
      object.members_count
    end

    def skills
      @@helper.skills(object)
    end
  end
end
