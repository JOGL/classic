# frozen_string_literal: true

module Api
  class ProjectIndexSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :title,
               :banner_url,
               :banner_url_sm,
               :short_description,
               :status,
               :skills,
               :geoloc,
               :interests,
               :is_private,
               :is_admin,
               :is_owner,
               :is_member,
               :is_reviewer,
               :users_sm,
               :has_followed,
               :followers_count,
               :needs_count
  end
end
