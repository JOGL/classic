# frozen_string_literal: true

module Api
  class FaqSerializer < ActiveModel::Serializer
    has_many :documents do
      object.documents.order(:position)
    end
  end
end
