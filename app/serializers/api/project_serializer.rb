# frozen_string_literal: true

module Api
  class ProjectSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :address,
               :affiliated_spaces,
               :badges,
               :banner_url_sm,
               :banner_url,
               :challenges,
               :city,
               :country,
               :created_at,
               :creator,
               :feed_id,
               :followers_count,
               :geoloc,
               :grant_info,
               :interests,
               :is_looking_for_collaborators,
               :is_private,
               :is_reviewed,
               :maturity,
               :members_count,
               :needs_count,
               :posts_count,
               :programs,
               :relation,
               :reviews_count,
               :saves_count,
               :short_description,
               :short_title,
               :skills,
               :status,
               :title,
               :updated_at,
               :users_sm

    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :is_owner, unless: :scope?
    attribute :is_pending, unless: :scope?
    attribute :is_reviewer, unless: :scope?

    # Show following attributes only if show_objects is true (set in controller, usually true only for :show api call)
    attribute :description, if: :show_objects?
    attribute :desc_communication_strat, if: :show_objects?
    attribute :desc_contributing, if: :show_objects?
    attribute :desc_elevator_pitch, if: :show_objects?
    attribute :desc_ethical_statement, if: :show_objects?
    attribute :desc_funding, if: :show_objects?
    attribute :desc_impact_strat, if: :show_objects?
    attribute :desc_objectives, if: :show_objects?
    attribute :desc_problem_statement, if: :show_objects?
    attribute :desc_progress, if: :show_objects?
    attribute :desc_stakeholder, if: :show_objects?
    attribute :desc_state_art, if: :show_objects?
    attribute :desc_sustainability_scalability, if: :show_objects?
    attribute :documents, if: :show_objects?
    attribute :documents_feed, if: :show_objects?

    def show_objects?
      @instance_options[:show_objects]
    end

    def challenges
      includes = %i[program]

      object.challenges.select('challenges.*, challenges_projects.project_status').includes(includes).map do |challenge|
        program = if challenge.program.nil?
                    {
                      id: -1,
                      title: '',
                      title_fr: '',
                      short_title: ''
                    }
                  else
                    {
                      id: challenge.program.id,
                      title: challenge.program.title,
                      title_fr: challenge.program.title_fr,
                      short_title: challenge.program.short_title
                    }
                  end
        {
          id: challenge.id,
          title: challenge.title,
          title_fr: challenge.title_fr,
          short_description: challenge.short_description,
          short_description_fr: challenge.short_description_fr,
          short_title: challenge.short_title,
          banner_url: challenge.banner_url,
          banner_url_sm: challenge.banner_url_sm,
          project_status: ChallengesProject.project_statuses.key(challenge.project_status), # This is needed as the SQL query above return the int value of the Enum
          status: challenge.status,
          program: program,
          space: space
        }
      end
    end

    def programs
      Program.where(id: Challenge.where(id: ChallengesProject.where(project_id: object.id, project_status: 1).select(:challenge_id)).select(:program_id)).select(
        :id, :title, :title_fr, :short_title
      )
    end

    def affiliated_spaces
      ::Affiliation.where(affiliate_type: 'Project', parent_type: 'Space', affiliate_id: object.id).map do |affiliation_space|
        Space.where(id: affiliation_space.parent_id).map do |space|
          {
            id: space.id,
            title: space.title,
            short_title: space.short_title,
            short_description: space.short_description,
            banner_url: space.banner_url,
            banner_url_sm: space.banner_url_sm,
            status: space.status,
            affiliation_id: affiliation_space.id,
            affiliation_status: affiliation_space.status
          }
        end
      end
    end

    def space
      ::Affiliation.where(affiliate_type: 'Project', parent_type: 'Space', affiliate_id: object.id).map do |affiliation_space|
        Space.where(id: affiliation_space.parent_id).map do |space|
          {
            id: space.id,
            title: space.title,
            short_title: space.short_title
          }
        end
      end
    end

    def scope?
      defined?(current_user).nil?
    end

    def relation
      object.respond_to?(:relation) ? object.relation : nil
    end
  end
end
