# frozen_string_literal: true

module Api
  class SharedNeedSerializer < Panko::Serializer
    @@helper = Api::SerializerHelper.new

    attributes  :id,
                :title,
                :content,
                :creator,
                :documents,
                :status,
                :feed_id,
                :users_sm,
                :skills,
                :ressources,
                :project,
                :is_owner,
                :is_member,
                :has_followed,
                :followers_count,
                :members_count,
                :posts_count,
                :has_saved,
                :is_urgent,
                :end_date,
                :created_at,
                :updated_at

    delegate    :followers_count,
                :posts_count,
                :users_sm,
                to: :object

    def creator
      @@helper.creator(object)
    end

    def documents
      @@helper.documents(object)
    end

    def feed_id
      object.feed.id
    end

    def has_followed
      @@helper.context = context
      @@helper.has_followed(object)
    end

    def has_saved
      @@helper.context = context
      @@helper.has_saved(object)
    end

    def is_member
      @@helper.is_member(object)
    end

    def is_owner
      @@helper.is_owner(object)
    end

    def members_count
      object.members_count
    end

    def ressources
      @@helper.ressources(object)
    end

    def project
      project = object.project
      if project.nil?
        {
          id: -1,
          title: object.title
        }
      else
        {
          id: project.id,
          title: project.title
        }
      end
    end

    def skills
      @@helper.skills(object)
    end
  end
end
