# frozen_string_literal: true

module Api
  class MentionSerializer < ActiveModel::Serializer
    attributes :obj_type,
               :obj_id,
               :obj_match
  end
end
