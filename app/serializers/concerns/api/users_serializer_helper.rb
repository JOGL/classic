# frozen_string_literal: true

module Api::UsersSerializerHelper
  include Rails.application.routes.url_helpers

  def get_user_json(user, obj)
    {
      id: user.id,
      first_name: user.first_name,
      last_name: user.last_name,
      nickname: user.nickname,
      last_active_at: user.last_active_at,
      skills: user.skills.pluck(:skill_name),
      ressources: user.ressources.pluck(:ressource_name),
      status: user.status,
      can_contact: user.can_contact,
      owner: user.has_role?(:owner, obj),
      admin: user.has_role?(:admin, obj),
      member: user.has_role?(:member, obj),
      reviewer: user.has_role?(:reviewer, obj),
      has_followed: has_followed(user),
      has_clapped: has_clapped(user),
      has_saved: has_clapped(user),
      current_sign_in_at: user.current_sign_in_at,
      projects_count: user.projects_count,
      mutual_count: user.mutual_count,
      logo_url: user.logo_url,
      logo_url_sm: user.logo_url_sm,
      short_bio: user.short_bio,
      geoloc: {
        lat: user.latitude,
        lng: user.longitude
      }
    }
  end

  def users(obj = nil)
    obj = object if obj.nil?
    obj.users.where(active_status: 'active').map do |user|
      get_user_json(user, obj)
    end
  end

  def members(obj = nil)
    obj = object if obj.nil?
    User.where(id: User.with_any_role(
      { name: :member, resource: obj },
      { name: :pending, resource: obj }
    ).map(&:id)).where(active_status: 'active').map do |user|
      get_user_json(user, obj)
    end
  end

  def reviewers(obj = nil)
    obj = object if obj.nil?
    User.where(id: User.with_any_role(
      { name: :reviewer, resource: obj }
    ).map(&:id)).where(active_status: 'active').map do |user|
      get_user_json(user, obj)
    end
  end

  def creator(obj = nil)
    obj = object if obj.nil?
    # Get the user id
    # TODO: move this to each model
    user_id = if obj.instance_of?(Post) || obj.instance_of?(Comment) || obj.instance_of?(Need)
                obj.user_id
              else
                obj.creator[:id]
              end

    # Get the user object
    user = User.find(user_id) || Users::DeletedUser.new
    # Render the object
    {
      id: user.id,
      first_name: user.first_name,
      last_name: user.last_name,
      logo_url: user.logo_url('80x80^'),
      logo_url_sm: user.logo_url_sm,
      short_bio: user.short_bio
    }
  end
end
