# frozen_string_literal: true

module Api::RelationsSerializerHelper
  # Helpers for clap and follow
  def has_clapped(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.clapped? obj
    end
  end

  def has_followed(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.follows? obj
    end
  end

  def has_saved(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.saved? obj
    end
  end
end
