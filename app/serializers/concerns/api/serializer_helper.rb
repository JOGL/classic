class Api::SerializerHelper
  include Api::RelationsSerializerHelper
  include Api::RolesSerializerHelper
  include Api::UsersSerializerHelper
  include Api::UtilsSerializerHelper

  attr_writer :context

  def current_user
    @context[:current_user]
  end
end
