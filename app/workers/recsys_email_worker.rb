# frozen_string_literal: true

require 'platform-api'

class RecsysEmailWorker
  include Sidekiq::Worker

  def perform
    users.confirmed.each do |user|
      next unless send?(user)
      
      RecsysNeedsFinder.new(user.id).call
    end
  end

  private

  # Only send an email to the user if their notification settings are enabled.
  def send?(user)
    user.settings.categories!.recsys!.enabled? and user.settings.categories!.recsys!.delivery_methods!.email!.enabled?
  end

  # Limit the users by the allowlist if present.
  def users
    return User.confirmed unless ENV['RECSYS_ALLOWLIST'].present?
    
    user_ids = ENV['RECSYS_ALLOWLIST'].split(',').map(&:strip)
    User.confirmed.where(id: user_ids)
  end
end
