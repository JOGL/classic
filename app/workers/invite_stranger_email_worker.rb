# frozen_string_literal: true

class InviteStrangerEmailWorker
  include Sidekiq::Worker

  def perform(user_id, object_type, object_id, to_email)
    invitor = User.find(user_id)
    object = object_type.constantize.find(object_id)

    begin
      InviteStrangerMailer.invite_stranger(invitor, object, to_email).deliver
    rescue Postmark::ApiInputError
      joiner_email.gsub! ',',''
      InviteStrangerMailer.invite_stranger(invitor, object, to_email).deliver
    end
  end
end
