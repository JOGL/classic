# frozen_string_literal: true

require 'csv'

class NetworkCsvMaker
  def initialize
    # Defining the attributes
    @node_attributes = %w{node type label status created_at updated_at}
    @edge_attributes = %w{source source_type target target_type relation_type value created_at updated_at}
  end

  def make_node_id(node)
    node.class.name + '_' + node.id.to_s
  end

  def make_node(node, label, status, created_at, updated_at)
    [make_node_id(node), node.class.name, label, status, created_at, updated_at]
  end

  def make_edge(source, target, edge)
    [make_node_id(source), source.class.name, make_node_id(target), target.class.name, edge.relation_type, edge.value, edge.created_at, edge.updated_at]
  end

  def find_records(c, created_at, &block)
    # Skill, Ressource and Interest currently do not have a created_at field
    return c.find_each if [Skill, Ressource, Interest].include?(c)

    if created_at.nil?
      c.find_each(&block)
    else
      c.where('created_at <= ?', created_at).or(c.where('created_at is null')).find_each(&block)
    end
  end

  def nodes_snapshot(created_at)
    # Going over all the nodes
    nodes_file = './tmp/network_nodes.csv'

    CSV.open(nodes_file, 'wb', headers: true) do |csv|
      csv << @node_attributes

      find_records(Skill, created_at) do |skill|
        csv << make_node(skill, skill.skill_name, "", "", "")
      end

      find_records(Ressource, created_at) do |ressource|
        csv << make_node(ressource, ressource.ressource_name, "", "", "")
      end

      find_records(Interest, created_at) do |interest|
        csv << make_node(interest, interest.interest_name, "", "", "")
      end

      find_records(User, created_at) do |user|
        csv << make_node(user, user.nickname, user.active_status, user.created_at, user.updated_at)
      end

      find_records(Post, created_at) do |post|
        csv << make_node(post, post.id, "", post.created_at, post.updated_at)
      end

      find_records(Comment, created_at) do |comment|
        csv << make_node(comment, comment.id, "", comment.created_at, comment.updated_at)
      end

      find_records(Need, created_at) do |need|
        csv << make_node(need, need.id, need.status, need.created_at, need.updated_at)
      end

      find_records(Project, created_at) do |project|
        csv << make_node(project, project.short_title, project.status, project.created_at, project.updated_at)
      end

      find_records(Workgroup, created_at) do |workgroup|
        csv << make_node(workgroup, workgroup.short_title, workgroup.status, workgroup.created_at, workgroup.updated_at)
      end

      find_records(Challenge, created_at) do |challenge|
        csv << make_node(challenge, challenge.short_title, challenge.status, challenge.created_at, challenge.updated_at)
      end

      find_records(Program, created_at) do |program|
        csv << make_node(program, program.short_title, program.status, program.created_at, program.updated_at)
      end
    end

    nodes_file
  end

  def edges_snapshot(created_at)
    # Going over the edges
    edges_file = './tmp/network_edges.csv'

    CSV.open(edges_file, 'wb', headers: true) do |csv|
      csv << @edge_attributes

      find_records(RecsysDatum, created_at) do |edge|
        source = edge.sourceable_node
        target = edge.targetable_node
        next if source.nil? || target.nil?

        csv << make_edge(source, target, edge)
      end
    end
    edges_file
  end

  def create_snapshot(network)
    nodes_file = nodes_snapshot(network.created_at)
    edges_file = edges_snapshot(network.created_at)
    network.node_list.attach(io: File.open(nodes_file), filename: 'network_nodes.csv')
    network.edge_list.attach(io: File.open(edges_file), filename: 'network_edges.csv')
    network.save
  end

  def create_network_snapshot
    network = Network.new
    create_snapshot(network)
  end

  def update_network_snapshot(network_id)
    return unless Network.exists?(network_id)

    network = Network.find(network_id)
    create_snapshot(network)
  end
end
