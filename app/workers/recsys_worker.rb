# frozen_string_literal: true

require 'platform-api'

class RecsysWorker
  include Sidekiq::Worker

  def perform
    # Create new recommendations
    # Instanciate to the Heorku API to launch a one off worker from our recsys docker image
    heroku = PlatformAPI.connect_oauth(ENV['HEROKU_API_TOKEN'])
    # Create the body of the requests
    # type is the name of the docker image (that's a bit confusing)
    body={"command": "python -m jogl.recsys -c #{ENV['RECSYS_CONFIG_FILE']}",
          "size": ENV['HEROKU_RECSYS_MACHINE_SIZE'],
          "type": "recsys",
          "time_to_live": 1800}

    dyno = heroku.dyno.create(ENV['HEROKU_APP'], body=body)
  end
end
