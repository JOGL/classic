# frozen_string_literal: true

class InviteUserEmailWorker
  include Sidekiq::Worker

  def perform(invitor_id, object_type, object_id, invitee_id)
    invitor = User.find(invitor_id)
    target = User.find(invitee_id)

    return if target.deleted?

    object = object_type.constantize.find(object_id)
    InviteUserMailer.invite_user(invitor, object, target).deliver
  end
end
