# frozen_string_literal: true

require_relative '../lib/service_flow_registry'

class ServiceFlowWorker
  include Sidekiq::Worker

  def perform(service_id, flow)
    service = Service.find(service_id)
    service_flow = ServiceFlowRegistry.load!(service, flow.to_sym)
  end
end
