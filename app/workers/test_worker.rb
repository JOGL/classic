# frozen_string_literal: true

class TestWorker
  include Sidekiq::Worker

  def perform
    Rails.logger.info 'POSITIVE ATTITUDE!'
  end
end
