# frozen_string_literal: true

class NotificationEmailWorker
  include Sidekiq::Worker

  def perform(id)
    return unless (notification = Notification.find(id))

    NotificationMailer.notify(notification).deliver
  end
end
