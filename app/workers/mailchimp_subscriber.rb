# frozen_string_literal: true

class MailchimpSubscriber
  include Sidekiq::Worker

  def perform(user_id, subscribe = true)
    if ENV['MAILCHIMP_API_KEY']
      @user = User.find_by(id: user_id)
      unless @user.nil?
        create_request
        call(@user, subscribe)
      end
    end
  rescue StandardError => e
    raise e
  end

  private

  def create_request
    @mailchimp = Gibbon::Request.new(api_key: ENV['MAILCHIMP_API_KEY'], symbolize_keys: true)
    @mailchimp.timeout = 30
    @mailchimp.open_timeout = 30
  end

  def call(user, subscribe = true)
    # raise MailchimpFailed.new unless @mailchimp
    status = (subscribe ? 'subscribed' : 'unsubscribed')
    list(user).upsert(
      body: {
        email_address: user.email,
        status: status,
        double_optin: false,
        merge_fields: {
          FNAME: user.first_name.to_s,
          LNAME: user.last_name.to_s,
          ADDRESS: "#{user.city}, #{user.country}",
          AFFI: user.affiliation.to_s,
          CAT: user.category.to_s,
          AGE: user.age.to_s,
          GENDER: user.gender.to_s
        }
      }
    )
  rescue Gibbon::MailChimpError => e
    raise e unless e.status_code == 400
  end

  def list(user)
    @mailchimp.lists(ENV['MAILCHIMP_LIST_ID']).members(
      Digest::MD5.hexdigest(user.email)
    )
  end
end
