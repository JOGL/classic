# frozen_string_literal: true

class Interest < ApplicationRecord
  include RecsysHelpers

  has_and_belongs_to_many :challenges
  has_and_belongs_to_many :workgroups
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :users
end
