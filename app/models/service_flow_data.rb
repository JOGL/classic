# frozen_string_literal: true

require_relative '../serializers/jsonb_serializer'

# Data for a specific ServiceFlow.
#
# @attr [Service] service Service the data belongs to
# @attr [String] flow Flow from which the data came
# @attr [Hash] data JSONB data represented as a Hash
class ServiceFlowData < ApplicationRecord
  belongs_to :service

  enum flow: { events: 0 }

  serialize :data, JsonbSerializer

  validates :data, :flow, presence: true
end
