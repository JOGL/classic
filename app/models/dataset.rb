# frozen_string_literal: true

class Dataset < ApplicationRecord
  include AlgoliaSearch
  include Mediumable
  include RecsysHelpers

  belongs_to :author, foreign_key: 'author_id', class_name: 'User'
  belongs_to :datasetable, polymorphic: true, optional: true

  has_many :data, dependent: :destroy
  has_many :sources, as: :sourceable, dependent: :destroy
  has_many :tags, as: :tagable, dependent: :destroy

  has_one :license, as: :licenseable, dependent: :destroy

  validates :type, presence: true
  validates :url, presence: true, unless: :customdataset

  has_paper_trail
  notification_object

  algoliasearch disable_indexing: !Rails.env.production? do
    use_serializer Api::DatasetSerializer
  end

  def customdataset
    type == Datasets::CustomDataset
  end
end
