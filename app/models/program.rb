# frozen_string_literal: true

# Umbrella object that holds this heirarchy: challenges -> projects -> needs
# Users can join the program (initiative)
# Can have a start and end date (as yet unused)
class Program < ApplicationRecord
  include AffiliatableChild
  include AlgoliaSearch
  include Avatarable
  include Bannerable
  include Boardable
  include Feedable
  include Linkable
  include Mediumable
  include Membership
  include NotificationsHelpers
  include RecsysHelpers
  include RelationHelpers
  include Utils

  belongs_to :space, optional: true

  enum status: %i[draft soon active completed]

  has_many :challenges
  has_many :resources, as: :documentable, class_name: 'Document', dependent: :destroy

  has_one :faq, as: :faqable, dependent: :destroy

  validates :short_title, uniqueness: true
  validates :title, presence: true

  before_create :sanitize_description
  before_update :sanitize_description

  notification_object
  resourcify

  algoliasearch disable_indexing: !Rails.env.production?, unless: :draft? do
    use_serializer Api::ProgramSerializer
  end

  def add_user_from_challenges
    challenges.each do |challenge|
      challenge.users.each do |user|
        next if users.include? user

        user.add_role(:member, self)
      end
    end
  end

  def needs_count
    challenges.includes(challenges_projects: { project: :needs }).where("challenges_projects.project_status = #{ChallengesProject.project_statuses[:accepted]}").count(:needs)
  end

  def projects_count
    challenges.includes(:projects).where("challenges_projects.project_status = #{ChallengesProject.project_statuses[:accepted]}").count(:projects)
  end

  # NOTE: the frontend has chosen "program" singular vs "programs" plural
  def frontend_link
    "/program/#{short_title}"
  end

  def all_owners_admins_members
    query = super()
    challenges.each do |challenge|
      query = query.or(challenge.all_owners_admins_members)
    end
    query
  end

  def user_is_member?(user)
    return true if super(user)

    challenges.any? { |challenge| challenge.user_is_member?(user) }
  end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-program.jpg')
  end

  def default_avatar_url
    ActionController::Base.helpers.image_url('default-avatar.png')
  end
end
