# frozen_string_literal: true

class Skill < ApplicationRecord
  include AlgoliaSearch
  include RecsysHelpers

  belongs_to :sourceable, polymorphic: true, optional: true
  belongs_to :targetable, polymorphic: true, optional: true

  has_and_belongs_to_many :challenge
  has_and_belongs_to_many :workgroup
  has_and_belongs_to_many :needs
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :users

  algoliasearch disable_indexing: Rails.env.test? do
    attribute :skill_name
  end
end
