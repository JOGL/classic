# frozen_string_literal: true

class Medium < ApplicationRecord
  belongs_to :mediumable, polymorphic: true

  has_one_attached :item

  validates :item, content_type: %w[image/png image/jpeg image/gif]
  validates :title, presence: true

  def item_url(resize = '600x600^')
    return item_variant_url(resize: resize) if item.attached? && item.variable?
    return item_blob_url if item.attached? && !item.variable?

    ''
  end

  def item_url_sm
    item_url('100x100^')
  end

  private

  def item_blob_url
    Rails
      .application
      .routes
      .url_helpers
      .rails_blob_url(item)
  end

  def item_variant_url(resize)
    Rails
      .application
      .routes
      .url_helpers
      .rails_representation_url(item.variant(resize: resize))
  end
end
