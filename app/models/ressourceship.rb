# frozen_string_literal: true

class Ressourceship < ApplicationRecord
  belongs_to :ressource
  belongs_to :ressourceable, polymorphic: true
end
