# frozen_string_literal: true

class NeedsProject < ApplicationRecord
  belongs_to :need
  belongs_to :project
end
