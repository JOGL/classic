# frozen_string_literal: true

class Activity < ApplicationRecord
  include Skillable

  belongs_to :service

  self.primary_keys = :service_id, :id

  enum category: { event: 0 }
end
