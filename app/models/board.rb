# frozen_string_literal: true

class Board < ApplicationRecord
  belongs_to :boardable, polymorphic: true, optional: true

  has_many :users_board
  has_many :users, through: :users_board

  validates :title, :description, presence: true
end
