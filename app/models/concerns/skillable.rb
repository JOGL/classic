# frozen_string_literal: true

module Skillable
  extend ActiveSupport::Concern

  included do
    has_and_belongs_to_many :skills
  end

  def update_skills(new_skills)
    new_skills = clean_array(new_skills)
    old_skills = skills.collect(&:skill_name)
    # Using set logic to get the new items
    skills_to_add = new_skills - old_skills
    unless skills_to_add.empty?
      skills_to_add.each do |skill_name|
        skill = Skill.find_or_create_by!(skill_name: skill_name)
        skills << skill
        add_edge(skill, 'has_skill')
      end
    end
    # Using set logic to get the items that don't exist anymore
    skills_to_remove = old_skills - new_skills
    unless skills_to_remove.empty?
      skills_to_remove.each do |skill_name|
        skill = Skill.find_by(skill_name: skill_name)
        skills.delete(skill)
        remove_edge(skill, 'has_skill')
      end
    end
  end

  private

  def clean_string(string)
    string.singularize.downcase.capitalize.strip
  end

  def clean_array(array)
    result = array.map do |el|
      el.split(',').map do |subel|
        clean_string(subel)
      end
    end
    result.flatten
  end
end
