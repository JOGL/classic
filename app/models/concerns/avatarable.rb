# frozen_string_literal: true

module Avatarable
  extend ActiveSupport::Concern

  included do
    has_one_attached :avatar
    validates :avatar, content_type: ['image/png', 'image/jpeg', 'image/gif']
    # validates :avatar, content_size: 314573 # Less than 300 Kb per avatar image
    # TODO Fix content_size validation and probably content type as well ...
  end

  def logo_url(resize = '200x200^')
    return avatar_variant_url(resize: resize) if avatar.attached? && avatar.variable?
    return avatar_blob_url if avatar.attached? && !avatar.variable?

    default_logo_url
  end

  def logo_url_sm
    logo_url('40x40^')
  end

  private

  def default_logo_url
    # NOTE: the below will add an asset digest, so
    # URLS may break if assets are changed in the future
    ActionController::Base.helpers.image_url('default-avatar.png')
  end

  def avatar_variant_url(resize:)
    Rails
      .application
      .routes
      .url_helpers
      .rails_representation_url(avatar.variant(resize: resize))
  end

  def avatar_blob_url
    Rails
      .application
      .routes
      .url_helpers
      .rails_blob_url(avatar)
  end
end
