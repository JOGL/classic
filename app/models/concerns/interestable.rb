# frozen_string_literal: true

module Interestable
  extend ActiveSupport::Concern

  included do
    has_and_belongs_to_many :interests
  end

  # challenge, workgroup, project, user
  def update_interests(new_interest_ids)
    # no need to clean interests array as it's a fixed set of integers
    current_interest_ids = interests.pluck(:id)
    # Using set logic to get the new items
    ids_of_interests_to_add = new_interest_ids - current_interest_ids
    Interest.where(id: ids_of_interests_to_add).find_each do |interest|
      interests << interest
      add_edge(interest, 'has_interest')
    end
    # Using set logic to get the items that don't exist anymore
    ids_of_interests_to_remove = current_interest_ids - new_interest_ids
    Interest.where(id: ids_of_interests_to_remove).find_each do |interest|
      interests.delete(interest)
      remove_edge(interest, 'has_interest')
    end
  end
end
