# frozen_string_literal: true

module Membership
  extend ActiveSupport::Concern

  def owners
    User.with_role(:owner, self)
  end

  def admins
    User.with_role(:admin, self)
  end

  def members
    User.with_role(:member, self)
  end

  def user_is_member?(user)
    user.has_role?(:member, self) ||
      user.has_role?(:admin, self) ||
      user.has_role?(:owner, self)
  end

  def pending_members
    User.with_role(:pending, self)
  end

  def users
    User
      .with_any_role({ name: :owner, resource: self },
                     { name: :admin, resource: self },
                     { name: :member, resource: self },
                     { name: :pending, resource: self })
  end

  def all_owners_admins_members
    owners
      .or(admins)
      .or(members)
      .active
      .except(:select)
      .select('DISTINCT ON (users.id) users.*, roles.name AS role')
      .order(Arel.sql("users.id, roles.name='owner' DESC, roles.name='admin' DESC, roles.name='member' DESC"))
  end

  def members_count
    all_owners_admins_members.active.size
  end

  def users_sm
    # Should return the first 6 members of a object ranked by their roles in the project
    # Owners first, then admins, then members.

    # NOTE:
    # feels bad that with_any_role returns an array and not a chainable ActiveRecord query object
    User
      .with_any_role({ name: :owner, resource: self },
                     { name: :admin,  resource: self },
                     { name: :member, resource: self })
      .uniq
      .first(6)
      .map do |user|
      {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        short_bio: user.short_bio,
        owner: user.has_role?(:owner, self),
        admin: user.has_role?(:admin, self),
        member: user.has_role?(:member, self),
        logo_url: user.logo_url_sm
      }
    end
  end

  def has_privacy?
    self.has_attribute?(:is_private)
  end
end
