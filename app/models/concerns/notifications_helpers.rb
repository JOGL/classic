# frozen_string_literal: true

module NotificationsHelpers
  extend ActiveSupport::Concern

  def notif_new_follower(follower)
    Notification.for_group(
      :admins,
      args: [self],
      attrs: {
        category: :follow,
        type: 'new_follower',
        object: self,
        metadata: {
          follower: follower
        }
      }
    )
  end
  
  def notif_new_member(member)
    Notification.for_group(
      :admins,
      args: [self],
      attrs: {
        category: :membership,
        type: 'new_member',
        object: self,
        metadata: {
          member: member
        }
      }
    )
  end

  def notif_pending_member(requestor)
    return unless self.has_privacy?
    return unless is_private

    Notification.for_group(
      :admins,
      args: [self],
      attrs: {
        category: :administration,
        type: 'pending_member',
        object: self,
        metadata: {
          requestor: requestor
        }
      }
    )
  end
end
