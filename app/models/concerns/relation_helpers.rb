# frozen_string_literal: true

module RelationHelpers
  extend ActiveSupport::Concern

  included do
    has_many :relations, as: :resource
  end

  def claps_count
    relations.clapped.size
  end

  def clappers
    relations.clapped.select(:user_id)
  end

  def followers_count
    relations.follows.size
  end

  def followers
    relations.follows.includes(:user).map(&:user)
  end

  def saves_count
    relations.saved.size
  end

  def reviews_count
    relations.reviewed.size
  end
end
