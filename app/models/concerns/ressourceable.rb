# frozen_string_literal: true

module Ressourceable
  extend ActiveSupport::Concern

  included do
    has_many :ressourceship, as: :ressourceable
    has_many :ressources, through: :ressourceship
  end

  def update_ressources(new_ressources)
    new_ressources = clean_array(new_ressources)
    old_ressources = ressources.collect(&:ressource_name)
    # Using set logic to get the new items
    ressources_to_add = new_ressources - old_ressources
    unless ressources_to_add.empty?
      ressources_to_add.each do |ressource_name|
        ressource = Ressource.find_or_create_by!(ressource_name: ressource_name)
        ressources << ressource
        add_edge(ressource, 'has_ressource')
      end
    end

    # Using set logic to get the items that don't exist anymore
    ressources_to_remove = old_ressources - new_ressources
    unless ressources_to_remove.empty?
      ressources_to_remove.each do |ressource_name|
        ressource = Ressource.find_by(ressource_name: ressource_name)
        ressources.delete(ressource)
        remove_edge(ressource, 'has_ressource')
      end
    end
  end

  private

  def clean_string(string)
    string.singularize.downcase.capitalize.strip
  end

  def clean_array(array)
    result = array.map do |el|
      el.split(',').map do |subel|
        clean_string(subel)
      end
    end
    result.flatten
  end
end
