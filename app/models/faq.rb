# frozen_string_literal: true

class Faq < ApplicationRecord
  belongs_to :faqable, polymorphic: true, optional: true

  has_many :documents, as: :documentable, dependent: :destroy
end
