# frozen_string_literal: true

class Mention < ApplicationRecord
  belongs_to :obj, polymorphic: true
  belongs_to :post

  validates :post_id, uniqueness: { scope: %i[obj_id obj_type] }

  notification_object

  def notif_new_mention(mentioner)
    case obj.class.name
    when 'User'
      notif_new_mention_user(mentioner)
    when 'Comment', 'Project'
      notif_new_mention_obj(mentioner)
    end
  end

  def notif_new_mention_user(mentioner)
    Notification.create(
      target: obj,
      category: :mention,
      type: 'new_mention',
      object: post,
      metadata: {
        mentioner: mentioner
      }
    )
  end

  def notif_new_mention_obj(mentioner)
    Notification.for_group(
      :admins,
      args: [obj],
      attrs: {
        category: :mention,
        type: 'new_mention',
        object: post,
        metadata: {
          mentioner: mentioner
        }
      }
    )
  end
end
