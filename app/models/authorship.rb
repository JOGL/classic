# frozen_string_literal: true

class Authorship < ApplicationRecord
  belongs_to :authorable, polymorphic: true
  belongs_to :user
end
