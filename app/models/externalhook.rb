# frozen_string_literal: true

class Externalhook < ApplicationRecord
  belongs_to :hookable, polymorphic: true

  has_many :slackhooks
end
