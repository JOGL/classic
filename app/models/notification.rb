# frozen_string_literal: true

require 'erb'
require 'yaml'

class Notification < NotificationHandler::Notification
  include Notifications

  before_create :set_cta_link
  before_create :set_subject_line
  after_create :push_notifications

  scope :unread, -> { where(read: false) }

  def push_notifications
    deliver(:email)
  end

  def read!
    update(read: true)
  end

  def deliver_email
    if target.settings.delivery_methods!.email! and target.settings.categories!.send("#{category}!").enabled?
      NotificationEmailWorker.perform_async(id)
    end
  end

  def set_cta_link
    file = File.open(Rails.root.join('config/notifications/cta_links.yml'))
    templates = YAML.safe_load(file)

    self.cta_link = object.frontend_link and return unless templates.key?(type)

    template = ERB.new(templates[type])

    self.cta_link = template.result_with_hash object: object, metadata: metadata
  end

  def set_subject_line
    file = File.open(Rails.root.join("config/locales/#{I18n.locale}.yml"))
    templates = YAML.safe_load(file)

    self.subject_line = '' and return unless templates.dig(I18n.locale.to_s, 'notifications', 'subject_lines', type)

    template = ERB.new(templates.dig(I18n.locale.to_s, 'notifications', 'subject_lines', type))

    self.subject_line = template.result_with_hash object: object, metadata: metadata
  end
end
