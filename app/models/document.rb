# frozen_string_literal: true

class Document < ApplicationRecord
  include Utils

  belongs_to :documentable, polymorphic: true, optional: true

  has_many :authorship, as: :authorable
  has_many :users, through: :authorship

  before_create :sanitize_content
  before_update :sanitize_content

  validates :title, :content, presence: true
end
