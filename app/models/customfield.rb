# frozen_string_literal: true

class Customfield < ApplicationRecord
  belongs_to :resource, polymorphic: true

  has_many :customdatas
end
