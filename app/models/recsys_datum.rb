# frozen_string_literal: true

class RecsysDatum < ApplicationRecord
  belongs_to :sourceable_node, polymorphic: true
  belongs_to :targetable_node, polymorphic: true
end
