# frozen_string_literal: true

# Affiliate two objects: one as the parent and the other as the child (affiliate).
#
# @example Affiliate a challenge with a space (child)
#   affiliation = Challenge.first.add_affiliation_to(Space.first)
#   affiliation.save!
#
# @example Affiliate a challenge with a space (parent)
#   affiliation = Space.first.add_affiliation(Challenge.first)
#   affiliation.save!
class Affiliation < ApplicationRecord
  belongs_to :affiliate, polymorphic: true
  belongs_to :parent, polymorphic: true

  enum status: { pending: 0, accepted: 1 }

  after_create :affiliation_created
  after_update :affiliation_updated
  before_destroy :affiliation_destroyed

  validates :affiliate_id, uniqueness: { scope: %i[affiliate_type parent_id parent_type] }

  private

  def affiliation_created
    notif_parent('affiliation_pending') if pending?
  end

  def affiliation_updated
    return unless saved_change_to_status? && accepted?

    user? ? notif_user('affiliation_accepted') : notif_affiliate('affiliation_accepted')
  end

  def affiliation_destroyed
    if pending?
      user? ? notif_user('affiliation_rejected') : notif_affiliate('affiliation_rejected')
    else
      user? ? notif_user('affiliation_destroyed') : notif_affiliate('affiliation_destroyed')
    end
  end

  def user?
    affiliate_type == 'User'
  end

  def notif_affiliate(type)
    Notification.for_group(
      :admins,
      args: [affiliate],
      attrs: {
        category: :administration,
        type: type,
        object: affiliate,
        metadata: {
          parent: parent
        }
      }
    )
  end

  def notif_parent(type)
    Notification.for_group(
      :admins,
      args: [parent],
      attrs: {
        category: :administration,
        type: type,
        object: parent,
        metadata: {
          affiliate: affiliate
        }
      }
    )
  end

  def notif_user(type)
    Notification.create(
      target: affiliate,
      category: :administration,
      type: type,
      object: affiliate,
      metadata: {
        parent: parent
      }
    )
  end
end
