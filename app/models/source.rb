# frozen_string_literal: true

class Source < ApplicationRecord
  belongs_to :sourceable, polymorphic: true
end
