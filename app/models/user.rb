# coding: utf-8
# frozen_string_literal: true

require 'csv'

class User < ApplicationRecord
  include AffiliatableChild
  include AlgoliaSearch
  include Avatarable
  include Coordinates
  include Feedable
  include Geocodable
  include Interestable
  include Linkable
  include NotificationsHelpers
  include RecsysHelpers
  include RelationHelpers
  include Ressourceable
  include Skillable
  include Utils

  accepts_nested_attributes_for :interests, allow_destroy: true

  enum active_status: %i[active archived]

  has_many :customdatas
  has_many :datasets, as: :datasetable
  has_many :owned_relations, class_name: 'Relation'
  has_many :posts

  after_create :active! # sets status to "active"
  after_create :send_confirmation_instructions, if: -> { !Rails.env.test? && ::User.devise_modules.include?(:confirmable) }
  after_update :mailchimp
  after_validation :geocode

  before_create :create_coordinates
  before_create :set_defaults
  before_create :skip_duplicate_devise_confirmation_email

  VALID_COUNTRY_NAMES = ['Afghanistan', 'Åland', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'British Indian Ocean Territory', 'British Virgin Islands', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos [Keeling], Islands', 'Colombia', 'Comoros', 'Congo', 'Cook Islands', 'Costa Rica', 'Croatia', 'Cuba', 'Curaçao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Islands', 'Federated States of Micronesia', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Territories', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Hashemite Kingdom of Jordan', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Ivory Coast', 'Jamaica', 'Japan', 'Jersey', 'Kazakhstan', 'Kenya', 'Kiribati', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Luxembourg', 'Macao', 'Macedonia', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar [Burma],', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'North Korea', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn Islands', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Republic of Korea', 'Republic of Lithuania', 'Republic of Moldova', 'Republic of the Congo', 'Réunion', 'Romania', 'Russia', 'Rwanda', 'Saint Helena', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin', 'Saint Pierre and Miquelon', 'Saint Vincent and the Grenadines', 'Saint-Barthélemy', 'Samoa', 'San Marino', 'São Tomé and Príncipe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Sint Maarten', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Georgia and the South Sandwich Islands', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo', 'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'U.S. Minor Outlying Islands', 'U.S. Virgin Islands', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City', 'Venezuela', 'Vietnam', 'Wallis and Futuna', 'Yemen', 'Zambia', 'Zimbabwe'].freeze

  validates :email, :nickname, uniqueness: true
  validates :country,
            allow_nil: true,
            inclusion: {
              in: VALID_COUNTRY_NAMES
            },
            unless: -> { reset_password_token.present? }

  devise :async,
         :confirmable,
         :database_authenticatable,
         :lockable,
         :jwt_authenticatable,
         :omniauthable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         jwt_revocation_strategy: JwtDenylist,
         omniauth_providers: %i[eventbrite]

  geocoded_by :make_address
  has_merit
  notification_object
  notification_target
  rolify strict: true
  valid_affiliations :space

  scope :confirmed, -> { where.not(confirmed_at: nil) }
  
  algoliasearch disable_indexing: !Rails.env.production?, if: :confirmed?, unless: :deleted? do
    use_serializer Api::UserSerializer

    add_attribute :is_confirmed do
      confirmed?
    end

    geoloc :latitude, :longitude
  end

  # Fixes problem with duplicate account confirmation emails
  def skip_duplicate_devise_confirmation_email
    skip_confirmation_notification!
  end

  alias deleted? archived?

  def active_at!
    update(last_active_at: DateTime.now)
  end
  
  def projects
    Project.where(id: roles.where(resource_type: 'Project').pluck(:resource_id))
  end

  def challenges
    Challenge.where(id: roles.where(resource_type: 'Challenge').pluck(:resource_id))
  end

  def workgroups
    Workgroup.where(id: roles.where(resource_type: 'Workgroup').pluck(:resource_id))
  end

  def programs
    Program.where(id: roles.where(resource_type: 'Program').pluck(:resource_id))
  end

  def spaces
    Space.where(id: roles.where(resource_type: 'Space').pluck(:resource_id))
  end

  def needs
    Need.where(id: roles.where(resource_type: 'Need').pluck(:resource_id))
  end

  def projects_count
    # Use the roles relation to filter projects where the user is a member
    roles.where(name: 'member', resource_type: 'Project').size
  end
  
  def needs_count
    Need.with_role(:member, self).size
  end

  def workgroups_count
    Workgroup.with_role(:member, self).size
  end

  def challenges_count
    challenges.count
  end

  def programs_count
    programs.count
  end

  def spaces_count
    spaces.count
  end

  def owned_posts_count
    posts.size
  end

  # override RelationHelpers for user (because it's owned_relations and not relations)
  def saves_count
    owned_relations.saved.size
  end

  # override RelationHelpers for user (because it's owned_relations and not relations)
  def reviews_count
    owned_relations.saved.size
  end

  # override RelationHelpers for user (because it's owned_relations and not relations)
  def claps_count
    owned_relations.saved.size
  end

  def token_validation_response
    Api::UserSigninSerializer.new(self, root: false).as_json
  end

  # Returns the users that self and user are following together (Or the interesection of their follow).
  def follow_mutual(user)
    mine = Set.new(owned_relations.follows.of_type('User').pluck(:resource_id))
    other = Set.new(user.owned_relations.follows.of_type('User').pluck(:resource_id))
    mutual = mine.intersection(other)
    User.where(id: mutual)
  end

  def follow_mutual_count(user)
    follow_mutual(user).count
  end

  # helpers for relation state.
  def follows?(object)
    owned_relations.where(resource: object).follows.any?
  end

  def clapped?(object)
    owned_relations.where(resource: object).clapped.any?
  end

  def saved?(object)
    owned_relations.where(resource: object).saved.any?
  end

  def reviewed?(object)
    owned_relations.where(resource: object).reviewed.any?
  end

  def mailchimp
    MailchimpSubscriber.perform_async(id, mail_newsletter)
  end

  def make_address
    return ip unless city.present?

    super
  end

  def has_badge?(badge_name)
    badges.any? { |badge| badge_name == badge.name }
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  alias title full_name

  def frontend_link
    "/user/#{id}"
  end

  def following_count
    following.size
  end

  def following
    owned_relations.follows
  end

  def timeline_posts
    # using IDs because concating onto feed.posts actually creates a feeds_posts record
    # and adds the post to the user's feeds permanently
    posts_ids = feed.post_ids
    following.map do |object|
      posts_ids.concat(object.resource.feed.post_ids) if object.resource.present?
    end
    Post.where(id: posts_ids).distinct(:id).order(created_at: :desc)
  end

  def notif_new_follower(follower)
    Notification.create(
      target: self,
      category: :follow,
      type: 'new_user_follower',
      object: follower
    )
  end

  def notif_new_clap(clapper)
    Notification.create(
      target: self,
      category: :clap,
      type: 'new_clap',
      object: self,
      metadata: {
        clapper: clapper
      }
    )
  end

  def notif_new_post_on_profile_feed
    Notification.for_group(
      :user_followers,
      args: [self],
      attrs: {
        category: :feed,
        type: 'new_post_on_profile_feed',
        object: self
      }
    )
  end

  def age
    return calculate_age if birth_date.present?

    self[:age]
  end

  # a worker resets this limit on the first day of each month
  def direct_message_limit_reached?
    message_limit = ENV['DIRECT_MESSAGE_LIMIT'] || 20
    direct_message_count >= message_limit
  end

  def add_role(*args)
    super(*args)
    # args[1] will be some resource that had a user role added to it, so let's reindex it
    # so it's user counts are accurate
    args[1]&.index!
  end

  def set_settings(new_settings)
    # Hashie::Mash syntax is
    settings.enabled = booleanify(new_settings[:enabled])
    new_settings[:categories]&.each do |category, _value|
      settings.categories!.send("#{category}!").enabled = booleanify(new_settings[:categories][category][:enabled])
      new_settings[:categories][category][:delivery_methods].each do |delivery_method, _value|
        settings.categories!.send("#{category}!").delivery_methods!.send("#{delivery_method}!").enabled = booleanify(new_settings[:categories][category][:delivery_methods][delivery_method][:enabled])
      end
    end
    new_settings[:delivery_methods]&.each do |delivery_method, _value|
      settings.delivery_methods!.send("#{delivery_method}!").enabled = booleanify(new_settings[:delivery_methods][delivery_method][:enabled])
    end
    save
  end

  def booleanify(value)
    ActiveModel::Type::Boolean.new.cast(value)
  end

  def set_defaults
    # Hashie::Mash is expected by the notifications gem
    settings = Hashie::Mash.new(
      enabled: true,
      categories: {},
      delivery_methods: {}
    )

    NotificationSettings.configuration.categories.each do |category|
      settings[:categories][category] = { enabled: true, delivery_methods: {} }
      NotificationPusher.configuration.delivery_methods.each do |delivery_method, _|
        settings[:categories][category][:delivery_methods][delivery_method] = { enabled: true }
      end
    end

    NotificationPusher.configuration.delivery_methods.each do |delivery_method, _|
      settings[:delivery_methods][delivery_method] = { enabled: true }
    end

    self.settings = settings
  end

  def self.to_csv(users)
    attributes = %w[id first_name last_name email] #customize columns here
    CSV.generate(headers: true) do |csv|
      csv << attributes
      users.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

  private

  def calculate_age
    # credit: https://stackoverflow.com/a/820186/1706831
    age_in_years = Date.today.year - birth_date.year
    age_in_years -= 1 if Date.today < birth_date + age_in_years.years
    age_in_years
  end

  def default_logo_url
    srand(id || -1)
    # NOTE: the below will add an asset digest, so
    # URLS may break if assets are changed in the future
    ActionController::Base.helpers.image_url("default-user-#{rand(1...12)}.png")
  end

  def self.from_omniauth(auth)
    User.find_or_create_by!(provider: auth.provider, uid: auth.uid) do | user |
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
    end
  end
end
