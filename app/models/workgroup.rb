# frozen_string_literal: true

class Workgroup < ApplicationRecord
  include AffiliatableChild
  include AlgoliaSearch
  include Bannerable
  include Coordinates
  include Feedable
  include Geocodable
  include Interestable
  include Linkable
  include Mediumable
  include Membership
  include NotificationsHelpers
  include RecsysHelpers
  include RelationHelpers
  include Ressourceable
  include Skillable
  include Utils

  belongs_to :creator, foreign_key: 'creator_id', class_name: 'User'
  belongs_to :space, optional: true

  enum status: { active: 0, archived: 1 }

  has_and_belongs_to_many :workgroup_tags

  has_many :challenges_workgroups
  has_many :challenges, through: :challenges_workgroups
  has_many :externalhooks, as: :hookable
  has_many :projects

  after_validation :geocode

  before_create :create_coordinates
  before_create :sanitize_description
  before_update :sanitize_description

  validates :short_title, uniqueness: true

  geocoded_by :make_address
  notification_object
  resourcify
  valid_affiliations :space

  algoliasearch disable_indexing: !Rails.env.production?, unless: :archived? do
    use_serializer Api::WorkgroupSerializer
    geoloc :latitude, :longitude
  end

  def frontend_link
    "/workgroup/#{id}"
  end

  def notif_pending_join_request(requestor)
    Notification.create(
      target: requestor,
      category: :membership,
      type: 'pending_join_request',
      object: self
    )
  end

  def notif_pending_join_request_approved(requestor)
    Notification.create(
      target: requestor,
      category: :membership,
      type: 'pending_join_request_approved',
      object: self
    )
  end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-group.jpg')
  end
end
