# frozen_string_literal: true

# An external service that JOGL can use on behalf of a User within the context of a Serviceable object.
#
# @attr [User] creator User who created the Service
# @attr [String] name popular name of the Service
# @attr [String] token provides access to the Service
class Service < ApplicationRecord
  belongs_to :creator, class_name: 'User'
  belongs_to :serviceable, polymorphic: true

  has_many :activities, dependent: :destroy

  enum name: { eventbrite: 0 }

  validates :creator, :name, :token, presence: true
  validates :name, uniqueness: { scope: %i[serviceable_id serviceable_type] }

  # Return all the flow names for this Service in the ServiceFlowRegistry.
  #
  # @return [Array<Symbol>] the .
  def flows
    ServiceFlowRegistry.service_flows[name.to_sym].keys
  end

  def as_json(_options = {})
    super(except: [:token], methods: [:flows])
  end
end
