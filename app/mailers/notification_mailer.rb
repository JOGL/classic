# frozen_string_literal: true

class NotificationMailer < ApplicationMailer
  def notify(notification)
    @metadata = notification.metadata
    @notification = notification
    @object = notification.object
    @to = notification.target
    @type = notification.type

    # Postmark metadatas
    metadata['user-id-to'] = notification.target.id

    mail(
      to: "<#{notification.target.email}>",
      from: "JOGL - Just One Giant Lab <notifications@#{Rails.configuration.email}>",
      subject: notification.subject_line,
      template_path: 'notification_mailer',
      template_name: 'notification',
      tag: notification.type,
      message_stream: 'notifications' # Postmark
    )
  end
end
