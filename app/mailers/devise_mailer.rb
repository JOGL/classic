# frozen_string_literal: true

require 'digest/sha2'

class DeviseMailer < Devise::Mailer
  layout 'external_mailer'

  default from: "JOGL - Just One Giant Lab <contact@#{Rails.configuration.email}>",
          message_id: "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@jogl.io>",
          message_stream: 'devise' # Postmark

  # Override the base method in order to pass in @to.
  def confirmation_instructions(record, token, opts = {})
    @to = record
    @token = token

    devise_mail(record, :confirmation_instructions, opts)
  end

  # Override the base method in order to pass in @to.
  def reset_password_instructions(record, token, opts = {})
    @to = record
    @token = token

    devise_mail(record, :reset_password_instructions, opts)
  end
end
