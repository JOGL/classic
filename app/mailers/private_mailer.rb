# frozen_string_literal: true

class PrivateMailer < ApplicationMailer
  def send_private_email(from, to, object, content)
    @content = content
    @from = from
    @to = to
    @object = object

    subject = object.nil? ? "#{@from.first_name} sent you a message" : object

    # Postmark metadatas
    metadata['user-id-from'] = @from.id
    metadata['user-id-to'] = @to.id

    mail(
      to: "#{@to.full_name} <#{@to.email}>",
      from: "#{@from.full_name} via JOGL <messages@#{Rails.configuration.email}>",
      reply_to: "#{@from.full_name} <#{@from.email}>",
      subject: subject,
      tag: 'private-message'
    )
  end
end
