# frozen_string_literal: true

class InviteUserMailer < ApplicationMailer
  def invite_user(invitor, object, to)
    @invitor = invitor
    @object = object
    @object_type = object.class.name.casecmp('workgroup').zero? ? 'group' : object.class.name.downcase
    @to = to

    # Postmark metadatas
    metadata['object-type'] = @object_type

    mail(
      to: "<#{to.email}>",
      subject: "You have been invited to join a #{@object_type}",
      tag: 'invite-user'
    )
  end
end
