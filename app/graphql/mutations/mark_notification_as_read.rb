# frozen_string_literal: true

module Mutations
  class MarkNotificationAsRead < BaseMutation
    field :notification, Types::NotificationType, null: false
    field :errors, [String], null: false
    field :unread_count, Integer, null: true

    def unread_count
      context[:current_user].notifications.unread.size
    end

    argument :id, ID, required: true

    def resolve(id:)
      notification = Notification.find(id)
      if notification.read?
        {
          notification: notification,
          unread_count: unread_count,
          errors: ["Notification already marked as 'read'"]
        }
      elsif notification.unread?
        if notification.read!
          # SUCCESS
          {
            notification: notification,
            unread_count: unread_count,
            errors: []
          }
        else
          # TODO: error
          {}
        end
      end
    end
  end
end
