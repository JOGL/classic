module Types
  class ProgramType < Types::BaseObject
    field :id, ID, null: false
    field :description, String, null: true
    field :title, String, null: true
    field :launch_date, GraphQL::Types::ISO8601DateTime, null: true
    field :end_date, GraphQL::Types::ISO8601DateTime, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :short_title, String, null: true
    field :faq, String, null: true
    field :enables, String, null: true
    field :ressources, String, null: true
    field :short_description, String, null: true
    field :title_fr, String, null: true
    field :short_title_fr, String, null: true
    field :description_fr, String, null: true
    field :faq_fr, String, null: true
    field :enables_fr, String, null: true
    field :status, ProgramStatus, null: false
    field :contact_email, String, null: true
    field :meeting_information, String, null: true
    field :custom_challenge_name, String, null: true

    # The space this program belongs to, if null it belongs to
    # the (implicit) JOGL "space"
    field :space, SpaceType, null: true

    def status
      %i[DRAFT SOON ACTIVE COMPLETED][object.status]
    end
  end
end
