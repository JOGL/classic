# frozen_string_literal: true

module Api
  module Utils
    extend ActiveSupport::Concern

    included do
      before_action :recsys_has_seen, only: [:show]
      # before_action :is_reviewer, only: [:review]
    end

    # Those methods are security methods that check that the user has the correct role to execute a particular methods.
    # They are to be included in the before_action of controllers to check for the correct role of a user
    # They are different from the Serializer concerns of the same name as they do not participate in the JSON rendering of the objects.

    def is_admin
      if current_user.nil?
        render json: { data: 'unauthorized' }, status: :unauthorized
      else
        if @obj.nil?
          render json: { data: 'Obj is not set' }, status: :not_found
        else
          if @obj.class.name == 'Need'
            render json: { data: 'Forbidden' }, status: :forbidden unless current_user.has_role?(:admin,
                                                                                                 @obj.project) || current_user.has_role?(:admin,
                                                                                                                                         @obj) || current_user.has_role?(:admin)
          else
            render json: { data: 'Forbidden' }, status: :forbidden unless current_user.has_role?(:admin, @obj) || current_user.has_role?(:admin)
          end
        end
      end
    end

    def is_member
      if current_user.nil?
        render json: { data: 'Unauthorized' }, status: :unauthorized
      else
        render json: { data: 'Forbidden' }, status: :forbidden unless current_user.has_role? :member, @obj
      end
    end

    def is_reviewer
      if current_user.nil?
        render json: { data: 'Unauthorized' }, status: :unauthorized
      else
        if @obj.nil?
          render json: { data: 'Obj is not set' }, status: :not_found
        else
          if @obj.class.name == 'Need'
            render json: { data: 'Forbidden' }, status: :forbidden unless current_user.has_role?(:reviewer,
                                                                                                 @obj.project) || current_user.has_role?(:reviewer,
                                                                                                                                         @obj) || current_user.has_role?(:reviewer)
          else
            render json: { data: 'Forbidden' }, status: :forbidden unless current_user.has_role?(:reviewer, @obj) || current_user.has_role?(:reviewer)
          end
        end
      end
    end

    def nickname_exist
      if User.where(nickname: params[:nickname]).count > 0
        render json: { data: 'Nickname already exists' }, status: :forbidden
      else
        render json: { data: 'Nickname is available' }, status: :ok
      end
    end

    def short_title_exist
      klass = controller_name.classify.constantize
      if klass.where(short_title: params[:short_title]).count > 0
        render json: { data: 'short_title already exists' }, status: :forbidden
      else
        render json: { data: 'short_title is available' }, status: :ok
      end
    end

    # NOTE: only used by [challenges, workgroups, programs, projects, spaces]
    # refactor this
    def get_id_from_short_title
      klass = controller_name.classify.constantize
      @obj = klass.where(short_title: params[:short_title]).first
      if @obj.nil?
        render json: { data: 'short_title does not exists' }, status: :not_found
      else
        render json: { id: @obj.id, data: 'Success' }, status: :ok
      end
    end

    private

    def recsys_has_seen
      current_user.add_edge(@obj, 'has_visited') unless current_user.nil? || @obj.nil?
    end
  end
end
