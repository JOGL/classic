# frozen_string_literal: true

module Api
  module Media
    extend ActiveSupport::Concern

    included do
      before_action :media_authenticate_user!, only: %i[media_create media_update media_destroy media_upload]
      before_action :media_is_admin!, only: %i[media_create media_update media_destroy media_upload]
      before_action :set_medium, only: %i[media_show media_update media_destroy media_upload]
    end

    # GET /media
    def media_index
      @media = @obj.media
      render json: @media
    end

    # GET /media/1
    def media_show
      render json: @medium
    end

    # POST /media
    def media_create
      @medium = Medium.new(medium_params)
      @medium.mediumable = @obj
      if @medium.save
        raise ApiExceptions::UnprocessableEntity.new(message: 'Missing item') if params[:item].nil?

        @medium.item.attach(params[:item])
        raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong') unless @medium.item.attached?

        @medium.item.filename = @medium.item.filename.to_s.sub('(', '-').sub(')', '-')

        unless @medium.item.save
          raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong')
        end

        render json: @medium, status: :created
      else
        render json: @medium.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /media/1
    def media_update
      if @medium.update(medium_params)
        render json: @medium
      else
        render json: @medium.errors, status: :unprocessable_entity
      end
    end

    # DELETE /media/1
    def media_destroy
      @medium.destroy
    end

    def media_upload
      raise ApiExceptions::UnprocessableEntity.new(message: 'Missing item') if params[:item].nil?

      @medium.item.attach(params[:item])
      raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong') unless @medium.item.attached?

      @medium.item.filename = @medium.item.filename.to_s.sub('(', '-').sub(')', '-')

      unless @medium.item.save
        raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong')
      end

      render json: @medium
    end

    private

    def media_authenticate_user!
      authenticate_user!
    end

    def media_is_admin!
      is_admin
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_medium
      @medium = Medium.find(params[:medium_id])
    end

    # Only allow a list of trusted parameters through.
    def medium_params
      params.require(:medium).permit(:title, :alt_text)
    end
  end
end
