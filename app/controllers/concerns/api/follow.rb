# frozen_string_literal: true

module Api
  module Follow
    extend ActiveSupport::Concern

    # Those helpers are user for finding followers and following of any given object
    # the serializer return a list of users.
    def followers
      render json: @obj, serializer: Api::FollowersSerializer
    end

    def following
      render json: @obj, serializer: Api::FollowingSerializer
    end
  end
end
