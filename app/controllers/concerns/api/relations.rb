# frozen_string_literal: true

module Api
  module Relations
    extend ActiveSupport::Concern

    included do
      before_action :follow_authenticate_user!, only: %i[clap follow review reviewed_object save saved_objects]
      before_action :find_relation, only: %i[clap follow review save]
    end

    def follow
      case request.method_symbol
      when :put
        @relation.follows = true
        save_relation('Followed')
        @relation.resource.notif_new_follower(current_user)
        current_user.add_edge(@relation.resource, 'has_followed')
      when :get
        render json: { has_followed: current_user.follows?(@obj) }, status: :ok
      when :delete
        @relation.follows = false
        save_relation('Unfollowed')
        current_user.remove_edge(@relation.resource, 'has_followed')
      end
    end

    def clap
      case request.method_symbol
      when :put
        @relation.has_clapped = true
        save_relation('Clapped')
        @relation.resource.notif_new_clap(current_user)
        current_user.add_edge(@relation.resource, 'has_clapped')
      when :get
        render json: { has_clapped: current_user.clapped?(@obj) }, status: :ok
      when :delete
        @relation.has_clapped = false
        save_relation('Unclapped')
        current_user.remove_edge(@relation.resource, 'has_clapped')
      end
    end

    def save
      case request.method_symbol
      when :put
        @relation.saved = true
        save_relation('Saved')
        current_user.add_edge(@relation.resource, 'has_saved')
      when :get
        render json: { has_saved: current_user.saved?(@obj) }, status: :ok
      when :delete
        @relation.saved = false
        save_relation('Unsaved')
        current_user.remove_edge(@relation.resource, 'has_saved')
      end
    end

    def review
      case request.method_symbol
      when :put
        @relation.reviewed = true
        save_relation('Reviewed')
        current_user.add_edge(@relation.resource, 'has_reviewed')
      when :get
        render json: { has_reviewed: current_user.reviewed?(@obj) }, status: :ok
      when :delete
        @relation.reviewed = false
        save_relation('Unreviewed')
        current_user.remove_edge(@relation.resource, 'has_reviewed')
      end
    end

    def clappers
      render json: @obj, serializer: Api::ClappersSerializer
    end

    def saved_objects
      render json: current_user, serializer: Api::SavedSerializer
    end

    def reviewed_object
      render json: current_user, serializer: Api::ReviewedSerializer
    end

    private

    def follow_authenticate_user!
      authenticate_user!
    end

    def save_relation(action)
      if @relation.save
        render json: { data: "#{action} successfully" }, status: :ok
      else
        render json: { data: 'Something went wrong' }, status: :unprocessable_entity
      end
    end

    def find_relation
      @relation = current_user.owned_relations.find_by(resource: @obj)
      case request.method_symbol
      when :put
        @relation ||= current_user.owned_relations.new(resource: @obj)
      when :delete
        raise ApiExceptions::ResourceNotFound.new(message: 'Relation does not exist') if @relation.nil?
      end
    end

    def relation_params
      params.permit(:resource_type, :resource_id)
    end
  end
end
