# frozen_string_literal: true

module Api
  module ExternalLinks
    extend ActiveSupport::Concern

    included do
      before_action :external_link_authenticate_user!, only: %i[create_link update_link destroy_link]
      before_action :external_link_is_admin!, only: %i[create_link update_link destroy_link]
      before_action :find_link, only: %i[update_link destroy_link]
    end

    # the params for this controller look like this:
    # params
    #
    # {
    #   link_id: 32,
    #   link: {
    #     url: "Slack",
    #     icon: File
    #   }
    # }

    def create_link
      link = ExternalLink.new(link_params)
      if params[:icon].present?
        link.icon.attach(params[:icon])
        render(json: { error: 'Something went wrong with the icon (Probably not an image!)' },
               status: :unprocessable_entity) && return unless link.icon.attached? && link.icon.variable?
      end
      if link.save!
        @obj.external_links << link
        render json: link, serializer: Api::ExternalLinkSerializer, status: :created
      else
        render json: { error: 'Something went wrong' }, status: :unprocessable_entity
      end
    end

    def index_link
      render json: @obj.external_links, each_serializer: Api::ExternalLinkSerializer, status: :ok
    end

    def update_link
      if params[:icon].present?
        @link.icon.purge
        @link.icon.attach(params[:icon])
        render(json: { error: 'Something went wrong with the icon (Probably not an image!)' },
               status: :unprocessable_entity) && return unless @link.icon.attached? && @link.icon.variable?
      end
      if @link.update(link_params)
        render json: @link, serializer: Api::ExternalLinkSerializer, status: :ok
      else
        render json: { error: 'Something went wrong with the update' }, status: :unprocessable_entity
      end
    end

    def destroy_link
      @obj.external_links.delete(@link)
      if @link.destroy
        render json: { data: 'Link destroyed' }, status: :ok
      else
        render json: { error: 'Something went wrong with the deletion' }, status: :unprocessable_entity
      end
    end

    private

    def external_link_authenticate_user!
      authenticate_user!
    end

    def external_link_is_admin!
      render json: { error: 'Only an admin can add edit or delete a link' }, status: :forbidden unless can_create_or_edit? && return
    end

    def can_create_or_edit?
      current_user.has_role?(:admin, @obj) || current_user == @obj
    end

    def link_params
      params.permit(:url, :icon, :name)
    end

    def find_link
      @link = ExternalLink.find(params[:link_id])
    end
  end
end
