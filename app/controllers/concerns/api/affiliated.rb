# frozen_string_literal: true

module Api
  module Affiliated
    extend ActiveSupport::Concern

    included do
      before_action :affiliated_authenticate_user!, only: [:affiliated_create]
      before_action :parent_admin?, only: [:affiliated_create]
      before_action :load_affiliate, only: [:affiliated_create]
    end

    # /:parent_type/:id/affiliated/:object_type
    def affiliated_index
      affiliated = ::Affiliation.where(parent: @obj, affiliate_type: params[:affiliate_type].classify).pluck(:affiliate_id, :status)
      response = affiliated.map do |affiliation|
        {
          affiliate_id: affiliation[0],
          status: affiliation[1]
        }
      end
      output = "{\"affiliates\": {\"#{params[:affiliate_type]}\": #{response.to_json}}}"
      render json: output, status: :ok
    end

    def affiliated_create
      render json: { success: true }, status: :ok and return if @obj.add_affiliate(@affiliate)

      render json: { error: 'Affiliated could not be created' }, status: :unprocessable_entity
    end

    def affiliated_stats
      affiliates = ::Affiliation.where(parent: @obj, status: 'accepted')
      # TODO: Fix the polymorphic error of this query
      # affiliates = Affiliation.where(parent: @obj, status:"accepted").joins(:affiliate).where.not(affiliate: {status: "draft"})
      results = affiliates.group(:affiliate_type).count
      results['total'] = affiliates.count
      render json: results, status: :ok
    end

    private

    def affiliated_authenticate_user!
      authenticate_user!
    end

    def parent_admin?
      render status: :forbidden and return unless current_user.has_role?(:admin, @obj)
    end

    def load_affiliate
      @affiliate = get_obj(params[:affiliate_type], params[:affiliate_id])

      render status: :not_found and return if @affiliate.nil?
    end

    def get_obj(type, id)
      klass = type.camelize.singularize.constantize
      klass.find(id)
    rescue StandardError
      nil
    end
  end
end
