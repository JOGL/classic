# frozen_string_literal: true

module Api
  module Faq
    extend ActiveSupport::Concern

    included do
      before_action :faq_authenticate_user!, only: %i[faq_create faq_update faq_destroy]
      before_action :faq_is_admin!, only: %i[faq_create faq_update faq_destroy]
      before_action :find_document, only: %i[faq_update faq_destroy faq_show]
    end

    def faq_index
      render json: @obj.faq, serializer: Api::FaqSerializer
    end

    def faq_show
      render json: @document, serializer: Api::DocumentSerializer
    end

    def faq_create
      @document = Document.new(faq_params)
      if @document.save
        @obj.faq.documents << @document
        render json: @obj.faq, serializer: Api::FaqSerializer, status: :created
      else
        render json: @document.errors, status: :unprocessable_entity
      end
    end

    def faq_update
      if @document.update(faq_params)
        render json: @obj.faq, serializer: Api::FaqSerializer
      else
        render json: @document.errors, status: :unprocessable_entity
      end
    end

    def faq_destroy
      if @document.destroy
        render json: @obj.faq, serializer: Api::FaqSerializer
      else
        render status: :unprocessable_entity
      end
    end

    private

    def find_document
      @document = Document.find_by(id: params[:document_id])
      render(json: { data: "Cannot find FAQ document with id: #{params[:document_id]}" }, status: :not_found) && return if @document.nil?
    end

    def faq_params
      params.require(:faq).permit(:title, :content, :position)
    end

    def faq_authenticate_user!
      authenticate_user!
    end

    def faq_is_admin!
      render json: { data: 'Only an admin can add update or remove documents from the FAQ' },
             status: :forbidden and return unless current_user.has_role? :admin,
                                                                         @obj
    end
  end
end
