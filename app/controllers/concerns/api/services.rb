# frozen_string_literal: true

module Api
  # Provides access to Service(s) and Activity(s) for Serviceable objects.
  module Services
    extend ActiveSupport::Concern

    included do
      before_action :serviceable_authenticate_user!, only: %i[index_service]
      before_action :is_admin, only: %i[index_service]
    end

    # Returns all activities for the given Serviceable object.
    def index_activity
      render json: @obj.activities
    end

    # Returns all services for the given Serviceable object.
    def index_service
      render json: @obj.services
    end

    private

    def serviceable_authenticate_user!
      authenticate_user!
    end
  end
end
