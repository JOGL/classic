# frozen_string_literal: true

module Api
  class ChallengesController < ApplicationController
    before_action :authenticate_user!, only: %i[create my_challenges can_create update destroy attach remove set_project_status]
    before_action :find_challenge, except: %i[index create my_challenges can_create short_title_exist get_id_from_short_title recommended]
    before_action :find_project, only: %i[attach remove]
    before_action :find_project_relation, only: [:set_project_status]
    before_action :set_obj, except: %i[index create my_challenges can_create short_title_exist get_id_from_short_title recommended]
    before_action :is_admin, only: %i[update invite upload update_member remove_member
                                      create_external_hook update_external_hook delete_external_hook get_external_hooks]

    # Including methods
    include Api::Affiliation
    include Api::ExternalLinks
    include Api::Faq
    include Api::Follow
    include Api::Hooks
    include Api::Media
    include Api::Members
    include Api::Recommendations
    include Api::Relations
    include Api::Upload
    include Api::Utils

    def index
      @pagy, @challenges = pagy(Challenge.where.not(status: 'draft').all)
      render json: @challenges
    end

    def my_challenges
      @challenges = Challenge.with_role(:owner, current_user)
      @challenges += Challenge.with_role(:admin, current_user)
      @challenges += Challenge.with_role(:member, current_user)
      @pagy, @challenges = pagy_array(@challenges.uniq)
      render json: @challenges
    end

    def can_create
      if can_access
        render json: { data: 'Authorized' }, status: :ok
      else
        render json: { data: 'Forbidden' }, status: :forbidden
      end
    end

    def create
      render(json: { data: 'Forbidden' }, status: :forbidden) && return unless can_access

      @challenge = Challenge.new(challenge_params)
      if @challenge.save
        current_user.add_edge(@challenge, 'is_author_of')
        update_skills_interests
        current_user.add_role :owner, @challenge
        current_user.add_role :admin, @challenge
        current_user.add_role :member, @challenge
        current_user.add_edge(@challenge, 'is_member_of')
        current_user.add_edge(@challenge, 'is_admin_of')
        current_user.add_edge(@challenge, 'is_owner_of')
        render json: @challenge, status: :created
      else
        render json: { data: 'Something went wrong' }, status: :unprocessable_entity
      end
    end

    def show
      render json: @challenge, show_objects: true
    end

    def update
      render(json: { data: 'You are not an admin of this challenge' }, status: :forbidden) && return unless current_user.has_role? :admin, @challenge

      if @challenge.update(challenge_params)
        update_skills_interests
        render json: { data: "Challenge id:#{@challenge.id} has been updated" }, status: :ok
      else
        render json: { data: 'Something went wrong' }, status: :unprocessable_entity
      end
    end

    # This method add project to a challenge and changes its status
    def attach
      if @challenge.projects.include?(@project)
        render json: { data: "Project id:#{@project.id} is already attached" }, status: :ok
      else
        @challenge.projects << @project
        @project.add_edge(@challenge, 'is_part_of')
        find_project_relation
        @relation.project_status = 'pending'
        @relation.save!
        hook_new_project_challenge(@challenge.externalhooks, @project)
        render json: { data: "Project id:#{@project.id} attached" }, status: :ok
      end
    end

    # The object relation is removed with no status change
    def remove
      if @challenge.projects.include?(@project)
        @challenge.projects.delete(@project)
        @project.remove_edge(@challenge, 'is_part_of')
        render json: { data: "Project id:#{@project.id} removed" }, status: :ok
      else
        render json: { data: "Project id:#{@project.id} is not attached" }, status: :not_found
      end
    end

    def index_projects
      # Don't take archived projects into account
      projects = Project
                 .joins(:challenges)
                 .distinct
                 .where(challenges: { id: @challenge.id })
                 .where.not(status: :archived)
                 .order(id: :desc) # can't use created_at since some records don't have it

      @pagy, records = pagy(projects)
      render json: Panko::Response.new(
        projects: Panko::ArraySerializer.new(
          records,
          each_serializer: Api::SharedProjectSerializer,
          context: { current_user: current_user, challenge_id: @challenge.id }
        )
      )
    end

    def index_needs
      # Only take accepted projects into account
      needs = Need
              .joins(project: :challenges_projects)
              .distinct
              .where('challenge_id = ?', @challenge.id)
              .where(challenges_projects: { project_status: ChallengesProject.project_statuses[:accepted] })
              .order(id: :desc) # can't use created_at since some records don't have it

      @pagy, records = pagy(needs)
      render json: Panko::Response.new(
        needs: Panko::ArraySerializer.new(
          records,
          each_serializer: Api::SharedNeedSerializer,
          context: { current_user: current_user }
        )
      )
    end

    def set_project_status
      render json: { data: "Status invalid. Valid statuses include: #{Project.statuses.keys}." } if params[:status] != 'accepted'

      @relation.accepted!
      @challenge.add_users_from_projects
      @challenge.program&.add_user_from_challenges

      render json: { data: 'Status updated' }, status: :ok
    end

    def destroy
      # NOTE: a challenge with 1 user is considered a test program, and can be destroyed
      if @challenge.users.count == 1 && current_user.has_role?(:owner, @challenge)
        @challenge.destroy
        current_user.remove_edge(@challenge, 'is_author_of')
        current_user.remove_edge(@challenge, 'is_member_of')
        current_user.remove_edge(@challenge, 'is_admin_of')
        current_user.remove_edge(@challenge, 'is_owner_of')
        render json: { data: "Challenge id:#{params[:id]} destroyed" }, status: :ok
      elsif current_user.has_role?(:owner, @challenge)
        @challenge.archived!
        render json: { data: "Challenge id:#{params[:id]} archived" }, status: :ok
      else
        render json: { data: 'You are not authorized to do that' }, status: :unauthorized
      end
    end

    private

    def can_access
      current_user.has_role?(:admin) || Program.with_role(:admin,
                                                          current_user).count.positive? || Space.with_role(:admin, current_user).count.positive?
    end

    def find_challenge
      @challenge = Challenge.find(params[:id])
      render json: { data: "Challenge id:#{params[:id]} was not found" }, status: :not_found if @challenge.nil?
    end

    def find_project
      @project = Project.find(params[:project_id])
      render json: { data: "Project id:#{params[:project_id]} was not found" }, status: :not_found if @project.nil?
    end

    def find_project_relation
      @relation = ChallengesProject.find_by(challenge_id: params[:id], project_id: params[:project_id])
      render json: { data: 'Challenge Project Relation not found' }, status: :not_found if @relation.nil?
    end

    def update_skills_interests
      @challenge.update_skills(params[:challenge][:skills]) unless params[:challenge][:skills].nil?
      @challenge.update_interests(params[:challenge][:interests]) unless params[:challenge][:interests].nil?
    end

    def set_obj
      @obj = @challenge
    end

    def challenge_params
      params.require(:challenge).permit(
        :address,
        :city,
        :country,
        :custom_type,
        :description_fr,
        :description,
        :end_date,
        :final_date,
        :launch_date,
        :program_id,
        :space_id,
        :rules_fr,
        :rules,
        :short_description_fr,
        :short_description,
        :short_title,
        :status,
        :title_fr,
        :title
      )
    end
  end
end
