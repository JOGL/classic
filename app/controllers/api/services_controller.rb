# frozen_string_literal: true

# Provides access to ServiceFlow(s) and ServiceFlowData.
module Api
  class ServicesController < ApplicationController
    before_action :authenticate_user!
    before_action :load_service!
    before_action :services_is_admin!
    before_action :load_service_flow!, only: %i[fetch_service_flow_data load_service_flow_data copy_service_flow_data]

    # Fetch the ServiceFlowData for the ServiceFlow.
    def fetch_service_flow_data
      render status: :bad_request and return if @service_flow.fetch_data(params[:opts]).nil?

      render status: :created
    end

    # Load the ServiceFlowData.data for the ServiceFlow.
    def load_service_flow_data
      data = @service_flow.load_data

      render status: :not_found and return if data.nil?

      render json: data, status: :ok
    end

    # Copy the ServiceFlowData for the ServiceFlow into some ActiveModel::Model.
    def copy_service_flow_data
      render status: :bad_request and return if @service_flow.copy_data(params[:opts]).nil?

      render status: :created
    end

    # Show the given Service.
    def show
      render json: @service
    end

    # Show all flows for the given Service.
    def index_flows
      render json: @service.flows
    end

    # Delete the activity.
    def delete_activity
      params.require(:activity_id)

      activity = Activity.find([@service.id, params[:activity_id]])

      render status: :not_found if activity.nil?

      activity.destroy
    end

    private

    # Load the Service given the :id.
    def load_service!
      @service = Service.find(params[:id])

      render status: :not_found if @service.nil?
    end

    # Return unauthorized if user is not the service creator or admin of the serviceable object.
    def services_is_admin!
      render status: :unauthorized if (current_user.id != @service.creator_id) && !current_user.has_role?(:admin, @service.serviceable)
    end

    # Load the ServiceFlow given the :type.
    def load_service_flow!
      @service_flow = ServiceFlowRegistry.load!(@service, params[:type].to_sym)
    rescue StandardError
      render status: :not_found and return
    end
  end
end
