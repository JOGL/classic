# frozen_string_literal: true

module Api
  class NotificationsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_notification, only: %i[read unread show]
    before_action :set_notifications, only: %i[index all_read all_unread]
    before_action :is_self, only: %i[read unread show]

    def index
      @pagy, @mynotifications = pagy(@notifications)
      render json: @mynotifications, status: :ok
    end

    def show
      render json: @notification, status: :ok
    end

    def read
      @notification.read = true
      @notification.save
      render json: { data: "Notification #{params[:id]} has been marked as read" }, status: :ok
    end

    def all_read
      @notifications.update_all(read: true)
      render json: { data: 'Notifications have been marked as read' }, status: :ok
    end

    def unread
      @notification.read = false
      @notification.save
      render json: { data: "Notification #{params[:id]} has marked as unread" }, status: :ok
    end

    def all_unread
      @notifications.update_all(read: false)
      render json: { data: "All notifications have been marked as 'unread'" }, status: :ok
    end

    def settings
      render json: { settings: current_user.settings }, status: :ok
    end

    def set_settings
      raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong') unless current_user.set_settings(params[:settings])

      render json: { data: 'Settings saved' }, status: :ok
    end

    # def subscribe
    #   @notification.read!
    #   render json: {data: "Notification " + params[:id].to_s + " has been read"}, status: :ok
    # end
    #
    # def unsubscribe
    #   @notification.unread!
    #   render json: {data: "Notification " + params[:id].to_s + " has been unread"}, status: :ok
    # end

    private

    def set_notification
      @notification = Notification.find_by(id: params[:id])
      render(json: { data: "Cannot find notification with id: #{params[:id]}" }, status: :not_found) && return if @notification.nil?
    end

    def set_notifications
      @notifications = current_user.notifications
    end

    def is_self
      render(json: { data: "You cannot look at someone else's notifications!" },
             status: :forbidden) && return unless current_user == @notification.target
    end
  end
end
