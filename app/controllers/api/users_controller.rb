# frozen_string_literal: true

module Api
  class UsersController < ApplicationController
    before_action :authenticate_user!, only: %i[update destroy send_private_email mutual validate_token]
    before_action :find_user, except: %i[index create destroy resend_confirmation
                                         nickname_exist recommended saved_objects validate_token]
    before_action :is_self?, only: %i[upload_avatar remove_avatar update]
    before_action :set_obj, except: %i[index create destroy resend_confirmation
                                       nickname_exist saved_objects recommended validate_token]
    before_action :is_valid?, only: [:update]

    include Api::ExternalLinks
    include Api::Follow
    include Api::Recommendations
    include Api::Relations
    include Api::Upload
    include Api::Utils
    include Api::Affiliation

    def index
      @pagy, @users = pagy(User.where(active_status: 'active').includes(%i[interests skills sash]).all)
      render json: @users
    end

    def create
      @user = User.where(email: create_user_params[:email])
      render(json: { error: 'Password confirmation does not match the password' },
             status: :unprocessable_entity) && return if create_user_params[:password] != create_user_params[:password_confirmation]

      if @user.blank?
        @user = User.new(create_user_params)
        @user.uid = @user.email
        @user.provider = 'email'
        if @user.save
          render json: { msg: 'Thank you for signing up, please confirm your email address to continue' }, status: :created
        else
          json_response(@user.errors)
        end
      else
        render json: { error: 'User already exists' }, status: :unprocessable_entity
      end
    end

    def show
      serializer = if current_user && current_user == @user
                     Api::UserSerializerWithPrivateFields
                   else
                     Api::UserSerializer
                   end
      render json: @user, serializer: serializer
    end

    def update
      current_user.active_at! unless current_user.nil?

      if @user.update(user_params)
        @user.update_skills(params[:user][:skills]) unless params[:user][:skills].nil?
        @user.update_ressources(params[:user][:ressources]) unless params[:user][:ressources].nil?
        @user.update_interests(params[:user][:interests]) unless params[:user][:interests].nil?
        render json: { data: 'User updated' }, status: :ok
      else
        render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
      end
    end

    def destroy
      if current_user.archived!
        current_user.roles.delete_all
        current_user.owned_relations.destroy_all
        json_response(current_user)
      end
    end

    def confirm_email
      @user = User.where(confirm_token: params[:token]).first
      if @user
        @user.validate_email
        redirect_to 'http://localhost:3000/newjogler' if @user.save && (ENV['RAILS_ENV'] == 'development')
      else
        render json: { data: 'Sorry. User does not exist' }, status: :not_found
      end
    end

    def projects
      users_projects = current_user.projects
      render json: users_projects, each_serializer: Api::ProjectSerializer
    end

    def user_object
      klass = params[:object_type].singularize.camelize.constantize
      serializer = 'Api::' + params[:object_type].singularize.camelize + 'Serializer'
      @results = klass.with_role(:owner, @user)
      @results += klass.with_role(:admin, @user)
      @results += klass.with_role(:member, @user)
      @results += klass.with_role(:reviewer, @user)
      render json: @results.uniq, each_serializer: serializer.constantize
    end

    def send_private_email
      if current_user.direct_message_limit_reached?
        render json: { data: 'Message limit reached' }, status: :forbidden
      elsif params[:object].nil? || params[:content].nil?
        render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
      else
        current_user.increment!(:direct_message_count)
        PrivateEmailWorker.perform_async(current_user.id, @user.id, params[:object], params[:content])
        render json: { data: 'Message sent' }, status: :ok
      end
    end

    def resend_confirmation
      unless user_params[:email]
        return render json: {
          success: false,
          errors: ['You must provide an email address.']
        }, status: 400
      end

      @user = User.find_by(email: user_params[:email])

      errors = nil

      if @user
        if @user.confirmed_at.present?
          errors = ['User already confirmed']
        else
          @user.resend_confirmation_instructions
        end
      else
        errors = ["Unable to find user with email '#{user_params[:email]}'."]
      end

      if errors
        render json: {
          success: false,
          errors: errors
        }, status: 400
      else
        render json: {
          status: 'success',
          data: @user.as_json
        }
      end
    end

    def mutual
      users = current_user.follow_mutual(@user)
      render json: users
    end

    def validate_token
      render json: current_user, status: :ok
    end

    private

    def find_user
      @user = User.find_by(id: params[:id])
      render json: { data: 'User not found' }, status: :not_found if @user.nil? || @user.deleted?
    end

    def set_obj
      @obj = @user
    end

    def is_self?
      render(json: { data: 'Only I can update my mind!' }, status: :forbidden) && return if current_user.id != params[:id].to_i
    end

    def is_valid?
      nickname = params[:user][:nickname]
      if nickname.nil?
        render(json: { data: 'You need to enter a nickname' }, status: :forbidden) && return
      else
        @other_user = User.find_by(nickname: nickname)
        render(json: { data: 'Nickname is already taken' }, status: :forbidden) && return if !@other_user.nil? && (@other_user.id != current_user.id)
      end
    end

    def user_params
      params.require(:user).permit(
        :address,
        :affiliation,
        :age,
        :bio,
        :birth_date,
        :can_contact,
        :category,
        :city,
        :country,
        :email,
        :first_name,
        :gender,
        :last_name,
        :mail_newsletter,
        :mail_weekly,
        :nickname,
        :other_user_id,
        :password_confirmation,
        :password,
        :phone_number,
        :short_bio,
        :status
      )
    end

    def create_user_params
      params.require(:user).permit(
        :birth_date,
        :gender,
        :email,
        :password,
        :password_confirmation,
        :first_name,
        :last_name,
        :nickname,
        :mail_newsletter,
        :mail_weekly
      )
    end
  end
end
