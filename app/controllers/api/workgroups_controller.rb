# frozen_string_literal: true

module Api
  class WorkgroupsController < ApplicationController
    # Before action validation are applied on different methods in order to
    # prevent events that are not destined to be executed, this also prevents
    # repeating the code on each method
    before_action :authenticate_user!, only: %i[create update destroy upload my_workgroups]
    before_action :find_workgroup, except: %i[create index my_workgroups short_title_exist get_id_from_short_title recommended]
    before_action :set_obj, except: %i[index create short_title_exist get_id_from_short_title recommended]
    before_action :is_admin, only: %i[update invite upload update_member remove_member]

    include Api::Follow
    include Api::Media
    include Api::Members
    include Api::Upload
    include Api::Relations
    include Api::Utils
    include Api::ExternalLinks
    include Api::Recommendations

    def index
      # take order param in the url, and order array depending on its value
      param = if params[:order] == 'desc'
                'id DESC'
              else
                'id ASC'
              end
      @pagy, @workgroups = pagy(Workgroup.where.not(status: 'archived').order(param).all)
      render json: @workgroups
    end

    def my_workgroups
      @workgroups = Workgroup.with_role(:owner, current_user)
      @workgroups += Workgroup.with_role(:admin, current_user)
      @workgroups += Workgroup.with_role(:member, current_user)
      @pagy, @workgroups = pagy_array(@workgroups.uniq)
      render json: @workgroups
    end

    # On creation the creator get all roles, skills and interests are added manually
    def create
      render(json: { data: 'ShortTitle is already taken' },
             status: :unprocessable_entity) && return unless Workgroup.where(short_title: params[:workgroup][:short_title]).empty?

      @workgroup = Workgroup.new(workgroup_params)
      @workgroup.creator_id = current_user.id
      @workgroup.status = 'active'
      if @workgroup.save
        current_user.add_edge(@workgroup, 'is_author_of')
        @workgroup.update_skills(params[:workgroup][:skills]) unless params[:workgroup][:skills].nil?
        @workgroup.update_ressources(params[:workgroup][:ressources]) unless params[:workgroup][:ressources].nil?
        @workgroup.update_interests(params[:workgroup][:interests]) unless params[:workgroup][:interests].nil?
        current_user.add_role :owner, @workgroup
        current_user.add_role :admin, @workgroup
        current_user.add_role :member, @workgroup
        current_user.add_edge(@workgroup, 'is_member_of')
        current_user.add_edge(@workgroup, 'is_admin_of')
        current_user.add_edge(@workgroup, 'is_owner_of')
        render json: { id: @workgroup.id, data: 'Success' }, status: :created
      end
    end

    def show
      render json: @workgroup, show_objects: true
    end

    def update
      unless params[:workgroup][:creator_id].nil?
        render json: { data: 'You cannot change the creator_id' },
               status: :forbidden if (@workgroup.creator_id != params[:workgroup][:creator_id]) && (@workgroup.creator_id != current_user.id)
      end
      if @workgroup.update(workgroup_params)
        @workgroup.update_skills(params[:workgroup][:skills]) unless params[:workgroup][:skills].nil?
        @workgroup.update_ressources(params[:workgroup][:ressources]) unless params[:workgroup][:ressources].nil?
        @workgroup.update_interests(params[:workgroup][:interests]) unless params[:workgroup][:interests].nil?
        json_response(@workgroup)
      else
        render json: { data: 'Something went wrong !' }, status: :unprocessable_entity
      end
    end

    def destroy
      # NOTE: a workgroup with 1 user is considered a test workgroup, and can be destroyed
      if @workgroup.users.count == 1 && current_user.has_role?(:owner, @workgroup)
        @workgroup.destroy
        current_user.remove_edge(@workgroup, 'is_author_of')
        render json: { data: 'Workgroup destroyed' }, status: :ok
      else
        @workgroup.archived!
        render json: { data: 'Workgroup archived' }, status: :ok
      end
    end

    private

    def find_workgroup
      @workgroup = Workgroup.find(params[:id])
      render json: { data: "Workgroup id:#{params[:id]} not found" }, status: :not_found if @workgroup.nil?
    end

    def set_obj
      @obj = @workgroup
    end

    def workgroup_params
      params.require(:workgroup)
            .permit(:title,
                    :short_title,
                    :description,
                    :short_description,
                    :country,
                    :city,
                    :address,
                    :is_private,
                    :creator_id,
                    :stranger_id,
                    :current_user_id,
                    :ressources,
                    project_ids: [])
    end
  end
end
