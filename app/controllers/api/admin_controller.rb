# frozen_string_literal: true

module Api
  class AdminController < ApplicationController
    before_action :authenticate_user!
    before_action :is_allowed!

    def users_with_skills
      params.require(:skills)

      users = User.joins(:skills).where(skills: { id: params[:skills] }).uniq

      respond_to do |format|
        format.json { render json: { users: users }, each_serializer: Api::UserSerializer, status: :ok }
        format.csv { send_data User.to_csv(users), filename: "users_with_skills-#{Date.today}.csv" }
      end
    end

    def email_users_with_skills
      params.require(%i[content object skills])

      user_ids = User.joins(:skills).where(skills: { id: params[:skills] }).uniq.pluck(:id)

      user_ids.each do |user_id|
        PrivateEmailWorker.perform_async(current_user.id, user_id, params[:object], params[:content])
      end

      render json: { message: 'Emailing started' }, status: :ok
    end

    private

    def is_allowed!
      render status: :forbidden and return unless current_user.has_role?(:admin) || Program.with_role(:admin, current_user).count.positive?
    end
  end
end
