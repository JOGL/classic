# frozen_string_literal: true

module Api
  class ProgramsController < ApplicationController
    # Before action validation are applied on different methods in order to
    # prevent events that are not destined to be executed, this also prevents
    # repeating the code on each method
    before_action :authenticate_user!, only: %i[create update destroy my_programs can_create]
    before_action :find_program, except: %i[index create my_programs get_id_from_short_title short_title_exist recommended can_create]
    before_action :find_challenge, only: %i[attach remove]
    before_action :set_obj, except: %i[index create destroy short_title_exist get_id_from_short_title recommended can_create]
    before_action :is_admin, only: %i[update invite upload_banner remove_banner
                                      update_member remove_member]

    include Api::Affiliation
    include Api::ExternalLinks
    include Api::Faq
    include Api::Follow
    include Api::Media
    include Api::Members
    include Api::Recommendations
    include Api::Relations
    include Api::Resources
    include Api::Upload
    include Api::Utils

    def index
      if params[:simple].nil?
        records = Program
                  .where.not(status: 'draft')
                  .includes([
                              :avatar_attachment,
                              { banner_attachment: :blob },
                              :feed,
                              { challenges: :projects }
                            ])
                  .all
        @pagy, @programs = pagy(records)
        render json: @programs
      else
        attrs = %w(id short_title title title_fr short_description short_description_fr status)
        records = Program
                  .where.not(status: 'draft')
                  .pluck(*attrs).map { |p| attrs.zip(p).to_h }
        render json: { programs: records }, status: :ok
      end
    end

    def my_programs
      @programs = Program.with_role(:owner, current_user)
      @programs += Program.with_role(:admin, current_user)
      @programs += Program.with_role(:member, current_user)
      @pagy, @programs = pagy_array(@programs.uniq)
      render json: @programs
    end

    def can_create
      if current_user.has_role? :program_creator
        render json: { data: 'Authorized' }, status: :ok
      else
        render json: { data: 'Forbidden' }, status: :forbidden
      end
    end

    def create
      render(json: { data: 'Forbidden' }, status: :forbidden) && return unless current_user.has_role? :program_creator

      @program = Program.new(program_params)
      @program.status = 'draft'
      if @program.save
        @program.create_faq
        current_user.add_role :owner, @program
        current_user.add_role :admin, @program
        current_user.add_role :member, @program
        render json: { id: @program.id, short_title: @program.short_title, data: 'Success' }, status: :created
      else
        render json: { data: 'Something went wrong' }, status: :unprocessable_entity
      end
    end

    def show
      render json: @program, includes: %i[projects], show_objects: true
    end

    def getid
      @program = Program.where(short_title: params[:nickname]).first
      render json: { id: @program.id, data: 'Success' }, status: :ok
    end

    def update
      if @program.update(program_params)
        render json: @program, status: :ok
      else
        render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
      end
    end

    def destroy
      # NOTE: a program with 1 user is considered a test program, and can be destroyed
      if @program.users.count == 1 && current_user.has_role?(:owner, @program)
        @program.destroy
        render json: { data: "Program id:#{@program.id} destroyed" }, status: :ok
      else
        @program.archived!
        render json: { data: "Program id:#{@program.id} archived" }, status: :ok
      end
    end

    # This method add challenge to a program and changes its status
    def attach
      if @program.challenges.include?(@challenge)
        render json: { data: "Challenge id:#{@challenge.id} is already attached" }, status: :ok
      else
        @program.challenges << @challenge
        render json: { data: "Challenge id:#{@challenge.id} attached" }, status: :ok
      end
    end

    # The object relation is remove no status change
    def remove
      if @program.challenges.include?(@challenge)
        @program.challenges.delete(@challenge)
        render json: { data: "Challenge id:#{@challenge.id} removed" }, status: :ok
      else
        render json: { data: "Challenge id:#{@challenge.id} is not attached" }, status: :not_found
      end
    end

    def index_challenges
      # render json: @program, serializer: Api::ProgramChallengeSerializer
      challenges = Challenge
                   .includes(%i[program])
                   .joins(:program)
                   .select('challenges.*')
                   .distinct
                   .where('programs.id = ?', @program.id)
                   .where.not(status: :archived)
                   .order(id: :desc)

      @pagy, records = pagy(challenges)
      render json: Panko::Response.new(
        challenges: Panko::ArraySerializer.new(
          records,
          each_serializer: Api::SharedChallengeSerializer,
          context: { current_user: current_user, program_id: @program.id }
        )
      )
    end

    def index_projects
      # Don't take archived projects into account
      projects = Project
                 .joins(challenges: :program)
                 .select('projects.*')
                 .distinct
                 .where('programs.id = ?', @program.id)
                 .where.not(status: :archived)
                 .order(id: :desc) # can't use created_at since some records don't have it

      @pagy, records = pagy(projects)
      render json: Panko::Response.new(
        projects: Panko::ArraySerializer.new(
          records,
          each_serializer: Api::SharedProjectSerializer,
          context: { current_user: current_user, program_id: @program.id }
        )
      )
    end

    def index_needs
      # Only take accepted projects into account
      needs = Need
              .includes(%i[relations])
              .joins(project: [{ challenges_projects: [{ challenge: :program }] }])
              .select('needs.*')
              .distinct
              .where('programs.id = ?', @program.id)
              .where(challenges_projects: { project_status: ChallengesProject.project_statuses[:accepted] })
              .order(id: :desc) # can't use created_at since some records don't have it

      @pagy, records = pagy(needs)
      render json: Panko::Response.new(
        needs: Panko::ArraySerializer.new(
          records,
          each_serializer: Api::SharedNeedSerializer,
          context: { current_user: current_user }
        )
      )
    end

    private

    def find_program
      @program = Program.find_by(id: params[:id])
      render json: { data: "Program id:#{params[:id]} not found" }, status: :not_found if @program.nil?
    end

    def find_challenge
      @challenge = Challenge.find_by(id: params[:challenge_id])
      render json: { data: "Challenge id:#{params[:challenge_id]} was not found" }, status: :not_found if @challenge.nil?
    end

    def set_obj
      @obj = @program
    end

    def program_params
      params.require(:program).permit(:title, :title_fr, :short_title, :short_title_fr, :short_description,
                                      :short_description_fr, :description, :description_fr, :status, :launch_date,
                                      :end_date, :enablers, :enablers_fr, :ressources, :contact_email,
                                      :meeting_information, :custom_challenge_name)
    end
  end
end
