# frozen_string_literal: true

module Api
  class ProjectsController < ApplicationController
    # Before action validation are applied on different methods in order to
    # prevent events that are not destined to be executed, this also prevents
    # repeating the code on each method
    before_action :authenticate_user!, only: %i[create update destroy my_projects]
    before_action :find_project, except: %i[index create my_projects short_title_exist get_id_from_short_title recommended]
    before_action :set_obj, except: %i[index create destroy short_title_exist get_id_from_short_title recommended]
    before_action :is_admin, only: %i[update invite upload_banner remove_banner update_member remove_member
                                      get_custom_data create_custom_field
                                      update_custom_field delete_custom_field
                                      create_external_hook update_external_hook delete_external_hook get_external_hooks]
    before_action :is_member, only: %i[create_custom_data update_custom_data delete_custom_data get_my_custom_datas]
    before_action :is_reviewer, only: %i[review]

    include Api::Affiliation
    include Api::Customfields
    include Api::ExternalLinks
    include Api::Follow
    include Api::Hooks
    include Api::Media
    include Api::Members
    include Api::Recommendations
    include Api::Relations
    include Api::Upload
    include Api::Utils

    # Before action for the relations Api::Relations

    def index
      param = params[:order] == 'desc' ? 'id DESC' : 'id ASC'
      @pagy, @projects = pagy(Project.where.not(status: 1).order(param).all)

      render json: @projects
    end

    def index_needs
      @pagy, @needs = pagy(@project.needs)

      render json: @needs, each_serializer: Api::NeedSerializer
    end

    def my_projects
      @projects = Project.with_role(:owner, current_user)
      @projects += Project.with_role(:admin, current_user)
      @projects += Project.with_role(:member, current_user)
      @pagy, @projects = pagy_array(@projects.uniq)

      render json: @projects
    end

    def create
      current_user.active_at! unless current_user.nil?

      if Project.where(short_title: params[:project][:short_title]).empty?
        @project = Project.new(project_params)
        @project.status = 'draft'
        @project.creator_id = current_user.id
        if @project.save
          @project.update_skills(params[:project][:skills]) unless params[:project][:skills].nil?
          @project.update_interests(params[:project][:interests]) unless params[:project][:interests].nil?
          current_user.add_role :owner, @project
          current_user.add_role :admin, @project
          current_user.add_role :member, @project
          current_user.add_edge(@project, 'is_author_of')
          current_user.add_edge(@project, 'is_member_of')
          current_user.add_edge(@project, 'is_admin_of')
          current_user.add_edge(@project, 'is_owner_of')

          render json: { id: @project.id, data: 'Success' }, status: :created
        else
          render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
        end
      else
        render json: { data: 'ShortTitle is already taken' }, status: :unprocessable_entity
      end
    end

    def show
      render json: @project, includes: %i[challenges], show_objects: true
    end

    def update
      if params[:project][:creator_id].present?
        if current_user.has_role? :owner, @project
          @project.roles.find_by(resource_type: @project.class.name, resource_id: @project.id, name: 'owner').delete
          @new_owner = User.find_by(id: params[:project][:creator_id])
          if @new_owner.nil?
            render json: { data: "New owner id:#{params[:project][:creator_id]} does not exist" }, status: :not_found
          else
            @new_owner.add_role :owner, @project
          end
        else
          render json: { data: 'You cannot change the creator_id' }, status: :forbidden
        end
      end
      if @project.update(project_params)
        @project.update_skills(params[:project][:skills]) unless params[:project][:skills].nil?
        @project.update_interests(params[:project][:interests]) unless params[:project][:interests].nil?
        render json: @project, status: :ok
      else
        render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
      end
    end

    def destroy
      # NOTE: a project with 1 user is considered a test project, and can be destroyed
      if @project.users.count == 1 && current_user.has_role?(:owner, @project)
        @project.destroy
        render json: { data: "Project id:#{params[:id]} destroyed" }, status: :ok
      else
        @project.archived!
        render json: { data: "Project id:#{@project.id} archived" }, status: :ok
      end
    end

    private

    def find_project
      @project = Project.find(params[:id])
      render json: { data: 'Project not found' }, status: :not_found if @project.nil?
    end

    def set_obj
      @obj = @project
    end

    def project_params
      params
        .require(:project)
        .permit(
          :address,
          :banner_url,
          :city,
          :country,
          :creator_id,
          :current_user_id,
          :desc_communication_strat,
          :desc_contributing,
          :desc_elevator_pitch,
          :desc_ethical_statement,
          :desc_funding,
          :desc_impact_strat,
          :desc_objectives,
          :desc_problem_statement,
          :desc_progress,
          :desc_stakeholder,
          :desc_state_art,
          :desc_sustainability_scalability,
          :description,
          :grant_info,
          :is_looking_for_collaborators,
          :is_private,
          :is_reviewed,
          :logo_url,
          :maturity,
          :short_description,
          :short_title,
          :status,
          :stranger_id,
          :title
        )
    end
  end
end
