# frozen_string_literal: true

module Api
  class SkillsController < ApplicationController
    # Before action validation are applied on different methods in order to
    # prevent events that are not destined to be executed, this also prevents
    # repeating the code on each method
    before_action :authenticate_user!, only: %i[create update destroy]

    def index
      render json: Skill.all
    end
  end
end
