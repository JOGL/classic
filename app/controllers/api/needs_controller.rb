# frozen_string_literal: true

module Api
  class NeedsController < ApplicationController
    # Before action validation are applied on different methods in order to
    # prevent events that are not destined to be executed, this also prevents
    # repeating the code on each method
    before_action :authenticate_user!, only: %i[create update destroy upload_document my_needs]
    before_action :find_need, except: %i[create index my_needs recommended]
    before_action :set_obj, except: %i[index create destroy recommended]
    before_action :can_create, only: :upload_document
    before_action :can_update, only: %i[update destroy]

    include Api::Relations
    include Api::Follow
    include Api::Media
    include Api::Members
    include Api::Utils
    include Api::Upload
    include Api::Hooks
    include Api::Recommendations

    def index
      param = params[:order] == 'desc' ? 'id DESC' : 'id ASC'

      @pagy, @needs = pagy(Need.includes(%i[project user]).order(param).all)

      render json: @needs, each_serializer: Api::NeedSerializer, status: :ok
    end

    def show
      render json: @need, includes: %i[project user], show_objects: true
    end

    def my_needs
      render json: Need.with_role(:member, current_user), each_serializer: Api::NeedSerializer
    end

    def create
      find_project
      can_create

      @need = Need.new(need_params)
      @need.user_id = current_user.id
      @need.status = :active
      if @need.save!
        @need.update_skills(params[:need][:skills]) unless params[:need][:skills].nil?
        @need.update_ressources(params[:need][:ressources]) unless params[:need][:ressources].nil?
        @project.needs << @need
        current_user.add_role :owner, @need
        current_user.add_role :admin, @project
        current_user.add_role :member, @need
        current_user.add_edge(@need, 'is_author_of')
        @project.add_edge(@need, 'has_need')
        # First let's render the JSON because the need went through
        render json: { id: @need.id, data: 'Your need has been published' }, status: :created
        hook_new_need(@project.externalhooks, @need)

      else
        render json: { data: 'Something went wrong' }, status: :unprocessable_entity
      end
    end

    def update
      if @need.update(need_params)
        @need.update_skills(params[:need][:skills]) unless params[:need][:skills].nil?
        @need.update_ressources(params[:need][:ressources]) unless params[:need][:ressources].nil?
        render json: { data: "Need id:#{@need.id} has been updated" }, status: :ok
      end
    end

    def destroy
      if @need.destroy
        current_user.remove_edge(@need, 'is_author_of')
        @need.project.remove_edge(@need, 'has_need')
        render json: { data: "Your need id:#{@need.id} has been deleted" }, status: :ok
      end
    end

    private

    def find_need
      @need = Need.find_by(id: params[:id])
      raise ApiExceptions::ResourceNotFound.new(message: 'Need not found') unless @need
    end

    def find_project
      project_id = params.dig(:need, :project_id)
      raise ApiExceptions::ResourceNotFound.new(message: 'Project not found. Did you specify a project_id?') unless project_id.present?

      @project = Project.find_by(id: project_id)
      raise ApiExceptions::ResourceNotFound.new(message: "Project with id #{project_id} not found.") unless @project
    end

    def can_create
      @project ||= Project.find_by(id: @need.project_id)
      raise ApiExceptions::ResourceNotFound.new(message: 'Project not found') unless @project
      raise ApiExceptions::NotResourceMemberError.new(message: "You must be a member of the project id: #{@project.id} to create a need") unless current_user.has_role?(
        :member, @project
      )
    end

    def can_update
      raise ApiExceptions::NeedAdminError unless current_user.has_role?(:owner, @need) || current_user.has_role?(:admin, @need.project)
    end

    def set_obj
      @obj = @need
    end

    def need_params
      params.require(:need).permit(
        :address,
        :city,
        :content,
        :country,
        :end_date,
        :is_urgent,
        :project_id,
        :ressources,
        :skills,
        :status,
        :title,
        :user_id
      )
    end
  end
end
