# frozen_string_literal: true

module Api
  class ActivitiesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_activity
    before_action :is_admin!

    def destroy
      @activity.destroy
    end

    private

    def set_activity
      params.require(%i[id])

      @activity = Activity.find(params[:id])
    end

    def is_admin!
      render status: :forbidden unless current_user.has_role?(:admin,
                                                              @activity.service) or current_user.has_role?(:admin, @activity.service.serviceable)
    end
  end
end
