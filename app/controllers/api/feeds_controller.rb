# frozen_string_literal: true

module Api
  class FeedsController < ApplicationController
    include Api::Utils

    before_action :find_feed, only: %i[show remove_post update]
    before_action :authenticate_user!, only: %i[index remove_post update]
    before_action :set_obj, only: %i[remove_post update]
    before_action :is_admin, only: %i[remove_post update]
    before_action :find_post, only: [:remove_post]

    def index
      _pagy, myfeed = pagy(current_user.timeline_posts)

      render json: myfeed, each_serializer: Api::PostSerializer, show_objects: true, status: :ok
    end

    def indexall
      param = params[:order] == 'desc' ? 'id DESC' : 'id ASC'

      @pagy, @joglfeed = pagy(Post.order(param).all)
      render json: @joglfeed, each_serializer: Api::PostSerializer, show_objects: true, status: :ok
    end

    def remove_post
      @feed.posts.delete(@post)

      render json: { data: "Post id:#{params[:post_id]} removed from feed" }, status: :ok
    end

    def show
      includes = %i[comments documents_attachments mentions]
      @pagy, @somefeed = pagy(@feed.posts.order('created_at DESC').includes(includes))

      render json: {
        allow_posting_to_all: @feed.allow_posting_to_all,
        posts: @somefeed.map { |post| Api::PostSerializer.new(post, show_objects: true) }
      }, status: :ok
    end

    def update
      @feed.update(params.permit(:allow_posting_to_all))

      render json: { data: 'Feed updated' }, status: :ok
    end

    private

    def find_feed
      @feed = Feed.find(params[:id])

      render json: { data: "Feed id:#{params[:id]} not found" }, status: :not_found if @feed.nil?
    end

    def find_post
      @post = Post.find(params[:post_id])

      render json: { data: "Post id:#{params[:post_id]} not found" }, status: :not_found if @post.nil?
    end

    def set_obj
      @obj = @feed.feedable
    end
  end
end
