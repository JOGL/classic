# frozen_string_literal: true

module Api
  class SpacesController < ApplicationController
    before_action :authenticate_user!, only: %i[create update destroy my_spaces can_create]
    before_action :find_space, except: %i[index create my_spaces get_id_from_short_title short_title_exist recommended can_create]
    before_action :set_obj, except: %i[index create destroy short_title_exist get_id_from_short_title recommended can_create]
    before_action :is_admin, only: %i[update invite upload_banner remove_banner
                                      update_member remove_member]

    serialization_scope :current_user

    include Api::Affiliated
    include Api::ExternalLinks
    include Api::Faq
    include Api::Follow
    include Api::Media
    include Api::Members
    include Api::Recommendations
    include Api::Relations
    include Api::Resources
    include Api::Services
    include Api::Upload
    include Api::Utils

    def index
      spaces = Space
               .where
               .not(status: 'draft')
               .includes(%i[avatar_attachment banner_attachment feed])
               .all

      @pagy, @spaces = pagy(spaces)
      render json: @spaces
    end

    def my_spaces
      @spaces = current_user.spaces
      @pagy, @spaces = pagy_array(@spaces.uniq)
      render json: @spaces
    end

    def can_create
      if current_user.has_role? :space_creator
        render json: { data: 'Authorized' }, status: :ok
      else
        render json: { data: 'Forbidden' }, status: :forbidden
      end
    end

    def create
      render(json: { data: 'Forbidden' }, status: :forbidden) && return unless current_user.has_role? :space_creator

      @space = Space.new(space_params)
      @space.status = 'draft'
      @space.creator_id = current_user.id
      @space.users << current_user

      if @space.save
        @space.create_faq
        current_user.add_role :owner, @space
        current_user.add_role :admin, @space
        current_user.add_role :member, @space
        render json: { id: @space.id, short_title: @space.short_title, data: 'Success' }, status: :created
      else
        render json: { data: 'Something went wrong' }, status: :unprocessable_entity
      end
    end

    def show
      render json: @space, show_objects: true
    end

    def getid
      @space = Space.where(short_title: params[:nickname]).first
      render json: { id: @space.id, data: 'Success' }, status: :ok
    end

    def update
      if @space.update(space_params)
        render json: @space, status: :ok
      else
        render json: { data: 'Something went wrong :(' }, status: :unprocessable_entity
      end
    end

    def destroy
      # NOTE: a space with 1 user is considered a test space, and can be destroyed
      if @space.users.count == 1 && current_user.has_role?(:owner, @space)
        @space.destroy
        render json: { data: "Space id:#{@space.id} destroyed" }, status: :ok
      else
        @space.archived!
        render json: { data: "Space id:#{@space.id} archived" }, status: :ok
      end
    end

    def index_challenges
      challenges = Challenge
                   .where(space: @space)
                   .order(id: :desc)

      @pagy, records = pagy(challenges)
      render json: records, each_serializer: Api::ChallengeSerializer, root: 'challenges', adapter: :json
    end

    # Return a list of Project(s) that are related to the Space.
    # Core - affiliated projects
    # Featureed - owned projects
    # Associated - projects owned by owned challenges
    def index_projects
      core = Project
             .select("projects.*, 'core' AS relation")
             .where(id: ::Affiliation.where(parent: @space, affiliate_type: 'Project', status: :accepted).select(:affiliate_id))

      featured = @space.projects.select("projects.*, 'featured' AS relation")

      if @space.show_associated_projects?
        associated = Project
                     .select("projects.*, 'associated' AS relation")
                     .joins(:challenges).where(challenges: { space: @space })
      else
        associated = []
      end

      projects = (core + featured + associated).uniq(&:id)

      pagy = Pagy.new(count: projects.count, items: params.key?(:items) ? params[:items] : 25, page: params.key?(:page) ? params[:page] : 1)

      render json: projects[pagy.offset, pagy.items],
             each_serializer: Api::ProjectSerializer,
             root: 'projects',
             adapter: :json
    end

    # Return a list of people (User) related to the Space.
    # Core - affiliated peopled
    # Members - owners, admins, members
    # Participates - members of space owned challenges or projects
    def index_people
      core = User
             .select("users.*, 'core' as relation")
             .where(id: ::Affiliation.where(parent: @space, affiliate_type: 'User', status: :accepted).select(:affiliate_id))

      member = User
               .select("users.*, 'member' as relation")
               .joins("LEFT JOIN users_roles ON users.id = users_roles.user_id")
               .joins("LEFT JOIN roles ON roles.id = users_roles.role_id")
               .where("roles.name != 'pending' AND roles.resource_type = 'Space' AND roles.resource_id = :space_id", space_id: @space.id)

      challenge_participant = User
                              .select("users.*, 'participant' as relation")
                              .joins("LEFT JOIN users_roles ON users.id = users_roles.user_id")
                              .joins("LEFT JOIN roles ON roles.id = users_roles.role_id")
                              .where("roles.name != 'pending' AND roles.resource_type = 'Challenge' AND roles.resource_id IN (SELECT id FROM challenges WHERE space_id = :space_id)", space_id: @space.id)

      project_participant = User
                            .select("users.*, 'participant' as relation")
                            .joins("LEFT JOIN users_roles ON users.id = users_roles.user_id")
                            .joins("LEFT JOIN roles ON roles.id = users_roles.role_id")
                            .where("roles.name != 'pending' AND roles.resource_type = 'Project' AND roles.resource_id IN (SELECT id FROM projects WHERE space_id = :space_id)", space_id: @space.id)

      if @space.show_associated_users?
        participant = challenge_participant + project_participant
      else
        participant = []
      end

      people = (core + member + participant).uniq(&:id)

      pagy = Pagy.new(count: people.count, items: params.key?(:items) ? params[:items] : 25, page: params.key?(:page) ? params[:page] : 1)

      render json: people[pagy.offset, pagy.items],
             each_serializer: Api::UserSerializer,
             root: 'people',
             adapter: :json
    end

    # Return a list of needs for the Space via its Project(s).
    # Omit any needs where the Project affiliation to the Space is still 'pending'.
    def index_needs
      needs = Need.where(project_id: ::Affiliation.where(parent: @space, affiliate_type: 'Project')
                  .where.not(status: 'pending')
                  .select(:affiliate_id))

      @pagy, records = pagy(needs)
      render json: records, each_serializer: Api::NeedSerializer, root: 'needs', adapter: :json
    end

    private

    def find_space
      @space = Space.find_by(id: params[:id])
      render json: { data: "Space id:#{params[:id]} not found" }, status: :not_found if @space.nil?
    end

    def set_obj
      @obj = @space
    end

    def space_params
      params
        .require(:space)
        .permit(
          :code_of_conduct,
          :contact_email,
          :custom_challenge_name,
          :description_fr,
          :description,
          :enablers_fr,
          :enablers,
          :end_date,
          :home_header,
          :home_info,
          :items,
          :launch_date,
          :meeting_information,
          :onboarding_steps,
          :page,
          :ressources,
          :short_description_fr,
          :short_description,
          :short_title_fr,
          :short_title,
          :show_associated_projects,
          :show_associated_users,
          :show_featured_challenges,
          :show_featured_needs,
          :show_featured_programs,
          :show_featured_projects,
          :space_type,
          :status,
          :title,
          :title_fr,
          project_ids: [],
          available_tabs: {},
          selected_tabs: {},
          show_featured: {}
        )
    end
  end
end
