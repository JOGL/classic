# frozen_string_literal: true

module Api
  class BoardsController < ApplicationController
    ALLOWED_PARENT_CLASSES = %w[program space]

    before_action :authenticate_user!, except: %i[show index]
    before_action :set_parent

    # GET /boards
    def index
      render json: @parent.boards
    end

    # GET /boards/1
    def show
      board = Board.find(params[:id])
      render json: board
    end

    # POST /boards
    def create
      raise ApiExceptions::AccessForbiddenError unless can_edit?

      board = Board.new(board_params)

      if board.save
        @parent.boards << board
        render json: board, status: :created
      else
        render json: board.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /boards/1
    def update
      raise ApiExceptions::AccessForbiddenError unless can_edit?

      board = Board.find(params[:id])

      if board.update(board_params)
        render json: board
      else
        render json: board.errors, status: :unprocessable_entity
      end
    end

    # DELETE /boards/1
    def destroy
      raise ApiExceptions::AccessForbiddenError unless can_edit?

      board = Board.find(params[:id])

      board.destroy
    end

    # POST /boards/1/users/34
    def add_user
      raise ApiExceptions::AccessForbiddenError unless can_edit?

      user = User.find(params[:user_id])
      board = Board.find(params[:id])
      board.users << user

      render json: board
    end

    # DELETE /boards/1/users/34
    def remove_user
      raise ApiExceptions::AccessForbiddenError unless can_edit?

      user = User.find(params[:user_id])
      board = Board.find(params[:id])
      board.users.delete(user)

      render json: board
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_parent
      klass = ALLOWED_PARENT_CLASSES.detect { |pk| params[:"#{pk}_id"].present? }
      return unless klass.present?

      @parent = klass.classify.constantize.find params[:"#{klass}_id"]
    end

    def can_edit?
      current_user.has_role?(:admin, @parent) || current_user.has_role?(:admin)
    end

    # Only allow a trusted parameter "white list" through.
    def board_params
      params.require(:board).permit(:title, :description)
    end
  end
end
