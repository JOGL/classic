# frozen_string_literal: true

module Api
  class AlgoliumController < ApplicationController
    before_action :authenticate_user!, only: [:create]

    def create
      if current_user.has_role? :jogl_admin
        UpdateObjectsAlgolia.perform_async
        render json: { data: 'The objects are updating' }, status: :ok
      else
        render json: { data: 'Forbidden' }, status: :forbidden
      end
    end
  end
end
