# frozen_string_literal: true

module Api
  class PostsController < ApplicationController
    # Before action validation are applied on different methods in order to
    # prevent events that are not destined to be executed, this also prevents
    # repeating the code on each method

    before_action :authenticate_user!, only: %i[create update destroy comment upload]
    before_action :find_post, except: %i[index create recommended]
    before_action :set_obj, except: %i[index create destroy comment recommended]
    before_action :is_allowed, only: %i[update destroy]

    include Api::Relations
    include Api::Upload
    include Api::Hooks
    include Api::Utils
    include Api::Recommendations

    def show
      render json: @post, includes: %i[relations], show_objects: true, status: :ok
    end

    def create
      current_user.active_at! unless current_user.nil?

      feed = Feed.find(params.dig(:post, :feed_id))
      from = feed.feedable
      can_post_to?(from)

      # NOTE: keep @post as instance variable for Merit gem
      @post = Post.new(post_params)
      @post.user_id = current_user.id
      @post.from = from

      @post.from_name = from.title
      @post.from_need_project_id = from.project.id if from.instance_of?(Need)

      raise ApiExceptions::UnprocessableEntity.new(message: 'Something went wrong') unless @post.save && !feed.nil? && !from.nil?

      current_user.add_edge(@post, 'is_author_of')
      current_user.add_edge(from, 'has_posted_on')
      @post.add_edge(from, 'is_post_of')
      feed.posts << @post

      if from.is_a?(User)
        from.notif_new_post_on_profile_feed
      else
        feed.notif_new_post_in_feed(@post, from)
        feed.notif_admins_about_new_post(@post)
      end

      # if posting to a need, we cross-post it to the need's project, apparently
      if from.is_a?(Need)
        from.project.feed.posts << @post
        from.project.feed.notif_new_post_in_feed(@post, from.project)
        from.project.feed.notif_admins_about_new_post(@post)
        hook_new_post(from.project.externalhooks, @post)
      elsif from.is_a?(Project) || from.is_a?(Challenge)
        hook_new_post(from.externalhooks, @post)
      end

      # First let's render the JSON because the post went through
      render json: { id: @post.id, data: 'Your post has been published' }, status: :created
      # Then we handle the different mentions "offline"
      save_mentions(params[:post][:mentions])
    end

    def update
      current_user.active_at! unless current_user.nil?

      render json: { data: 'Post has been updated' }, status: :ok if @post.update(post_params)
      # Now we handle the mentions
      save_mentions(params[:post][:mentions])
    end

    def destroy
      if @post.destroy
        current_user.remove_edge(@post, 'is_author_of')
        current_user.remove_edge(@post.from, 'has_posted_on')
        @post.remove_edge(@post.from, 'is_post_of')
        render json: { data: 'Your post has been deleted' }, status: :ok
      end
    end

    def comment
      case request.method_symbol
      when :post
        @comment = Comment.new(user_id: current_user.id,
                               post_id: @post.id,
                               content: params[:comment][:content])
        object = @post.from_object.constantize.find(@post.from_id)
        # change "workgroup" wording to "group"
        post_from_object_name = @post.from_object == 'Workgroup' ? 'group' : @post.from_object.downcase
        the_url = "post/#{@post.id}"
        # if @post.from_object == 'Need'
        # 	the_url = "project/" + @post.from_need_project_id + "#needs"
        # else
        # 	the_url = @post.from_object.downcase + '/' + @post.from_id.to_s + "#news"
        # end
        if @comment.save
          @post.comments << @comment
          current_user.add_edge(@post, 'has_commented_on')
          @comment.add_edge(@post, 'is_comment_of')
          current_user.add_edge(@comment, 'is_author_of')
          # First let's render the JSON because the post went through
          render json: { data: 'Your comment has been published' }, status: :created
          save_mentions(params[:comment][:mentions])
        end
      when :patch
        @comment = Comment.find(params[:comment_id])
        is_allowed @comment
        render json: { data: 'Cannot find comment id' } if @comment.nil?
        @comment.update(content: params[:comment][:content])
        render json: { data: 'Your post has been updated' }, status: :ok
        save_mentions(params[:comment][:mentions])
      when :delete
        @comment = Comment.find(params[:comment_id])
        is_allowed @comment
        @comment.destroy
        current_user.remove_edge(@post, 'has_commented_on')
        @comment.remove_edge(@post, 'is_comment_of')
        current_user.remove_edge(@comment, 'is_author_of')
        render json: { data: 'Comment deleted' }, status: :ok
      end
    end

    private

    def save_mentions(mentions)
      # Create a mention in the table
      # We check that the mention is valid, this means that it has a unique set of post and object mentions
      # Basically we do not store double mentions as double but just once
      # This also avoid retriggering !
      # The we do the whole logic for the mentions
      mentions&.map do |mention|
        # Create a mention in the table
        @mention = Mention.new(post_id: @post.id,
                               obj_type: mention[:obj_type].capitalize,
                               obj_id: mention[:obj_id],
                               obj_match: mention[:obj_match])
        # We check that the mention is valid, this means that it has a unique set of post and object mentions
        # Basically we do not store double mentions as double but just once
        # This also avoid retriggering !
        next unless @mention.valid?

        # The we do the whole logic for the mentions
        @object = get_object_mention(mention)
        unless @object.nil?
          @mention.save
          @post.mentions << @mention
          @mention.notif_new_mention(current_user)
          current_user.add_edge(@object, 'has_mentionned')
          @post.add_edge(@object, 'is_mentioning')
          @object.feed.posts << @post unless @object.feed.posts.exists?(@post.id)
        end
      end
    end

    def find_post
      @post = Post.find(params[:id])
      render json: { data: 'Post not found' }, status: :not_found if @post.nil?
    end

    def set_obj
      @obj = @post
    end

    def get_object_mention(mention)
      @object = mention[:obj_type].humanize.constantize.find(mention[:obj_id])
    end

    def post_params
      params.require(:post).permit(:user_id, :content, :media, :feed_id, :is_need)
    end

    def is_allowed(object = @post)
      return true if object.user_id == current_user.id

      render(json: { data: 'Forbidden' }, status: :forbidden) && return unless current_user.has_role?(:admin, @post.feed.feedable)
    end

    def can_post_to?(from)
      case from.class.name
      when 'Need'
        # Anyone can post to a need, no permissions necessary
      when 'User'
        raise ApiExceptions::AccessForbiddenError unless current_user.id == from.id
      when 'Program'
        raise ApiExceptions::AccessForbiddenError unless current_user.has_role?(:admin, from)
      when 'Challenge'
        raise ApiExceptions::AccessForbiddenError unless current_user.has_role?(:admin, from)
      else
        raise ApiExceptions::AccessForbiddenError unless current_user.has_role?(:member, from)
      end
    end
  end
end
