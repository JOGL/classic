# frozen_string_literal: true

# Extend devise omniauth.
class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  # Omniauth callback for [Eventbrite](http://eventbrite.com).
  # Adds an Eventbrite Service to the Serviceable `serviceable_type`, `serviceable_id` with the creator of `user_id`.
  def eventbrite
    auth = request.env['omniauth.auth']
    token = auth['credentials']['token']

    params = request.env['omniauth.params']

    redirect_link = params.key?('redirect_link') ? params['redirect_link'] : ''
    serviceable_id = params['serviceable_id']
    serviceable_type = params['serviceable_type']&.camelize
    user_id = params['user_id']

    render status: :not_found and return unless Serviceable.serviceable?(serviceable_type, serviceable_id, user_id)

    service = { creator_id: user_id, name: :eventbrite, serviceable_id: serviceable_id, serviceable_type: serviceable_type, token: token }
    Service.upsert(service, unique_by: :unique_serviceable_index)

    redirect_to Rails.configuration.frontend_url + redirect_link
  end

  def failure
    redirect_to Rails.configuration.frontend_url
  end
end
