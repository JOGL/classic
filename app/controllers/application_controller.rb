# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ExceptionHandler
  include Pagy::Backend
  include Response

  respond_to :json

  rescue_from ApiExceptions::BaseException, with: :render_api_error_response

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_paper_trail_whodunnit
  before_action :set_sentry_context

  after_action { pagy_headers_merge(@pagy) if @pagy }
  after_action :save_user_ip

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in)
  end

  def save_user_ip
    current_user.update(ip: request.remote_ip) if defined?(current_user) && current_user&.ip
  end

  def render_api_error_response(error)
    render json: error.as_json, status: error.status
  end

  private

  def set_sentry_context
    Sentry.set_user(id: current_user.id, email: current_user.email) if defined?(current_user) && current_user
    Sentry.set_extras(params: params.to_unsafe_h, url: request.url)
  end
end
