# frozen_string_literal: true
module ApiExceptions
  class UnprocessableEntity < ApiExceptions::BaseException
    def initialize(message: 'Cannot process request', status: :unprocessable_entity)
      @data = message
      @status = status
    end
  end
end
