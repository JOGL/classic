# frozen_string_literal: true

module ApiExceptions
  class NotResourceMemberError < ApiExceptions::BaseException
    def initialize(message: 'You must be a member of the resource to perform this action', status: :forbidden)
      @data = message
      @status = status
    end
  end
end
