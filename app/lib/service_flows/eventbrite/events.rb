# frozen_string_literal: true

require_relative '../../http'

module ServiceFlows
  module Eventbrite
    class Events < ServiceFlow
      @@base_uri = 'https://www.eventbriteapi.com/v3'

      def fetch_data(*)
        response = Http.json.get("#{@@base_uri}/users/me/organizations/", { token: @service.token })
        return nil if response.status != 200

        data = response.body['organizations']
                       .reduce({}) { |organizations, organization| fetch_events(organizations, organization) }

        service_flow_data = { service_id: @service.id, flow: @flow, data: data }
        ServiceFlowData.upsert(service_flow_data, returning: %w[data], unique_by: :unique_service_flow_data_index)
      end

      def copy_data(opts)
        return nil if opts.nil?

        ids = opts[:ids]

        service_flow_data = ServiceFlowData.find_by(service: @service, flow: @flow)
        return nil if service_flow_data.nil?

        activities = service_flow_data.data.values.flatten
                                      .select { |event| ids.include? event[:id] }
                                      .collect { |event| event_to_activity(event) }
        Activity.upsert_all(activities, returning: %w[service_id id], unique_by: %i[service_id id])
      end

      private

      def fetch_events(organizations, organization)
        id, name = organization.values_at('id', 'name')

        response = Http.json.get("#{@@base_uri}/organizations/#{id}/events/?expand=venue", { token: @service.token })
        return organizations if response.status != 200

        organizations[name] = response.body['events']
        organizations
      end

      def event_to_activity(event)
        activity = {
          service_id: @service.id,
          id: event[:id],
          category: :event,
          name: event.dig(:name, :text),
          description: event.dig(:description, :text),
          url: event[:url],
          logo: event.dig(:logo, :url),
          start: event.dig(:start, :utc),
          end: event.dig(:end, :utc)
        }

        activity.merge!(extract_location(event))
      end

      def extract_location(event)
        address = event.dig(:venue, :address)
        return {} if address.nil?

        address.slice(:latitude, :longitude).merge!(location: address[:localized_address_display])
      end
    end
  end
end
