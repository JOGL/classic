# frozen_string_literal: true

# @abstract Subclass and override {#fetch_data} and {#copy_data} to implement
#   a custom ServiceFlow.
#
# @!attribute [r] flow
#   @return [Symbol] the flow that fetches, loads, and copies data
# @!attribute [r] service
#   @return [Service] the service that provides information such as the token
class ServiceFlow
  attr_reader :flow, :service

  def initialize(service)
    @flow = self.class.name.demodulize.downcase.to_sym
    @service = service
  end

  # Fetch and store the ServiceFlowData associated with the service flow.
  # E.g., fetch events from Eventbrite and store them in service_flow_data.
  #
  # @return [ActiveRecord::Result, nil] the service flow data.
  def fetch_data(opts = nil)
    raise NotImplementedError
  end

  # Load the saved ServiceFlowData data associated with the service flow.
  #
  # @return [ServiceFlowData, nil] the service flow data.
  def load_data
    ServiceFlowData.find_by(service_id: @service.id, flow: flow)
  end

  # Copy a subset of the saved ServiceFlowData somewhere else in JOGL.
  # E.g., copy Eventbrite events from service_flow_data into Activities.
  #
  # @return [Boolean] whether or not the copy was successful.
  def copy_data(opts = nil)
    raise NotImplementedError
  end
end
