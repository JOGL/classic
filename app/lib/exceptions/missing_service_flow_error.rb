# frozen_string_literal: true

module Exceptions
  class MissingServiceFlowError < ArgumentError
  end
end
