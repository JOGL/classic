# frozen_string_literal: true

require 'exceptions/missing_service_flow_error'
require 'service_flows/eventbrite/events'

# A registry of ServiceFlow.
class ServiceFlowRegistry
  @service_flows = {
    eventbrite: {
      event: ServiceFlows::Eventbrite::Event,
      events: ServiceFlows::Eventbrite::Events
    }
  }

  # Return all service flows.
  #
  # @return [Hash] service flows
  class << self
    attr_reader :service_flows
  end

  # Return all service flows for the given Service.
  #
  # @param [Service] service the service to load flows for
  # @return [Hash] flows for the service
  def self.flows(service)
    @service_flows[service.name.to_sym]
  end

  # Load a ServiceFlow given the Service and flow.
  #
  # @param [Service] service the service to load a flow for
  # @param [Symbol] flow the flow to load
  # @return [ServiceFlow] an instantiated service flow
  def self.load!(service, flow)
    service_flow = @service_flows.dig(service.name.to_sym, flow)

    raise Exceptions::MissingServiceFlowError, "Service #{service.name} missing #{flow} flow" if service_flow.nil?

    service_flow.new(service)
  end
end
