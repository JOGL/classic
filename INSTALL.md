# Installation Instructions

Instructions to install JOGL's Backend repository on both Linux and MacOS operating systems.


## Ubuntu 20.10

The following installation process was drafted on an Ubuntu 20.10 computer but may be more widely
applicable to debian based linux distributions.

### Initial State

After cloning (or forking, in case of non added contributors) the repo on GitLab onto your computer, running `bundle install` produces the following error:

```bash
luis@shangrila:~/Code/jogl/backend-v0.1$ bundle install
Command 'bundle' not found, but can be installed with:
sudo snap install ruby          # version 2.7.2, or
sudo apt  install ruby-bundler  # version 2.1.4-2
See 'snap info ruby' for additional versions.
```

Meaning Ruby is not installed.  Let's install Ruby Version Manager so we are not stuck with whatever version
is available in the Ubuntu repos.

### Install RVM

RVM has it's own PPA for Ubuntu.  The [following instructions](https://github.com/rvm/ubuntu_rvm) show how to install it.
Unfortunately, the maintainer hasn't produced a build for Ubuntu 20.10.  Let's try a more general version manager, namely asdf!

### Install ASDF

The [ASDF site](https://asdf-vm.com/#/) has instructions for installing it.  


### Install Ruby
Once asdf is installed, you can install ruby 2.7.2
as follows.  Why do we need 2.7.2?  Hint: take a look at the `.ruby-version` file.  (Actually if you see another ruby version
in that file, then definitely replace 2.7.2 by that version below)  Don't worry if the last installation step takes a bit,
the actual ruby sources are compiled at this time.

```bash
sudo apt install -y build-essential libssl-dev zlib1g-dev
asdf plugin add ruby
asdf install ruby 2.7.2
```

### Install Ruby Gems

Now it's possible to install Ruby dependencies by running

```bash
sudo apt install libpq-dev
bundle install
```

### Install NVM

Unless you have already installed NodeJS or a version manager like NVM, then you need to install
a recent version of node.  Here NVM is installed although ASDF which we installed previously
can also install node.  The NVM site [has instructions](https://github.com/nvm-sh/nvm) on installing NVM itself.

### Install Node Dependencies

We currently use MJML to render email sent by the platform, so it is important to install node dependencies by running 

```bash
npm i
```

### Install Postgres

The JOGL backend uses a postgres database. To install postgres run

```bash
sudo apt install -y postgresql
```

### Create Postgres User

We need to create a user for running tests and for local development as follows.  When prompted for the password
of the `jogl` user you create below, please enter `devpassword`.

```bash
sudo -postgres createuser jogl --createdb --pwprompt --superuser 
```

Then type `exit` to get back to your previous session.

### Create the JOGL Databases

Before creating the JOGL databases, you need to add an entry to your `/etc/hosts` file because the database configuration
for the JOGL backend uses the `postgress` host.

```bash
sudo -s <<'EOF'
echo "127.0.0.1      postgress" >> /etc/hosts
EOF
```

Use the following to create the databases used in development and when running tests.

```bash
bundle exec rails db:create
bundle exec rails db:schema:load
bundle exec rails db:migrate
```

All set!


<br>

## macOS

Installation instructions for Mac Operating System.

### Initial state

After cloning (or forking, in case of non added contributors) the repo on GitLab onto your computer, running `bundle install` may produce errors linked to the lack of other previously necessary installations such as Ruby and/or Postgres.

### Install ASDF

ASDF is a language package manager that will prove useful to install Ruby. The [ASDF site](https://asdf-vm.com/#/) provides instructions for installing it, including using macOS package manager [Homebrew](https://brew.sh/).
   

### Install Ruby

Once asdf is installed, you can install ruby 2.7.2
as follows.  Why do we need 2.7.2?  Hint: take a look at the `.ruby-version` file.  (Actually if you see another ruby version
in that file, then definitely replace 2.7.2 by that version below). Don't worry if the last installation step takes a bit,
the actual ruby sources are compiled at this time.

```bash
asdf plugin add ruby
asdf install ruby 2.7.2
```

Check your version through
```bash
ruby -v
```

In case the version is still not correct, it could be because you need to activate asdf inside your current shell, which you can do either:
- by adding the *. /usr/local/opt/asdf/asdf.sh* line into your personal *.zprofile* file;
- or by running `source ~/.bash_profile` into your terminal.

### Install Ruby Gems

Now it should be possible to install Ruby dependencies by running

```bash
bundle install
```

You might still run into a few errors - the most common ones can be solved by:
 - installing libv8 via
 `install libv8 -v '3.16.14.13' -- --with-system-v8` as indicated [here](https://gist.github.com/fernandoaleman/868b64cd60ab2d51ab24e7bf384da1ca);
 - installing Postgres (see how below);
 - replace *therubyracer* by *mini_racer* on Gemfile text.

### Install NVM

Unless you have already installed NodeJS or a version manager like NVM, then you need to install
a recent version of node.  Here NVM is installed although ASDF which we installed previously
can also install node.  The NVM site [has instructions](https://github.com/nvm-sh/nvm) on installing NVM itself.

### Install Node Dependencies

We currently use MJML to render email sent by the platform, so it is important to install node dependencies by running 

```bash
npm i
```

### Install Postgres

The JOGL backend uses a postgres database. To install postgres and then start it, run

```bash
brew install postgresql
brew services start postgres
```

### Create Postgres User

We need to create a user for running tests and for local development as follows.  When prompted for the password
of the `jogl` user you create below, please enter `devpassword`.

```bash
sudo -u postgres createuser jogl --createdb -P --superuser
```

Then type `exit` to get back to your previous session.

*Possible issues while creating Postgres User*

In case your install didn't make a postgres user, it probably made a user name after your userspace, so try subbing that in instead: `-u yourusername`.

Generally, to make sure of the existence of certain roles and roles attributions, remember to run and explore `psql template1` .

If problems persist, also check your .env settings, where the following minimum variables need to be present:
*FRONTEND_URL=http://localhost:3000
BACKEND_URL=http://localhost:3001
RAILS_ENV=development
ALGOLIA_APP_ID=YOUR_ALGOLIA_APP_ID
ALGOLIA_API_TOKEN=YOUR_ALGOLIA_API_TOKEN*

### Create the JOGL Databases

Before creating the JOGL databases, you need to add an entry to your `/etc/hosts` file because the database configuration for the JOGL backend uses the `postgress` host (as you can check inside the `database.yml` file). To do so, run


```bash
sudo -s <<'EOF'
echo "127.0.0.1      postgress" >> /etc/hosts
EOF
```

You can check this was correctly done via `cat /etc/hosts`.

Finally, use the following commands to create the databases used in development and when running tests.

```bash
bundle exec rails db:create
bundle exec rails db:schema:load
bundle exec rails db:migrate
```
You are now all set up. Happy coding!
