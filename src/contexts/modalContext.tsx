import { Close } from '@emotion-icons/material/Close';
import { Router } from 'next/router';
import React, {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react';
import Modal from 'react-modal';
import { LayoutProps } from 'styled-system';
import Box from 'components/Box';
import P from 'components/primitives/P';
import styled from 'utils/styled';
import styles from './modalContext.module.scss';

interface ShowModal {
  children: ReactNode;
  title?: string;
  maxWidth?: LayoutProps['maxWidth'];
  showTitle?: boolean;
  showCloseButton?: boolean;
  allowOverflow?: boolean;
  modalClassName?: string;
}

export interface IModalContext {
  setModalChildren: Dispatch<SetStateAction<ReactNode>>;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  closeModal: (value?: boolean) => void;
  showModal: ({ children, maxWidth, title, showCloseButton, showTitle, modalClassName }: ShowModal) => void;
  isOpen: boolean;
}
// Required as mentioned in the docs.
Modal.setAppElement('#__next');
const ModalContext = createContext<IModalContext | undefined>(undefined);

export function ModalProvider({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const [modalChildren, setModalChildren] = useState<ReactNode>(
    <div>You haven\&apos;t provided any modalChildren</div>
  );
  const [contentLabel, setContentLabel] = useState(undefined);
  const [showTitle, setShowTitle] = useState<boolean>(true);
  const [allowOverflow, setAllowOverflow] = useState<boolean>(false);
  const [modalTitle, setModalTitle] = useState<undefined | string>(undefined);
  const [showCloseButton, setShowCloseButton] = useState<boolean>(true);
  const [maxWidth, setMaxWith] = useState<LayoutProps['maxWidth']>('30rem');
  const [modalClassName, setModalClassName] = useState<string>('');

  const showModal = useCallback(
    ({
      children,
      maxWidth = '30rem',
      title,
      showCloseButton = true,
      showTitle = true,
      allowOverflow = false,
      modalClassName,
    }: ShowModal) => {
      setModalChildren(children);
      setIsOpen(true);
      setModalTitle(title);
      setShowTitle(showTitle);
      setAllowOverflow(allowOverflow);
      setShowCloseButton(showCloseButton);
      setContentLabel(title);
      setMaxWith(maxWidth);
      setModalClassName(modalClassName);
    },
    []
  );
  const closeModal = (value: boolean = true) => setIsOpen(!value);
  // console.log(modalTitleValues);
  // Memoize value so it doesn't re-render the tree
  const value: IModalContext = useMemo(
    () => ({ showModal, setModalChildren, isOpen, closeModal }),
    [closeModal, showModal, setModalChildren, isOpen]
  );
  // force closing modal when soft changing route
  Router.events.on('routeChangeStart', closeModal);
  return (
    <ModalContext.Provider value={value}>
      <Modal
        isOpen={isOpen}
        contentLabel={contentLabel}
        onRequestClose={closeModal}
        className={styles.modal}
        overlayClassName={styles.overlay}
      >
        <Box maxWidth={maxWidth} width={['90vw', '80vw']} className={modalClassName}>
          {(showTitle || showCloseButton) && (
            <Box
              justifyContent="space-between"
              alignItems="center"
              row
              p={4}
              pr={2}
              borderBottom="1px solid lightgrey"
              tw="relative bg-primary"
            >
              <P
                flexGrow={1}
                color="white"
                fontWeight="500"
                fontSize={modalClassName === 'onBoardingModal' ? '2.3rem' : '1.3rem'}
                mb={0}
                tw="text-center"
              >
                {showTitle && modalTitle}
              </P>
              {showCloseButton && (
                <Box as="button" onClick={closeModal}>
                  {/* <CloseButton icon={['far', 'times-circle']} /> */}
                  <CloseButton size={modalClassName === 'onBoardingModal' ? 18 : 27} title="Close modal" />
                </Box>
              )}
              {modalClassName === 'createSpaceModal' && (
                <img
                  src="/images/banner-beta-feature-blue.png"
                  tw="hidden xs:block absolute top-0 left-0 w-1/4 md:w-[150px]"
                />
              )}
            </Box>
          )}
          {/* To be able to scroll in the modal content (not it's header), we need wrap modalChildren with a ModalFrame that has a set height and overflow-y:auto, and then by a div with overflow:scroll */}
          <ModalFrame overflowY={allowOverflow ? 'visible' : 'auto'}>
            {/* Can allow content to overflow for some modals */}
            <Box p={4} overflow={allowOverflow ? 'visible' : 'scroll'}>
              {modalChildren}
            </Box>
          </ModalFrame>
        </Box>
      </Modal>
      {children}
    </ModalContext.Provider>
  );
}

const CloseButton = styled(Close)`
  color: white;
  &:hover {
    color: lightgrey;
  }
`;

const ModalFrame = styled(Box)`
  max-height: calc(95vh - 10rem);
`;

export const useModal = () => useContext(ModalContext);
export default ModalContext;
