import { Times } from '@emotion-icons/fa-solid/Times';
import React, { createContext, Dispatch, SetStateAction, useCallback, useContext, useMemo, useState } from 'react';
import Modal from 'react-modal';
import { LayoutProps } from 'styled-system';
import Box from 'components/Box';
import P from 'components/primitives/P';
import { NotLoggedInModal } from 'utils/getApi';
import styled from 'utils/styled';
import styles from './modalContext.module.scss';

interface ShowModal {
  title: string;
  maxWidth?: LayoutProps['maxWidth'];
  showTitle?: boolean;
  showCloseButton?: boolean;
}

export interface INotLoggedInModalContext {
  setIsOpen: Dispatch<SetStateAction<boolean>>;
  closeModal: (value?: boolean) => void;
  showModal: ({ maxWidth, title, showCloseButton, showTitle }: ShowModal) => void;
  isOpen: boolean;
}
// Required as mentioned in the docs.
Modal.setAppElement('#__next');
const NotLoggedInModalContext = createContext<INotLoggedInModalContext | undefined>(undefined);

export function NotLoggedInModalProvider({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const [contentLabel, setContentLabel] = useState(undefined);
  const [showTitle, setShowTitle] = useState<boolean>(true);
  const [modalTitle, setModalTitle] = useState<undefined | string>(undefined);
  const [showCloseButton, setShowCloseButton] = useState<boolean>(true);
  const [maxWidth, setMaxWith] = useState<LayoutProps['maxWidth']>('30rem');

  const showModal = useCallback(({ maxWidth, title, showCloseButton = true, showTitle = true }: ShowModal) => {
    setIsOpen(true);
    setModalTitle(title);
    setShowTitle(showTitle);
    setShowCloseButton(showCloseButton);
    setContentLabel(title);
    setMaxWith(maxWidth);
  }, []);
  const closeModal = (value: boolean = true) => setIsOpen(!value);
  // Memoize value so it doesn't re-render the tree
  const value: INotLoggedInModalContext = useMemo(
    () => ({ showModal, isOpen, closeModal }),
    [closeModal, showModal, isOpen]
  );

  return (
    <NotLoggedInModalContext.Provider value={value}>
      <Modal
        isOpen={isOpen}
        contentLabel={contentLabel}
        onRequestClose={closeModal}
        className={styles.modal}
        overlayClassName={styles.overlay}
      >
        <Box maxWidth={maxWidth} width="80vw">
          {(showTitle || showCloseButton) && (
            <Box justifyContent="space-between" alignItems="center" row p={4} pr={1} borderBottom="1px solid lightgrey">
              <P flexGrow={1} color="#212529" fontWeight="500" fontSize={'1.3rem'} mb={0}>
                {showTitle && modalTitle}
              </P>
              {showCloseButton && (
                <Box as="button" onClick={closeModal}>
                  <CloseButton size={20} title="Close modal" />
                </Box>
              )}
            </Box>
          )}
          <Box p={4}>
            <NotLoggedInModal hideModal={() => closeModal()} />
          </Box>
        </Box>
      </Modal>
      {children}
    </NotLoggedInModalContext.Provider>
  );
}

const CloseButton = styled(Times)`
  color: #868686;
  &:hover {
    color: #212529;
  }
`;

export const useNotLoggedInModal = () => useContext(NotLoggedInModalContext);
export default NotLoggedInModalContext;
