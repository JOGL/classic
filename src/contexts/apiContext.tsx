import { AxiosInstance } from 'axios';
import React, { FC, useContext, useMemo } from 'react';
import { Credentials } from 'types';
import getApi from 'utils/getApi';
import { useNotLoggedInModal } from './notLoggedInModalContext';
import useTranslation from 'next-translate/useTranslation';

type ApiContextType = AxiosInstance;
export const ApiContext = React.createContext<ApiContextType | undefined>(
  undefined // default value
);
// The credentials cookies come from the next-cookies in _app.js
interface Props {
  credentials: Credentials;
}
export const ApiProvider: FC<Props> = ({ children, credentials }) => {
  const { authorization, userId } = credentials;
  const notLoggedInModal = useNotLoggedInModal();
  const { t } = useTranslation('common');

  const api = useMemo(
    () => getApi({ authorization, userId, t }, notLoggedInModal),
    [authorization, notLoggedInModal, userId]
  );

  return <ApiContext.Provider value={api}>{children}</ApiContext.Provider>;
};
// HOC to use it in legacy class component code.
export function withApi(Component) {
  return function ApiComponent(props) {
    return <ApiContext.Consumer>{(api) => <Component {...props} api={api} />}</ApiContext.Consumer>;
  };
}

// Custom hook to use it fast and clean.
// This is the preferred way of using it.
export const useApi = () => useContext(ApiContext);
