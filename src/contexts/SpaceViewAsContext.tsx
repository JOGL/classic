import React, { useContext, useState } from 'react';

/**
 * ViewAsMode for a given Space. If you are the space owner you may
 * switch between owner, member and public views.
 */
export type ViewAsMode = 'owner' | 'member' | 'public';

/**
 * ViewAsContext is the type of a React Context that may be used to switch between
 * different ViewAsModes.
 */
interface ViewAsContext {
  viewAsMode: ViewAsMode;
  setViewAsMode?: (mode: ViewAsMode) => void;
}

/**
 * ViewAsContext is a React Context that may be used to switch between
 * different ViewAsModes.
 */
const ViewAsContext = React.createContext<ViewAsContext>({ viewAsMode: 'owner' });

/**
 * useSpaceViewAsContext may be used in a space's subcomponents to display
 * different views.
 */
export const useSpaceViewAsContext = () => useContext(ViewAsContext);

/**
 * Provides the space view as context for components displayed as part
 * of a space.
 * @param children Children of this provider
 */
export const SpaceViewAsContextProvider: React.FC = ({ children }) => {
  const [mode, setMode] = useState<ViewAsMode>('owner');
  return (
    <ViewAsContext.Provider value={{ viewAsMode: mode, setViewAsMode: setMode }}>{children}</ViewAsContext.Provider>
  );
};
