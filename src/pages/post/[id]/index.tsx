import { NextPage } from 'next';
import React from 'react';
import Box from 'components/Box';
import PostDisplay from 'components/Feed/Posts/PostDisplay';
import Layout from 'components/Layout';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import { Post } from 'types';
import { getApiFromCtx } from 'utils/getApi';

interface Props {
  post: Post;
}
const PostSingleDisplay: NextPage<Props> = ({ post: postProp }) => {
  const { data: post } = useGet('/api/posts/' + postProp.id, { initialData: postProp });
  const { userData } = useUserData();
  return (
    <Layout
      title={`Post from ${post.from.object_type} ${post.from.object_name} | JOGL`}
      desc={`${post.creator.first_name} ${post.creator.last_name}: ${post?.content}`}
    >
      {post && (
        <Box px={2} maxWidth="800px" width="100%" margin="auto">
          <PostDisplay post={post} user={userData} isSingle />
        </Box>
      )}
    </Layout>
  );
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/posts/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { props: { post: res.data } };
  return { redirect: { destination: '/404', permanent: false } };
}

export default PostSingleDisplay;
