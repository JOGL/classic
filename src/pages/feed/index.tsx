import useTranslation from 'next-translate/useTranslation';
import Box from 'components/Box';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
// import useUserData from 'hooks/useUserData';

const GeneralFeed = () => {
  // const { userData } = useUserData();
  const { t } = useTranslation('common');
  return (
    <Layout>
      <Box alignItems="center">
        <h3 style={{ marginBottom: '20px' }}>{t('feed.all')}</h3>
        {/* NOTE: set allowPosting to false, cause new post don't appear when we post in this page/feed */}
        {/* <Feed feedId="all" allowPosting={!!userData} isAdmin={false} /> */}
        <Feed feedId="all" allowPosting={false} isAdmin={false} />
      </Box>
    </Layout>
  );
};
export default GeneralFeed;
