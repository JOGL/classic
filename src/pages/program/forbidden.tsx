import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React from 'react';
import Layout from 'components/Layout';

const ProgramForbiddenPage: NextPage = () => {
  const { t } = useTranslation('common');

  return (
    <Layout>
      <div className="container-fluid ProgramPage">
        <div className="row">
          <div className="col-12 text-center">
            <br />
            <br />
            {t('program.info.forbidden')}
            <br />
            <a href="mailto:hello@jogl.io">hello@jogl.io</a>
            <br />
            <br />
            <br />
            <Link href="/" as="/">
              <a>
                <button className="btn btn-primary" type="button">
                  {t('program.info.forbiddenBtn')}
                </button>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default ProgramForbiddenPage;
