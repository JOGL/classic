import { BookOpen, Envelope, QuestionCircle } from '@emotion-icons/fa-solid';
import { ChevronsLeft, ChevronsRight } from '@emotion-icons/boxicons-regular';
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { flexbox, layout, space, typography } from 'styled-system';
import Box from 'components/Box';
import Feed from 'components/Feed/Feed';
import PostDisplayMinimal from 'components/Feed/Posts/PostDisplayMinimal';
import Grid from 'components/Grid';
import Layout from 'components/Layout';
import A from 'components/primitives/A';
import Button from 'components/primitives/Button';
import H1 from 'components/primitives/H1';
import H2 from 'components/primitives/H2';
import P from 'components/primitives/P';
import ProgramAbout from 'components/Program/ProgramAbout';
import ProgramFaq from 'components/Program/ProgramFaq';
import ProgramHeader from 'components/Program/ProgramHeader';
// import ProgramHome from 'components/Program/ProgramHome';
import ProgramMembers from 'components/Program/ProgramMembers';
import ProgramNeeds from 'components/Program/ProgramNeeds';
import ProgramProjects from 'components/Program/ProgramProjects';
import ProgramResources from 'components/Program/ProgramResources';
import BtnFollow from 'components/Tools/BtnFollow';
import BtnStar from 'components/Tools/BtnStar';
import { ContactForm } from 'components/Tools/ContactForm';
import InfoHtmlComponent from 'components/Tools/Info/InfoHtmlComponent';
import Loading from 'components/Tools/Loading';
import NoResults from 'components/Tools/NoResults';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import useMembers from 'hooks/useMembers';
import useUserData from 'hooks/useUserData';
import { Challenge, Faq, Program } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import styled from 'utils/styled';
import { useScrollHandler } from 'utils/utils';
import InviteMember from 'components/Tools/InviteMember';
import ChallengeCard from 'components/Challenge/ChallengeCard';
import ReactGA from 'react-ga';
import ReactTooltip from 'react-tooltip';
import Image2 from 'components/Image2';
import tw from 'twin.macro';
import BtnJoin from 'components/Tools/BtnJoin';
// import Image from 'next/image';

import 'intro.js/introjs.css';
import { startProgramIntro } from 'utils/onboarding/index';

const DEFAULT_TAB = 'about';
const ProgramDetails: NextPage<{ program: Program }> = ({ program }) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const { members } = useMembers('programs', program?.id, 25); // take 25 first members
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const [offSetTop, setOffSetTop] = useState();
  const customChalName = program.custom_challenge_name;
  const [tabs, setTabs] = useState([
    // { value: 'home', translationId: 'program.home.title' },
    { value: 'about', translationId: 'general.tab.about' },
    { value: 'news', translationId: 'entity.tab.news' },
    { value: 'challenges', translationId: 'general.challenges' },
    { value: 'projects', translationId: 'entity.tab.projects' },
    { value: 'needs', translationId: 'entity.tab.needs' },
    { value: 'members', translationId: 'entity.card.members' },
    { value: 'resources', translationId: 'program.tab.resources' },
  ]);
  const isOnboarding = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOnboarding'));
  const isAnIndividual = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isIndividual'));
  const isDesktop = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isDesktop'));
  const [isCollapsed, setIsCollapsed] = useState(true);
  const { data: dataPosts } = useGet(`/api/feeds/${program?.feed_id}?items=5&page=1`);
  const { data: dataChallenges } = useGet<{ challenges: Challenge }>(`/api/programs/${program?.id}/challenges`);
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/programs/${program?.id}/links`);
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/programs/${program.id}/faq`);
  const { userData } = useUserData();
  const headerRef = useRef();
  const navRef = useRef();
  const isSticky = useScrollHandler(offSetTop);
  useEffect(() => {
    setOffSetTop(headerRef?.current?.offsetTop);
  }, [headerRef]);

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // add faq tab only if has faq
  useEffect(() => {
    tabs[7]?.value !== 'faq' &&
      faqList &&
      faqList.documents.length !== 0 &&
      setTabs([...tabs, { value: 'faq', translationId: 'entity.info.faq' }]); // don't add another time if tab already exists
  }, [faqList]);

  const contactEmail = program?.contact_email || 'hello@jogl.io';

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/program/${router.query.short_title}`, undefined, {
          shallow: true,
        });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const sendMessageToAdmin = (id, first_name, last_name) => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={closeModal} />,
      title: t('user.contactModal.title', {
        userFullName: `${first_name} ${last_name}`,
      }),
    });
  };

  // [Onboarding][5] - Retaking onboarding flow on program
  useEffect(() => {
    if (isOnboarding && isAnIndividual) {
      setTimeout(() => {
        startProgramIntro(t, router, isDesktop);
      }, 2000);
    }
  }, [isOnboarding, isAnIndividual]);

  return (
    <Layout
      title={program?.title && `${program.title} | JOGL`}
      desc={program?.short_description}
      img={program?.banner_url || '/images/default/default-program.jpg'}
      className="small-top-margin"
      noIndex={program?.status === 'draft'}
    >
      <div tw="w-full max-height[410px] overflow-hidden justify-center">
        <Img
          src={program?.banner_url || '/images/default/default-program.jpg'}
          alt={`${program?.title} banner`}
          priority
        />
      </div>
      <TWGrid>
        <div tw="flex mt-0 flex-wrap md:-mt-24 lg:(-mt-28 items-center)" ref={headerRef}>
          <ProgramHeader program={program} lang={locale} />
          <div tw="flex flex-wrap gap-x-2 gap-y-2 mt-6 mb-4 self-center w-full justify-center md:(mb-0 mt-4)">
            {!program?.is_owner && ( // show join button only if we are not owner of challenge
              <BtnJoin
                joinState={program.is_member}
                itemType="programs"
                itemId={program.id}
                count={program.members_count}
                showMembersModal={() =>
                  router.push(`/program/${router.query.short_title}?tab=members`, undefined, { shallow: true })
                }
              />
            )}
            <BtnFollow
              followState={program.has_followed}
              itemType="programs"
              itemId={program.id}
              count={program.followers_count}
            />
            <BtnStar
              itemType="programs"
              itemId={program.id}
              hasStarred={program.has_saved}
              count={program.saves_count}
            />
          </div>
        </div>
        <Box pl={[0, undefined, 6]} py={3} alignSelf="center">
          <Text>{(locale === 'fr' && program?.short_description_fr) || program?.short_description}</Text>
        </Box>
      </TWGrid>
      <StickyHeading isSticky={isSticky} className="stickyHeading" bg="white" justifyContent="center">
        <Box row alignItems="center" px={[4, undefined, undefined, undefined, 0]}>
          <LogoImg src={program?.logo_url || 'images/jogl-logo.png'} mr={4} alt="program logo" />
          <H1 fontSize={['1.8rem', '2.18rem']} lineHeight="26px">
            {(locale === 'fr' && program?.title_fr) || program?.title}
          </H1>
          <Box row pl={5} className="actions">
            <ShareBtns type="program" specialObjId={program?.id} />
          </Box>
        </Box>
      </StickyHeading>

      {/* ---- Page grid starts here ---- */}
      <div
        css={[
          tw`bg-lightBlue pb-6 grid grid-cols-1 md:(bg-white grid-template-columns[12rem calc(100% - 12rem)])`,
          isCollapsed ? tw`xl:grid-template-columns[15% 85%]` : tw`xl:grid-template-columns[15% 60% 25%]`,
        ]}
        ref={navRef}
      >
        {/* Header and nav (left col on desktop, top col on mobile */}
        <Box
          alignItems={[undefined, undefined, 'center']}
          bg="white"
          position="sticky"
          top={['125px', undefined, '50px']}
          zIndex={8}
        >
          <NavContainer bg="white">
            {/* quick actions buttons */}
            {/* <Box row justifyContent="center" spaceX={6} mb={4}>
              <Link shallow scroll={false} href={`/program/${program.short_title}?tab=projects`} passHref>
                <QuickAction as="button">
                  <QuickActionIcon>
                    <P mb={0} color={oldTheme.colors.greys['600']} fontSize={8}>
                      +
                    </P>
                  </QuickActionIcon>
                  {t('program.getStarted')}
                </QuickAction>
              </Link>
              <Link shallow scroll={false} href={`/program/${program.short_title}?tab=about`} passHref>
                <QuickAction as="button">
                  <QuickActionIcon fontSize={2}>
                    <Eye size={18} color={oldTheme.colors.greys['600']} title="Learn more" />
                  </QuickActionIcon>
                  {t('program.learnMore')}
                </QuickAction>
              </Link>
            </Box> */}
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="left" />
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="right" />
            <Nav
              as="nav"
              id="nav"
              flexDirection={['row', undefined, 'column']}
              spaceX={[3, undefined, 0]}
              pt={{ _: '10px', md: '0' }}
              width="100%"
              bg="white"
            >
              {tabs.map((item, index) => (
                <Link
                  shallow
                  scroll={false}
                  key={index}
                  href={`/program/${router.query.short_title}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                >
                  <Tab
                    selected={item.value === selectedTab}
                    px={[1, undefined, 3]}
                    py={3}
                    as="button"
                    id={`${item.value}-tab`}
                  >
                    {/* If tab is challenge, and space has a custom chal name set, show this name for the tab instead of "Challenges" */}
                    {item.value === 'challenges' && customChalName ? customChalName : t(item.translationId)}
                  </Tab>
                </Link>
              ))}
            </Nav>
          </NavContainer>
        </Box>
        {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
        <Box px={[0, undefined, 4]} tw="relative">
          {/* Toggle button to open/close third column side drawer*/}
          <button
            tw="hidden xl:(block bg-gray-100 rounded w-max absolute right-4)"
            onClick={() => setIsCollapsed(!isCollapsed)}
          >
            {isCollapsed ? (
              <ChevronsLeft size={25} title="un-collapse" />
            ) : (
              <ChevronsRight size={25} title="collapse" />
            )}
          </button>
          {/* {selectedTab === 'home' && (
            <TabPanel id="home" px={[3, 4, undefined, 0]} pt={[5, undefined, 0]}>
              <DesktopBorders>
                <ProgramHome
                  shortDescription={(locale === 'fr' && program?.short_description_fr) || program?.short_description}
                  programId={program.id}
                  programFeedId={program.feed_id}
                  meetingInfo={program.meeting_information}
                  isAdmin={program.is_admin}
                  posts={dataPosts}
                  challenges={dataChallenges?.challenges}
                  customChalName={customChalName}
                />
              </DesktopBorders>
            </TabPanel>
          )} */}
          {selectedTab === 'about' && (
            <TabPanel id="about" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <ProgramAbout
                  programId={program?.id}
                  enablers={program.enablers}
                  description={(locale === 'fr' && program?.description_fr) || program?.description}
                  launchDate={program?.launch_date}
                  status={program?.status}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'news' && (
            <TabPanel id="news" style={{ margin: '0 auto' }} width="100%" pt={[2, undefined, 0]}>
              {/* Show feed, and pass DisplayCreate to admins */}
              {program.feed_id && (
                <Feed
                  feedId={program.feed_id}
                  allowPosting={program.is_member}
                  isAdmin={program.is_admin || program.is_owner}
                />
              )}
            </TabPanel>
          )}
          {selectedTab === 'challenges' && (
            <TabPanel id="challenges" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <P>
                {t('general.attached_challenges_explanation', {
                  challenge_wording: customChalName || t('general.challenges'),
                })}
                <br />
                {t('program.home.challengeCta')}.
              </P>
              <Box py={4} position="relative">
                {!dataChallenges ? (
                  <Loading />
                ) : dataChallenges?.challenges.length === 0 ? (
                  <NoResults type={customChalName ? undefined : 'challenge'} />
                ) : (
                  <Grid tw="py-4">
                    {dataChallenges.challenges
                      ?.filter(({ status }) => status !== 'draft')
                      // don't display draft challenges
                      .map((challenge, index) => (
                        <ChallengeCard
                          key={index}
                          id={challenge.id}
                          banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                          short_title={challenge.short_title}
                          title={challenge.title}
                          title_fr={challenge.title_fr}
                          short_description={challenge.short_description}
                          short_description_fr={challenge.short_description_fr}
                          membersCount={challenge.members_count}
                          needsCount={challenge.needs_count}
                          clapsCount={challenge.claps_count}
                          projectsCount={challenge.projects_count}
                          space={challenge.space}
                          status={challenge.status}
                          has_saved={challenge.has_saved}
                        />
                      ))}
                  </Grid>
                )}
              </Box>
            </TabPanel>
          )}
          {selectedTab === 'projects' && (
            <TabPanel id="projects" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <ProgramProjects programId={program.id} customChalName={customChalName} />
            </TabPanel>
          )}
          {selectedTab === 'needs' && (
            <TabPanel id="needs" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <ProgramNeeds programId={program.id} customChalName={customChalName} />
            </TabPanel>
          )}
          {selectedTab === 'members' && (
            <TabPanel id="members" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]} position="relative">
              <ProgramMembers programId={program.id} customChalName={customChalName} />
            </TabPanel>
          )}
          {selectedTab === 'resources' && (
            <TabPanel id="resources" px={0} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <ProgramResources programId={program?.id} />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'faq' && (
            <TabPanel id="faq" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <ProgramFaq faqList={faqList} />
              </DesktopBorders>
            </TabPanel>
          )}
          <Box pt={8} pb={7} px={4} display={['block', undefined, undefined, undefined, 'none']}>
            <CtaAndLinks dataExternalLink={dataExternalLink} userData={userData} t={t} />
          </Box>
          {program?.is_admin && (
            <Box px={4} display={['block', undefined, undefined, undefined, 'none']}>
              <H2>{t('member.invite.general')}</H2>
              <InviteMember itemType="programs" itemId={program.id} />
            </Box>
          )}
          <Box pt={8} pb={7} px={4} display={['block', undefined, undefined, undefined, 'none']}>
            <ProgramLinks router={router} contactEmail={contactEmail} t={t} />
          </Box>
        </Box>
        {/* THIRD COLUMN, as a collapsible side drawer, only on desktop */}
        <section css={[tw`hidden flex-col mt-4 bg-white xl:flex`, isCollapsed && tw`hidden!`]}>
          {/* Create account CTA + external links */}
          <div tw="mb-8">
            <CtaAndLinks dataExternalLink={dataExternalLink} userData={userData} t={t} />
          </div>
          {/* Supporters bloc */}
          {selectedTab === 'about' &&
            program?.enablers && ( // show only on about page
              <Box display={['none', undefined, undefined, undefined, 'flex']} py={3}>
                <H2>{t('program.supporters')}</H2>
                <InfoHtmlComponent content={program?.enablers} />
                <a href="mailto:hello@jogl.io">{t('program.supporters_interested')}</a>
              </Box>
            )}
          {/* Latest announcements */}
          {selectedTab !== 'faq' && ( // show on all tabs except faq
            <Box pb={4}>
              <Box borderBottom="1px solid #d3d3d3" py={2}>
                <H2 mb={0}>{t('program.rightCompo.latestsAnnouncements')}</H2>
              </Box>
              {!dataPosts ? (
                <Loading />
              ) : dataPosts?.posts.length === 0 ? (
                <NoResults type="post" />
              ) : (
                <Box>
                  <Box>
                    {[...dataPosts?.posts].splice(0, 3).map((post, i) => (
                      <PostDisplayMinimal post={post} key={i} />
                    ))}
                  </Box>
                  <Box row p={2} justifyContent="center">
                    <A href={`/program/${router.query.short_title}?tab=news`} shallow scroll={false}>
                      <Button>{t('program.seeAll')}</Button>
                    </A>
                  </Box>
                </Box>
              )}
            </Box>
          )}
          {/* Meeting info bloc */}
          {(selectedTab === 'home' || selectedTab === 'about') &&
            program?.meeting_information && ( // show only on home page
              <Box bg="white" display={['none', undefined, undefined, undefined, 'flex']} pt={3}>
                <H2>{t('program.meeting_information')}</H2>
                <InfoHtmlComponent content={program?.meeting_information} />
              </Box>
            )}
          {/* Contact program leaders bloc */}
          {selectedTab === 'faq' && members && (
            <Box display={['none', undefined, undefined, undefined, 'flex']} py={3}>
              <H2>{t('footer.contactUs')}</H2>
              <Box pt={5} spaceY={4}>
                {members
                  .filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                  .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                    <Box key={index} row spaceX={4} alignItems="center">
                      <Link href={`/user/${id}`}>
                        <a>
                          <Box row spaceX={4} alignItems="center">
                            <img
                              width="50px"
                              height="50px"
                              style={{
                                borderRadius: '50%',
                                objectFit: 'cover',
                              }}
                              src={logo_url_sm}
                              alt={`${first_name} ${last_name}`}
                            />
                            <P fontWeight="bold" fontSize="1.2rem" mb={0}>
                              {first_name + ' ' + last_name}
                            </P>
                          </Box>
                        </a>
                      </Link>
                      <Box>
                        <ContactButton
                          size={20}
                          title="Contact user"
                          onClick={() => sendMessageToAdmin(id, first_name, last_name)}
                          onKeyUp={(e) =>
                            // execute only if it's the 'enter' key
                            (e.which === 13 || e.keyCode === 13) && sendMessageToAdmin(id, first_name, last_name)
                          }
                          tabIndex={0}
                          data-tip={t('user.contactModal.title', {
                            userFullName: first_name + ' ' + last_name,
                          })}
                          data-for="contactAdmin"
                          // show/hide tooltip on element focus/blur
                          onFocus={(e) => ReactTooltip.show(e.target)}
                          onBlur={(e) => ReactTooltip.hide(e.target)}
                        />
                        <ReactTooltip id="contactAdmin" effect="solid" place="bottom" />
                      </Box>
                    </Box>
                  ))}
              </Box>
            </Box>
          )}
          {/* style to make whole section sticky on top when attaining it */}
          <div tw="hidden flex-col xl:(flex sticky top-36)">
            {/* Invite someone component */}
            {program?.is_admin && (
              <Box py={4}>
                <H2>{t('member.invite.general')}</H2>
                <InviteMember itemType="programs" itemId={program.id} />
              </Box>
            )}
            {/* Program nav links */}
            <Box pt={3}>
              <ProgramLinks router={router} contactEmail={contactEmail} t={t} />
            </Box>
          </div>
        </section>
      </div>
    </Layout>
  );
};

const CtaAndLinks = ({ dataExternalLink, userData, t }) => {
  return (
    <>
      {!userData && ( // if user is not connected
        <Box row alignItems={['start', undefined, undefined, undefined, 'center']} pt={4} pb={6} flexWrap="wrap">
          <A href="/signup">
            <Button>{t('program.rightCompo.createAccount.btn')}</Button>
          </A>
          <P pt={2} mb={0}>
            {t('program.rightCompo.createAccount.text')}
          </P>
        </Box>
      )}
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <>
          <H2 pb={4}>{t('general.externalLink.findUs')}</H2>
          {dataExternalLink && dataExternalLink?.length !== 0 && (
            <div tw="flex flex-wrap gap-x-4 sm:gap-x-3">
              {[...dataExternalLink].map((link, i) => (
                <a tw="items-center self-center" key={i} href={link.url} target="_blank">
                  <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                </a>
              ))}
            </div>
          )}
        </>
      )}
    </>
  );
};

const ProgramLinks = ({ router, contactEmail, t }) => {
  return (
    <>
      <H2>{t('program.rightCompo.questions')}</H2>
      <Box row justifyContent="flex-start" spaceX={3}>
        <QuestionBox>
          <A href={`/program/${router.query.short_title}?tab=faq`} shallow scroll={false}>
            <QuestionCircle size={35} />
            <P>{t('faq.title')}</P>
          </A>
        </QuestionBox>
        <QuestionBox>
          <A href={`/program/${router.query.short_title}?tab=resources`} shallow scroll={false}>
            <BookOpen size={35} title="Resources" />
            <P>{t('program.resources.title')}</P>
          </A>
        </QuestionBox>
        <QuestionBox>
          <a href={`mailto:${contactEmail}`}>
            <Envelope size={35} title="Contact by email" />
            <P>{t('user.btn.contact')}</P>
          </a>
        </QuestionBox>
      </Box>
    </>
  );
};

const Tab = styled.a`
  display: inline-block;
  border-bottom: 3px solid transparent;
  border-bottom-color: ${(p) => p.selected && p.theme.colors.primary};
  font-weight: ${(p) => p.selected && 700};
  color: inherit;
  text-align: right;
  cursor: pointer;
  &:hover {
    border-bottom-color: ${(p) => p.theme.colors.primary};
    color: inherit;
    text-decoration: none;
  }
  &:focus {
    outline: none;
  }
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    white-space: nowrap;
  }
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid transparent;
    &:hover {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
    &:focus {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
  }
  ${[space, layout]};
`;

const TabPanel = styled.div`
  ${[layout, space]};
  .infoHtml {
    width: 100% !important;
  }
`;

const NavContainer = styled(Box)`
  position: relative;
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid ${(p) => p.theme.colors.greys['200']};
  }
  /* To make element sticky on top when reach top of page */
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    position: sticky;
    position: -webkit-sticky;
    top: 150px;
  }

  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    width: 100%;
    justify-content: flex-end;
    padding-right: 10px;
  }
`;
const Nav = styled(Box)`
  overflow-x: scroll;
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    font-size: ${(p) => p.theme.fontSizes.xl};
  }
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    padding-left: 1.8rem;
    button:last-child {
      padding-right: 3rem;
    }
    button {
      padding: 0 3px 5px;
    }
  }
`;

const OverflowGradient = styled.div`
  ${layout};
  height: 100%;
  position: absolute;
  ${(p) =>
    p.gradientPosition === 'right' &&
    'right:0;background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 3rem;'};
  ${(p) =>
    p.gradientPosition === 'left' &&
    'left:0;background: linear-gradient(90.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 2rem;'};
`;

const QuestionBox = styled(Box)`
  border: 2px solid ${(p) => p.theme.colors.greys['800']};
  height: 6rem;
  width: 6rem;
  border-radius: 1rem;
  align-items: center;
  justify-content: center;
  background: white;
  cursor: pointer;
  svg {
    font-size: 2.3rem;
    color: ${(p) => p.theme.colors.greys['800']};
    margin-right: 0;
  }
  p {
    margin: 10px 0 0;
    font-size: 100%;
    font-weight: bold;
  }
  a {
    width: 100%;
    text-align: center;
  }
  &:hover {
    p,
    svg {
      color: ${(p) => p.theme.colors.primary};
    }
    border: 2px solid ${(p) => p.theme.colors.primary};
    a {
      text-decoration: none !important;
    }
  }
`;

const ContactButton = styled(Envelope)`
  color: ${(p) => p.theme.colors.greys['700']};
  :hover {
    cursor: pointer;
    color: ${(p) => p.theme.colors.primary};
  }
`;

const Text = styled(P)`
  ${[typography, layout]};
  margin-bottom: 0;
  font-size: 17.5px;
  text-align: justify;
`;

// const QuickAction = styled(Box)`
//   justify-content: center;
//   padding: 0 0 1.5rem;
//   text-align: center;
//   align-items: center;
//   svg {
//     margin-right: 0;
//   }
//   &:hover {
//     color: ${(p) => p.theme.colors.greys['900']};
//     cursor: pointer;
//     > div {
//       border-color: ${(p) => p.theme.colors.greys['900']};
//       p,
//       svg {
//         color: ${(p) => p.theme.colors.greys['900']};
//       }
//     }
//   }
// `;

// const QuickActionIcon = styled(Box)`
//   border-radius: 50%;
//   border: 2px solid ${(p) => p.theme.colors.greys['600']};
//   width: 2.5rem;
//   height: 2.5rem;
//   justify-content: center;
//   align-items: center;
// `;

const Img = styled(Image2)`
  ${[flexbox, layout, space]};
  width: 100%;
`;

const LogoImg = styled.img`
  ${[flexbox, space, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
  width: 50px;
  height: 50px;
`;

const StickyHeading = styled(Box)`
  position: fixed;
  height: 70px;
  top: ${(p) => (p.isSticky ? '64px' : '-1000px')};
  width: 100%;
  z-index: 9;
  border-top: 1px solid grey;
  left: 0;
  transition: top 333ms;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
  overflow: hidden;
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    height: 60px;
    .actions {
      display: none;
    }
    img {
      width: 40px;
      height: 40px;
    }
  }
  > div {
    max-width: 1280px;
    margin: 0 auto;
    width: 100%;
  }
`;

// for desktop tabs that need an englobing div with padding and border
const DesktopBorders = ({ children }) => (
  <Box
    borderLeft={['none', undefined, '2px solid #f4f4f4']}
    borderRight={['none', undefined, undefined, undefined, '2px solid #f4f4f4']}
    pl={[0, undefined, 4]}
    pr={[0, undefined, undefined, undefined, 4]}
  >
    {children}
  </Box>
);

const TWGrid = styled.div([
  tw`bg-white pb-6 px-4 md:pb-11`,
  tw`grid grid-cols-1 md:grid-template-columns[20rem calc(100% - 20rem)] xl:grid-template-columns[25% 75%]`,
]);

const getProgram = async (api, programId) => {
  const res = await api.get(`/api/programs/${programId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/programs/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const program = await getProgram(api, res.data.id).catch((err) => console.error(err));
      return { props: { program } };
    }
    return { redirect: { destination: '/', permanent: false } };
  }
  // Case short_title is actually an id
  const program = await getProgram(api, query.short_title).catch((err) => console.error(err));
  if (program) {
    return { props: { program } };
  } else {
    return { redirect: { destination: '/', permanent: false } };
  }
}

export default ProgramDetails;
