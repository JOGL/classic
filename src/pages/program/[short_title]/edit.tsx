import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import ChallengeAdminCard from 'components/Challenge/ChallengeAdminCard';
import { ChallengeLinkModal } from 'components/Challenge/ChallengeLinkModal';
import Grid from 'components/Grid';
import Layout from 'components/Layout';
import MembersList from 'components/Members/MembersList';
import Button from 'components/primitives/Button';
import ProgramForm from 'components/Program/ProgramForm';
import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';
import Loading from 'components/Tools/Loading';
import ManageBoards from 'components/Tools/ManageBoards';
import ManageExternalLink from 'components/Tools/ManageExternalLink';
import ManageFaq from 'components/Tools/ManageFaq';
import ManageResources from 'components/Tools/ManageResources';
import NoResults from 'components/Tools/NoResults';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import { Challenge, Program } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import H2 from 'components/primitives/H2';
import ChallengeCreate from 'components/Challenge/ChallengeCreate';

interface Props {
  program: Program;
}
const ProgramEdit: NextPage<Props> = ({ program: programProp }) => {
  const [program, setProgram] = useState(programProp);
  const [updatedProgram, setUpdatedProgram] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const api = useApi();
  const { showModal, closeModal } = useModal();
  const router = useRouter();
  const {
    data: challengesData,
    revalidate: challengesRevalidate,
    mutate: mutateChallenges,
  } = useGet<{
    challenges: Challenge[];
  }>(`/api/programs/${program.id}/challenges`);
  const { t } = useTranslation('common');
  const customChalName = program.custom_challenge_name;
  const customChalNameSing = customChalName ? customChalName.slice(0, -1) : t('challenge.lowerCase');

  const handleChange = (key, content) => {
    setProgram((prevProgram) => ({ ...prevProgram, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedProgram((prevUpdatedProgram) => ({ ...prevUpdatedProgram, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api.patch(`/api/programs/${program.id}`, { program: updatedProgram }).catch((err) => {
      console.error(`Couldn't patch program with id=${program.id}`, err);
      setSending(false);
    });
    if (res) {
      // on success
      setSending(false);
      setUpdatedProgram(undefined); // reset updated program component
      setHasUpdated(true); // show update confirmation message
      setTimeout(() => {
        setHasUpdated(false);
      }, 3000); // hide confirmation message after 3 seconds
    }
  };

  // Tabs elements: tabs list & handleTabsChange
  // All explained in pages-project-index file
  const tabs = [
    { value: 'basic_info', tranlationId: 'entity.tab.basic_info' },
    { value: 'members', tranlationId: 'entity.tab.members' },
    { value: 'challenges', tranlationId: 'general.challenges' },
    { value: 'faqs', tranlationId: 'faq.title' },
    { value: 'boards', tranlationId: 'program.boardTitle' },
    { value: 'resources', tranlationId: 'program.resources.title' },
    { value: 'advanced', tranlationId: 'entity.tab.advanced' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/program/${program.short_title}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${program.title} | JOGL`}>
      <div className="programEdit" tw="container mx-auto px-4">
        <h1>{t('program.edit.title')}</h1>
        <Link href={`/program/${program.short_title}`}>
          <a>
            <ArrowLeft size={15} title="Go back" />
            {t('program.edit.back')}
          </a>
        </Link>
        <Tabs
          defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>
                {/* if tab is "challenges" and program has customChalName, show this string or "Challenges" */}
                {item.value === 'challenges' && customChalName ? customChalName : t(item.tranlationId)}
              </NavTab>
            ))}
          </TabListStyle>
          <TabPanels tw="justify-center">
            {/* Basic info */}
            <TabPanel>
              <ProgramForm
                mode="edit"
                program={program}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* Members */}
            <TabPanel>
              {program.id && <MembersList itemType="programs" itemId={program.id} isOwner={program.is_owner} />}
            </TabPanel>
            {/* Challenges */}
            <TabPanel>
              {/* Challenges */}
              <H2>{customChalName || t('general.challenges')}</H2>
              <div tw="my-4 justify-end">
                <Button
                  onClick={() => {
                    showModal({
                      children: (
                        <ChallengeCreate
                          closeModal={closeModal}
                          isModal
                          objectId={program.id}
                          objectType="program"
                          callBack={challengesRevalidate}
                          customChalName={program.custom_challenge_name}
                        />
                      ),
                      title: t('challenge.create.title', { challenge_wording: customChalNameSing }),
                      maxWidth: '50rem',
                      allowOverflow: true,
                    });
                  }}
                  btnType="secondary"
                >
                  {t('challenge.create.title', { challenge_wording: customChalNameSing })}
                </Button>
              </div>
              {challengesData ? (
                <div className="challengesAttachedList">
                  <div className="justify-content-end challengesAttachedListBar">
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            <ChallengeLinkModal
                              alreadyPresentChallenges={challengesData?.challenges}
                              objectId={program.id}
                              objectType="programs"
                              mutateChallenges={mutateChallenges}
                              closeModal={closeModal}
                            />
                          ),
                          title: t('attach.challenge.button.text', {
                            challenge_wording: customChalNameSing,
                          }),
                          maxWidth: '50rem',
                        });
                      }}
                    >
                      {t('attach.challenge.button.text', { challenge_wording: customChalNameSing })}
                    </Button>
                  </div>
                  {!challengesData ? (
                    <Loading />
                  ) : challengesData?.challenges.length === 0 ? (
                    <NoResults type="challenge" />
                  ) : (
                    <div tw="pt-6 divide-x-0 border-0 divide-y divide-gray-400 divide-solid">
                      {challengesData?.challenges.map((challenge, i) => (
                        <ChallengeAdminCard
                          challenge={challenge}
                          key={i}
                          parentId={program.id}
                          parentType="programs"
                          callBack={challengesRevalidate}
                        />
                      ))}
                    </div>
                  )}
                </div>
              ) : (
                <Loading />
              )}
            </TabPanel>
            {/* FAQs */}
            <TabPanel>
              <ManageFaq itemType="programs" itemId={program.id} />
            </TabPanel>
            {/* Boards */}
            <TabPanel>
              <ManageBoards itemType="programs" itemId={program.id} />
            </TabPanel>
            {/* Resources */}
            <TabPanel>
              <ManageResources itemType="programs" itemId={program.id} />
            </TabPanel>
            {/* Advanced */}
            <TabPanel>
              <ManageExternalLink itemType="programs" itemId={program.id} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/programs/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch program with short_title=${query.short_title}`, err));

  if (getIdRes?.data?.id) {
    const programRes = await api
      .get(`/api/programs/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch program with id=${getIdRes.data.id}`, err));
    // Check if it got the program and if the user is admin
    if (programRes?.data?.is_admin) return { props: { program: programRes.data } };
  }
  return { redirect: { destination: '/', permanent: false } };
}

export default ProgramEdit;
