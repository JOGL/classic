import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from 'contexts/UserProvider';
import Layout from 'components/Layout';
import ProgramForm from 'components/Program/ProgramForm';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import Loading from 'components/Tools/Loading';

const CreateProgram: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');
  const [loading, setLoading] = useState(true);
  const [sending, setSending] = useState(false);
  const { userData } = useUserData();
  const [newProgram, setNewProgram] = useState({
    title: '',
    short_title: '',
    logo_url: '',
    description: '',
    short_description: '',
    creator_id: Number(userContext.credentials.userId),
    status: 'draft',
    interests: [],
    skills: [],
    banner_url: '',
    is_private: false,
  });

  // Check for :programs_creator role on backend
  const { data: canCreate, error: cannotCreate } = useGet('/api/programs/can_create', { user: userData });
  useEffect(() => {
    if (canCreate) {
      setLoading(false);
    }
    if (cannotCreate) {
      setLoading(false);
      router.push('/program/forbidden');
    }
  }, [canCreate, cannotCreate]);

  const handleChange = (key, content) => {
    setNewProgram((prevProgram) => ({ ...prevProgram, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      // check if short_title already exists
      .get(`/api/programs/exists/${newProgram.short_title}`)
      // if it's not the case, then we creat the program
      .then((res) => {
        if (res.data.data === 'short_title is available') {
          api
            .post('/api/programs/', { program: newProgram })
            .then((res) => router.push(`/program/${res.data.short_title}/edit`))
            .catch(() => setSending(false));
        }
      })
      // else, show error message
      .catch((err) => {
        setSending(false);
        if (err.response.data.data === 'short_title already exists') {
          alert(t('err-4006'));
        }
      });
  };

  if (canCreate) {
    return (
      <Layout title={`${t('program.create.title')} | JOGL`}>
        <Loading active={loading}>
          <div className="programCreate container-fluid">
            <h1>{t('program.create.title')}</h1>
            <ProgramForm
              mode="create"
              program={newProgram}
              handleChange={handleChange}
              handleSubmit={handleSubmit}
              sending={sending}
            />
          </div>
        </Loading>
      </Layout>
    );
  }

  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default CreateProgram;
