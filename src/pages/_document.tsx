import Document, { Head, Main, NextScript, DocumentContext, Html } from 'next/document';

const dev = process.env.NODE_ENV !== 'production';
export default class IntlDocument extends Document {
  static async getStaticProps(context: DocumentContext) {
    const initialProps = await Document.getStaticProps(context);
    const language = context.req.language;
    return { props: { ...initialProps, language } };
  }

  render() {
    // check if page is embed one, and if yes, then it's rendered inside an iframe and we don't want certain scripts to launch on this case
    const isEmbedPage = this.props?.__NEXT_DATA__?.page === '/embedded/[objectType]/[[...index]]';
    return (
      <Html lang={this.props.language}>
        <Head>
          <link rel="shortcut icon" href="/images/favicon.ico" />
          <link rel="manifest" href="/manifest.json" />
          <link
            href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Roboto:wght@300;400;500;700&display=swap"
            rel="stylesheet"
          />
        </Head>
        {/* assume that user is using mouse by default. We use this to remove style from elements on focus if user is using mouse */}
        <body className="using-mouse">
          <Main />
          <script
            src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossOrigin="anonymous"
          />

          <script
            src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossOrigin="anonymous"
          />

          <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossOrigin="anonymous"
          />
          {/* Klaro config */}
          <link rel="stylesheet" href="/klaro.css" />
          {!dev && <script defer type="application/javascript" src="/klaroConfig.js"></script>}
          {!dev && (
            <script
              defer
              data-config="klaroConfig"
              type="application/javascript"
              src="https://cdn.kiprotect.com/klaro/v0.7/klaro-no-css.js"
            ></script>
          )}
          {/* Global site tag (gtag.js) - Google Analytics*/}
          {!dev && (
            <script
              // type="opt-in"
              // data-type="text/javascript"
              // data-name="googleTagManager"
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GOOGLE_ANALYTICS4_ID}`}
            ></script>
          )}
          {!dev && (
            <script
              // type="opt-in"
              // data-name="googleTagManager"
              // data-type="application/javascript"
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{
                __html: `
                  window.dataLayer = window.dataLayer || [];
                  function gtag(){dataLayer.push(arguments);}
                  gtag('js', new Date());
                  gtag('config', '${process.env.GOOGLE_ANALYTICS4_ID}');`,
              }}
            />
          )}
          {/* Start of Tawk.to Script */}
          {!dev && !isEmbedPage && (
            <script
              type="opt-in"
              data-name="tawk"
              data-type="application/javascript"
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{
                __html: `
              var Tawk_API = Tawk_API || {},
              Tawk_LoadStart = new Date();
            (function () {
              var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
              s1.async = true;
              s1.src = "https://embed.tawk.to/5d4849017d27204601c962e9/default";
              s1.charset = "UTF-8";
              s1.setAttribute("crossorigin", "*");
              s0.parentNode.insertBefore(s1, s0);
            })()
          `,
              }}
            />
          )}

          {/* <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script> */}
          <NextScript />
        </body>
      </Html>
    );
  }
}
