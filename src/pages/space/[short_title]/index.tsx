/* eslint-disable camelcase */
import { Envelope } from '@emotion-icons/fa-solid/Envelope';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import ReactGA from 'react-ga';
import ReactTooltip from 'react-tooltip';
import { flexbox, layout, space } from 'styled-system';
import Box from 'components/Box';
import Feed from 'components/Feed/Feed';
import Image2 from 'components/Image2';
import Layout from 'components/Layout';
import H2 from 'components/primitives/H2';
import P from 'components/primitives/P';
import SpaceAbout from 'components/Space/SpaceAbout';
import SpaceFaq from 'components/Space/SpaceFaq';
import SpaceHeader from 'components/Space/SpaceHeader';
// import SpaceHome from 'components/Space/SpaceHome';
import SpaceMembers from 'components/Space/SpaceMembers';
import SpaceNeeds from 'components/Space/SpaceNeeds';
import SpaceProjects from 'components/Space/SpaceProjects';
import SpaceActivities from 'components/Space/SpaceActivities';
import SpaceResources from 'components/Space/SpaceResources';
import { ContactForm } from 'components/Tools/ContactForm';
import { useModal } from 'contexts/modalContext';
// import Image from 'next/image';
import { useSpaceViewAsContext } from 'contexts/SpaceViewAsContext';
import useGet from 'hooks/useGet';
import useMembers from 'hooks/useMembers';
import { Faq, Space } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import styled from 'utils/styled';
import { useTheme } from 'utils/theme';

const DEFAULT_TAB = 'about';
const SpaceDetails: NextPage<{ space: Space }> = ({ space }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { locale } = router;
  const { showModal, closeModal } = useModal();
  const { members } = useMembers('spaces', space?.id, 25); // take 25 first members
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/spaces/${space.id}/faq`);
  const { data: resourcesList } = useGet(`/api/spaces/${space.id}/resources`);
  const tabs = [
    // add certain tabs to array only if admin had selected those tabs to be displayed
    // { value: 'home', translationId: 'program.home.title' },
    { value: 'about', translationId: 'general.tab.about' },
    ...(space.selected_tabs.challenges ? [{ value: 'activities', translationId: 'general.activities' }] : []),
    { value: 'projects', translationId: 'entity.tab.projects' },
    { value: 'members', translationId: 'entity.card.members' },
    { value: 'feed', translationId: 'user.profile.tab.feed' },
    { value: 'needs', translationId: 'entity.tab.needs' },
    // show resources tab only if admin wanted it to show, and if space has at least a resource OR a document uploaded
    ...(space.selected_tabs.resources && (space.documents.length !== 0 || resourcesList?.length !== 0)
      ? [{ value: 'resources', translationId: 'program.tab.resources' }]
      : []),
    ...(space.selected_tabs.faqs ? [{ value: 'faq', translationId: 'entity.info.faq' }] : []),
    { value: 'contact', translationId: 'user.btn.contact' },
  ];
  // const { data: dataPosts } = useGet(`/api/feeds/${space?.feed_id}?items=5&page=1`);
  const theme = useTheme();
  const navRef = useRef();
  const { viewAsMode } = useSpaceViewAsContext();

  useEffect(() => {
    // on first load, check if url has a tab param, and if it does, select this tab
    router.query.tab && setSelectedTab(router.query.tab as string);
  }, []);

  // space?.contact_email || 'hello@jogl.io';

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'about') {
        router.push(`/space/${router.query.short_title}`, undefined, { shallow: true });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 130 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const sendMessageToAdmin = (id, first_name, last_name) => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={closeModal} />,
      title: t('user.contactModal.title', { userFullName: `${first_name} ${last_name}` }),
    });
  };

  return (
    <Layout
      title={space?.title && `${space.title} | JOGL`}
      desc={space?.short_description}
      img={space?.banner_url || '/images/default/default-space.jpg'}
      className="small-top-margin"
      noIndex={space?.status === 'draft'}
    >
      <Box width="100%" position="relative">
        <img src={space?.banner_url || '/images/default/default-space.jpg'} alt={`${space?.title} banner`} />
        <Box position="absolute" top="10px" left="10px"></Box>
      </Box>
      <Box bg="white" pb={8} px={[3, 4, undefined, undefined, 0]} position="relative">
        <SpaceHeader space={space} members={members} lang={locale} />
      </Box>

      {/* ---- Page grid starts here ---- */}
      <div
        tw="bg-lightBlue pb-6 grid grid-cols-1 md:(bg-white grid-template-columns[12rem calc(100% - 12rem)])"
        ref={navRef}
      >
        {/* Header and nav (left col on desktop, top col on mobile */}
        <Box
          alignItems={[undefined, undefined, 'center']}
          bg="white"
          position="sticky"
          top={['125px', undefined, '50px']}
          zIndex={8}
        >
          <NavContainer bg="white">
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="left" />
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="right" />
            <Nav
              as="nav"
              flexDirection={['row', undefined, 'column']}
              spaceX={[3, undefined, 0]}
              pr={[0, undefined, 4]}
              pt={{ _: '10px', md: '0' }}
              width="100%"
              bg="white"
            >
              {tabs.map((item, index) => (
                <Link
                  shallow
                  scroll={false}
                  key={index}
                  href={`/space/${router.query.short_title}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                >
                  <Tab selected={item.value === selectedTab} px={[1, undefined, 3]} py={3} as="button">
                    {t(item.translationId)}
                  </Tab>
                </Link>
              ))}
            </Nav>
          </NavContainer>
        </Box>

        {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
        <div tw="px-0 pb-8 md:pb-0">
          {/* {selectedTab === 'home' && (
            <TabPanel id="home" px={[3, 4, 3, undefined, 0]} pt={[5, undefined, 0]}>
              <DesktopBorders>
                <SpaceHome
                  space={space}
                  shortDescription={space?.short_description}
                  isAdmin={space.is_admin && viewAsMode === 'owner'}
                  posts={dataPosts}
                  challenges={dataChallenges?.challenges}
                />
              </DesktopBorders>
            </TabPanel>
          )} */}
          {selectedTab === 'about' && (
            <TabPanel id="about" px={[3, 4, 3, undefined, 0]} pt={[3, undefined, undefined]}>
              <DesktopBorders>
                <SpaceAbout
                  description={space?.description}
                  shortDescription={space?.short_description}
                  status={space?.status}
                  spaceId={space?.id}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'feed' && (
            <TabPanel id="news" style={{ margin: '0 auto' }} width="100%" pt="2" px={[0, 4, 0]}>
              {/* Show feed, and pass allowPosting to admins */}
              {space.feed_id && (
                <Feed
                  feedId={space.feed_id}
                  allowPosting={space?.is_member}
                  isAdmin={(space.is_admin || space.is_owner) && viewAsMode === 'owner'}
                />
              )}
            </TabPanel>
          )}
          {selectedTab === 'activities' && (
            <TabPanel id="activities" px={[3, 4, 3, undefined, 0]} pt={[3, undefined, undefined]}>
              <SpaceActivities spaceId={space.id} isAdmin={space.is_admin} />
            </TabPanel>
          )}
          {selectedTab === 'projects' && (
            <TabPanel id="projects" px={[3, 4, 3, undefined, 0]} pt={3}>
              <SpaceProjects spaceId={space.id} isMember={space.is_member} hasFollowed={space.has_followed} />
            </TabPanel>
          )}
          {selectedTab === 'needs' && (
            <TabPanel id="needs" px={[3, 4, 3, undefined, 0]} pt={3}>
              <SpaceNeeds spaceId={space.id} />
            </TabPanel>
          )}
          {selectedTab === 'members' && (
            <TabPanel id="members" px={[3, 4, 3, undefined, 0]} pt={3} position="relative">
              <SpaceMembers spaceId={space.id} isMember={space.is_member} isAdmin={space.is_admin} />
            </TabPanel>
          )}
          {selectedTab === 'resources' && (
            <TabPanel id="resources" px={[3, 4, 3, undefined, 0]} pt={3}>
              <DesktopBorders>
                <SpaceResources
                  spaceId={space?.id}
                  isAdmin={space?.is_admin}
                  documents={space?.documents}
                  resources={resourcesList}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'faq' && (
            <TabPanel id="faq" px={[3, 4, 3, undefined, 0]} pt={3}>
              <DesktopBorders>
                <SpaceFaq faqList={faqList} />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'contact' && (
            <TabPanel id="contact" px={[3, 4, 3, undefined, 0]} pt={3}>
              <DesktopBorders>
                <H2>{t('footer.contactUs')}</H2>
                <Box pt={5} spaceY={4}>
                  {members
                    ?.filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                    .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                      <Box key={index} row spaceX={4} alignItems="center">
                        <Link href={`/user/${id}`}>
                          <a>
                            <Box row spaceX={4} alignItems="center">
                              <img
                                width="50px"
                                height="50px"
                                style={{ borderRadius: '50%', objectFit: 'cover' }}
                                src={logo_url_sm}
                                alt={`${first_name} ${last_name}`}
                              />
                              <P fontWeight="bold" fontSize="1.2rem" mb={0}>
                                {first_name + ' ' + last_name}
                              </P>
                            </Box>
                          </a>
                        </Link>
                        <Box>
                          <ContactButton
                            size={20}
                            title="Contact user"
                            onClick={() => sendMessageToAdmin(id, first_name, last_name)}
                            onKeyUp={(e) =>
                              // execute only if it's the 'enter' key
                              (e.which === 13 || e.keyCode === 13) && sendMessageToAdmin(id, first_name, last_name)
                            }
                            tabIndex={0}
                            data-tip={t('user.contactModal.title', { userFullName: first_name + ' ' + last_name })}
                            data-for="contactAdmin"
                            // show/hide tooltip on element focus/blur
                            onFocus={(e) => ReactTooltip.show(e.target)}
                            onBlur={(e) => ReactTooltip.hide(e.target)}
                          />
                          <ReactTooltip id="contactAdmin" effect="solid" place="bottom" />
                        </Box>
                      </Box>
                    ))}
                </Box>
              </DesktopBorders>
            </TabPanel>
          )}
        </div>
      </div>
    </Layout>
  );
};

const Tab = styled.a`
  display: inline-block;
  border-bottom: 3px solid transparent;
  border-bottom-color: ${(p) => p.selected && p.theme.colors.primary};
  font-weight: ${(p) => p.selected && 700};
  color: inherit;
  text-align: right;
  cursor: pointer;
  &:hover {
    border-bottom-color: ${(p) => p.theme.colors.primary};
    color: inherit;
    text-decoration: none;
  }
  &:focus {
    outline: none;
  }
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    white-space: nowrap;
  }
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid transparent;
    &:hover {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
    &:focus {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
  }
  ${[space, layout]};
`;

const TabPanel = styled.div`
  ${[layout, space]};
  .infoHtml {
    width: 100% !important;
  }
`;

const NavContainer = styled(Box)`
  position: relative;
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid ${(p) => p.theme.colors.greys['200']};
  }
  /* To make element sticky on top when reach top of page */
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    position: sticky;
    position: -webkit-sticky;
    top: 150px;
  }

  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    width: 100%;
    justify-content: flex-end;
  }
`;
const Nav = styled(Box)`
  overflow-x: scroll;
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    font-size: ${(p) => p.theme.fontSizes.xl};
  }
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    padding-left: 1.8rem;
    button:last-child {
      padding-right: 3rem;
    }
    button {
      padding: 0 3px 5px;
    }
  }
`;

const OverflowGradient = styled.div`
  ${layout};
  height: 100%;
  position: absolute;
  ${(p) =>
    p.gradientPosition === 'right' &&
    'right:0;background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 3rem;'};
  ${(p) =>
    p.gradientPosition === 'left' &&
    'left:0;background: linear-gradient(90.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 2rem;'};
`;

const ContactButton = styled(Envelope)`
  color: ${(p) => p.theme.colors.greys['700']};
  :hover {
    cursor: pointer;
    color: ${(p) => p.theme.colors.primary};
  }
`;

const Img = styled(Image2)`
  ${[flexbox, layout, space]};
  width: 100%;
`;

// for desktop tabs that need an englobing div with padding and border
const DesktopBorders = ({ children }) => (
  <Box
    borderLeft={['none', undefined, '2px solid #f4f4f4']}
    borderRight={['none', undefined, undefined, undefined, '2px solid #f4f4f4']}
    px={[0, undefined, 4]}
  >
    {children}
  </Box>
);

const getSpace = async (api, spaceId) => {
  const res = await api.get(`/api/spaces/${spaceId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/spaces/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const space = await getSpace(api, res.data.id).catch((err) => console.error(err));
      return { props: { space } };
    }
    return { redirect: { destination: '/', permanent: false } };
  }
  // Case short_title is actually an id
  const space = await getSpace(api, query.short_title).catch((err) => console.error(err));
  if (space) {
    return { props: { space } };
  } else {
    return { redirect: { destination: '/', permanent: false } };
  }
}

export default SpaceDetails;
