import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import ChallengeCreate from 'components/Challenge/ChallengeCreate';
import { useModal } from 'contexts/modalContext';
import ManageFaq from 'components/Tools/ManageFaq';
import ManageResources from 'components/Tools/ManageResources';
import ManageExternalLink from 'components/Tools/ManageExternalLink';
import ManageBoards from 'components/Tools/ManageBoards';
import Box from 'components/Box';
import ChallengeAdminCard from 'components/Challenge/ChallengeAdminCard';
import Layout from 'components/Layout';
import MembersList from 'components/Members/MembersList';
import Button from 'components/primitives/Button';
import H2 from 'components/primitives/H2';
import ProjectAdminCard from 'components/Project/ProjectAdminCard';
import SpaceForm from 'components/Space/SpaceForm';
import DocumentsManager from 'components/Tools/Documents/DocumentsManager';
import Loading from 'components/Tools/Loading';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import { Space, Project, Challenge } from 'types';
import { NavTab, TabListStyle, TabListStyleNoSticky } from 'components/Tabs/TabsStyles';
import useTranslation from 'next-translate/useTranslation';
import { getApiFromCtx } from 'utils/getApi';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import ProjectsAddModal from 'components/Project/ProjectsAddModal';
import MemberCardAffiliate from 'components/Members/MemberCardAffiliate';
import MembersAddModal from 'components/Members/MembersAddModal';
import useUserData from 'hooks/useUserData';
import { SpaceAddEventsModal } from 'components/Space/SpaceAddEventsModal';
import SpaceEventAdminCard from 'components/Space/SpaceEventAdminCard';
import { ProgramLinkModal } from 'components/Program/ProgramLinkModal';
import ProgramAdminCard from 'components/Program/ProgramAdminCard';

interface Props {
  space: Space;
}
const SpaceEdit: NextPage<Props> = ({ space: spaceProp }) => {
  const [space, setSpace] = useState(spaceProp);
  const { userData } = useUserData();
  const [updatedSpace, setUpdatedSpace] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [loadPending, setLoadPending] = useState(false);
  const [membersTab, setMembersTab] = useState(undefined);
  const [serviceId, setServiceId] = useState(undefined);
  const [affiliatedMembers, setAffiliatedMembers] = useState([]);
  const [affiliatedPrograms, setAffiliatedPrograms] = useState([]);
  const [nbOfPendingMembers, setNbOfPendingMembers] = useState(0);
  const [buttonToShow, setButtonToShow] = useState(undefined);
  const api = useApi();
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const router = useRouter();
  const {
    data: projectsData,
    revalidate: projectsRevalidate,
    mutate: mutateProjects,
  } = useGet<{
    projects: Project[];
  }>(`/api/spaces/${space.id}/projects`);
  const { data: challengesData, revalidate: challengesRevalidate } = useGet<{ challenges: Challenge[] }>(
    `/api/spaces/${space.id}/challenges`
  );
  const { data: eventsData, revalidate: eventsRevalidate } = useGet<{
    events: Challenge[];
  }>(`/api/spaces/${space.id}/activities`);

  const getAffiliatedMembers = () => {
    setAffiliatedMembers([]);
    api.get(`api/spaces/${space.id}/affiliated/users`).then((res) => {
      res.data.affiliates.users.map((user) => {
        api.get(`/api/users/${user.affiliate_id}`).then((res) => {
          setAffiliatedMembers((affiliatedMembers) => [
            ...affiliatedMembers,
            {
              first_name: res.data.first_name,
              last_name: res.data.last_name,
              logo_url_sm: res.data.logo_url_sm,
              status: user.status,
              id: user.affiliate_id,
            },
          ]);
        });
      });
    });
  };

  const getAffiliatedPrograms = () => {
    setAffiliatedPrograms([]);
    api.get(`api/spaces/${space.id}/affiliated/programs`).then((res) => {
      res.data.affiliates.programs.map((program) => {
        api.get(`/api/programs/${program.affiliate_id}`).then((res) => {
          setAffiliatedPrograms((affiliatedPrograms) => [
            ...affiliatedPrograms,
            {
              title: res.data.title,
              logo_url_sm: res.data.logo_url_sm,
              short_title: res.data.short_title,
              id: program.affiliate_id,
            },
          ]);
        });
      });
    });
  };

  useEffect(() => {
    getAffiliatedMembers();
    getAffiliatedPrograms();
    // get all space services
    api.get(`api/spaces/${space.id}/services`).then((res) => {
      // get id of service that has the name "eventbrite"
      const theServiceId = res.data.find((service) => service.name === 'eventbrite')?.id;
      setServiceId(theServiceId);
      api
        .get(`api/services/${theServiceId}`)
        // if we successfully get the service, show the "add events" button
        .then(() => setButtonToShow('addEvents'))
        // else show the "connect to eventbrite" button
        .catch(() => setButtonToShow('eventbriteConnect'));
    });
  }, []);

  const handleChange: (key: any, content: any) => void = (key, content) => {
    setSpace((prevSpace) => ({ ...prevSpace, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage space content
    setUpdatedSpace((prevUpdatedSpace) => ({ ...prevUpdatedSpace, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleChangeShowTabs: (key: any, content: any) => void = (key, content) => {
    const selected_tabs = key.split('.')[1];
    setSpace((prevSpace) => ({
      ...prevSpace,
      selected_tabs: { ...prevSpace.selected_tabs, [selected_tabs]: content },
    })); // update fields as user changes them
    // TODO: have only one place where we manage space content
    setUpdatedSpace((prevUpdatedSpace) => ({
      ...prevUpdatedSpace,
      selected_tabs: { ...space.selected_tabs, [selected_tabs]: content },
    })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api.patch(`/api/spaces/${space.id}`, { space: updatedSpace }).catch((err) => {
      console.error(`Couldn't patch space with id=${space.id}`, err);
      setSending(false);
    });
    if (res) {
      // on success
      setSending(false);
      setUpdatedSpace(undefined); // reset updated space component
      setHasUpdated(true); // show update confirmation message
      setTimeout(() => {
        setHasUpdated(false);
      }, 3000); // hide confirmation message after 3 seconds
    }
  };

  // Tabs elements: tabs list & handleTabsChange
  // All explained in pages-project-index file
  const tabs = [
    { value: 'general', translationId: 'footer.general' },
    // { value: 'home', translationId: 'program.home.title' },
    { value: 'about', translationId: 'general.tab.about' },
    { value: 'projects', translationId: 'entity.tab.projects' },
    { value: 'activities', translationId: 'general.activities' },
    { value: 'members', translationId: 'entity.tab.members' },
    { value: 'affiliations', translationId: 'general.affiliations' },
    { value: 'faqs', translationId: 'faq.title' },
    // { value: 'partners', translationId: 'entity.info.enablers' },
    { value: 'resources', translationId: 'program.tab.resources' },
    { value: 'advanced', translationId: 'entity.tab.advanced' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/space/${space.short_title}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${space.title} | JOGL`}>
      <div className="programEdit" tw="container mx-auto px-4">
        <h1>{t('space.edit.title')}</h1>
        <Link href={`/space/${space.short_title}`}>
          <a>
            <ArrowLeft size={15} title="Go back" />
            {t('space.edit.back')}
          </a>
        </Link>
        <Tabs
          defaultIndex={
            router.query.t
              ? // if there is a "t" query param, make this tab default
                tabs.findIndex((t) => t.value === router.query.t)
              : typeof window !== 'undefined' && router.asPath.match(/#([a-z0-9-]+)/gi)
              ? // else check if we have a hashtag (meaning we want to redirect to the activities tab, it's a workaround we used to go back to the same url when connecting to eventbrite)
                tabs.findIndex((t) => t.value === 'activities')
              : // else null
                null
          }
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>{t(item.translationId)}</NavTab>
            ))}
          </TabListStyle>
          <TabPanels tw="justify-center">
            {/* General infos tab */}
            <TabPanel>
              <SpaceForm
                mode="edit"
                space={space}
                handleChange={handleChange}
                handleChangeShowTabs={handleChangeShowTabs}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </TabPanel>

            {/* Home tab infos */}
            {/* <TabPanel>
              <SpaceForm
                mode="edit_home"
                space={space}
                handleChange={handleChange}
                handleChangeFeatured={handleChangeFeatured}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </TabPanel> */}

            {/* About tab */}
            <TabPanel>
              <SpaceForm
                mode="edit_about"
                space={space}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
              {/* waiting to have boards support for spaces */}
              <hr tw="mt-8" />
              <ManageBoards itemType="spaces" itemId={space.id} />
            </TabPanel>

            {/* Projects tab */}
            <TabPanel>
              <H2 pb={3}>{t('user.profile.tab.projects')}</H2>
              <div className="projectsAttachedList">
                <Box pb={6}>
                  <div className="justify-content-end projectsAttachedListBar">
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            <ProjectsAddModal itemId={space.id} callBack={mutateProjects} closeModal={closeModal} />
                          ),
                          title: t('attach.project.add'),
                          maxWidth: '50rem',
                          allowOverflow: true,
                        });
                      }}
                    >
                      {t('attach.project.add')}
                    </Button>
                  </div>
                </Box>
                {projectsData ? (
                  <div tw="pt-4">
                    {projectsData?.projects // sort the array to have the pending projects first
                      .reduce((acc, element) => {
                        if (
                          // find space affiliation_status by accessing object with same space_id as the accessed space id
                          element.affiliated_spaces[0]?.find((obj) => obj.id === space.id)?.affiliation_status ===
                          'pending'
                        ) {
                          return [element, ...acc];
                        }
                        return [...acc, element];
                      }, [])
                      .map((project, i) => (
                        <ProjectAdminCard
                          key={i}
                          project={project}
                          parentType="spaces"
                          parentId={space.id}
                          callBack={projectsRevalidate}
                        />
                      ))}
                  </div>
                ) : (
                  <Loading />
                )}
              </div>
            </TabPanel>

            {/* Activities tab */}
            <TabPanel>
              {/* Challenges */}
              <H2>{t('general.challenges')}</H2>
              <div tw="my-4 justify-end">
                <Button
                  onClick={() => {
                    showModal({
                      children: (
                        <ChallengeCreate
                          isModal
                          objectId={space.id}
                          objectType="space"
                          closeModal={closeModal}
                          callBack={challengesRevalidate}
                        />
                      ),
                      title: t('challenge.create.title', { challenge_wording: t('challenge.lowerCase') }),
                      maxWidth: '50rem',
                    });
                  }}
                  btnType="secondary"
                >
                  {t('challenge.create.title', { challenge_wording: t('challenge.lowerCase') })}
                </Button>
              </div>
              <div className="projectsAttachedList">
                {/* <Box pb={6}>
                  <div className="justify-content-end projectsAttachedListBar">
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            // <ChallengeLinkToSpace
                            //   alreadyPresentChallenges={challengesData?.challenges}
                            //   spaceId={space.id}
                            //   mutateChallenges={mutateChallenges}
                            //   closeModal={closeModal}
                            // />
                            <ChallengeLinkModal
                              alreadyPresentChallenges={challengesData?.challenges}
                              objectId={space.id}
                              objectType="spaces"
                              mutateChallenges={mutateChallenges}
                              closeModal={closeModal}
                            />
                          ),
                          title: t('attach.challenge.button.text', { challenge_wording: t('challenge.lowerCase') }),
                          maxWidth: '50rem',
                        });
                      }}
                    >
                      {t('attach.challenge.button.text', { challenge_wording: t('challenge.lowerCase') })}
                    </Button>
                  </div>
                </Box> */}
                {challengesData ? (
                  <div tw="pt-4 divide-x-0 border-0 divide-y divide-gray-400 divide-solid">
                    {challengesData?.challenges.map((challenge, i) => (
                      <ChallengeAdminCard
                        challenge={challenge}
                        key={i}
                        parentType="spaces"
                        parentId={space.id}
                        showChalType
                        callBack={challengesRevalidate}
                      />
                    ))}
                  </div>
                ) : (
                  <Loading />
                )}
              </div>
              <hr tw="mt-8" />
              {/* Programs */}
              <H2>{t('general.programs')}</H2>
              <div tw="my-4 justify-end"></div>
              <Button
                onClick={() => {
                  showModal({
                    children: (
                      <ProgramLinkModal
                        alreadyPresentPrograms={affiliatedPrograms}
                        objectId={space.id}
                        callBack={getAffiliatedPrograms}
                        closeModal={closeModal}
                      />
                    ),
                    title: t('attach.challenge.button.text', {
                      challenge_wording: t('program.title'),
                    }),
                    maxWidth: '50rem',
                  });
                }}
              >
                {t('attach.challenge.button.text', {
                  challenge_wording: t('program.title'),
                })}
              </Button>
              {!affiliatedPrograms ? (
                <Loading />
              ) : (
                <div tw="pt-4 divide-x-0 border-0 divide-y divide-gray-400 divide-solid">
                  {affiliatedPrograms?.map((program, i) => (
                    <ProgramAdminCard program={program} key={i} spaceId={space.id} callBack={getAffiliatedPrograms} />
                  ))}
                </div>
              )}
              {/* Events */}
              <hr tw="mt-8" />
              <H2>{t('space.event.titlePlural')}</H2>
              <div tw="my-4 justify-end">
                {buttonToShow === 'eventbriteConnect' ? (
                  // show button that will connect space to eventbrite
                  <a
                    href={`${process.env.ADDRESS_BACK}/api/auth/eventbrite?user_id=${userData?.id}&serviceable_type=space&serviceable_id=${space?.id}`}
                    // href={`${process.env.ADDRESS_BACK}/api/auth/eventbrite?user_id=${userData?.id}&serviceable_type=space&serviceable_id=${space?.id}&redirect_link=/space/${space?.short_title}/edit#activities
                    target="_blank"
                  >
                    <Button btnType="secondary">Connect to Eventbrite</Button>
                  </a>
                ) : buttonToShow === 'addEvents' ? (
                  // else show button to add events
                  <Button
                    onClick={() => {
                      showModal({
                        children: (
                          <SpaceAddEventsModal
                            alreadyPresentEvents={eventsData?.map(({ id }) => id)} // get only events ids
                            serviceId={serviceId}
                            closeModal={closeModal}
                            callBack={eventsRevalidate}
                          />
                        ),
                        maxWidth: '50rem',
                        title: t('space.event.add'),
                      });
                    }}
                    btnType="secondary"
                  >
                    {t('space.event.add')}
                  </Button>
                ) : (
                  <Loading />
                )}
                {!eventsData ? (
                  <Loading />
                ) : (
                  <div tw="pt-4 divide-x-0 border-0 divide-y divide-gray-400 divide-solid">
                    {eventsData?.map((event, i) => (
                      <SpaceEventAdminCard event={event} key={i} serviceId={serviceId} callBack={eventsRevalidate} />
                    ))}
                  </div>
                )}
              </div>
            </TabPanel>

            {/* Members tab */}
            <TabPanel>
              <Tabs
                defaultIndex={nbOfPendingMembers > 0 ? 1 : 0}
                onChange={(id) => {
                  id === 1 && setLoadPending(true);
                  setMembersTab(id);
                }}
              >
                {/* {nbOfPendingMembers > 0 && ( */}
                <TabListStyleNoSticky>
                  <NavTab>{t('entity.tab.activeMembers')}</NavTab>
                  <NavTab>
                    {t('entity.tab.pendingMembers')} ({nbOfPendingMembers})
                  </NavTab>
                </TabListStyleNoSticky>
                {/* )} */}
                <TabPanels tw="justify-center">
                  {/* Active members */}
                  <TabPanel>
                    {space.id && (
                      <MembersList
                        itemType="spaces"
                        itemId={space.id}
                        isOwner={space.is_owner}
                        setNbOfPendingMembers={setNbOfPendingMembers}
                      />
                    )}
                  </TabPanel>
                  {/* Pending members */}
                  <TabPanel>
                    {space.id && (
                      <MembersList
                        itemType="spaces"
                        itemId={space.id}
                        isOwner={space.is_owner}
                        onlyPending
                        setNbOfPendingMembers={setNbOfPendingMembers}
                      />
                    )}
                  </TabPanel>
                </TabPanels>
              </Tabs>
            </TabPanel>

            {/* Affiliations tab */}
            <TabPanel>
              {/* Members */}
              <H2 pb={3}>{t('entity.tab.members')}</H2>
              <Button
                onClick={() => {
                  showModal({
                    children: (
                      <MembersAddModal
                        itemType="spaces"
                        itemId={space.id}
                        callBack={getAffiliatedMembers}
                        type="affiliate"
                      />
                    ),
                    title: t('member.btnNewMembers.title'),
                    allowOverflow: true,
                  });
                }}
              >
                {t('member.btnNewMembers.title')}
              </Button>
              {affiliatedMembers?.length !== 0 && (
                <div tw="pt-4">
                  {affiliatedMembers?.map((member, i) => (
                    <MemberCardAffiliate
                      key={i}
                      itemId={space.id}
                      itemType="spaces"
                      member={member}
                      role={member.status}
                      callBack={getAffiliatedMembers}
                    />
                  ))}
                </div>
              )}
            </TabPanel>

            {/* FAQs tab */}
            <TabPanel>
              <ManageFaq itemType="spaces" itemId={space.id} />
            </TabPanel>

            {/* Partners tab */}
            {/* <TabPanel>
              <SpaceForm
                mode="edit_partners"
                space={space}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </TabPanel> */}

            {/* Resources tab */}
            <TabPanel>
              <H2 pb={3}>{t('entity.tab.documents')}</H2>
              <DocumentsManager
                documents={space.documents}
                isAdmin={space.is_admin}
                itemId={space.id}
                itemType="spaces"
              />
              <Box pb={5}></Box>
              <ManageResources itemType="spaces" itemId={space.id} />
            </TabPanel>

            {/* Advanced tab */}
            <TabPanel>
              <ManageExternalLink itemType="spaces" itemId={space.id} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/spaces/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch space with short_title=${query.short_title}`, err));

  if (getIdRes?.data?.id) {
    const spaceRes = await api
      .get(`/api/spaces/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch space with id=${getIdRes.data.id}`, err));
    // Check if it got the space and if the user is admin
    if (spaceRes?.data?.is_admin) return { props: { space: spaceRes.data } };
  }
  // else redirect to homepage
  return { redirect: { destination: '/', permanent: false } };
}

export default SpaceEdit;
