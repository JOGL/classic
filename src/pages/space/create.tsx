import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { useContext, useEffect, useState } from 'react';
import { UserContext } from 'contexts/UserProvider';
import Layout from 'components/Layout';
import SpaceForm from 'components/Space/SpaceForm';
import Loading from 'components/Tools/Loading';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';

const CreateSpace: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [sending, setSending] = useState(false);
  const { t } = useTranslation('common');
  const [newSpace, setNewSpace] = useState({
    title: '',
    short_title: '',
    logo_url: '',
    description: '',
    short_description: '',
    creator_id: Number(userContext.credentials.userId),
    status: 'draft',
    interests: [],
    skills: [],
    banner_url: '',
  });
  const { userData } = useUserData();
  // Limit spaces access to a set of specific users while the feature is built
  // Check for :spaces_creator role on backend
  const { data: canCreate, error: cannotCreate } = useGet('/api/spaces/can_create', { user: userData });

  // If can't create a space, display forbidden page
  useEffect(() => {
    if (canCreate) {
      setLoading(false);
    }
    if (cannotCreate) {
      setLoading(false);
      router.push('/space/forbidden');
    }
  }, [canCreate, cannotCreate]);

  const handleChange = (key, content) => {
    setNewSpace((prevSpace) => ({ ...prevSpace, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      // check if short_title already exists
      .get(`/api/spaces/exists/${newSpace.short_title}`)
      // if it's not the case, then we creat the space
      .then((res) => {
        if (res.data.data === 'short_title is available') {
          api
            .post('/api/spaces/', { space: newSpace })
            .then((res) => router.push(`/space/${res.data.short_title}/edit`))
            .catch(() => setSending(false));
        }
      })
      // else, show error message
      .catch((err) => {
        setSending(false);
        if (err.response.data.data === 'short_title already exists') {
          alert(t('err-4006'));
        }
      });
  };

  if (canCreate) {
    return (
      <Layout title={`${t('space.create.title')} | JOGL`}>
        <Loading active={loading}>
          <div className="programCreate container-fluid">
            <h1>{t('space.create.title')}</h1>
            <SpaceForm
              mode="create"
              space={newSpace}
              handleChange={handleChange}
              handleSubmit={handleSubmit}
              sending={sending}
            />
          </div>
        </Loading>
      </Layout>
    );
  }

  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default CreateSpace;
