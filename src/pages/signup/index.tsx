/* eslint-disable @rushstack/no-null */
// ? Too many states, maybe group all the form state into one state. Or use useReducer
/* eslint-disable no-nested-ternary */
import Link from 'next/link';
import signUpFormRules from './signUpFormRules.json';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { toAlphaNum } from 'components/Tools/Nickname';
import { UserContext } from 'contexts/UserProvider';
import Alert from 'components/Tools/Alert';
import FormValidator from 'components/Tools/Forms/FormValidator';
import Layout from 'components/Layout';
import { useApi } from 'contexts/apiContext';
import Box from 'components/Box';
// import Image from 'next/image';
import Image2 from 'components/Image2';
import Trans from 'next-translate/Trans';
import SpinLoader from 'components/Tools/SpinLoader';
import { logEventToGA } from 'utils/analytics';
import tw from 'twin.macro';
import SignInUpCarousel from 'components/SignInUpCarousel';

const validator = new FormValidator(signUpFormRules);

const SignUp: NextPage = () => {
  const router = useRouter();

  const [acceptConditions, setAcceptConditions] = useState(false);
  const [acceptLegal, setAcceptLegal] = useState(false);
  const [mailing_auth, setMailing_auth] = useState(false);
  const [first_name, setFirst_name] = useState('');
  const [last_name, setLast_name] = useState('');
  const [nickname, setNickname] = useState('');
  // Passing the mail from the url params to the state as a default value
  const [email_value, setEmail_value] = useState(router.query.email ? router.query.email : '');
  const [password, setPassword] = useState('');
  const [password_confirmation, setPassword_confirmation] = useState('');
  const [error, setError] = useState('');
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [stateValidation, setStateValidation] = useState({
    valid_password_confirmation: '',
    valid_acceptConditions: '',
    valid_acceptLegal: '',
    valid_first_name: '',
    valid_last_name: '',
    valid_nickname: '',
    valid_email: '',
    valid_password: '',
  });
  const api = useApi();
  const userContext = useContext(UserContext);
  const { t } = useTranslation('common');

  const checkPassword = (pwd, pwdConfirm) => {
    if (pwd !== pwdConfirm) {
      setStateValidation((prevState) => ({
        ...prevState,
        valid_password_confirmation: { isInvalid: 'is-invalid', message: 'err-4001' },
      }));
      return false;
    }
    return true;
  };
  const generateNickname = (firstName, lastName) => {
    let proposalNickname = firstName.trim() + lastName.trim();
    proposalNickname = toAlphaNum(proposalNickname);
    setNickname(proposalNickname);
    return proposalNickname;
  };

  const handleChange = (e) => {
    const { checked, name } = e.target;
    let { value } = e.target;
    let proposalNickname;
    switch (name) {
      case 'first_name':
        proposalNickname = generateNickname(value, last_name);
        setFirst_name(value);
        break;
      case 'last_name':
        proposalNickname = generateNickname(first_name, value);
        setLast_name(value);
        break;
      case 'nickname':
        value = toAlphaNum(value);

        setNickname(value);
        break;
      case 'acceptConditions':
        value = checked;
        setAcceptConditions(value);
        break;
      case 'mailing_auth':
        value = checked;
        setMailing_auth(value);
        break;
      case 'email':
        setEmail_value(value);
        break;
      case 'password':
        setPassword(value);
        break;
      case 'password_confirmation':
        setPassword_confirmation(value);
        break;
      case 'acceptLegal':
        value = checked;
        setAcceptLegal(value);
        break;
      default:
        // console.warn(`Case ${name} is not handled`);
        break;
    }
    /* Validators start */
    const state = {};
    state[name] = value;

    // Check proposalNickname
    if (name === 'first_name' || name === 'last_name') {
      state.nickname = proposalNickname;
    }

    const validation = validator.validate(state);
    if (validation[name] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${name}`] = validation[name];
      // Update nickname too only if firstname or nickname has been changed
      if (name === 'first_name' || name === 'last_name') {
        newStateValidation.valid_nickname = validation.nickname;
      }
      setStateValidation(newStateValidation);
    }
    /* Validators end */
    setError('');
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const randomNumber = Math.floor(Math.random() * 12) + 1; // with 12 = max, and 1 = min
    // make logo_url randomly equals to one of our default image (1 to 12)
    const logoUrl = `/images/default/default-user-${randomNumber}.png`;
    const mail_newsletter = mailing_auth;
    const user = {
      first_name,
      last_name,
      nickname,
      email: email_value,
      password,
      password_confirmation,
      acceptConditions,
      acceptLegal,
      mail_newsletter,
      logo_url: logoUrl,
    };
    /* Validators control before submit */
    const validation = validator.validate(user);
    if (validation.isValid) {
      if (checkPassword(password, password_confirmation)) {
        setError('');
        setSending(true);
        api
          .post('/api/users/', { user })
          .then((res) => {
            setSending(false);
            if (res?.data?.nickname) {
              setStateValidation((prevState) => ({
                ...prevState,
                valid_nickname: { isInvalid: 'is-invalid', message: 'err-4005' },
              }));
            } else {
              setSuccess(true);
              // send event to google analytics
              logEventToGA('signup', 'User', '', '');
              // add "?success=1" to the url, so we can measure the number of people who signed up in google analytics
              router.push({ pathname: '/signup', query: { success: 1 } }, undefined, { shallow: true });
              // force scroll to top
              typeof window !== 'undefined' && window.scrollTo(0, 0);
            }
          })
          .catch((err) => {
            if (err?.response?.data?.error) {
              const errorMessage = err.response.data.error;
              setError(errorMessage);
            }
            setSending(false);
          });
      }
    } else {
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };
  const {
    valid_acceptConditions,
    valid_acceptLegal,
    valid_password_confirmation,
    valid_first_name,
    valid_last_name,
    valid_nickname,
    valid_email,
    valid_password,
  } = stateValidation;

  const errorMessage = error.includes('err-') ? t(error) : error;

  useEffect(() => {
    if (userContext.isConnected) {
      router.push('/');
    }
  });

  return (
    <Layout className="no-margin" title={`${t('header.signUp')} | JOGL`}>
      <Box pb={[10, undefined, undefined, 0]}>
        <div className="auth-form row align-items-center">
          {/* Left section carousel on desktop (and top section jogl logo on mobile) */}
          <div className="col-12 col-lg-6 leftPannel d-flex align-items-center justify-content-center" tw="pl-8 pr-4">
            <div className="leftDivMobile">
              <Link href="/">
                <a>
                  <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" />
                </a>
              </Link>
            </div>
            <SignInUpCarousel />
          </div>

          {/* Right section (or bottom on mobile) */}
          <div className="col-12 col-lg-6 rightPannel">
            <div className="form-content">
              <div
                tw="flex flex-col justify-center items-center mb-4 md:mb-8 mt-4 lg:mt-0"
                className="form-header signup"
              >
                {/* align-items */}
                {/* TODO understand why font size and color do not work */}
                <h2 className="form-title" id="signModalLabel" tw="(text-primary text-3xl mb-0)! text-center">
                  {success ? t('signUp.confTitle') : t('signUp.title')}
                </h2>
                <p tw="(text-gray-400 text-base italic)! text-center">
                  {success ? t('signUp.confMsg') : t('signUp.description')}
                </p>
              </div>

              {/* Message to let user that already have account go to signin page */}
              <div className="form-body" css={[tw`block`, success && tw`hidden`]}>
                <div className="goToSignUp signup">
                  <span>{t('signUp.alreadyAccount')}</span>
                  <span className="goToSignUp--signup">
                    <Link href="/signin">
                      <a>{t('header.signIn')}</a>
                    </Link>
                  </span>
                </div>

                {/* Actual form */}
                <form onSubmit={handleSubmit} className="row">
                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      {t('signUp.firstname')}
                    </label>
                    <div className="input-group">
                      <input
                        type="text"
                        name="first_name"
                        id="first_name"
                        className={`form-control ${
                          valid_first_name ? (!valid_first_name.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={t('signUp.firstname_placeholder')}
                        onChange={handleChange}
                      />
                      {valid_first_name ? (
                        valid_first_name.message !== '' ? (
                          <div className="invalid-feedback">{t(valid_first_name.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      {t('signUp.lastname')}
                    </label>
                    <div className="input-group">
                      <input
                        type="text"
                        name="last_name"
                        id="last_name"
                        className={`form-control ${
                          valid_last_name ? (!valid_last_name.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={t('signUp.lastname_placeholder')}
                        onChange={handleChange}
                      />
                      {valid_last_name ? (
                        valid_last_name.message !== '' ? (
                          <div className="invalid-feedback">{t(valid_last_name.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      {t('signUp.nickname')}
                    </label>
                    <div className="input-group">
                      <input
                        type="text"
                        name="nickname"
                        id="nickname"
                        className={`form-control ${
                          valid_nickname ? (!valid_nickname.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={t('signUp.nickname_placeholder')}
                        onChange={handleChange}
                        value={nickname}
                      />
                      {valid_nickname ? (
                        valid_nickname.message !== '' ? (
                          <div className="invalid-feedback">{t(valid_nickname.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="email">
                      {t('auth.email.title')}
                    </label>
                    <div className="input-group">
                      <input
                        type="email"
                        name="email"
                        id="email"
                        value={email_value}
                        placeholder={t('auth.email.placeholder')}
                        className={`form-control ${
                          valid_email ? (!valid_email.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        onChange={handleChange}
                      />
                      {valid_email ? (
                        valid_email.message !== '' ? (
                          <div className="invalid-feedback">{t(valid_email.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="password">
                      {t('signUp.pwd')}
                    </label>
                    <div className="input-group">
                      <input
                        type="password"
                        name="password"
                        id="password"
                        className={`form-control ${
                          valid_password ? (!valid_password.isInvalid ? 'is-valid' : 'is-invalid') : ''
                        }`}
                        placeholder={t('signUp.pwd_placeholder')}
                        onChange={handleChange}
                      />
                      {valid_password ? (
                        valid_password.message !== '' ? (
                          <div className="invalid-feedback">{t(valid_password.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-group col-12 col-md-6">
                    <label className="form-check-label" htmlFor="password_confirmation">
                      {t('signUp.pwdConfirm')}
                    </label>
                    <div className="input-group">
                      <input
                        type="password"
                        name="password_confirmation"
                        id="password_confirmation"
                        className={`form-control ${
                          valid_password_confirmation
                            ? !valid_password_confirmation.isInvalid
                              ? 'is-valid'
                              : 'is-invalid'
                            : ''
                        }`}
                        placeholder={t('signUp.pwdConfirm_placeholder')}
                        onChange={handleChange}
                      />
                      {valid_password_confirmation ? (
                        valid_password_confirmation.message !== '' ? (
                          <div className="invalid-feedback">{t(valid_password_confirmation.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  {error !== '' && <Alert type="danger" message={errorMessage} />}

                  {/* One user authorization for both monthly newsletters and weekly updates */}
                  <div className="form-check">
                    <div className="input-group">
                      <input
                        className="form-check-input"
                        id="mailing_auth"
                        name="mailing_auth"
                        onChange={handleChange}
                        type="checkbox"
                        checked={mailing_auth}
                      />
                      <label className="form-check-label" htmlFor="mailing_auth">
                        {t('signUp.mailing_auth')}
                      </label>
                    </div>
                  </div>

                  <div className="form-check accept">
                    <div className="input-group">
                      <input
                        className={`form-check-input ${
                          valid_acceptConditions ? (!valid_acceptConditions.isInvalid ? '' : 'is-invalid') : ''
                        }`}
                        id="acceptConditions"
                        name="acceptConditions"
                        onChange={handleChange}
                        type="checkbox"
                        ischecked={acceptConditions.toString()}
                      />
                      <label className="form-check-label d-inline" htmlFor="acceptConditions">
                        <Trans
                          i18nKey="common:signUp.termsOfService"
                          components={[
                            <div className="hookExplain" />,
                            <a href="/terms" target="_blank" className="nav-link" />,
                            <a href="/data" target="_blank" className="nav-link" />,
                            <a href="/ethics-pledge" target="_blank" className="nav-link" />,
                          ]}
                          values={{
                            termsOfService: t('signUp.termsOfServiceLinkText'),
                            dataPolicy: t('footer.data'),
                            ethicsPledge: t('signUp.ethicsPledgeTextLink'),
                          }}
                        />
                      </label>
                      {valid_acceptConditions ? (
                        valid_acceptConditions.message !== '' ? (
                          <div className="invalid-feedback text-left">{t(valid_acceptConditions.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <div className="form-check accept">
                    <div className="input-group">
                      <input
                        className={`form-check-input ${
                          valid_acceptLegal ? (!valid_acceptLegal.isInvalid ? '' : 'is-invalid') : ''
                        }`}
                        id="acceptLegal"
                        name="acceptLegal"
                        onChange={handleChange}
                        type="checkbox"
                        ischecked={acceptLegal.toString()}
                      />
                      <label className="form-check-label" htmlFor="acceptLegal">
                        <span>{t('signUp.legalConf')}</span>
                      </label>
                      {valid_acceptLegal ? (
                        valid_acceptLegal.message !== '' ? (
                          <div className="invalid-feedback text-left">{t(valid_acceptLegal.message)}</div>
                        ) : null
                      ) : null}
                    </div>
                  </div>

                  <button type="submit" className="btn btn-primary btn-block" disabled={sending}>
                    {sending && <SpinLoader />}
                    {t('signUp.btnCreate')}
                  </button>
                </form>
              </div>
              <div className="form-message" css={[tw`hidden`, success && tw`block`]}>
                <img src="/images/envelope.svg" alt="Message sent envelope" />
              </div>
            </div>
          </div>
        </div>
      </Box>
    </Layout>
  );
};

export default SignUp;
