import { StatusCodes } from 'http-status-codes';
import { NextApiRequest, NextApiResponse } from 'next';
import { FeatureFlags } from 'types';
import { isSpacesAllowed } from 'utils/allowlist';

/**
 * Serverless function that provides a set of feature flags for a
 * given user.  It uses the `SPACES_ALLOWLIST` environment variable
 * (see the `isSpacesAllowed` function) to determine if a given user
 * has access to this feature.
 */
export default (req: NextApiRequest, res: NextApiResponse<FeatureFlags>) => {
  if (req.method === 'GET') {
    const {
      query: { id },
    } = req;
    res.status(StatusCodes.OK).json({
      isSpacesAllowed: isSpacesAllowed(Number(id)),
    });
  } else {
    res.status(StatusCodes.METHOD_NOT_ALLOWED);
  }
};
