import tw, { styled } from 'twin.macro';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import Button from 'components/primitives/Button';
import Layout from 'components/Layout';

const Donate: NextPage = () => {
  const { t } = useTranslation('common');
  return (
    <Layout title="Donate | JOGL">
      {/* Text and banner */}
      <section tw="-mt-6 background[linear-gradient(to right, #DEF6FC, #BCEAF7)] flex py-4 px-6 max-height[350px] sm:(py-10 px-12)">
        <div tw="flex w-1/2 items-center">
          <TitleDonate>
            {t('donate.title.1')}
            <strong>{t('donate.title.2')}</strong>
          </TitleDonate>
        </div>
        <div tw="font-size[4px] w-1/2 flex flex-wrap justify-end">
          <img tw="h-full w-full" src="/images/donate/research.svg" />
          <a tw="lg:mr-20" href="http://www.freepik.com">
            Designed by Freepik
          </a>
        </div>
      </section>

      {/* Donation explanation */}
      <TextDonate>
        <p>
          {t('donate.mission.1')}
          <strong>{t('donate.mission.2')}</strong>
          {t('donate.mission.3')}
        </p>
        <p>
          <br />
          {t('donate.use.1')}
          <strong>{t('donate.use.2')}</strong>
        </p>
      </TextDonate>

      {/* Payment cards */}
      <section tw="mx-8 md:mx-20">
        <div tw="flex flex-col items-center md:(flex flex-row justify-around)">
          <PayCard>
            <img tw="w-2/3 py-5 overflow-hidden" src={`/images/donate/IconPaiement.png`} />
            <a className="custom-dbox-popup" href="https://donorbox.org/donate-to-jogl">
              <BtnDonate>{t('donate.btn')}</BtnDonate>
            </a>
          </PayCard>
          <PayCard>
            <img tw="w-2/3 py-5 overflow-hidden" src={`/images/donate/IconCrypto.png`} />
            <a target="_blank" href="https://commerce.coinbase.com/checkout/1053d085-47f6-425f-b711-9972cac2907e">
              <BtnDonate>{t('donate.btn')}</BtnDonate>
            </a>
          </PayCard>
        </div>
      </section>
      <script type="text/javascript" defer src="https://donorbox.org/install-popup-button.js" />
      <script
        type="text/javascript"
        dangerouslySetInnerHTML={{
          __html: `window.DonorBox = { widgetLinkClassName: 'custom-dbox-popup' }`,
        }}
      />
    </Layout>
  );
};

const TitleDonate = styled.h1`
  ${tw`text-lg color[#2b5077] sm:text-2xl md:text-3xl lg:text-5xl`}
`;

const TextDonate = styled.section`
  ${tw`text-base py-10 px-6 text-gray-600 leading-7 sm:(text-lg px-12 py-16) md:text-xl lg:text-2xl`}
`;

const PayCard = styled.div`
  box-shadow: 0 2px 14px 0 rgba(0, 0, 0, 0.14);
  ${tw`flex flex-col items-center justify-center rounded-3xl bg-white w-11/12 mb-6 md:(mb-0 w-72 h-64) lg:(w-96 h-80)`};
`;

const BtnDonate = styled(Button)`
  ${tw`border-transparent font-medium background[#fb7351] mb-5 px-10 md:my-2`}
`;

export default Donate;

// export async function getStaticProps(context) {
//   return { props: {} };
// }
