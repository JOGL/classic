import Router from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { useContext, useState } from 'react';
import ProjectForm from 'components/Project/ProjectForm';
import Layout from 'components/Layout';
import { UserContext } from 'contexts/UserProvider';
import { useApi } from 'contexts/apiContext';
import { logEventToGA } from 'utils/analytics';

const ProjectCreate: NextPage = () => {
  const userContext = useContext(UserContext);
  const api = useApi();
  const { t } = useTranslation('common');
  const [newProject, setNewProject] = useState({
    title: '',
    short_title: '',
    logo_url: '',
    description: '',
    short_description: '',
    creator_id: Number(userContext.credentials.userId),
    status: 'draft',
    interests: [],
    skills: [],
    banner_url: '',
    is_private: false, // force all new projects to be private (TEMP @TOFIX)
  });
  const [sending, setSending] = useState(false);

  const handleChange = (key, content) => {
    setNewProject((prevProject) => ({ ...prevProject, [key]: content }));
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .post('/api/projects/', { project: newProject })
      .then((res) => {
        const userId = res.config.headers.userId;
        // record event to Google Analytics
        logEventToGA('create project', 'Project', `[${userId},${res.data.id}]`, { userId, itemId: res.data.id });
        // follow the object after having created it
        api.put(`/api/projects/${res.data.id}/follow`);
        // go to the created project edition page
        Router.push(`/project/${res.data.id}/edit`);
      })
      .catch((err) => {
        console.error("Couldn't post new project", err);
        setSending(false);
        // if error response data is that shortTitle is already taken, show message in an alert warning
        if (err.response.data.data === 'ShortTitle is already taken') {
          alert(t('err-4006'));
        }
      });
  };
  return (
    <Layout title={`${t('project.create.title')} | JOGL`}>
      <div className="projectCreate container-fluid">
        <h1>{t('project.create.title')}</h1>
        <ProjectForm
          mode="create"
          project={newProject}
          handleChange={handleChange}
          handleSubmit={handleSubmit}
          sending={sending}
        />
      </div>
    </Layout>
  );
};

export default ProjectCreate;
