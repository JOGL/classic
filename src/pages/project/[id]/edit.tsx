import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
// import { FC, useMemo, useState } from 'react';
import { useState } from 'react';
import Box from 'components/Box';
import Carousel from 'components/Carousel';
import ChallengeMiniCard from 'components/Challenge/ChallengeMiniCard';
import SpaceMiniCard from 'components/Space/SpaceMiniCard';
import Grid from 'components/Grid';
import Layout from 'components/Layout';
import MembersList from 'components/Members/MembersList';
import NeedCard from 'components/Need/NeedCard';
import NeedCreate from 'components/Need/NeedCreate';
import Button from 'components/primitives/Button';
import P from 'components/primitives/P';
import ProjectForm from 'components/Project/ProjectForm';
import Alert from 'components/Tools/Alert';
import DocumentsManager from 'components/Tools/Documents/DocumentsManager';
// import Loading from 'components/Tools/Loading';
import ManageExternalLink from 'components/Tools/ManageExternalLink';
import WebHooks from 'components/Tools/Webhooks/Webhooks';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
// import useGet from 'hooks/useGet';
import useNeeds from 'hooks/useNeeds';
import useUser from 'hooks/useUser';
// import { Program } from 'types';
import { Project } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import { NavTab, TabListStyle, TabListStyleNoSticky } from 'components/Tabs/TabsStyles';
import Trans from 'next-translate/Trans';
// import SpinLoader from 'components/Tools/SpinLoader';
import { logEventToGA } from 'utils/analytics';

interface Props {
  project: Project;
}
const ProjectEdit: NextPage<Props> = ({ project: projectProp }) => {
  const [project, setProject] = useState(projectProp);
  const router = useRouter();
  const [sending, setSending] = useState(false);
  const [updatedProject, setUpdatedProject] = useState(undefined);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [errors, setErrors] = useState(undefined);
  const [loadPending, setLoadPending] = useState(false);
  const [membersTab, setMembersTab] = useState(undefined);
  const [nbOfPendingMembers, setNbOfPendingMembers] = useState(0);
  const urlBack = `/project/${router.query.id}/${project?.short_title}`;
  const { dataNeeds, needsRevalidate } = useNeeds('projects', parseInt(router.query.id as string));
  const { showModal, closeModal } = useModal();
  const api = useApi();
  const { user } = useUser();
  const { t } = useTranslation('common');

  const handleChange = (key, content) => {
    setProject((prevProject) => ({ ...prevProject, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedProject((prevUpdatedProject) => ({ ...prevUpdatedProject, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .patch(`/api/projects/${project.id}`, { project: updatedProject })
      .then(() => {
        setSending(false);
        setUpdatedProject(undefined); // reset updated program component
        setHasUpdated(true); // show update confirmation message
        setTimeout(() => {
          setHasUpdated(false);
        }, 3000); // hide confirmation message after 3 seconds
      })
      .catch(() => {
        setSending(false);
      });
  };
  const deleteProj = () => {
    api
      .delete(`api/projects/${project.id}`)
      .then(() => {
        // record event to Google Analytics
        logEventToGA('delete project', 'Project', `[${user.id},${project.id}]`, {
          userId: user.id,
          itemId: project.id,
        });
        closeModal(); // close modal
        router.push('/search/projects');
      })
      .catch((error) => {
        setErrors(error.toString());
      });
  };
  const onChallengeDelete = (id) => {
    const newProject = { ...project, challenges: project.challenges.filter((challenge) => challenge.id !== id) };
    setProject(newProject); // update fields as user changes them
    // mutateProject({ data: newProject }, false);
  };

  const onSpaceDelete = (id) => {
    const newProject = {
      ...project,
      affiliated_spaces: project.affiliated_spaces[0]?.filter((space) => space.id !== id),
    };
    setProject(newProject); // update fields as user changes them
    // mutateProject({ data: newProject }, false);
  };

  const delBtnTitleId = project?.members_count > 1 ? 'project.archive.title' : 'project.delete.title';
  const delBtnTextId = project?.members_count > 1 ? 'project.archive.text' : 'project.delete.text';

  const errorMessage = errors?.includes('err-') ? t(errors) : errors;

  // Tabs elements: tabs list & handleTabsChange
  // Explained on pages-project-index file
  const tabs = [
    { value: 'basic_info', translationId: 'entity.tab.basic_info' },
    { value: 'members', translationId: 'entity.tab.members' },
    { value: 'needs', translationId: 'entity.tab.needs' },
    { value: 'documents', translationId: 'entity.tab.documents' },
    { value: 'advanced', translationId: 'entity.tab.advanced' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/project/${project.id}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${project?.title} | JOGL`} desc={project?.short_description} img={project?.banner_url}>
      {/* <Loading active={!project}> */}
      <div className="projectEdit" tw="container mx-auto px-4">
        <h1>{t('project.edit.title')}</h1>
        <Link href={urlBack}>
          <a>
            {/* go back link */}
            <ArrowLeft size={15} title="Go back" />
            {t('project.edit.back')}
          </a>
        </Link>
        {/* TODO switch to custom nav 'same as program' */}
        <Tabs
          defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>{t(item.translationId)}</NavTab>
            ))}
          </TabListStyle>
          <TabPanels tw="justify-center">
            {/* Basic info tab */}
            <TabPanel>
              <ProjectForm
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                mode="edit"
                project={project}
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* Members tab */}
            <TabPanel>
              <Tabs
                defaultIndex={0}
                onChange={(id) => {
                  id === 1 && setLoadPending(true);
                  setMembersTab(id);
                }}
              >
                <TabListStyleNoSticky>
                  <NavTab>{t('entity.tab.activeMembers')}</NavTab>
                  <NavTab>
                    {t('entity.tab.pendingMembers')} ({nbOfPendingMembers})
                  </NavTab>
                </TabListStyleNoSticky>
                <TabPanels tw="justify-center">
                  <TabPanel>
                    {' '}
                    {router.query.id ? (
                      <MembersList
                        itemType="projects"
                        itemId={parseInt(router.query.id as string)}
                        isOwner={project?.is_owner}
                        setNbOfPendingMembers={setNbOfPendingMembers}
                      />
                    ) : // eslint-disable-next-line @rushstack/no-null
                    null}
                  </TabPanel>
                  <TabPanel>
                    {' '}
                    {router.query.t && loadPending && (
                      <MembersList
                        itemType="projects"
                        itemId={parseInt(router.query.id as string)}
                        isOwner={project?.is_owner}
                        setNbOfPendingMembers={setNbOfPendingMembers}
                        onlyPending
                      />
                    )}
                  </TabPanel>
                </TabPanels>
              </Tabs>
            </TabPanel>
            {/* Needs tab */}
            <TabPanel>
              {project && (
                <>
                  {!!user && <NeedCreate projectId={project.id} refresh={needsRevalidate} />}
                  <Grid tw="py-4">
                    {dataNeeds &&
                      [...dataNeeds]
                        ?.reverse()
                        .map((need, i) => (
                          <NeedCard
                            key={i}
                            title={need.title}
                            skills={need.skills}
                            resources={need.ressources}
                            hasSaved={need.has_saved}
                            id={need.id}
                            postsCount={need.posts_count}
                            membersCount={need.members_count}
                            publishedDate={need.created_at}
                            dueDate={need.end_date}
                            status={need.status}
                          />
                        ))}
                  </Grid>
                </>
              )}
            </TabPanel>
            {/* Documents tab */}
            <TabPanel>
              {project && (
                <DocumentsManager
                  documents={project.documents}
                  isAdmin={project.is_admin}
                  itemId={project.id}
                  itemType="projects"
                />
              )}
            </TabPanel>
            {/* Advanced tab */}
            <TabPanel>
              {/* <h5>{t('attach.myproject.title')}</h5>
              <Button
                onClick={() => {
                  showModal({
                    children: (
                      <LinkChallengeModal project={project} closeModal={closeModal} setProject={setProject} t={t} />
                    ),
                    title: t('attach.myproject.title'),
                    maxWidth: '50rem',
                  });
                }}
              >
                {t('attach.myproject.btn')}
              </Button> */}
              {/* Manage attached challenges */}

              {/* Many challenges happen on JOGL. Your project might fit one. You should consider trying to participate in challenge that you can find here < /explore/challenges.>
              Does your project belong to an organization that is already on JOGL? Well don't wait!! If so and you don't see it here, find their space <a /explore/space /> and submit your affiliation request through their project tab */}
              {project && project.challenges.length !== 0 && (
                <>
                  <Box>
                    <h4>{t('project.info.participatingToChallenges')}</h4>
                  </Box>
                  <Carousel spaceX={12} tw="pt-3">
                    {project.challenges.map((challenge, index) => (
                      <ChallengeMiniCard
                        key={index}
                        icon={challenge.banner_url_sm}
                        title={challenge.title}
                        shortTitle={challenge.short_title}
                        status={challenge.project_status}
                        projectId={project.id}
                        projectChallengeId={challenge.id}
                        onChallengeDelete={onChallengeDelete}
                        isEditCard
                      />
                    ))}
                  </Carousel>
                  <hr />
                </>
              )}
              {/* Manage affiliated spaces */}
              {project && project.affiliated_spaces.length !== 0 && (
                <>
                  <Box pt={5}>
                    <h4>{t('project.info.affiliatedToSpaces')}</h4>
                  </Box>
                  <Carousel spaceX={12}>
                    {project.affiliated_spaces.map((space, index) => (
                      <SpaceMiniCard
                        key={index}
                        icon={space[0].banner_url_sm}
                        title={space[0].title}
                        shortTitle={space[0].short_title}
                        status={space[0].affiliation_status}
                        affiliateId={project.id}
                        spaceId={space[0].id}
                        onSpaceDelete={onSpaceDelete}
                        isEditCard
                      />
                    ))}
                  </Carousel>
                  <hr tw="mt-8" />
                </>
              )}

              <ManageExternalLink itemType="projects" itemId={router.query.id} />
              <hr tw="mt-8" />

              <h4>{t('hook.setup')}</h4>
              <Trans
                i18nKey="common:hook.explanation"
                components={[
                  <p className="hookExplain" />,
                  <a href="https://api.slack.com/messaging/webhooks" target="_blank" rel="noopener noreferrer" />,
                ]}
                values={{ tutorial: t('hook.tutorial') }}
              />
              <div className="hooksContainer">{project && <WebHooks itemId={router.query.id} />}</div>
              <hr tw="mt-8" />

              <h4>{t('project.advancedParam')}</h4>
              <div className="deleteBtns">
                {project && project?.members_count > 1 && <p>{t('project.delete.explain')}</p>}
                {project && (
                  <Button
                    onClick={() => {
                      showModal({
                        children: (
                          <>
                            {errors && <Alert type="danger" message={errorMessage} />}
                            <P fonSize="1rem">{t(delBtnTextId)}</P>
                            <div tw="inline-flex space-x-3">
                              <Button btnType="danger" onClick={deleteProj}>
                                {t('general.yes')}
                              </Button>
                              <Button onClick={closeModal}>{t('general.no')}</Button>
                            </div>
                          </>
                        ),
                        title: t(delBtnTitleId),
                        maxWidth: '30rem',
                      });
                    }}
                    btnType="danger"
                  >
                    {t(delBtnTitleId)}
                  </Button>
                )}
                {project && project.members_count > 1 && (
                  <Button btnType="danger" disabled tw="ml-3">
                    {t('project.delete.title')}
                  </Button>
                )}
              </div>
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

// interface PropsModal {
//   project: Project;
//   closeModal: () => void;
//   setProject: React.Dispatch<any>;
//   t: any;
// }
// const LinkChallengeModal: FC<PropsModal> = ({ project, closeModal, setProject, t }) => {
//   const alreadyLinkedChallenges: any[] = project?.challenges ? project?.challenges : [];
//   const { data: dataChallenges } = useGet('/api/challenges');
//   const [selectedChallenge, setSelectedChallenge] = useState<
//     undefined | { id: number; is_member: boolean; program: Program }
//   >();
//   const [sending, setSending] = useState(false);
//   const [requestSent, setRequestSent] = useState(false);
//   const [isButtonDisabled, setIsButtonDisabled] = useState(true);
//   const api = useApi();

//   // Filter the projects that are already in this challenge so you don't add it twice!
//   const filteredChallenges = useMemo(() => {
//     if (dataChallenges) {
//       return (
//         dataChallenges
//           // .filter((challenge) => challenge.project_status === 'accepting')
//           .filter((challenge) => {
//             // Check if my project is found in alreadyLinkedProjects
//             const isChallengeAlreadyLinked = alreadyLinkedChallenges.find((alreadyLinkedChallenge) => {
//               return alreadyLinkedChallenge.id === challenge.id;
//             });
//             // We keep only the ones that are not present
//             return !isChallengeAlreadyLinked;
//           })
//       );
//     }
//   }, [dataChallenges, alreadyLinkedChallenges]);

//   const onSubmit = async (e) => {
//     e.preventDefault();
//     setSending(true);
//     // Link this project to the challenge then mutate the cache of the projects from the parent prop
//     if (selectedChallenge?.id) {
//       await api
//         .put(`/api/challenges/${selectedChallenge?.id}/projects/${project.id}`)
//         .catch(() =>
//           console.error(`Could not PUT/link challengeId=${selectedChallenge?.id} with project projectId=${project.id}`)
//         );
//       setSending(false);
//       setRequestSent(true);
//       setIsButtonDisabled(true);
//       // mutateProject({ data: { ...project, challenges: [...project.challenges, selectedChallenge] } });
//       const newProject = { ...project, challenges: [...project.challenges, selectedChallenge] };
//       setProject(newProject); // update challenges list
//       !selectedChallenge.is_member && api.put(`/api/challenges/${selectedChallenge?.id}/join`); // join the challenge if user is not member already
//       api.put(`/api/challenges/${selectedChallenge?.id}/follow`); // then follow it
//       api.put(`/api/programs/${selectedChallenge?.program.id}/follow`); // and follow its program
//       setTimeout(() => {
//         // close modal after 3.5sec
//         closeModal();
//       }, 3500);
//     }
//   };
//   const onProjectSelect = (e) => {
//     setSelectedChallenge(filteredChallenges.find((item) => item.id === parseInt(e.target.value)));
//     setIsButtonDisabled(false);
//   };

//   return (
//     <div>
//       {filteredChallenges && filteredChallenges.length > 0 ? (
//         <form style={{ textAlign: 'left' }}>
//           {filteredChallenges
//             // filter to only get challenges with status accepting, and challenges that are attached to a program (id ≠ -1)
//             .filter(({ status, program }) => status === 'accepting' && program.id !== -1)
//             .map((project, index) => (
//               <div className="form-check" key={index} style={{ height: '60px' }}>
//                 <input
//                   type="radio"
//                   className="form-check-input"
//                   name="exampleRadios"
//                   id={`project-${index}`}
//                   value={project.id}
//                   onChange={onProjectSelect}
//                 />
//                 <label className="form-check-label" htmlFor={`project-${index}`}>
//                   {project.title}
//                   <br />
//                   <Box opacity=".6">
//                     {t('entity.info.program_title')}"{project.program.title}"
//                   </Box>
//                 </label>
//               </div>
//             ))}
//           <div className="btnZone">
//             <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-2">
//               <>
//                 {sending && <SpinLoader />}
//                 {t('attach.myproject.btn')}
//               </>
//             </Button>
//             {requestSent && <Alert type="success" message={t('attach.project.success')} />}
//           </div>
//         </form>
//       ) : (
//         <Loading />
//       )}
//     </div>
//   );
// };

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/projects/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch project with id=${query.id}`, err));
  // Check if it got the project and if the user is admin
  if (res?.data?.is_admin) return { props: { project: res.data } };
  return { redirect: { destination: '/search/projects', permanent: false } };
}

export default ProjectEdit;
