import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
// import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
// import 'react-vertical-timeline-component/style.min.css';
import Box from 'components/Box';
import Feed from 'components/Feed/Feed';
import Grid from 'components/Grid';
import Layout from 'components/Layout';
import NeedCard from 'components/Need/NeedCard';
import NeedCreate from 'components/Need/NeedCreate';
import ProjectHeader from 'components/Project/ProjectHeader';
import DocumentsManager from 'components/Tools/Documents/DocumentsManager';
import InfoHtmlComponent from 'components/Tools/Info/InfoHtmlComponent';
import Loading from 'components/Tools/Loading';
import NoResults from 'components/Tools/NoResults';
import useNeeds from 'hooks/useNeeds';
import useUserData from 'hooks/useUserData';
import { Project } from 'types';
import { getApiFromCtx } from 'utils/getApi';
import { Tabs, TabPanels } from '@reach/tabs';
import { NavTab, TabListStyle, TabPanelFeed, TabPanelResp } from 'components/Tabs/TabsStyles';
import InfoDefaultComponent from 'components/Tools/Info/InfoDefaultComponent';
import { linkify, displayObjectDate, formatNumberToShowCommas } from 'utils/utils';
import Trans from 'next-translate/Trans';
// import useGet from 'hooks/useGet';

interface Props {
  project: Project;
}

const ProjectDetails: NextPage<Props> = ({ project }) => {
  // const ProjectDetails: NextPage<Props> = ({ project: projectProp }) => {
  //   const { data: project } = useGet(`/api/projects/${projectProp.id}`, { initialData: projectProp });
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const router = useRouter();
  const [isMember, setIsMember] = useState(false);
  const { dataNeeds, needsRevalidate } = useNeeds('projects', project?.id);
  const grant_info = project.grant_info?.split(', ');
  const needTabRef = useRef();

  // Test data for displaying vertical timeline
  // const testTimelineData = [
  //   {
  //     id: 'prototyping',
  //     title: 'Prototyping',
  //     color: 'green',
  //     date_start: '2020-10-25',
  //     date_end: '2020-12-10',
  //     moments: [
  //       {
  //         id: '1',
  //         title: 'First community meeting',
  //         description: '...',
  //         date: '2020-10-25',
  //       },
  //       {
  //         id: '3',
  //         title: 'Start design',
  //         description: '...',
  //         date: '2020-11-14',
  //       },
  //     ],
  //   },
  //   {
  //     id: 'testing',
  //     title: 'Testing',
  //     color: 'blue',
  //     date_start: '2020-12-10',
  //     date_end: '2021-02-05',
  //     moments: [
  //       {
  //         id: '4',
  //         title: 'Prototype testing',
  //         description: '...',
  //         date: '2021-01-08',
  //       },
  //     ],
  //   },
  // ];

  const showNeedsTab = (project.needs_count !== 0 && (!project.is_admin || !userData)) || project.is_admin;

  // list of tabs
  const tabs = [
    { value: 'feed', translationId: 'entity.tab.news' },
    { value: 'about', translationId: 'general.tab.about' },
    // hide needs tab if user is not admin and there are no needs
    ...(showNeedsTab ? [{ value: 'needs', translationId: 'entity.tab.needs' }] : []),
    // hide documents tab if user is not admin and there are no docs
    ...((project?.documents.length !== 0 && (!project?.is_admin || !userData)) || project.is_admin
      ? [{ value: 'documents', translationId: 'entity.tab.documents' }]
      : []),
    // "..." show information with conditions, here with tab, without having to show all prop
  ];

  const defaultIndex = router.query.t
    ? // if router has tab param, make default Tab index be the t query (url finishing with ?t=index)
      tabs.findIndex((t) => t.value === router.query.t)
    : // else make defaultIndex be the about tab
      tabs.findIndex((t) => t.value === 'about');

  // Things to do as we change tabs
  const handleTabsChange = (index) => {
    // scroll back up to <Tabs> element
    const element = document.querySelector('[data-reach-tabs]'); // get <Tabs> element
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight; // calculate its top value and remove 80 of offset
    window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to it
    // soft replace url t param (tab)
    router.push(`/project/${project.id}?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  useEffect(() => {
    (project.is_member || router.query.t === 'feed') && setIsMember(true);
  }, [project.is_member]);

  // Dynamic SEO meta tags
  return (
    <Layout
      title={project?.title && `${project.title} | JOGL`}
      desc={project?.short_description}
      img={project?.banner_url || '/images/default/default-project.jpg'}
      noIndex={project?.status === 'draft'}
    >
      {project && (
        <div className="projectDetails container-fluid">
          <ProjectHeader project={project} forceClickNeedTab={() => needTabRef?.current?.click()} />
          <Tabs defaultIndex={defaultIndex} onChange={handleTabsChange}>
            <TabListStyle>
              {tabs.map((item, key) => (
                <NavTab {...(showNeedsTab && item.value === 'needs' && { ref: needTabRef })} key={key}>
                  {t(item.translationId)}
                </NavTab>
              ))}
            </TabListStyle>
            <TabPanels tw="justify-center">
              {/* Feed tab */}
              <TabPanelFeed>
                {project.feed_id && (
                  <Feed
                    feedId={project.feed_id}
                    // display post create component if you are member of the project
                    allowPosting={project.is_member}
                    isAdmin={project.is_admin}
                    needToJoinMsg={project.is_private}
                  />
                )}
              </TabPanelFeed>
              {/* About tab */}
              <TabPanelResp>
                <div tw="mt-10 sm:mt-12">
                  <Box width={['100%', undefined, undefined, '70%']} margin="auto">
                    {/* Show other project stats & infos */}
                    <div tw="mb-8 py-6 px-2 rounded bg-gray-100">
                      <ul tw="space-y-2 mb-0">
                        <li>
                          <span tw="underline">{t('entity.info.short_name')}</span>: #{project.short_title}
                        </li>
                        <li>
                          <span tw="underline">{t('entity.info.status.title')}</span>:
                          {' ' + t(`entity.info.status.${project.status}`)}
                        </li>
                        {project.maturity && (
                          <li>
                            <span tw="underline">{t('project.maturity.title')}</span>:
                            {' ' + t(`project.maturity.${project.maturity}`)}
                          </li>
                        )}
                        {project.is_looking_for_collaborators && (
                          <li>
                            <span tw="underline">{t('project.form.looking_for_collab_title')}</span>:&nbsp;✅
                          </li>
                        )}
                        {grant_info && (
                          <li>
                            <span tw="underline">{t('project.info.grantInfoTitle')}</span>
                            <Trans
                              i18nKey="common:project.info.grantInfoDescription"
                              components={[
                                <span />,
                                <strong />,
                                // transform name to link if last param is link
                                grant_info[3] ? <a href={grant_info[3]} target="_blank" /> : <strong />,
                                <strong />,
                              ]}
                              values={{
                                grantAmount: formatNumberToShowCommas(grant_info[1]) + '€',
                                grantNameRound: grant_info[0],
                                grantDate: displayObjectDate(new Date(grant_info[2])),
                              }}
                            />
                          </li>
                        )}
                      </ul>
                    </div>
                    {/* display full description or short description conditionally */}
                    {project.description && project.description !== '<p><br></p>' ? (
                      <InfoHtmlComponent content={project.description} />
                    ) : (
                      <InfoDefaultComponent content={linkify(project.short_description)} containsHtml />
                    )}
                    {project.desc_elevator_pitch && <h4>{t('entity.info.desc_elevator_pitch')}</h4>}
                    <InfoHtmlComponent content={project.desc_elevator_pitch} />
                    {project.desc_contributing && <h4>{t('entity.info.desc_contributing')}</h4>}
                    <InfoHtmlComponent content={project.desc_contributing} />
                    {project.desc_problem_statement && <h4>{t('entity.info.desc_problem_statement')}</h4>}
                    <InfoHtmlComponent content={project.desc_problem_statement} />
                    {project.desc_objectives && <h4>{t('entity.info.desc_objectives')}</h4>}
                    <InfoHtmlComponent content={project.desc_objectives} />
                    {project.desc_state_art && <h4>{t('entity.info.desc_state_art')}</h4>}
                    <InfoHtmlComponent content={project.desc_state_art} />
                    {project.desc_progress && <h4>{t('entity.info.desc_progress')}</h4>}
                    <InfoHtmlComponent content={project.desc_progress} />
                    {project.desc_stakeholder && <h4>{t('entity.info.desc_stakeholder')}</h4>}
                    <InfoHtmlComponent content={project.desc_stakeholder} />
                    {project.desc_impact_strat && <h4>{t('entity.info.desc_impact_strat')}</h4>}
                    <InfoHtmlComponent content={project.desc_impact_strat} />
                    {project.desc_ethical_statement && <h4>{t('entity.info.desc_ethical_statement')}</h4>}
                    <InfoHtmlComponent content={project.desc_ethical_statement} />
                    {project.desc_sustainability_scalability && (
                      <h4>{t('entity.info.desc_sustainability_scalability')}</h4>
                    )}
                    <InfoHtmlComponent content={project.desc_sustainability_scalability} />
                    {project.desc_communication_strat && <h4>{t('entity.info.desc_communication_strat')}</h4>}
                    <InfoHtmlComponent content={project.desc_communication_strat} />
                    {project.desc_funding && <h4>{t('entity.info.desc_funding')}</h4>}
                    <InfoHtmlComponent content={project.desc_funding} />
                    {/* For now, always render the timeline, but eventually only render it if a project has a timeline */}
                    {/* {true && (
                    <Box mt={4}>
                      <H2 textAlign="center">Project Timeline</H2>
                      <VerticalTimeline
                        className={css`
                          .vertical-timeline-element-subtitle {
                            margin-top: 5px !important; //needed to override about's h4 styling
                            font-size: 1.2rem;
                          }
                          &::before {
                            background-color: rgba(87, 87, 87, 0.344);
                          }
                        `}
                      >
                        {testTimelineData.map((el) => {
                          // map through all phases/periods
                          const phase = testTimelineData.find(({ id }) => id === el.id); // set phase/period object
                          return (
                            <> */}
                    {/* Phase box */}
                    {/* <TimelinePhaseBox color={phase.color}>
                                {phase.title}
                                <br />
                                {`${phase.date_start} - ${phase.date_end}`}
                              </TimelinePhaseBox> */}
                    {/* map through phase moments/events */}
                    {/* {phase.moments.map((el, idx) => (
                                <VerticalTimelineElement
                                  key={idx} // change the key when timeline is added to API
                                  className="vertical-timeline-element--work"
                                  contentStyle={{ borderTop: `3px solid ${phase.color}` }}
                                  contentArrowStyle={{ borderRight: `7px solid ${phase.color}` }}
                                  date={el.date}
                                  iconStyle={{ background: phase.color, color: '#fff' }}
                                  // icon={''} (no icon for now)
                                >
                                  <h3 className="vertical-timeline-element-title">{el.title}</h3>
                                  <p>{el.description}</p>
                                </VerticalTimelineElement>
                              ))}
                            </>
                          );
                        })}
                      </VerticalTimeline>
                    </Box>
                  )} */}
                  </Box>
                </div>
              </TabPanelResp>
              {/* Needs tab */}
              {showNeedsTab && (
                <TabPanelResp>
                  {!!project.is_admin && <NeedCreate projectId={project.id} refresh={needsRevalidate} />}
                  <Grid>
                    {!dataNeeds ? (
                      <Loading />
                    ) : dataNeeds.length === 0 ? (
                      <NoResults type="need" />
                    ) : (
                      [...dataNeeds]
                        .reverse()
                        .map((need, i) => (
                          <NeedCard
                            key={i}
                            title={need.title}
                            skills={need.skills}
                            resources={need.ressources}
                            hasSaved={need.has_saved}
                            id={need.id}
                            postsCount={need.posts_count}
                            membersCount={need.members_count}
                            publishedDate={need.created_at}
                            dueDate={need.end_date}
                            status={need.status}
                          />
                        ))
                    )}
                  </Grid>
                </TabPanelResp>
              )}
              {/* Documents tab */}
              <TabPanelResp>
                <div>
                  <DocumentsManager
                    isAdmin={project.is_admin}
                    documents={project.documents}
                    itemId={project.id}
                    itemType="projects"
                  />
                </div>
              </TabPanelResp>
            </TabPanels>
          </Tabs>
        </div>
      )}
    </Layout>
  );
};
// const TimelinePhaseBox = styled.div(({ color }) => [
//   tw`text-center rounded bg-white border-solid relative p-4 text-xl font-bold`,
//   `border-color: ${color};`,
// ]);

export const getServerSideProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/projects/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { props: { project: res.data } };
  return { redirect: { destination: '/search/projects', permanent: false } };
};

export default ProjectDetails;
