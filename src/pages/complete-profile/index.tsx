import { useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import Layout from 'components/Layout';
import Loading from 'components/Tools/Loading';
// Form objects
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormInterestsComponent from 'components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from 'components/Tools/Forms/FormResourcesComponent';
import FormDropdownComponent from 'components/Tools/Forms/FormDropdownComponent';
import FormTextAreaComponent from 'components/Tools/Forms/FormTextAreaComponent';
import TitleInfo from 'components/Tools/TitleInfo';
import Select, { createFilter } from 'react-select';
import { FixedSizeList as List } from 'react-window';
import UserCard from 'components/User/UserCard';
import Box from 'components/Box';
import { useApi } from 'contexts/apiContext';
import useUserData from 'hooks/useUserData';
import { getCountriesAndCitiesList } from 'utils/utils';
import SpinLoader from 'components/Tools/SpinLoader';
import { logEventToGA } from 'utils/analytics';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
// Validator
import FormValidator from 'components/Tools/Forms/FormValidator';
import { toAlphaNum } from 'components/Tools/Nickname';
import userProfileFormRules from 'components/User/userProfileFormRules.json';
// Onboarding
import { useModal } from 'contexts/modalContext';
import { showCompleteProfileModal } from 'utils/onboarding/index';
import styled from 'utils/styled';

const Container = styled(Box)`
  box-shadow: ${(p) => p.theme.shadows.default};
  border-radius: ${(p) => p.theme.radii.xl};
  border: 1px solid ${(p) => p.theme.colors.greys['200']};
  background: white;
`;

const CompleteProfile = () => {
  const validator = new FormValidator(userProfileFormRules);
  const [sending, setSending] = useState(false);
  const [suggestedUser, setSuggestedUser] = useState(undefined);
  const [user, setUser] = useState(undefined);
  const [hasSetUserData, setHasSetUserData] = useState(false);
  const { t } = useTranslation('common');
  const modal = useModal();
  const sawCompleteProfileModal =
    typeof window !== 'undefined' && JSON.parse(localStorage.getItem('sawCompleteProfileModal'));
  const api = useApi();
  const router = useRouter();
  const { userData } = useUserData();
  const suggestedUserId = process.env.NODE_ENV !== 'production' ? 1 : 191;
  // if env is dev, suggestuserId is 1, else if it's prod, make it 191 which is the JOGL user on app.jogl.io
  const [countriesCitiesData, setCountriesCitiesData] = useState([]);
  const [countryNotInList, setCountryNotInList] = useState(false);
  const [countriesList, setCountriesList] = useState([]);
  const [citiesList, setCitiesList] = useState([]);
  const [stateValidation, setStateValidation] = useState({});
  useEffect(() => {
    api
      .get(`/api/users/${suggestedUserId}`) // get jogl user
      .then((res) => setSuggestedUser(res.data))
      .catch((err) => console.error(`Couldn't GET user id=${suggestedUserId}`, err));
  }, []);

  // get userData once only
  useEffect(() => {
    !hasSetUserData && userData && setUser(userData);
    userData && setHasSetUserData(true);
  }, [userData]);

  useEffect(() => {
    // [Onboarding][1][New user] -  Launch complete-profile onboarding modal, only on 1st signin
    if (router.query?.launch_onboarding === 'true') {
      localStorage.setItem('isOnboarding', 'true');
      // so modal is shown only once, not each time we reload the complete-profile, has to be localStorage instead of state.
      if (sawCompleteProfileModal !== true) {
        setTimeout(() => {
          showCompleteProfileModal(modal, t);
        }, 1500);
      }
    }
  }, [sawCompleteProfileModal, router.query]);

  const handleChange = (key, content) => {
    setUser((prevUser) => ({ ...prevUser, [key]: content }));
    /* Validators start */
    const state = {};
    if (key === 'nickname') {
      state[key] = toAlphaNum(content);
    } else state[key] = content;
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
  };

  // handling change of year separately, cause we're saving it in a different format than how it's shown in frontend
  const handleChangeYear = (key, content) => {
    const formattedBirthDate = new Date(content, 0) // make date be January 1 of the selected year (where content is the selected year)
      .toLocaleDateString('en-US'); // transform date into a YYYY-MM-DD format
    // we do this because for now we only need the year, but maybe in the future we'll ask the user the full date
    setUser((prevUser) => ({ ...prevUser, ['birth_date']: formattedBirthDate }));
    /* Validators start */
    const state = {};
    state['birth_date'] = content.toString(); // as value is a number, transform it into a string, needed for the form validation
    const validation = validator.validate(state);
    if (validation['birth_date'] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_birth_date`] = validation['birth_date'];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };

  const handleCountryChange = (key, content) => {
    var newKey,
      newContent = '';
    if (content.action === 'set-value' || content.action === 'select-option') {
      newKey = ['country'];
      newContent = key.value;
    } else if (content.action === 'clear') {
      newKey = ['country'];
      newContent = '';
    }
    setUser((prevUser) => ({ ...prevUser, [newKey]: newContent }));
    /* Validators start */
    const state = {};
    state['country'] = key ? key.value : undefined;
    const validation = validator.validate(state);
    if (validation['country'] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_country`] = validation['country'];
      setStateValidation(newStateValidation);
    }
  };

  const handleCityChange = (key, content) => {
    var newKey,
      newContent = '';
    if (content.action === 'set-value' || content.action === 'select-option') {
      newKey = ['city'];
      newContent = key.value;
    } else if (content.action === 'clear') {
      newKey = ['city'];
      newContent = '';
    }
    setUser((prevUser) => ({ ...prevUser, [newKey]: newContent }));
  };

  const handleSubmit = (event) => {
    // [Onboarding][1] - Trigger point: first onboarding flow for new user
    const isNewUser = router.query?.launch_onboarding === 'true';
    if (isNewUser) {
      localStorage.setItem('isOnboarding', 'true');
    }
    if (event) {
      event.preventDefault();
    }
    /* Validators control before submit */
    const validation = validator.validate(user);
    if (validation.isValid) {
      setSending(true);
      api
        .patch(`/api/users/${user.id}`, { user })
        .then(() => {
          // send event to google analytics
          logEventToGA('completed profile', 'User', `[${user.id}]`, { userId: user.id });
          router.push({
            pathname: `/user/${user.id}`,
            query: { new_user: isNewUser },
          });
        })
        .catch((err) => {
          setSending(false);
          console.log('valid ok but did enter in catch err api');
          console.error(`Couldn't PATCH user id=${user.id}`, err);
        });
    } else {
      setSending(false);
      let firstError = true;
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        // map through all the object of validation
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 100; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };

  const handleCancel = () => {
    localStorage.setItem('sawOnboardingFeature', 'true');
    localStorage.setItem('isOnboarding', 'false');
    router.push('/');
  };

  getCountriesAndCitiesList(
    countriesCitiesData,
    setCountriesCitiesData,
    countryNotInList,
    setCountryNotInList,
    setCountriesList,
    setCitiesList,
    user?.country
  );

  const MenuList = ({ children, maxHeight }) => {
    return (
      <List height={maxHeight} itemCount={children.length} itemSize={30}>
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  };

  // Same country and city style as in edit user page
  const countriesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
    control: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      border: valid_country
        ? valid_country.isInvalid
          ? '1px solid #dc3545'
          : !valid_country.isInvalid && '1px solid #28a745'
        : '1px solid lightgrey',
    }),
  };
  const citiesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
  };

  // List of compulsary fields
  const disabledBtns =
    !user?.first_name ||
    !user?.last_name ||
    !user?.nickname ||
    !user?.birth_date ||
    !user?.gender ||
    !user?.short_bio ||
    !user?.country ||
    !user?.affiliation ||
    user?.interests.length === 0 ||
    user?.skills?.length === 0;

  // List of fields needing validation (all but bio & city & resources)
  const {
    valid_first_name,
    valid_last_name,
    valid_nickname,
    valid_birth_date,
    valid_gender,
    valid_short_bio,
    valid_country,
    valid_affiliation,
    valid_interests,
    valid_skills,
  } = stateValidation || '';

  const yearsList = [];
  const currentYear = new Date().getFullYear();
  for (let y = currentYear - 15; y >= 1900; y--) {
    yearsList.push(y);
  }

  return (
    <Layout title={`${t('completeProfile.title')} | JOGL`} noHeaderFooter>
      <Container tw="lg:mt-36 m-auto width[95%] sm:width[85%] lg:width[70%]">
        <div
          className="form-header text-center"
          tw="pt-6 pb-5 lg:(pb-8 pt-10) background[#2987cd] rounded-t-2xl text-white"
        >
          <h2 className="form-title" id="signModalLabel">
            {t('user.profile.edit.complete')}
          </h2>
        </div>
        {/* <div tw="flex border-b border-t-0 border-r-0 border-l-0 border-gray-200 border-solid">
          <div tw="ml-6 border-b-4 border-t-0 border-r-0 border-l-0 border-black border-solid">
            <h3 tw="text-xl"> {t('user.profile.edit.title')}</h3>
          </div>
        </div> */}
        <div className="form-body" tw="p-6">
          <Loading active={!user} height="200px">
            <form onSubmit={handleSubmit}>
              {/* 1. Personal info */}
              <div className="personalInformation">
                <TitleInfo
                  isFormSubpartTitle
                  title={t('user.profile.personal_infos')}
                  tooltipMessage={t('user.profile.personal_infos_tooltip')}
                />
                <div tw="border border-gray-200 border-solid rounded-md p-4">
                  {/* First infos: img left, other core infos right */}
                  <div tw="flex flex-col lg:flex-row w-full">
                    <div>
                      <FormImgComponent
                        itemId={user?.id}
                        fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                        maxSizeFile={4194304}
                        itemType="users"
                        type="avatar"
                        id="logo_url"
                        title={t('user.profile.logo_url')}
                        imageUrl={user?.logo_url}
                        defaultImg="/images/default/default-user.png"
                        onChange={handleChange}
                      />
                    </div>
                    <div tw="flex-1 lg:ml-8">
                      <div tw="flex flex-col lg:flex-row">
                        <div tw="lg:mr-4 flex-1">
                          <FormDefaultComponent
                            id="first_name"
                            placeholder={t('user.profile.firstname_placeholder')}
                            title={t('signUp.firstname')}
                            content={user?.first_name}
                            onChange={handleChange}
                            errorCodeMessage={valid_first_name ? valid_first_name.message : ''}
                            isValid={valid_first_name ? !valid_first_name.isInvalid : undefined}
                            mandatory
                            tw="mr-8"
                          />
                        </div>
                        <div tw="lg:ml-4 flex-1">
                          <FormDefaultComponent
                            id="last_name"
                            placeholder={t('user.profile.lastname_placeholder')}
                            title={t('user.profile.lastname')}
                            content={user?.last_name}
                            onChange={handleChange}
                            errorCodeMessage={valid_last_name ? valid_last_name.message : ''}
                            isValid={valid_last_name ? !valid_last_name.isInvalid : undefined}
                            mandatory
                          />
                        </div>
                      </div>
                      <div tw="flex flex-col lg:flex-row">
                        <div tw="lg:mr-2 flex-1">
                          <FormDefaultComponent
                            id="nickname"
                            placeholder={t('user.profile.nickname_placeholder')}
                            title={t('user.profile.nickname')}
                            content={user?.nickname}
                            onChange={handleChange}
                            prepend="@"
                            mandatory
                            errorCodeMessage={valid_nickname ? valid_nickname.message : ''}
                            isValid={valid_nickname ? !valid_nickname.isInvalid : undefined}
                            pattern={/[A-Za-z0-9]/g}
                          />
                        </div>
                        <div tw="lg:mx-2 flex-1">
                          <FormDropdownComponent
                            id="birth_date"
                            title={t('user.profile.birth_date')}
                            content={user?.birth_date && parseInt(user?.birth_date?.substr(0, 4))} // if user has set his birth year already, transform it into a number and only take the year from the value YYYY-MM-DD
                            placeholder={t('user.profile.birth_date_placeholder')}
                            options={yearsList}
                            errorCodeMessage={valid_birth_date ? valid_birth_date.message : ''}
                            mandatory
                            isSearchable
                            tooltipMessage={t('user.profile.birth_date_tooltip')}
                            onChange={handleChangeYear}
                          />
                        </div>
                        <div tw="lg:ml-2 flex-1">
                          <FormDropdownComponent
                            id="gender"
                            title={t('user.profile.gender')}
                            content={user?.gender}
                            placeholder={t('user.profile.gender_placeholder')}
                            options={['male', 'female', 'other', 'prefer_not_to_say']}
                            errorCodeMessage={valid_gender ? valid_gender.message : ''}
                            mandatory
                            tooltipMessage={t('user.profile.gender_tooltip')}
                            onChange={handleChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* Bios */}
                  <div>
                    <FormDefaultComponent
                      id="short_bio"
                      placeholder={t('user.profile.bioShort_placeholder')}
                      title={t('user.profile.bioShort')}
                      tooltipMessage={t('user.profile.bioShort_tooltip')}
                      content={user?.short_bio}
                      maxChar={140}
                      mandatory
                      errorCodeMessage={valid_short_bio ? valid_short_bio.message : ''}
                      isValid={valid_short_bio ? !valid_short_bio.isInvalid : undefined}
                      onChange={handleChange}
                    />
                  </div>
                  <FormTextAreaComponent
                    id="bio"
                    placeholder={t('user.profile.bio_placeholder')}
                    title={t('user.profile.bio')}
                    content={user?.bio}
                    rows={5}
                    supportLinks
                    onChange={handleChange}
                  />
                  <FormDefaultComponent
                    id="affiliation"
                    placeholder={t('user.profile.affiliation_placeholder')}
                    title={t('user.profile.affiliation')}
                    content={user?.affiliation}
                    mandatory
                    errorCodeMessage={valid_affiliation ? valid_affiliation.message : ''}
                    isValid={valid_affiliation ? !valid_affiliation.isInvalid : undefined}
                    onChange={handleChange}
                  />
                  {/* Country and city */}
                  <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                    <div className="formDropdown">
                      <TitleInfo mandatory title={t('general.country')} tooltipMessage={t('general.country_tooltip')} />
                      <div className="content">
                        <Select
                          name="country"
                          id="country"
                          defaultValue={user?.country && { label: user?.country, value: user?.country }}
                          options={countriesList}
                          placeholder={t('general.country_placeholder')}
                          filterOption={createFilter({ ignoreAccents: false })} // this line greatly improves performance
                          components={{ MenuList }}
                          noOptionsMessage={() => null}
                          onChange={handleCountryChange}
                          styles={countriesSelectStyles}
                          isClearable
                        />
                        {user?.country && countryNotInList && (
                          <Box color="#dc3545">
                            {
                              t('general.country_notInList') // ask user to re-select their country if it's not part of the new countries list we have (wrong spelling for ex)
                            }
                          </Box>
                        )}
                        {valid_country?.isInvalid && <Box color="#dc3545">{t(valid_country?.message || '')}</Box>}
                      </div>
                    </div>
                    {user?.country && !countryNotInList && (
                      <div className="formDropdown">
                        <TitleInfo title={t('general.city')} tooltipMessage={t('general.city_tooltip')} />
                        <div className="content">
                          <Select
                            name="city"
                            defaultValue={user?.city && { label: user?.city, value: user?.city }}
                            options={citiesList}
                            placeholder={t('general.city_placeholder')}
                            filterOption={createFilter({ ignoreAccents: citiesList?.length > 2150 ? false : true })} // ignore accents for countries that have over 2150 cities, because it greatly improves performance!
                            components={{ MenuList }}
                            noOptionsMessage={() => null}
                            onChange={handleCityChange}
                            styles={citiesSelectStyles}
                            isClearable
                          />
                        </div>
                      </div>
                    )}
                  </Box>
                </div>
              </div>
              {/* 2/ Complementary info */}
              <div tw="mt-8" className="additionalInformation">
                <TitleInfo
                  isFormSubpartTitle
                  title={t('user.profile.additional_infos')}
                  tooltipMessage={t('user.profile.additional_infos_tooltip')}
                />
                <div tw="border border-gray-200 border-solid rounded-md p-4">
                  <FormInterestsComponent
                    content={user?.interests}
                    onChange={handleChange}
                    errorCodeMessage={valid_interests ? valid_interests.message : ''}
                    mandatory
                  />
                  <FormSkillsComponent
                    id="skills"
                    type="user"
                    placeholder={t('general.skills.placeholder')}
                    title={t('user.profile.skills')}
                    tooltipMessage={t('user.profile.skills_tooltip')}
                    content={user?.skills}
                    mandatory
                    errorCodeMessage={valid_skills ? valid_skills.message : ''}
                    onChange={handleChange}
                  />
                  <FormResourcesComponent
                    id="ressources"
                    type="user"
                    placeholder={t('general.resources.placeholder')}
                    title={t('user.profile.resources')}
                    tooltipMessage={t('user.profile.resources_tooltip')}
                    content={user?.ressources}
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="buttonsContainer" tw="flex justify-center mt-8">
                <button type="button" className="btn btn-outline-primary" tw="mr-8" onClick={handleCancel}>
                  {t('user.profile.edit.back')}
                </button>
                {/* done as in user profile edit page: disables button only on sending, bc there is already a validation before submitting */}
                <button type="submit" className="btn btn-primary next" disabled={sending || disabledBtns}>
                  {sending && <SpinLoader />}
                  {t('completeProfile.btn.finish')}
                </button>
              </div>
            </form>
          </Loading>
          {!user && <p tw="text-center italic">{t('signUp.still_loading')}</p>}
        </div>
      </Container>
      {/* "You might want to follow" - section */}
      <Box margin="auto" textAlign="center" mt={8} mb={8} tw="width[fit-content]">
        {suggestedUser && <p>{t('completeProfile.suggestFollow')}</p>}
        {suggestedUser && (
          <UserCard
            id={suggestedUser.id}
            firstName={suggestedUser.first_name}
            lastName={suggestedUser.last_name}
            nickName={suggestedUser.nickname}
            shortBio={suggestedUser.short_bio}
            logoUrl={suggestedUser.logo_url}
            hasFollowed={suggestedUser.has_followed}
            isCompact
          />
        )}
      </Box>
    </Layout>
  );
};
export default CompleteProfile;
