import { useState } from 'react';
import { useRouter } from 'next/router';
import Layout from 'components/Layout';
import { useApi } from 'contexts/apiContext';
import { getApiFromCtx } from 'utils/getApi';
import { NextPage } from 'next';
import Box from 'components/Box';
import Card from 'components/Cards/Card';
import NeedUpdate from 'components/Need/NeedUpdate';
import NeedContent from 'components/Need/NeedContent';
import { Need } from 'types';
export type NeedDisplayMode = 'details' | 'update' | 'create';

const Needs: NextPage<{ need: Need }> = ({ need: needProp }) => {
  const [need, setNeed] = useState(needProp);
  const [mode, setMode] = useState();
  const api = useApi();
  const { query } = useRouter();
  const refresh = async () => {
    const res = await api.get(`/api/needs/${query.id}`).catch((err) => console.error(err));
    if (res?.data) setNeed(res.data);
  };
  const changeMode = (newMode) => {
    if (newMode) {
      setMode(newMode);
      refresh();
    }
  };
  return (
    <Layout title={`Need: ${need?.title} | JOGL`} desc={need?.content} img={need?.project.banner_url}>
      <Box px={2} maxWidth="800px" width="100%" margin="auto">
        <Card>
          {mode === 'update' ? (
            <NeedUpdate changeMode={changeMode} need={need} refresh={refresh} />
          ) : (
            <NeedContent changeMode={changeMode} need={need} />
          )}
        </Card>
      </Box>
    </Layout>
  );
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/needs/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { props: { need: res.data } };
  return { redirect: { destination: '/search/needs', permanent: false } };
}

export default Needs;
