import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import Box from 'components/Box';
import CommunityHeader from 'components/Community/CommunityHeader';
import Feed from 'components/Feed/Feed';
import Layout from 'components/Layout';
import H2 from 'components/primitives/H2';
import InfoHtmlComponent from 'components/Tools/Info/InfoHtmlComponent';
import useGet from 'hooks/useGet';
import { Community } from 'types';
import { getApiFromCtx } from 'utils/getApi';
// import "Components/Main/Similar.scss";
import { Tabs, TabPanels } from '@reach/tabs';
import { NavTab, TabListStyle, TabPanelFeed, TabPanelResp } from 'components/Tabs/TabsStyles';

interface Props {
  community: Community;
}
const CommunityDetails: NextPage<Props> = ({ community }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(
    `/api/communities/${community?.id}/links`
  );
  // TODO: This should be a state
  // Dynamic SEO meta tags
  const pageTitle = `${community.title} | JOGL`;
  const pageDesc = community.short_description;
  const pageImg = community.banner_url || '/images/default/default-group.jpg';

  // Tabs elements: tabs list, defaultIndex & handleTabsChange
  // All explained in pages-project-index file
  const tabs = [
    { value: 'feed', translationId: 'entity.tab.news' },
    { value: 'about', translationId: 'general.tab.about' },
  ];

  const defaultIndex = router.query.t
    ? tabs.findIndex((t) => t.value === router.query.t)
    : community.is_member
    ? tabs.findIndex((t) => t.value === 'feed')
    : tabs.findIndex((t) => t.value === 'about');

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/community/${community.id}/${community.short_title}?t=${tabs[index].value}`, undefined, {
      shallow: true,
    });
  };

  return (
    <Layout title={pageTitle} desc={pageDesc} img={pageImg} noIndex={community?.status === 'draft'}>
      {community && (
        <div className="communityDetails container-fluid">
          <CommunityHeader community={community} />
          <Tabs defaultIndex={defaultIndex} onChange={handleTabsChange}>
            <TabListStyle>
              {tabs.map((item, key) => (
                <NavTab key={key}>{t(item.translationId)}</NavTab>
              ))}
            </TabListStyle>
            <TabPanels tw="justify-center">
              {/* News Tab */}
              <TabPanelFeed>
                {community.feed_id && (
                  <Feed
                    feedId={community.feed_id}
                    // display post create component if you are member of the group
                    allowPosting={community.is_member}
                    isAdmin={community.is_admin}
                    needToJoinMsg={community.is_private}
                  />
                )}
              </TabPanelFeed>
              {/* About Tab */}
              <TabPanelResp>
                <Box width={['100%', undefined, undefined, '70%']} margin="auto">
                  {community.description ? (
                    <InfoHtmlComponent content={community.description} />
                  ) : (
                    community.short_description
                  )}
                  {dataExternalLink && dataExternalLink?.length !== 0 && (
                    <Box tw="pt-10">
                      <H2 tw="pb-4">{t('general.externalLink.findUs')}</H2>
                      <div tw="flex flex-wrap gap-x-4 sm:gap-x-3">
                        {[...dataExternalLink].map((link, i) => (
                          <a tw="items-center self-center" key={i} href={link.url} target="_blank">
                            <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                          </a>
                        ))}
                      </div>
                    </Box>
                  )}
                </Box>
              </TabPanelResp>
            </TabPanels>
          </Tabs>
        </div>
      )}
    </Layout>
  );
  // }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  try {
    const res = await api.get<Community>(`/api/communities/${ctx.query.id}`);
    return { props: { community: res.data } };
  } catch (err) {
    return { redirect: { destination: '/search/groups', permanent: false } };
  }
}
export default CommunityDetails;
