import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState } from 'react';
import CommunityForm from 'components/Community/CommunityForm';
import Layout from 'components/Layout';
import MembersList from 'components/Members/MembersList';
import Button from 'components/primitives/Button';
import P from 'components/primitives/P';
import Alert from 'components/Tools/Alert';
import ManageExternalLink from 'components/Tools/ManageExternalLink';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import useUser from 'hooks/useUser';
import { Community } from 'types';
import { logEventToGA } from 'utils/analytics';
import { getApiFromCtx } from 'utils/getApi';
import { NavTab, TabListStyle } from '../../../components/Tabs/TabsStyles';

interface Props {
  community: Community;
}
const CommunityEdit: NextPage<Props> = ({ community: communityProp }) => {
  const [community, setCommunity] = useState(communityProp);
  const [updatedCommunity, setUpdatedCommunity] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const [errors, setErrors] = useState('');
  const [loadPending, setLoadPending] = useState(false);
  const [nbOfPendingMembers, setNbOfPendingMembers] = useState(0);
  const { showModal, closeModal } = useModal();
  const urlBack = `/community/${community?.id}/${community?.short_title}`;
  const router = useRouter();
  const api = useApi();
  const { user } = useUser();
  const [membersTab, setMembersTab] = useState(undefined);
  const { t } = useTranslation('common');

  const handleChange = (key, content) => {
    setCommunity((prevCommunity) => ({ ...prevCommunity, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedCommunity((prevUpdatedCommunity) => ({ ...prevUpdatedCommunity, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api
      .patch(`/api/communities/${community.id}`, { community: updatedCommunity })
      .catch(() => setSending(false));
    if (res) {
      setSending(false);
      setUpdatedCommunity(undefined); // reset updated community component
      setHasUpdated(true); // show update confirmation message
      setTimeout(() => {
        setHasUpdated(false);
      }, 3000); // hide confirmation message after 3 seconds
    }
  };

  const deleteGroup = async () => {
    const res = await api.delete(`api/communities/${community.id}/`).catch((error) => {
      setErrors(error.toString());
    });
    if (res) {
      // record event to Google Analytics
      logEventToGA('delete group', 'Group', `[${user.id},${community.id}]`, { userId: user.id, itemId: community.id });
      closeModal(); // close modal
      router.push('/search/groups');
    }
  };
  const errorMessage = errors.includes('err-') ? t(errors) : errors;
  const delBtnTitleId = community.members_count > 1 ? 'community.archive.title' : 'community.delete.title';
  const delBtnTitle = community.members_count > 1 ? 'Archive group' : 'Delete group';
  const delBtnTextId = community.members_count > 1 ? 'community.archive.text' : 'community.delete.text';
  const delBtnText =
    community.members_count > 1
      ? 'Are you sure you want to archive this group?'
      : 'Are you sure you want to delete this group?  It will be permanently deleted.';

  // Tabs elements: tabs list & handleTabsChange
  // All explained in pages-project-index file
  const tabs = [
    { value: 'basic_info', translationId: 'entity.tab.basic_info' },
    { value: 'members', translationId: 'entity.tab.members' },
    { value: 'advanced', translationId: 'entity.tab.advanced' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/community/${community.id}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${community.title} | JOGL`}>
      <div className="communityEdit" tw="container mx-auto px-4">
        <h1>{t('community.edit.title')}</h1>
        <Link href={urlBack}>
          <a>
            {/* go back link */}
            <ArrowLeft size={15} title="Go back" />
            {t('community.edit.back')}
          </a>
        </Link>
        <Tabs
          defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>{t(item.translationId)}</NavTab>
            ))}
          </TabListStyle>
          <TabPanels tw="justify-center">
            <TabPanel>
              <CommunityForm
                community={community}
                mode="edit"
                sending={sending}
                hasUpdated={hasUpdated}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
              />
            </TabPanel>
            <TabPanel>
              <Tabs
                defaultIndex={nbOfPendingMembers > 0 ? 1 : 0}
                onChange={(id) => {
                  id === 1 && setLoadPending(true);
                  setMembersTab(id);
                }}
              >
                {nbOfPendingMembers > 0 && (
                  <TabListStyle>
                    <NavTab>{t('entity.tab.activeMembers')}</NavTab>
                    <NavTab>
                      {t('entity.tab.pendingMembers')} ({nbOfPendingMembers})
                    </NavTab>
                  </TabListStyle>
                )}
                <TabPanels tw="justify-center">
                  <TabPanel>
                    <MembersList
                      itemType="communities"
                      itemId={parseInt(router.query.id as string)}
                      isOwner={community.is_owner}
                      setNbOfPendingMembers={setNbOfPendingMembers}
                    />
                  </TabPanel>
                  <TabPanel>
                    {router.query.t && loadPending && (
                      <MembersList
                        itemType="communities"
                        itemId={parseInt(router.query.id as string)}
                        isOwner={community.is_owner}
                        onlyPending
                        setNbOfPendingMembers={setNbOfPendingMembers}
                      />
                    )}
                  </TabPanel>
                </TabPanels>
              </Tabs>
            </TabPanel>
            <TabPanel>
              <ManageExternalLink itemType="communities" itemId={router.query.id} />
              <hr />
              <div className="deleteBtns">
                {community && community.members_count > 1 && <p>{t('community.delete.explain')}</p>}
                <Button
                  onClick={() => {
                    showModal({
                      children: (
                        <>
                          {errors && <Alert type="danger" message={errorMessage} />}
                          <P fonSize="1rem">{t(delBtnTextId)}</P>
                          <div tw="inline-flex space-x-3">
                            <Button btnType="danger" onClick={deleteGroup}>
                              {t('general.yes')}
                            </Button>
                            <Button onClick={closeModal}>{t('general.no')}</Button>
                          </div>
                        </>
                      ),
                      title: t(delBtnTitleId),
                      maxWidth: '30rem',
                    });
                  }}
                  btnType="danger"
                >
                  {t(delBtnTitleId)}
                </Button>
                {community && community.members_count > 1 && (
                  <Button btnType="danger" disabled tw="ml-3">
                    {t('community.delete.title')}
                  </Button>
                )}
              </div>
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/communities/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch community with id=${query.id}`, err));
  // Check if it got the community and if the user is admin
  if (res?.data?.is_admin) return { props: { community: res.data } };
  return { redirect: { destination: '/search/groups', permanent: false } };
}

export default CommunityEdit;
