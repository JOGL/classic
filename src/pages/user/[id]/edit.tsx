/* eslint-disable camelcase */
import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import nextCookie from 'next-cookies';
import { toAlphaNum } from 'components/Tools/Nickname';
/** * Form objects ** */
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormTextAreaComponent from 'components/Tools/Forms/FormTextAreaComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from 'components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from 'components/Tools/Forms/FormResourcesComponent';
import FormToggleComponent from 'components/Tools/Forms/FormToggleComponent';
/** * Validators ** */
import FormValidator from 'components/Tools/Forms/FormValidator';
import userProfileFormRulesEdit from 'components/User/userProfileFormRulesEdit.json';
import Layout from 'components/Layout';
import { useApi } from 'contexts/apiContext';
import { getApiFromCtx } from 'utils/getApi';
import ManageExternalLink from 'components/Tools/ManageExternalLink';
import Box from 'components/Box';
import Button from 'components/primitives/Button';
import { useModal } from 'contexts/modalContext';
import Select, { createFilter } from 'react-select';
import { FixedSizeList as List } from 'react-window';
import FormDropdownComponent from 'components/Tools/Forms/FormDropdownComponent';
import TitleInfo from 'components/Tools/TitleInfo';
import { changesSavedConfAlert, getCountriesAndCitiesList } from 'utils/utils';
import SpinLoader from 'components/Tools/SpinLoader';

import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';
import UserDelete from 'components/User/Settings/UserDelete';
import UserDownloadInfos from 'components/User/Settings/UserDownloadInfos';
import UserNotificationsSettings from 'components/User/Settings/UserNotificationsSettings';
import FormChangePwd from 'components/Tools/Forms/FormChangePwd';
import useUserData from 'hooks/useUserData';

const UserProfileEdit = ({ user: userProp }) => {
  const validator = new FormValidator(userProfileFormRulesEdit);
  const [user, setUser] = useState(userProp); // contain all user fields (to update fields as user changes them)
  // updatedUser will contain only the updated fields (always populate with nickname, for it to be valid for backend)
  const [updatedUser, setUpdatedUser] = useState({ nickname: user.nickname });
  const [stateValidation, setStateValidation] = useState({});
  const [isSending, setIsSending] = useState(false);
  const { showModal } = useModal();
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');
  const [countriesCitiesData, setCountriesCitiesData] = useState([]);
  const [countryNotInList, setCountryNotInList] = useState(false);
  const [countriesList, setCountriesList] = useState([]);
  const [citiesList, setCitiesList] = useState([]);
  const { userData } = useUserData();

  getCountriesAndCitiesList(
    countriesCitiesData,
    setCountriesCitiesData,
    countryNotInList,
    setCountryNotInList,
    setCountriesList,
    setCitiesList,
    user?.country
  );

  const MenuList = ({ children, maxHeight }) => {
    return (
      <List height={maxHeight} itemCount={children.length} itemSize={30}>
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  };

  const handleChange = (key, content) => {
    setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, [key]: content })); // set an object containing only the fields/inputs that are updated by user
    setUser((prevUser) => ({ ...prevUser, [key]: content }));
    /* Validators start */
    const state = {};
    if (key === 'nickname') {
      state[key] = toAlphaNum(content);
    } else state[key] = content;
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };

  const handleCountryChange = (key, content) => {
    if (content.action === 'set-value' || content.action === 'select-option') {
      setUser((prevUser) => ({ ...prevUser, ['country']: key.value }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['country']: key.value })); // set an object containing only the fields/inputs that are updated by user
    }
    if (content.action === 'clear') {
      setUser((prevUser) => ({ ...prevUser, ['country']: '' }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['country']: '' })); // set an object containing only the fields/inputs that are updated by user
    }
    /* Validators start */
    const state = {};
    state['country'] = key ? key.value : undefined;
    const validation = validator.validate(state);
    if (validation['country'] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_country`] = validation['country'];
      setStateValidation(newStateValidation);
    }
  };

  const handleCityChange = (key, content) => {
    if (content.action === 'set-value' || content.action === 'select-option') {
      setUser((prevUser) => ({ ...prevUser, ['city']: key.value }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['city']: key.value })); // set an object containing only the fields/inputs that are updated by user
    }
    if (content.action === 'clear') {
      setUser((prevUser) => ({ ...prevUser, ['city']: '' }));
      setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['city']: '' })); // set an object containing only the fields/inputs that are updated by user
    }
  };

  // handling change of year separately, cause we're saving it in a different format than how it's shown in frontend
  const handleChangeYear = (key, content) => {
    const formattedBirthDate = new Date(content, 0) // make date be January 1 of the selected year (where content is the selected year)
      .toLocaleDateString('en-US'); // transform date into a YYYY-MM-DD format
    // we do this because for now we only need the year, but maybe in the future we'll ask the user the full date
    setUser((prevUser) => ({ ...prevUser, ['birth_date']: formattedBirthDate }));
    // TODO: have only one place where we manage need content
    setUpdatedUser((prevUpdatedUser) => ({ ...prevUpdatedUser, ['birth_date']: formattedBirthDate })); // set an object containing only the fields/inputs that are updated by user
    /* Validators start */
    const state = {};
    state['birth_date'] = content.toString(); // as value is a number, transform it into a string, needed for the form validation
    const validation = validator.validate(state);
    if (validation['birth_date'] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_birth_date`] = validation['birth_date'];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
  };

  const handleSubmit = () => {
    /* Validators control before submit */
    const validation = validator.validate(user);
    if (validation.isValid) {
      setIsSending(true);
      api.patch(`/api/users/${user.id}`, { user: updatedUser }).then(() => {
        setIsSending(false);
        setUpdatedUser({ nickname: user.nickname }); // reset updated user value
        // show conf message when project has been successfully saved/updated
        changesSavedConfAlert(t)
          .fire()
          // go back to project page if user clicked on conf button
          .then(({ isConfirmed }) => isConfirmed && router.push(`/user/${user.id}/${user.nickname}`));
      });
    } else {
      setIsSending(false);
      let firstError = true;
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        // map through all the object of validation
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 100; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };
  const {
    valid_first_name,
    valid_last_name,
    valid_nickname,
    valid_category,
    valid_country,
    valid_affiliation,
    valid_interests,
    valid_ressources,
    valid_skills,
    valid_short_bio,
    valid_birth_date,
    valid_gender,
  } = stateValidation || '';

  const countriesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
    control: (provided) => ({
      ...provided,
      // different border style depending if value is empty or not (validation)
      border: valid_country
        ? valid_country.isInvalid
          ? '1px solid #dc3545'
          : !valid_country.isInvalid && '1px solid #28a745'
        : '1px solid lightgrey',
    }),
  };
  const citiesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
  };
  const yearsList = [];
  const currentYear = new Date().getFullYear();
  for (let y = currentYear; y >= 1900; y--) {
    yearsList.push(y);
  }

  const tabs = [
    { value: 'information', translationId: 'entity.tab.basic_info' },
    { value: 'notification-settings', translationId: 'settings.notifications.name' },
    { value: 'admin', translationId: 'member.role.admin' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/user/${user.id}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${user?.first_name} ${user?.last_name} | JOGL`} desc={user?.bio} img={user?.logo_url}>
      {user ? (
        <div className="userProfileEdit">
          <div className="container-fluid justify-content-center">
            <h1>{t('user.profile.edit.title')}</h1>
            <Tabs
              defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
              onChange={handleTabsChange}
            >
              <TabListStyle>
                {tabs.map((item, key) => (
                  <NavTab key={key}>{t(item.translationId)}</NavTab>
                ))}
              </TabListStyle>
              <TabPanels tw="justify-center">
                {/* Information tab */}
                {/* This information tab is almost the same content as complete-profile page. It was used to create the UserCompleteProfile component.
                TODO futurely: this component, if harmonized, could be called here & as a modal in complete-profile. Only a few wordings would differ. */}
                <TabPanel>
                  <form>
                    <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                      <FormDefaultComponent
                        id="first_name"
                        placeholder={t('user.profile.firstname_placeholder')}
                        title={t('signUp.firstname')}
                        content={user.first_name}
                        onChange={handleChange}
                        errorCodeMessage={valid_first_name ? valid_first_name.message : ''}
                        isValid={valid_first_name ? !valid_first_name.isInvalid : undefined}
                        mandatory
                      />
                      <FormDefaultComponent
                        id="last_name"
                        placeholder={t('user.profile.lastname_placeholder')}
                        title={t('user.profile.lastname')}
                        content={user.last_name}
                        onChange={handleChange}
                        errorCodeMessage={valid_last_name ? valid_last_name.message : ''}
                        isValid={valid_last_name ? !valid_last_name.isInvalid : undefined}
                        mandatory
                      />
                      <FormDefaultComponent
                        id="nickname"
                        placeholder={t('user.profile.nickname_placeholder')}
                        title={t('user.profile.nickname')}
                        content={user.nickname}
                        onChange={handleChange}
                        prepend="@"
                        mandatory
                        errorCodeMessage={valid_nickname ? valid_nickname.message : ''}
                        isValid={valid_nickname ? !valid_nickname.isInvalid : undefined}
                        pattern={/[A-Za-z0-9]/g}
                      />
                    </Box>
                    <FormDefaultComponent
                      id="short_bio"
                      placeholder={t('user.profile.bioShort_placeholder')}
                      title={t('user.profile.bioShort')}
                      content={user.short_bio}
                      maxChar={140}
                      mandatory
                      errorCodeMessage={valid_short_bio ? valid_short_bio.message : ''}
                      isValid={valid_short_bio ? !valid_short_bio.isInvalid : undefined}
                      onChange={handleChange}
                    />
                    <FormTextAreaComponent
                      id="bio"
                      placeholder={t('user.profile.bio_placeholder')}
                      title={t('user.profile.bio')}
                      content={user.bio}
                      rows={5}
                      supportLinks
                      onChange={handleChange}
                    />
                    <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                      <FormDropdownComponent
                        id="category"
                        title={t('user.profile.category')}
                        content={user.category}
                        placeholder={t('user.profile.category_placeholder')}
                        mandatory
                        errorCodeMessage={valid_category ? valid_category.message : ''}
                        // prettier-ignore
                        options={['fulltime_worker', "parttime_worker", "fulltime_student", "parttime_student",
                  "freelance", "between_jobs", "retired", "student_looking_internship"]}
                        onChange={handleChange}
                      />
                      <FormDefaultComponent
                        id="affiliation"
                        placeholder={t('user.profile.affiliation_placeholder')}
                        title={t('user.profile.affiliation')}
                        content={user.affiliation}
                        mandatory
                        errorCodeMessage={valid_affiliation ? valid_affiliation.message : ''}
                        isValid={valid_affiliation ? !valid_affiliation.isInvalid : undefined}
                        onChange={handleChange}
                      />
                    </Box>
                    <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                      {/* countries dropdown */}
                      <div className="formDropdown">
                        <TitleInfo mandatory title={t('general.country')} />
                        <div className="content">
                          <Select
                            name="country"
                            id="country"
                            defaultValue={user.country && { label: user.country, value: user.country }}
                            options={countriesList}
                            placeholder={t('general.country_placeholder')}
                            filterOption={createFilter({ ignoreAccents: false })} // this line greatly improves performance
                            components={{ MenuList }}
                            noOptionsMessage={() => null}
                            onChange={handleCountryChange}
                            styles={countriesSelectStyles}
                            isClearable
                          />
                          {user.country && countryNotInList && (
                            <Box color="#dc3545">
                              {
                                t('general.country_notInList') // ask user to re-select their country if it's not part of the new countries list we have (wrong spelling for ex)
                              }
                            </Box>
                          )}
                          {valid_country?.isInvalid && <Box color="#dc3545">{t(valid_country?.message || '')}</Box>}
                        </div>
                      </div>
                      {/* cities dropdown */}
                      {user.country && !countryNotInList && (
                        <div className="formDropdown">
                          <TitleInfo title={t('general.city')} />
                          <div className="content">
                            <Select
                              name="city"
                              defaultValue={user.city && { label: user.city, value: user.city }}
                              options={citiesList}
                              placeholder={t('general.city_placeholder')}
                              filterOption={createFilter({ ignoreAccents: citiesList?.length > 2150 ? false : true })} // ignore accents for countries that have over 2150 cities, because it greatly improves performance!
                              components={{ MenuList }}
                              noOptionsMessage={() => null}
                              onChange={handleCityChange}
                              styles={citiesSelectStyles}
                              isClearable
                            />
                          </div>
                        </div>
                      )}
                    </Box>
                    <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
                      <FormDropdownComponent
                        id="gender"
                        title={t('user.profile.gender')}
                        content={user.gender}
                        placeholder={t('user.profile.gender_placeholder')}
                        options={['male', 'female', 'other', 'prefer_not_to_say']}
                        errorCodeMessage={valid_gender ? valid_gender.message : ''}
                        mandatory
                        tooltipMessage={t('user.profile.gender_tooltip')}
                        onChange={handleChange}
                      />
                      <FormDropdownComponent
                        id="birth_date"
                        title={t('user.profile.birth_date')}
                        content={user.birth_date && parseInt(user.birth_date?.substr(0, 4))} // if user has set his birth year already, transform it into a number and only take the year from the value YYYY-MM-DD
                        placeholder={t('user.profile.birth_date_placeholder')}
                        options={yearsList}
                        errorCodeMessage={valid_birth_date ? valid_birth_date.message : ''}
                        mandatory
                        isSearchable
                        tooltipMessage={t('user.profile.birth_date_tooltip')}
                        onChange={handleChangeYear}
                      />
                    </Box>
                    <Button
                      tw="mt-3 mb-2"
                      onClick={() => {
                        showModal({
                          children: <ManageExternalLink itemType="users" itemId={user.id} showTitle={false} />,
                          title: t('general.externalLink.addView'),
                          maxWidth: '70rem',
                          allowOverflow: true,
                        });
                      }}
                      type="button"
                    >
                      {t('general.externalLink.addView')}
                    </Button>
                    <FormImgComponent
                      itemId={user.id}
                      fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                      maxSizeFile={4194304}
                      itemType="users"
                      type="avatar"
                      id="logo_url"
                      title={t('user.profile.logo_url')}
                      imageUrl={user.logo_url}
                      defaultImg="/images/default/default-user.png"
                      onChange={handleChange}
                    />
                    <FormInterestsComponent
                      content={user.interests}
                      onChange={handleChange}
                      errorCodeMessage={valid_interests ? valid_interests.message : ''}
                      mandatory
                    />
                    <FormSkillsComponent
                      id="skills"
                      type="user"
                      placeholder={t('general.skills.placeholder')}
                      title={t('user.profile.skills')}
                      content={user.skills}
                      mandatory
                      errorCodeMessage={valid_skills ? valid_skills.message : ''}
                      onChange={handleChange}
                    />
                    <FormResourcesComponent
                      id="ressources"
                      type="user"
                      placeholder={t('general.resources.placeholder')}
                      title={t('user.profile.resources')}
                      content={user.ressources}
                      mandatory={false}
                      errorCodeMessage={valid_ressources ? valid_ressources.message : ''}
                      onChange={handleChange}
                    />
                    <FormToggleComponent
                      id="can_contact"
                      title={t('user.profile.canContact')}
                      choice1={t('general.no')}
                      choice2={t('general.yes')}
                      isChecked={user.can_contact || user.can_contact === null}
                      onChange={handleChange}
                    />
                    <FormToggleComponent
                      id="mail_newsletter"
                      title={t('signUp.mail_newsletter')}
                      choice1={t('general.no')}
                      choice2={t('general.yes')}
                      isChecked={user.mail_newsletter}
                      onChange={handleChange}
                    />
                    {/* <FormToggleComponent
                id="mail_weekly"
                title={t('signUp.mail_weekly')}
                choice1={t("general.no"}
                choice2={t("general.yes"}
                isChecked={user.mail_weekly}
                onChange={handleChange}
              /> */}
                    <div className="buttons">
                      <Link href={`/user/${user.id}/${user.nickname}`}>
                        <a>
                          <button type="button" className="btn btn-outline-primary">
                            {t('user.profile.edit.back')}
                          </button>
                        </a>
                      </Link>
                      <button
                        className="btn btn-primary"
                        style={{ marginRight: '10px', marginLeft: '10px' }}
                        type="button"
                        disabled={isSending}
                        onClick={handleSubmit}
                      >
                        {isSending && <SpinLoader />}
                        {t('user.profile.edit.submit')}
                      </button>
                    </div>
                  </form>
                </TabPanel>
                <TabPanel>
                  <div className="userSettings">
                    <div className="container justify-content-center">
                      <UserNotificationsSettings />
                    </div>
                  </div>
                </TabPanel>
                <TabPanel>
                  <div className="userSettings">
                    <div className="container justify-content-center">
                      {/* <h3>{t('settings.others')}</h3> */}
                      <Box className="d-flex">
                        <a
                          className="btn btn-primary changePwdBtn"
                          data-toggle="collapse"
                          href="#changePwdBtn"
                          role="button"
                          aria-expanded="false"
                          aria-controls="collapseExample"
                        >
                          {t('auth.changePwd.title')}
                        </a>
                        <div className="collapse" id="changePwdBtn">
                          <FormChangePwd />
                          <hr />
                        </div>
                        <UserDownloadInfos userId={userData?.id} />
                        <UserDelete userId={userData?.id} />
                      </Box>
                    </div>
                  </div>
                </TabPanel>
              </TabPanels>
            </Tabs>
          </div>
        </div>
      ) : (
        <div className="errorMessage text-center">{t('user.profile.edit.unable_load')}</div>
      )}
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const { userId } = nextCookie(ctx);
  const res = await api
    .get(`/api/users/${query.id}`)
    .catch((err) => console.error(`Could not fetch user with id=${query.id}`, err));
  // Check if it got the user and if user is the connected user, else redirect to user page
  if (query.id === userId) return { props: { user: res.data } };
  return { redirect: { destination: `/user/${query.id}`, permanent: false } };
}

export default UserProfileEdit;
