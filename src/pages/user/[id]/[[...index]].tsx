// import "./UserProfile.scss";
import { TabPanels, Tabs } from '@reach/tabs';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import { Fragment, useState, useEffect } from 'react';
import Box from 'components/Box';
import Chips from 'components/Chip/Chips';
import Feed from 'components/Feed/Feed';
import Grid from 'components/Grid';
import Layout from 'components/Layout';
import { NavTab, TabListStyle, TabPanelAbout, TabPanelFeed, TabPanelResp } from 'components/Tabs/TabsStyles';
import InfoAddressComponent from 'components/Tools/Info/InfoAddressComponent';
import InfoDefaultComponent from 'components/Tools/Info/InfoDefaultComponent';
import InfoInterestsComponent from 'components/Tools/Info/InfoInterestsComponent';
import Loading from 'components/Tools/Loading';
import TitleInfo from 'components/Tools/TitleInfo';
import UserCard from 'components/User/UserCard';
import UserCollection from 'components/User/UserCollection';
import UserHeader from 'components/User/UserHeader';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import useUserData from 'hooks/useUserData';
import { getApiFromCtx } from 'utils/getApi';
import { TextWithPlural } from 'utils/managePlurals';
import { linkify } from 'utils/utils';
import useGet from 'hooks/useGet';
import Link from 'next/link';

import 'intro.js/introjs.css';
import useBreakpoint from 'hooks/useBreakpoint';
import { showOnboardingStartModal, startUserIntro } from 'utils/onboarding/index';

export default function UserProfile({ user }) {
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const router = useRouter();
  const api = useApi();
  const modal = useModal();
  const [tabIndex, setTabIndex] = useState(1);
  const [loadCollection, setLoadCollection] = useState(false);
  const [myProfile, setMyProfile] = useState(false);
  const [sawIntroModal, setSawIntroModal] = useState(false);
  const isOnboarding = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOnboarding'));
  const isAnIndividual = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isIndividual'));
  const isAnOrganization = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOrganization'));
  const isDesktop = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isDesktop'));
  const breakpoint = useBreakpoint();
  const { data: affiliations } = useGet(`api/users/${user.id}/affiliations`);

  const showMutualConnections = () => {
    // get all user's mutual connections, then show modal displaying them
    api.get(`/api/users/${user?.id}/mutual`).then((res) => {
      modal.showModal({
        children: (
          <Grid tw="py-0 sm:py-2 md:py-4">
            {res.data.map((member, i) => (
              <UserCard
                key={i}
                id={member.id}
                firstName={member.first_name}
                lastName={member.last_name}
                nickName={member.nickname}
                shortBio={member.short_bio}
                logoUrl={member.logo_url}
                hasFollowed={member.has_followed}
                mutualCount={member.stats.mutual_count}
                isCompact
              />
            ))}
          </Grid>
        ),
        maxWidth: '61rem',
        title: t('general.mutualConnection', { count: 2 }),
      });
    });
  };

  const bioWithLinks = user.bio ? linkify(user.bio) : '';
  const hasObjInPortfolio =
    user.stats.projects_count !== 0 ||
    // user.stats.communities_count !== 0 ||
    user.stats.challenges_count !== 0 ||
    user.stats.programs_count !== 0 ||
    user.stats.spaces_count !== 0 ||
    user.id === userData?.id; // even if user has no obj in Portfolio, show the tab if it's him

  // Tabs elements: tabs list, setDefaultIndex & handleTabsChange
  // All explained in pages-project-index file
  const tabs = [
    { value: 'feed', translationId: 'user.profile.tab.feed' },
    { value: 'about', translationId: 'general.tab.about' },
    ...(hasObjInPortfolio ? [{ value: 'collection', translationId: 'user.profile.tab.collections' }] : []),
  ];

  const setDefaultIndex = () => {
    if (router.query.t) {
      return tabs.findIndex((t) => t.value === router.query.t);
    } else return tabs.findIndex((t) => t.value === 'about');
  };

  useEffect(() => {
    // load content of "collection" tab only when user clicks on its tab
    router.query.t === 'collection' && setLoadCollection(true);
  }, [router.query.t]);

  const handleTabsChange = (index) => {
    setTabIndex(index);
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/user/${user.id}?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  useEffect(() => {
    // [Onboarding][2] -  Lauching first onboarding modal, choosing individual/organization and triggering flow
    if (isOnboarding && !sawIntroModal && breakpoint !== 'mobile' && breakpoint !== 'tablet') {
      const isNewUser = router.query?.new_user === 'true';
      showOnboardingStartModal(modal, setSawIntroModal, handleTabsChange, t, isNewUser);
      localStorage.setItem('sawOnboardingFeature', 'true'); // so new feature announcement modal does not come back on homepage at the end of this onboarding
    }
  }, [isOnboarding, sawIntroModal, breakpoint]);

  useEffect(() => {
    // Set the breakpoint steadily for the whole tour
    if (breakpoint !== 'mobile' && breakpoint !== 'tablet') {
      localStorage.setItem('isDesktop', 'true');
    } else {
      localStorage.setItem('isDesktop', 'false');
    }
  }, [breakpoint]);

  useEffect(() => {
    // We have to check that this is the user profile as well as if we are on an onboarding flow to launch it
    if (myProfile && isOnboarding && (isAnIndividual || isAnOrganization)) {
      // time reduced compared to other pages because there is already the modal to start with
      setTimeout(() => {
        startUserIntro(isAnIndividual, isAnOrganization, t, router, isDesktop);
      }, 1000);
    }
  }, [myProfile, isOnboarding, isAnIndividual, isAnOrganization, breakpoint]);

  const userAffiliations = () =>
    (user.affiliation || affiliations?.parents.length !== 0) && (
      <div className="infoDefault">
        <TitleInfo title={t('user.profile.affiliation')} />
        <div className="content" tw="inline-flex">
          {/* if user has affiliation, display them */}
          {affiliations?.parents.length !== 0 &&
            affiliations?.parents
              .filter((affiliate) => affiliate.status !== 'pending')
              .map((affiliate, i) => {
                return (
                  <Fragment key={i}>
                    <Link href={`/${affiliate.parent_type.toLowerCase()}/${affiliate.short_title}`}>
                      <a>{affiliate.title}</a>
                    </Link>
                    {/* add comma, except for last item */}
                    {(affiliations?.parents.length !== i + 1 ||
                      (affiliations?.parents.length === i + 1 && user.affiliation)) &&
                      ', '}
                  </Fragment>
                );
              })}
          {/* show other user affiliation (with non JOGL objects/entities)  */}
          {user.affiliation}
        </div>
      </div>
    );

  return (
    <Layout
      title={`${user.first_name} ${user.last_name} | JOGL`}
      desc={user.short_bio || user.bio}
      img={user.logo_url}
      noIndex={user.confirmed_at === undefined} // don't index user if they didn't confirm their account
    >
      <div className="userProfile">
        <Loading active={false}>
          <div className="userHeader justify-content-center container-fluid">
            <UserHeader user={user} setMyProfile={setMyProfile} />
          </div>
          <Tabs defaultIndex={setDefaultIndex()} onChange={handleTabsChange} index={tabIndex}>
            <TabListStyle>
              {tabs.map((item, key) => (
                <NavTab key={key}>
                  <span id={item.value}>{t(item.translationId)}</span>
                </NavTab>
              ))}
            </TabListStyle>
            <TabPanels tw="justify-center">
              {/* Feed Tab */}
              <TabPanelFeed>
                {user.feed_id && (
                  <div className="feed">
                    <Feed
                      feedId={user.feed_id}
                      // show post creation box only to the user
                      allowPosting={userData && user.id === userData.id}
                      isAdmin={user.is_admin}
                    />
                  </div>
                )}
              </TabPanelFeed>
              {/* About Tab */}
              <TabPanelAbout tw="px-4">
                <Box width={['100%', undefined, undefined, '67%']} margin="auto">
                  <InfoDefaultComponent title={t('user.profile.bio')} content={bioWithLinks} containsHtml />
                  {userAffiliations()}
                  {user.category && user.category !== 'default' && (
                    <InfoDefaultComponent
                      title={t('user.profile.category')}
                      content={t(`user.profile.edit.select.${user.category}`)}
                    />
                  )}
                  {user.skills.length !== 0 && (
                    <Box className="infoSkills" tw="h-16 overflow-hidden mb-4">
                      <TitleInfo title={t('user.profile.skills')} />
                      <Chips
                        data={user.skills.map((skill) => ({
                          title: skill,
                          href: `/search/members/?refinementList[skills][0]=${skill}`,
                        }))}
                        type="skills"
                      />
                    </Box>
                  )}
                  {user.ressources.length !== 0 && (
                    <Box className="infoResources" tw="h-16 overflow-hidden mb-4">
                      <TitleInfo title={t('user.profile.resources')} />
                      <Chips
                        data={user.ressources.map((resource) => ({
                          title: resource,
                          href: `/search/members/?refinementList[ressources][0]=${resource}`,
                        }))}
                        type="resources"
                      />
                    </Box>
                  )}
                  <InfoInterestsComponent title={t('user.profile.interests')} content={user.interests} />
                  <InfoAddressComponent
                    title={t('user.profile.address')}
                    address={user.address}
                    city={user.city}
                    country={user.country}
                  />
                  {/* display projects counts and mutual connections (if you are connected and not the user */}
                  {userData &&
                    userData.id !== user.id &&
                    (user.stats.mutual_count > 0 || user.stats.projects_count > 0) && (
                      <Box tw="pb-6" width="fit-content">
                        <TitleInfo title={t('user.info.other')} />
                        {user.stats.projects_count > 0 && (
                          <Box tw="pt-2">
                            {`${t('user.info.currentlyOn')} ${user.stats.projects_count}`}{' '}
                            <TextWithPlural type="project" count={user.stats.projects_count} />
                          </Box>
                        )}
                        {user.stats.mutual_count > 0 && (
                          <div tw="pt-2 hover:(cursor-pointer underline)" onClick={showMutualConnections}>
                            {user.stats.mutual_count}{' '}
                            <TextWithPlural type="mutualConnection" count={user.stats.mutual_count} />
                          </div>
                        )}
                      </Box>
                    )}
                </Box>
              </TabPanelAbout>
              {/* Collection Tab (load only on tab click) */}
              <TabPanelResp>
                {loadCollection && <UserCollection t={t} userId={user.id} userStats={user.stats} userData={userData} />}
              </TabPanelResp>
            </TabPanels>
          </Tabs>
        </Loading>
      </div>
    </Layout>
  );
}

export async function getServerSideProps(ctx) {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/users/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { props: { user: res.data } };
  return { redirect: { destination: '/search/members', permanent: false } };
}
