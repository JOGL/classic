import Link from 'next/link';
// import Image from 'next/image';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import FormChangePwd from 'components/Tools/Forms/FormChangePwd';
import Layout from 'components/Layout';
import Image2 from 'components/Image2';

const ChangePassword: NextPage = () => {
  const { t } = useTranslation('common');
  return (
    <Layout className="no-margin" title={`${t('auth.changePwd.title')} | JOGL`} noIndex>
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link href="/">
            <a>
              <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" />
            </a>
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <FormChangePwd />
        </div>
      </div>
    </Layout>
  );
};
export default ChangePassword;
