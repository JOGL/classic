import Link from 'next/link';
import Router, { useRouter } from 'next/router';
// import Image from 'next/image';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { ReactNode, useEffect, useState } from 'react';
import Alert from 'components/Tools/Alert';
import Layout from 'components/Layout';
import { useApi } from 'contexts/apiContext';
import Image2 from 'components/Image2';
import SpinLoader from 'components/Tools/SpinLoader';

const NewPwd: NextPage = () => {
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [error, setError] = useState('');
  const [fireRedirect, setFireRedirect] = useState(false);
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | ReactNode>(error);
  const { t } = useTranslation('common');
  const router = useRouter();
  const api = useApi();

  const checkPassword = (pwd, pwdConfirm) => {
    if (pwd !== pwdConfirm) {
      setError('err-4001');
      return false;
    }
    if (pwd.length < 8) {
      setError('err-4002');
      return false;
    }
    return true;
  };

  const handleChange = (e) => {
    setError('');
    switch (e.target.name) {
      case 'password':
        setPassword(e.target.value);
        break;
      case 'password_confirmation':
        setPasswordConfirmation(e.target.value);
        break;
      default:
        console.error(`The input with name=${e.target.name} is not handled`);
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const headers = {
      'access-token': router.query['access-token'],
      client: router.query.client,
      uid: router.query.uid,
    };
    if (checkPassword(password, passwordConfirmation)) {
      setError('');
      setSending(true);
      api
        .put('/api/auth/password', { password, password_confirmation: passwordConfirmation }, { headers })
        .then(() => {
          setSending(false);
          setSuccess(true);
          setTimeout(() => {
            setFireRedirect(true);
          }, 3500);
        })
        .catch((err) => {
          setError(err.response.data.errors[0]);
          setSending(false);
        });
    }
  };

  useEffect(() => {
    if (fireRedirect) {
      Router.push('/signin');
    }
  }, [fireRedirect]);

  useEffect(() => {
    setErrorMessage(error.includes('err-') ? t(error) : error);
  }, [error]);

  const title = {
    text: 'Password reset',
    id: 'auth.newPwd.title',
  };
  const msg = {
    text: 'Please enter a new password.',
    id: 'auth.newPwd.description',
  };
  return (
    <Layout className="no-margin" title={`${t('forgotPwd.title')} | JOGL`} noIndex>
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link href="/">
            <a>
              <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" />
            </a>
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <div className="form-content">
            <div className="form-header">
              <h2 className="form-title" id="signModalLabel">
                {t(title.id)}
              </h2>
              <p>{t(msg.id)}</p>
            </div>
            <div className="form-body" style={{ display: 'block' }}>
              <form onSubmit={handleSubmit} className="newPwdForm">
                <div className="form-group">
                  <label className="form-check-label" htmlFor="password">
                    {t('auth.newPwd.pwd')}
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    placeholder={t('auth.newPwd.pwd_placeholder')}
                    onChange={handleChange}
                  />
                </div>
                <div className="form-group">
                  <label className="form-check-label" htmlFor="password_confirmation">
                    {t('auth.newPwd.pwdConfirm')}
                  </label>
                  <input
                    type="password"
                    name="password_confirmation"
                    id="password_confirmation"
                    className="form-control"
                    placeholder={t('auth.newPwd.pwd_placeholder')}
                    onChange={handleChange}
                  />
                </div>
                {error !== '' && <Alert type="danger" message={errorMessage} />}
                {success && <Alert type="success" message={t('info-4001')} />}

                <button className="btn btn-primary btn-block" disabled={!!sending} type="submit">
                  {sending && <SpinLoader />}
                  {t('auth.newPwd.btnConfirm')}
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default NewPwd;
