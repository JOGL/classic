import Link from 'next/link';
// import Image from 'next/image';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { ReactNode, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Alert from 'components/Tools/Alert';
import Layout from 'components/Layout';
import { useApi } from 'contexts/apiContext';
import Image2 from 'components/Image2';
import SpinLoader from 'components/Tools/SpinLoader';

const ForgotPwd: NextPage = () => {
  const [email, setEmail] = useState('');
  const [error, setError] = useState<string | ReactNode>('');
  const [fireRedirect, setFireRedirect] = useState(false);
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [formStyle, setFormStyle] = useState({ display: 'block' });
  const [messageStyle, setMessageStyle] = useState({ display: 'none' });
  const router = useRouter();
  const [title, setTitle] = useState({
    text: 'Reset Password',
    id: 'forgotPwd.title',
  });
  const [msg, setMsg] = useState({
    text: 'Password reset instruction will be sent to your email address.',
    id: 'forgotPwd.description',
  });
  const { t } = useTranslation('common');

  const api = useApi();

  const handleChange = (e) => {
    setError('');
    switch (e.target.name) {
      case 'email':
        setEmail(e.target.value);
        break;
      default:
        setError('Unknown input');
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (email === '') {
      setError(t('err-4003'));
    } else {
      const redirectUrl = `${process.env.ADDRESS_FRONT}/auth/new-password`;
      setError('');
      setSending(true);

      api
        .post('/api/auth/password', { email, redirect_url: redirectUrl })
        .then(() => {
          setSending(false);
          setSuccess(true);
          setTimeout(() => {
            setFireRedirect(true);
          }, 10000);
        })
        .catch((err) => {
          setSending(false);
          setError(err.response.data.errors[0]);
        });
    }
  };
  useEffect(() => {
    if (fireRedirect) {
      router.push('/signin');
    }
  }, [fireRedirect]);

  useEffect(() => {
    if (!success) {
      setFormStyle({ display: 'block' });
      setMessageStyle({ display: 'none' });
      setTitle({
        text: 'Reset Password',
        id: 'forgotPwd.title',
      });
      setMsg({
        text: 'Password reset instruction will be sent to your email address.',
        id: 'forgotPwd.description',
      });
    } else {
      setFormStyle({ display: 'none' });
      setMessageStyle({ display: 'block' });
      setTitle({
        text: 'An email has been sent to you.',
        id: 'forgotPwd.confTitle',
      });
      setMsg({
        text: 'Password reset instruction will be sent to your email address.',
        id: 'forgotPwd.confMsg',
      });
    }
  }, [success]);

  return (
    <Layout className="no-margin" title={`${t('forgotPwd.title')} | JOGL`} noIndex>
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link href="/">
            <a>
              <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" />
            </a>
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <div className="form-content">
            <div className="form-header">
              <h2 className="form-title" id="signModalLabel">
                {t(title.id)}
              </h2>
              <p>{t(msg.id)}</p>
            </div>
            <div className="form-body" style={formStyle}>
              <form onSubmit={handleSubmit}>
                <div className="form-group">
                  <label className="form-check-label" htmlFor="password">
                    {t('auth.email.title')}
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className="form-control"
                    placeholder={t('auth.email.placeholder')}
                    onChange={handleChange}
                  />
                </div>
                {error !== '' && <Alert type="danger" message={error} />}
                {success && <Alert type="success" message={t('info-4000')} />}

                <button type="submit" className="btn btn-primary btn-block" disabled={!!sending}>
                  {sending && <SpinLoader />}
                  {t('forgotPwd.btnSendMail')}
                </button>
              </form>
            </div>
            <div className="form-message" style={messageStyle}>
              <img src="/images/envelope.svg" alt="Message sent envelope" />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ForgotPwd;
