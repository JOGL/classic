import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React from 'react';
import Layout from 'components/Layout';
// import "assets/css/ChallengePage.scss";

const ChallengeForbiddenPage: NextPage = () => {
  const { t } = useTranslation('common');
  return (
    <Layout noIndex>
      <div className="container-fluid ChallengePage">
        <div className="row">
          <div className="col-12 text-center">
            <br />
            <br />
            {t('challenge.info.forbidden')}
            <br />
            <a href="mailto:hello@jogl.io">hello@jogl.io</a>
            <br />
            <br />
            <br />
            <Link href="/search/challenges">
              <a>
                <button className="btn btn-primary" type="button">
                  {t('challenge.info.forbiddenBtn')}
                </button>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default ChallengeForbiddenPage;
