import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Box from 'components/Box';
import ChallengeForm from 'components/Challenge/ChallengeForm';
import Grid from 'components/Grid';
import Layout from 'components/Layout';
import MembersList from 'components/Members/MembersList';
import Button from 'components/primitives/Button';
import ProjectAdminCard from 'components/Project/ProjectAdminCard';
import { ProjectLinkModal } from 'components/Project/ProjectLinkModal';
import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';
import DocumentsManager from 'components/Tools/Documents/DocumentsManager';
import Loading from 'components/Tools/Loading';
import ManageExternalLink from 'components/Tools/ManageExternalLink';
import ManageFaq from 'components/Tools/ManageFaq';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import { Challenge, Project } from 'types';
import { getApiFromCtx } from 'utils/getApi';

interface Props {
  challenge: Challenge;
}
const ChallengeEdit: NextPage<Props> = ({ challenge: challengeProp }) => {
  const [challenge, setChallenge] = useState(challengeProp);
  const [updatedChallenge, setUpdatedChallenge] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const api = useApi();
  const router = useRouter();
  const {
    data: projectsData,
    error: projectsError,
    revalidate: projectsRevalidate,
    mutate: mutateProjects,
  } = useGet<{
    projects: Project[];
  }>(`/api/challenges/${challenge.id}/projects`);
  const { showModal, closeModal } = useModal();
  const { t } = useTranslation('common');

  const handleChange = (key, content) => {
    setChallenge((prevChallenge) => ({ ...prevChallenge, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage need content
    setUpdatedChallenge((prevUpdatedChallenge) => ({ ...prevUpdatedChallenge, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = () => {
    setSending(true);
    api
      .patch(`/api/challenges/${challenge.id}`, { challenge: updatedChallenge })
      .then(() => {
        setSending(false);
        setUpdatedChallenge(undefined); // reset updated program component
        setHasUpdated(true); // show update confirmation message
        setTimeout(() => {
          setHasUpdated(false);
        }, 3000); // hide confirmation message after 3 seconds
      })
      .catch(() => setSending(false));
  };

  // Tabs elements: tabs list & handleTabsChange
  // All explained in pages-project-index file
  const tabs = [
    { value: 'basic_info', translationId: 'entity.tab.basic_info' },
    { value: 'members', translationId: 'entity.tab.members' },
    { value: 'projects', translationId: 'user.profile.tab.projects' },
    { value: 'documents', translationId: 'entity.tab.documents' },
    { value: 'faqs', translationId: 'faq.title' },
    { value: 'advanced', translationId: 'entity.tab.advanced' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/challenge/${challenge.short_title}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${challenge.title} | JOGL`}>
      <div className="challengeEdit" tw="container mx-auto px-4">
        <h1>{t('challenge.edit.title')}</h1>
        <Link href={`/challenge/${challenge?.short_title}`}>
          <a>
            {/* go back link */}
            <ArrowLeft size={15} title="Go back" />
            {t('challenge.edit.back')}
          </a>
        </Link>
        <Tabs
          defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>{t(item.translationId)}</NavTab>
            ))}
          </TabListStyle>
          <TabPanels tw="justify-center">
            {/* Basic information Tab */}
            <TabPanel>
              <ChallengeForm
                challenge={challenge}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                mode="edit"
                sending={sending}
                hasUpdated={hasUpdated}
              />
            </TabPanel>
            {/* Members Tab */}
            <TabPanel>
              <MembersList itemType="challenges" itemId={challenge.id} isOwner={challenge.is_owner} />
            </TabPanel>
            {/* Projects Tab */}
            <TabPanel>
              {projectsData ? (
                <div className="projectsAttachedList">
                  <Box pb={6}>
                    {challenge?.status === 'accepting' &&
                    (challenge?.program.id !== -1 || challenge?.space.id !== -1) ? (
                      // show submit button only if challenge is attached to a program (or space) and has the status 'accepting'
                      <div className="justify-content-end projectsAttachedListBar">
                        <Button
                          onClick={() => {
                            showModal({
                              children: (
                                <ProjectLinkModal
                                  alreadyPresentProjects={projectsData?.projects}
                                  challengeId={challenge.id}
                                  programId={challenge.program.id}
                                  spaceId={challenge.space.id}
                                  isMember={challenge.is_member}
                                  hasFollowed={challenge.has_followed}
                                  mutateProjects={mutateProjects}
                                  closeModal={closeModal}
                                />
                              ),
                              title: t('attach.project.title'),
                              maxWidth: '50rem',
                            });
                          }}
                        >
                          {t('attach.project.title')}
                        </Button>
                      </div>
                    ) : (
                      // else show informative message
                      t('attach.project.cantAdd')
                    )}
                  </Box>
                  <div tw="pt-4">
                    {projectsData?.projects
                      // sort the array to have the pending projects first
                      .reduce((acc, element) => {
                        if (
                          // find challenge project_status by accessing object with same challenge_id as the accessed challenge id (old way was "element.challenge_status === 'pending'")
                          element.challenges.find((obj) => obj.challenge_id === challenge.id).project_status ===
                          'pending'
                        ) {
                          return [element, ...acc];
                        }
                        return [...acc, element];
                      }, [])
                      // then map through it
                      .map((project, i) => (
                        <ProjectAdminCard
                          key={i}
                          project={project}
                          parentId={challenge.id}
                          parentType="challenges"
                          callBack={projectsRevalidate}
                        />
                      ))}
                  </div>
                </div>
              ) : (
                <Loading />
              )}
            </TabPanel>
            {/* Documents Tab */}
            <TabPanel>
              <DocumentsManager
                documents={challenge.documents}
                isAdmin={challenge.is_admin}
                itemId={challenge.id}
                itemType="challenges"
              />
            </TabPanel>
            {/* Faq Tab */}
            <TabPanel>
              <ManageFaq itemType="challenges" itemId={challenge.id} />
            </TabPanel>
            {/* Advanced Tab */}
            <TabPanel>
              <ManageExternalLink itemType="challenges" itemId={challenge.id} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/challenges/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch challenge with short_title=${query.short_title}`, err));
  if (getIdRes?.data?.id) {
    const challengeRes = await api
      .get(`/api/challenges/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch challenge with id=${getIdRes.data.id}`, err));
    // Check if it got the challenge and if the user is admin
    if (challengeRes?.data?.is_admin) return { props: { challenge: challengeRes.data } };
  }
  return { redirect: { destination: '/search/challenges', permanent: false } };
}
export default ChallengeEdit;
