import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useCallback, useContext, useEffect, useState } from 'react';
import Layout from 'components/Layout';
import A from 'components/primitives/A';
import Button from 'components/primitives/Button';
import P from 'components/primitives/P';
import { displayObjectRelativeDate } from 'utils/utils';
import { NotifIconWrapper, notifIconTypes } from 'utils/notificationsIcons';
import { useApi } from 'contexts/apiContext';
import { UserContext } from 'contexts/UserProvider';
import SpinLoader from 'components/Tools/SpinLoader';

const NotificationPage = () => {
  const [notifications, setNotifications] = useState([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(0);
  const [totalNotificationsCount, setTotalNotificationsCount] = useState(0);
  const [loadingBtn, setLoadingBtn] = useState(false);
  const [offSetCursor, setOffSetCursor] = useState();
  const numberOfNotifPerCall = 30;
  const api = useApi();
  const router = useRouter();
  const { user, isConnected } = useContext(UserContext);
  const { t } = useTranslation('common');

  const loadNotifications = useCallback(() => {
    setLoadingBtn(true);
    const pageScroll = window.pageYOffset; // get whole page offSetTop
    const paginationFilters = !offSetCursor // if we don't have offSetCursor, it means it's first call, so call only first number of wanted notifications
      ? `first: ${numberOfNotifPerCall}`
      : // else, it has been set, so call wanted number of notifications, after the last one (so we have a pagination)
        `first: ${numberOfNotifPerCall} after: "${offSetCursor}"`;
    user &&
      api
        .post('/graphql/', {
          query: `
          query {
            user (id: ${user.id} ) {
              notifications(${paginationFilters}) {
                unreadCount
                totalCount
                edges {
                  node {
                    id
                    ctaLink
                    read
                    subjectLine
                    createdAt
                    category
                  }
                  cursor
                }
              }
            }
          }
        `,
        })
        .then((response) => {
          var userNotifications = response.data.data.user.notifications;
          // set offset cursor to the last notification cursor value, so we can call the next notifications after that one
          setOffSetCursor(userNotifications?.edges[userNotifications.edges.length - 1].cursor);
          // update notification counts
          setUnreadNotificationsCount(userNotifications.unreadCount);
          setTotalNotificationsCount(userNotifications.totalCount);
          // concatenate current notifications with the new ones
          setNotifications([...notifications, ...userNotifications.edges.map((edge) => edge.node)]);
          window.scrollTo(0, pageScroll); // force to stay at pageScroll (else it would focus you at bottom of the list of posts)
          setLoadingBtn(false);
        })
        .catch((err) => {});
  }, [user, notifications]);

  useEffect(() => {
    // get first notifications on first load
    loadNotifications();
    // redirect to homepage if user is not connected
    if (!isConnected) router.push('/');
  }, [user]);

  const visitNotificationLinkAndMarkAsRead = (notification) => {
    api
      .post('/graphql/', {
        query: `
          mutation {
            markNotificationAsRead(input: {id: ${notification.id}}) {
              notification {
                id
                read
              }
            }
          }`,
      })
      .then((_response) => {
        // On success, update unread count
        notification.read = true;
        setUnreadNotificationsCount(unreadNotificationsCount - 1);
      })
      .catch((err) => {
        // do something
      });
  };

  const markAllNotificationsAsRead = () => {
    api
      .post('/graphql/', {
        query: `
          mutation {
            markAllNotificationsAsRead(input: {}) {
              unreadCount
            }
          }`,
      })
      .then((response) => {
        // On success, update unread count
        setUnreadNotificationsCount(response.data.unreadCount);
        let notifs = notifications.map((notif) => {
          notif.read = true;
          return notif;
        });
        setNotifications(notifs);
      });
  };

  return (
    <Layout title="Notifications | JOGL" noIndex>
      <div tw="px-2 max-w-3xl w-full m-auto">
        <div tw="flex flex-row justify-between items-center py-2">
          <div tw="font-bold">{t('settings.notifications.title')}</div>
          <div tw="flex flex-row items-end">
            <div onClick={() => markAllNotificationsAsRead()}>
              <A href="#">{t('settings.notifications.markAsRead')}</A>
            </div>
            <div tw="px-1">.</div>
            <Link href={`/user/${user?.id}/edit?t=notification-settings`} passHref>
              <A>{t('menu.profile.settings')}</A>
            </Link>
          </div>
        </div>
        <div tw="flex flex-col border border-solid border-gray-600">
          {notifications?.length === 0
            ? [...Array(8)].map((e, i) => (
                <div tw="py-4 px-2 w-full mx-auto border-0 border-b-2 border-solid border-gray-300" key={i}>
                  <div tw="animate-pulse flex space-x-2">
                    <div tw="rounded-full bg-blue-200 h-8 w-8 mr-1"></div>
                    <div tw="flex-1 space-y-2">
                      <div tw="h-4 bg-blue-200 rounded"></div>
                      <div tw="h-3 bg-blue-200 rounded w-2/6"></div>
                    </div>
                  </div>
                </div>
              ))
            : notifications &&
              notifications?.map((notification) => {
                return (
                  <a
                    href={notification.ctaLink}
                    key={notification.id}
                    onClick={() => visitNotificationLinkAndMarkAsRead(notification)}
                    tw="(py-2.5 px-3) background[#fcfcfc] border-0 border-b border-gray-300 border-solid hover:(background[#eaedf0] no-underline)"
                  >
                    <div tw="flex justify-between items-center">
                      <div tw="flex">
                        <div tw="flex pr-2">
                          <NotifIconWrapper>{notifIconTypes[notification.category]}</NotifIconWrapper>
                        </div>
                        <div tw="flex flex-wrap flex-row">
                          <P mr={2} mb={0}>
                            {notification.subjectLine}
                          </P>
                          <P mb={0} color="#797979">
                            {displayObjectRelativeDate(notification.createdAt)}
                          </P>
                        </div>
                      </div>
                      {!notification.read && (
                        <div tw="ml-2">
                          <div tw="h-3 w-3 bg-primary rounded-full" />
                        </div>
                      )}
                    </div>
                  </a>
                );
              })}
        </div>
        {notifications.length !== totalNotificationsCount && (
          <div tw="self-center mt-4">
            <Button onClick={loadNotifications} disabled={loadingBtn}>
              {loadingBtn && <SpinLoader />}
              {t('general.load')}
            </Button>
          </div>
        )}
      </div>
    </Layout>
  );
};

export default NotificationPage;
