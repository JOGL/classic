import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import Layout from 'components/Layout';
import Button from 'components/primitives/Button';

export default function Page404() {
  const { t } = useTranslation('common');

  return (
    <Layout title="404 | JOGL" className="no-margin" noIndex>
      <div tw="bg-gray-100 flex items-center px-5 py-16 overflow-hidden relative justify-center lg:p-20">
        <div tw="flex-1 rounded-3xl bg-white shadow-xl p-20 text-gray-800 relative items-center text-center max-w-4xl md:(flex text-left)">
          {/* <div tw="w-full md:w-1/2"> */}
          <div tw="w-full">
            {/* <div tw="mb-10 lg:mb-20">logo here</div> */}
            <div tw="mb-10 text-gray-600 font-light md:mb-14">
              <h1 tw="font-black uppercase text-3xl text-primary mb-10 lg:text-5xl">{t('err-404.title')}</h1>
              {/* <p>The page you're looking for isn't available.</p>
              <p>Try searching again or use the Go Back button below.</p> */}
            </div>
            <div tw="mb-20 md:mb-0">
              <Link href="/">
                <a>
                  <Button>{t('err-404.btn')}</Button>
                </a>
              </Link>
            </div>
          </div>
          {/* <div tw="w-full md:w-1/2 text-center"> */}
          <div tw="w-full text-center">{/* Img here */}</div>
        </div>
        {/* <div tw="w-64 md:w-96 h-96 md:h-full bg-blue-200 bg-opacity-30 absolute -top-64 md:-top-96 right-20 md:right-32 rounded-full pointer-events-none -rotate-45 transform"></div>
        <div tw="w-96 h-full bg-yellow-200 bg-opacity-20 absolute -bottom-96 right-64 rounded-full pointer-events-none -rotate-45 transform"></div> */}
      </div>
    </Layout>
  );
}
