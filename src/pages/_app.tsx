/* eslint-disable global-require */
// General imports
// import { CacheProvider as CacheProviderEmotion } from '@emotion/react';
// import { GlobalStyles } from 'twin.macro';
// import { cache as emotionCache } from '@emotion/css';
import { ThemeProvider as ThemeProviderEmotion } from '@emotion/react';
import '@reach/menu-button/styles.css';
import { Router, useRouter } from 'next/router';
import nProgress from 'nprogress';
import React, { useEffect, useState } from 'react';
import 'components/Auth.scss';
import 'components/Challenge/ChallengeHeader.scss';
import 'components/Community/CommunityForm.scss';
import 'components/Feed/Comments/CommentCreate.scss';
import 'components/Feed/Comments/CommentDisplay.scss';
import 'components/Feed/Feed.scss';
import 'components/Feed/Posts/PostCreate.scss';
import 'components/Feed/Posts/PostDisplay.scss';
import 'components/Footer/Footer.scss';
import 'components/Need/need.scss';
import 'components/Need/NeedContent.scss';
import 'components/Need/NeedDocsManagement.scss';
import 'components/Need/NeedForm.scss';
import 'components/Need/NeedWorkers.scss';
import 'components/Search/Search.scss';
import 'components/Similar.scss';
import 'components/Tools/BtnClap.scss';
import 'components/Tools/BtnUploadFile.scss';
import 'components/Tools/CustomChip.scss';
import 'components/Tools/Documents/DocumentCard.scss';
import 'components/Tools/Documents/DocumentCardFeed.scss';
import 'components/Tools/DropdownRole.scss';
import 'components/Tools/Forms/FormDefaultComponent.scss';
import 'components/Tools/Forms/FormImgComponent.scss';
import 'components/Tools/Forms/FormResourcesComponent.scss';
import 'components/Tools/Forms/FormSkillsComponent.scss';
import 'components/Tools/Forms/FormTextAreaComponent.scss';
import 'components/Tools/Forms/FormToggleComponent.scss';
import 'components/Tools/Forms/PostInputMentions.scss';
import 'components/Tools/Info/InfoAddressComponent.scss';
import 'components/Tools/Info/InfoDefaultComponent.scss';
import 'components/Tools/Info/InfoHtmlComponent.scss';
import 'components/Tools/Info/InfoInterestsComponent.scss';
import 'components/Tools/ShareBtns/ShareBtns.scss';
import 'components/Tools/Webhooks/Webhook/HookCard.scss';
import 'components/Tools/Webhooks/Webhook/HookForm.scss';
import 'components/Tools/Webhooks/Webhooks.scss';
import 'components/User/Settings/UserSettings.scss';
import 'components/User/UserHeader.scss';
import 'components/User/UserProfile.scss';
import 'components/User/UserProfileEdit.scss';
import { ApiProvider } from 'contexts/apiContext';
import { ModalProvider } from 'contexts/modalContext';
import { NotLoggedInModalProvider } from 'contexts/notLoggedInModalContext';
import { SpaceViewAsContextProvider } from 'contexts/SpaceViewAsContext';

import UserProvider from 'contexts/UserProvider';
import useGA from 'hooks/useGA';
import useGTM from 'hooks/useGTM';
import useHotjar from 'hooks/useHotjar';
import useSyncLogout from 'hooks/useSyncLogout';
import 'pages/AlgoliaResultsPages.scss';
import 'pages/complete-profile/CompleteProfile.scss';
import 'pages/custom.scss';
import 'pages/global.scss';
import 'pages/legal.scss';
import 'pages/nProgress.scss';
import 'pages/search/search.scss';
import { Credentials } from 'types';
// import { getApiFromCtx } from 'utils/getApi';
import theme from 'utils/theme';
import usePersistLocaleCookie from 'hooks/usePersistLocaleCookie';
import cookie from 'js-cookie';
import 'intl-pluralrules';
// Formatting dates with day.js
import 'dayjs/locale/fr';
import 'dayjs/locale/de';
import 'dayjs/locale/es';
// prettier-ignore
// This code comes from /with-loading NextJS example (allows loading indicator at the top of the page)
// Current link : https://github.com/zeit/next.js/blob/canary/examples/with-loading/pages/_app.js
Router.events.on('routeChangeStart', () => nProgress.start());
Router.events.on('routeChangeComplete', () => nProgress.done());
Router.events.on('routeChangeError', () => nProgress.done());
const dev = process.env.NODE_ENV !== 'production';
function CustomApp({ Component, pageProps }) {
  const [credentials, setCredentials] = useState<Credentials>({
    authorization: cookie.get('authorization'),
    userId: cookie.get('userId'),
  });
  const [klaro, setKlaro] = useState(null);
  const router = useRouter();
  const { locale, defaultLocale } = router;

  useEffect(() => {
    document.documentElement.lang = locale;
  }, [locale]);
  // add class "using-mouse" to body if user is using mouse.
  // We do this so we can remove style from all elements on focus, but add a special focus style if user uses the tab key to navigate
  // so all the elements he navigates to will be focused (accessibility purpose)
  const addBodyClass = () => document.body.classList.add('using-mouse');
  const removeBodyClass = (e) => e.keyCode === 9 && document.body.classList.remove('using-mouse');
  useEffect(() => {
    window.addEventListener('mousedown', addBodyClass);
    window.addEventListener('keydown', removeBodyClass);
    return () => {
      window.removeEventListener('mousedown', addBodyClass);
      window.removeEventListener('keydown', removeBodyClass);
    };
  });
  usePersistLocaleCookie(locale, defaultLocale);
  useEffect(() => {
    import('klaro').then((klaro) => setKlaro(klaro));
  });
  useSyncLogout(setCredentials);
  let manager;
  if (klaro && !dev) {
    manager = klaro?.getManager();
  }
  useGA(process.env.GOOGLE_ANALYTICS_ID, manager?.getConsent('googleAnalytics'));
  useGTM(process.env.TAGMANAGER_ARGS_ID, manager?.getConsent('googleTagManager'));
  useHotjar(process.env.HOTJAR_ID, manager?.getConsent('hotjar'));
  return (
    // <CacheProviderEmotion value={emotionCache}>
    <>
      {/* <GlobalStyles /> */}
      <ThemeProviderEmotion theme={theme}>
        <NotLoggedInModalProvider>
          <ApiProvider credentials={credentials}>
            <UserProvider credentials={credentials} setCredentials={setCredentials}>
              <ModalProvider>
                <SpaceViewAsContextProvider>
                  <Component {...pageProps} />
                </SpaceViewAsContextProvider>
              </ModalProvider>
            </UserProvider>
          </ApiProvider>
        </NotLoggedInModalProvider>
      </ThemeProviderEmotion>
    </>
    // </CacheProviderEmotion>
  );
}
export default CustomApp;
