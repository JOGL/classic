import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { UserContext } from 'contexts/UserProvider';
import Box from 'components/Box';
// import CommunityList from 'components/Community/CommunityList';
import MyFeed from 'components/Feed/MyFeed';
import Grid from 'components/Grid';
import Image2 from 'components/Image2';
import Layout from 'components/Layout';
import NeedCard from 'components/Need/NeedCard';
import A from 'components/primitives/A';
import Button from 'components/primitives/Button';
import SpaceCard from 'components/Space/SpaceCard';
import ProgramCard from 'components/Program/ProgramCard';
import ProjectList from 'components/Project/ProjectList';
import ProjectRecommended from 'components/Project/ProjectRecommended';
import Loading from 'components/Tools/Loading';
import useGet from 'hooks/useGet';
import Link from 'next/link';
import { css, styled } from 'twin.macro';
import { Tabs, TabPanels, TabPanel } from '@reach/tabs';
import { NavTab, TabListStyle } from 'components/Tabs/TabsStyles';

import 'intro.js/introjs.css';
import useUserData from 'hooks/useUserData';
import { useModal } from 'contexts/modalContext';
import { startHomeIntro, showOnboardingNewFeature } from 'utils/onboarding/index';
import { Program } from 'types';
import { Space } from 'types';

const CustomLoader = () => {
  return (
    <div
      style={{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: '#f0f0f0',
      }}
    >
      <img src="/images/logo_JOGL-02.png" alt="JOGL icon" width="150px" />
      <Loading />
    </div>
  );
};

interface PageProps {}
const Home: NextPage<PageProps> = () => {
  const { isConnected, userData } = useContext(UserContext);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, [isConnected]);

  if (!loading) {
    if (isConnected) {
      return <HomeConnected user={userData} />;
    }
    return <HomeNotConnected />;
  } else return <CustomLoader />;
};

const HomeConnected = ({ user }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const [userInfo, setUserInfo] = useState(undefined);
  const { data: projects, error: projectsError } = useGet('/api/projects?items=8&order=desc');
  const { data: needs, error: needsError } = useGet('/api/needs?items=4&order=desc');
  const { data: recommendedNeeds } = useGet('/api/needs/recommended');
  const { data: recommendedProjects } = useGet('/api/projects/recommended');
  const prod = process.env.ADDRESS_FRONT === 'https://app.jogl.io';
  const featuredProgramId = !prod ? 1 : 12;
  const { data: featuredProgram } = useGet<Program>(`/api/programs/${featuredProgramId}`);
  const featuredSpaceId = !prod ? 1 : 4; // corresponding to test space in dev env & afterigem in prod env
  const { data: featuredSpace } = useGet<Space>(`/api/spaces/${featuredSpaceId}`);

  const isOnboarding = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOnboarding'));
  const isAnIndividual = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isIndividual'));
  // const isAnOrganization = typeof window !== "undefined" && JSON.parse(localStorage.getItem('isOrganization'));
  const isDesktop = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isDesktop'));
  const sawOnboardingFeature =
    typeof window !== 'undefined' && JSON.parse(localStorage.getItem('sawOnboardingFeature'));
  const { userData } = useUserData();
  const modal = useModal();
  const hasRecommendedObjects = recommendedNeeds?.length !== 0 && recommendedProjects?.length !== 0;

  // [Onboarding][6] - Retaking onboarding flow on last page, home
  useEffect(() => {
    if (isOnboarding && isAnIndividual) {
      setTimeout(() => {
        startHomeIntro(modal, t, isDesktop);
      }, 2000);
    }
  }, [isOnboarding, isAnIndividual]);

  // [Onboarding][1][Former user but new feature] - Trigger announcing modal
  useEffect(() => {
    setUserInfo(userData);
  }, [userData]);

  useEffect(() => {
    if (sawOnboardingFeature !== true && userInfo) {
      showOnboardingNewFeature(modal, t, router, userInfo);
    }
  }, [sawOnboardingFeature, userInfo]);

  // TODO: after app redesign, get rid of bootstrap tabs (02.2021)
  // Example of code http://dontpad.com/homewithoutbootstrap .
  return (
    <>
      {user.id ? (
        <Layout>
          <div className="connectedHome" tw="mt-24 flex flex-col mx-auto md:px-4">
            <nav className="nav nav-tabs" tw="py-0 px-2 lg:hidden">
              <a className="nav-item nav-link active feedtab" tw="p-2" href="#feed" data-toggle="tab">
                <h5 tw="text-lg">{t('feed.title')}</h5>
              </a>
              <a className="nav-item nav-link newstab" tw="p-2" href="#latest" data-toggle="tab">
                <h5 tw="text-lg">{t('home.latest')}</h5>
              </a>
            </nav>

            <div className="tabContainer" tw="mt-5 lg:(w-full mt-0) ">
              <div className="tab-content" tw="flex flex-wrap mx-0 md:-mx-4">
                <div
                  className="tab-pane active col-12 col-lg-8 col-xl-6"
                  tw="px-0 w-full relative sm:px-4 md:pr-2 xl:(px-0 pr-3)"
                  id="feed"
                >
                  <MyFeed user={user} />
                </div>
                <div
                  className="tab-pane active col-12 col-lg-4 col-xl-6"
                  tw="px-4 w-full relative md:pl-2 xl:(px-0 pl-3)"
                  id="latest"
                >
                  <div className="justify-content-center">
                    {/* Featured Program */}
                    {/* <h3>{t('home.featuredProgram')}</h3>
                    {featuredProgram ? (
                      <Grid tw="grid-template-columns[repeat(auto-fill, 100%)]">
                        <ProgramCard
                          id={featuredProgram.id}
                          short_title={featuredProgram.short_title}
                          title={featuredProgram.title}
                          title_fr={featuredProgram.title_fr}
                          short_description={featuredProgram.short_description}
                          short_description_fr={featuredProgram.short_description_fr}
                          banner_url={featuredProgram.banner_url || '/images/default/default-program.jpg'}
                          has_saved={featuredProgram.has_saved}
                          cardFormat="compact"
                          width="100%"
                        />
                      </Grid>
                    ) : (
                      <Loading />
                    )} */}
                    {/* Featured (Space) */}
                    <h3>{t('home.featured')}</h3>
                    {featuredSpace ? (
                      <Grid tw="grid-template-columns[repeat(auto-fill, 100%)]">
                        <SpaceCard
                          id={featuredSpace.id}
                          short_title={featuredSpace.short_title}
                          title={featuredSpace.title}
                          short_description={featuredSpace.short_description}
                          membersCount={featuredSpace.members_count}
                          needsCount={featuredSpace.needs_count}
                          projectsCount={featuredSpace.projects_count}
                          spaceType={featuredSpace.space_type}
                          banner_url={featuredSpace.banner_url || '/images/default/default-space.jpg'}
                          cardFormat="compact"
                          width="100%"
                        />
                      </Grid>
                    ) : (
                      <Loading />
                    )}
                    {/* Recommended and Latest elements */}
                    <div tw="mt-8">
                      {/* set default tab to "latest" (index 1) if user doesn't have recommended objects */}
                      <Tabs defaultIndex={hasRecommendedObjects ? 0 : 1}>
                        {/* show tabs only if user has recommended objects */}
                        {hasRecommendedObjects && (
                          <TabListStyle>
                            <NavTab>{t('home.recommendedTab')}</NavTab>
                            <NavTab>{t('home.latestTab')}</NavTab>
                          </TabListStyle>
                        )}
                        <TabPanels>
                          <TabPanel>
                            <section>
                              {/* Recommended needs */}
                              <div className="latestNeeds">
                                <h3 tw="mt-8">{t('needs.recommended')}</h3>
                                {recommendedNeeds ? (
                                  <Grid tw="pt-3">
                                    {[...recommendedNeeds]
                                      .filter((need) => need.targetable_node !== null)
                                      .map((need, i) => (
                                        // or just {recommendedNeeds.map((need, i) => (
                                        <NeedCard
                                          key={i}
                                          title={need.targetable_node?.need.title}
                                          project={need.targetable_node?.need.project}
                                          hasSaved={need.targetable_node?.need.has_saved}
                                          id={need.targetable_node?.need.id}
                                          cardFormat="compact"
                                        />
                                      ))
                                      // get only 4 needs
                                      .slice(0, 4)}
                                  </Grid>
                                ) : (
                                  <Loading />
                                )}
                              </div>
                              {/* Recommended projects */}
                              <div className="projectsList" tw="mt-10">
                                <h3 style={{ margin: '10px 0 0' }}>{t('projects.recommended')}</h3>
                                {recommendedProjects && (
                                  // do not use project list!
                                  <ProjectRecommended
                                    listProjects={recommendedProjects
                                      .filter((need) => need.targetable_node !== null)
                                      .filter((proj) => proj.targetable_node?.project?.status !== 'draft')
                                      .slice(0, 4)} // don't show draft projects, and get first 4 projects
                                    gridCols={[1, 2, 1, 2]}
                                    cardFormat="compact"
                                  />
                                )}
                              </div>
                            </section>
                          </TabPanel>
                          <TabPanel>
                            <section>
                              {/* Latest needs */}
                              <h3 style={{ margin: '24px 0 0' }}>{t('needs.latest')}</h3>
                              {needs ? (
                                <Grid tw="pt-3">
                                  {[...needs].reverse().map((need) => (
                                    <NeedCard
                                      key={need.id}
                                      title={need.title}
                                      project={need.project}
                                      hasSaved={need.has_saved}
                                      id={need.id}
                                      cardFormat="compact"
                                    />
                                  ))}
                                </Grid>
                              ) : (
                                <Loading />
                              )}
                              <Box pt={1} pb={4}>
                                <A href="/search/needs" passHref>
                                  {t('home.viewMoreRecent')}
                                </A>
                              </Box>
                              {/* Latest projects */}
                              <h3 style={{ margin: '10px 0 0' }}>{t('projects.latest')}</h3>
                              {projects && (
                                <ProjectList
                                  listProjects={projects.filter(({ status }) => status !== 'draft').slice(0, 4)} // don't show draft projects, and get first 4 last projects out of the 6
                                  gridCols={[1, 2, 1, 2]}
                                  cardFormat="compact"
                                />
                              )}
                              <Box pt={1} pb={4}>
                                <A href="/search/projects" passHref>
                                  {t('home.viewMoreRecent')}
                                </A>
                              </Box>
                            </section>
                          </TabPanel>
                        </TabPanels>
                      </Tabs>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Layout>
      ) : (
        // Splash loader
        <CustomLoader />
      )}
    </>
  );
};

const HomeNotConnected = () => {
  const { t } = useTranslation('common');
  const { data: programs } = useGet('/api/programs?items=3');

  return (
    <Layout className="no-margin">
      <Background>
        <div tw="max-width[1280px] mx-auto pb-20 px-6 xl:px-3">
          <div
            className="heading"
            tw="flex flex-col items-center pt-12 text-gray-800 mb-8 md:pt-16 lg:(flex-row justify-between) xl:pt-20"
          >
            <div tw="max-width[60ch] flex flex-col flex-1 items-center lg:(items-start justify-between flex-auto)">
              <h1 tw="text-center font-normal font-size[2rem] leading-tight lg:(text-left font-size[2.6rem])">
                {t('home.intro.title')}
              </h1>
              <p tw="text-lg text-justify mt-8 mb-12 md:text-xl lg:(text-left text-2xl)">{t('home.intro.subTitle')}</p>
              <Link href="/signup">
                <Button tw="px-12 py-3 text-lg lg:(text-2xl px-14 py-3.5)">{t('home.intro.join')}</Button>
              </Link>
            </div>
            <div tw="flex flex-none w-full mx-auto justify-center mt-8 lg:(mt-0 w-2/4)">
              <Image2
                tw="w-full sm:w-80! md:w-96! lg:w-full!"
                src="/images/home/Heading.svg"
                alt="Heading Homepage"
                hastw
              />
            </div>
          </div>
          <div tw="flex flex-col mt-24 md:mt-40" className="content">
            <h2 tw="uppercase font-medium text-2xl lg:text-3xl">{t('home.howItWorks')}</h2>
            <div
              tw="flex flex-col justify-center mt-4 gap-y-8 gap-x-0 my-5 mx-auto lg:(flex-row gap-y-0 gap-x-20)"
              className="cards"
            >
              <div className="singleCard" tw="w-3/4 mx-auto sm:w-2/4 lg:w-2/5">
                <Image2 tw="(flex mx-auto w-3/5 mb-3)!" src="/images/home/Community.png" alt="Community" hastw />
                <div className="cardContent" tw="-mt-20 pt-12 px-6 pb-8 shadow-md rounded-xl bg-white">
                  <h3 tw="mt-8 uppercase text-primary text-center text-2xl font-medium leading-9">
                    {t('home.community.title')}
                  </h3>
                  <p tw="text-base leading-5 h-16">{t('home.community.valueProposal')}</p>
                </div>
              </div>
              <div className="singleCard" tw="w-3/4 mx-auto sm:w-2/4 lg:w-2/5">
                <Image2 tw="(flex mx-auto w-3/5 mb-3)!" src="/images/home/Contribute.svg" alt="Contribute" hastw />
                <div className="cardContent" tw="-mt-20 pt-12 px-6 pb-8 shadow-md rounded-xl bg-white">
                  <h3 tw="mt-8 uppercase text-primary text-center text-2xl font-medium leading-9">
                    {t('home.contribute.title')}
                  </h3>
                  <p tw="text-base leading-5 h-16">{t('home.contribute.valueProposal')}</p>
                </div>
              </div>
              <div className="singleCard" tw="w-3/4 mx-auto sm:w-2/4 lg:w-2/5">
                <Image2 tw="(flex mx-auto w-3/5 mb-3)!" src="/images/home/Create.svg" alt="Contribute" hastw />
                <div className="cardContent" tw="-mt-20 pt-12 px-6 pb-8 shadow-md rounded-xl bg-white">
                  <h3 tw="mt-8 uppercase text-primary text-center text-2xl font-medium leading-9">
                    {t('home.create.title')}
                  </h3>
                  <p tw="text-base leading-5 h-16">{t('home.create.valueProposal')}</p>
                </div>
              </div>
            </div>
          </div>

          {programs ? (
            <>
              <h2 tw="uppercase font-medium text-2xl mt-24 lg:text-3xl">{t('home.latestPrograms')}</h2>
              <Grid>
                {programs.map((program) => {
                  return (
                    <ProgramCard
                      key={program.id}
                      id={program.id}
                      short_title={program.short_title}
                      title={program.title}
                      title_fr={program.title_fr}
                      short_description={program.short_description}
                      short_description_fr={program.short_description_fr}
                      banner_url={program.banner_url || '/images/default/default-program.jpg'}
                      cardFormat="compact"
                    />
                  );
                })}
              </Grid>
            </>
          ) : (
            <Loading />
          )}
        </div>
      </Background>
    </Layout>
  );
};

const Background = styled.div(() => [
  css`
    background-color: #fdfdfd;
    color: #5c5d5d;
    background-image: url("data:image/svg+xml,%3Csvg width='100' height='100' viewBox='0 0 100 100' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M11 18c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm48 25c3.866 0 7-3.134 7-7s-3.134-7-7-7-7 3.134-7 7 3.134 7 7 7zm-43-7c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm63 31c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM34 90c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zm56-76c1.657 0 3-1.343 3-3s-1.343-3-3-3-3 1.343-3 3 1.343 3 3 3zM12 86c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm28-65c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm23-11c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-6 60c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm29 22c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zM32 63c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm57-13c2.76 0 5-2.24 5-5s-2.24-5-5-5-5 2.24-5 5 2.24 5 5 5zm-9-21c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM60 91c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM35 41c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zM12 60c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2z' fill='%23cacaca' fill-opacity='0.05' fill-rule='evenodd'/%3E%3C/svg%3E");
  `,
]);

export default Home;

export async function getStaticProps(context) {
  return { props: {} };
}
