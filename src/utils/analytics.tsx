import ReactGA from 'react-ga';

const dev = process.env.NODE_ENV !== 'production';

// Google Universal Analytics
export const initGA = (propertyId) => {
  ReactGA.initialize(propertyId);
};

export const logPageView = () => {
  ReactGA.set({ page: window.location.pathname + window.location.search });
  ReactGA.pageview(window.location.pathname + window.location.search);
};

export const logEvent = (category = '', action = '', label = '') => {
  if (category && action) {
    ReactGA.event({ category, action, label });
  }
};

export const logException = (description = '', fatal = false) => {
  if (description) {
    ReactGA.exception({ description, fatal });
  }
};

export function logEventToGA(action, category, customParamsGA, customParamsGA4) {
  // send event to GA Universal (old)
  ReactGA.event({ category, action, label: customParamsGA });
  // send event to GA4 (new)
  if (typeof window !== undefined && !dev) window.gtag('event', action, { category: category, ...customParamsGA4 });
}
