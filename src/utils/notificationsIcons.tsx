import { CommentDots } from '@emotion-icons/boxicons-regular';
import { Feed } from '@emotion-icons/icomoon';
import { PersonPlusFill } from '@emotion-icons/bootstrap';
import { Mention } from '@emotion-icons/octicons';
import { AdminPanelSettings, Event } from '@emotion-icons/material';
import { UserGroup } from '@emotion-icons/heroicons-outline';
import { SignLanguage } from '@emotion-icons/fa-solid';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import styled from 'utils/styled';

export const NotifIconWrapper = styled.div`
  ${EmotionIconBase} {
    color: ${(p) => p.theme.colors.primary};
    width: 25px;
    height: 25px;
  }
`;

export const notifIconTypes = {
  administration: <AdminPanelSettings title="Admin notification" />,
  clap: <SignLanguage title="Clap notification" />,
  comment: <CommentDots title="Comment notification" />,
  follow: <PersonPlusFill title="Follow notification" />,
  feed: <Feed title="Feed notification" />,
  program: <Event title="Program notification" />,
  mention: <Mention title="Mention notification" />,
  membership: <UserGroup title="Membership notification" />,
  space: <Event title="Space notification" />,
};
