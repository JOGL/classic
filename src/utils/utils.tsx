import React, { Fragment, useRef, useEffect, useState } from 'react';
import Link from 'next/link';
import A from 'components/primitives/A';
import axios from 'axios';
// Formatting dates with day.js
import day from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';
import relativeTime from 'dayjs/plugin/relativeTime';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import { styled, theme } from 'twin.macro';

export function linkify(content) {
  // detect links in a text and englobe them with a <a> tag
  const urlRegex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm;
  return content?.replace(urlRegex, (url) => {
    const addHttp = url.substr(0, 4) !== 'http' ? 'http://' : '';
    return `<a href="${addHttp}${url}" target="_blank" rel="noopener">${url}</a>`;
  });
}

export function linkifyQuill(text) {
  // detect links outside of <a> tag and linkify them (only for quill content)
  const exp = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
  return text.replace(exp, function () {
    return arguments[1] ? arguments[0] : `<a href="${arguments[3]}" target='_blank' rel="noopener">${arguments[3]}</a>`;
  });
}

export const useScrollHandler = (elOffSetTop: number) => {
  // setting initial value to true
  const [scroll, setScroll] = useState(false);
  // running on mount
  useEffect(() => {
    const onScroll = () => {
      const scrollCheck = window.scrollY > elOffSetTop;
      if (scrollCheck !== scroll) {
        setScroll(scrollCheck);
      }
    };
    // setting the event handler from web API
    document.addEventListener('scroll', onScroll);
    // cleaning up from the web API
    return () => {
      document.removeEventListener('scroll', onScroll);
    };
  }, [scroll, setScroll, elOffSetTop]);
  return scroll;
};

export function renderOwnerNames(users, projectCreator, t) {
  let noOwner = true;
  users.map((user) => {
    if (user.owner) noOwner = false;
  }); // map through users, and set noOwner to false if there are no owners
  return (
    <p className="card-by">
      {t('entity.card.by')}
      {users
        .filter((user) => user.owner)
        .map((user, index, users) => {
          // filter to get only owners, and then map
          const rowLen = users.length;
          if (index < 6) {
            return (
              <Fragment key={index}>
                <Link href={`/user/${user.id}`}>
                  <a>{`${user.first_name} ${user.last_name}`}</a>
                </Link>
                {/* add comma, except for last item */}
                {rowLen !== index + 1 && <span>, </span>}
              </Fragment>
            );
          }
          if (index === 6) {
            return '...';
          }
          return '';
        })}
      {noOwner && ( // if project don't have any owner, display creator as the owner
        <Link href={`/user/${projectCreator.id}`}>
          <a>{`${projectCreator.first_name} ${projectCreator.last_name}`}</a>
        </Link>
      )}
    </p>
  );
}

const BubbleTeam = styled.div`
  display: flex;
  > span:nth-child(1) {
    margin-right: 3px;
  }
  .userImg {
    background-size: cover;
    width: 43px;
    height: 43px;
    border-radius: 50%;
    background-position: center center;
    background-repeat: no-repeat;
    margin: 2px -16px 2px 2px;
    background-color: white;
    border: 1px solid lightgray;
    &:hover {
      opacity: 0.9;
    }
  }

  a:hover {
    text-decoration: none;
  }

  .moreMembers {
    width: 45px;
    height: 45px;
    background: blue;
    color: white;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    display: inline-flex;
    font-size: 15.8px;
    &:hover {
      opacity: 0.9;
    }
  }
`;

export function renderTeam(users, objType, objSlug, members_count, type = 'external') {
  return (
    <BubbleTeam>
      {users?.map((user, index) => (
        <Link href={`/user/${user.id}`} key={index}>
          <a>
            <div
              className="userImg"
              style={{ backgroundImage: `url(${user.logo_url || '/images/default/default-user.png'})` }}
            />
          </a>
        </Link>
      ))}
      {/* if obj has more than 6 members, show a bubble with the left members number, linking to different source depending on "type" prop */}
      {members_count > 6 && type === 'internal' && (
        <A href={`/${objType}/${objSlug}?tab=members`} shallow noStyle scroll={false}>
          <div className="moreMembers">+{members_count - 6}</div>
        </A>
      )}
      {members_count > 6 && type !== 'internal' && (
        <A href={`/${objType}/${objSlug}`} noStyle>
          <div className="moreMembers">+{members_count - 6}</div>
        </A>
      )}
    </BubbleTeam>
  );
}

export function formatNumber(value) {
  return Number(value).toLocaleString();
}

export function displayObjectDate(date, format = 'L', noRelativeTime = false) {
  // https://day.js.org/docs/en/display/format#list-of-localized-formats
  day.extend(localizedFormat);
  day.extend(relativeTime);
  const objectCreatedDate = new Date(date);
  const { locale } = useRouter();
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // else, if date diff less than 1 day (3600sec * 24h), AND noRelativeTime param is false, display related date (eg: a few seconds ago, 4 days ago...)
  if (dateDiff < 86400 && !noRelativeTime) return day(objectCreatedDate).locale(locale).fromNow();
  // else display date with day, month & year
  return day(objectCreatedDate).locale(locale).format(format);
}

export function displayObjectRelativeDate(date) {
  const objectCreatedDate = new Date(date);
  const { locale } = useRouter();
  day.extend(localizedFormat);
  day.extend(relativeTime);
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  if (dateDiff > 3974400) return day(objectCreatedDate).format('DD/MM/YYYY'); // if date is approx older than 1 month, show full date
  return day(objectCreatedDate).locale(locale).fromNow(); // else show relative date
}

export function isDateUrgent(date) {
  const objectCreatedDate = new Date(date);
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // else, if date diff less than 2 days (3600sec * 24h * 2), display related date without time update
  if (dateDiff < 86400) return true;
}

// return first link of a post (the one we want the metadata from)
export function returnFirstLink(content) {
  if (content) {
    const regexLinks = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gm; // match all links (http/https only!)
    const match = content.match(regexLinks);
    if (match) {
      const firstLink = match[0];
      return firstLink;
    }
  }
}

// default settings/params for a toast alert
export const confAlert = Swal.mixin({
  toast: true,
  position: 'bottom-start',
  showConfirmButton: false,
  timer: 3000,
});

// default settings/params for the "changes were saved" modal
export function changesSavedConfAlert(t) {
  return Swal.mixin({
    icon: 'success',
    title: t('general.editSuccessMsg'),
    showCancelButton: true,
    confirmButtonColor: theme`colors.primary`,
    confirmButtonText: t('general.ok'),
    cancelButtonText: t('entity.form.confModal.continueEditing'),
  });
}

export function copyLink(objectId, objectType, commentId, t) {
  // function to copy post link to user clipboard, we use a tweak because the browser don't like we force copy to clipboard
  const el = document.createElement('textarea'); // create textarea element
  const link =
    objectType !== 'comment'
      ? `${window.location.origin}/${objectType}/${objectId}` // if objectType is not comment, make its value be the object url
      : objectType === 'comment' && `${window.location.origin}/post/${objectId}#comment-${commentId}`; // if it' a comment, make its value be the comment single url
  el.value = link;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute'; // put some style that make it not visible
  el.style.left = '-9999px';
  document.body.appendChild(el); // add element to html (body)
  el.select(); // select the textarea text
  document.execCommand('copy'); // copy content to user clipboard
  document.body.removeChild(el); // remove element
  // show conf alert
  confAlert.fire({
    icon: 'success',
    title:
      objectType === 'post'
        ? t('feed.object.copyLink_conf')
        : objectType === 'comment'
        ? t('feed.object.comment.copyLink_conf')
        : t('general.copyLink_conf'),
  });
}

export function reportContent(contentType, postId, commentId, api, t) {
  // function to send mail with the reported post to a JOGL admin
  const reportLink =
    contentType === 'post'
      ? `${window.location.origin}/post/${postId}`
      : `${window.location.origin}/post/${postId}#comment-${commentId}`;
  const param = {
    object: `${contentType} report`, // mail subject
    content: `The user reported the following ${contentType}: ${reportLink}`, // mail content, with reported post/comment link
  };
  api
    .post('/api/users/2/send_email', param) // send it to user 2, which is JOGL admin user
    .then(() => {
      // show conf alert
      confAlert.fire({
        icon: 'success',
        title: contentType === 'post' ? t('feed.object.reportPost.conf') : t('feed.object.reportComment.conf'),
      });
    });
}

export function useWhyDidYouUpdate(name, props) {
  // Get a mutable ref object where we can store props ...
  // ... for comparison next time this hook runs.
  const previousProps = useRef();

  useEffect(() => {
    if (previousProps.current) {
      // Get all keys from previous and current props
      const allKeys = Object.keys({ ...previousProps.current, ...props });
      // Use this object to keep track of changed props
      const changesObj = {};
      // Iterate through keys
      allKeys.forEach((key) => {
        // If previous is different from current
        if (previousProps.current[key] !== props[key]) {
          // Add to changesObj
          changesObj[key] = {
            from: previousProps.current[key],
            to: props[key],
          };
        }
      });
    }

    // Finally update previousProps with current props for next hook call
    previousProps.current = props;
  });
}

export function defaultSdgsInterests(translate) {
  return [
    { value: 1, label: translate('sdg-title-1'), color: '#EC1B30' },
    { value: 2, label: translate('sdg-title-2'), color: '#D49F2B' },
    { value: 3, label: translate('sdg-title-3'), color: '#2DA354' },
    { value: 4, label: translate('sdg-title-4'), color: '#CB233B' },
    { value: 5, label: translate('sdg-title-5'), color: '#ED4129' },
    { value: 6, label: translate('sdg-title-6'), color: '#02B7DF' },
    { value: 7, label: translate('sdg-title-7'), color: '#FEBF12' },
    { value: 8, label: translate('sdg-title-8'), color: '#981A41' },
    { value: 9, label: translate('sdg-title-9'), color: '#F37634' },
    { value: 10, label: translate('sdg-title-10'), color: '#DE1768' },
    { value: 11, label: translate('sdg-title-11'), color: '#FA9D26' },
    { value: 12, label: translate('sdg-title-12'), color: '#C69932' },
    { value: 13, label: translate('sdg-title-13'), color: '#498A50' },
    { value: 14, label: translate('sdg-title-14'), color: '#347DB7' },
    { value: 15, label: translate('sdg-title-15'), color: '#40B04A' },
    { value: 16, label: translate('sdg-title-16'), color: '#02558B' },
    { value: 17, label: translate('sdg-title-17'), color: '#1B366B' },
  ];
}

export function getCountriesAndCitiesList(
  countriesCitiesData,
  setCountriesCitiesData,
  countryNotInList,
  setCountryNotInList,
  setCountriesList,
  setCitiesList,
  selectedCountry
) {
  const apiUrl = 'https://gitlab.com/api/v4/projects/9964040/snippets/2084062/raw';

  async function getCountriesCities() {
    await axios.get(apiUrl).then((response) => setCountriesCitiesData(response.data));
  }

  useEffect(() => {
    getCountriesCities();
  }, []);

  useEffect(() => {
    if (countriesCitiesData) {
      // check if current user country is in the official array of countries (had to do this for when users could type their country with a text input, and they could type it differently..)
      setCountryNotInList(!Object.keys(countriesCitiesData).includes(selectedCountry));

      setCountriesList(
        Object.keys(countriesCitiesData) // an array of all the countries from the list
          .sort((a, b) => a.localeCompare(b))
          .map((content) => {
            return { value: content, label: content };
          })
      );

      if (selectedCountry && !countryNotInList) {
        setCitiesList(
          countriesCitiesData[selectedCountry] // to access the country array containing all its cities
            ?.sort((a, b) => a.localeCompare(b))
            .map((content) => {
              return { value: content, label: content };
            })
        );
      }
    }
  }, [countriesCitiesData, selectedCountry, countryNotInList]);
}

// How to save the user-defined language: https://github.com/vinissimus/next-translate#10-how-to-save-the-user-defined-language
export function usePersistLocaleCookie() {
  const { locale, defaultLocale } = useRouter();

  useEffect(persistLocaleCookie, [locale, defaultLocale]);
  function persistLocaleCookie() {
    const date = new Date();
    const expireMs = 100 * 365 * 24 * 60 * 60 * 1000; // 100 days
    date.setTime(date.getTime() + expireMs);
    document.cookie = `NEXT_LOCALE=${locale};expires=${date.toUTCString()};path=/`;
  }
}

export function kFormatter(num) {
  return Math.abs(num) > 999
    ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + 'k'
    : Math.sign(num) * Math.abs(num);
}

export function formatNumberToShowCommas(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

export function noScroll() {
  window.scrollTo(0, 0);
}
// To take away & restaure the user's ability to scroll, like so:
// window.addEventListener('scroll', noScroll);
// window.removeEventListener('scroll', noScroll);
