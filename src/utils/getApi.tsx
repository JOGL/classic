import React from 'react';
import axios from 'axios';
import nextCookie from 'next-cookies';
import useTranslation from 'next-translate/useTranslation';
import Router, { useRouter } from 'next/router';
import Button from 'components/primitives/Button';
import { INotLoggedInModalContext } from 'contexts/notLoggedInModalContext';
import cookie from 'js-cookie';

const getApi = ({ authorization, userId, t }, notLoggedInModal?: INotLoggedInModalContext) => {
  const BACKEND_URL = `${process.env.PROTOCOL}://${process.env.HOSTNAME}:${process.env.BACKEND_PORT}`;
    
  // Create the api
  const api = axios.create({
    baseURL: BACKEND_URL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

  // Intercepts requests to add the credentials
  // cookies to every axios requests if connected
  api.interceptors.request.use(
    (config) => {
      if (authorization) {
        const common = { Authorization: authorization, userId: userId };
        // add the new common without deleting some extra keys in config
        return { ...config, headers: { ...config.headers, common: { ...config.headers.common, ...common } } };
      }
      return config;
    },
    (error) => Promise.reject(error)
  );

  // Intercepts responses to handles errors.
  api.interceptors.response.use(
    (response) => response,
    (error) => {
      const status = error?.response?.status;
      // check if user is logged in with old credentials (before integrating omniAuth with https://gitlab.com/JOGL/backend-v0.1/-/merge_requests/317)
      const loggedInWithOldCredentials = error?.response?.data.error === 'Signature verification raised';
      // check if user's signature has expired
      const hasExpiredSession = error?.response?.data.error === 'Signature has expired';
      // Handle 401 unauthorized error.
      if (status === 401) {
        // if 401 error on all pages except sign in
        if (!document?.location?.pathname.includes('/signin')) {
          if (loggedInWithOldCredentials || hasExpiredSession) {
            // if true, force logout and redirect to sign in page
            cookie.remove('authorization');
            cookie.remove('userId');
            typeof window !== 'undefined' && window.localStorage.setItem('logout', Date.now().toString());
            Router.push('/signin').then(() => Router.reload());
          } else if (notLoggedInModal) {
            const { showModal } = notLoggedInModal;
            // ! The reason we cannot use the classic ModalContext is that it isn't available in this scope
            // ! since ModalProvider must be a child of UserProvider and ApiProvider to add any type of component;
            // ! That explains the existence of the NotLoggedInContext which won't have access to the context the contexts cited above
            // ! Which is fine.
            showModal({
              title: t('footer.modalSignIn.title'),
              maxWidth: '25rem',
            });
          }
        }
      }
      // ? To get better errors when using e.g. api.get() it should return only the error without the Promise
      // ? But when using SWR, it requires an error as a Promise to use errors. So maybe we should leave it with the Promise
      // ? to enforce the usage of SWR.
      return Promise.reject(error);
    }
  );
  return api;
};

/**
 * @param {*} ctx NextJS getInitialProps Context
 * @returns api
 */
export const getApiFromCtx = (ctx) => {
  // Get the cookies if they are present
  const { authorization, userId } = nextCookie(ctx);
  return getApi({ authorization, userId });
};

export const NotLoggedInModal = ({ hideModal }) => {
  // ! When using this component with NotLoggedInModalContext, it won't have access to the ApiContext nor the UserContext.
  const router = useRouter();
  const { t } = useTranslation('common');
  return (
    <>
      <p>{t('footer.modalSignIn.text')}</p>
      <Button
        onClick={() => {
          hideModal();
          router.push({ pathname: '/signin', query: { redirectUrl: router.asPath } });
        }}
      >
        {/* Pass current URL to sign in, so that it redirects there after sign in */}
        {t('header.signIn')}
      </Button>
    </>
  );
};

export default getApi;
