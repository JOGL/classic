import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';

export const TextWithPlural = ({ type, count = 0 }) => {
  const router = useRouter();
  const { locale } = router;
  // if count is zero, make it 1, cause we have same translation if count is 0 or 1 (so we don't need to add same translation twice,
  // and if locale is english, display plural form when count is 0)
  const translatedCount = count === 0 ? (locale === 'en' ? 2 : 1) : count;
  const { t } = useTranslation('common');
  return <>{t(`general.${type}`, { count: translatedCount })}</>;
};
