import Button from 'components/primitives/Button';
import tw, { styled } from 'twin.macro';

const EmojiModal = styled.div`
  ${tw`mx-auto text-5xl sm:text-6xl`}
`;

const PModal = styled.p`
  ${tw`text-black text-xl sm:text-2xl`}
`;

// rule for primary VS. secondary Buttons in all modals: primary for buttons making the user go to / stay on our feature (our most desired flow)

export const showCompleteProfileModal = (modal, t) => {
  localStorage.setItem('sawCompleteProfileModal', 'true');
  modal.showModal({
    children: (
      <>
        <EmojiModal>🙏</EmojiModal>
        <br />
        <PModal>{t('onBoarding.modalCompleteProfile.body-1')}</PModal>
        <PModal>{t('onBoarding.modalCompleteProfile.body-2')}</PModal>
        <br />
        <div tw="flex justify-around">
          <Button btnType="primary" tw="text-lg px-6 sm:text-xl" onClick={modal.closeModal}>
            {t('onBoarding.modalCompleteProfile.begin')}
          </Button>
        </div>
      </>
    ),
    title: t('onBoarding.modalCompleteProfile.title'),
    showCloseButton: false,
    maxWidth: '30rem',
    modalClassName: 'onBoardingModal',
  });
};

// futurely set individual vs organization wording and buttons
export const showOnboardingStartModal = (modal, setSawIntroModal, handleTabsChange, t, isNewUser) => {
  const titleModal = isNewUser ? t('onBoarding.modalUserProfile.title') : t('onBoarding.modalUserProfile.title-JOGLer');
  setSawIntroModal(true);
  modal.showModal({
    children: (
      <>
        <EmojiModal>🥳</EmojiModal>
        <br />
        <PModal>
          {isNewUser ? t('onBoarding.modalUserProfile.body-1') : t('onBoarding.modalUserProfile.body-1-JOGLer')}
        </PModal>
        {/* <PModal>{t('onBoarding.modalUserProfile.body-2')}</PModal> */}
        <br />
        <div tw="flex justify-around">
          <Button
            btnType="primary"
            tw="text-lg sm:text-xl"
            onClick={() => {
              handleTabsChange(0); // to be on feed tab so that onboarding step 4 works properly
              localStorage.setItem('isIndividual', 'true');
            }}
          >
            {/* {t('onBoarding.modalUserProfile.individual')} */}
            {isNewUser ? t('onBoarding.modalUserProfile.start') : t('onBoarding.modalUserProfile.start-JOGLer')}
          </Button>
          {/* <Button
                btnType="secondary"
                tw="text-lg sm:text-xl"
                onClick={() => {
                  handleTabsChange(0);
                  localStorage.setItem('isOrganisation', 'true');
                }}
              >
                {t('onBoarding.modalUserProfile.organization')}
              </Button> */}
        </div>
      </>
    ),
    title: `${titleModal}`,
    showCloseButton: false,
    maxWidth: '30rem',
    modalClassName: 'onBoardingModal',
  });
};

export const showOnboardingEndModal = (modal, t) => {
  modal.showModal({
    children: (
      <>
        <EmojiModal>🥳</EmojiModal>
        <br />
        <PModal>{t('onBoarding.modalEnd.body-1')}</PModal>
        <PModal>{t('onBoarding.modalEnd.body-2')}</PModal>
        <br />
        <div tw="flex m-auto space-x-6">
          <Button
            btnType="secondary"
            tw="text-lg sm:text-xl"
            onClick={() => window.open('https://jogl.tawk.help/', '_ blank')}
          >
            {t('entity.info.faq')}
          </Button>
          <Button btnType="primary" tw="text-lg sm:text-xl" onClick={modal.closeModal}>
            {t('onBoarding.modalEnd.discover')}
          </Button>
        </div>
      </>
    ),
    title: t('onBoarding.modalEnd.title'),
    maxWidth: '30rem',
    showCloseButton: true,
    modalClassName: 'onBoardingModal',
  });
};

export const showOnboardingNewFeature = (modal, t, router, userInfo) => {
  modal.showModal({
    children: (
      <>
        <EmojiModal>🙏</EmojiModal>
        <br />
        <PModal>{t('onBoarding.modalNewFeature.body-1')}</PModal>
        <PModal>{t('onBoarding.modalNewFeature.body-2')}</PModal>
        <PModal>{t('onBoarding.modalNewFeature.body-3')}</PModal>
        <PModal>{t('onBoarding.modalNewFeature.body-4')}</PModal>
        <br />
        <div tw="flex m-auto space-x-4">
          <Button
            btnType="primary"
            tw="text-lg sm:text-xl"
            onClick={() => {
              router.push(`/user/${userInfo.id}`); // go to user profile where the actual flow begins
              localStorage.setItem('isOnboarding', 'true'); // set onboarding to true
            }}
          >
            {t('onBoarding.modalNewFeature.yes')}
          </Button>
          <Button
            btnType="secondary"
            tw="text-lg sm:text-xl"
            onClick={() => {
              modal.closeModal();
              localStorage.setItem('sawOnboardingFeature', 'true'); // so the same current modal does not trigger again
            }}
          >
            {t('onBoarding.modalNewFeature.no')}
          </Button>
        </div>
      </>
    ),
    title: t('onBoarding.modalNewFeature.title'),
    maxWidth: '30rem',
    showCloseButton: false,
    modalClassName: 'onBoardingModal',
  });
};
