import introJs from 'intro.js';
import 'intro.js/introjs.css';
import { triggerOnExit, goToNextIntroPage, completeTour, showOnboardingEndModal } from './index';

// INTROJS Flows / Page

// TODOs: types

let intro;
let skipped;
let stepNumber;

// 1. User page
export const startUserIntro = (isAnIndividual, isAnOrganization, t, router, isDesktop) => {
  // const exploreElement = isDesktop ? document.querySelectorAll('#explore')[0] : null; // Tried to condition this way
  localStorage.setItem('multipage', 'true');
  intro = introJs();
  // INDIVIDUALS intro flow, the default flow so far
  if (isAnIndividual && isDesktop) {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.nextpage'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true, // interaction will be reactivated only on specific tooltips where needed
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        // introjs automatically decides of the best position for each tooltip, overriding the code decision if needed
        {
          element: document.querySelector('#collection'),
          // element: document.querySelector('#testwhendoesnotexist'),
          intro: t('onBoarding.userProfile.dataStep-1'),
          position: 'right',
        },
        {
          element: document.querySelector('#about'),
          intro: t('onBoarding.userProfile.dataStep-2'),
          position: 'bottom',
        },
        {
          element: document.querySelector('.feed'),
          intro: `${t('onBoarding.userProfile.dataStep-3a')}</br></br>${t('onBoarding.userProfile.dataStep-3b')}`,
          position: 'left',
          disableInteraction: false,
        },
        // TODO: ask for confirmation of the number of this edit profile step and of its wording
        {
          element: document.querySelector('#editProfile'),
          intro: t('onBoarding.userProfile.dataStep-4'),
        },
        {
          element: document.querySelectorAll('#explore')[0],
          intro: t('onBoarding.userProfile.dataStep-5'),
          position: 'right',
          tooltipClass: 'customTooltip',
        },
        // filter in case a step element is not present yet, so it gets skipped instead of breaking the flow. It may change the number of steps.
      ].filter((obj) => {
        return $(obj.element).length;
      }),
    });
  } else if (isAnIndividual && !isDesktop) {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.nextpage'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        {
          element: document.querySelector('#collection'),
          intro: t('onBoarding.userProfile.dataStep-1'),
          // tooltipClass: 'customTooltip correctOffTooltip'
        },
        {
          element: document.querySelector('#about'),
          intro: t('onBoarding.userProfile.dataStep-2'),
        },
        {
          element: document.querySelector('.feed'),
          intro: t('onBoarding.userProfile.dataStep-3a'),
        },
        {
          element: document.querySelector('#editProfile'),
          intro: t('onBoarding.userProfile.dataStep-4'),
        },
        {
          intro: t('onBoarding.userProfile.dataStep-5'),
        },
      ].filter((obj) => {
        return $(obj.element).length || obj.intro === t('onBoarding.userProfile.dataStep-5'); // to not skip explore floating element
      }),
    });
    // ORGANIZATIONS intro flow, to do after spaces is a public feature
  } else if (isAnOrganization) {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.nextstep'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      tooltipClass: 'customTooltip',
      steps: [
        {
          element: document.querySelectorAll('#explore')[0],
          intro: t('onBoarding.userProfile.dataStep-4b'),
          position: 'right',
        },
      ].filter((obj) => {
        return $(obj.element).length;
      }),
    });
  }
  // onbefore change triggers before each step. We want to scroll back to the top so every tooltip is always well positioned, whatever the number of steps.
  intro.onbeforechange(() => {
    window.scrollTo({ top: 0, behavior: 'auto' });
  });
  // onskip triggers only on skip button and always leads to onexit callback, even when user cancelled on confirmation alert
  intro.onskip(() => {
    if (confirm(t('onBoarding.btns.confirm'))) {
      skipped = true;
      localStorage.setItem('multipage', 'false');
    } else {
      skipped = false;
      stepNumber =
        intro._currentStep === intro._introItems.length - 1 ? intro._currentStep + 1 : intro._currentStep + 2;
      // to go to next step with the exception of when we are on the last step
    }
  });
  // onexit triggers both after skip button and done (last "next" of each page) button
  intro.onexit(() => {
    const isMultipage = JSON.parse(localStorage.getItem('multipage'));
    const nextUrl = '/search/projects';
    if (skipped) {
      // user confirmed he wanted to close mid-onboarding
      triggerOnExit(router);
    } else if (skipped === false) {
      // user cancelled intention to close mid-onboarding and goes back to the flow
      setTimeout(() => {
        skipped = undefined;
        // intro.nextStep(); // does not work because does not seem to know its current step when triggered
        intro.goToStepNumber(stepNumber);
      }, 500);
    } else if (skipped === undefined) {
      // user never touched close button or already cancelled it, and exit should trigger end or next page
      !isMultipage ? triggerOnExit(router) : goToNextIntroPage(router, nextUrl);
    }
  });
  intro.start();
};

// 2. Search page
export const startSearchIntro = (isAnIndividual, t, router, isDesktop) => {
  localStorage.setItem('multipage', 'true');
  intro = introJs();
  if (isAnIndividual && isDesktop) {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.next'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        {
          element: document.querySelector('#search-header'),
          intro: `${t('onBoarding.searchPage.dataStep-1a')}</br></br>${t('onBoarding.searchPage.dataStep-1b')}`,
          position: 'bottom',
          disableInteraction: false,
        },
        {
          element: document.querySelector('.ais-Hits-list .ais-Hits-item:first-child .saveBtn'),
          intro: `${t('onBoarding.searchPage.dataStep-2a')}</br></br>${t('onBoarding.searchPage.dataStep-2b')}`,
          position: 'left',
          disableInteraction: false,
        },
        {
          element: document.querySelector('#activePrograms'),
          intro: t('onBoarding.searchPage.dataStep-3'),
          position: 'bottom',
        },
      ].filter((obj) => {
        return $(obj.element).length;
      }),
    });
  } else if (isAnIndividual && !isDesktop) {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.next'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        {
          element: document.querySelector('#search-header'),
          intro: t('onBoarding.searchPage.dataStep-1a'),
        },
        {
          element: document.querySelector('.ais-Hits-list .ais-Hits-item:first-child .saveBtn'),
          intro: `${t('onBoarding.searchPage.dataStep-2a-mobile')}</br></br>${t(
            'onBoarding.searchPage.dataStep-2b-mobile'
          )}`,
        },
        {
          intro: t('onBoarding.searchPage.dataStep-3'),
        },
      ].filter((obj) => {
        return $(obj.element).length || obj.intro === t('onBoarding.searchPage.dataStep-3');
      }),
    });
  }
  // }
  // } else if (isAnOrganization) {
  // }
  intro.onbeforechange(() => {
    window.scrollTo({ top: 0, behavior: 'auto' });
  });
  intro.onskip(() => {
    if (confirm(t('onBoarding.btns.confirm'))) {
      skipped = true;
      localStorage.setItem('multipage', 'false');
    } else {
      skipped = false;
      stepNumber =
        intro._currentStep === intro._introItems.length - 1 ? intro._currentStep + 1 : intro._currentStep + 2;
    }
  });
  intro.onexit(() => {
    const isMultipage = JSON.parse(localStorage.getItem('multipage'));
    const nextUrl = '/program/opencovid19';
    if (skipped) {
      triggerOnExit(router);
    } else if (skipped === false) {
      setTimeout(() => {
        skipped = undefined;
        intro.goToStepNumber(stepNumber);
      }, 500);
    } else if (skipped === undefined) {
      !isMultipage ? triggerOnExit(router) : goToNextIntroPage(router, nextUrl);
    }
  });
  intro.start();
};

// 3. Program page
export const startProgramIntro = (t, router, isDesktop) => {
  localStorage.setItem('multipage', 'true');
  intro = introJs();
  if (isDesktop) {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.next'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        { intro: t('onBoarding.programPage.dataStep-1') },
        {
          element: document.querySelector('#followBtn'),
          intro: `${t('onBoarding.programPage.dataStep-2a')}</br></br>${t(
            'onBoarding.programPage.dataStep-2b'
          )}</br></br>${t('onBoarding.programPage.dataStep-3')}`,
          position: 'right',
          disableInteraction: false,
        },
        {
          element: document.querySelector('#nav'),
          intro: `${t('onBoarding.programPage.dataStep-4a')}</br></br>${t('onBoarding.programPage.dataStep-4b')}`,
          position: 'right',
        },
        {
          element: document.querySelector('#home-link-desktop'),
          intro: t('onBoarding.programPage.dataStep-5'),
        },
      ].filter((obj) => {
        return $(obj.element).length || obj.intro === t('onBoarding.programPage.dataStep-1'); // to include the first floating element step
      }),
    });
  } else {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.next'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        { intro: t('onBoarding.programPage.dataStep-1') },
        {
          element: document.querySelector('#followBtn'),
          intro: t('onBoarding.programPage.dataStep-2a'),
        },
        {
          element: document.querySelector('#nav'),
          intro: `${t('onBoarding.programPage.dataStep-4a')}</br></br>${t('onBoarding.programPage.dataStep-4b')}`,
        },
        {
          element: document.querySelector('#home-link-mobile'),
          intro: t('onBoarding.programPage.dataStep-5'),
        },
      ].filter((obj) => {
        return $(obj.element).length || obj.intro === t('onBoarding.programPage.dataStep-1');
      }),
    });
  }
  intro.onbeforechange(() => {
    window.scrollTo({ top: 0, behavior: 'auto' });
  });
  intro.onskip(() => {
    if (confirm(t('onBoarding.btns.confirm'))) {
      skipped = true;
      localStorage.setItem('multipage', 'false');
    } else {
      skipped = false;
      stepNumber =
        intro._currentStep === intro._introItems.length - 1 ? intro._currentStep + 1 : intro._currentStep + 2;
    }
  });
  intro.onexit(() => {
    const isMultipage = JSON.parse(localStorage.getItem('multipage'));
    const nextUrl = '/';
    if (skipped) {
      triggerOnExit(router);
    } else if (skipped === false) {
      setTimeout(() => {
        skipped = undefined;
        intro.goToStepNumber(stepNumber);
      }, 500);
    } else if (skipped === undefined) {
      !isMultipage ? triggerOnExit(router) : goToNextIntroPage(router, nextUrl);
    }
  });
  intro.start();
};

// 4. Homepage
export const startHomeIntro = (modal, t, isDesktop) => {
  intro = introJs();
  if (isDesktop) {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.next'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        {
          element: document.querySelector('#feed'),
          intro: `${t('onBoarding.homePage.dataStep-1')}</br></br>${t('onBoarding.homePage.dataStep-2a')}`,
          position: 'right',
        },
        {
          element: document.querySelector('#latest'),
          intro: t('onBoarding.homePage.dataStep-2b'),
          position: 'left',
        },
      ].filter((obj) => {
        return $(obj.element).length;
      }),
    });
  } else {
    intro.setOptions({
      prevLabel: t('onBoarding.btns.previous'),
      nextLabel: t('onBoarding.btns.next'),
      doneLabel: t('onBoarding.btns.next'),
      skipLabel: t('onBoarding.btns.skip'),
      exitOnEsc: true,
      keyboardNavigation: true,
      exitOnOverlayClick: false,
      disableInteraction: true,
      scrollTo: 'tooltip',
      tooltipClass: 'customTooltip',
      steps: [
        {
          element: document.querySelector('.feedtab'),
          intro: `${t('onBoarding.homePage.dataStep-1-mobile')}</br></br>${t('onBoarding.homePage.dataStep-2a')}`,
          position: 'right',
        },
        {
          element: document.querySelector('.newstab'),
          intro: t('onBoarding.homePage.dataStep-2b'),
          position: 'left',
        },
      ].filter((obj) => {
        return $(obj.element).length;
      }),
    });
  }
  intro.onbeforechange(() => {
    if (!isDesktop && intro._currentStep == '1') {
      document.querySelector('.newstab').click();
    } else if (!isDesktop && intro._currentStep == '0') {
      // so user can also come back
      document.querySelector('.feedtab').click();
    }
  });
  intro.onskip(() => {
    if (confirm(t('onBoarding.btns.confirm'))) {
      skipped = true;
    } else {
      skipped = false;
      stepNumber =
        intro._currentStep === intro._introItems.length - 1 ? intro._currentStep + 1 : intro._currentStep + 2;
    }
  });
  intro.onexit(async () => {
    if (skipped) {
      completeTour();
    } else if (skipped === false) {
      setTimeout(() => {
        skipped = undefined;
        intro.goToStepNumber(stepNumber);
      }, 500);
    } else if (skipped === undefined) {
      await completeTour();
      showOnboardingEndModal(modal, t);
    }
  });
  intro.start();
};
