import { Creator, UsersSm } from './common';
import { Project } from './project';

export interface Need {
  id: number;
  title: string;
  content: string;
  creator: Creator;
  documents: Document[];
  status: string;
  feed_id: number;
  users_sm: UsersSm[];
  skills: string[];
  ressources: string[];
  project: Pick<Project, 'id' | 'title' | 'banner_url'>;
  followers_count: number;
  members_count: number;
  claps_count: number;
  saves_count: number;
  posts_count: number;
  created_at?: Date;
  updated_at: Date;
  end_date?: Date;
  is_urgent: boolean;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  has_followed: boolean;
  has_saved: boolean;
}
