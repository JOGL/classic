export interface Event {
  service_id: number;
  id: string;
  category: string;
  name: string;
  description: string;
  start: string;
  end: string;
  location: string;
  latitude: string;
  longitude: string;
  url: string;
  logo: string;
  created_at: string;
  updated_at: string;
}
