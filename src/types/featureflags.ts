/**
 * Type listing a set of feature flags that can be used to hide or display
 * features for a given user.
 */
export interface FeatureFlags {
  isSpacesAllowed: boolean;
}
