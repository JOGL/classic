import { Creator, Geoloc, UsersSm } from './common';

export interface Community {
  id: number;
  title: string;
  short_title: string;
  banner_url: string;
  banner_url_sm: string;
  short_description: string;
  creator: Creator;
  status: string;
  skills: string[];
  ressources: string[];
  interests: number[];
  geoloc: Geoloc;
  country?: string;
  city?: string;
  feed_id: number;
  is_private: boolean;
  users_sm: UsersSm[];
  claps_count: number;
  posts_count: number;
  followers_count: number;
  saves_count: number;
  members_count: number;
  created_at: Date;
  updated_at: Date;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  is_pending: boolean;
  has_followed: boolean;
  has_saved: boolean;
  description: string;
}
