interface Badge {
  id: number;
  name: string;
  description: string;
  custom_fields?: any;
}

export interface User {
  id: number;
  first_name: string;
  last_name: string;
  nickname: string;
  affiliation: string;
  category: string;
  country: string;
  city: string;
  bio?: string;
  short_bio: string;
  can_contact: boolean;
  status: string;
  confirmed_at: Date;
  interests: number[];
  skills: string[];
  ressources: string[];
  badges: Badge[];
  feed_id: number;
  logo_url: string;
  logo_url_sm: string;
  mail_weekly: boolean;
  mail_newsletter: boolean;
  is_admin: boolean;
  has_clapped: boolean;
  has_followed: boolean;
  has_saved: boolean;
  geoloc: {
    lat: number;
    lng: number;
  };
  stats: {
    claps_count: number;
    saves_count: number;
    followers_count: number;
    following_count: number;
    mutual_count: number;
    projects_count: number;
    needs_count: number;
    // communities_count: number;
    challenges_count: number;
    programs_count: number;
    spaces_count: number;
  }
}
