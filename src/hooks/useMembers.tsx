import useGet from 'hooks/useGet';
import { ItemType, Members } from 'types';

export default function useMembers(itemType: ItemType, itemId: number, membersPerQuery?: number) {
  const apiRoute = membersPerQuery
    ? `/api/${itemType}/${itemId}/members?items=${membersPerQuery}` // if membersPerQuery is set, get only this number of members
    : `/api/${itemType}/${itemId}/members`; // else get all members
  const { data, error: membersError, response } = useGet<{ members: Members['members'] }>(apiRoute);
  return {
    members: data?.members.sort((a, b) => {
      // Sort by owner first
      if (a.owner && b.owner) return 0;
      if (a.owner && !b.owner) return -1;
      if (!a.owner && b.owner) return 1;

      // Then by admin
      if (a.admin && b.admin) return 0;
      if (a.admin && !b.admin) return -1;
      if (!a.admin && b.admin) return 1;
      // Leave members at the end
    }),
    membersError,
    response,
  };
}
