// This hook captures the user device width and returns corresponding breakpoint name

import { useEffect, useState } from 'react';

const useBreakpoint = () => {
  const [width, setWidth] = useState(undefined);

  const handleResize = () => setWidth(window.innerWidth);

  useEffect(() => {
    window.addEventListener('resize', handleResize);
  }, []);

  // JOGL breakpoints same as tailwind
  // tip: consider the lowest breakpoint in the market as 420px when testing responsivity
  if (width <= 640) return 'mobile';
  else if (width > 640 && width <= 768) return 'tablet';
  else if (width > 768 && width <= 1024) return 'laptop';
  else if (width > 1024 && width <= 1536) return 'desktop';
  else return 'desktop-wide';
};

export default useBreakpoint;
