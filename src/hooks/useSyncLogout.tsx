// import { useRouter } from 'next/router';
import { useEffect } from 'react';

// It allows all tabs to be in sync at logout
const syncLogout = (event, setCredentials) => {
  // const router = useRouter();
  if (event.key === 'logout') {
    // TODO: Add a query param with the redirect URL if possible
    // router.push('/signin');
    // Remove all credentials
    setCredentials({});
  } else if (event.key === 'login') {
    window.location.reload();
  }
};
export default function useSyncLogout(setCredentials) {
  useEffect(() => {
    // sync logout across tabs
    window.addEventListener('storage', (e) => syncLogout(e, setCredentials));
    return () => {
      window.removeEventListener('storage', (e) => syncLogout(e, setCredentials));
      window.localStorage.removeItem('logout');
      window.localStorage.removeItem('login');
    };
  }, [setCredentials]);
}
