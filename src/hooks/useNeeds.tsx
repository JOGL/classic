import useGet from 'hooks/useGet';
import { ItemType, Need } from 'types';

export default function useNeeds(itemType: ItemType, itemId: number) {
  const { data, error: needsError, mutate: needsMutate, revalidate: needsRevalidate } = useGet<{ needs: Need[] }>(
    `/api/${itemType}/${itemId}/needs`
  );
  return { dataNeeds: data, needsError, needsMutate, needsRevalidate };
}
