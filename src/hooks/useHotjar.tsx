import { useEffect } from 'react';
import { hotjar } from 'react-hotjar';

export default function useHotjar(HOTJAR_ID, klaroConsent) {
  useEffect(() => {
    if (HOTJAR_ID && klaroConsent) {
      hotjar.initialize(HOTJAR_ID);
    }
  }, [HOTJAR_ID, klaroConsent]);
}
