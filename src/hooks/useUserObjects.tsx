import useGet from 'hooks/useGet';
import { ItemType, Project, Community, Challenge, Program, Space } from 'types';

export default function useUserObjects(userId: number, itemType: ItemType) {
  const { data, error } = useGet<Project | Community | Challenge | Program | Space>(
    `/api/users/${userId}/objects/${itemType}`
  );
  return { data, error };
}
