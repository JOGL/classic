// Code comes mostly from the /examples folder in the SWR Github repo (https://github.com/vercel/swr/blob/master/examples/axios-typescript/libs/useRequest.ts)
import useSWR, { ConfigInterface, responseInterface } from 'swr';
import Axios, { AxiosResponse, AxiosError, AxiosRequestConfig } from 'axios';
import { useApi } from 'contexts/apiContext';
export type GetRequest = AxiosRequestConfig | string | undefined;
interface Return<Data, Error>
  extends Pick<
    responseInterface<AxiosResponse<Data>, AxiosError<Error>>,
    'isValidating' | 'revalidate' | 'error' | 'mutate'
  > {
  data: Data | undefined;
  loading: boolean;
  cancel: (message?: string) => void;
  response: AxiosResponse<Data> | undefined;
}
export interface Config<Data = unknown, Error = unknown>
  extends Omit<ConfigInterface<AxiosResponse<Data>, AxiosError<Error>>, 'initialData'> {
  initialData?: Data;
}
export default function useGet<Data = unknown, Error = unknown>(
  request: GetRequest,
  { initialData, ...config }: Config<Data, Error> = {}
): Return<Data, Error> {
  // Since request could be a string corresponding to the URL or a AxiosRequestConfig,
  // we need to know which type it is.
  let url: string | undefined;
  if (request) {
    url = typeof request === 'string' ? request : request.url;
    if (url === '/api/program') {
    }
  } else {
    url = undefined;
  }

  const requestConfig = typeof request === 'string' ? {} : request;
  const api = useApi();
  // Allows the GET to be cancelled before the Promise is fulfilled.
  // ! Cancel feature not tested yet.
  const source = Axios.CancelToken.source();
  const { data: response, error, isValidating, revalidate, mutate } = useSWR<AxiosResponse<Data>, AxiosError<Error>>(
    request && JSON.stringify(request),
    () => api.get(url, { ...requestConfig, cancelToken: source.token }),
    {
      ...config,
      initialData: initialData && {
        status: 200,
        statusText: 'InitialData',
        headers: {},
        config: requestConfig,
        data: initialData,
      },
    }
  );

  return {
    data: response && response.data,
    loading: !response,
    error,
    response,
    revalidate,
    isValidating,
    mutate,
    cancel: source.cancel,
  };
}
