import useGet from 'hooks/useGet';
import { Challenge, ItemType } from 'types';

export default function useChallenges(itemType: ItemType, itemId: number) {
  const { data, error: challengesError } = useGet<{ challenges: Challenge[] }>(`/api/${itemType}/${itemId}/challenges`);
  return { dataChallenges: data?.challenges, challengesError };
}
