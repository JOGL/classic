import { FC } from 'react';
import { Delete } from '@emotion-icons/material/Delete';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import Box from 'components/Box';
interface Props {
  postId: number;
  type: string;
  commentId?: number;
  feedId?: number | string;
  origin: 'self' | 'other';
  refresh: () => void;
}

const PostDelete: FC<Props> = ({ postId, commentId, type, origin, feedId, refresh }) => {
  const api = useApi();
  const { t } = useTranslation('common');

  const deletePost = () => {
    if (postId) {
      let apiUrl;
      if (origin === 'self') {
        apiUrl = type === 'post' ? `/api/posts/${postId}` : `/api/posts/${postId}/comment/${commentId}`;
      } else {
        apiUrl = type === 'post' ? `/api/feeds/${feedId}/${postId}` : `/api/posts/${postId}/comment/${commentId}`;
      }
      api.delete(apiUrl).then(() => {
        refresh();
      });
    }
  };
  return (
    <Box row alignItems="center" as="button" onClick={deletePost}>
      <Delete size={25} title="Delete post" />
      {t('feed.object.delete')}
    </Box>
  );
};
export default PostDelete;
