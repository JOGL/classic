import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import BtnUploadFile from 'components/Tools/BtnUploadFile';
import PostInputMentions from 'components/Tools/Forms/PostInputMentions';
import Button from 'components/primitives/Button';
import Box from 'components/Box';
import ReactTooltip from 'react-tooltip';
import SpinLoader from 'components/Tools/SpinLoader';

interface Props {
  handleChange: (content: any) => any;
  handleSubmit: () => void;
  handleChangeDoc: (document: any) => void;
  handleCancel: () => void;
}
interface State {}
class FormPost extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  static get defaultProps() {
    return {
      content: '',
      action: 'create',
      uploading: false,
      handleChange: () => console.warn('Missing function'),
      handleSubmit: () => console.warn('Missing function'),
      handleChangeDoc: () => console.warn('Missing function'),
      handleCancel: () => console.warn('Missing function'),
      documents: [],
    };
  }

  handleChange(content) {
    this.props.handleChange(content);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit();
  }

  handleChangeDoc(documents) {
    this.props.handleChangeDoc(documents);
  }

  handleCancel() {
    this.props.cancelEdit();
  }

  attachDocuments(documents) {
    const arrayDocuments = this.props.documents;
    for (let i = 0; i < documents.length; i++) {
      arrayDocuments.push(documents[i]);
    }
    this.props.handleChangeDoc(arrayDocuments);
  }

  deleteDocument(document) {
    const { documents } = this.props;
    documents.forEach((documentToInspect, index) => {
      if (documentToInspect === document) {
        documents.splice(index, 1);
      }
    });
    this.props.handleChangeDoc(documents);
  }

  renderPreviewDocuments(documents) {
    if (documents.length !== 0) {
      return (
        <div className="preview">
          <ul className="listDocuments">
            {documents.map((document, index) => (
              <li key={index}>
                {document.name}
                <button
                  type="button"
                  className="close"
                  aria-label="Close"
                  data-tip="Delete"
                  data-for="documentFeedEdit_delete"
                  onClick={() => this.deleteDocument(document)}
                  // show/hide tooltip on element focus/blur
                  onFocus={(e) => ReactTooltip.show(e.target)}
                  onBlur={(e) => ReactTooltip.hide(e.target)}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <ReactTooltip id="documentFeedEdit_delete" effect="solid" />
              </li>
            ))}
          </ul>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }

  render() {
    const { action, content, documents, uploading, userImg } = this.props;
    const { t } = this.props.i18n;
    const submitBtnText = action === 'create' ? 'Publish' : 'Update';
    const postTypeClass = action === 'create' ? 'postCreate' : 'postUpdate';
    const userLogo = userImg || '/images/default/default-user.png';
    return (
      <div className={postTypeClass}>
        <form onSubmit={this.handleSubmit}>
          <div className="inputBox">
            {action === 'create' && ( // if post action is create, display image on the left
              <div className="userImgContainer">
                <div className="userImg" style={{ backgroundImage: `url(${userLogo})` }} />
              </div>
            )}
            <PostInputMentions content={content} onChange={this.handleChange} placeholder={t('post.placeholder')} />
          </div>
          <div className="actionBar">
            <BtnUploadFile setListFiles={this.attachDocuments.bind(this)} />
            <Box row>
              {action === 'update' && (
                <Button btnType="secondary" type="button" onClick={this.handleCancel} tw="mr-2">
                  {t('feed.object.cancel')}
                </Button>
              )}
              <button type="submit" className="btn btn-primary" disabled={!!uploading || content === ''}>
                {uploading && <SpinLoader />}
                {t(`post.create.btn${submitBtnText}`)}
              </button>
            </Box>
          </div>
        </form>
        {documents && this.renderPreviewDocuments(documents)}
      </div>
    );
  }
}
export default withTranslation(FormPost, 'common');
