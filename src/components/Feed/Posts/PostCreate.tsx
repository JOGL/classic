import { useState, FC, Dispatch, SetStateAction } from 'react';
import FormPost from './FormPost';
import { findMentions, transformMentions } from 'components/Feed/Mentions';
import { useApi } from 'contexts/apiContext';
import { logEventToGA } from 'utils/analytics';

interface Props {
  feedId: number | string;
  content?: string;
  userImg: string;
  userId: number;
  refresh?: () => void;
}
const PostCreate: FC<Props> = ({ feedId, content: contentProp = '', userImg, userId, refresh: refreshProp }) => {
  const [content, setContent] = useState(contentProp);
  const [documents, setDocuments] = useState([]);
  const [uploading, setUploading] = useState(false);
  const api = useApi();
  const handleChange = (newContent) => {
    setContent(newContent);
  };

  const handleChangeDoc = (newDocuments) => {
    refreshProp();
    setDocuments(newDocuments);
  };
  const refresh = () => {
    refreshProp();
    setContent('');
    setUploading(false);
    setDocuments([]);
  };
  const handleSubmit = () => {
    const mentions = findMentions(content);
    const contentNoMentions = transformMentions(content);
    type PostJson = {
      post: {
        user_id: number;
        content: string;
        feed_id: number | string;
        mentions?: typeof mentions;
        documents?: typeof documents;
      };
    };
    const postJson: PostJson = {
      post: {
        user_id: userId,
        content: contentNoMentions,
        feed_id: feedId,
      },
    };
    if (mentions) {
      postJson.post.mentions = mentions;
    }
    if (documents) {
      postJson.post.documents = documents;
    }
    if (feedId !== undefined) {
      setUploading(true);
      api.post('/api/posts', postJson).then((res) => {
        // record event to Google Analytics
        logEventToGA('post', 'Post', `[${userId},${res.data.id}]`, { userId, postId: res.data.id });
        if (documents.length > 0) {
          const itemId = res.data.id;
          const itemType = 'posts';
          const type = 'documents';
          if (itemId) {
            const bodyFormData = new FormData();
            Array.from(documents).forEach((file) => {
              bodyFormData.append(`${type}[]`, file);
            });

            const config = { headers: { 'Content-Type': 'multipart/form-data' } };

            api
              .post(`/api/${itemType}/${itemId}/documents`, bodyFormData, config)
              .then((res2) => (res2.status === 200 ? refresh() : setUploading(false)))
              .catch((err) => {
                console.error(`Couldn't POST ${itemType} of itemId=${itemId}`, err);
                setUploading(false);
              });
          } else {
            refresh();
          }
        } else {
          refresh();
        }
      });
    } else {
      console.warn('feedId is undefined', feedId);
    }
  };

  return (
    <FormPost
      action="create"
      content={content}
      documents={documents}
      handleChange={handleChange}
      handleChangeDoc={handleChangeDoc}
      handleSubmit={handleSubmit}
      uploading={uploading}
      userImg={userImg}
    />
  );
};
export default PostCreate;
