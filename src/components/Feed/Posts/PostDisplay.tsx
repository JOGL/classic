// ! This component is too complicated, there should a fixed number of post types
import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import { Link as LinkIcon, Comments, Flag } from '@emotion-icons/fa-solid';
import Link from 'next/link';
// import Image from 'next/image';
import React, { FC, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { LayoutProps } from 'styled-system';
import Box from 'components/Box';
import Card from 'components/Cards/Card';
import CommentDisplay from 'components/Feed/Comments/CommentDisplay';
import PostUpdate from 'components/Feed/Posts/PostUpdate';
import Grid from 'components/Grid';
import UserCard from 'components/User/UserCard';
import BtnClap from 'components/Tools/BtnClap';
import DocumentsList from 'components/Tools/Documents/DocumentsList';
import Loading from 'components/Tools/Loading';
import ShareBtns from 'components/Tools/ShareBtns/ShareBtns';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import { Post, User } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import { useTheme } from 'utils/theme';
import { displayObjectDate, linkify, reportContent, copyLink, returnFirstLink } from 'utils/utils';
import CommentCreate from '../Comments/CommentCreate';
import PostDelete from './PostDelete';
import Image2 from 'components/Image2';
// import Button from 'components/primitives/Button';
// import A from 'components/primitives/A';
import dynamic from 'next/dynamic';

const ReactTinyLink = dynamic(() => import('react-tiny-link').then((mod) => mod.ReactTinyLink), { ssr: false });

interface Props {
  post: Post;
  user: User;
  feedId?: number | string;
  refresh?: () => void;
  isSingle?: boolean;
  isAdmin?: boolean;
  cardNoComments?: boolean;
  width?: LayoutProps['width'];
  isHomeFeed?: boolean;
}
const PostDisplay: FC<Props> = ({ post: postProp, user, feedId, isAdmin, cardNoComments = false, width, isSingle }) => {
  const modal = useModal();
  const [showComments, setShowComments] = useState(!cardNoComments);
  const [isEditing, setIsEditing] = useState(false);
  const [viewMore, setViewMore] = useState(false);
  const [loadingPosts, setLoadingPosts] = useState(false);
  const [postDeleted, setPostDeleted] = useState(false);
  const [goodImage, setGoodImage] = useState(true);
  const [showPreview, setShowPreview] = useState(true);
  const api = useApi();
  const theme = useTheme();
  const { t } = useTranslation('common');
  const { data: post, revalidate: revalidatePost } = useGet<Post>(`/api/posts/${postProp.id}`, {
    initialData: postProp,
  });
  if (post !== undefined) {
    const changeDisplayComments = () => {
      // toggle display of comments unless we don't want the comments to show at all (in post card)
      if (!cardNoComments) {
        setShowComments((prevState) => !prevState);
      } else {
        // else, open single post page in a new page
        const win = window.open(`/post/${post.id}`, '_blank');
        win.focus();
      }
    };

    const isEdit = () => {
      setIsEditing((prevState) => !prevState);
    };

    const showClapModal = () => {
      api.get(`/api/posts/${post.id}/clappers`).then((res) => {
        modal.showModal({
          children: <ClappersModal clappedUsers={res.data.clappers} />,
          title: t('post.clappers'),
          maxWidth: '30rem',
        });
      });
    };

    const callViewMore = () => {
      if (!cardNoComments) {
        setViewMore((prevState) => !prevState);
      } else {
        // else, open single post page in a new page
        var win = window.open(`/post/${post.id}`, '_blank');
        win.focus();
      }
    };

    const handlePreview = (data) => {
      const { image } = data;
      if (image.length === 0) setGoodImage(false);
    };
    const handleError = () => {
      setShowPreview(false);
    };

    const contentWithLinks = linkify(post?.content);
    const contentFirstLink = returnFirstLink(post?.content);

    // TODO use this instead of <ReactTinyLink/>, to have better styling
    // if (typeof window !== 'undefined') {
    // const { useScrapper } = require('react-tiny-link');
    // const [result, loading, error] = useScrapper({
    //   url: contentFirstLink,
    //   proxyUrl: 'https://cors.bridged.cc',
    //   onSuccess: (data) => handlePreview(data),
    //   onError: () => handleError(),
    // });
    // console.log(result, error);
    // }

    useEffect(() => {
      setLoadingPosts(false);
    }, []);

    const onPostDelete = () => {
      setPostDeleted(true);
      // refresh();
    };
    if (loadingPosts) {
      return <Loading height="18rem" active={loadingPosts} />;
    }
    if (postDeleted) {
      return (
        <Box
          p={4}
          border="1px solid black"
          borderColor={theme.colors.successes[200]}
          color={theme.colors.successes[200]}
          backgroundColor={theme.colors.successes[900]}
          borderRadius={2}
        >
          {t('feed.object.delete_conf')}
        </Box>
      );
    }

    // const user = post !== undefined || !user ? userProp : user;
    const postId = post.id;
    const creatorId = post.creator.id;
    const objImg = post.creator.logo_url ? post.creator.logo_url : '/images/default/default-user.png';
    const postFromName =
      post.from.object_type === 'need' // if post is a need
        ? t('post.need') + post.from.object_name
        : post.from.object_name;

    const commentsCount = post.comments.length;
    const clapsCount = post.claps_count;
    const userImgStyle = { backgroundImage: `url(${objImg})` };

    const maxChar = 280; // maximum character in a post to be displayed by default;
    const isLongText = !viewMore && post.content.length > maxChar;
    const postImages = post.documents.filter(
      (document) => document.content_type === 'image/jpeg' || document.content_type === 'image/png'
    );

    return (
      // @TODO best would be to apply props in Card only if isSingle is true
      // <Card maxWidth="700px" width={width} margin="auto">
      <Card width={width}>
        <div className={`post post-${postId} ${cardNoComments && 'postCard'}`}>
          <div className="topContent">
            <div className="topBar">
              <div className="left">
                <div className="userImgContainer">
                  <Link href={`/user/${creatorId}`}>
                    <a>
                      <div className="userImg" style={userImgStyle} />
                    </a>
                  </Link>
                </div>
                <div className="topInfo">
                  <Link href={`/user/${creatorId}`}>
                    <a>{`${post.creator.first_name} ${post.creator.last_name}`}</a>
                  </Link>

                  <div className="date">{displayObjectDate(post.created_at)}</div>
                  <div className="from">
                    {t('post.from')}
                    <Link href={`/${post.from.object_type}/${post.from.object_id}`}>
                      <a>{postFromName}</a>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="post-manage right d-flex flex-row">
                <div className="btn-group dropright">
                  <button
                    type="button"
                    className="btn btn-secondary dropdown-toggle"
                    data-display="static"
                    data-flip="false"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    •••
                  </button>
                  <div className="dropdown-menu dropdown-menu-right">
                    {user && user.id === creatorId && !isEditing && (
                      <Box row as="button" onClick={() => setIsEditing((prevState) => !prevState)}>
                        <Edit size={25} title="Edit post" />
                        {t('feed.object.update')}
                      </Box>
                    )}
                    {
                      user &&
                        user.id === creatorId && ( // if it's user's post
                          <PostDelete postId={postId} type="post" origin="self" refresh={onPostDelete} />
                        ) // set origin to self (it's your post)
                    }
                    {
                      user &&
                        user.id !== creatorId &&
                        isAdmin && ( // if user is admin and it's NOT his post
                          <PostDelete
                            postId={postId}
                            type="post"
                            origin="other"
                            feedId={feedId}
                            refresh={onPostDelete}
                          />
                        ) // set origin to other (not your post)
                    }
                    {user && user.id !== creatorId && (
                      <Box row as="button" onClick={() => reportContent('post', postId, undefined, api, t)}>
                        {' '}
                        {/* on click, launch report function (hide if user is creator */}
                        <Flag size={20} title="Report post" />
                        {t('feed.object.report')}
                      </Box>
                    )}
                    <Box
                      row
                      as="button"
                      textAlign="left"
                      lineHeight="25px"
                      onClick={() => copyLink(postId, 'post', undefined, t)}
                    >
                      {/* on click, launch copyLink function */}
                      <LinkIcon size={20} title="Copy post's link" />
                      {t('feed.object.copyLink')}
                    </Box>
                  </div>
                </div>
              </div>
            </div>
            {!isEditing ? ( // if post is not being edited, show post content
              <div className={`postTextContainer ${isLongText && 'hideText'}`}>
                {' '}
                {/* add hideText class to hide text if it's too long */}
                <div className="text extra" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
                {isLongText && ( // show "view more" link if text is too long
                  <button type="button" className="viewMore" onClick={callViewMore}>
                    ...
                    {t('general.showmore')}
                  </button>
                )}
              </div>
            ) : (
              // else show post edition component
              <PostUpdate
                postId={postId}
                content={post.content}
                closeOrCancelEdit={isEdit}
                refresh={revalidatePost}
                userImg={user.logo_url_sm}
                userId={user.id}
              />
            )}
            {postImages.length > 0 /* If post has image, display it */ && (
              <div className="postImage">
                <Image2 src={postImages[0].url} alt="preview" />
              </div>
            )}
            {/* If post has a link and no image, display link metaData*/}
            {postImages.length == 0 && contentFirstLink && showPreview && (
              <>
                {!cardNoComments && ( // don't show link preview if prop is cardNoComment
                  <ReactTinyLink
                    cardSize="large"
                    showGraphic={goodImage}
                    maxLine={2}
                    minLine={1}
                    url={contentFirstLink}
                    proxyUrl="https://cors.bridged.cc"
                    onSuccess={(data) => handlePreview(data)}
                    onError={handleError}
                    // defaultMedia=''
                  />
                )}
                {/* <div className="postLinkMeta">
                  {loading ? (
                    <Loading />
                  ) : (
                    <a href={result?.url} target="_blank">
                      {result?.image.length !== 0 && <img className="postLinkMeta--image" src={result?.image[0]} />}
                      <div className="postLinkMeta--infos">
                        <h4>{result?.title}</h4>
                        <p>{result?.provider}</p>
                      </div>
                    </a>
                  )}
                </div> */}
              </>
            )}
            <DocumentsList
              documents={post.documents}
              postId={post.id}
              cardType="feed"
              isEditing={isEditing}
              refresh={revalidatePost}
            />
          </div>
          <div className="actionBar">
            {!(commentsCount === 0 && clapsCount === 0) && ( // show post stats unless post has 0 clap and comment
              <div className="postStats">
                {/* on clap icon click, show modal */}
                <span>
                  {/* show claps count only if there are */}
                  {clapsCount > 0 && (
                    <Box as="button" onClick={showClapModal}>
                      {clapsCount + ' '}
                      <TextWithPlural type="clap" count={clapsCount} />
                    </Box>
                  )}
                </span>
                <span>
                  {/* show comments count only if there are */}
                  {commentsCount > 0 && (
                    <Box as="button" onClick={changeDisplayComments}>
                      {commentsCount + ' '}
                      <TextWithPlural type="comment" count={commentsCount} />
                    </Box>
                  )}
                </span>
              </div>
            )}
            <Box row marginTop="10px" paddingTop="8px" borderTop="1px solid #d3d3d3">
              <BtnClap itemType="posts" itemId={post.id} clapState={post.has_clapped} refresh={revalidatePost} />
              <button className="btn-postcard btn" onClick={changeDisplayComments} type="button">
                <Comments size={22} title="Show/hide comments" />
                {t('post.commentAction')}
              </button>
              <ShareBtns type="post" specialObjId={post.id} />
            </Box>
            {showComments && (
              <CommentDisplay
                comments={post.comments}
                postId={postId}
                refresh={revalidatePost}
                user={user}
                isAdmin={isAdmin}
                isSingle={isSingle}
              />
            )}
            {!cardNoComments && <CommentCreate postId={postId} refresh={revalidatePost} user={user} />}
            {/* {cardNoComments && (
              <Button>
                <A href={`/post/${post.id}`}>
                  View comments
                </A>
              </Button>
            )} */}
          </div>
        </div>
      </Card>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
const ClappersModal = ({ clappedUsers }) => (
  <>
    {/* Show modal content only if there are clappers */}
    {clappedUsers.length !== 0 ? (
      <Grid tw="gap-4">
        {clappedUsers?.map((user, i) => (
          <UserCard
            key={i}
            id={user.id}
            firstName={user.first_name}
            lastName={user.last_name}
            nickName={user.nickname}
            shortBio={user.short_bio}
            logoUrl={user.logo_url}
            // projectsCount={user.projects_count}
            hasFollowed={user.has_followed}
            isCompact
          />
        ))}
      </Grid>
    ) : (
      <Loading />
    )}
  </>
);

export default PostDisplay;
