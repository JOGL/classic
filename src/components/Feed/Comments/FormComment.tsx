import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import PostInputMentions from 'components/Tools/Forms/PostInputMentions';
import Button from 'components/primitives/Button';
import SpinLoader from 'components/Tools/SpinLoader';
import { User } from 'types';
// import "./PostCreate.scss";

const commentStyle = {
  control: {
    backgroundColor: '#F2F3F4',

    fontSize: 12,
    fontWeight: 'normal',
  },

  highlighter: {
    overflow: 'hidden',
  },

  input: {
    margin: 0,
  },

  '&singleLine': {
    control: {
      display: 'inline-block',

      width: 130,
    },

    highlighter: {
      padding: 1,
      border: '2px inset transparent',
    },

    input: {
      padding: 1,

      border: '2px inset',
    },
  },

  '&multiLine': {
    control: {
      border: '1px solid silver',
    },

    highlighter: {
      padding: 9,
    },

    input: {
      padding: 9,
      minHeight: 30,
      border: 0,
    },
  },

  suggestions: {
    list: {
      backgroundColor: 'white',
      border: '1px solid rgba(0,0,0,0.15)',
      fontSize: 10,
    },

    item: {
      padding: '5px 15px',
      borderBottom: '1px solid rgba(0,0,0,0.15)',

      '&focused': {
        backgroundColor: '#cee4e5',
      },
    },
  },
};

interface Props {
  action;
  content: string;
  handleChange: (content: string) => void;
  handleSubmit: () => void;
  cancelEdit: ((n: number) => void) | (() => void); // same doubt as in CommentUpdate
  index: number;
  uploading: boolean;
  user: User;
}

class FormComment extends Component<Props> {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  static get defaultProps() {
    return {
      content: '',
      action: 'create',
      user: '', // TODO utiliser context,
      postId: '',
      index: '',
      uploading: false,
      handleCancel: () => console.warn('Missing function'),
      handleChange: () => console.warn('Missing function'),
      handleSubmit: () => console.warn('Missing function'),
    };
  }

  handleChange(content) {
    this.props.handleChange(content);
    if (this.props.action === 'create') {
      // only when we create a post (not on update)
      const commentBtn = document.querySelector(`.commentCreate#post${this.props.postId} .btn-primary`); // select comment button from parent post
      // if we start typing, show it, and hide it if it's empty
      if (content) {
        commentBtn.style.display = 'block';
      } else {
        commentBtn.style.display = 'none';
      }
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit();
  }

  handleCancel(index) {
    this.props.cancelEdit(index);
  }

  render() {
    const { t } = this.props.i18n;
    const { action, content, user, postId, index, uploading } = this.props;
    const submitBtnText = action === 'create' ? 'Comment' : 'Update';
    const postTypeClass = action === 'create' ? 'postCreate commentCreate' : 'commentUpdate';
    const userImg = user?.logo_url_sm ? user.logo_url_sm : '/images/default/default-user.png';
    const userImgStyle = { backgroundImage: `url(${userImg})` };
    return (
      <div className={postTypeClass} id={`post${postId}`}>
        <form onSubmit={this.handleSubmit}>
          <div className="inputBox">
            {action === 'create' && ( // if post action is create, display image on the left
              <div className="userImgContainer">
                <div className="userImg" style={userImgStyle} />
              </div>
            )}
            <PostInputMentions
              content={content}
              onChange={this.handleChange}
              className="suggestionsContainer"
              style={commentStyle}
              placeholder={t('comment.placeholder')}
            />
          </div>
          {action === 'update' && (
            <Button
              onClick={() => this.handleCancel(index)}
              style={{ padding: '0.25rem 0.5rem', fontSize: '0.875rem' }}
              btnType="secondary"
              tw="mr-2"
            >
              {t('feed.object.cancel')}
            </Button>
          )}
          <button type="submit" className="btn btn-primary btn-sm" disabled={!!uploading}>
            {uploading && <SpinLoader />}
            {t(`post.comment.create.btn${submitBtnText}`)}
          </button>
        </form>
      </div>
    );
  }
}

export default withTranslation(FormComment, 'common');
