const Grid = (props) => (
  <div tw="grid grid-template-columns[repeat(auto-fill, 300px)] gap-4 justify-center justify-items-center" {...props} />
);

export default Grid;
