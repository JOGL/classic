// eslint-disable-next-line no-unused-vars
import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import AsyncSelect from 'react-select/async';
import CreatableSelect from 'react-select/creatable';
import algoliasearch from 'algoliasearch';
import Box from '../Box';
import styled from 'utils/styled';
import Button from '../primitives/Button';
import Alert from '../Tools/Alert';
import SpinLoader from '../Tools/SpinLoader';
import { logEventToGA } from 'utils/analytics';
import { ItemType } from 'types';

interface Props {
  itemType: ItemType;
  itemId: number;
  callBack: () => void;
  type?: 'attach' | 'affiliate';
}

const MembersAddModal: FC<Props> = ({
  itemType = '',
  itemId,
  callBack = () => {
    console.warn('Missing callback');
  },
  type = 'attach',
}) => {
  const [usersList, setUsersList] = useState([]);
  const [emailsList, setEmailsList] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [sending, setSending] = useState(false);
  const [error, setError] = useState(false);
  const api = useApi();
  const { closeModal } = useModal();
  const { t } = useTranslation('common');

  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const formatOptionLabel = ({ label, logo_url }) => (
    <UserLabel row>
      <img src={logo_url} />
      <div>{label}</div>
    </UserLabel>
  );

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('User'); // User
  let algoliaMembers = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['id', 'nickname', 'first_name', 'last_name', 'logo_url_sm'],
        hitsPerPage: 5,
      })
      .then((content) => {
        if (content) {
          algoliaMembers = content.hits;
        } else {
          algoliaMembers = [''];
        }
        algoliaMembers = algoliaMembers.map((user) => {
          return { value: user.id, label: `${user.first_name} ${user.last_name}`, logo_url: user.logo_url_sm };
        });
        resolve(algoliaMembers);
      });
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const handleChangeEmail = (content) => {
    const tempEmailsList = [];
    content?.map(function (invitee_mail) {
      invitee_mail && tempEmailsList.push(invitee_mail.value);
    });
    setUsersList([]);
    setEmailsList(tempEmailsList);
  };

  const handleChangeUser = (content) => {
    const tempUsersList = [];
    content?.map(function (user) {
      user && tempUsersList.push(user.value);
    });
    setEmailsList([]);
    setUsersList(tempUsersList);
  };

  const onSuccess = () => {
    setInviteSend(true);
    setSending(false);
    callBack(); // to refresh members list
    setTimeout(() => {
      closeModal(); // close modal after 1,5sec
      resetState();
    }, 1500);
  };

  const onError = () => {
    setError(true);
    setSending(false);
    setTimeout(() => {
      setError(false);
    }, 8000);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      itemType === 'projects' ||
      itemType === 'communities' ||
      itemType === 'challenges' ||
      itemType === 'programs' ||
      itemType === 'spaces'
    ) {
      if (itemId) {
        if (type === 'attach') {
          // set params depending if we are inviting JOGL members or people outside JOGL
          const params = emailsList.length !== 0 ? { stranger_emails: emailsList } : { user_ids: usersList };
          setSending(true);
          // api.post(`/api/users/${userData?.id}/affiliations/spaces/${spaceId}`)
          api
            .post(`/api/${itemType}/${itemId}/invite`, params)
            .then((res) => {
              const userId = res.config.headers.userId;
              logEventToGA('invite user', 'Invite', `[${userId},${itemId},${itemType}]`, { userId, itemId, itemType });
              onSuccess();
            })
            .catch(() => onError());
        } else {
          // type is affiliation
          for (let i = 0, len = usersList.length; i < len; i++) {
            api
              .post(`/api/users/${usersList[i]}/affiliations/spaces/${itemId}`)
              .then(() => {
                // when all projects have been affiliated, call this
                if (i === usersList.length - 1) onSuccess();
              })
              .catch(() => {
                if (i === usersList.length - 1) onError();
              });
          }
        }
      } else console.warn('itemId is missing');
    } else console.warn('itemType not compatible');
  };

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
  };

  // show different confirmation message depending on number of users added, and if it's via email or jogl user
  const confMsg =
    usersList.length === 1 // if 1 user has been invited
      ? t('member.invite.added')
      : usersList.length > 1 // if more than 1 user have been invited
      ? t('member.invite.added_plural') // if we invited people via their email
      : t('member.invite.btnSendEnded');

  return (
    <form>
      <div>
        <label htmlFor="joglUser">{t('member.invite.user.label')}</label>
        <div className="input-group">
          <AsyncSelect
            isMulti
            cacheOptions
            styles={customStyles}
            onChange={handleChangeUser}
            formatOptionLabel={formatOptionLabel}
            placeholder={t('member.invite.user.placeholder')}
            loadOptions={loadOptions}
            noOptionsMessage={() => null}
            components={{ DropdownIndicator: null, IndicatorSeparator: null }}
            isClearable
          />
        </div>
      </div>
      <div tw="py-4 text-center">{t('member.invite.or')}</div>
      <div className="form-group">
        <label htmlFor="byEmail">{t('member.invite.mail.label')}</label>
        <CreatableSelect
          isClearable
          isMulti
          noOptionsMessage={() => null}
          menuShouldScrollIntoView={true} // force scroll into view
          placeholder={t('member.invite.mail.placeholder.multiple')}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) => inputValue}
          onChange={handleChangeEmail}
          className="react-select-emails"
          // TODO check if email is valid before adding it to the list
        />
      </div>
      <div className="text-center btnZone">
        {!inviteSend ? (
          <Button
            type="button"
            onClick={handleSubmit}
            disabled={
              (emailsList.length === 0 && usersList.length === 0) ||
              (emailsList.length !== 0 && usersList.length !== 0) ||
              sending
            }
          >
            {sending && <SpinLoader />}
            {t('member.invite.btnSend')}
          </Button>
        ) : (
          <Alert type="success" message={confMsg} />
        )}
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          {t('err-')}
        </div>
      )}
    </form>
  );
};

const UserLabel = styled(Box)`
  align-items: center;
  div {
    font-size: 15px;
    color: #5c5d5d;
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
  }
`;

export default MembersAddModal;
