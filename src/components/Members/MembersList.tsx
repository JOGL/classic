import useTranslation from 'next-translate/useTranslation';
import React, { FC, useState, Dispatch, SetStateAction } from 'react';
import Loading from 'components/Tools/Loading';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import { ItemType } from 'types';
import Button from '../primitives/Button';
import MemberCard from './MemberCard';
import MembersAddModal from './MembersAddModal';
import DropdownRole from '../Tools/DropdownRole';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';

// import "./MembersList.scss";

interface Props {
  itemId: number;
  itemType: ItemType;
  isOwner: boolean;
  onlyPending?: boolean;
  setNbOfPendingMembers?: Dispatch<SetStateAction<any>>;
}
const MembersList: FC<Props> = ({
  itemId = 0,
  itemType,
  isOwner = false,
  onlyPending = false,
  setNbOfPendingMembers,
}) => {
  const [selectedMembers, setSelectedMembers] = useState([]);
  const [showChangedMessage, setShowChangedMessage] = useState({ changing: false, ids: [] });
  const [sending, setSending] = useState(false);
  const [newRole, setNewRole] = useState({});
  const [getDeleted, setGetDeleted] = useState(false);
  const api = useApi();
  const { showModal } = useModal();
  const { t } = useTranslation('common');
  // const membersPerQuery = 24;
  // add special sorting by desc role capability if it's members tab, and only show pending members if it's pending tab
  const specialSorting = !onlyPending ? 'sort=roles' : 'status=pending';

  const {
    data: dataMembers,
    response,
    revalidate: membersRevalidate,
    error,
    size,
    setSize,
  } = useInfiniteLoading(
    (index) => `/api/${itemType}/${itemId}/members?${specialSorting}`
    // (index) => `/api/${itemType}/${itemId}/members?items=${membersPerQuery}&page=${index + 1}${specialSorting}`
  );
  const members = dataMembers ? [].concat(...dataMembers?.map((d) => d.members)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataMembers && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.length === 0;
  const isReachingEnd = isEmpty || members?.length === totalNumber;
  // manually get pending members just to set number of pending members in the tab of parent component
  !onlyPending &&
    api.get(`/api/${itemType}/${itemId}/members?status=pending`).then((res) => {
      setNbOfPendingMembers && setNbOfPendingMembers(res.data.members.length);
    });

  const handleMembers = () => {
    if (!selectedMembers.length || selectedMembers.length !== nonOwnersMembersLength) {
      const selectedMembersIds = selectedMembers.map((m) => m.id);
      for (let i = 0, len = members.length; i < len; i++) {
        if (!selectedMembersIds.includes(members[i].id)) {
          if (getRole(members[i]) !== 'owner') setSelectedMembers((old) => [...old, members[i]]);
        }
      }
    } else if (selectedMembers.length === nonOwnersMembersLength) setSelectedMembers([]);
  };

  const getRole = (member) => {
    let actualRole = 'member';
    if (!member.owner && !member.admin && !member.member) {
      actualRole = 'pending';
    }
    if (member.member) {
      actualRole = 'member';
    }
    if (member.owner) {
      actualRole = 'owner';
    } else if (member.admin) {
      actualRole = 'admin';
    }
    return actualRole;
  };

  const onRoleChanged = (member) => {
    if (Array.isArray(member)) {
      setShowChangedMessage({ changing: true, ids: member.map((m) => m.id) });
    }
    setShowChangedMessage({ changing: true, ids: [member.id] });
    setTimeout(() => {
      setShowChangedMessage({ changing: false, ids: [] });
    }, 1500);
  };

  const removeOrRejectMember = () => {
    setSending(true);
    for (let i = 0, len = selectedMembers.length; i < len; i++) {
      api
        .delete(`/api/${itemType}/${itemId}/members/${selectedMembers[i].id}`)
        .then(() => setSending(false))
        .catch(() => setSending(false));
      // setTimeout is used to delay the update of listMembers state after processing loop requests
      if (i === selectedMembers.length - 1) {
        setTimeout(() => membersRevalidate(), 1000);
      }
    }
    setSelectedMembers([]);
    setGetDeleted(false);
  };

  const handleChange = () => {
    if (newRole.value) {
      if (itemId || itemType || selectedMembers.length) {
        for (let i = 0; i < selectedMembers.length; i++) {
          const jsonToSend = {
            user_id: selectedMembers[i].id,
            previous_role: getRole(selectedMembers[i]),
            new_role: newRole.value.toLowerCase(),
          };
          setSending(true);

          api
            .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
            .then(() => setSending(false))
            .catch((err) => {
              console.error(`Couldn't POST ${itemType} with itemId=${itemId} members`, err);
              setSending(false);
            });
          // setTimeout is used to delay the update of listMembers state after processing loop requests
          if (i === selectedMembers.length - 1) {
            setTimeout(() => membersRevalidate(), 1000);
          }
        }
      }
      setSelectedMembers([]);
      setNewRole({});
    }
  };

  const handleCancel = () => {
    setSelectedMembers([]);
    setNewRole({});
    setGetDeleted(false);
  };

  const nonOwnersMembersLength = members.filter((m) => getRole(m) !== 'owner').length;

  const acceptMember = (newRole) => {
    for (let i = 0, len = selectedMembers.length; i < len; i++) {
      const jsonToSend = {
        user_id: selectedMembers[i].id,
        previous_role: getRole(selectedMembers[i]),
        new_role: newRole,
      };
      setSending(true);
      api
        .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
        .then(() => {
          setSending(false);
          // setNbOfPendingMembers(0);
        })
        .catch((err) => {
          console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
          setSending(false);
        });
      // setTimeout is used to delay the update of listMembers state after processing loop requests
      if (i === selectedMembers.length - 1) {
        setTimeout(() => membersRevalidate(), 1000);
      }
    }
  };

  return (
    <div>
      <div className="justify-content-end" tw="flex flex-col">
        {!onlyPending && (
          <Button
            onClick={() => {
              showModal({
                children: <MembersAddModal itemType={itemType} itemId={itemId} callBack={membersRevalidate} />,
                title: t('member.btnNewMembers.title'),
                allowOverflow: true,
              });
            }}
          >
            {t('member.btnNewMembers.title')}
          </Button>
        )}
        {!!selectedMembers.length && !onlyPending ? (
          <div tw="flex flex-col justify-between mt-6 space-y-4 sm:(flex-row space-y-0)">
            <div tw="flex flex-auto justify-start space-x-4 sm:items-center">
              <div tw="w-48">
                <DropdownRole
                  onRoleChanged={() => onRoleChanged(selectedMembers)}
                  itemId={itemId}
                  itemType={itemType}
                  // show different list roles depending if user is owner (to prevent admins changing their roles to "owner")
                  listRole={['admin', 'member']}
                  member={selectedMembers}
                  isDisabled={true}
                  showPlaceholder
                  setNewRole={setNewRole}
                />
              </div>
              <div tw="flex flex-auto justify-start space-x-4">
                <Button
                  onClick={handleChange}
                  disabled={(newRole.value || getDeleted) && selectedMembers.length ? false : true}
                >
                  {sending && <SpinLoader />}
                  {t('entity.form.btnApply')}
                </Button>
                <Button
                  btnType="secondary"
                  onClick={handleCancel}
                  disabled={newRole.value || getDeleted ? false : true}
                >
                  {t('entity.form.btnCancel')}
                </Button>
              </div>
            </div>
            <Button onClick={removeOrRejectMember} btnType="danger">
              {t('general.remove')}
            </Button>
          </div>
        ) : (
          !!selectedMembers.length &&
          onlyPending && (
            <div tw="flex mt-6">
              <button
                type="button"
                className="btn btn-outline-success"
                disabled={sending}
                onClick={() => acceptMember('member')}
              >
                {sending && <SpinLoader />}
                {t('general.accept')}
              </button>
              <button
                type="button"
                className="btn btn-outline-danger"
                disabled={sending === 'remove'}
                tw="ml-4"
                onClick={removeOrRejectMember}
              >
                {sending === 'remove' && <SpinLoader />}
                {t('general.reject')}
              </button>
            </div>
          )
        )}
        {members?.length !== 0 && (
          <div tw="flex mt-8 items-center">
            <input
              id="members"
              type="checkbox"
              tw="hover:cursor-pointer"
              onChange={handleMembers}
              checked={selectedMembers.length && selectedMembers.length === nonOwnersMembersLength}
            />
            <label htmlFor="members" tw="ml-4 mb-0">
              {t('general.selectMembers', { members: t('general.member_other') })}
            </label>
          </div>
        )}
      </div>
      {!response ? (
        <Loading />
      ) : (
        members?.length !== 0 && (
          <div tw="pt-6">
            {members.map((member, i) => (
              <MemberCard
                itemId={itemId}
                itemType={itemType}
                member={member}
                key={i}
                callBack={membersRevalidate}
                isOwner={isOwner}
                selectedMembers={selectedMembers}
                setSelectedMembers={setSelectedMembers}
                role={getRole(member)}
                showChangedMessage={showChangedMessage}
                onRoleChanged={() => onRoleChanged(member)}
                isSending={sending}
              />
            ))}
          </div>
        )
      )}

      {/* show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page */}
      {/* {totalNumber > membersPerQuery && size <= response?.[0].headers?.['total-pages'] && (
        <div tw="self-center pt-4">
          <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
            {isLoadingMore && <SpinLoader />}
            {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
          </Button>
        </div>
      )} */}
    </div>
  );
};
export default MembersList;
