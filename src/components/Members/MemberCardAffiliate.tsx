import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import Box from '../Box';
import Button from '../primitives/Button';
import A from '../primitives/A';
import SpinLoader from '../Tools/SpinLoader';
export default function MemberCardAffiliate({
  member,
  itemId,
  itemType,
  callBack = () => {
    console.warn('Missing callback');
  },
  role,
}) {
  const api = useApi();
  const [accepting, setAccepting] = useState(false);
  const [rejecting, setRejecting] = useState(false);
  const { t } = useTranslation('common');
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const route = `/api/users/${member.id}/affiliations/spaces/${itemId}`;

  const acceptMember = () => {
    setAccepting(true);
    api
      .patch(route, {
        affiliations: {
          status: 'accepted',
        },
      })
      .then(() => {
        setAccepting(false);
        callBack();
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        setAccepting(false);
        callBack();
      });
  };
  const removeOrRejectMember = () => {
    setRejecting(true);
    api
      .delete(route)
      .then(() => {
        setRejecting(false);
        callBack();
      })
      .catch(() => {
        setRejecting(false);
        callBack();
      });
  };

  if (member) {
    let imgTodisplay = '/images/default/default-user.png';
    if (member.logo_url_sm) {
      imgTodisplay = member.logo_url_sm;
    }

    const bgLogo = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };

    return (
      <div>
        <Box flexDirection={['column', 'row']} justifyContent={['flex-start', 'space-between']} key={member.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <A href={`/user/${member.id}`}>
              {member.first_name} {member.last_name}
            </A>
          </Box>
          <Box row>
            {role === 'pending' ? (
              // if member role is pending, show buttons to accept or reject the request
              <>
                <button
                  type="button"
                  className="btn btn-outline-success"
                  disabled={accepting}
                  style={{ marginBottom: 0, marginLeft: '5px' }}
                  onClick={acceptMember}
                >
                  {accepting && <SpinLoader />}
                  {t('general.accept')}
                </button>
                <button
                  type="button"
                  className="btn btn-outline-danger"
                  disabled={rejecting}
                  style={{ marginBottom: 0, marginLeft: '5px' }}
                  onClick={removeOrRejectMember}
                >
                  {rejecting && <SpinLoader />}
                  {t('general.reject')}
                </button>
              </>
            ) : (
              // else show member role dropdown, and more action button
              <Button
                btnType="danger"
                disabled={isButtonDisabled}
                onClick={() => {
                  setIsButtonDisabled(true);
                  removeOrRejectMember();
                }}
              >
                {isButtonDisabled && <SpinLoader />}
                {t('general.remove_affiliation')}
              </Button>
            )}
          </Box>
        </Box>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
