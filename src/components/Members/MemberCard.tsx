import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import DropdownRole from '../Tools/DropdownRole';
import { useApi } from 'contexts/apiContext';
import Box from '../Box';
import Button from '../primitives/Button';
import A from '../primitives/A';
// import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';
import { useModal } from 'contexts/modalContext';
import styled from 'utils/styled';
import ReactTooltip from 'react-tooltip';
import SpinLoader from '../Tools/SpinLoader';
import { Members, ItemType } from 'types';

const MoreActions = styled(Box)`
  :hover {
    background: lightgrey;
  }
`;

interface Props {
  member: Members['users'];
  itemId: number;
  itemType: ItemType;
  isOwner: boolean;
  callBack: () => void;
  selectedMembers: Members[];
  setSelectedMembers: () => void;
  // role: string,
  role: 'pending' | 'member' | 'owner' | 'admin';
  getRole: (member: Members['users']) => void;
  onRoleChanged: (members: Members) => void;
  showChangedMessage: { changing: boolean; ids: number[] };
  isSending: boolean;
}

const MemberCard: FC<Props> = ({
  member,
  itemId,
  itemType,
  isOwner,
  callBack = () => {
    console.warn('Missing callback');
  },
  selectedMembers,
  setSelectedMembers,
  role,
  onRoleChanged,
  showChangedMessage,
  isSending,
}) => {
  const api = useApi();
  const [sending, setSending] = useState(isSending);
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const acceptMember = (newRole) => {
    const jsonToSend = {
      user_id: member.id,
      previous_role: role,
      new_role: newRole,
    };
    setSending(true);
    api
      .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
      .then(() => {
        setSending(false);
        callBack();
      })
      .catch((err) => {
        console.error(`Couldn't POST ${itemType} with itemId=${itemId}`, err);
        setSending(false);
        callBack();
      });
  };
  const removeOrRejectMember = () => {
    setSending('remove');

    api
      .delete(`/api/${itemType}/${itemId}/members/${member.id}`)
      .then(() => {
        setSending('');
        closeModal();
        callBack();
      })
      .catch(() => {
        setSending('');
        closeModal();
        callBack();
      });
  };

  const handleMembers = (event: { target: HTMLInputElement }) => {
    const id = parseInt(event.target.id);
    if (selectedMembers.map((s) => s.id).includes(id)) {
      const membersToKeep = selectedMembers.map((s) => s.id).filter((i) => i !== id);
      setSelectedMembers(selectedMembers.filter((m) => membersToKeep.includes(m.id)));
    } else setSelectedMembers((members) => [...members, member]);
  };

  if (member) {
    let imgTodisplay = '/images/default/default-user.png';
    if (member.logo_url_sm) {
      imgTodisplay = member.logo_url_sm;
    }

    const bgLogo = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };
    const MoreActionsModal = ({ setSending }) => {
      // when opening more actions modal (from the "...")
      const [showSubmitBtn, setShowSubmitBtn] = useState(false);
      const [customRole, setCustomRole] = useState('');
      const [isButtonDisabled, setIsButtonDisabled] = useState('');
      const handleChange: (key: number, content: string) => void = (key, content) => {
        setCustomRole(content);
        setShowSubmitBtn(content && content !== customRole); // show save button only if there is content and it's different from current customRole
      };
      const handleSubmit = () => {
        // api.patch(`/api/${itemType}/${itemId}/members`, {"custom_role": customRole}).then((res) => {
        //   setShowSubmitBtn(false); // hide update button
        // });
      };
      return (
        <>
          {/* <FormDefaultComponent
            content={customRole}
            id="custom_role"
            title={t('member.custom_role.add')}
            onChange={handleChange}
            placeholder={t('member.custom_role.placeholder')}
          />
          {showSubmitBtn && (
            <Button type="button" onClick={handleSubmit} width="100%">
              {t('entity.form.btnSave')}
            </Button>
          )} */}
          {(role !== 'owner' || isOwner) && (
            // restriction that prevents admins from removing owners, but allow owners to remove other owners
            <Box>
              {/* <Box style={{ borderTop: '1px solid lightgrey' }} pt={5} mt={5}> */}
              <Button
                btnType="danger"
                disabled={isButtonDisabled}
                onClick={() => {
                  setIsButtonDisabled(true);
                  removeOrRejectMember();
                }}
              >
                {isButtonDisabled && <SpinLoader />}
                {t('member.remove')}
              </Button>
            </Box>
          )}
        </>
      );
    };

    return (
      <div>
        <Box flexDirection={['column', 'row']} justifyContent={['flex-start', 'space-between']} key={member.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            {role !== 'owner' ? (
              <input
                id={member.id}
                type="checkbox"
                tw="hover:cursor-pointer"
                checked={selectedMembers?.map((s) => s.id).includes(member.id)}
                onChange={handleMembers}
              />
            ) : (
              <input type="checkbox" disabled />
            )}
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <A href={`/user/${member.id}`}>
              {member.first_name} {member.last_name}
            </A>
          </Box>
          <Box row>
            {role === 'pending' ? (
              // if member role is pending, show buttons to accept or reject the request
              <>
                <button
                  type="button"
                  className="btn btn-outline-success"
                  disabled={sending}
                  style={{ marginBottom: 0, marginLeft: '5px' }}
                  onClick={() => acceptMember('member')}
                >
                  {sending && <SpinLoader />}
                  {t('general.accept')}
                </button>
                <button
                  type="button"
                  className="btn btn-outline-danger"
                  disabled={sending === 'remove'}
                  style={{ marginBottom: 0, marginLeft: '5px' }}
                  onClick={removeOrRejectMember}
                >
                  {sending === 'remove' && <SpinLoader />}
                  {t('general.reject')}
                </button>
              </>
            ) : (
              // else show member role dropdown, and more action button
              <Box row width={['100%', '20rem']} alignItems="center">
                <Box width="100%">
                  <DropdownRole
                    onRoleChanged={onRoleChanged}
                    actualRole={role}
                    callBack={callBack}
                    itemId={itemId}
                    itemType={itemType}
                    // show different list roles depending if user is owner (to prevent admins changing their roles to "owner")
                    listRole={isOwner ? ['owner', 'admin', 'member'] : ['admin', 'member']}
                    member={member}
                    // don't show dropdown of roles if member you want to change is owner (except if you are the owner), to prevent admins changing owner's role.
                    isDisabled={role !== 'owner' || isOwner}
                  />
                </Box>
                <MoreActions
                  role="button"
                  onClick={() =>
                    showModal({
                      children: <MoreActionsModal setSending={setSending} />,
                      title: t('general.more_actions'),
                    })
                  }
                  onKeyUp={(e) =>
                    e.keyCode === 13 &&
                    showModal({
                      children: <MoreActionsModal setSending={setSending} />,
                      title: t('general.more_actions'),
                    })
                  }
                  // show/hide tooltip on element focus/blur
                  onFocus={(e) => ReactTooltip.show(e.target)}
                  onBlur={(e) => ReactTooltip.hide(e.target)}
                  tabIndex="0"
                  ml={2}
                  px={2}
                  borderRadius="5px"
                  fontSize="120%"
                  data-tip={t('general.more_actions')}
                  data-for="more_actions"
                >
                  •••
                </MoreActions>
                <ReactTooltip id="more_actions" delayHide={300} effect="solid" role="tooltip" />
              </Box>
            )}
          </Box>
        </Box>
        {showChangedMessage?.changing && showChangedMessage.ids.includes(member.id) && (
          <div
            className={`roleChangedMessage alert member${member.id} alert-success`}
            role="alert"
            style={{ marginTop: '7px' }}
          >
            {t('member.role.changed')}
          </div>
        )}
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default MemberCard;
