// eslint-disable-next-line no-unused-vars
import { useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import AsyncSelect from 'react-select/async';
import algoliasearch from 'algoliasearch';
import Box from '../Box';
import styled from 'utils/styled';
import Button from '../primitives/Button';
import Alert from '../Tools/Alert';
import SpinLoader from '../Tools/SpinLoader';

export default function ProjectsAddModal({
  itemId,
  closeModal,
  callBack = () => {
    console.warn('Missing callback');
  },
}) {
  const [projectsList, setProjectsList] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [sending, setSending] = useState(false);
  const [error, setError] = useState(false);
  const api = useApi();
  // const { closeModal } = useModal();
  const { t } = useTranslation('common');

  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const formatOptionLabel = ({ label, banner_url }) => (
    <ProjectLabel row>
      <img src={banner_url} />
      <div>{label}</div>
    </ProjectLabel>
  );

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('Project');
  let algoliaProjects = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['id', 'title', 'banner_url_sm'],
        hitsPerPage: 5,
      })
      .then((content) => {
        algoliaProjects = content ? content.hits : [''];
        algoliaProjects = algoliaProjects.map((project) => {
          return { value: project.id, label: project.title, banner_url: project.banner_url_sm };
        });
        resolve(algoliaProjects);
      });
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const handleChange = (content) => {
    const tempProjectsList = [];
    content?.map(function (project) {
      project && tempProjectsList.push(project.value);
    });
    setProjectsList(tempProjectsList);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setSending(true);
    for (let i = 0, len = projectsList.length; i < len; i++) {
      api
        .post(`/api/projects/${projectsList[i]}/affiliations/spaces/${itemId}`)
        .then(() => {
          if (i === projectsList.length - 1) {
            // when all projects have been affiliated, do those actions
            setInviteSend(true);
            setSending(false);
            callBack(); // to refresh projects list
            setTimeout(() => {
              closeModal(); // close modal after 3sec
              resetState();
            }, 3000);
          }
        })
        .catch(() => {
          if (i === projectsList.length - 1) {
            setError(true);
            setSending(false);
            setTimeout(() => {
              setError(false);
            }, 8000);
          }
        });
    }
  };

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
  };
  return (
    <form>
      <div className="input-group">
        <AsyncSelect
          isMulti
          cacheOptions
          styles={customStyles}
          onChange={handleChange}
          formatOptionLabel={formatOptionLabel}
          placeholder={t('attach.project.asAdmin_placeholder')}
          loadOptions={loadOptions}
          noOptionsMessage={() => null}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          isClearable
        />
      </div>
      <div className="text-center btnZone">
        {!inviteSend ? (
          <Button type="button" onClick={handleSubmit} disabled={projectsList.length === 0 || sending}>
            {sending && <SpinLoader />}
            {t('general.add')}
          </Button>
        ) : (
          <Alert type="success" message={t('program.board.invite.added')} />
        )}
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          {t('err-')}
        </div>
      )}
    </form>
  );
}

const ProjectLabel = styled(Box)`
  align-items: center;
  div {
    font-size: 15px;
    color: #5c5d5d;
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
  }
`;
