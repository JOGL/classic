import Grid from '../Grid';
import ProjectCard from './ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

export default function ProjectList({ listProjects, cardFormat = undefined, gridCols = [1, 2, undefined, 3] }) {
  return (
    <Grid tw="pt-3">
      {!listProjects ? (
        <Loading />
      ) : listProjects.length === 0 ? (
        <NoResults type="project" />
      ) : (
        listProjects.map((project, i) => (
          <ProjectCard
            key={i}
            id={project.id}
            title={project.title}
            shortTitle={project.short_title}
            short_description={project.short_description}
            members_count={project.members_count}
            needs_count={project.needs_count}
            followersCount={project.followers_count}
            savesCount={project.saves_count}
            postsCount={project.posts_count}
            has_saved={project.has_saved}
            reviewsCount={project.reviews_count}
            banner_url={project.banner_url || '/images/default/default-project.jpg'}
            // show skills only if cardFormat is not compact
            {...(cardFormat !== 'compact' && { skills: project.skills })}
            cardFormat={cardFormat}
          />
        ))
      )}
    </Grid>
  );
}
