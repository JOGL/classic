import { Edit } from '@emotion-icons/fa-solid/Edit';
import Link from 'next/link';
import React, { FC, Fragment, useMemo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H1 from 'components/primitives/H1';
// import BtnClap from 'components/Tools/BtnClap';
import InfoInterestsComponent from 'components/Tools/Info/InfoInterestsComponent';
import { useModal } from 'contexts/modalContext';
import useMembers from 'hooks/useMembers';
import useUserData from 'hooks/useUserData';
import { Project } from 'types';
import { displayObjectDate, linkify, renderOwnerNames } from 'utils/utils';
import Box from '../Box';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import BtnFollow from '../Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import QuickSearchBar from '../Tools/QuickSearchBar';
import ReactGA from 'react-ga';
import Chips from '../Chip/Chips';
// import Image from 'next/image';
import Image2 from '../Image2';
import { theme } from 'twin.macro';
import { useRouter } from 'next/router';
import useGet from 'hooks/useGet';
import InfoDefaultComponent from '../Tools/Info/InfoDefaultComponent';
import BtnReview from '../Tools/BtnReview';
import ReactTooltip from 'react-tooltip';
import { PatchCheckFill } from '@emotion-icons/bootstrap';
import BtnStar from '../Tools/BtnStar';
import { TextWithPlural } from 'utils/managePlurals';

interface Props {
  project: Project;
  forceClickNeedTab: () => void;
}
const ProjectHeader: FC<Props> = ({ project, forceClickNeedTab }) => {
  const { members, membersError } = useMembers('projects', project.id, undefined);
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/projects/${project.id}/links`);
  const { showModal } = useModal();
  const { t } = useTranslation('common');
  const { userData } = useUserData();
  const router = useRouter();
  const { locale } = router;

  const participatingToChallenges = (challenges) => {
    return (
      <p className="card-by">
        {t('project.info.participatingToChallenges')}
        {challenges.map((challenge, index) => {
          const rowLen = challenges.length;
          return (
            <Fragment key={index}>
              <Link href={`/challenge/${challenge.short_title}`}>
                <a>{(locale === 'fr' && challenge.title_fr) || challenge.title}</a>
              </Link>
              {/* add comma, except for last item */}
              {rowLen !== index + 1 && <span>, </span>}
            </Fragment>
          );
        })}
      </p>
    );
  };

  const affiliatedToSpaces = (spaces) => {
    return (
      <p className="card-by">
        {t('project.info.affiliatedToSpaces')}
        {spaces.map((space, index) => {
          return (
            <Fragment key={index}>
              <Link href={`/space/${space[0].short_title}`}>
                <a>{space[0].title}</a>
              </Link>
              {/* add comma, except for last item */}
              {spaces.length !== index + 1 && <span>, </span>}
            </Fragment>
          );
        })}
      </p>
    );
  };

  const Stats = ({ value, title }) => (
    <div tw="justify-center items-center">
      <div>{value}</div>
      <div>{title}</div>
    </div>
  );

  const showMembersModal = (e) => {
    if (members && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      // capture the opening of the modal as a special modal page view to google analytics
      ReactGA.modalview(`/viewMembers/project/${project.id}`);
      showModal({
        children: (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid tw="py-0 sm:py-2 md:py-4">
              {members.map((member, i) => (
                <UserCard
                  key={i}
                  id={member.id}
                  firstName={member.first_name}
                  lastName={member.last_name}
                  nickName={member.nickname}
                  shortBio={member.short_bio}
                  logoUrl={member.logo_url}
                  skills={member.skills}
                  hasFollowed={member.has_followed}
                  affiliation={member.affiliation}
                  projectsCount={member.stats?.projects_count}
                  followersCount={member.stats?.followers_count}
                  spacesCount={member.stats?.spaces_count}
                  mutualCount={member.stats.mutual_count}
                  role={member.owner ? 'leader' : !member.owner && member.admin && 'admin'}
                />
              ))}
            </Grid>
          </>
        ),
        title: t('entity.tab.members'),
        maxWidth: '61rem',
      });
    }
  };

  let { members_count, followers_count } = project;
  const {
    banner_url,
    has_followed,
    id,
    is_member,
    is_owner,
    short_description,
    title,
    interests,
    skills,
    saves_count,
    needs_count,
    has_saved,
    users_sm,
    challenges,
    creator,
    created_at,
    updated_at,
    is_pending,
    is_private,
    reviews_count,
    affiliated_spaces,
  } = project;

  if (followers_count === undefined) followers_count = 0;
  if (members_count === undefined) members_count = 0;
  const activeChallenges = challenges.filter((challenge) => challenge.project_status !== 'pending');
  // const activeSpaces = spaces[0].filter((space) => space.status !== 'pending');
  const activeSpaces = affiliated_spaces;
  return (
    <div className="row projectHeader">
      <Box alignItems="center" width={'full'} mb={5} px={4}>
        <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
          <H1 fontSize={['2.55rem', undefined, '3rem']}>{title}</H1>
          <Box row>
            {userData &&
              project.is_reviewer && ( // show review button only to JOGL global reviewer
                <div tw="pl-2">
                  <BtnReview itemType="projects" itemId={id} reviewState={reviews_count > 0} />
                </div>
              )}
            {project.reviews_count > 0 && !project.is_reviewer && (
              // show reviewed badge if project has been reviewed
              <div tw="inline-flex items-center pl-2">
                <PatchCheckFill
                  size="24"
                  title="About reviewed project"
                  data-tip={t('project.info.reviewedTooltip')}
                  data-for="reviewed_project"
                  color={theme`colors.primary`}
                />
                <ReactTooltip id="reviewed_project" effect="solid" type="dark" className="forceTooltipBg" />
              </div>
            )}
          </Box>
          <div tw="pl-2">
            <ShareBtns type="project" />
          </div>
        </Box>
        {/* edit button */}
        {project.is_admin && (
          <Link href={`/project/${project.id}/edit`}>
            <a style={{ display: 'flex', justifyContent: 'center' }}>
              <Edit size={18} />
              {t('entity.form.btnAdmin')}
            </a>
          </Link>
        )}
      </Box>
      <div className="col-lg-7 col-md-12 projectHeader--banner">
        <Image2 src={banner_url || '/images/default/default-project.jpg'} alt={title} priority />
      </div>
      <div className="col-lg-5 col-md-12 projectHeader--info">
        <div tw="font-medium">
          <InfoDefaultComponent content={linkify(short_description)} containsHtml />
        </div>
        {users_sm !== undefined && users_sm.length > 0 && renderOwnerNames(users_sm, creator, t)}
        {activeChallenges?.length !== 0 && participatingToChallenges(activeChallenges)}
        {activeSpaces?.length !== 0 && affiliatedToSpaces(activeSpaces)}
        <div tw="text-gray-500 inline-flex">
          {created_at && `${t('general.created_at')} ${displayObjectDate(created_at, 'LL', true)}`}
          {/* show updated date only if it exists and if it's not the same day as created date (OR if only updated_at exists and not created_at) */}
          {((updated_at && created_at && created_at.substr(0, 10) !== updated_at.substr(0, 10)) ||
            (updated_at && !created_at)) &&
            `${created_at ? ' / ' : ''} ${t('general.updated_at')} ${displayObjectDate(updated_at, 'LL', true)}`}
        </div>
        <Box pt={4} /> {/* empty div with paddingTop */}
        <InfoInterestsComponent content={interests} title={t('general.sdgs')} />
        <Box mb={3}>
          <Box color={theme`colors.secondary`}>{t('user.profile.skills')}</Box>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              href: `/search/projects/?refinementList[skills][0]=${skill}`,
            }))}
            overflowText="seeMore"
            type="skills"
            showCount={6}
          />
        </Box>
        <div tw="flex flex-wrap gap-x-4 gap-y-2 pb-6 items-center sm:pb-4">
          {!is_owner && (
            <BtnJoin
              joinState={is_pending ? 'pending' : is_member}
              isPrivate={is_private}
              itemType="projects"
              itemId={id}
              count={members_count}
              showMembersModal={showMembersModal}
            />
          )}
          <BtnFollow followState={has_followed} itemType="projects" itemId={id} count={followers_count} />
          <BtnStar itemType="projects" itemId={id} hasStarred={has_saved} count={saves_count} />
        </div>
        <div tw="text-center items-center justify-start space-x-6 w-full inline-flex pb-2">
          <div onClick={showMembersModal} tw="hover:(text-primary cursor-pointer)">
            <Stats value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
          </div>
          <div tw="border-right[2px solid] border-gray-300 h-8" />
          <div onClick={forceClickNeedTab} tw="hover:(text-primary cursor-pointer)">
            <Stats value={needs_count} title={<TextWithPlural type="need" count={needs_count} />} />
          </div>
        </div>
        {/* Project external links */}
        {dataExternalLink && dataExternalLink?.length !== 0 && (
          <div tw="flex flex-wrap gap-x-4 mb-5 mt-3 sm:gap-x-3">
            {[...dataExternalLink].map((link, i) => (
              <a tw="items-center self-center" key={i} href={link.url} target="_blank">
                <img tw="w-10 hover:opacity-80" src={link.icon_url} />
              </a>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};
export default React.memo(ProjectHeader);
