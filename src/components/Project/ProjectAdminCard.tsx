import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { Project } from 'types';
import Box from '../Box';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  project: Project;
  parentId?: number;
  parentType: 'challenges' | 'spaces';
  callBack: () => void;
}

const ProjectAdminCard: FC<Props> = ({ project, parentType, parentId, callBack }) => {
  const [sending, setSending] = useState('');
  const [error, setError] = useState('');
  const api = useApi();
  const { t } = useTranslation('common');

  // have different api route depending on parentType
  const route =
    parentType === 'challenges'
      ? `/api/challenges/${parentId}/projects/${project.id}`
      : `/api/projects/${project.id}/affiliations/spaces/${parentId}`;

  const onSuccess = () => {
    setSending('');
    callBack();
  };

  const onError = (type, err) => {
    console.error(`Couldn't ${type} ${parentType} with parentId=${parentId}`, err);
    setSending('');
    setError(t('err-'));
  };

  const acceptProject = () => {
    setSending('accepted');
    parentType === 'challenges'
      ? api
          .post(route, { status: 'accepted' })
          .then(() => onSuccess())
          .catch((err) => onError('post', err))
      : api
          .patch(route, {
            affiliations: {
              status: 'accepted',
            },
          })
          .then(() => onSuccess())
          .catch((err) => onError('post', err));
  };

  const removeProject = () => {
    setSending('remove');
    api
      .delete(route)
      .then(() => onSuccess())
      .catch((err) => onError('delete', err));
  };

  const isProjectPending =
    parentType === 'challenges'
      ? project.challenges.find((obj) => obj.challenge_id === parentId)?.project_status === 'pending'
      : project.affiliated_spaces?.find((obj) => obj[0].id === parentId)?.[0].affiliation_status === 'pending';

  if (project !== undefined) {
    let imgTodisplay = '/images/default/default-project.jpg';
    if (project.banner_url_sm) {
      imgTodisplay = project.banner_url_sm;
    }

    const bgBanner = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };

    return (
      <div>
        <Box row justifyContent="space-between" key={project.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgBanner} />
            </div>
            <Box>
              <A href={`/project/${project.id}/${project.short_title}`}>{project.title}</A>
              <Box fontSize="93%" pt={1}>
                {t('attach.members')}
                {project.members_count}
              </Box>
            </Box>
          </Box>
          <Box row alignItems="center" ml={3}>
            {isProjectPending ? ( // if project status is pending, display the accept/reject buttons
              <Box flexDirection={['column', 'row']}>
                <button
                  type="button"
                  className="btn btn-outline-success"
                  style={{ marginBottom: '5px', marginLeft: '8px' }}
                  disabled={sending === 'accepted'}
                  onClick={acceptProject}
                >
                  {sending === 'accepted' && <SpinLoader />}
                  {t('general.accept')}
                </button>
                <button
                  type="button"
                  className="btn btn-outline-danger"
                  style={{ marginBottom: '5px', marginLeft: '8px' }}
                  disabled={sending === 'remove'}
                  onClick={removeProject}
                >
                  {sending === 'remove' && <SpinLoader />}
                  {t('general.reject')}
                </button>
              </Box>
            ) : (
              // else display the remove button
              <Button btnType="danger" disabled={sending === 'remove'} onClick={removeProject}>
                {sending === 'remove' && <SpinLoader />}
                {t('attach.remove')}
              </Button>
            )}
            {error !== '' && <Alert type="danger" message={error} />}
          </Box>
        </Box>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ProjectAdminCard;
