import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormTextAreaComponent from 'components/Tools/Forms/FormTextAreaComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from 'components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormToggleComponent from 'components/Tools/Forms/FormToggleComponent';
import FormWysiwygComponent from 'components/Tools/Forms/FormWysiwygComponent';
import FormDropdownComponent from '../Tools/Forms/FormDropdownComponent';
import { toAlphaNum } from 'components/Tools/Nickname';
import Box from '../Box';
import FormValidator from 'components/Tools/Forms/FormValidator';
import projectFormRules from './projectFormRules.json';
import { Project } from 'types';
import { changesSavedConfAlert } from 'utils/utils';
import { useRouter } from 'next/router';
import SpinLoader from '../Tools/SpinLoader';
import AllowPostingToAllToggle from 'components/Tools/AllowPostingToAllToggle';
import Alert from '../Tools/Alert';

interface Props {
  mode: 'edit' | 'create';
  project: Project;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const ProjectForm: FC<Props> = ({ mode, project, sending = false, hasUpdated = false, handleChange, handleSubmit }) => {
  const validator = new FormValidator(projectFormRules);
  const [stateValidation, setStateValidation] = useState({});
  const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } =
    stateValidation || '';
  const { t } = useTranslation('common');
  const [hasChanged, setHasChanged] = useState(false);
  const urlBack = mode === 'edit' ? `/project/${project.id}/${project.short_title}` : '/search/projects';
  const router = useRouter();

  const generateSlug = (projectTitle) => {
    let proposalShortName = projectTitle.trim();
    proposalShortName = toAlphaNum(proposalShortName);
    return proposalShortName;
  };

  // show conf message when project has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to project page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));

  const handleChangeProject = (key, content) => {
    let proposalShortName;
    if (key === 'title') {
      // generate a shortname when typing a title, and update short_name field with the value
      proposalShortName = generateSlug(content);
      handleChange('short_title', proposalShortName);
    }
    /* Validators start */
    const state = {};
    state[key] = content;
    // Check proposalShortName
    if (key === 'title') {
      state.short_title = proposalShortName;
    }
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      // Update short_title too only if title has been changed
      if (key === 'title') {
        newStateValidation.valid_short_title = validation.short_title;
      }
      setStateValidation(newStateValidation);
    }
    /* Validators end */
    handleChange(key, content);
    // set has changed to true, do undisable submit buttons (so user can update project only when they changed a field)
    setHasChanged(true);
  };

  const handleSubmitProject = () => {
    /* Validators control before submit */
    let firsterror = true;
    const validation = validator.validate(project);
    if (validation.isValid) {
      handleSubmit();
    } else {
      const newStateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firsterror) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 140; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firsterror = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };

  const renderBtnsForm = () => {
    const textAction = mode === 'edit' ? 'Update' : 'Next';

    return (
      <>
        <Box row justifyContent="center" mt={5} mb={3}>
          <Link href={urlBack}>
            <a>
              <button type="button" className="btn btn-outline-primary" disabled={sending}>
                {t('entity.form.back')}
              </button>
            </a>
          </Link>
          <button
            type="button"
            onClick={handleSubmitProject}
            className="btn btn-primary"
            disabled={sending || !hasChanged}
            style={{ marginLeft: '10px' }}
          >
            {sending && <SpinLoader />}
            {t(`entity.form.btn${textAction}`)}
          </button>
        </Box>
      </>
    );
  };

  if (
    project.desc_elevator_pitch ||
    project.desc_problem_statement ||
    project.desc_objectives ||
    project.desc_state_art ||
    project.desc_progress ||
    project.desc_stakeholder ||
    project.desc_impact_strat ||
    project.desc_ethical_statement ||
    project.desc_sustainability_scalability ||
    project.desc_communication_strat ||
    project.desc_funding ||
    project.desc_contributing
  ) {
    var old_description = true;
  } else {
    var old_description = false;
  }

  return (
    <form className="projectForm">
      {/* Show warning msg if project's status is draft */}
      {project.status === 'draft' && mode === 'edit' && (
        <Alert type="warning" message={t('entity.info.status.dropDownDraftWarningMsg')} />
      )}
      <FormDefaultComponent
        content={project.title}
        errorCodeMessage={valid_title ? valid_title.message : ''}
        id="title"
        isValid={valid_title ? !valid_title.isInvalid : undefined}
        mandatory
        onChange={handleChangeProject}
        placeholder={t('project.form.title_placeholder')}
        title={t('entity.info.title')}
      />
      <FormDefaultComponent
        content={project.short_title}
        errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
        id="short_title"
        isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
        onChange={handleChangeProject}
        mandatory
        pattern={/[A-Za-z0-9]/g}
        placeholder={t('project.form.short_title_placeholder')}
        prepend="#"
        title={t('entity.info.short_name')}
      />
      <FormTextAreaComponent
        content={project.short_description}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        id="short_description"
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory
        maxChar={340}
        onChange={handleChangeProject}
        placeholder={t('project.form.short_description_placeholder')}
        rows={5}
        supportLinks
        title={t('entity.info.short_description')}
      />
      {mode === 'edit' && (
        <>
          <FormWysiwygComponent
            content={project.description}
            id="description"
            onChange={handleChangeProject}
            show
            placeholder={t('project.form.description_placeholder')}
            title={t('entity.info.description')}
          />
          {old_description && (
            <a
              className="btn btn-primary collapseDetailedInfo"
              data-toggle="collapse"
              href="#DetailedAbout"
              role="button"
              aria-expanded="false"
            >
              {t('project.fillDetailedInfo')}
            </a>
          )}
          <div className="collapse" id="DetailedAbout">
            <FormWysiwygComponent
              content={project.desc_elevator_pitch}
              id="desc_elevator_pitch"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_elevator_pitch_placeholder')}
              title={t('entity.info.desc_elevator_pitch')}
            />
            <FormWysiwygComponent
              content={project.desc_contributing}
              id="desc_contributing"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_contributing_placeholder')}
              title={t('entity.info.desc_contributing')}
            />
            <FormWysiwygComponent
              content={project.desc_problem_statement}
              id="desc_problem_statement"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_problem_statement_placeholder')}
              title={t('entity.info.desc_problem_statement')}
            />
            <FormWysiwygComponent
              content={project.desc_objectives}
              id="desc_objectives"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_objectives_placeholder')}
              title={t('entity.info.desc_objectives')}
            />
            <FormWysiwygComponent
              content={project.desc_state_art}
              id="desc_state_art"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_state_art_placeholder')}
              title={t('entity.info.desc_state_art')}
            />
            <FormWysiwygComponent
              content={project.desc_progress}
              id="desc_progress"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_progress_placeholder')}
              title={t('entity.info.desc_progress')}
            />
            <FormWysiwygComponent
              content={project.desc_stakeholder}
              id="desc_stakeholder"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_stakeholder_placeholder')}
              title={t('entity.info.desc_stakeholder')}
            />
            <FormWysiwygComponent
              content={project.desc_impact_strat}
              id="desc_impact_strat"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_impact_strat_placeholder')}
              title={t('entity.info.desc_impact_strat')}
            />
            <FormWysiwygComponent
              content={project.desc_ethical_statement}
              id="desc_ethical_statement"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_ethical_statement_placeholder')}
              title={t('entity.info.desc_ethical_statement')}
            />
            <FormWysiwygComponent
              content={project.desc_sustainability_scalability}
              id="desc_sustainability_scalability"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_sustainability_scalability_placeholder')}
              title={t('entity.info.desc_sustainability_scalability')}
            />
            <FormWysiwygComponent
              content={project.desc_communication_strat}
              id="desc_communication_strat"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_communication_strat_placeholder')}
              title={t('entity.info.desc_communication_strat')}
            />
            <FormWysiwygComponent
              content={project.desc_funding}
              id="desc_funding"
              onChange={handleChangeProject}
              show
              placeholder={t('project.form.desc_funding_placeholder')}
              title={t('entity.info.desc_funding')}
            />
          </div>
        </>
      )}
      {mode === 'edit' && (
        <FormImgComponent
          type="banner"
          id="banner_url"
          imageUrl={project.banner_url}
          itemId={project.id}
          itemType="projects"
          title={t('project.info.banner_url')}
          content={project.banner_url}
          defaultImg="/images/default/default-project.jpg"
          onChange={handleChangeProject}
        />
      )}
      <FormInterestsComponent
        content={project.interests}
        errorCodeMessage={valid_interests ? valid_interests.message : ''}
        mandatory
        onChange={handleChangeProject}
      />
      <FormSkillsComponent
        content={project.skills}
        errorCodeMessage={valid_skills ? valid_skills.message : ''}
        id="skills"
        type="project"
        isValid={valid_skills ? !valid_skills.isInvalid : undefined}
        mandatory
        onChange={handleChangeProject}
        placeholder={t('general.skills.placeholder')}
        title={t('entity.info.skills')}
      />
      {mode === 'edit' && (
        <>
          <FormDropdownComponent
            id="status"
            title={t('entity.info.status.title')}
            content={project.status}
            warningMsg={t('entity.info.status.dropDownDraftWarningMsg')}
            options={['draft', 'active', 'on_hold', 'completed', 'terminated']}
            onChange={handleChangeProject}
          />
          <FormDropdownComponent
            id="maturity"
            title={t('project.maturity.title')}
            content={project.maturity}
            // prettier-ignore
            options={['problem_statement', 'solution_proposal', 'prototype', 'proof_of_concept', 'implementation', 'demonstrated_impact']}
            onChange={handleChangeProject}
          />
          <FormToggleComponent
            id="is_looking_for_collaborators"
            title={t('project.form.looking_for_collab_title')}
            choice1={t('general.no')}
            choice2={t('general.yes')}
            isChecked={project.is_looking_for_collaborators}
            onChange={handleChangeProject}
          />
          <FormToggleComponent
            id="is_private"
            warningMsg={t('project.info.publicPrivateToggleMsg')}
            title={t('entity.info.is_private')}
            choice1={t('general.public')}
            choice2={t('general.private')}
            color1="#F9530B"
            color2="#27B40E"
            isChecked={project.is_private}
            onChange={handleChangeProject}
          />
          <AllowPostingToAllToggle feedId={project.feed_id} />
          {/* show grant info field only to project super reviewer */}
          {project.is_reviewer && project.reviews_count === 1 && (
            <FormDefaultComponent
              id="grant_info"
              title="Grant Info: round name/number, amount, date, link(optional) [each value separated by comma]"
              placeholder="OpenCovid19 Grant Review (Round 5), 4000, 2021-03-21"
              content={project.grant_info}
              onChange={handleChangeProject}
            />
          )}
        </>
      )}
      {renderBtnsForm()}
    </form>
  );
};

export default ProjectForm;
