import React, { useState, useMemo, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import Alert from '../Tools/Alert';
import Button from '../primitives/Button';
import Box from '../Box';
import Loading from '../Tools/Loading';
import { Project } from 'types';
import SpinLoader from '../Tools/SpinLoader';

interface PropsModal {
  alreadyPresentProjects: Project[];
  spaceId: number;
  hasFollowed: boolean;
  // mutateProjects: (data?: any, shouldRevalidate?: boolean) => Promise<any>;
  closeModal: () => void;
}
export const ProjectLinkToSpace: FC<PropsModal> = ({
  alreadyPresentProjects,
  spaceId,
  hasFollowed,
  // mutateProjects,
  closeModal,
}) => {
  const { data: dataProjectsMine, error } = useGet('/api/projects/mine');
  const [selectedProject, setSelectedProject] = useState(null);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();
  const { t } = useTranslation('common');

  // Filter the projects that are already in this challenge so you don't add it twice!
  const filteredProjects = useMemo(() => {
    if (dataProjectsMine) {
      return dataProjectsMine.filter((projectMine) => {
        // Check if my project is found in alreadyPresentProjects
        const isMyProjectAlreadyPresent = alreadyPresentProjects
          // filter to not add the projects of which submission are pending, so they can appear in the other list, marked as "pending approval"
          ?.filter(
            ({ affiliated_spaces }) =>
              affiliated_spaces[0]?.find((obj) => obj.id === spaceId)?.affiliation_status !== 'pending'
          )
          .find((alreadyPresentProject) => {
            return alreadyPresentProject.id === projectMine.id;
          });
        // We keep only the ones that are not present
        return !isMyProjectAlreadyPresent;
      });
    }
    return undefined;
  }, [dataProjectsMine, alreadyPresentProjects]);

  // const filteredProjects = dataProjectsMine;

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Link this project to the challenge then mutate the cache of the projects from the parent prop
    if ((selectedProject as { id: number })?.id) {
      await api
        .post(`/api/projects/${selectedProject.id}/affiliations/spaces/${spaceId}`)
        .then(() => {
          setSending(false);
          setRequestSent(true);
          setIsButtonDisabled(true);
          // mutateProjects({ projects: [...alreadyPresentProjects, selectedProject] });
        })
        .catch(() => {
          setSending(false);
          console.error(`Could not PUT/affiliate spaceId=${spaceId} with project projectId=${selectedProject.id}`);
        });
      !hasFollowed && api.put(`/api/spaces/${spaceId}/follow`); // then follow it if user is not following already
      // close modal after 3.5sec
      setTimeout(() => {
        closeModal();
      }, 3500);
    }
  };

  const onProjectSelect = (e) => {
    setSelectedProject(filteredProjects.find((item) => item.id === parseInt(e.target.value)));
    setIsButtonDisabled(false);
  };

  return (
    <div>
      <Box mb={[7, 4]}>
        <a href="/project/create" target="_blank" style={{ right: '1rem', position: 'absolute' }}>
          {t('general.createProject')}
        </a>
      </Box>
      {!filteredProjects ? (
        <Loading />
      ) : filteredProjects?.length > 0 && dataProjectsMine?.filter((project) => project.is_admin).length > 0 ? (
        <form style={{ textAlign: 'left' }}>
          {filteredProjects
            .filter((project) => project.is_admin)
            .map((project, index) => {
              const projectCurrentChallengeInfo = project.affiliated_spaces[0];
              const isPending =
                projectCurrentChallengeInfo &&
                projectCurrentChallengeInfo.length !== 0 &&
                projectCurrentChallengeInfo[0].affiliation_status === 'pending'; // check if project's submission to the challenge is pending, and then disable to radio and add a text
              return (
                <div className={`form-check ${isPending && 'disabled'}`} key={index} style={{ height: '50px' }}>
                  <input
                    type="radio"
                    className="form-check-input"
                    name="exampleRadios"
                    id={`project-${index}`}
                    value={project.id}
                    onChange={onProjectSelect}
                    disabled={isPending}
                  />
                  <label className="form-check-label" htmlFor={`project-${index}`}>
                    {project.title}
                    {isPending && ` (${t('challenge.acceptState.pendingApproval')})`}
                  </label>
                </div>
              );
            })}
          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-2">
              <>
                {sending && <SpinLoader />}
                {t('attach.project.btnSend')}
              </>
            </Button>
            {requestSent && <Alert type="success" message={t('attach.project.success_space')} />}
          </div>
        </form>
      ) : (
        <div className="noProject" style={{ textAlign: 'center' }}>
          {t('attach.project.noProject')}
        </div>
      )}
    </div>
  );
};
