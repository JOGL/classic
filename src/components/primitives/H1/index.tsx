import { typography, flexbox, space } from 'styled-system';
import styled from 'utils/styled';

const H1 = styled.h1`
  ${[flexbox, space, typography]};
  font-family: ${(p) => p.theme.fonts.secondary};
  margin-bottom: 0;
`;
export default H1;
