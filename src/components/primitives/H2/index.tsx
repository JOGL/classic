import { typography, flexbox, space } from 'styled-system';
import styled from 'utils/styled';

const H2 = styled.h2`
  ${[flexbox, space, typography]};
  font-family: ${(p) => p.theme.fonts.secondary};
  margin-bottom: 0;
  /* font-size: 1.8rem !important; */
`;
export default H2;
