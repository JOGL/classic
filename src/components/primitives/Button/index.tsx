import tw, { css, styled } from 'twin.macro';

const Container = styled.button(({ btnType, width, disabled }) => [
  tw`text-base px-3 py-2`,
  tw`transition-colors duration-150`,
  btnType === 'secondary' ? tw`bg-white` : btnType === 'danger' ? tw`bg-danger` : tw`bg-primary`,
  btnType === 'secondary' ? tw`text-primary` : tw`text-white`,
  tw`border rounded border-solid focus:(outline-none border-blue-300)`,
  btnType === 'danger' ? tw`border-danger` : tw`border-primary`,
  btnType === 'secondary' && !disabled && tw`hocus:(text-white bg-primary)`,
  btnType !== 'secondary' && !disabled && tw`hover:opacity[.95]`,
  disabled ? tw`cursor-not-allowed opacity-50` : tw`cursor-pointer`,
  css`
    width: ${width || 'fit-content'};
  `,
]);
export default function Button(props) {
  return <Container {...props} />;
}
