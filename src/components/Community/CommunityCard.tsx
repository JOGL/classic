/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import { WidthProps } from 'styled-system';
import Box from 'components/Box';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStarIcon from 'components/Tools/BtnStarIcon';
import { DataSource } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import Chips from '../Chip/Chips';
import { theme } from 'twin.macro';

interface Props {
  id: number;
  title: string;
  shortTitle: string;
  short_description: string;
  postsCount?: number;
  members_count: number;
  has_saved?: boolean;
  banner_url?: string;
  width?: WidthProps['width'];
  cardFormat?: string;
  source?: DataSource;
  skills?: string[];
}
const CommunityCard: FC<Props> = ({
  id,
  title,
  shortTitle,
  short_description,
  postsCount,
  members_count,
  has_saved,
  banner_url = '/images/default/default-group.jpg',
  width,
  cardFormat,
  source,
  skills,
}) => {
  const groupUrl = `/community/${id}/${shortTitle}`;
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';
  return (
    <ObjectCard imgUrl={banner_url} href={groupUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={groupUrl} passHref>
          <Title tw="pr-2">
            <H2 tw="word-break[break-word]" fontSize={TitleFontSize}>
              {title}
            </H2>
          </Title>
        </Link>
        {(has_saved !== undefined || source === 'algolia') && (
          <BtnStarIcon itemType="communities" itemId={id} saveState={has_saved} source={source} />
        )}
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <div tw="line-clamp-4 flex-1">{short_description}</div>
      {skills && (
        <Chips
          data={skills.map((skill) => ({
            title: skill,
            href: `/search/groups/?refinementList[skills][0]=${skill}`,
          }))}
          overflowLink={`/community/${id}/${shortTitle}`}
          type="skills"
          showCount={3}
          smallChips
        />
      )}
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2} flexWrap="wrap">
          <CardData value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
          {postsCount > 0 && <CardData value={postsCount} title={<TextWithPlural type="post" count={postsCount} />} />}
        </Box>
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

export default CommunityCard;
