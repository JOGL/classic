import { FC } from 'react';
import Grid from '../Grid';
import CommunityCard from './CommunityCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import { Community } from 'types';

interface Props {
  listCommunities: Community | any[];
  cardFormat?: string;
}

const CommunityList: FC<Props> = ({ listCommunities, cardFormat = undefined }) => {
  return (
    <Grid tw="pt-3">
      {!listCommunities ? (
        <Loading />
      ) : listCommunities.length === 0 ? (
        <NoResults type="group" />
      ) : (
        listCommunities.map((community, i) => (
          <CommunityCard
            key={i}
            id={community.id}
            title={community.title}
            shortTitle={community.short_title}
            short_description={community.short_description}
            members_count={community.members_count}
            clapsCount={community.claps_count}
            postsCount={community.posts_count}
            has_saved={community.has_saved}
            // show skills only if cardFormat is not compact
            {...(cardFormat !== 'compact' && { skills: community.skills })}
            banner_url={community.banner_url || '/images/default/default-group.jpg'}
            cardFormat={cardFormat}
          />
        ))
      )}
    </Grid>
  );
};

export default CommunityList;
