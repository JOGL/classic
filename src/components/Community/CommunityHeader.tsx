/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H1 from 'components/primitives/H1';
// import BtnClap from 'components/Tools/BtnClap';
import InfoInterestsComponent from 'components/Tools/Info/InfoInterestsComponent';
import { useModal } from 'contexts/modalContext';
import useMembers from 'hooks/useMembers';
import { Community } from 'types';
import { displayObjectDate, renderOwnerNames } from 'utils/utils';
import Box from '../Box';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import BtnFollow from '../Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import BtnStar from '../Tools/BtnStar';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import QuickSearchBar from '../Tools/QuickSearchBar';
import Chips from '../Chip/Chips';
// import Image from 'next/image';
import Image2 from '../Image2';
import { theme } from 'twin.macro';
interface Props {
  community: Community;
}
const CommunityHeader: FC<Props> = ({ community }) => {
  const { members, membersError } = useMembers('communities', community.id);
  const { showModal } = useModal();
  const { t } = useTranslation('common');

  const showMembersModal = (e) => {
    members &&
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid tw="py-0 sm:py-2 md:py-4">
              {members.map((member, i) => (
                <UserCard
                  key={i}
                  id={member.id}
                  firstName={member.first_name}
                  lastName={member.last_name}
                  nickName={member.nickname}
                  shortBio={member.short_bio}
                  logoUrl={member.logo_url}
                  hasFollowed={member.has_followed}
                  projectsCount={member.stats.projects_count}
                  mutualCount={member.stats.mutual_count}
                  skills={member.skills}
                  role={member.owner ? 'leader' : !member.owner && member.admin && 'admin'}
                />
              ))}
            </Grid>
          </>
        ),
        title: t('entity.tab.members'),
        maxWidth: '70rem',
      });
  };

  const {
    banner_url,
    followers_count = 0,
    has_followed,
    id,
    is_member,
    is_owner,
    members_count = 0,
    short_description,
    short_title,
    title,
    skills,
    ressources,
    interests,
    has_saved,
    saves_count,
    users_sm,
    creator,
    created_at,
    updated_at,
    is_pending,
    is_private,
  } = community;
  return (
    <div className="row communityHeader">
      <Box alignItems="center" width="full" mb={5} px={4}>
        <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
          <H1 fontSize={['2.55rem', undefined, '3rem']}>{title}</H1>
          <div tw="pl-2">
            <ShareBtns type="community" specialObjId={id} />
          </div>
        </Box>
        {/* edit button */}
        {community.is_admin && (
          <Box mt={5}>
            <Link href={`/community/${community.id}/edit`}>
              <a style={{ display: 'flex', justifyContent: 'center' }}>
                <Edit size={23} title="Edit community" />
                {t('entity.form.btnAdmin')}
              </a>
            </Link>
          </Box>
        )}
      </Box>
      <div className="col-lg-7 col-md-12 communityHeader--banner">
        <Image2 src={banner_url || '/images/default/default-group.jpg'} alt={`${title} banner`} />
      </div>
      <div className="col-lg-5 col-md-12 communityHeader--info">
        <p className="infos">#{short_title}</p>
        <p className="info">{short_description}</p>
        <div tw="text-gray-500 inline-flex">
          {created_at && `${t('general.created_at')} ${displayObjectDate(created_at, 'LL', true)}`}
          {/* show updated date only if it exists and if it's not the same day as created date (OR if only updated_at exists and not created_at) */}
          {((updated_at && created_at && created_at.substr(0, 10) !== updated_at.substr(0, 10)) ||
            (updated_at && !created_at)) &&
            `${created_at ? ' / ' : ''} ${t('general.updated_at')} ${displayObjectDate(updated_at, 'LL', true)}`}
        </div>
        <Box pt={4} /> {/* empty div with paddingTop */}
        {users_sm !== undefined && users_sm.length > 0 && renderOwnerNames(users_sm, creator, t)}
        <InfoInterestsComponent title={t('general.sdgs')} content={interests} />
        {skills.length !== 0 && (
          <Box mb={4}>
            <Box color={theme`colors.secondary`}>{t('entity.info.skills')}</Box>
            <Chips
              data={skills.map((skill) => ({
                title: skill,
                href: `/search/groups/?refinementList[skills][0]=${skill}`,
              }))}
              overflowText="seeMore"
              type="skills"
              showCount={4}
            />
          </Box>
        )}
        {ressources.length !== 0 && (
          <Box mb={4}>
            <Box color={theme`colors.secondary`}>{t('entity.info.resources')}</Box>
            <Chips
              data={ressources.map((resource) => ({
                title: resource,
                href: `/search/groups/?refinementList[ressources][0]=${resource}`,
              }))}
              overflowText="seeMore"
              type="resources"
              showCount={4}
            />
          </Box>
        )}
        {/* <div className="communityStats">
          <span className="text" onClick={showFollowersModal} onKeyUp={showFollowersModal} tabIndex={0}>
            <strong>{followers_count}</strong>&nbsp;
            <TextWithPlural type="follower" count={followers_count} />
          </span>
          <span className="text" onClick={showMembersModal} onKeyUp={showMembersModal} tabIndex={0}>
            <strong>{members_count || 0}</strong>&nbsp;
            <TextWithPlural type="member" count={members_count} />
          </span>
        </div> */}
        <div tw="flex flex-wrap gap-x-4 gap-y-2 pb-7 items-center sm:pb-5">
          {!is_owner && (
            <BtnJoin
              joinState={is_pending ? 'pending' : is_member}
              isPrivate={is_private}
              itemType="projects"
              itemId={id}
              count={members_count}
              showMembersModal={showMembersModal}
            />
          )}
          <BtnFollow followState={has_followed} itemType="communities" itemId={id} count={followers_count} />
          <BtnStar itemType="communities" itemId={id} hasStarred={has_saved} count={saves_count} />
        </div>
      </div>
    </div>
  );
};
export default CommunityHeader;
