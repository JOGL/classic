import useTranslation from 'next-translate/useTranslation';
import AsyncCreatableSelect from 'react-select/async-creatable';
import algoliasearch from 'algoliasearch/lite';
import TitleInfo from 'components/Tools/TitleInfo';
// import "./FormResourcesComponent.scss";

const FormResourcesComponent = ({
  content = [],
  errorCodeMessage = '',
  id = 'resources',
  mandatory = false,
  type = 'default',
  onChange = (value, resources) => console.warn(`onChange doesn't exist to update ${value} of ${resources}`),
  placeholder = '',
  title = 'Title',
  tooltipMessage = undefined,
}) => {
  const { t } = useTranslation('common');

  const handleChange = (resourcesList) => {
    // convert react-select array (resourcesList) into an arry with just the resources labels
    const resources = resourcesList?.map(({ label }) => label);
    onChange(id, resources);
  };

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('Ressource'); // Ressource
  let algoliaResources = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['ressource_name'], // ressource_name
        hitsPerPage: 5,
      })
      .then((content) => {
        if (content) {
          algoliaResources = content.hits;
        } else {
          algoliaResources = [''];
        }
        algoliaResources = algoliaResources.map(({ ressource_name }) => {
          return { value: ressource_name, label: ressource_name };
        }); // ressource_name
        resolve(algoliaResources);
      });
  };
  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const currentResources = content.map((resource) => {
    if (resource.resource_name) {
      return { label: resource.resource_name, value: resource.resource_name };
    }
    return { label: resource, value: resource };
  });

  const customStyles = {
    loadingIndicator: (provided) => ({
      ...provided,
      display: 'none',
    }),
  };

  return (
    <div className="formResources">
      <TitleInfo title={title} mandatory={mandatory} tooltipMessage={tooltipMessage} />

      <div className="content ressource-container" id="ressources">
        <label tw="text-gray-400" className="form-check-label" htmlFor={`show${id}`}>
          {t(`help.resources.${type}`)}
        </label>
        <AsyncCreatableSelect
          name={id}
          cacheOptions
          isMulti
          defaultValue={currentResources && currentResources}
          defaultOptions={false}
          components={{ DropdownIndicator: null, IndicatorSeparator: null }}
          formatCreateLabel={(inputValue) => `${t('general.add')} "${inputValue}"`}
          blurInputOnSelect={false} // force keeping focus when selecting option (for mobile)
          noOptionsMessage={() => null}
          loadOptions={loadOptions}
          onChange={handleChange}
          placeholder={placeholder}
          // allowCreateWhileLoading={true}
          styles={customStyles}
        />
        {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
      </div>
    </div>
  );
};

export default FormResourcesComponent;
