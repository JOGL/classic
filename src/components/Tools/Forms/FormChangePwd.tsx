import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import cookie from 'js-cookie';
import Alert from 'components/Tools/Alert';
import { ApiContext } from 'contexts/apiContext';
import SpinLoader from '../SpinLoader';

class FormChangePwd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      old_password: '',
      password: '',
      password_confirmation: '',

      error: '',
      /* fireRedirect: false, */
      sending: false,
      success: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  checkPassword(pwd, pwd_confirm) {
    if (pwd !== pwd_confirm) {
      this.setState({ error: 'err-4001' });
      return false;
    }
    if (pwd.length < 8) {
      this.setState({ error: 'err-4002' });
      return false;
    }
    return true;
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value, error: '' });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { old_password, password, password_confirmation } = this.state;
    const jsonToSend = {
      current_password: old_password,
      password,
      password_confirmation,
    };
    if (this.checkPassword(password, password_confirmation)) {
      this.setState({ error: '', sending: true });
      const api = this.context;
      api
        .put('/api/auth/password', jsonToSend)
        .then((res) => {
          cookie.set('authorization', res.headers.authorization);
          cookie.set('uid', res.headers.uid);
          this.setState({ sending: false, success: true });
        })
        .catch((error) => {
          const errorMessage = error.response.data.error;
          this.setState({ sending: false, error: errorMessage });
        });
    }
  }

  render() {
    const { error, sending, success } = this.state;
    const { t } = this.props.i18n;
    const errorMessage = error.includes('err-') ? t(error) : error;

    let formStyle;
    let messageStyle;
    let title;
    let msg;
    if (!this.props.isSent) {
      formStyle = { display: 'block' };
      messageStyle = { display: 'none' };
      title = {
        text: 'Password reset',
        id: 'auth.changePwd.title',
      };
      msg = {
        text: 'Please enter a new password.',
        id: 'auth.changePwd.description',
      };
    }

    return (
      <div className="form-content">
        <div className="form-header">
          <h2 className="form-title" id="signModalLabel">
            {t(title.id)}
          </h2>
          <p>{t(msg.id)}</p>
        </div>
        <div className="form-body" style={formStyle}>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label className="form-check-label" htmlFor="old_password">
                {t('auth.changePwd.oldPwd')}
              </label>
              <input
                type="password"
                name="old_password"
                id="old_password"
                className="form-control"
                placeholder={t('auth.changePwd.oldPwd_placeholder')}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="password">
                {t('auth.changePwd.pwd')}
              </label>
              <input
                type="password"
                name="password"
                id="password"
                className="form-control"
                placeholder={t('auth.changePwd.pwd_placeholder')}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="password">
                {t('auth.changePwd.pwdConfirm')}
              </label>
              <input
                type="password"
                name="password_confirmation"
                id="password_confirmation"
                className="form-control"
                placeholder={t('auth.changePwd.pwd_placeholder')}
                onChange={this.handleChange}
              />
            </div>

            {error !== '' && <Alert type="danger" message={errorMessage} />}
            {success && <Alert type="success" message={t('info-4001')} />}

            <button tw="w-1/2" type="submit" className="btn btn-primary btn-block" disabled={!!sending}>
              {sending && <SpinLoader />}
              {t('auth.changePwd.btnConfirm')}
            </button>
          </form>
        </div>
        <div className="form-message" style={messageStyle}>
          <img src="/images/envelope.svg" alt="Message sent envelope" />
        </div>
      </div>
    );
  }
}
FormChangePwd.contextType = ApiContext;
export default withTranslation(FormChangePwd, 'common');
