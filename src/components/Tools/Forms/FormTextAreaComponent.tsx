import withTranslation from 'next-translate/withTranslation';
import { Component } from 'react';
import InfoMaxCharComponent from 'components/Tools/Info/InfoMaxCharComponent';
import TitleInfo from 'components/Tools/TitleInfo';
// import "./FormTextAreaComponent.scss";

class FormTextAreaComponent extends Component {
  static get defaultProps() {
    return {
      errorCodeMessage: '',
      id: 'default',
      isValid: undefined,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      placeholder: '',
      maxChar: undefined,
      mandatory: false,
      title: 'Title',
      supportLinks: false,
    };
  }

  handleChange(event) {
    this.props.onChange(event.target.id, event.target.value);
  }

  render() {
    const {
      content,
      errorCodeMessage,
      id,
      isValid,
      maxChar,
      mandatory,
      placeholder,
      title,
      rows = 5,
      supportLinks = false,
    } = this.props;
    const { t } = this.props.i18n;

    return (
      <div className="formTextArea">
        <div tw="inline-flex">
          <TitleInfo title={title} mandatory={mandatory} />
          <div tw="italic pl-1.5">
            ({supportLinks && `${t('form.supportLinks')}, `}
            {maxChar || '340'} {t('form.maxChar')})
          </div>
        </div>
        <div className="content">
          <div className="input-group">
            <textarea
              className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''}`}
              id={id}
              placeholder={placeholder}
              value={content === null ? '' : content}
              rows={rows}
              onChange={this.handleChange.bind(this)}
            />
            {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
          </div>
          <InfoMaxCharComponent content={content} maxChar={maxChar} />
        </div>
      </div>
    );
  }
}

export default withTranslation(FormTextAreaComponent, 'common');
