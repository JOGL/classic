import withTranslation from 'next-translate/withTranslation';
import { Component } from 'react';
import { applyPattern } from 'components/Tools/Forms/FormChecker';
import TitleInfo from 'components/Tools/TitleInfo';
import InfoMaxCharComponent from '../Info/InfoMaxCharComponent';
// import "./FormDefaultComponent.scss";

class FormDefaultComponent extends Component {
  static get defaultProps() {
    return {
      beHide: false,
      content: '',
      errorCodeMessage: '',
      id: 'default',
      maxChar: undefined,
      isValid: undefined,
      mandatory: false,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      pattern: undefined,
      placeholder: '',
      title: undefined,
      tooltipMessage: undefined,
      type: 'text',
    };
  }

  handleChange(event) {
    let { id, value } = event.target;
    const { content, pattern } = this.props;
    value = applyPattern(value, content, pattern);
    this.props.onChange(id, value);
  }

  renderPrepend(prepend) {
    if (prepend) {
      return (
        <div className="input-group-prepend">
          <div className="input-group-text">{prepend}</div>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }

  render() {
    const {
      content,
      errorCodeMessage,
      id,
      isValid,
      mandatory,
      placeholder,
      prepend,
      title,
      tooltipMessage,
      type,
      minDate,
      maxChar,
    } = this.props;
    const { t } = this.props.i18n;
    return (
      <div className="formDefault">
        {title && !tooltipMessage && <TitleInfo title={title} mandatory={mandatory} />}
        {title && tooltipMessage && <TitleInfo title={title} mandatory={mandatory} tooltipMessage={tooltipMessage} />}
        <div className="content">
          <div className="input-group">
            {this.renderPrepend(prepend)}
            <input
              type={type}
              className={`form-control ${isValid !== undefined ? (isValid ? 'is-valid' : 'is-invalid') : ''}`}
              id={id}
              placeholder={placeholder}
              value={content === null ? '' : content}
              onChange={this.handleChange.bind(this)}
              // if input is date and has a minDate, disable selecting all days before minDate
              min={minDate && minDate}
            />
            {errorCodeMessage && <div className="invalid-feedback">{t(errorCodeMessage)}</div>}
          </div>
          <InfoMaxCharComponent content={content} maxChar={maxChar} />
        </div>
      </div>
    );
  }
}

export default withTranslation(FormDefaultComponent, 'common');
