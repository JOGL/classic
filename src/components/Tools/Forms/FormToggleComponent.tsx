import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import TitleInfo from 'components/Tools/TitleInfo';
import Box from 'components/Box';
import { UserContext } from 'contexts/UserProvider';
import { logEventToGA } from 'utils/analytics';
// import "./FormToggleComponent.scss";

class FormToggleComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      id: 'default',
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      isChecked: false,
    };
  }

  handleChange(event) {
    const userId = this.context?.user?.id;
    if (this.props.toggleType === 'notif' && !event.target.checked) {
      // if toggle is a notif toggle and user disabled it, record this event to Google Analytics
      logEventToGA('disable notification type', 'Toggle', `[${userId},${event.target.id}]`, {
        userId,
        notifCategory: event.target.id,
      });
    }
    this.props.onChange(event.target.id, event.target.checked);
  }

  render() {
    const { id, title, isChecked, color1, color2, choice1, choice2, warningMsg, isDisabled, toggleType } = this.props;
    const colorUnchecked = color2 || 'grey';
    const colorChecked = color1 || '#27B40E';

    return (
      <Box className={`formToggle ${toggleType === 'notif' && 'notif'}`} opacity={isDisabled ? '.2' : undefined}>
        {title && <TitleInfo title={title} />}
        <div className="content">
          {warningMsg && <p>{warningMsg}</p>}
          <div className="toggle">
            {choice1 && <span className={`choice left ${isChecked ? ' selected' : ''}`}>{choice1}</span>}
            <label className="switch">
              <input id={id} type="checkbox" checked={isChecked} onChange={this.handleChange} />
              <Box
                className="slider round"
                style={{
                  backgroundColor: isChecked ? colorChecked : colorUnchecked,
                  cursor: isDisabled ? 'not-allowed' : 'pointer',
                }}
              />
            </label>
            {choice2 && <span className={`choice${!isChecked ? ' selected' : ''}`}>{choice2}</span>}
          </div>
        </div>
      </Box>
    );
  }
}
export default withTranslation(FormToggleComponent, 'common');
FormToggleComponent.contextType = UserContext;
