import algoliasearch from 'algoliasearch';
import withTranslation from 'next-translate/withTranslation';
import dynamic from 'next/dynamic';
import { Component } from 'react';
import TitleInfo from 'components/Tools/TitleInfo';
// import "./FormTextAreaComponent.scss";

const CustomReactQuill = dynamic(import('components/Tools/CustomReactQuill'), { ssr: false });

class FormWysiwygComponent extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  modules = {
    keyboard: {
      bindings: {
        tab: {
          key: 9,
          handler: function (range, context) {
            return true;
          },
        },
      },
    },
    toolbar: [
      [{ header: 1 }, { header: 2 }],
      ['bold', 'italic', 'underline', 'strike'],
      [{ color: [] }, { background: [] }],
      [{ align: [] }],
      [{ script: 'sub' }, { script: 'super' }],
      ['code-block', 'blockquote'],
      [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
      ['link', 'image', 'video'],
      ['clean'],
      ['emoji'], // emoji support
    ],
    'emoji-toolbar': true,
    'emoji-textarea': true,
    'emoji-shortname': true, // emoji support
    imageDrop: true, // ability to drop image directly in the editor
    imageResize: {
      modules: ['Resize', 'DisplaySize'],
    },
    mention: {
      // quill-mention features
      allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/, // allowed characters
      mentionDenotationChars: ['@', '#'], // characters that triggers source function
      linkTarget: '_blank',
      source(searchTerm, renderList, mentionChar) {
        // function triggered when typing one of mentionDenotationChars
        const appId = process.env.ALGOLIA_APP_ID; // algolia app id
        const token = process.env.ALGOLIA_TOKEN; // algolia token
        const client = algoliasearch(appId, token); // initialize algolia client
        const index = mentionChar === '@' ? client.initIndex('User') : client.initIndex('Project'); // set client index depending on mention character
        index
          .search(searchTerm, {
            hitsPerPage: 10,
          })
          .then((content) => {
            const objectsList =
              mentionChar === '@'
                ? // if mentionChar is @ (user), render a user specific object
                  content.hits.map((obj) => ({
                    value: `${obj.first_name} ${obj.last_name}`, // "value" is needed by quill-mention, it's the text that will be displayed after selecting the mention. Here it's user full name
                    name: obj.nickname, // not required by quill-mention but we set "name" so it can be displayed in the mention list (renderItem function). Here it's user nickname
                    link: `/user/${obj.objectID}`, // "link" is required by quill-mention to englobe "value" around a link automatically. Here it's relative link to the user
                    imageUrl: obj.logo_url_sm, // not required by quill-mention but we set "imageUrl" so it can be displayed in the mention list (renderItem function). Here it's user profile image
                  }))
                : // else render project specific object
                  content.hits.map((obj) => ({
                    value: obj.title, // project title
                    name: obj.short_title, // project short yiyle
                    link: `/project/${obj.objectID}`, // relative link to the project
                    imageUrl: obj.banner_url ? obj.banner_url : '/images/default/default-project.jpg', // user profile image // project banner image
                  }));
            renderList(objectsList); // quill-mention function that renders the list of matching mentions
          });
      },
      renderItem(item) {
        // quill-mention function that gives control over how matching mentions from source are displayed within the list
        return `
          <div class="cql-list-item-inner">
            <img src="${item.imageUrl}"/>
            ${item.value} <span>${item.name}</span>
          </div>`;
      },
    },
  };

  formats = [
    'header',
    'align',
    'color',
    'background',
    'script',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
    'code-block',
    'span',
    'video',
    'emoji',
    'mention',
    'alt',
    'width',
    'height',
    'style',
  ];

  static get defaultProps() {
    return {
      id: 'default',
      title: 'Title',
      content: '',
      show: false,
      onChange: (value) => console.warn(`onChange doesn't exist to update ${value}`),
      placeholder: '',
      maxChar: undefined,
      mandatory: false,
      beHide: false,
    };
  }

  handleChange(event) {
    this.props.onChange(this.props.id, event);
  }

  static renderBeHide(id, beHide) {
    if (beHide) {
      return (
        <div className="form-group form-check">
          <input type="checkbox" className="form-check-input" id={`show${id}`} />
          <label className="form-check-label" htmlFor={`show${id}`}>
            {this.props.i18n.t('entity.form.btnAdmin')}
          </label>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }

  static renderPrepend(prepend) {
    if (prepend) {
      return (
        <div className="input-group-prepend">
          <div className="input-group-text">{prepend}</div>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }

  render() {
    const { title, id, content, show, placeholder, mandatory } = this.props;
    // document.querySelector('.ql-clipboard').focus();
    const { t } = this.props.i18n;

    return (
      <div className="formTextArea">
        <TitleInfo title={title} mandatory={mandatory} />
        <div>
          {!show && ( // only show button if prop is true
            <a
              className="btn btn-primary"
              data-toggle="collapse"
              href={`#${id}`}
              role="button"
              aria-expanded="false"
              aria-controls="collapseExample"
            >
              {t('entity.form.btnAdmin')}
            </a>
          )}
          <div className={show ? '' : 'collapse'} id={id}>
            <CustomReactQuill
              formats={this.formats}
              modules={this.modules}
              onChange={this.handleChange}
              placeholder={placeholder}
              style={{ width: '100%', paddingBottom: '10px' }}
              theme="snow"
              value={content !== null ? content : ''}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation(FormWysiwygComponent, 'common');
