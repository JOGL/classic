import { Paperclip } from '@emotion-icons/fa-solid/';
import withTranslation from 'next-translate/withTranslation';
import React, { Component, ReactNode } from 'react';
import { ApiContext } from 'contexts/apiContext';
import { ItemType } from 'types';
import SpinLoader from './SpinLoader';

interface Props {
  fileTypes: string[];
  itemId: number;
  itemType: ItemType;
  maxSizeFile: number;
  onIconUpload: (result: any) => void;
  setListFiles: (listFiles: any) => void;
  refresh: () => void;
  type: string;
  imageUrl: string;
  text: string | ReactNode;
  textUploading: string;
  uploadNow: boolean;
}
interface State {
  uploading: boolean;
}
class BtnUploadIcon extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      fileTypes: [''],
      itemId: '',
      itemType: '',
      maxSizeFile: 31457280,
      onIconUpload: (result) => console.warn('Oops, the function onIconUpload is missing !', result),
      setListFiles: (listFiles) => console.warn('Oops, the function setListFiles is missing !', listFiles),
      type: '',
      imageUrl: '',
      uploadNow: false,
    };
  }

  handleChange(event) {
    event.preventDefault();
    const { t } = this.props.i18n;
    if (event.target.files) {
      if (!this.validFilesSize(event.target.files)) {
        this.props.onIconUpload({
          error: (
            <>
              {t('err-1001')}
              &nbsp;(max : {this.props.maxSizeFile / 1024 / 1024}
              Mo)
            </>
          ),
          url: '',
        });
      } else {
        this.props.onIconUpload(event.target.files[0]);
        document.getElementById('btnUpload').value = '';
      }
    }
  }

  validFilesSize(files) {
    const { maxSizeFile } = this.props;
    let nbFilesValid = 0;
    if (files) {
      for (let index = 0; index < files.length; index++) {
        if (files[index].size <= maxSizeFile) {
          nbFilesValid++;
        }
      }
    }
    return nbFilesValid === files.length; // return true or false depending on condition
  }

  render() {
    const { fileTypes, text } = this.props;
    const { uploading } = this.state;
    const { t } = this.props.i18n;
    if (uploading) {
      // if "uploading" true, make button and input disabled (unclickable)
      // TODO: Don't use querySelector, maybe use ref or conditional rendering.
      document.querySelector('.upload-btn-wrapper .btn').disabled = true;
      document.querySelector('.upload-btn-wrapper input').disabled = true;
    }

    const textUpload = text || t('info-1000');
    const textUploading = t('info-1001');

    return (
      <>
        <div className="upload-btn-wrapper">
          <div className="btn">
            {uploading ? ( // render differently depending on uploading state
              <>
                <SpinLoader />
                {textUploading}
              </>
            ) : (
              <>
                <Paperclip size={25} title="Upload file" style={{ position: 'relative', top: '-2px' }} />
                {textUpload}
              </>
            )}
          </div>
          <input id="btnUpload" type="file" accept={fileTypes.join()} multiple={false} onChange={this.handleChange} />
        </div>
      </>
    );
  }
}
BtnUploadIcon.contextType = ApiContext;
export default withTranslation(BtnUploadIcon, 'common');
