import React, { FC, useEffect, useState, Dispatch, SetStateAction } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import { ItemType } from 'types';
import useUser from 'hooks/useUser';
import Axios from 'axios';
import SpinLoader from './SpinLoader';
import { logEventToGA } from 'utils/analytics';
import { Notifications, NotificationsNone } from '@emotion-icons/material';
import { useModal } from 'contexts/modalContext';
import ListFollowers from './ListFollowers';
import tw from 'twin.macro';

interface Props {
  followState: boolean;
  itemType: ItemType;
  itemId: number;
  count?: number;
  hasNoStat?: boolean;
  dataComesFromAlgolia?: boolean;
  isDisabled?: boolean;
}

const BtnFollow: FC<Props> = ({
  followState: followStateProp,
  itemType = undefined,
  itemId = undefined,
  count: countProp = 0,
  hasNoStat = false,
  dataComesFromAlgolia = false,
  isDisabled = false,
}) => {
  const [followState, setFollowState] = useState(followStateProp);
  const [sending, setSending] = useState(false);
  const [count, setCount] = useState(countProp);
  const api = useApi();
  const { showModal } = useModal();
  const { t } = useTranslation('common');
  const { user } = useUser();

  useEffect(() => {
    // if data also comes from algolia (and not only api), get save state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (dataComesFromAlgolia) {
      if (user) {
        const fetchSaves = async () => {
          const res = await api
            .get(`/api/${itemType}/${itemId}/follow`, { cancelToken: axiosSource.token })
            .catch((err) => {
              if (!Axios.isCancel(err)) {
                console.error("Couldn't GET saves", err);
              }
            });
          if (res?.data?.has_followed) {
            setFollowState(res.data.has_followed);
          }
        };
        fetchSaves();
      }
      setFollowState(false);
    }
    return () => {
      // This will prevent to setFollowState on unmounted BtnStarIcon
      axiosSource.cancel();
    };
  }, [user]);

  const showFollowersModal = (e) => {
    count > 0 && // open modal only if project has followers
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={itemId} itemType={itemType} />,
        title: t('entity.tab.followers'),
        maxWidth: '61rem',
      });
  };

  const changeStateFollow = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      const action = followState ? 'unfollow' : 'follow';
      // send event to google analytics
      user &&
        logEventToGA(action, 'Button', `[${user.id},${itemId},${itemType}]`, { userId: user.id, itemId, itemType });
      if (action === 'follow') {
        // if user is following
        api
          .put(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setCount(count + 1);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't PUT /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      } else {
        // else unfollow
        api
          .delete(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setCount(count - 1);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't DELETE /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      }
    }
  };
  const text = followState ? t('general.unfollow') : t('general.follow');
  return (
    <span tw="relative z-0 inline-flex shadow-sm rounded-md" id="followBtn">
      <button
        type="button"
        tw="relative inline-flex items-center p-2 border border-solid bg-white text-sm font-medium hover:bg-gray-100 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
        css={[
          hasNoStat ? tw`rounded-md border-primary text-primary` : tw`rounded-l-md border-gray-300 text-gray-700`,
          isDisabled && tw`opacity-40`,
        ]}
        onClick={changeStateFollow}
        disabled={sending || isDisabled}
      >
        {sending ? (
          <SpinLoader />
        ) : followState ? (
          <Notifications size={22} title={text} css={[hasNoStat && tw`text-primary`]} />
        ) : (
          <NotificationsNone size={22} title={text} css={[hasNoStat && tw`text-primary`]} />
        )}
        {text}
      </button>
      {!hasNoStat && (
        <button
          type="button"
          tw="-ml-px relative inline-flex items-center px-2 py-2 rounded-r-md border border-solid border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-100 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
          onClick={showFollowersModal}
        >
          {count}
        </button>
      )}
    </span>
  );
};
export default BtnFollow;
