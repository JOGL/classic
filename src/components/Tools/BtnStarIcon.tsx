// TODO This component over-fetches way to much data just to get the save count.
// TODO The component should useGet for caching.
// ! This component isn't self explanatory,
import Axios from 'axios';
import React, { FC, useEffect, useState, Dispatch, SetStateAction } from 'react';
import styled from '@emotion/styled';
import { Star as StarFilled } from '@emotion-icons/boxicons-solid';
import { Star } from '@emotion-icons/boxicons-regular';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import { useApi } from 'contexts/apiContext';
import { DataSource, ItemType } from 'types';
import ReactTooltip from 'react-tooltip';
import useTranslation from 'next-translate/useTranslation';
import useUser from 'hooks/useUser';
import SpinLoader from './SpinLoader';
import { logEventToGA } from 'utils/analytics';
import tw, { css } from 'twin.macro';

interface Props {
  source?: DataSource;
  saveState: boolean;
  itemId: number;
  itemType: ItemType;
  refresh?: () => void;
}

const StarStyleWrapper = styled.div`
  ${EmotionIconBase} {
    opacity: 0.8;
    cursor: pointer;
    color: #4c4c4c;
    width: 27px;
    height: 27px;
    margin: 0;
    padding: 2.5px;
    display: flex;
  }
`;

const saveStateIcon = {
  unsaved: <Star title="Star" />,
  saved: <StarFilled title="Unstar" />,
};

const BtnStarIcon: FC<Props> = ({ source = 'api', saveState: propsSaveState = false, itemId, itemType, refresh }) => {
  const [saveState, setSaveState] = useState(propsSaveState);
  const [action, setAction] = useState<'save' | 'unsave'>(propsSaveState ? 'unsave' : 'save');
  const [actionStar, setActionStar] = useState<'star' | 'unstar'>(propsSaveState ? 'unstar' : 'star');
  const [icon, setIcon] = useState<'saved' | 'unsaved'>(saveState ? 'saved' : 'unsaved');
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');
  const { user } = useUser();
  useEffect(() => {
    // if data also comes from algolia (and not only api), get save state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (source === 'algolia' && user) {
      const fetchSaves = async () => {
        const res = await api
          .get(`/api/${itemType}/${itemId}/save`, { cancelToken: axiosSource.token })
          .catch((err) => {
            if (!Axios.isCancel(err)) {
              console.error("Couldn't GET saves", err);
            }
          });
        if (res?.data?.has_saved) {
          setSaveState(true);
          setIcon('saved');
        }
      };
      fetchSaves();
    }
    return () => {
      // This will prevent to setSaveState on unmounted BtnStarIcon
      axiosSource.cancel();
    };
    // }, [api, itemId, itemType, source, user]);
  }, [user]);

  useEffect(() => {
    setAction(saveState ? 'unsave' : 'save');
    setActionStar(saveState ? 'unstar' : 'star');
  }, [saveState]);

  const changeStateSave = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      if (action === 'save') {
        api
          .put(`/api/${itemType}/${itemId}/save`)
          .then(() => {
            // send event to google analytics
            logEventToGA(action, 'Button', `[${user.id},${itemId},${itemType}]`, { userId: user.id, itemId, itemType });
            setSaveState(true);
            setSending(false);
          })
          .catch(() => setSending(false));
      } else {
        api.delete(`/api/${itemType}/${itemId}/save`).then(() => {
          setSaveState(false);
          setSending(false);
        });
      }
      refresh && refresh();
    }
  };
  return (
    <div tw="absolute top-2 right-2 bg-gray-200 rounded-full height[25px] width[25px] flex place-content-center">
      {/* <div tw="absolute top-2 right-2 bg-white rounded-full border border-gray-400 border-solid"> */}
      <div
        className="saveBtn"
        css={[
          tw`m-auto`,
          !sending && tw`margin-top[-2px]`,
          css`
            svg {
              margin: 0 !important;
            }
          `,
        ]}
      >
        {sending ? (
          <SpinLoader />
        ) : (
          <>
            <a
              tabIndex={0}
              onMouseEnter={() => setIcon(saveState ? 'unsaved' : 'saved')}
              onMouseLeave={() => setIcon(saveState ? 'saved' : 'unsaved')}
              onClick={changeStateSave}
              onKeyUp={changeStateSave}
              data-tip={t(`general.${actionStar}_explain`)}
              data-for="save"
              aria-describedby={`save ${itemType}`}
            >
              <StarStyleWrapper>{saveStateIcon[icon]}</StarStyleWrapper>
            </a>
            <ReactTooltip id="save" delayHide={300} effect="solid" role="tooltip" />
          </>
        )}
      </div>
    </div>
  );
};

export default BtnStarIcon;
