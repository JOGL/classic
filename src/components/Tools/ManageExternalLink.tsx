import React, { FC, FormEvent, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Box from 'components/Box';
import Button from 'components/primitives/Button';
import H2 from 'components/primitives/H2';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import Card from 'components/Cards/Card';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormDropdownComponent from './Forms/FormDropdownComponent';
import FormIconComponent from './Forms/FormIconComponent';
import Loading from './Loading';
import Swal from 'sweetalert2';

interface Link {
  icon_url: string;
  id: number;
  icon: any;
  name: string;
  url: string;
}
var linkStartWithProtocolPattern = /^((http|https|ftp):\/\/)/;
const ManageExternalLink = ({ itemType, itemId, showTitle = true }) => {
  const { t } = useTranslation('common');
  const {
    data: linkList,
    loading,
    mutate: linkListMutate,
    revalidate: linkListRevalidate,
  } = useGet<Link[]>(`/api/${itemType}/${itemId}/links`);
  const [showLinkCreate, setShowLinkCreate] = useState(false);
  const addLink = () => {
    setShowLinkCreate(!showLinkCreate);
  };
  return (
    <Box spaceY={3}>
      {showTitle && <H2>{t('general.externalLink.title')}</H2>}
      <Button type="button" onClick={addLink}>
        {t('general.add')}
      </Button>
      {showLinkCreate && (
        <CardFormCreate
          itemType={itemType}
          itemId={itemId}
          onCreate={(newLink: Link) => {
            linkListMutate(
              {
                data: [...linkList, newLink],
              },
              false
            );
            setShowLinkCreate(false);
          }}
        />
      )}
      <Box display="grid" tw="gridTemplateColumns[1fr] gap-5 lg:gridTemplateColumns[1fr 1fr]" pt={2}>
        {linkList &&
          [...linkList]
            .sort(function (a, b) {
              return a.id - b.id; // sort by id (asc)
            })
            .map((value, i) => (
              <Box key={i} pt={3}>
                <CardFormEdit
                  value={value}
                  itemType={itemType}
                  itemId={itemId}
                  onChange={(changedLink: Link) => {
                    linkListMutate(
                      {
                        data: linkList.map((link) => {
                          if (link.id === changedLink.id) return changedLink;
                          return link;
                        }),
                      },
                      false
                    );
                  }}
                  onDelete={(linkId: number) => {
                    linkListMutate(
                      {
                        // removes the link which has the same id as linkId
                        data: [...linkList].filter((link) => link.id !== linkId),
                      },
                      false
                    );
                  }}
                />
              </Box>
            ))}
        {loading && <Loading />}
      </Box>
    </Box>
  );
};
interface ICardFormCreate {
  itemType: string;
  itemId: number;
  onCreate?: (newLink: Link) => void;
}
interface ICardFormEdit {
  value: Link;
  itemType: string;
  itemId: number;
  onDelete?: (linkId: number) => void;
  onChange?: (changedLink: Link) => void;
}
const CardFormCreate: FC<ICardFormCreate> = ({ itemType, itemId, onCreate }) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [newIcon, setNewIcon] = useState();
  const [newLink, setNewLink] = useState<Link>({
    id: undefined,
    url: undefined,
    icon: '/images/icons/links-website.png',
    name: 'website',
    icon_url: undefined,
  });

  // handle every changes to the link
  const handleChange: (key: string, content: string) => void = (key, content) => {
    if (key === 'name') {
      const theIcon = `/images/icons/links-${content}.png`;
      setNewLink((prevLink) => ({ ...prevLink, ['icon']: theIcon }));
    }
    setNewLink((prevLink) => ({ ...prevLink, [key]: content }));
  };
  // handle upload of custom icon
  const handleIconUpload: (icon) => void = (icon) => {
    setNewIcon(icon);
  };
  // handle when submitting (update, or create), an link
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (newLink.url !== '' && newLink.name !== '') {
      // check if link starts by http or https
      if (!linkStartWithProtocolPattern.test(newLink.url)) {
        Swal.fire({ icon: 'error', text: t('general.externalLink.wrongProtocol') });
      } else {
        const bodyFormData = new FormData();
        bodyFormData.append('url', newLink.url);
        bodyFormData.append('name', newLink.name);
        // if user selected the custom option AND if he uploaded a custom icon, append uploaded icon to the request
        newLink.name === 'custom' && newIcon && bodyFormData.append('icon', newIcon);
        const config = {
          headers: { 'Content-Type': 'multipart/form-data' },
        };
        api
          .post(`/api/${itemType}/${itemId}/links`, bodyFormData, config)
          .then((res) => {
            onCreate(res.data); // call onCreate function
            setNewLink({ url: '', icon: '/images/icons/links-website.png', name: 'website' }); // reset link fields
          })
          .catch((err) => console.warn(err));
      }
    } else {
      alert('Please fill the url field');
    }
  };
  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <Box flexDirection={['column', undefined, 'row']} justifyContent="space-between">
          <Box width={'100%'} pr={[0, undefined, 5]}>
            <div tw="inline-flex">
              <FormDropdownComponent
                id="name"
                title={t('general.externalLink.website')}
                content={newLink.name}
                // prettier-ignore
                options={['website', 'facebook', 'linkedin', 'twitter', 'slack', 'instagram', 'github', 'gitlab',
                'dribbble', 'drive', 'dropbox', 'medium', 'skype', 'vimeo', 'youtube', 'custom']}
                onChange={handleChange}
              />
              <FormIconComponent
                id="icon"
                itemId={itemId}
                fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                maxSizeFile={4194304}
                itemType="users"
                imageUrl={newLink?.icon}
                isCustomLink={newLink.name === 'custom' ? true : false}
                onIconUpload={handleIconUpload}
              />
            </div>
            <FormDefaultComponent
              id="url"
              content={newLink.url}
              title={t('general.externalLink.url')}
              placeholder={t('general.externalLink.url_placeholder')}
              onChange={handleChange}
            />
          </Box>
          <Box spaceY={2} justifyContent="center" alignItems="flex-end" pt={[4, undefined, 0]}>
            <Button type="submit">{t('general.add')}</Button>
          </Box>
        </Box>
      </form>
    </Card>
  );
};

const CardFormEdit: FC<ICardFormEdit> = ({ value, itemType, itemId, onDelete, onChange }) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  // const [newIcon, setNewIcon] = useState();
  // handle every changes to the link
  const handleChange: (key: string, content: string) => void = (key, content) => {
    if (key === 'name') {
      const theIcon = `/images/icons/links-${content}.png`;
      // setLink((prevLink) => ({ ...prevLink, ['icon']: theIcon }));
      onChange({ ...value, icon: theIcon });
    } else onChange({ ...value, [key]: content });
    setShowSubmitBtn(content !== value.url || content !== value.icon || content !== value.name);
  };
  // handle upload of custom icon
  const handleIconUpload: (icon) => void = (icon) => {
    // setNewIcon(icon);
  };
  // handle when submitting (update, or create), an link
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (value.url !== '' && value.name !== '') {
      // check if link starts by http or https
      if (!linkStartWithProtocolPattern.test(value.url)) {
        Swal.fire({ icon: 'error', text: t('general.externalLink.wrongProtocol') });
      } else {
        api.patch(`/api/${itemType}/${itemId}/links/${value.id}`, value).then((res) => {
          onChange(value); // call onChange function
          setShowSubmitBtn(false); // hide update button
        });
      }
    } else {
      alert('Please fill the url field');
    }
  };
  // handle when deleting on link
  const handleDelete = () => {
    api.delete(`/api/${itemType}/${itemId}/links/${value.id}`).then((res) => {
      onDelete(value.id); // call onDelete function
    });
  };

  return (
    <Card overflow="show">
      <form onSubmit={handleSubmit}>
        <Box flexDirection={['column', undefined, 'row']} justifyContent="space-between">
          <Box width={'100%'} pr={[0, undefined, 5]}>
            <div tw="inline-flex">
              <FormDropdownComponent
                id="name"
                title={t('general.externalLink.website')}
                content={value.name}
                // prettier-ignore
                options={['website', 'facebook', 'linkedin', 'twitter', 'slack', 'instagram', 'github', 'gitlab',
                'dribbble', 'drive', 'dropbox', 'medium', 'skype', 'vimeo', 'youtube', 'custom']}
                onChange={handleChange}
              />
              <FormIconComponent
                id="icon"
                itemId={itemId}
                fileTypes={['image/jpeg', 'image/png', 'image/jpg']}
                maxSizeFile={4194304}
                itemType="users"
                imageUrl={value?.icon_url}
                isCustomLink={value.name === 'custom' ? true : false}
                onIconUpload={handleIconUpload}
              />
            </div>
            <FormDefaultComponent
              id="url"
              content={value.url}
              title={t('general.externalLink.url')}
              placeholder={t('general.externalLink.url_placeholder')}
              onChange={handleChange}
            />
          </Box>
          <Box spaceY={2} justifyContent="center" alignItems="flex-end" pt={[4, undefined, 0]}>
            {showSubmitBtn && (
              <Button type="submit" width="100%">
                {t('entity.form.btnUpdate')}
              </Button>
            )}
            <Button type="button" btnType="danger" onClick={handleDelete} width="100%">
              {t('feed.object.delete')}
            </Button>
          </Box>
        </Box>
      </form>
    </Card>
  );
};

export default ManageExternalLink;
