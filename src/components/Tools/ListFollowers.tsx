import React, { FC } from 'react';
import Loading from 'components/Tools/Loading';
import useGet from 'hooks/useGet';
import { ItemType } from 'types';
import Grid from '../Grid';
import UserCard from '../User/UserCard';

interface Props {
  itemId: number;
  itemType: ItemType;
}
const ListFollowers: FC<Props> = ({ itemId, itemType }) => {
  const { data, loading } = useGet(`/api/${itemType}/${itemId}/followers`);

  if (loading) return <Loading />;
  return (
    <div className="listFollowers">
      <Grid tw="gap-4">
        {data?.followers.map((follower, i) => (
          <UserCard
            key={i}
            id={follower.id}
            firstName={follower.first_name}
            lastName={follower.last_name}
            nickName={follower.nickname}
            shortBio={follower.short_bio}
            logoUrl={follower.logo_url}
            hasFollowed={follower.has_followed}
            projectsCount={follower.projects_count}
            mutualCount={follower.mutual_count}
            isCompact
          />
        ))}
      </Grid>
    </div>
  );
};
export default ListFollowers;
