import React, { FC } from 'react';
import SpinLoader from './SpinLoader';

interface Props {
  active?: boolean;
  height?: string;
}
const Loading: FC<Props> = ({ active = true, height = '5rem', children }) => {
  if (active) {
    return (
      <div style={{ height, display: 'flex' }}>
        <div className="d-flex justify-content-center" style={{ margin: 'auto', color: 'grey' }}>
          <SpinLoader size="32" />
        </div>
      </div>
    );
  }
  return <>{children}</>;
};
export default Loading;
