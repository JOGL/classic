import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Box from '../Box';
import H2 from 'components/primitives/H2';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import P from '../primitives/P';
import useGet from 'hooks/useGet';
import Loading from '../Tools/Loading';
import Carousel from '../Carousel';
import styled from 'utils/styled';
import Card from 'components/Cards/Card';
import { useModal } from 'contexts/modalContext';
import Link from 'next/link';

interface Props {
  objectType: 'spaces' | 'programs';
  objectId: number;
}

const ShowBoards: FC<Props> = ({ objectType, objectId }) => {
  const { t } = useTranslation('common');
  const { showModal } = useModal();
  const { data: dataBoards } = useGet(`/api/${objectType}/${objectId}/boards`);

  const showBoardModal = (title, description, users) => {
    showModal({
      children: <BoardMembersModal description={description} users={users} />,
      title: title,
      maxWidth: '61rem',
    });
  };

  return (
    <>
      {!dataBoards ? (
        <Loading />
      ) : (
        dataBoards?.length !== 0 && (
          <>
            <H2>{t('program.boardTitleFull')}</H2>
            <P>{t('program.board.message')}</P>
            <Carousel spaceX={12}>
              {dataBoards
                ?.sort(function (a, b) {
                  return a.id - b.id; // sort by id (asc)
                })
                .map(({ title, description, users }, index) => {
                  return (
                    <BoardCard key={index} width="16rem" height="18rem" justifyContent="space-between">
                      <Box>
                        <H2 onClick={() => showBoardModal(title, description, users)} as="button" fontSize="1.5rem">
                          {title}
                        </H2>
                        <Box maxHeight="9.5rem" overflow="hidden" position="relative">
                          <InfoHtmlComponent content={description} />
                          {description.length > 150 && ( // show view more link only of text has more than 15O char
                            <ViewMore onClick={() => showBoardModal(title, description, users)} as="button">
                              ...{t('general.showmore')}
                            </ViewMore>
                          )}
                        </Box>
                      </Box>
                      {users.length !== 0 && (
                        <Box row alignItems="center" pt={2}>
                          {/* show only 3 first members */}
                          {users?.slice(0, 3).map(({ logo_url_sm, id, first_name, last_name }, index) => (
                            <Link href={`/user/${id}`} key={index}>
                              <a>
                                <Img width="30px" height="30px" src={logo_url_sm} alt={`${first_name} ${last_name}`} />
                              </a>
                            </Link>
                          ))}
                          {users.length > 3 && <MoreMembers>{`+${users.length - 3}`}</MoreMembers>}
                          <Box pl={4}>
                            {users.length}&nbsp;{t('general.member', { count: users.length })}
                          </Box>
                        </Box>
                      )}
                    </BoardCard>
                  );
                })}
            </Carousel>
          </>
        )
      )}
    </>
  );
};

const BoardMembersModal = ({ description, users }) => (
  <Box width="100%">
    <InfoHtmlComponent content={description} />
    {users.length !== 0 && (
      <Box row justifyContent="space-around" pt={5} flexWrap="wrap">
        {users?.map(({ logo_url, id, first_name, last_name, short_bio }, index) => (
          <Card
            key={index}
            justifyContent="center"
            width="17rem"
            containerStyle={{ margin: '0 1.1rem 1.3rem' }}
            tw="mt-5"
          >
            <Link href={`/user/${id}`} key={index}>
              <a style={{ textAlign: 'center' }}>
                <Img width="80px" height="80px" src={logo_url} alt={`${first_name} ${last_name}`} />
                <P fontWeight="bold" fontSize="1.2rem" pt={2}>
                  {first_name + ' ' + last_name}
                </P>
              </a>
            </Link>
            <P mt={0}>{short_bio || '_ _'}</P>
          </Card>
        ))}
      </Box>
    )}
  </Box>
);

const BoardCard = styled(Card)`
  position: relative;
  border: 1px solid lightgray;
  h2:hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

const ViewMore = styled('span')`
  position: absolute;
  bottom: 0;
  right: 0;
  margin-bottom: 0;
  padding-left: 8px;
  background-color: white;
  font-size: 15px;
  cursor: pointer;
  text-transform: lowercase;

  &:hover {
    box-shadow: ${(p) => p.theme.colors.primary};
    text-decoration: underline;
  }
`;

const MoreMembers = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  object-fit: cover;
  background: ${(p) => p.theme.colors.greys['200']};

  text-align: center;
  line-height: 30px;
  font-size: 14px;
`;

const Img = styled.img`
  border-radius: 50%;
  object-fit: cover;
  margin-right: -4px;
`;

export default ShowBoards;
