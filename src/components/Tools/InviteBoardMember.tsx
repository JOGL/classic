// eslint-disable-next-line no-unused-vars
import { useState, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import AsyncSelect from 'react-select/async';
import algoliasearch from 'algoliasearch';
import Box from '../Box';
import styled from 'utils/styled';

const UserLabel = styled(Box)`
  align-items: center;
  div {
    font-size: 15px;
    color: #5c5d5d;
  }
  img {
    width: 28px;
    height: 28px;
    border-radius: 50%;
    object-fit: cover;
    margin-right: 6px;
  }
`;

interface Props {
  boardId: number;
  itemId: number;
  itemType: 'spaces' | 'programs';
  onMembersAdded: () => void;
}
const InviteBoardMembers: FC<Props> = ({ itemId, itemType, boardId, onMembersAdded }) => {
  const [usersArray, setUsersArray] = useState([]);
  const [inviteSend, setInviteSend] = useState(false);
  const [error, setError] = useState(false);
  const api = useApi();
  const { closeModal } = useModal();
  const { t } = useTranslation('common');
  const resetState = () => {
    setInviteSend(false);
    setError(false);
  };

  const formatOptionLabel = ({ label, logo_url }) => (
    <UserLabel row>
      <img src={logo_url} />
      <div>{label}</div>
    </UserLabel>
  );

  const appId = process.env.ALGOLIA_APP_ID;
  const token = process.env.ALGOLIA_TOKEN;

  const client = algoliasearch(appId, token);
  const index = client.initIndex('User'); // User
  let algoliaMembers = [];

  const fetchAlgolia = (resolve, value) => {
    index
      .search(value, {
        attributesToRetrieve: ['id', 'nickname', 'first_name', 'last_name', 'logo_url_sm'],
        hitsPerPage: 4,
      })
      .then((content) => {
        if (content) {
          algoliaMembers = content.hits;
        } else {
          algoliaMembers = [''];
        }
        algoliaMembers = algoliaMembers.map((user) => {
          return { value: user.id, label: `${user.first_name} ${user.last_name}`, logo_url: user.logo_url_sm };
        });
        resolve(algoliaMembers);
      });
  };

  const loadOptions = (inputValue) =>
    new Promise((resolve) => {
      fetchAlgolia(resolve, inputValue);
    });

  const handleChangeUser = (user) => {
    const newBoardMember = [];
    if (user) {
      newBoardMember.push(user.value);
      setUsersArray(newBoardMember);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    usersArray.map((userId) => {
      api
        .post(`/api/${itemType}/${itemId}/boards/${boardId}/users/${userId}`)
        .then((res) => {
          setInviteSend(true);
          setTimeout(() => {
            // close
            closeModal();
            resetState();
            onMembersAdded();
          }, 1400);
        })
        .catch(() => {
          setError(true);
          setTimeout(() => {
            setError(false);
          }, 5000);
        });
    });
  };

  const customStyles = {
    container: (provided) => ({
      ...provided,
      width: '100%',
    }),
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <div className="input-group">
          <AsyncSelect
            cacheOptions
            styles={customStyles}
            onChange={handleChangeUser}
            formatOptionLabel={formatOptionLabel}
            placeholder={t('member.invite.user.placeholder')}
            loadOptions={loadOptions}
            noOptionsMessage={() => null}
            components={{ DropdownIndicator: null, IndicatorSeparator: null }}
            isClearable
          />
        </div>
      </div>
      <div className="text-center btnZone">
        <button type="submit" className="btn btn-primary btn-block" disabled={!usersArray || usersArray.length === 0}>
          {inviteSend ? t('program.board.invite.added') : t('program.board.invite.add')}
        </button>
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          {t('err-')}
        </div>
      )}
    </form>
  );
};

export default InviteBoardMembers;
