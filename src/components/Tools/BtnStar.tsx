import Axios from 'axios';
import React, { FC, useEffect, useState } from 'react';
import { useApi } from 'contexts/apiContext';
import { ItemType } from 'types';
import useTranslation from 'next-translate/useTranslation';
import useUser from 'hooks/useUser';
import SpinLoader from './SpinLoader';
import { logEventToGA } from 'utils/analytics';
import { Star as StarFilled } from '@emotion-icons/boxicons-solid';
import { Star } from '@emotion-icons/boxicons-regular';
import ReactTooltip from 'react-tooltip';

interface Props {
  hasStarred: boolean | 'pending';
  itemId: number;
  itemType: ItemType;
  count?: number;
}

const BtnStar: FC<Props> = ({ hasStarred: propsHasStarred = false, itemId, itemType, count: countProp = 0 }) => {
  const [hasStarred, setHasStarred] = useState(propsHasStarred);
  const [action, setAction] = useState<'star' | 'unstar'>(!propsHasStarred ? 'star' : 'unstar');
  const [sending, setSending] = useState(false);
  const [count, setCount] = useState(countProp);
  const api = useApi();
  const { t } = useTranslation('common');
  const { user } = useUser();
  useEffect(() => {
    const axiosSource = Axios.CancelToken.source();
    // This will prevent to setHasStarred on unmounted BtnStar
    return () => axiosSource.cancel();
  }, [api, itemId, itemType, user]);

  useEffect(() => {
    setAction(!hasStarred ? 'star' : 'unstar');
  }, [hasStarred]);

  const changeStarredState = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      if (action === 'star') {
        api
          .put(`/api/${itemType}/${itemId}/save`)
          .then((res) => {
            const userId = res.config.headers.userId;
            setHasStarred(true);
            setCount(count + 1);
            setSending(false);
            // send event to google analytics
            logEventToGA(action, 'Button', `[${userId},${itemId},${itemType}]`, { userId, itemId, itemType });
          })
          .catch(() => setSending(false));
      } else {
        api
          .delete(`/api/${itemType}/${itemId}/save`)
          .then(() => {
            setHasStarred(false);
            setCount(count - 1);
            setSending(false);
          })
          .catch(() => setSending(false));
      }
    }
  };

  return (
    <span tw="relative z-0 inline-flex shadow-sm rounded-md">
      <button
        type="button"
        tw="relative inline-flex items-center p-2 rounded-l-md border border-solid border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-100 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
        onClick={changeStarredState}
        disabled={sending}
        data-tip={t(`general.${action}_explain`)}
        data-for={`star${itemId}`}
      >
        {sending ? (
          <SpinLoader />
        ) : hasStarred ? (
          <StarFilled size={22} title={t(`general.${action}`)} />
        ) : (
          <Star size={22} title={t(`general.${action}`)} />
        )}
        {t(`general.${action}`)}
      </button>
      <ReactTooltip
        id={`star${itemId}`}
        delayHide={300}
        effect="solid"
        role="tooltip"
        type="dark"
        place="top"
        className="forceTooltipBg"
      />
      <button
        type="button"
        tw="-ml-px relative inline-flex items-center px-2 py-2 cursor-default! rounded-r-md border border-solid border-gray-300 bg-white text-sm font-medium text-gray-700"
      >
        {count}
      </button>
    </span>
  );
};

export default BtnStar;
