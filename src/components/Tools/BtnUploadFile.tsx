import { Paperclip } from '@emotion-icons/fa-solid/';
import React, { Component, ReactNode } from 'react';
import withTranslation from 'next-translate/withTranslation';
import { ApiContext } from 'contexts/apiContext';
import { ItemType } from 'types';
import SpinLoader from './SpinLoader';

interface Props {
  fileTypes: string[];
  itemId: number;
  itemType: ItemType;
  maxSizeFile: number;
  singleFileOnly?: boolean;
  onChange: (result: any) => void;
  setListFiles: (listFiles: any) => void;
  refresh?: () => void;
  type: string;
  imageUrl: string;
  text: string | ReactNode;
  textUploading: string;
  uploadNow: boolean;
}
interface State {
  uploading: boolean;
}
class BtnUploadFile extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static get defaultProps() {
    return {
      fileTypes: [''],
      itemId: '',
      itemType: '',
      maxSizeFile: 31457280,
      onChange: (result) => console.warn('Oops, the function onChange is missing !', result),
      setListFiles: (listFiles) => console.warn('Oops, the function setListFiles is missing !', listFiles),
      type: '',
      imageUrl: '',
      uploadNow: false,
    };
  }

  handleChange(event) {
    event.preventDefault();
    const { itemType, uploadNow } = this.props;
    const { t } = this.props.i18n;
    if (event.target.files) {
      if (!this.validFilesSize(event.target.files)) {
        this.props.onChange({
          error: (
            <>
              {t('err-1001')}
              &nbsp;(max : {this.props.maxSizeFile / 1024 / 1024}
              Mo)
            </>
          ),
          url: '',
        });
      } else if (!uploadNow) {
        // if uploadNow is false, just show list of doc, without attaching them (when attaching files to a post for ex)
        this.props.setListFiles(event.target.files);
        if (itemType === 'needs') alert(t('need.docs.uploaded.onCreate'));
        // TODO: Do not use getElementById, maybe use ref.
        document.getElementById('btnUpload').value = '';
      } else {
        // else, attach file directly to object
        this.uploadFiles(event.target.files);
      }
    }
  }

  uploadFiles(files) {
    const { itemId, itemType, singleFileOnly, type } = this.props;
    const acceptMultipleFiles = !singleFileOnly || false;
    const { t } = this.props.i18n;
    if (!itemId || !itemType || !type || !files) {
      this.props.onChange({ error: t('err-') });
    } else {
      this.setState({ uploading: true }); // setting to true will render a loading wheel + disable the button

      const bodyFormData = new FormData();
      if (acceptMultipleFiles) {
        Array.from(files).forEach((file) => {
          bodyFormData.append(`${type}[]`, file);
        });
      } else {
        bodyFormData.append(type, files[0]);
      }

      const config = {
        headers: { 'Content-Type': 'multipart/form-data' },
      };
      const api = this.context;
      api
        .post(`/api/${itemType}/${itemId}/${type}`, bodyFormData, config)
        .then((res) => {
          if (res.status === 200) {
            this.setState({ uploading: false }); // set to false to show user upload has been successful
            if (itemType === 'needs') alert(t('need.docs.uploaded.onUpdate'));
            if (res.data.url) {
              this.props.onChange({ error: '', url: res.data.url }); // on success, show new uploading image in the front
            } else if (this.props.refresh !== undefined) {
              this.props.refresh();
            }
          } else {
            this.setState({ uploading: false }); // stop uploading if error
            this.props.onChange({
              error: t('err-'),
              url: '',
            });
          }
        })
        .catch((err) => {
          this.setState({ uploading: false }); // stop uploading if error
          console.error({ err });
          // this.props.onChange({ error: `${error.response.data.status} : ${error.response.data.error}`, url: "" });
        });
    }
  }

  removeImg() {
    const { itemId, itemType, type } = this.props;
    const randomNumber = Math.floor(Math.random() * 12) + 1; // with 12 = max, and 1 = min
    // each static media has a code at the end of url, so making an array of those codes for images 1 to 12
    // make logo_url randomly equals to one of our default image (1 to 12)
    const imgUrl =
      type === 'avatar' // set imgUrl depending if it's user profile or a banner
        ? `/images/default/users/default-user-${randomNumber}.png` // if type is avatar, reset image to a random default user avatar
        : ''; // else make it empty so it shows object default image
    this.setState({ uploading: true }); // setting to true will render a loading wheel + disable the button
    const api = this.context;
    api.delete(`/api/${itemType}/${itemId}/${type}`).then((res) => {
      if (res) {
        this.setState({ uploading: false }); // set to false to show user upload has been successful
        this.props.onChange({ error: '', url: imgUrl }); // set new imgUrl
      }
    });
    // .catch(error=>{});
  }

  validFilesSize(files) {
    const { maxSizeFile } = this.props;
    let nbFilesValid = 0;
    if (files) {
      for (let index = 0; index < files.length; index++) {
        if (files[index].size <= maxSizeFile) {
          nbFilesValid++;
        }
      }
    }
    return nbFilesValid === files.length; // return true or false depending on condition
  }

  // validFilesType(files) {
  //   const fileTypes = this.props.fileTypes;
  //   var nbFilesValid = 0;
  //   if(files){
  //     for(var index = 0; index < files.length; index++){
  //       for(var i = 0; i < fileTypes.length; i++) {
  //         if(files[index].type === fileTypes[i]) {
  //           nbFilesValid++;
  //           break;
  //         }
  //       }
  //     }
  //   }
  //   return (nbFilesValid === files.length) ? true : false // return true or false depending on condition
  // }

  render() {
    const { singleFileOnly, fileTypes, text } = this.props;
    const { uploading } = this.state;
    const { t } = this.props.i18n;
    const acceptMultipleFiles = !singleFileOnly || false;
    const textUpload = text || t('info-1000');
    const textUploading = t('info-1001');
    if (uploading) {
      // if "uploading" true, make button and input disabled (unclickable)
      // TODO: Don't use querySelector, maybe use ref or conditional rendering.
      document.querySelector('.upload-btn-wrapper .btn').disabled = true;
      document.querySelector('.upload-btn-wrapper input').disabled = true;
    }
    return (
      <>
        <div className="upload-btn-wrapper">
          <div className="btn" style={{ verticalAlign: 'sub' }}>
            {uploading ? ( // render differently depending on uploading state
              <>
                {<SpinLoader />}
                {textUploading}
              </>
            ) : (
              <>
                <Paperclip size={22} title="Upload file" />
                {textUpload}
              </>
            )}
          </div>
          <input
            id={!uploading ? 'btnUpload' : ''}
            type="file"
            accept={fileTypes.join()}
            multiple={acceptMultipleFiles}
            onChange={this.handleChange}
          />
        </div>
        {/* {(
          (itemType !== "users" && imageUrl !== "") // if it's not a user (so all other object) and has a stored image (not empty)
          || (itemType === "users" && !imageUrl.includes('/static/')) // OR if it's a user and he has a real stored image (not a default one)
        ) && // then show buttn to remove image
          <>
            <div onClick={() => this.removeImg()} id="btnRemove">{t('general.removeImage_btn')}</div>
          </>
        } */}
      </>
    );
  }
}
BtnUploadFile.contextType = ApiContext;
export default withTranslation(BtnUploadFile, 'common');
