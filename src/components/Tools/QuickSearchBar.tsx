import useTranslation from 'next-translate/useTranslation';
import Select from 'react-select';
import styled from 'utils/styled';
import A from '../primitives/A';
import Box from 'components/Box';

const UserLabel = styled(Box)`
  div {
    align-items: center;
    padding: 6px 10px;
    span {
      font-size: 15px;
      color: black !important;
    }
    img {
      width: 28px;
      height: 28px;
      border-radius: 50%;
      object-fit: cover;
      margin-right: 6px;
    }
  }
`;

const QuickSearchBar = ({ members }) => {
  const { t } = useTranslation('common');

  const customStyles = {
    option: (provided) => ({
      ...provided,
      padding: '0px',
    }),
  };

  const formatOptionLabel = ({ label, logo_url, value }) => (
    <UserLabel>
      <A href={`/user/${value}`} noStyle>
        <Box row>
          <img src={logo_url} />
          <span>{label}</span>
        </Box>
      </A>
    </UserLabel>
  );

  const membersList = members?.map((member) => {
    return {
      value: member.id,
      label: `${member.first_name} ${member.last_name}`,
      logo_url: member.logo_url_sm,
    };
  });

  return (
    <Select
      options={membersList}
      formatOptionLabel={formatOptionLabel}
      placeholder={t('algolia.search.placeholder')}
      styles={customStyles}
      controlShouldRenderValue={false}
      menuShouldScrollIntoView={true}
      hideSelectedOptions={false}
      components={{ DropdownIndicator: null, IndicatorSeparator: null }}
      noOptionsMessage={() => null}
    />
  );
};
export default QuickSearchBar;
