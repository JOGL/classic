import { useState, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import Button from '../primitives/Button';
import useUserData from 'hooks/useUserData';
import SpinLoader from './SpinLoader';
import { logEventToGA } from 'utils/analytics';

interface PropsModal {
  itemId: number;
  closeModal: () => void;
}
export const ContactForm: FC<PropsModal> = ({ itemId, closeModal }) => {
  const [object, setObject] = useState('');
  const [message, setMessage] = useState('');
  const [showConf, setShowConf] = useState(false);
  const [showError, setShowError] = useState(false);
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { userData } = useUserData();

  const { t } = useTranslation('common');
  const handleChange = (event) => {
    if (event.target.name === 'object') {
      setObject(event.target.value);
    } else if (event.target.name === 'message') {
      setMessage(event.target.value);
    } else {
      console.warn(`${event.target.name} is not handled`);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setSending(true);
    const param = {
      object,
      content: message,
    };
    api
      .post(`/api/users/${itemId}/send_email`, param)
      .then((res) => {
        setShowConf(true);
        setSending(false);
        setTimeout(() => {
          setShowConf(false);
          setMessage('');
          closeModal(); // close modal
        }, 2500);
        // send event to google analytics
        logEventToGA('contact user', 'Contact', `[${userData.id},${itemId}]`, { userId: userData.id, itemId });
      })
      .catch((errors) => {
        // if message limit is reached
        setShowError(true);
        setSending(false);
        setTimeout(() => {
          setShowError(false);
          setMessage('');
          closeModal(); // close modal
        }, 2500);
      });
  };
  return (
    <form className="contactForm" onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor={`recipient-name-${itemId}`} className="col-form-label">
          {t('user.contactModal.subject')}
        </label>
        <input
          type="text"
          className="form-control object"
          id={`recipient-name-${itemId}`}
          name="object"
          onChange={handleChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor={`message-text-${itemId}`} className="col-form-label">
          {t('user.contactModal.message')}
        </label>
        <textarea
          className="form-control message"
          style={{ minHeight: '200px' }}
          id={`message-text-${itemId}`}
          name="message"
          onChange={handleChange}
          required
        />
      </div>
      {showConf ? (
        // on submit success, replace submit button by this success message
        <Alert type="success" message={t('user.contactModal.success')} />
      ) : showError ? (
        // on submit error, replace submit button by this error message
        <Alert type="danger" message={t('user.contactModal.error')} />
      ) : (
        // show send button if not showing the response message
        <Button type="submit" disabled={sending || !message || !object}>
          {/* disable button on sending, or if object or message is empty */}
          {sending && <SpinLoader />}
          {t('user.contactModal.send')}
        </Button>
      )}
    </form>
  );
};
