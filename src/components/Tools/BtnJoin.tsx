import Axios from 'axios';
import React, { FC, useEffect, useState, ReactNode } from 'react';
import { useApi } from 'contexts/apiContext';
import { ItemType } from 'types';
import useTranslation from 'next-translate/useTranslation';
import useUser from 'hooks/useUser';
import SpinLoader from './SpinLoader';
import { logEventToGA } from 'utils/analytics';
import { Groups as GroupsFilled } from '@emotion-icons/material-rounded/';
import { Groups } from '@emotion-icons/material-outlined';

interface Props {
  joinState: boolean | 'pending';
  itemId: number;
  itemType: ItemType;
  textJoin?: string | ReactNode;
  textUnjoin?: string | ReactNode;
  isPrivate?: boolean;
  count: number;
  showMembersModal: (e: any) => void | Promise<boolean>;
}

const BtnJoin: FC<Props> = ({
  joinState: propsJoinState = false,
  itemId,
  itemType,
  textJoin,
  textUnjoin,
  isPrivate,
  count: countProp = 0,
  showMembersModal,
}) => {
  const [joinState, setJoinState] = useState(propsJoinState);
  const [action, setAction] = useState<'join' | 'leave'>(!propsJoinState ? 'join' : 'leave');
  const [count, setCount] = useState(countProp);
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');
  const { user } = useUser();
  useEffect(() => {
    const axiosSource = Axios.CancelToken.source();
    // This will prevent to setJoinState on unmounted BtnJoin
    return () => axiosSource.cancel();
  }, [api, itemId, itemType, user]);

  useEffect(() => {
    setAction(!joinState ? 'join' : 'leave');
  }, [joinState]);

  const changeStateJoin = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      api
        .put(`/api/${itemType}/${itemId}/${action}`)
        .then((res) => {
          const userId = res.config.headers.userId;
          // if object is private, set joinState as pending on "join" action, or false if "leave".
          // if not private, set to opposite current joinState
          setJoinState(isPrivate ? (action === 'join' ? 'pending' : false) : !joinState);
          setSending(false);
          setCount(action === 'join' ? (isPrivate ? count : count + 1) : count - 1);
          api.get(`/api/${itemType}/${itemId}/follow`).then((res) => {
            !res.data.has_followed && action === 'join' && api.put(`/api/${itemType}/${itemId}/follow`); // follow the object after having joined it and made sure user is not following object already
          });
          // send event to google analytics
          logEventToGA(action, 'Button', `[${userId},${itemId},${itemType}]`, { userId, itemId, itemType });
        })
        .catch((err) => {
          console.error(`Couldn't PUT ${itemType} with itemId=${itemId} and action=${action}`, err);
          setSending(false);
        });
    }
  };

  const textJoin2 = textJoin || t('general.join');
  const textUnjoin2 = textUnjoin || t('general.leave');
  const textPending = t('general.pending');
  const btnText = joinState === true ? textUnjoin2 : joinState === 'pending' ? textPending : textJoin2;
  return (
    <span tw="relative z-0 inline-flex shadow-sm rounded-md">
      <button
        type="button"
        tw="relative inline-flex items-center p-2 rounded-l-md border border-solid border-gray-300 bg-primary text-sm font-medium text-white hover:background-color[#2275b3]"
        onClick={changeStateJoin}
        disabled={sending}
      >
        {sending ? (
          <SpinLoader />
        ) : joinState ? (
          <GroupsFilled size={22} title={btnText} />
        ) : (
          <Groups size={22} title={btnText} />
        )}
        {btnText}
      </button>
      <button
        type="button"
        tw="-ml-px relative inline-flex items-center px-2 py-2 rounded-r-md border border-solid border-gray-300 bg-primary text-sm font-medium text-white hover:background-color[#2275b3]"
        onClick={showMembersModal}
      >
        {count}
      </button>
    </span>
  );
};

export default BtnJoin;
