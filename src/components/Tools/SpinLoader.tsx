import { FC } from 'react';
import { Spinner2 } from '@emotion-icons/icomoon';

interface Props {
  size?: string;
}
const SpinLoader: FC<Props> = ({ size = '18' }) => (
  <Spinner2 size={size} tw="animate-spin mr-2" title="Loading spinner" />
);
export default SpinLoader;
