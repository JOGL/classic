// TODO This component over-fetches way to much data just to get the clap count.
// TODO The component should useGet for caching.
// ! This component isn't self explanatory,
import { SignLanguage } from '@emotion-icons/fa-solid/SignLanguage';
import useTranslation from 'next-translate/useTranslation';
import React, { useEffect, useState } from 'react';
import { useApi } from 'contexts/apiContext';
import useUser from 'hooks/useUser';
import { ItemType } from 'types';
import { logEventToGA } from 'utils/analytics';
import SpinLoader from './SpinLoader';

interface Props {
  clapState: boolean;
  clapCount?: number;
  itemId: number;
  itemType: ItemType;
  refresh?: () => {};
}
const BtnClap = ({
  clapState: propsClapState = false,
  clapCount: propsClapCount = 0,
  itemId = undefined,
  itemType = undefined,
  refresh = undefined,
}: Props) => {
  const [clapState, setClapState] = useState(propsClapState);
  const [clapCount, setClapCount] = useState(propsClapCount);
  const [isSending, setIsSending] = useState(false);
  const [action, setAction] = useState<'clap' | 'unclap'>(propsClapState ? 'unclap' : 'clap');
  const api = useApi();
  const { user } = useUser();
  const { t } = useTranslation('common');

  useEffect(() => {
    setAction(clapState ? 'unclap' : 'clap');
  }, [clapState]);

  const changeStateClap = () => {
    // toggle display of comments unless we don't want the comments to show at all (in post card)
    if (itemId && itemType) {
      setIsSending(true);
      // send event to google analytics
      user &&
        logEventToGA(action, 'Button', `[${user.id},${itemId},${itemType}]`, { userId: user.id, itemId, itemType });
      const newClapCount = clapState ? clapCount - 1 : clapCount + 1;
      if (action === 'clap') {
        api
          .put(`/api/${itemType}/${itemId}/clap`)
          .then(() => {
            setClapCount(newClapCount);
            setClapState(true);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      } else {
        api
          .delete(`/api/${itemType}/${itemId}/clap`)
          .then(() => {
            setClapCount(newClapCount);
            setClapState(false);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      }
    }
  };
  return (
    <div
      onClick={changeStateClap}
      className={clapState ? 'btn-postcard btn text-primary' : 'btn-postcard btn text-dark'}
      role="button"
      tabIndex={0}
    >
      {isSending ? <SpinLoader /> : <SignLanguage size={20} title={t('post.clap')} />}
      &nbsp;
      {t('post.clap')}
    </div>
  );
};

export default BtnClap;
