import { FC, ReactNode } from 'react';
interface Props {
  message: string | ReactNode;
  type: string;
}
const Alert: FC<Props> = ({ message, type }) => (
  <div className={`w-100 alert alert-${type}`} role="alert">
    {message}
  </div>
);

export default Alert;
