import { FC } from 'react';
import HookCreate from 'components/Tools/Webhooks/Webhook/HookCreate';
import HookCard from 'components/Tools/Webhooks/Webhook/HookCard';
import Loading from 'components/Tools/Loading';
import useGet from 'hooks/useGet';
import { useApi } from 'contexts/apiContext';
// import "./Hooks.scss";

interface Props {
  itemId: number;
}
const WebHooks: FC<Props> = ({ itemId }) => {
  const { data: hooks, revalidate: revalidateHooks, mutate: mutateHooks } = useGet(`/api/projects/${itemId}/hooks`);

  const api = useApi();
  const onDelete = (hookId: number) => {
    api.delete(`api/projects/${itemId}/hooks/${hookId}`).then(() => {
      // const hooksWithoutDeleteOne = hooks.filter((hook) => hook.id !== hookId);
      // mutateHooks(hooksWithoutDeleteOne).then(() => {});
      revalidateHooks();
    });
  };
  return (
    <div className="hooks">
      <HookCreate projectId={itemId} refresh={revalidateHooks} />
      <div className="row">
        {hooks ? (
          hooks.map((hook, index) => (
            <div className="col-12 col-lg-6" key={index}>
              <HookCard
                hook={hook}
                hookProjectId={itemId}
                mode="display"
                refresh={revalidateHooks}
                onDelete={() => onDelete(hook.id)}
              />
            </div>
          ))
        ) : (
          <Loading />
        )}
      </div>
      {/* <div>
        {hooks.map((hook, key) => (
          <div key={key}>
            {hook.id} {hook.hook_params.channel}
          </div>
        ))}
      </div> */}
    </div>
  );
};
export default WebHooks;
