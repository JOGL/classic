import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import { UserContext } from 'contexts/UserProvider';
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
/** * Validators ** */
import FormValidator from 'components/Tools/Forms/FormValidator';
import HookFormRules from './HookFormRules';
/** * Images/Style ** */
// import "./HookForm.scss";

class HookForm extends Component {
  validator = new FormValidator(HookFormRules);

  static get defaultProps() {
    return {
      action: 'create',
      cancel: () => console.warn('Missing cancel function'),
      handleChange: () => console.warn('Missing handleChange function'),
      handleSubmit: () => console.warn('Missing handleSubmit function'),
      hook: undefined,
      refresh: () => console.warn('Missing refresh function'),
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      isCheckedPost: this.props.hook.trigger_post,
      isCheckedNeed: this.props.hook.trigger_need,
      isCheckedMember: this.props.hook.trigger_member,
    };
  }

  // handleChange(event){
  //   const key = event.target.id;
  //   const content = event.target.content;
  //   this.props.handleChange(key, content);
  // }

  // handleChange(key, content){
  //   this.props.handleChange(key, content);
  // }

  handleChange(key, content) {
    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation[`valid_${key}`] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleChangeToggle(event) {
    const key = event.target.id;
    let content;
    if (key === 'trigger_post') {
      content = this.state.isCheckedPost;
      this.setState({ isCheckedPost: !this.state.isCheckedPost });
    }
    if (key === 'trigger_need') {
      content = this.state.isCheckedNeed;
      this.setState({ isCheckedNeed: !this.state.isCheckedNeed });
    }
    if (key === 'trigger_member') {
      content = this.state.isCheckedMember;
      this.setState({ isCheckedMember: !this.state.isCheckedMember });
    }
    this.props.handleChange(key, content);
  }

  handleSubmit(event) {
    event.preventDefault();
    /* Validators control before submit */
    const validation = this.validator.validate(this.props.hook.hook_params);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          stateValidation[`valid_${key}`] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  // renderInvalid(valid_obj){
  //   if(valid_obj){
  //     if(valid_obj.message !== ""){
  //       return (
  //         <div className="invalid-feedback">
  //           {t(valid_obj.message)}
  //         </div>
  //       );
  //     }
  //   }
  //   return null;
  // }

  render() {
    const { t } = this.props.i18n;
    const { action, hook } = this.props;
    const { valid_hook_url, valid_username, valid_channel, isCheckedPost, isCheckedNeed, isCheckedMember } = this.state
      ? this.state
      : '';
    const submitBtnText = action === 'create' ? 'Create' : 'Update';
    return (
      <div className="hookForm">
        <div className="formContainer">
          <div className="triggers">
            <h6>{t('hook.triggerOn')}</h6>
            <div className="newPost">
              <input
                type="checkbox"
                name="checkbox"
                id="trigger_post"
                checked={isCheckedPost}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="newPost">{t('hook.trigger.post')}</label>
            </div>
            <div className="newNeed">
              <input
                type="checkbox"
                name="checkbox"
                id="trigger_need"
                checked={isCheckedNeed}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="newNeed">{t('hook.trigger.need')}</label>
            </div>
            <div className="newMember">
              <input
                type="checkbox"
                name="checkbox"
                id="trigger_member"
                checked={isCheckedMember}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="newMember">{t('hook.trigger.member')}</label>
            </div>
          </div>
          <div className="parameters">
            <h6>{t('hook.parameters.title')}</h6>
            <FormDefaultComponent
              id="hook_url"
              title={t('hook.parameters.slackHookUrl')}
              placeholder={t('hook.parameters.slackHookUrl_placeholder')}
              content={hook?.hook_params.hook_url}
              onChange={this.handleChange.bind(this)}
              isValid={valid_hook_url ? !valid_hook_url.isInvalid : undefined}
              mandatory
            />
            {/* {this.renderInvalid(valid_hook_url)} */}
            <FormDefaultComponent
              id="channel"
              title={t('hook.parameters.channel')}
              placeholder={t('hook.parameters.channel_placeholder')}
              content={hook.hook_params.channel}
              onChange={this.handleChange.bind(this)}
              isValid={valid_channel ? !valid_channel.isInvalid : undefined}
              mandatory
            />
            {/* {this.renderInvalid(valid_channel)} */}
            <FormDefaultComponent
              id="username"
              title={t('hook.parameters.username')}
              placeholder={t('hook.parameters.username_placeholder')}
              content={hook.hook_params.username}
              onChange={this.handleChange.bind(this)}
              isValid={valid_username ? !valid_username.isInvalid : undefined}
              mandatory
            />
            {/* {this.renderInvalid(valid_username)} */}
          </div>
        </div>
        <div className="actionBar">
          <button
            className="btn btn-primary"
            onClick={this.handleSubmit.bind(this)}
            type="submit"
            style={{ marginRight: '10px' }}
          >
            {t(`post.create.btn${submitBtnText}`)}
          </button>
          <button
            type="button"
            className="btn btn-outline-primary cancel"
            onClick={() => {
              this.props.cancel();
            }}
          >
            {t('entity.form.btnCancel')}
          </button>
        </div>
      </div>
    );
  }
}
export default withTranslation(HookForm, 'common');
HookForm.contextType = UserContext;
