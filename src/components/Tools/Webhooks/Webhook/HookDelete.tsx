import { Component } from 'react';
import { Delete } from '@emotion-icons/material/Delete';
import { ApiContext } from 'contexts/apiContext';

export default class HookDelete extends Component {
  static get defaultProps() {
    return {
      hook: undefined,
      refresh: () => console.warn('Missing refresh function'),
    };
  }

  deleteHook() {
    const { hook, refresh, hookProjectId } = this.props;
    if (hook === undefined) {
    } else {
      const api = this.context;
      api
        .delete(`api/projects/${hookProjectId}/hooks/${hook.id}`)
        .then(() => {
          refresh('hardReload'); // temp fix to force page reload, to render correct list of hooks
        })
        .catch(() => {
          console.error(`Couldn't DELETE project (${hookProjectId}) hook (${hook.id})`, err);
        });
    }
  }

  render() {
    return (
      <div onClick={this.deleteHook.bind(this)}>
        <Delete size={22} title="Delete hook" />
      </div>
    );
  }
}
HookDelete.contextType = ApiContext;
