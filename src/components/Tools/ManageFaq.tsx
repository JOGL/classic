import React, { FC, FormEvent, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Box from 'components/Box';
import Button from 'components/primitives/Button';
import H2 from 'components/primitives/H2';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import { Faq } from 'types';
import Card from 'components/Cards/Card';
import P from '../primitives/P';
import FormDefaultComponent from './Forms/FormDefaultComponent';
import FormWysiwygComponent from './Forms/FormWysiwygComponent';
import Loading from './Loading';

const ManageFaq = ({ itemType, itemId }) => {
  const { t } = useTranslation('common');
  const { data: faqList, mutate: faqListMutate } = useGet<{ documents: Faq[] }>(`/api/${itemType}/${itemId}/faq`);
  const [showFaqCreate, setShowFaqCreate] = useState(false);
  const addFaq = () => {
    setShowFaqCreate(!showFaqCreate);
  };
  return (
    <Box spaceY={3}>
      <H2>{t('faq.title')}</H2>
      <Button onClick={addFaq}>{t('general.add')}</Button>
      {showFaqCreate && (
        <FaqFormCard
          mode="create"
          itemType={itemType}
          itemId={itemId}
          onCreate={(newFaq: Faq) =>
            faqListMutate(
              {
                data: { documents: [...faqList.documents, newFaq] },
              },
              false
            )
          }
        />
      )}
      <Box spaceY={3} pt={2}>
        {!faqList?.documents ? (
          <Loading />
        ) : (
          faqList?.documents
            .sort(function (a, b) {
              return a.id - b.id; // sort by id (asc)
            })
            .map((value, i) => (
              <Box key={i} pt={3}>
                <P pl={2} mb={0} style={{ fontSize: '2rem' }}>
                  {i + 1}
                </P>
                <FaqFormCard
                  value={value}
                  itemType={itemType}
                  itemId={itemId}
                  mode="edit"
                  onUpdate={(updatedFaq: Faq) => {
                    faqListMutate(
                      {
                        data: {
                          documents: faqList.documents.map((faq) => {
                            if (faq.id === updatedFaq.id) return updatedFaq;
                            return faq;
                          }),
                        },
                      },
                      false
                    );
                  }}
                  onDelete={(faqId: number) => {
                    faqListMutate(
                      {
                        // removes the faq which has the same id as faqId
                        data: { documents: faqList.documents.filter((faq) => faq.id !== faqId) },
                      },
                      false
                    );
                  }}
                />
              </Box>
            ))
        )}
      </Box>
    </Box>
  );
};
interface IFaqFormCard {
  value?: Faq;
  mode: 'create' | 'edit';
  itemType: string;
  itemId: number;
  onCreate?: (newFaq: Faq) => void;
  onUpdate?: (updatedFaq: Faq) => void;
  onDelete?: (faqId: number) => void;
}
const FaqFormCard: FC<IFaqFormCard> = ({
  value = { title: '', content: '', id: undefined },
  mode,
  itemType,
  itemId,
  onCreate,
  onUpdate,
  onDelete,
}) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const [showSubmitBtn, setShowSubmitBtn] = useState(false);
  const [faq, setFaq] = useState<{ faq: Faq }>({
    faq: {
      id: value.id,
      title: value.title,
      content: value.content,
    },
  });
  // handle every changes to the faq
  const handleChange: (key: number, content: string) => void = (key, content) => {
    setFaq((prevFaq) => ({ faq: { ...prevFaq.faq, [key]: content } }));
    setShowSubmitBtn(content !== faq.faq.title || content !== faq.faq.content);
  };
  // handle when submitting (update, or create), an faq
  const handleSubmit: (event: FormEvent<HTMLFormElement>) => void = (e) => {
    e.preventDefault();
    if (mode === 'edit') {
      api.patch(`/api/${itemType}/${itemId}/faq/${faq.faq.id}`, faq).then((res) => {
        onUpdate(faq.faq); // call onUpdate function
        setShowSubmitBtn(false); // hide update button
      });
    }
    if (mode === 'create') {
      api.post(`/api/${itemType}/${itemId}/faq`, faq).then((res) => {
        onCreate(faq.faq); // call onCreate function
        setFaq({ faq: { title: '', content: '', id: undefined } }); // reset faq fields
      });
    }
  };
  // handle when deleting on faq
  const handleDelete = () => {
    api.delete(`/api/${itemType}/${itemId}/faq/${faq.faq.id}`).then((res) => {
      onDelete(faq.faq.id); // call onDelete function
    });
  };
  const tranlationId = mode === 'create' ? 'entity.form.btnCreate' : 'entity.form.btnUpdate';
  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <Box flexDirection={['column', undefined, 'row']} justifyContent="space-between">
          <Box spaceY={4} width={'100%'} pr={[0, undefined, 5]}>
            <Box>
              <FormDefaultComponent
                id="title"
                content={faq.faq.title}
                title={t('faq.question')}
                placeholder={t('faq.question')}
                onChange={handleChange}
              />
            </Box>
            <Box>
              <FormWysiwygComponent
                id="content"
                content={faq.faq.content}
                title={t('faq.answer')}
                placeholder={t('faq.answer')}
                onChange={handleChange}
                show
              />
            </Box>
          </Box>
          <Box spaceY={2} justifyContent="center" alignItems="flex-end">
            {(showSubmitBtn || mode === 'create') && (
              <Button type="submit" width="100%">
                {t(tranlationId)}
              </Button>
            )}
            {mode !== 'create' && (
              <Button type="button" btnType="danger" onClick={handleDelete} width="100%">
                {t('feed.object.delete')}
              </Button>
            )}
          </Box>
        </Box>
      </form>
    </Card>
  );
};

export default ManageFaq;
