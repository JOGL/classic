import { useState, FC } from 'react';
import mimetype2fa from './mimetypes';
import { useApi } from 'contexts/apiContext';
import ReactTooltip from 'react-tooltip';
import useTranslation from 'next-translate/useTranslation';

interface Props {
  document: {
    content_type: string;
    filename: string;
    url: string;
    id: number;
  };
  isEditing: boolean;
  postId?: number;
  refresh: () => void;
}
const DocumentCardFeed: FC<Props> = ({
  document: documentProp = {
    content_type: 'image/jpeg',
    filename: 'MyImage.jpg',
    url: 'https://something.com/image.jpg',
    id: 1,
  },
  isEditing,
  postId,
  refresh,
}) => {
  const [document] = useState(documentProp);
  const api = useApi();
  const { t } = useTranslation('common');

  const deleteDocument = () => {
    api
      .delete(`api/posts/${postId}/documents/${document.id}`)
      .then(() => {
        refresh(); // this function comes from PostDisplay, it revalidates the post after deleting the doc
      })
      .catch((error) => {
        console.error({ error });
      });
  };

  return (
    <div className="documentCardFeed col-12 col-sm-6">
      <div tw="inline-flex">
        {mimetype2fa(document.content_type)}
        {document.content_type === 'image/jpeg' ? (
          // if document is a jpg image, make it hoverable with the image appearing in a popup
          <div className="hover_img">
            <a href={document.url} target="_blank" rel="noopener noreferrer">
              {document.filename}
              <span>
                <img src={document.url} alt={document.filename} width="280" />
              </span>
            </a>
          </div>
        ) : (
          // else just display the document
          <a href={document.url} target="_blank" rel="noopener noreferrer">
            {document.filename}
          </a>
        )}
        {isEditing && (
          <>
            <button
              type="button"
              className="close"
              aria-label="Close"
              data-tip={t('general.remove')}
              data-for="documentFeed_delete"
              onClick={deleteDocument}
              onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && deleteDocument()}
              // show/hide tooltip on element focus/blur
              onFocus={(e) => ReactTooltip.show(e.target)}
              onBlur={(e) => ReactTooltip.hide(e.target)}
              style={{ marginLeft: '10px' }}
            >
              <span aria-hidden="true">&times;</span>
            </button>
            <ReactTooltip id="documentFeed_delete" effect="solid" />
          </>
        )}
      </div>
    </div>
  );
};

export default DocumentCardFeed;
