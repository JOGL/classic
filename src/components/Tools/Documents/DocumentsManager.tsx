import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import Box from 'components/Box';
import BtnUploadFile from 'components/Tools/BtnUploadFile';
import DocumentsList from 'components/Tools/Documents/DocumentsList';
import Loading from 'components/Tools/Loading';
import { useApi } from 'contexts/apiContext';
import { ItemType } from 'types';
import { Document } from 'types';

interface Props {
  itemId: number;
  itemType: ItemType;
  isAdmin: boolean;
  documents: Document[];
  showTitle?: boolean;
}

const DocumentsManager: FC<Props> = ({ itemId, itemType, documents, isAdmin, showTitle }) => {
  const [loading, setLoading] = useState(false);
  const [documentsList, setDocumentsList] = useState(documents);
  const api = useApi();
  const { t } = useTranslation('common');

  const refresh = () => {
    setLoading(true);
    api
      .get(`/api/${itemType}/${itemId}`)
      .then((res) => {
        res.data && setDocumentsList(res.data.documents);
        setLoading(false);
      })
      .catch((err) => {
        console.error(`Couldn't GET ${itemType} with itemId=${itemId}`, err);
        setLoading(false);
      });
  };

  return (
    <>
      {documentsList.length !== 0 && showTitle && <Box fontWeight="bold">{t('entity.tab.documents')}</Box>}
      <div className="documentsManager">
        {isAdmin && (
          <BtnUploadFile
            itemId={itemId}
            itemType={itemType}
            refresh={refresh}
            text={t('info-1003')}
            type="documents"
            uploadNow
          />
        )}
        <Loading active={loading}>
          <DocumentsList
            documents={documentsList}
            itemId={itemId}
            isAdmin={isAdmin}
            itemType={itemType}
            refresh={refresh}
            cardType="cards"
          />
        </Loading>
      </div>
    </>
  );
};

export default DocumentsManager;
