import React from 'react';
import useTranslation from 'next-translate/useTranslation';
// import "./InfoInterestsComponent.scss";
import $ from 'jquery';
import TitleInfo from 'components/Tools/TitleInfo';
import Box from 'components/Box';
import ReactTooltip from 'react-tooltip';
import { defaultSdgsInterests } from 'utils/utils';

export default React.memo(function InfoInterestsComponent({ title = 'Title', content = [] }) {
  const { t } = useTranslation('common');
  const showMore = (items) => {
    $('.less').show();
    $(`.interest:lt(${items})`).show(300);
    $('.more').hide();
  };

  const showLess = () => {
    $('.interest').not(':lt(4)').hide(300);
    $('.more').show();
    $('.less').hide();
  };
  // get sdgs infos array from external function
  const defaultInterests = defaultSdgsInterests(t);

  let interestClass = '';
  if (content.length > 0) {
    return (
      <div className="infoInterests">
        {title && <TitleInfo title={title} />}
        <div className="interests">
          {content
            .sort((a, b) => a - b) // sort sdgs by asc order
            .map((interestId, index) => {
              interestClass = index < 4 ? 'interest' : 'interest toggle';
              const interest = defaultInterests.find((el) => el.value === interestId);
              const largeLabel = interest.label.length > 29;
              const superLargeLabel = interest.label.length > 37;

              return (
                <div className={interestClass} key={index}>
                  <Box
                    bg={interest.color}
                    alignItems="center"
                    position="absolute"
                    height="full"
                    lineHeight="14px"
                    pb={2}
                    data-tip={t(`sdg-description-${interestId}`)}
                    data-for={`sdg-${interestId}`}
                  >
                    <Box
                      row
                      color="white"
                      pt={'.65rem'}
                      px={2}
                      pb={0}
                      height={superLargeLabel ? '60px' : '50px'}
                      overflow="hide"
                      width="100%"
                      fontFamily="Bebas Neue"
                    >
                      <span
                        style={{
                          fontSize: interest.value < 10 ? '1.7rem' : '1.6rem',
                          paddingRight: largeLabel ? '5px' : '10px',
                          paddingTop: '1px',
                          fontWeight: 'bold',
                        }}
                      >
                        {interestId}
                      </span>
                      <span
                        style={{
                          fontSize: superLargeLabel
                            ? 'calc(100% - 4px)'
                            : interest.label.length > 18
                            ? 'calc(100% - 3px)'
                            : 'calc(100% - 1px)',
                          lineHeight: superLargeLabel ? '10px' : largeLabel ? '12px' : '13px',
                        }}
                      >
                        {interest.label}
                      </span>
                    </Box>
                    <img
                      style={{
                        width: '100%',
                        height: superLargeLabel ? '45px' : largeLabel ? '50px' : '52px',
                        objectFit: 'contain',
                      }}
                      src={`/images/interests/Interest-${interestId}-icon.png`}
                    />
                  </Box>
                  <ReactTooltip id={`sdg-${interestId}`} effect="solid" type="info" className="solid-tooltip" />
                </div>
              );
            })}
          {content.length > 4 && (
            <>
              <Box as="button" className="more" onClick={() => showMore(content.length)}>
                {t('general.showmore')}
              </Box>
              <Box as="button" className="less" onClick={showLess}>
                {t('general.showless')}
              </Box>
            </>
          )}
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
});
