import $ from 'jquery';
import { linkifyQuill } from 'utils/utils';
import 'react-quill/dist/quill.snow.css';
import { useEffect } from 'react';
// import "./InfoHtmlComponent.scss";

export default function InfoHtmlComponent({ content }) {
  useEffect(() => {
    $('.infoHtml a[href]').attr('target', '_blank');
  }, []);

  if (content) {
    const newContent = linkifyQuill(content);
    return (
      <div className="infoHtml">
        <div className="content">
          <div dangerouslySetInnerHTML={{ __html: newContent }} className="rendered-quill-content ql-editor" />
        </div>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
