import tw, { styled } from 'twin.macro';

export default function InfoMaxCharComponent({ content = '', maxChar = 340 }) {
  const actualSize = content?.length;
  if (content?.length > maxChar * 0.8)
    return (
      <StyledInput type={actualSize > maxChar ? 'danger' : actualSize > maxChar * 0.8 ? 'warning' : 'default'}>
        {actualSize}/{maxChar} max.
      </StyledInput>
    );
  return null;
}
const StyledInput = styled.div`
  text-align: right;
  ${({ type }) => (type === 'warning' ? tw`text-yellow-500` : type === 'danger' ? tw`text-danger` : tw`text-gray-500`)}
`;
