import { FC } from 'react';
import Box from '../Box';
import { QuestionCircle } from '@emotion-icons/fa-solid/QuestionCircle';
import ReactTooltip from 'react-tooltip';
import tw from 'twin.macro';

interface Props {
  title: string;
  mandatory?: boolean;
  tooltipMessage?: string;
  isFormSubpartTitle?: boolean;
}

const TitleInfo: FC<Props> = ({ title, mandatory, tooltipMessage, isFormSubpartTitle = false }) => (
  <Box row alignItems="center" className="titleInfo">
    <span
      css={[
        tw`underline font-bold sm:no-underline`,
        isFormSubpartTitle && tw`text-lg color[#2987cd] no-underline mb-1`,
      ]}
    >
      {title}
    </span>
    {mandatory && <div tw="text-red-500 contents">&nbsp;*</div>}
    {tooltipMessage && (
      <Box pl={2}>
        <QuestionCircle
          size={16}
          title="More information"
          data-tip={tooltipMessage}
          data-for="bannerTooltip"
          tabIndex={0}
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
        />
        <ReactTooltip
          id="bannerTooltip"
          effect="solid"
          type="dark"
          className="solid-tooltip"
          overridePosition={({ left, top }, _e, _t, node) => {
            return {
              top,
              left: typeof node === 'string' ? left : Math.max(left, 20),
            };
          }}
        />
      </Box>
    )}
  </Box>
);

export default TitleInfo;
