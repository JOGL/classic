import React, { useState, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import {
  InstantSearch,
  RefinementList,
  SortBy,
  Pagination,
  ClearRefinements,
  ToggleRefinement,
  Hits,
  HitsPerPage,
  Panel,
  SearchBox,
} from 'react-instantsearch-dom';
import algoliasearch from 'algoliasearch/lite';
import UserCard from '../User/UserCard';
import ProjectCard from 'components/Project/ProjectCard';
import NeedCard from 'components/Need/NeedCard';
// import CommunityCard from 'components/Community/CommunityCard';
import ChallengeCard from 'components/Challenge/ChallengeCard';
import SpaceCard from 'components/Space/SpaceCard';

import { ClearFiltersMobile, NoSearchResults, ResultsNumberMobile, SaveFiltersMobile } from 'components/Search/widgets';
import { defaultSdgsInterests, kFormatter } from 'utils/utils';
import { styled } from 'twin.macro';
import ProgramCard from '../Program/ProgramCard';

import { useRouter } from 'next/router';

import 'intro.js/introjs.css';
import { startSearchIntro } from 'utils/onboarding/index';

const ToggleContainer = styled.div`
  .ais-ToggleRefinement-checkbox:checked::before {
    content: '${({ t }) => t('general.yes')}';
  }
  .ais-ToggleRefinement-checkbox::before {
    content: '${({ t }) => t('general.no')}';
  }
`;

// import "./Search.scss";
const searchClient = algoliasearch(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_TOKEN);

const Search = ({ index, refinements = [], sortByItems = [], onSearchStateChange, searchState = {} }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const isOnboarding = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isOnboarding'));
  const isAnIndividual = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isIndividual'));
  // const isAnOrganization = typeof window !== "undefined" && JSON.parse(localStorage.getItem('isOrganization'));
  const isDesktop = typeof window !== 'undefined' && JSON.parse(localStorage.getItem('isDesktop'));

  const openFilters = () => {
    document.querySelector('.searchPageAll').classList.add('filtering');
    window.scrollTo(0, 0);
  };

  const closeFilters = () => {
    document.querySelector('.searchPageAll').classList.remove('filtering');
  };

  // change nb of hits per page depending of index (grid)
  const nBOfHitsPerPage = [18, 36, 72];

  // [Onboarding][4] - Retaking onboarding flow on search page
  useEffect(() => {
    if (isOnboarding && isAnIndividual) {
      setTimeout(() => {
        startSearchIntro(isAnIndividual, t, router, isDesktop);
      }, 2000);
    }
  }, [isOnboarding, isAnIndividual]);

  return (
    <InstantSearch
      indexName={index}
      searchClient={searchClient}
      searchState={searchState}
      onSearchStateChange={onSearchStateChange}
    >
      <div className="d-flex searchContainer">
        <div className="container-wrapper">
          <section className="container-filters">
            <div className="container-header">
              <p className="filters">{t('algolia.filters.name')}</p>
              <div className="clear-filters" data-layout="desktop">
                <ClearRefinements
                  translations={{
                    reset: (
                      <>
                        <svg xmlns="http://www.w3.org/2000/svg" width="11" height="11" viewBox="0 0 11 11">
                          <g fill="none" fillRule="evenodd" opacity=".4">
                            <path d="M0 0h11v11H0z" />
                            <path
                              fill="#000"
                              fillRule="nonzero"
                              d="M8.26 2.75a3.896 3.896 0 1 0 1.102 3.262l.007-.056a.49.49 0 0 1 .485-.456c.253 0 .451.206.437.457 0 0 .012-.109-.006.061a4.813 4.813 0 1 1-1.348-3.887v-.987a.458.458 0 1 1 .917.002v2.062a.459.459 0 0 1-.459.459H7.334a.458.458 0 1 1-.002-.917h.928z"
                            />
                          </g>
                        </svg>
                        {t('algolia.filters.clear')}
                      </>
                    ),
                  }}
                />
              </div>
              <div className="clear-filters">
                <ResultsNumberMobile />
              </div>
            </div>
            {refinements && (
              <div className="container-body">
                <RefinementLists index={index} t={t} refinements={refinements} />
              </div>
            )}
          </section>

          <div className="container-filters-footer" data-layout="mobile">
            <div className="container-filters-footer-button-wrapper">
              <ClearFiltersMobile closeFilters={closeFilters} />
            </div>

            <div className="container-filters-footer-button-wrapper">
              <SaveFiltersMobile onClick={closeFilters} />
            </div>
          </div>
        </div>
        <div className="resultsContainer" id="search-results">
          <header className="container-header container-options" id="search-header">
            <SearchBox
              translations={{
                placeholder: t('algolia.search.placeholder'),
              }}
            />
            {sortByItems && <CustomSortBy sortByItems={sortByItems} index={index} />}
            <HitsPerPage
              className="container-option"
              items={nBOfHitsPerPage.map((value, i) => ({
                label: `${value} ${t('algolia.hitsPerPage')}`,
                value: value,
              }))}
              defaultRefinement={nBOfHitsPerPage[0]}
            />
          </header>
          <CustomHits index={index} />

          <NoSearchResults />
          <footer className="container-footer">
            <Pagination
              padding={2}
              showFirst={true}
              showLast={true}
              // force scroll to top of the page each time you change page
              onClick={typeof window !== 'undefined' && window.scrollTo(0, 0)}
              translations={{
                previous: (
                  <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                    <g
                      fill="none"
                      fillRule="evenodd"
                      stroke="#000"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.143"
                    >
                      <path d="M9 5H1M5 9L1 5l4-4" />
                    </g>
                  </svg>
                ),
                next: (
                  <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                    <g
                      fill="none"
                      fillRule="evenodd"
                      stroke="#000"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.143"
                    >
                      <path d="M1 5h8M5 9l4-4-4-4" />
                    </g>
                  </svg>
                ),
              }}
            />
          </footer>
          {/* {index !== 'Challenge' && ( */}
          <aside data-layout="mobile">
            <button className="filters-button" data-action="open-overlay" onClick={openFilters} type="button">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 14">
                <path
                  d="M15 1H1l5.6 6.3v4.37L9.4 13V7.3z"
                  stroke="#fff"
                  strokeWidth="1.29"
                  fill="none"
                  fillRule="evenodd"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
              {t('algolia.filters.name')}
            </button>
          </aside>
          {/* )} */}
        </div>
      </div>
    </InstantSearch>
  );
};

export default Search;

// custom component to show a single SDG filter
const singleSdg = (sdg) => (
  <div style={{ background: sdg.color }} tw="flex text-white px-2 py-2 rounded-md w-20 min-height[76px]">
    <span style={{ lineHeight: sdg.label.length > 20 ? '.7rem' : '.85rem' }}>
      <span style={{ fontSize: '1rem' }}>{sdg.value} - </span>
      <span
        style={{
          fontSize: sdg.label.length > 20 ? '.75rem' : '.85rem',
          wordBreak: 'break-word',
        }}
      >
        {sdg.label}
      </span>
    </span>
  </div>
);

const transformItems = (items, attribute, t, index) =>
  // map through items, and transform them under certain conditions
  items
    .filter(({ label }) => label !== 'draft') // don't show draft status filter
    .map((item, key) => ({
      ...item,
      count: kFormatter(item.count), // format thousands to K form (1.000 to 1K)
      label:
        attribute === 'interests' // if attribute is interests/sdg
          ? // show sdg custom component, with props coming from external "defaultSdgsInterests" function returning all sdgs info
            singleSdg(defaultSdgsInterests(t).find((element) => element.value == item.label))
          : attribute === 'status' && index === 'Challenge' // if attribute is a status (from challenge), translate status to its full name
          ? t(`challenge.info.status_${item.label}`)
          : attribute === 'status' && (index !== 'Member' || index !== 'Challenge' || index !== 'Community') // if attribute is a status (), translate status to its full name
          ? t(`entity.info.status.${item.label}`)
          : attribute === 'maturity' && index === 'Project' // if attribute is project's maturity, translate status to its full name
          ? t(`project.maturity.${item.label}`)
          : attribute === 'space_type' && index === 'Space' // if attribute is space_type, translate status to its full name
          ? t(`space.type.${item.label}`)
          : // else simply display item label
            item.label,
    }));

const CustomSortBy = ({ sortByItems, index: defaultIndex }) => {
  // defaultRefinement is first index of sortByItems array (from [active-index.js])
  const defaultRefinement = sortByItems[0].index;
  const items = sortByItems.map(({ index = defaultIndex, label = 'Undefined' }) => ({ value: index, label }));
  return <SortBy className="container-option" defaultRefinement={defaultRefinement} items={items} />;
};

const translateAttributeTitle = (attribute) => {
  switch (attribute) {
    case 'ressources':
      return 'user.profile.resources';
    case 'skills':
      return 'user.profile.skills';
    case 'interests':
      return 'algolia.interests.title';
    case 'project.title':
      return 'user.profile.tab.projects';
    case 'programs.title':
      return 'general.programs';
    case 'program.title':
      return 'general.programs';
    case 'challenges.title':
      return 'general.challenges';
    case 'status':
      return 'entity.info.status.title';
    case 'maturity':
      return 'project.maturity.title';
    case 'reviews_count':
      return 'algolia.projects.is_reviewed';
    case 'space_type':
      return 'space.form.space_type';
    default:
      return 'need.isUrgentShort';
  }
};

const translateAttributePlaceholder = (attribute) => {
  switch (attribute) {
    case 'ressources':
      return 'algolia.resources.placeholder';
    case 'interests':
      return 'algolia.interests.placeholder';
    case 'challenges.title':
      return 'algolia.challenges.placeholder';
    case 'project.title':
      return 'algolia.projects.placeholder';
    default:
      return 'algolia.skills.placeholder';
  }
};

const CustomRefinementList = RefinementList;

const RefinementLists = ({ refinements, index, t }) => {
  const refinementList = refinements.map(({ attribute, type, showMore = true, limit = 10, searchable = true }, key) => {
    const titleTranslateId = translateAttributeTitle(attribute);
    const placeholderTranslateId = translateAttributePlaceholder(attribute);

    return (
      (index !== 'Project' || (index === 'Project' && attribute !== 'ressources')) && (
        <Panel header={t(titleTranslateId)} className={attribute} key={key}>
          {type !== 'toggle' && (
            <CustomRefinementList
              attribute={attribute}
              showMore={showMore}
              limit={limit}
              searchable={searchable}
              transformItems={(items) => transformItems(items, attribute, t, index)}
              translations={{
                placeholder: t(placeholderTranslateId),
                noResults: t('algolia.noResult'),
                showMore(expanded) {
                  return expanded ? t('general.showless') : t('general.showmore');
                },
              }}
            />
          )}
          {/* replace value to `true` when we'll use is_reviewed instead of checking if reviews_count is equal to 1 */}
          {type === 'toggle' && (
            <ToggleContainer t={t}>
              <ToggleRefinement attribute={attribute} value={1} label={t('algolia.projects.is_reviewed')} />
            </ToggleContainer>
          )}
        </Panel>
      )
    );
  });
  return refinementList;
};

const CustomHits = ({ index }) => {
  const HitComponent = React.memo(({ hit }) => {
    switch (index) {
      case 'User':
        return (
          <UserCard
            id={hit.id}
            key={hit.id}
            firstName={hit.first_name}
            lastName={hit.last_name}
            nickName={hit.nickname}
            shortBio={hit.short_bio}
            logoUrl={hit.logo_url}
            affiliation={hit.affiliation}
            projectsCount={hit.stats?.projects_count}
            followersCount={hit.stats?.followers_count}
            spacesCount={hit.stats?.spaces_count}
            skills={hit.skills}
            source="algolia"
          />
        );
      case 'Project':
        return (
          <ProjectCard
            id={hit.id}
            key={hit.id}
            title={hit.title}
            shortTitle={hit.short_title}
            short_description={hit.short_description}
            members_count={hit.members_count}
            followersCount={hit.followers_count}
            postsCount={hit.posts_count}
            needs_count={hit.needs_count}
            has_saved={hit.has_saved}
            skills={hit.skills}
            reviewsCount={hit.reviews_count}
            banner_url={hit.banner_url || '/images/default/default-project.jpg'}
            source="algolia"
          />
        );
      case 'Need':
        return (
          <NeedCard
            title={hit.title}
            project={hit.project}
            skills={hit.skills}
            resources={hit.ressources}
            hasSaved={hit.has_saved}
            id={hit.id}
            key={hit.id}
            postsCount={hit.posts_count}
            publishedDate={hit.created_at}
            membersCount={hit.members_count}
            dueDate={hit.end_date}
            status={hit.status}
            source="algolia"
          />
        );
      // case 'Community':
      //   return (
      //     <CommunityCard
      //       id={hit.id}
      //       key={hit.id}
      //       title={hit.title}
      //       shortTitle={hit.short_title}
      //       short_description={hit.short_description}
      //       clapsCount={hit.claps_count}
      //       members_count={hit.members_count}
      //       has_saved={hit.has_saved}
      //       skills={hit.skills}
      //       banner_url={hit.banner_url || '/images/default/default-group.jpg'}
      //       source="algolia"
      //     />
      //   );
      case 'Challenge':
        return (
          <ChallengeCard
            id={hit.id}
            key={hit.id}
            short_title={hit.short_title}
            title={hit.title}
            title_fr={hit.title_fr}
            short_description={hit.short_description}
            short_description_fr={hit.short_description_fr}
            membersCount={hit.members_count}
            needsCount={hit.needs_count}
            has_saved={hit.has_saved}
            clapsCount={hit.claps_count}
            customType={hit.custom_type || ''}
            status={hit.status}
            program={hit.program}
            space={hit.space}
            projectsCount={hit.projects_count}
            banner_url={hit.banner_url || '/images/default/default-challenge.jpg'}
            source="algolia"
          />
        );
      case 'Space':
        return (
          <SpaceCard
            id={hit.id}
            key={hit.id}
            short_title={hit.short_title}
            title={hit.title}
            title_fr={hit.title_fr}
            short_description={hit.short_description}
            short_description_fr={hit.short_description_fr}
            membersCount={hit.members_count}
            needsCount={hit.needs_count}
            has_saved={hit.has_saved}
            clapsCount={hit.claps_count}
            projectsCount={hit.projects_count}
            spaceType={hit.space_type}
            banner_url={hit.banner_url || '/images/default/default-space.jpg'}
            source="algolia"
          />
        );
      case 'Program':
        return (
          <ProgramCard
            id={hit.id}
            key={hit.id}
            short_title={hit.short_title}
            title={hit.title}
            title_fr={hit.title_fr}
            short_description={hit.short_description}
            short_description_fr={hit.short_description_fr}
            membersCount={hit.members_count}
            needsCount={hit.needs_count}
            has_saved={hit.has_saved}
            projectsCount={hit.projects_count}
            banner_url={hit.banner_url || '/images/default/default-program.jpg'}
            source="algolia"
          />
        );
      default:
        return (
          <UserCard
            id={hit.id}
            key={hit.id}
            firstName={hit.first_name}
            lastName={hit.last_name}
            nickName={hit.nickname}
            shortBio={hit.short_bio}
            logoUrl={hit.logo_url}
            affiliation={hit.affiliation}
            projectsCount={hit.stats?.projects_count}
            followersCount={hit.stats?.followers_count}
            spacesCount={hit.stats?.spaces_count}
            source="algolia"
          />
        );
    }
  });

  const memoizedHitFunction = React.useCallback((props) => <HitComponent {...props} />, []);

  return (
    <div className={`${index}-hits`}>
      <Hits hitComponent={memoizedHitFunction} />
    </div>
  );
};
