import useTranslation from 'next-translate/useTranslation';
import { connectCurrentRefinements } from 'react-instantsearch-dom';

const ClearFiltersMobile = ({ items, refine }) => {
  const { t } = useTranslation('common');

  function closeFilters() {
    refine(items);
    document.body.classList.remove('filtering');
    // containerRef.current.scrollIntoView();
    // force click on "see results" button
    const seeResultsBtn: HTMLElement = document.querySelector(
      '.container-filters-footer .button-primary'
    ) as HTMLElement;
    seeResultsBtn.click();
  }

  return (
    <div className="ais-ClearRefinements">
      <button className="ais-ClearRefinements-button" onClick={closeFilters}>
        {t('algolia.filters.clear2')}
      </button>
    </div>
  );
};

export default connectCurrentRefinements(ClearFiltersMobile);
