import useTranslation from 'next-translate/useTranslation';
import { connectStats } from 'react-instantsearch-dom';
import { formatNumber } from 'utils/utils';

const SaveFiltersMobile = ({ nbHits, onClick }) => {
  const { t } = useTranslation('common');

  return (
    <button className="button button-primary" onClick={onClick}>
      {t('algolia.seeResults', { number: formatNumber(nbHits) })}
    </button>
  );
};

export default connectStats(SaveFiltersMobile);
