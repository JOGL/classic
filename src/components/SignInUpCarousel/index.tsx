import useTranslation from 'next-translate/useTranslation';
import Carousel from 'components/Carousel';

const SignInUpCarousel = () => {
  const { t } = useTranslation('common');
  const slides = [
    {
      imgLink: '/images/carousel-slides/Map.png',
      langTitle: 'signInUpSlides.title1',
      langText: 'signInUpSlides.text1',
    },
    {
      imgLink: '/images/carousel-slides/Create.png',
      langTitle: 'signInUpSlides.title2',
      langText: 'signInUpSlides.text2',
    },
    {
      imgLink: '/images/carousel-slides/Program.png',
      langTitle: 'signInUpSlides.title3',
      langText: 'signInUpSlides.text3',
    },
    {
      imgLink: '/images/carousel-slides/Collab.png',
      langTitle: 'signInUpSlides.title4',
      langText: 'signInUpSlides.text4',
    },
    {
      imgLink: '/images/carousel-slides/Community.png',
      langTitle: 'signInUpSlides.title5',
      langText: 'signInUpSlides.text5',
    },
  ];

  return (
    <Carousel fullSize>
      {slides.map((slide) => (
        <section tw="flex flex-col items-center justify-center m-10 -mt-1 w-full">
          <div>
            <img tw="max-w-xl max-h-80 mb-8" src={slide.imgLink} alt="Carousel slide img" loading="lazy" />
          </div>
          <div>
            <h4 tw="text-white text-3xl text-center pb-4">{t(`${slide.langTitle}`)}</h4>
            <p tw="text-white text-xl text-center">{t(`${slide.langText}`)}</p>
          </div>
        </section>
      ))}
    </Carousel>
  );
};

export default SignInUpCarousel;
