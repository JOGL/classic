import { ArrowRight as ArrowRightIcon, ArrowLeft as ArrowLeftIcon } from '@emotion-icons/fa-solid';
import React, { FC, useEffect, useRef, useState } from 'react';
import { SnapItem, SnapList, useDragToScroll, useScroll, useVisibleElements } from 'react-snaplist-carousel';
import { PaddingProps, space } from 'styled-system';
import styled from 'utils/styled';
import Box from '../Box';

interface Props {
  spaceX?: number;
  spaceY?: number;
  showDots?: boolean;
  paddingX?: PaddingProps;
  children: JSX.Element[];
  fullSize?;
  noArrows?;
}

const Carousel: FC<Props> = ({
  spaceX = 0,
  spaceY = 0,
  children,
  showDots = true,
  paddingX = '0',
  fullSize = false,
  noArrows = false,
}) => {
  const snapList = useRef(undefined);
  const [showControls, setShowControls] = useState(true);
  const [hideArrow, setHideArrow] = useState<undefined | 'left' | 'right'>('left');
  const selectItems = useVisibleElements({ debounce: 10, ref: snapList }, (elements) => elements);
  useEffect(() => {
    // Hide controls when there's no invisible elements aka the children don't overflow
    if (selectItems.length === children.length) {
      setShowControls(false);
    } else {
      setShowControls(true);
    }
    // Hide right arrow when last selectItems is visible
    if (children.length - 1 === selectItems[selectItems.length - 1]) {
      setHideArrow('right');
    } else if (selectItems[0] === 0) {
      // Hide left arrow when selectItems first visible element is the first children.
      setHideArrow('left');
    } else {
      setHideArrow(undefined);
    }
  }, [selectItems]);

  const goToElement = useScroll({ ref: snapList });
  useDragToScroll({ ref: snapList });

  return (
    <Container px={paddingX} fullSize className={fullSize && 'leftDivDesktop'}>
      {showControls && !noArrows && (
        <>
          {hideArrow !== 'left' && (
            <ArrowLeft type="button" onClick={() => goToElement(selectItems[0] - 1)}>
              <ArrowLeftIcon size={18} title="Move left" />
            </ArrowLeft>
          )}
          {hideArrow !== 'right' && (
            <ArrowRight type="button" onClick={() => goToElement(selectItems[selectItems.length - 1] + 1)}>
              <ArrowRightIcon size={18} title="Move right" />
            </ArrowRight>
          )}
        </>
      )}

      <SnapList ref={snapList} width="100%" direction="horizontal" height={fullSize && '100%'}>
        {children.map((child, i) => (
          <SnapItem
            key={i}
            margin={{
              // @ts-ignore
              left: i === 0 ? 0 : spaceX,
              // @ts-ignore
              right: i === children.length ? 0 : spaceX,
              // @ts-ignore
              top: spaceY,
              // @ts-ignore
              bottom: '.5rem',
            }}
            snapAlign={i === 0 ? 'start' : 'center'}
            height={fullSize && '100%'}
            width={fullSize && '100%'}
          >
            {child}
          </SnapItem>
        ))}
      </SnapList>
      {showDots && showControls && (
        <Box
          row
          justifyContent="center"
          pt={4}
          spaceX={2}
          position={fullSize && 'relative'}
          bottom={fullSize && '4rem'}
        >
          {children.map((_, index) => (
            <Dot key={index} active={selectItems.includes(index)} type="button" onClick={() => goToElement(index)} />
          ))}
        </Box>
      )}
    </Container>
  );
};

const Container = styled.div`
  ${space};
  position: relative;
  width: ${(props) => props.fullSize && '100%'};
  height: ${(props) => props.fullSize && '100%'};
`;
const ArrowLeft = styled.button`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  position: absolute;
  bottom: 0;
  left: 0;
  top: calc(50% - 45px);
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  box-shadow: ${(props) => props.theme.shadows.default};
  background-color: white;
`;
const ArrowRight = styled.button`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  position: absolute;
  bottom: 0;
  right: 0;
  top: calc(50% - 45px);
  height: 2rem;
  width: 2rem;
  border-radius: 50%;
  box-shadow: ${(props) => props.theme.shadows.default};
  background-color: white;
`;
interface DotProps {
  active: boolean;
}
const Dot = styled.button<DotProps>`
  background: none;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  border-radius: 50%;
  width: 10px;
  height: 10px;
  background-color: ${(p) => (p.active ? p.theme.colors.greys['800'] : p.theme.colors.greys['500'])};
  &:hover {
    background-color: ${(p) => p.theme.colors.greys['800']};
  }
`;

export default Carousel;
