import Link from 'next/link';
import React, { useEffect, useState, FC } from 'react';
import { Bell } from '@emotion-icons/fa-solid/Bell';
import useTranslation from 'next-translate/useTranslation';
import { Menu, MenuLink } from '@reach/menu-button';
import { useApi } from 'contexts/apiContext';
import { DropDownMenu, StyledMenuButton } from './Header.styles';
import { displayObjectRelativeDate } from 'utils/utils';
import P from '../primitives/P';
import { NotifIconWrapper } from 'utils/notificationsIcons';
import { notifIconTypes } from 'utils/notificationsIcons';
import tw, { styled } from 'twin.macro';

interface Props {
  userId?: number;
}

const HeaderNotifications: FC<Props> = ({ userId }) => {
  const api = useApi();
  const [loading, setLoading] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(0);
  const { t } = useTranslation('common');

  useEffect(() => {
    // to make sure API calls do not set state when unmounted
    let isFetching = true;
    userId &&
      api
        .post('/graphql/', {
          query: `
            query {
              user (id: ${userId} ) {
                notifications(first: 1) {
                  unreadCount
                  totalCount
                }
              }
            }
          `,
        })
        .then((response) => {
          isFetching && setUnreadNotificationsCount(response.data.data.user.notifications.unreadCount);
        });

    return () => (isFetching = false);
  }, [userId]);

  const visitNotificationLinkAndMarkAsRead = (notification) => {
    api
      .post('/graphql/', {
        query: `
          mutation {
            markNotificationAsRead(input: {id: ${notification.id}}) {
              notification {
                id
                read
              }
            }
          }`,
      })
      .then((_response) => {
        // On success, update unread count
        notification.read = true;
        setUnreadNotificationsCount(unreadNotificationsCount - 1);
      });
  };

  const markAllNotificationsAsRead = () => {
    api
      .post('/graphql/', {
        query: `
          mutation {
            markAllNotificationsAsRead(input: {}) {
              unreadCount
            }
          }`,
      })
      .then((response) => {
        // On success, update unread count
        setUnreadNotificationsCount(response.data.unreadCount);
        let notifs = notifications.map((notif) => {
          notif.read = true;
          return notif;
        });
        setNotifications(notifs);
      });
  };

  const handleClick = () => {
    setLoading(true);
    api
      .post('/graphql/', {
        query: `
              query {
                user (id: ${userId} ) {
                  notifications(first: 20) {
                    edges {
                      node {
                        id
                        ctaLink
                        read
                        subjectLine
                        createdAt
                        category
                      }
                    }
                  }
                }
              }
            `,
      })
      .then((response) => {
        var notifications = response.data.data.user.notifications;
        setNotifications(notifications.edges.map((edge) => edge.node));
        setLoading(false);
      })
      .catch(() => setLoading(false));
  };

  return (
    <Menu>
      {({ isExpanded }) => (
        <React.Fragment>
          <div tw="flex self-center">
            <StyledMenuButton
              style={{ position: 'relative' }}
              onClick={() => isExpanded && handleClick()}
              onTouchStart={handleClick} // isExpanded seems to be not working on mobile, so always trigger handleClick on mobile
            >
              <Bell size={18} title="Notifications" />
              <div tw="absolute right-0.5 -top-2 text-white text-xs font-semibold py-0 px-0.5 bg-red-600 rounded">
                {unreadNotificationsCount > 0 && unreadNotificationsCount <= 99
                  ? // if notif number is between 1 and 99, display count
                    unreadNotificationsCount
                  : // if more than 99, display 99+
                    unreadNotificationsCount > 99 && '99+'}
              </div>
            </StyledMenuButton>
          </div>
          <NotifDropDown>
            <div tw="flex flex-row justify-between items-center py-2 px-3 border-0 border-b border-solid border-gray-700 flex-wrap">
              <div tw="font-bold pr-2">{t('settings.notifications.title')}</div>
              <div tw="flex flex-row items-end border border-t-0">
                <div tw="cursor-pointer text-primary hover:opacity-80" onClick={() => markAllNotificationsAsRead()}>
                  {t('settings.notifications.markAsRead')}
                </div>
                <div tw="px-1">.</div>
                <Link href={`/user/${userId}/edit?t=notification-settings`}>
                  <a>{t('menu.profile.settings')}</a>
                </Link>
              </div>
            </div>
            <NotificationsList>
              {loading
                ? [...Array(8)].map((e, i) => (
                    <div tw="py-4 px-2 w-full mx-auto border-0 border-b-2 border-solid border-gray-300" key={i}>
                      <div tw="animate-pulse flex space-x-2">
                        <div tw="rounded-full bg-blue-200 h-8 w-8 mr-1"></div>
                        <div tw="flex-1 space-y-2">
                          <div tw="h-4 bg-blue-200 rounded"></div>
                          <div tw="h-3 bg-blue-200 rounded w-2/6"></div>
                        </div>
                      </div>
                    </div>
                  ))
                : notifications.map((notification) => {
                    return (
                      <MenuLink
                        href={notification.ctaLink}
                        key={notification.id}
                        onClick={() => visitNotificationLinkAndMarkAsRead(notification)}
                        tw="py-2.5 px-3 background[#fcfcfc] border-0 border-b border-gray-300 border-solid hover:(background[#eaedf0] no-underline)"
                      >
                        <div tw="flex justify-between items-center">
                          <div tw="flex">
                            <div tw="flex pr-2">
                              <NotifIconWrapper>{notifIconTypes[notification.category]}</NotifIconWrapper>
                            </div>
                            <div tw="flex flex-wrap flex-row">
                              <P mr={2} mb={0}>
                                {notification.subjectLine}
                              </P>
                              <P mb={0} color="#797979">
                                {displayObjectRelativeDate(notification.createdAt)}
                              </P>
                            </div>
                          </div>
                          {!notification.read && (
                            <div tw="ml-2">
                              <div tw="h-3 w-3 bg-primary rounded-full" />
                            </div>
                          )}
                        </div>
                      </MenuLink>
                    );
                  })}
            </NotificationsList>
            <div tw="flex flex-row justify-center py-2 border-t border-gray-700">
              <Link href="/notifications" passHref>
                <a>{t('program.seeAll')}</a>
              </Link>
            </div>
          </NotifDropDown>
        </React.Fragment>
      )}
    </Menu>
  );
};

const NotifDropDown = styled(DropDownMenu)`
  ${tw`border border-gray-600 border-solid`}
  > div * {
    ${tw`border-t-0`}
    font-size: 0.9rem;
  }
  a {
    ${tw`whitespace-normal`}
  }
  width: 400px;

  @media (max-width: 500px) {
    width: 330px;
  }
  @media (max-width: 450px) {
    width: 300px;
  }
  @media (max-width: 425px) {
    width: 280px;
  }
  @media (max-width: 400px) {
    width: 235px;
  }
  @media (max-width: 360px) {
    width: 215px;
  }
  @media (max-width: 330px) {
    width: 205px;
  }
`;

const NotificationsList = styled.div`
  ${tw`overflow-y-scroll`}
  height: 470px;
  max-height: 70vh;
  a {
    ${tw`whitespace-normal`}
    font-size: .9rem;
  }
`;

export default HeaderNotifications;
