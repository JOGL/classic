import Box from '../Box';
import Link from 'next/link';
import Loading from '../Tools/Loading';
import React, { useContext, useState, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Menu, MenuLink } from '@reach/menu-button';
import { Router, useRouter } from 'next/router';
import { Transition } from 'react-transition-group';
import Button from 'components/primitives/Button';
import UserMenu from 'components/User/UserMenu';
import { UserContext } from 'contexts/UserProvider';
import useGet from 'hooks/useGet';
import { Program } from 'types';
import styled from 'utils/styled';
import { useTheme } from 'utils/theme';
import { useModal } from 'contexts/modalContext';
import {
  AMenu,
  Container,
  DesktopNav,
  DropDownMenu,
  LangItem,
  Logo,
  MobileNav,
  MobileSideNav,
  StyledMenuButton,
} from './Header.styles';
import HeaderNotifications from './HeaderNotifications';
import setLanguage from 'next-translate/setLanguage';

import { Bars, Check, Globe, Search } from '@emotion-icons/fa-solid';

const languages = [
  { id: 'en', title: 'English' },
  { id: 'fr', title: 'Français' },
  { id: 'de', title: 'Deutsch' },
  { id: 'es', title: 'Español' },
];

const Header = () => {
  const router = useRouter();
  const theme = useTheme();
  const { userData, isConnected } = useContext(UserContext);
  const [isOpen, setOpen] = useState(false);
  const [loading, setLoading] = useState(true);
  const { data: programs } = useGet<Program[]>('/api/programs?simple=true');
  const { t } = useTranslation('common');
  const modal = useModal();

  // force close mobile menu when changing page, as the href of some pages are the same(all search page)
  Router.events.on('routeChangeStart', () => setOpen(false));

  const gotoSignInPage = () => {
    // don't add the redirectUrl query param if we go to signin page from the homepage (for url esthetics reason)
    if (router.pathname === '/') router.push({ pathname: '/signin' });
    else router.push({ pathname: '/signin', query: { redirectUrl: router.asPath } });
  };

  useEffect(() => {
    setLoading(false);
  }, [isConnected]);

  const ProgramsDropDown = () => (
    <Menu>
      <StyledMenuButton>
        {t('general.active_programs')} <span aria-hidden>▾</span>
      </StyledMenuButton>

      <DropDownMenu>
        {programs?.programs ? (
          programs?.programs
            ?.reverse()
            .filter(({ status }) => status !== 'completed')
            .map((program, i) => (
              // browse through programs (from newest to oldest)
              <Link key={i} href={`/program/${program.short_title}`} passHref>
                <MenuLink to={`/program/${program.short_title}`}>{program.title}</MenuLink>
              </Link>
            ))
        ) : (
          <Loading />
        )}
      </DropDownMenu>
    </Menu>
  );

  const openCreateSpaceModal = () => {
    modal.showModal({
      children: (
        <div tw="sm:p-6 text-center">
          <p tw="mx-auto text-4xl sm:text-5xl">🏖</p>
          <p tw="text-lg sm:text-xl">
            <strong>{t('space.modalCreate.sub-title')}</strong>
          </p>
          <p tw="text-lg sm:text-xl mb-10">{t('space.modalCreate.text')}</p>
          <div tw="text-center">
            <a href="mailto:contact@jogl.io?subject=Contact request to create a Space as a beta community">
              <Button btnType="primary">{t('space.modalCreate.button2-contact')}</Button>
            </a>
          </div>
        </div>
      ),
      title: t('space.modalCreate.title'),
      maxWidth: '20rem',
      modalClassName: 'createSpaceModal',
    });
  };

  return (
    <Container height={10} px={[3]}>
      {/* for users navigating with keyboard, show this so they can skip content*/}
      <div className="skip-links">
        Skip to <a href="#main">content</a> or <a href="#footer">footer</a>
      </div>
      {/* Mobile Nav -- Will only display on mobile breakpoints */}
      <MobileNav display={['flex', undefined, 'none']}>
        <div id="home-link-mobile">
          <Link href="/" passHref>
            <Box width={theme.sizes[11]} height="auto">
              <Logo src="/images/logo_img.png" alt="JOGL icon" quality={50} />
            </Box>
          </Link>
        </div>
        <Box row>
          {isConnected && !loading && (
            <div tw="flex space-x-4 mr-2">
              <HeaderNotifications userId={userData?.id} />
              <UserMenu />
            </div>
          )}
          <Menu>
            <BurgerButton onClick={() => setOpen(!isOpen)}>
              <Bars size={23} title="My objects" />
            </BurgerButton>
            <Transition in={isOpen} timeout={300}>
              {(state) => (
                // state change: exited -> entering -> entered -> exiting -> exited
                <MobileSideNav open={state === 'entering' || state === 'entered'} p={4}>
                  <MobileLinksBox spaceY={3}>
                    <Link href="/search/projects" passHref>
                      <AMenu>{t('general.explore')}</AMenu>
                    </Link>
                    <div tw="pt-3">
                      <ProgramsDropDown />
                    </div>
                    {isConnected && (
                      <div tw="pt-3">
                        <CreateObjectsDropdown t={t} openCreateSpaceModal={openCreateSpaceModal} />
                      </div>
                    )}
                    <LangDropdown />
                    {!isConnected && <Button onClick={gotoSignInPage}>{t('header.signInUp')}</Button>}
                  </MobileLinksBox>
                </MobileSideNav>
              )}
            </Transition>
          </Menu>
        </Box>
      </MobileNav>
      {/* Desktop Nav */}
      <DesktopNav display={['none', undefined, 'flex']}>
        <Box row spaceX={[undefined, undefined, 4, 5]} display="inline-flex" alignItems="center">
          <div id="home-link-desktop">
            <Link href="/" passHref>
              <AMenu>
                <Box width={theme.sizes[12]} height="auto">
                  <Logo src="/images/logo_img.png" alt="JOGL icon" quality={50} />
                </Box>
              </AMenu>
            </Link>
          </div>
          <div id="explore">
            <Link href="/search/members" passHref className="explore">
              <AMenu>
                <Search size={15} title="Search website" />
                {t('general.explore')}
              </AMenu>
            </Link>
          </div>
          <div id="activePrograms">
            <ProgramsDropDown />
          </div>
          {isConnected && !loading && <CreateObjectsDropdown t={t} openCreateSpaceModal={openCreateSpaceModal} />}
        </Box>
        <Box row spaceX={4}>
          {isConnected && !loading && <HeaderNotifications userId={userData?.id} />}
          <LangDropdown />
          {loading ? (
            <Loading />
          ) : isConnected ? (
            <UserMenu />
          ) : (
            <Button onClick={gotoSignInPage}>{t('header.signInUp')}</Button>
          )}
        </Box>
      </DesktopNav>
    </Container>
  );
};

const CreateObjectsDropdown = ({ t, openCreateSpaceModal }) => (
  <Menu>
    <StyledMenuButton textAlign={['left', undefined, undefined, 'center']}>
      {t('entity.form.btnCreate')} <span aria-hidden>▾</span>
    </StyledMenuButton>
    <DropDownMenu>
      <Link href="/project/create" passHref>
        <MenuLink to="/project/create">{t('header.createProject')}</MenuLink>
      </Link>
      {/* <Link href="/community/create" passHref>
        <MenuLink to="/community/create">{t('header.createGroup')}</MenuLink>
      </Link> */}
      <div tw="hover:text-white">
        <MenuLink onClick={openCreateSpaceModal}>{t('header.createSpace')}</MenuLink>
      </div>
    </DropDownMenu>
  </Menu>
);

const LangDropdown = () => {
  const router = useRouter();
  const { locale } = router;

  const changeLanguage = (newLanguage) => {
    // This is allowed because it is only called on client side.
    // This code has a lot of implications, because normally there's no locale set in the
    // cookies so the prioritized language is the one from your browser if it exists.
    // If your browser language is not supported it will fallback to english.
    // But if you set a locale in the cookies this lang will prioritize over all langs.
    // This is all defined in the custom server in server.js
    // cookie.set('locale', newLanguage, { expires: 365 });

    // Using next-translate new feature setLanguage to update the language globally within the app
    setLanguage(newLanguage);

    // router.push(router.pathname, router.asPath, { locale: newLanguage });
  };

  return (
    <Menu>
      <StyledMenuButton textAlign={['left', undefined, undefined, 'center']} uppercase>
        <Globe size={15} title="Change locale" /> <span tw="-ml-1">{locale}</span> <span aria-hidden>▾</span>
      </StyledMenuButton>
      <DropDownMenu>
        {languages.map((lang, i) => (
          <LangItem
            onClick={() => changeLanguage(lang.id)}
            onSelect={() => changeLanguage(lang.id)}
            selected={locale === lang.id}
            key={i}
          >
            {lang.title} {locale === lang.id && <Check size={16} title="Selected locale" />}
          </LangItem>
        ))}
      </DropDownMenu>
    </Menu>
  );
};

const BurgerButton = styled(StyledMenuButton)`
  z-index: 1;
  position: relative;
  margin-left: 0.75rem;
  font-size: 1.5rem;
`;

const MobileLinksBox = styled(Box)`
  > * + div,
  > * + button {
    ${(p) => `border-top: 1px solid ${p.theme.colors.greys['400']}!important`};
    padding-top: 0.75rem;
  }
`;

export default React.memo(Header);
