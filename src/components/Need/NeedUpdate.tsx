import useTranslation from 'next-translate/useTranslation';
import { FC, useState } from 'react';
import { useApi } from 'contexts/apiContext';
import { NeedDisplayMode } from 'pages/need/[id]';
import { Need } from 'types';
import { logEventToGA } from 'utils/analytics';
import NeedDocsManagement from './NeedDocsManagement';
import NeedForm from './NeedForm';
import NeedWorkers from './NeedWorkers';
// import "../Needs.scss";

interface Props {
  changeMode: (newMode: NeedDisplayMode) => void;
  need: Need;
  refresh: () => void;
}
const NeedUpdate: FC<Props> = ({
  changeMode = () => console.warn('Missing changeMode function'),
  need: needProp,
  refresh: refreshProp = () => console.warn('Missing refresh function'),
}) => {
  const [error, setError] = useState('');
  const [need, setNeed] = useState(needProp);
  const [shouldUploadDoc, setShouldUploadDoc] = useState(false);
  const [uploading, setUploading] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');

  const handleChange = (key, content) => {
    const updateNeed = need;
    updateNeed[key] = content;
    setNeed(updateNeed);
    setError('');
  };
  const refresh = () => {
    setNeed(undefined);
    setUploading(false);
    refreshProp();
    changeMode('details');
  };
  const handleChangeDoc = (newDocuments) => {
    handleChange('documents', newDocuments);
    refreshProp();
    setShouldUploadDoc(true);
  };

  const handleSubmit = () => {
    setUploading(true);
    const needId = need.id;
    api
      .patch(`/api/needs/${needId}`, { need })
      .then((res) => {
        const userId = res.config.headers.userId;
        // send event to google analytics
        logEventToGA('update need', 'Need', `[${userId},${needId}]`, { userId, needId });
        if (need.documents.length > 0 && shouldUploadDoc) {
          if (needId) {
            const bodyFormData = new FormData();
            Array.from(need.documents).forEach((file) => {
              bodyFormData.append(`documents[]`, file);
            });
            const config = {
              headers: { 'Content-Type': 'multipart/form-data' },
            };

            api
              .post(`/api/needs/${needId}/documents`, bodyFormData, config)
              .then(() => {
                if (res.status === 200) {
                  refresh();
                } else {
                  setUploading(false);
                  setError(t('err-'));
                }
              })
              .catch((err) => {
                console.error(`Couldn't POST need with needId=${needId}`, err);
                setUploading(false);
                setError(`${err.response.data.status} : ${err.response.data.error}`);
              });
          } else {
            // Unable to upload files (No Id defined)
            refresh();
          }
        } else {
          refresh();
        }
      })
      .catch((err) => {
        console.error(`Couldn't PATCH need with id=${need.id}`, err);
        setUploading(false);
        setError(t('err-'));
      });
  };

  // put need card back to different state depending if it's in an object, or as a single need page
  if (need === undefined) {
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }
  return (
    <div className="needUpdate">
      {error && (
        <div className="alert alert-danger" role="alert">
          {t('err-')}
        </div>
      )}
      <NeedForm
        action="update"
        cancel={() => changeMode('details')}
        need={need}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        uploading={uploading}
      />
      <NeedWorkers need={need} mode="update" />
      <NeedDocsManagement need={need} handleChange={handleChangeDoc} mode="update" />
    </div>
  );
};
export default NeedUpdate;
