import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { Delete } from '@emotion-icons/material/Delete';
import { useApi } from 'contexts/apiContext';
import { useRouter } from 'next/router';

import { Need } from 'types';

interface Props {
  need?: Need;
}

const NeedDelete: FC<Props> = ({ need }) => {
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');

  const deleteNeed = () => {
    if (need) {
      api
        .delete(`api/needs/${need.id}`)
        .then(() => {
          // after deletion, show delete confirmation modal + redirect to project page
          alert('Deleted');
          router.push(`/project/${need.project.id}?t=needs`);
        })
        .catch((error) => {
          console.error(error);
        });
    }
  };
  return (
    <div onClick={deleteNeed} onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && deleteNeed()} tabIndex={0}>
      <Delete size={25} title="Delete need" />
      {t('feed.object.delete')}
    </div>
  );
};

export default NeedDelete;
