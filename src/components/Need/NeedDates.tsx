import { Bolt } from '@emotion-icons/fa-solid/Bolt';
import useTranslation from 'next-translate/useTranslation';
import styled from 'utils/styled';
import { useTheme } from 'utils/theme';
import { displayObjectDate, isDateUrgent } from 'utils/utils';
import Box from '../Box';
import P from '../primitives/P';

interface Props {
  publishedDate: Date;
  dueDate: Date;
  status: string;
}
const NeedDates = ({ publishedDate, dueDate, status }: Props) => {
  const theme = useTheme();
  const { t } = useTranslation('common');
  const getDaysLeft = (dueDate) => {
    const now = new Date();
    const end = new Date(dueDate);
    var daysLeft = Math.ceil((end - now) / 1000 / 60 / 60 / 24);
    if (daysLeft < 0) {
      daysLeft = -1;
    }
    return daysLeft;
  };

  return (
    <Box row alignItems="center" spaceX={2}>
      {/* show published date if we have it */}
      {publishedDate && (
        <P mb={0} color={theme.colors.greys['600']}>
          {`${t('post.posted')}: `}
          {displayObjectDate(publishedDate)}
        </P>
      )}
      {/* separator if we have the 2 dates, and status is not completed, and if due date is still in future */}
      {dueDate && publishedDate && status !== 'completed' && getDaysLeft(dueDate) >= 0 && <Box>•</Box>}
      {/* if there is a due date and it's less than 10 days */}
      {dueDate && getDaysLeft(dueDate) <= 10 && status !== 'completed' && (
        <div tw="text-gray-700">
          {
            0 <= getDaysLeft(dueDate) === getDaysLeft(dueDate) < 1 && t('need.card.due.today') // if date is today (between 0 and 1 day)
          }
          {getDaysLeft(dueDate) === 1 && // if date is tomorrow
            t('need.card.due.tomorrow')}
          {getDaysLeft(dueDate) > 1 && // if date is more than tomorrow (and less then 10 days)
            t('need.card.due.start') + ` ${Math.ceil(getDaysLeft(dueDate))} ` + t('need.card.due.end')}
        </div>
      )}
      {/* if there is a due date and it's more than 10 days */}
      {dueDate &&
        getDaysLeft(dueDate) > 10 &&
        status !== 'completed' && ( // if due date is more than 10 days
          <P mb={0} color={theme.colors.greys['600']}>
            {`${t('need.end_date')}: `}
            {displayObjectDate(dueDate)}
          </P>
        )}
      {/* if due date is less than 48 hours, show little 'bolt' icon */}
      {dueDate && isDateUrgent(dueDate) && status !== 'completed' && <BoltIcon title="Urgent need" />}
    </Box>
  );
};

const BoltIcon = styled(Bolt)`
  background-color: ${(p) => p.theme.colors.orange};
  padding: 0.3rem;
  width: 1.8rem !important;
  height: 1.8rem !important;
  color: white;
  border-radius: 50%;
`;

export default NeedDates;
