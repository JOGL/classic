/* eslint-disable camelcase */
import { useState, ReactNode, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import NeedDocsManagement from './NeedDocsManagement';
import NeedForm from './NeedForm';
import { useApi } from 'contexts/apiContext';
import useUserData from 'hooks/useUserData';
import { logEventToGA } from 'utils/analytics';
// import "../Needs.scss";

interface Props {
  projectId: number;
  refresh?: () => void;
}
const NeedCreate: FC<Props> = ({
  projectId,
  refresh: refreshProp = () => console.warn('Missing refresh function'),
}) => {
  const [error, setError] = useState<string | ReactNode>('');
  const { t } = useTranslation('common');
  const api = useApi();
  const [isCreating, setIsCreating] = useState(false);
  const { userData, userDataError } = useUserData();
  const [need, setNeed] = useState({
    content: '',
    creator: userData,
    documents: [],
    skills: [],
    resources: [],
    title: '',
    users: [],
    end_date: '',
    project_id: undefined,
  });
  const [uploading, setUploading] = useState(false);
  const handleChange = (key, content) => {
    setNeed((prevNeed) => ({ ...prevNeed, [key]: content }));
    setError('');
  };

  const handleChangeDoc = (documents) => {
    handleChange('documents', documents);
  };
  const refresh = () => {
    setIsCreating(false);
    setNeed((prevState) => ({
      ...prevState,
      content: '',
      end_date: '',
      creator: userData,
      documents: [],
      skills: [],
      resources: [],
      title: '',
    }));
    setUploading(false);

    refreshProp();
  };

  const handleSubmit = () => {
    if (!projectId) {
      // project_id is missing
    } else {
      setUploading(true);
      need.project_id = projectId;
      api
        .post('/api/needs', { need })
        .then((res) => {
          // send event to google analytics
          logEventToGA('create need', 'Need', `[${userData.id},${res.data.id}]`, {
            userId: userData.id,
            needId: res.data.id,
          });
          const itemId = res.data.id;
          if (need.documents.length > 0) {
            const itemType = 'needs';
            const type = 'documents';
            if (itemId) {
              const bodyFormData = new FormData();
              Array.from(need.documents).forEach((file) => {
                bodyFormData.append(`${type}[]`, file);
              });
              const config = {
                headers: { 'Content-Type': 'multipart/form-data' },
              };

              api
                .post(`/api/${itemType}/${itemId}/documents`, bodyFormData, config)
                .then((res2) => {
                  if (res2.status === 200) {
                    // Documents uploaded
                    refresh();
                  } else {
                    setUploading(false);
                    setError(t('err-'));
                  }
                })
                .catch((err) => {
                  setError(`${err.response.data.status} : ${err.response.data.error}`);
                  setUploading(false);
                });
            } else {
              // Unable to upload files (No Id defined)
              refresh();
            }
          } else {
            refresh();
          }
          api.put(`/api/needs/${itemId}/follow`); // follow the need when creating it
        })
        .catch(() => {
          setUploading(false);
          setError(t('err-'));
        });
    }
  };

  const changeDisplay = () => {
    setIsCreating((prevState) => !prevState);
  };

  if (!need) {
    return 'OK';
  }
  if (isCreating) {
    return (
      <div className="needCreate isCreating row">
        <div className="col-12 col-md-10">
          {error && (
            <div className="alert alert-danger" role="alert">
              {t('err-')}
            </div>
          )}
          <NeedForm
            action="create"
            cancel={changeDisplay}
            need={need}
            handleChange={handleChange}
            handleSubmit={handleSubmit}
            uploading={uploading}
          />
        </div>
        <div className="col-12 col-md-2">
          <NeedDocsManagement need={need} handleChange={handleChangeDoc} mode="create" />
        </div>
      </div>
    );
  }
  return (
    <div className="needCreate justButton">
      <button className="btn btn-primary" onClick={changeDisplay} type="button">
        {t('need.addNeed')}
      </button>
    </div>
  );
};

export default NeedCreate;
