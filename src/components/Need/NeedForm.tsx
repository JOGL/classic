/* eslint-disable @rushstack/no-null */
import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import NeedBtnStatus from './NeedBtnStatus';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormResourcesComponent from 'components/Tools/Forms/FormResourcesComponent';
/** * Validators ** */
import FormValidator from 'components/Tools/Forms/FormValidator';
import needFormRules from './needFormRules.json';
import FormWysiwygComponent from '../Tools/Forms/FormWysiwygComponent';
import FormDefaultComponent from '../Tools/Forms/FormDefaultComponent';
import { Need } from 'types';
import useUserData from 'hooks/useUserData';
import SpinLoader from '../Tools/SpinLoader';
// import "./NeedForm.scss";

interface Props {
  action: 'update' | 'create';
  need: Need;
  uploading: boolean;
  cancel: () => void;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const NeedForm: FC<Props> = ({
  action = 'create',
  need = undefined,
  uploading = false,
  cancel,
  handleChange,
  handleSubmit,
}) => {
  const { t } = useTranslation('common');
  const validator = new FormValidator(needFormRules);
  const user = useUserData();
  const [stateValidation, setStateValidation] = useState({});
  const { valid_skills, valid_title } = stateValidation || '';

  const handleChangeNeed = (event, value) => {
    const key = event;
    const content = value;

    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation[`valid_${key}`] = validation[key];
      setStateValidation(stateValidation);
    }
    /* Validators end */
    handleChange(key, content);
  };

  const handleSubmitNeed = (event) => {
    event.preventDefault();
    /* Validators control before submit */
    const validation = validator.validate(need);
    if (validation.isValid) {
      handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          stateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(stateValidation);
    }
  };

  const renderInvalid = (valid_obj) => {
    if (valid_obj) {
      if (valid_obj.message !== '') {
        return <div className="invalid-feedback">{t(valid_obj.message)}</div>;
      }
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  };

  const submitBtnText = action === 'create' ? 'Create' : 'Update';
  return (
    <div className="needForm">
      <div className="form-row">
        <div className="input-group col-12 col-md-6">
          <FormDefaultComponent
            id="title"
            content={need.title}
            title={t('need.needTitle')}
            placeholder={t('need.needTitle')}
            onChange={handleChangeNeed}
            errorCodeMessage={valid_title ? valid_title.message : ''}
            isValid={valid_title ? !valid_title.isInvalid : undefined}
            mandatory
          />
          {renderInvalid(valid_title)}
        </div>
        <div className="input-group col-12 col-md-6">
          <FormDefaultComponent
            id="end_date"
            content={need.end_date && need.end_date.substr(0, 10)}
            title={t('need.end_date')}
            onChange={handleChangeNeed}
            type="date"
            // set min date to to today
            minDate={new Date().toISOString().split('T')[0]}
          />
        </div>
        <div className="input-group col-12">
          <FormWysiwygComponent
            id="content"
            content={need.content}
            title=""
            placeholder={t('need.content')}
            onChange={handleChangeNeed}
            show
          />
          {/* {renderInvalid(valid_content)} */}
        </div>
        <div className="input-group col-12">
          <FormSkillsComponent
            className={`form-control ${valid_skills ? (!valid_skills.isInvalid ? 'is-valid' : 'is-invalid') : ''}`}
            content={need.skills}
            errorCodeMessage={valid_skills ? valid_skills.message : ''}
            id="skills"
            isValid={valid_skills ? !valid_skills.isInvalid : undefined}
            onChange={handleChangeNeed}
            placeholder={t('general.skills.placeholder')}
            type="need"
            mandatory
            title={t('need.skills.title')}
          />
          {renderInvalid(valid_skills)}
        </div>
        <div className="input-group col-12">
          <FormResourcesComponent
            className="form-control"
            content={need.ressources}
            id="ressources"
            onChange={handleChangeNeed}
            placeholder={t('general.resources.placeholder')}
            type="need"
            title={t('need.resources.title')}
          />
        </div>
      </div>
      <div className="actionBar">
        {user && need.is_owner && <NeedBtnStatus need={need} />}
        <button type="button" className="btn btn-outline-primary cancel" onClick={cancel} disabled={uploading}>
          {t('entity.form.btnCancel')}
        </button>
        <button className="btn btn-primary" disabled={!!uploading} onClick={handleSubmitNeed} type="submit">
          {uploading && <SpinLoader />}
          {t(`post.create.btn${submitBtnText}`)}
        </button>
      </div>
    </div>
  );
};
export default NeedForm;
