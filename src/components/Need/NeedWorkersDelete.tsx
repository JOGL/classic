import { Component } from 'react';
import withTranslation from 'next-translate/withTranslation';
import { ApiContext } from 'contexts/apiContext';
import { ItemType } from 'types';
import ReactTooltip from 'react-tooltip';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  itemId: number;
  itemType: ItemType;
  worker: { id: number };
}
interface State {
  sending: boolean;
}
class NeedWorkersDelete extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      sending: false,
    };
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: undefined,
      worker: undefined,
    };
  }

  deleteWorker(worker) {
    const api = this.context;
    const { itemId, itemType } = this.props;
    const { t } = this.props.i18n;
    this.setState({ sending: true });
    api
      .delete(`/api/${itemType}/${itemId}/members/${worker.id}`)
      .then(() => {
        this.setState({ sending: false });
        alert(t('need.user.deleted'));
      })
      .catch((err) => {
        console.error(`Couldn't delete ${itemType} itemId=${itemId}`, err);
        alert(t('need.user.deleted'));

        this.setState({ sending: false });
      });
  }

  render() {
    const { worker } = this.props;
    const { t } = this.props.i18n;
    const { sending } = this.state;
    if (worker === undefined) {
      // eslint-disable-next-line @rushstack/no-null
      return null;
    }
    return (
      <>
        <button
          type="button"
          className="close delete"
          aria-label="Close"
          disabled={sending}
          onClick={() => this.deleteWorker(worker)}
          data-tip={t('general.remove')}
          data-for="need_worker_delete"
          // show/hide tooltip on element focus/blur
          onFocus={(e) => ReactTooltip.show(e.target)}
          onBlur={(e) => ReactTooltip.hide(e.target)}
        >
          {sending ? <SpinLoader /> : <span aria-hidden="true">&times;</span>}
        </button>
        <ReactTooltip id="need_worker_delete" effect="solid" />
      </>
    );
  }
}
NeedWorkersDelete.contextType = ApiContext;
export default withTranslation(NeedWorkersDelete, 'common');
