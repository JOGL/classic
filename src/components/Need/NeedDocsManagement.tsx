import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import BtnUploadFile from 'components/Tools/BtnUploadFile';
import mimetype2fa from 'components/Tools/Documents/mimetypes';
import { useApi } from 'contexts/apiContext';
import { NeedDisplayMode } from 'pages/need/[id]';
import Box from '../Box';
import ReactTooltip from 'react-tooltip';
import { Need } from 'types';

interface Props {
  need: Need;
  mode: NeedDisplayMode;
  handleChange?: (documents: any[]) => void;
}
const NeedDocsManagement: FC<Props> = ({ need, mode, handleChange }) => {
  const api = useApi();
  const { t } = useTranslation('common');
  const attachDocuments = (documents) => {
    // Documents received
    const arrayDocuments = need.documents;
    for (let i = 0; i < documents.length; i++) {
      arrayDocuments.push(documents[i]);
    }
    handleChange(arrayDocuments);
  };
  const deleteDocument = (document) => {
    if (mode === 'create') {
      const { documents } = need;
      documents.forEach((documentToInspect, index) => {
        if (documentToInspect === document) {
          documents.splice(index, 1);
        }
      });
      handleChange(documents);
    } else if (mode === 'update') {
      api
        .delete(`api/needs/${need.id}/documents/${document.id}`)
        .then(() => {
          const { documents } = need;
          documents.forEach((documentToInspect, index) => {
            if (documentToInspect === document) {
              documents.splice(index, 1);
            }
          });
          handleChange(documents);
        })
        .catch((error) => {
          console.error({ error });
        });
    }
  };
  const renderPreviewDocuments = (documents) => {
    if (documents.length !== 0) {
      return (
        <div className="preview">
          <div className="listDocuments">
            {documents.map((document, index) => (
              <div className="documentCardFeed" key={index}>
                <Box row>
                  {/* ! FA doesn't require 'fa' prefix */}
                  {mimetype2fa(document.content_type)}
                  {document.content_type === 'image/jpeg' ? (
                    // if document is a jpg image, make it hoverable with the image appearing in a popup
                    <div className="hover_img">
                      <a href={document.url} target="_blank" rel="noopener noreferrer">
                        {document.filename}
                        <span>
                          <img src={document.url} alt={document.filename} width="280" />
                        </span>
                      </a>
                    </div>
                  ) : (
                    // else just display the document
                    <a href={document.url} target="_blank" rel="noopener noreferrer">
                      {document.filename}
                    </a>
                  )}
                  {mode === 'update' && need.is_owner && (
                    <>
                      <button
                        type="button"
                        style={{ fontSize: '24px', opacity: '.8' }}
                        aria-label="Close"
                        data-tip={t('general.remove')}
                        data-for="need_doc_delete"
                        onClick={() => deleteDocument(document)}
                        // show/hide tooltip on element focus/blur
                        onFocus={(e) => ReactTooltip.show(e.target)}
                        onBlur={(e) => ReactTooltip.hide(e.target)}
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <ReactTooltip id="need_doc_delete" effect="solid" />
                    </>
                  )}
                </Box>
              </div>
            ))}
          </div>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  };

  if (need) {
    return (
      <div className="needDocsManagement">
        <h6>{t('need.docs.title')}</h6>
        {need.documents.length > 0 ? (
          renderPreviewDocuments(need.documents)
        ) : (
          <div className="noDoc">{t('need.docs.noDoc')}</div>
        )}
        {handleChange && (
          <BtnUploadFile
            itemId={need.id}
            itemType="needs"
            setListFiles={attachDocuments}
            type="documents"
            text={t('info-1003')}
            uploadNow={mode === 'update'}
          />
        )}
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default NeedDocsManagement;
