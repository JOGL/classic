import NextImage from 'next/image';
import React from 'react';
import tw, { styled } from 'twin.macro';

const ImageContainer = styled.div`
  > div {
    &:first-of-type {
      position: unset !important;
    }
    ${({ height }) => height && `max-height: ${height}`};
    ${({ width }) => width && `width: ${width}`};
  }
  /* display: flex; */
`;
const BaseImage = styled(NextImage)`
  object-fit: cover;
  width: 100% !important;
  position: relative !important;
  height: unset !important;
  // if image has tw styling, add this to override style from layout="fill"
  ${({ hastw }) => hastw && tw`(min-height[auto] min-width[auto])!`}
`;

const ImageOverlay = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1.25rem;
  width: ${({ color }) => color};
  text-decoration: none;
`;

const Image2 = (props) => {
  const { width, height, maxHeight, maxWidth, children, hastw, ...rest } = props;
  return (
    <ImageContainer width={width} height={height} maxHeight={maxHeight} maxWidth={maxWidth}>
      <BaseImage layout="fill" {...rest} hastw={hastw} />
      {children && <ImageOverlay>{children}</ImageOverlay>}
    </ImageContainer>
  );
};

export default Image2;
