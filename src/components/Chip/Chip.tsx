import React, { FC, forwardRef } from 'react';
import ReactTooltip from 'react-tooltip';
import styled from 'utils/styled';
import Box from '../Box';

interface Props {
  type?: 'skills' | 'resources';
  maxLength?: number;
  children: string | string[];
  smallChips?: boolean;
  hasOpacity?: boolean;
}
// TODO: Explain why we use forwardRef here.
const Chip: FC<Props> = forwardRef(
  ({ type = 'skill', children, maxLength = 1000, smallChips, hasOpacity = false }, ref) => {
    // add "..." to value if it's length is > than maxLength
    const value = children.length > maxLength ? `${children.slice(0, maxLength)}...` : children;
    return (
      <>
        <Container
          ref={ref}
          border={`1px solid #CED4DB`}
          backgroundColor={type === 'skills' ? '#7A7979' : '#D5D5D5'}
          color={type === 'skills' ? 'white' : '#5F5D5D'}
          borderRadius={smallChips ? '0' : '1rem'}
          height={smallChips ? '1.5rem' : '2.2rem'}
          fontWeight={smallChips ? '400' : '500'}
          justifyContent="center"
          alignItems="center"
          px={2}
          fontSize={smallChips ? 1 : '.9rem'}
          mr={smallChips ? 2 : '.65rem'}
          mb={smallChips ? 1 : '.6rem'}
          opacity={hasOpacity ? 0.65 : 1}
          hasOpacity={hasOpacity}
          // add tooltip prop to show chip tooltip, it its length is more than maxLength
          {...(children.length > maxLength && { 'data-tip': children, 'data-for': `skillTooltip${children}` })}
        >
          {value}
        </Container>
        <ReactTooltip
          id={`skillTooltip${children}`}
          effect="solid"
          role="tooltip"
          type="dark"
          className="forceTooltipBg"
        />
      </>
    );
  }
);

const Container = styled(Box)`
  white-space: nowrap;
  &:hover {
    ${(p) => !p.hasOpacity && `text-decoration: underline;`};
    ${(p) => p.hasOpacity && `opacity: 1`};
  }
`;

export default Chip;
