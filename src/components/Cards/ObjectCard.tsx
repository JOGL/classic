import Link from 'next/link';
import React, { FC, ReactNode } from 'react';
import Image2 from '../Image2';
import tw from 'twin.macro';

interface CardProps {
  imgUrl?: string;
  href?: string;
  children: ReactNode;
  containerStyle?: any;
  width?: string;
  chip?: string;
  hrefNewTab?: boolean;
}

const ObjectCard: FC<CardProps> = ({ imgUrl, children, href, width = '300px', chip, hrefNewTab = false, ...props }) => {
  return (
    <div
      {...props.containerStyle}
      style={{ width }}
      css={[tw`flex flex-col shadow-custom rounded-xl bg-white relative overflow-hidden`]}
    >
      {/* if card has image and image is also a link */}
      {imgUrl && href && (
        <div tw="relative">
          <Link href={href}>
            <a target={hrefNewTab && '_blank'}>
              <div tw="relative">
                <div tw="animate-pulse bg-gray-200 w-full h-full rounded-t-2xl absolute" />
                <Image2
                  src={imgUrl}
                  priority
                  tw="w-full object-cover rounded-tl-xl rounded-tr-xl h-24! hover:opacity-95"
                />
              </div>
            </a>
          </Link>
          {chip && (
            <div tw="absolute left-3 top-3 color[#4d4d4d] capitalize rounded bg-white bg-opacity-80 px-1 box-shadow[0px 4px 15px rgb(0 0 0 / 9%)]">
              {chip}
            </div>
          )}
        </div>
      )}
      {/* Card content */}
      <div
        css={[
          tw`flex flex-col flex-1`,
          !props.noPadding && tw`py-3 px-4`,
          imgUrl && tw`border-0 border-t border-solid border-gray-300`,
          // props.noPadding ? tw`px-0` : tw`px-4`,
        ]}
        {...props}
      >
        {children}
      </div>
    </div>
  );
};
export default ObjectCard;
