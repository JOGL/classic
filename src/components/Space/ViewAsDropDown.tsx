import { Menu, MenuItem } from '@reach/menu-button';
import React, { useCallback } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { DropDownMenu, StyledMenuButton } from './ViewAsDropDown.styles';
import { useSpaceViewAsContext, ViewAsMode } from 'contexts/SpaceViewAsContext';
import Box from '../Box';
import { useTheme } from 'utils/theme';

/**
 * Props to render each menu item in the drop down.
 */
interface ViewAsMenuItemProps {
  viewAsMode: ViewAsMode;
  menuItemText: string;
  selectItemHandler: (mode: ViewAsMode) => void;
}

/**
 * Simple menu item that presents a mode that can be selected.
 */
const ViewAsMenuItem: React.FC<ViewAsMenuItemProps> = ({ menuItemText, viewAsMode, selectItemHandler }) => {
  const handleSelect = useCallback(() => {
    selectItemHandler(viewAsMode);
  }, [selectItemHandler, viewAsMode]);

  return <MenuItem onSelect={handleSelect}>{menuItemText}</MenuItem>;
};

/**
 * Drop Down menu that allows selecting a mode to view a space as.
 */
export const ViewAsDropDown: React.FC = () => {
  const { t } = useTranslation('common');
  const theme = useTheme();
  const { viewAsMode, setViewAsMode } = useSpaceViewAsContext();
  const viewAsText = t(`general.viewingas.${viewAsMode}`);

  const handleSelectItem = useCallback(
    (mode: ViewAsMode) => {
      setViewAsMode(mode);
    },
    [setViewAsMode]
  );

  return (
    <Box bg={theme.colors.primaries['900']} p="5px">
      <Menu>
        <StyledMenuButton>
          {viewAsText}
          <span aria-hidden style={{ paddingLeft: '6px', paddingRight: '6px' }}>
            ▾
          </span>
        </StyledMenuButton>

        <DropDownMenu>
          <ViewAsMenuItem
            menuItemText={t('general.viewingas.owner')}
            viewAsMode={'owner'}
            selectItemHandler={handleSelectItem}
          />
          <ViewAsMenuItem
            menuItemText={t('general.viewingas.member')}
            viewAsMode={'member'}
            selectItemHandler={handleSelectItem}
          />
          <ViewAsMenuItem
            menuItemText={t('general.viewingas.public')}
            viewAsMode={'public'}
            selectItemHandler={handleSelectItem}
          />
        </DropDownMenu>
      </Menu>
    </Box>
  );
};
