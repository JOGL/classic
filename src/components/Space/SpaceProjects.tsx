import useTranslation from 'next-translate/useTranslation';
import useUserData from 'hooks/useUserData';
import { Project } from 'types';
import Box from '../Box';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import P from '../primitives/P';
import ProjectCard from '../Project/ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';
import { useModal } from 'contexts/modalContext';
import { ProjectLinkToSpace } from '../Project/ProjectLinkToSpace';
import FakeLink from '../primitives/FakeLink';

// const OverflowGradient = styled.div`
//   ${layout};
//   width: 3rem;
//   height: 100%;
//   position: absolute;
//   right: 0;
//   /* background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 134.37%); */
//   background: ${(p) =>
//     `linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`};
// `;

const SpaceProjects = ({ spaceId, isMember, hasFollowed }) => {
  const projectsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  // const [projectsEndpoints, setProjectsEndpoint] = useState(
  //   `/api/spaces/${spaceId}/projects?items=${projectsPerQuery}`
  // );
  // const { data: dataProjects, response } = useGet<{ projects: Project[] }>(projectsEndpoints);
  // const { data: dataPrograms, error: dataProgramsError } = useGet<{ programs }>(`/api/spaces/${spaceId}/programs`);
  // const [selectedProgramFilterId, setSelectedProgramFilterId] = useState(undefined);
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const {
    data: dataProjects,
    response,
    error,
    size,
    setSize,
  } = useInfiniteLoading<{
    projects: Project[];
  }>((index) => `/api/spaces/${spaceId}/projects?items=${projectsPerQuery}&page=${index + 1}`);
  const projects = dataProjects ? [].concat(...dataProjects?.map((d) => d.projects)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataProjects && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataProjects && typeof dataProjects[size - 1] === 'undefined');
  const isEmpty = dataProjects?.[0]?.length === 0;
  const isReachingEnd = isEmpty || projects?.length === totalNumber;

  // const onFilterChange = (e) => {
  //   const id = e.target.name;
  //   const itemsNb = projects.length > projectsPerQuery ? projects.length : projectsPerQuery;
  //   if (id) {
  //     setSelectedProgramFilterId(Number(id));
  //     setProjectsEndpoint(`/api/programs/${id}/projects?items=${itemsNb}`);
  //   } else {
  //     setSelectedProgramFilterId(undefined);
  //     setProjectsEndpoint(`/api/spaces/${spaceId}/projects?items=${projectsPerQuery}`);
  //   }
  // };

  return (
    <div>
      <Box>
        <P>{t('general.attached_projects_explanation')}</P>
        {!userData && ( // if user is not connected
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.project')}
          </A>
        )}

        {isMember && (
          <>
            {t('general.attached_projects_affiliation_cta')}
            <FakeLink
              onClick={() => {
                showModal({
                  children: (
                    <ProjectLinkToSpace
                      alreadyPresentProjects={dataProjects && dataProjects[0].projects}
                      spaceId={spaceId}
                      hasFollowed={hasFollowed}
                      closeModal={closeModal}
                    />
                  ),
                  title: t('attach.project.title'),
                  maxWidth: '50rem',
                });
              }}
            >
              {t('attach.project.title')}
            </FakeLink>
          </>
        )}
      </Box>
      <Box py={4} position="relative">
        {/* <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel="challenge.list_all"
            content={dataPrograms?.program
              // filter to hide draft programs
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!dataProgramsError}
            errorMessage="*Could not get programs filters"
            selectedId={selectedProgramFilterId}
          />
        </Box> */}
        {!dataProjects ? (
          <Loading />
        ) : projects?.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Grid tw="py-4">
            {projects
              // filter to only show association projects (that belongs to its challenges for ex) OR affiliated projects that have been accepted
              ?.filter(
                (project) =>
                  project.relation === 'associated' ||
                  project.affiliated_spaces?.find((affiliate) => affiliate[0].id === spaceId)?.[0]
                    .affiliation_status === 'accepted'
              )
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  followersCount={project.followers_count}
                  savesCount={project.saves_count}
                  postsCount={project.posts_count}
                  has_saved={project.has_saved}
                  skills={project.skills}
                  banner_url={project.banner_url}
                  relation={project.relation}
                />
              ))}
          </Grid>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          totalNumber > projectsPerQuery && size <= response?.[0].headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default SpaceProjects;
