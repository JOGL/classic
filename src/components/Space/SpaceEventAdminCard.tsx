import useTranslation from 'next-translate/useTranslation';
import React, { FC, ReactNode, useState } from 'react';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { Event } from 'types';
import Box from '../Box';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  event: Event;
  serviceId: number;
  callBack: () => void;
}
const SpaceEventAdminCard: FC<Props> = ({ event, serviceId, callBack }) => {
  const [sending, setSending] = useState<'remove' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const api = useApi();
  const { t } = useTranslation('common');

  const removeEvent = (): void => {
    setSending('remove');
    api
      .delete(`/api/services/${serviceId}/activities/${event.id}`)
      .then(() => {
        setSending(undefined);
        callBack();
      })
      .catch((err) => {
        console.error(err);
        setSending(undefined);
        setError(t('err-'));
      });
  };

  const bgLogo = {
    backgroundImage: `url(${event.logo || '/images/default/default-challenge.jpg'})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (event) {
    return (
      <div>
        <Box flexDirection="row" justifyContent="space-between" key={event.id} tw="py-3">
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <Box>
              <a href={event.url} target="_blank">
                {event.name}
              </a>
            </Box>
          </Box>
          <Box row alignItems="center">
            <Button btnType="danger" disabled={sending === 'remove'} onClick={removeEvent} tw="ml-3">
              {sending === 'remove' && <SpinLoader />}
              {t('general.remove')}
            </Button>
            {error && <Alert type="danger" message={error} />}
          </Box>
        </Box>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default SpaceEventAdminCard;
