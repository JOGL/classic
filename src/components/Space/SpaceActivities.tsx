import useTranslation from 'next-translate/useTranslation';
import Box from '../Box';
import Grid from '../Grid';
import Button from '../primitives/Button';
import P from '../primitives/P';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import { Challenge, Event } from 'types';
import ChallengeCreate from '../Challenge/ChallengeCreate';
import ActivityCard from '../Challenge/ActivityCard';
import { useEffect, useState } from 'react';
import { useApi } from 'contexts/apiContext';

const SpaceActivities = ({ spaceId, isAdmin }) => {
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const [affiliatedPrograms, setAffiliatedPrograms] = useState([]);
  const api = useApi();
  const { data: dataChallenges, revalidate: challengesRevalidate } = useGet<{ challenges: Challenge }>(
    `/api/spaces/${spaceId}/challenges`
  );
  const { data: eventsData } = useGet<{ events: Event[] }>(`/api/spaces/${spaceId}/activities`);

  const getAffiliatedPrograms = () => {
    setAffiliatedPrograms([]);
    api.get(`api/spaces/${spaceId}/affiliated/programs`).then((res) => {
      res.data.affiliates.programs.map((program) => {
        api.get(`/api/programs/${program.affiliate_id}`).then((res) => {
          setAffiliatedPrograms((affiliatedPrograms) => [
            ...affiliatedPrograms,
            {
              title: res.data.title,
              banner_url: res.data.banner_url,
              short_description: res.data.short_description,
              short_title: res.data.short_title,
              id: program.affiliate_id,
            },
          ]);
        });
      });
    });
  };

  useEffect(() => {
    getAffiliatedPrograms();
  }, []);

  return (
    <>
      {/* <P>{t('general.attached_challenges_explanation', { challenge_wording: t('general.activities') })}</P> */}
      {isAdmin && (
        <div tw="mt-4">
          <Button
            onClick={() => {
              showModal({
                children: (
                  <ChallengeCreate
                    isModal
                    objectId={spaceId}
                    objectType="space"
                    closeModal={closeModal}
                    callBack={challengesRevalidate}
                  />
                ),
                title: t('challenge.create.title', { challenge_wording: t('challenge.lowerCase') }),
                maxWidth: '50rem',
              });
            }}
            btnType="secondary"
          >
            {t('challenge.create.title', { challenge_wording: t('challenge.lowerCase') })}
          </Button>
        </div>
      )}
      {/* Grid showing all activities (challenges, events, programs) */}
      <Box py={4} position="relative">
        {!dataChallenges || !eventsData ? (
          <Loading />
        ) : dataChallenges?.challenges.length === 0 && eventsData?.length === 0 && affiliatedPrograms?.length === 0 ? (
          <NoResults type="activity" />
        ) : (
          <Grid tw="py-4">
            {dataChallenges.challenges
              ?.filter(({ status }) => status !== 'draft')
              // don't display draft challenges
              .map((challenge, index) => (
                <ActivityCard
                  key={index}
                  banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                  short_title={challenge.short_title}
                  title={challenge.title}
                  title_fr={challenge.title_fr}
                  short_description={challenge.short_description}
                  short_description_fr={challenge.short_description_fr}
                  custom_type={challenge.custom_type}
                />
              ))}
            {eventsData?.map((event, index) => (
              <ActivityCard
                key={index}
                banner_url={event.logo || '/images/default/default-challenge.jpg'}
                link={event.url}
                title={event.name}
                short_description={event.description}
                custom_type={t('space.event.title')}
              />
            ))}
            {affiliatedPrograms?.map((program, index) => (
              <ActivityCard
                key={index}
                banner_url={program.banner_url || '/images/default/default-challenge.jpg'}
                short_title={program.short_title}
                title={program.title}
                short_description={program.short_description}
                custom_type={t('program.title')}
              />
            ))}
          </Grid>
        )}
      </Box>
    </>
  );
};

export default SpaceActivities;
