import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import ShowBoards from '../Tools/ShowBoards';
import useGet from 'hooks/useGet';

interface Props {
  description: string;
  shortDescription: string;
  status: string;
  spaceId: number;
  isAdmin: boolean;
}

const SpaceAbout: FC<Props> = ({ description, status, spaceId }) => {
  const { t } = useTranslation('common');
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/spaces/${spaceId}/links`);

  return (
    <>
      {/* space short intro */}
      {/* <H2>{t('space.home.shortIntro')}</H2>
      <P fontSize="17.5px">{shortDescription}</P> */}
      {/* space full description */}
      {/* <H2 pt={5}>{t('general.tab.about')}</H2> */}
      <div tw="max-w-3xl pb-4 md:pb-0">
        {/* <div tw="md:(background[#f3f4f699] rounded p-6 max-width[76ch])"> */}
        <InfoHtmlComponent content={description} />
        <hr />
        {/* display space dates info, and status */}
        {/* <Box pt={2} row>
          <strong>{t('attach.status')}</strong>&nbsp;
          {t(`entity.info.status.${status}`)}
        </Box> */}
        {/* Social medias */}
        {dataExternalLink && dataExternalLink?.length !== 0 && (
          <div tw="flex flex-wrap gap-x-3 mt-6">
            {[...dataExternalLink].map((link, i) => (
              <a tw="items-center" key={i} href={link.url} target="_blank">
                <img tw="w-10 hover:opacity-80" src={link.icon_url} />
              </a>
            ))}
          </div>
        )}
        {/* Boards */}
        <div tw="mt-10 mb-3">
          <ShowBoards objectType="spaces" objectId={spaceId} />
        </div>
      </div>
    </>
  );
};
export default SpaceAbout;
