import useTranslation from 'next-translate/useTranslation';
import React, { useState } from 'react';
import useUserData from 'hooks/useUserData';
import { Need } from 'types';
import Box from '../Box';
import Grid from '../Grid';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';

// const OverflowGradient = styled.div`
//   ${layout};
//   width: 3rem;
//   height: 100%;
//   position: absolute;
//   right: 0;
//   background: ${(p) =>
//     `linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`};
// `;

const SpaceNeeds = ({ spaceId }) => {
  const needsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  const [needsEndpoints, setNeedsEndpoint] = useState(`/api/spaces/${spaceId}/needs?items=${needsPerQuery}`);
  // const [selectedProgramFilterId, setSelectedProgramFilterId] = useState(undefined);
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const {
    data: dataNeeds,
    response,
    error,
    size,
    setSize,
  } = useInfiniteLoading<{
    needs: Need[];
  }>((index) => `${needsEndpoints}&page=${index + 1}`);

  const needs = dataNeeds ? [].concat(...dataNeeds?.map((d) => d.needs)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataNeeds && !error;
  const isLoadingMore = isLoadingInitialData || (size > 0 && dataNeeds && typeof dataNeeds[size - 1] === 'undefined');
  const isEmpty = dataNeeds?.[0]?.length === 0;
  const isReachingEnd = isEmpty || needs?.length === totalNumber;

  // const onFilterChange = (e) => {
  //   const id = e.target.name;
  //   const itemsNb = needs.length > needsPerQuery ? needs.length : needsPerQuery;
  //   if (id) {
  //     setSelectedProgramFilterId(Number(id));
  //     setNeedsEndpoint(`/api/programs/${id}/needs?items=${itemsNb}`);
  //   } else {
  //     setSelectedProgramFilterId(undefined);
  //     setNeedsEndpoint(`/api/spaces/${spaceId}/needs?items=${needsPerQuery}`);
  //   }
  // };

  return (
    <div>
      <Box>
        {!userData && ( // if user is not connected
          <A href="/signIn">
            {t('header.signIn')} {t('program.signinCta.need')}
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        {/* <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel="challenge.list_all"
            content={dataPrograms?.program
              // filter to hide draft programs
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!dataProgramsError}
            errorMessage="*Could not get programs filters"
            selectedId={selectedProgramFilterId}
          />
        </Box> */}
        {!dataNeeds ? (
          <Loading />
        ) : needs.length === 0 ? (
          <NoResults type="need" />
        ) : (
          <>
            <p tw="italic">{t('general.attached_needs_explanation')}</p>
            <Grid tw="py-4">
              {needs?.map((need, i) => (
                <NeedCard
                  key={i}
                  title={need.title}
                  project={need.project}
                  skills={need.skills}
                  resources={need.ressources}
                  hasSaved={need.has_saved}
                  id={need.id}
                  postsCount={need.posts_count}
                  membersCount={need.members_count}
                  publishedDate={need.created_at}
                  dueDate={need.end_date}
                  status={need.status}
                />
              ))}
            </Grid>
          </>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          totalNumber > needsPerQuery && size <= response?.[0].headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default SpaceNeeds;
