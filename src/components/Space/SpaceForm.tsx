import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormWysiwygComponent from 'components/Tools/Forms/FormWysiwygComponent';
import FormDropdownComponent from 'components/Tools/Forms/FormDropdownComponent';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import FormToggleComponent from '../Tools/Forms/FormToggleComponent';
import H2 from '../primitives/H2';
import { Space } from 'types';
import { useRouter } from 'next/router';
import { changesSavedConfAlert } from 'utils/utils';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import AllowPostingToAllToggle from '../Tools/AllowPostingToAllToggle';
// import "./SpaceForm.scss";

interface Props {
  mode: string;
  space: Space;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key: any, content: any) => void;
  handleChangeFeatured?: (key: any, content: any) => void;
  handleChangeShowTabs?: (key: any, content: any) => void;
  handleSubmit: () => void;
}

const SpaceForm: FC<Props> = ({
  mode,
  space,
  sending = false,
  hasUpdated = false,
  handleChange,
  // handleChangeFeatured,
  handleChangeShowTabs,
  handleSubmit,
}) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const urlBack = mode === 'create' ? '/' : `/space/${space.short_title}`;
  const textAction = mode === 'create' ? 'Create' : 'Update';

  // show conf message when project has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to project page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));

  return (
    <form className="spaceForm">
      {/* Basic fields */}
      {(mode === 'edit' || mode === 'create') && (
        <>
          <FormDefaultComponent
            id="title"
            content={space.title}
            title={t('entity.info.title')}
            placeholder={t('space.form.title_placeholder')}
            onChange={handleChange}
            mandatory
          />
          <FormDefaultComponent
            id="short_title"
            content={space.short_title}
            title={t('entity.info.short_name')}
            placeholder={t('space.form.short_title_placeholder')}
            onChange={handleChange}
            prepend="#"
            mandatory
            pattern={/[A-Za-z0-9]/g}
          />
          <FormTextAreaComponent
            content={space.short_description}
            id="short_description"
            maxChar={500}
            onChange={handleChange}
            rows={3}
            title={t('entity.info.short_description')}
            placeholder={t('space.form.short_description_placeholder')}
            mandatory
          />
        </>
      )}
      {(mode === 'edit_about' || mode === 'create') && (
        <FormWysiwygComponent
          id="description"
          content={space.description}
          title={t('entity.info.description')}
          placeholder={t('space.form.description_placeholder')}
          onChange={handleChange}
          show
        />
      )}

      {/* <FormDefaultComponent
        id="home_header"
        content={space.home_header}
        title={t('space.form.home_header')}
        placeholder={t('space.form.home_header_placeholder')}
        onChange={handleChange}
      />
      <FormWysiwygComponent
        id="home_info"
        content={space.home_info}
        title={t('space.form.home_info')}
        placeholder={t('space.form.home_info_placeholder')}
        onChange={handleChange}
        show
      /> */}
      {/* {mode === 'edit_partners' && (
        <FormWysiwygComponent
          id="enablers"
          content={space.enablers}
          title={t('entity.info.enablers')}
          placeholder={t('space.form.meeting_information_placeholder')}
          onChange={handleChange}
          show
        />
      )} */}
      {mode === 'edit' && (
        <>
          <FormImgComponent
            id="banner_url"
            content={space.banner_url}
            title={t('space.info.banner_url')}
            imageUrl={space.banner_url}
            itemId={space.id}
            type="banner"
            itemType="spaces"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChange}
            tooltipMessage={t('challenge.info.banner_url_tooltip')}
          />
          <FormImgComponent
            id="logo_url"
            content={space.logo_url}
            title={t('space.info.logo_url')}
            imageUrl={space.logo_url}
            itemId={space.id}
            type="avatar"
            itemType="spaces"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChange}
          />
          <FormDropdownComponent
            id="space_type"
            content={space.space_type}
            title={t('space.form.space_type')}
            // prettier-ignore
            options={['digital_community', 'local_community', 'ngo', 'not_for_profit_organization', 'startup',
            'maker_space','community_lab','company', 'social_enterprise', 'school', 'foundation', 'academic_lab',
            'professional_network', 'public_agency', 'public_institution', 'incubator', 'living_lab', 'fund', 'other']}
            onChange={handleChange}
            mandatory
          />
          <FormDropdownComponent
            id="status"
            content={space.status}
            title={t('entity.info.status.title')}
            options={['draft', 'active']}
            onChange={handleChange}
          />
          <AllowPostingToAllToggle feedId={space.feed_id} />
          <FormToggleComponent
            id="show_associated_users"
            title={t('space.form.show_associated_users')}
            warningMsg={t('space.form.show_associated_users_msg')}
            choice1={t('general.no')}
            choice2={t('general.yes')}
            isChecked={space.show_associated_users}
            onChange={handleChange}
          />
          <FormToggleComponent
            id="show_associated_projects"
            title={t('space.form.show_associated_projects')}
            warningMsg={t('space.form.show_associated_projects_msg')}
            choice1={t('general.no')}
            choice2={t('general.yes')}
            isChecked={space.show_associated_projects}
            onChange={handleChange}
          />
          {/* <FormDefaultComponent
            id="contact_email"
            content={space.contact_email}
            title={t('space.form.contact_email')}
            placeholder={t('space.form.contact_email_placeholder')}
            onChange={handleChange}
            mandatory
          /> */}
          {/* Optional spaces tabs */}
          <H2 pt={6}>Optional spaces tabs</H2>
          <div tw="flex flex-wrap gap-x-8 pb-5">
            <FormToggleComponent
              id="selected_tabs.challenges"
              title={t('general.activities')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={space.selected_tabs.challenges}
              onChange={handleChangeShowTabs}
            />
            <FormToggleComponent
              id="selected_tabs.resources"
              title={t('entity.info.resources')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={space.selected_tabs.resources}
              onChange={handleChangeShowTabs}
            />
            <FormToggleComponent
              id="selected_tabs.enablers"
              title={t('entity.info.enablers')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={space.selected_tabs.enablers}
              onChange={handleChangeShowTabs}
            />
            <FormToggleComponent
              id="selected_tabs.faqs"
              title={t('entity.info.faq')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={space.selected_tabs.faqs}
              onChange={handleChangeShowTabs}
            />
          </div>
        </>
      )}
      {/* Home tab fields */}
      {/* {mode === 'edit_home' && (
        <>
          // <H2>*Show featured objects</H2> --> change to featured when we will be able to select featured objects in admin panel
          <H2>Show latest objects in homepage</H2>
          <div tw="flex flex-wrap gap-x-8 pb-5">
            <FormToggleComponent
              id="show_featured.challenges"
              title={t('general.challenges')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={space.show_featured.challenges}
              onChange={handleChangeFeatured}
            />
            //
            <FormToggleComponent
              id="show_featured.projects"
              title={t('user.profile.tab.projects')}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={space.show_featured.projects}
              onChange={handleChangeFeatured}
            />
            //
            <FormToggleComponent
              id="show_featured.needs"
              title={t('general.need', { count: 2 })}
              choice1={t('general.no')}
              choice2={t('general.yes')}
              isChecked={space.show_featured.needs}
              onChange={handleChangeFeatured}
            />
          </div>
        </>
      )} */}
      <div tw="text-center mt-5 mb-3">
        <Button onClick={handleSubmit} disabled={sending}>
          {sending && <SpinLoader />}
          {t(`entity.form.btn${textAction}`)}
        </Button>
      </div>
    </form>
  );
};
export default SpaceForm;
