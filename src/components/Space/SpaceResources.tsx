import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Box from 'components/Box';
import H2 from 'components/primitives/H2';
import useGet from 'hooks/useGet';
import { Document } from 'types';
import DocumentsList from '../Tools/Documents/DocumentsList';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';

interface Props {
  documents: Document[];
  isAdmin: boolean;
  spaceId: number;
  resources: any;
}
const SpaceResources: FC<Props> = ({ spaceId, isAdmin, documents, resources }) => {
  const { t } = useTranslation('common');
  return (
    <Box>
      {!resources ? (
        <Loading />
      ) : resources?.length === 0 ? (
        ''
      ) : (
        <>
          <H2>{t('program.resources.title')}</H2>
          <Box spaceY={7}>
            {[...resources].map((resource, i) => (
              <Box>
                <H2 px={[3, 4]} pb={2}>
                  {resource.title}
                </H2>
                <Box bg="white" px={[3, 4]}>
                  <InfoHtmlComponent content={resource.content} />
                </Box>
              </Box>
            ))}
          </Box>
        </>
      )}
      {documents.length !== 0 && (
        <>
          <H2 pt={6}>{t('entity.tab.documents')}</H2>
          <DocumentsList
            documents={documents}
            itemId={spaceId}
            isAdmin={isAdmin}
            itemType="spaces"
            cardType="cards"
            showDeleteIcon={false}
          />
        </>
      )}
    </Box>
  );
};

export default SpaceResources;
