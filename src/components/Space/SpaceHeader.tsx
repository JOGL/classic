/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC, useEffect, useRef, useState } from 'react';
import { display, flexbox, layout, space } from 'styled-system';
import H1 from 'components/primitives/H1';
import { Members, Space } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import { renderTeam, useScrollHandler } from 'utils/utils';
import A from '../primitives/A';
import BtnJoin from '../Tools/BtnJoin';
import BtnStar from '../Tools/BtnStar';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import { styled } from 'twin.macro';
import BasicChip from '../BasicChip';
import BtnFollow from '../Tools/BtnFollow';

interface Props {
  space: Space;
  lang: string;
  members: Members;
}
const SpaceHeader2: FC<Props> = ({ space, lang = 'en' }) => {
  const router = useRouter();
  const [offSetTop, setOffSetTop] = useState();
  const headerRef = useRef();
  const isSticky = useScrollHandler(offSetTop);
  // const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/spaces/${space?.id}/links`);
  const { t } = useTranslation('common');

  useEffect(() => {
    setOffSetTop(headerRef?.current?.offsetTop);
  }, [headerRef]);

  const Stats = ({ value, title }) => (
    <div tw="justify-center items-center">
      <div>{value}</div>
      <div>{title}</div>
    </div>
  );

  const {
    id,
    logo_url,
    members_count,
    challenges_count,
    projects_count,
    followers_count,
    saves_count,
    // needs_count,
    title,
    title_fr,
    short_title,
    is_admin,
    is_member,
    is_owner,
    is_pending,
    has_followed,
    has_saved,
    status,
    short_description,
  } = space;

  const LogoImgComponent = () => (
    <Img
      src={logo_url || 'images/jogl-logo.png'}
      style={{ objectFit: 'contain', backgroundColor: 'white' }}
      mr={4}
      ml={[undefined, undefined, 4]}
      mb={[undefined, undefined, 2]}
      width={['5rem', '7rem', '9rem', '10rem']}
      height={['5rem', '7rem', '9rem', '10rem']}
      alignSelf="center"
      alt="space logo"
      mt={[0, undefined, '-90px', '-100px']}
    />
  );

  return (
    <div tw="bg-white grid grid-cols-1 md:grid-template-columns[20rem calc(100% - 20rem)] xl:grid-template-columns[25% 75%]">
      <div tw="flex mt-0 flex-wrap order-2 md:order-1 lg:items-center" ref={headerRef}>
        <div tw="w-full py-4 justify-center">
          <div tw="flex-col hidden justify-center md:(flex flex-row)">
            <LogoImgComponent />
          </div>
          <div tw="text-center items-center justify-center space-x-6 w-full inline-flex pb-2 px-0 md:(px-4 mt-6 space-x-5)">
            <A href={`/space/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
              <Stats value={projects_count} title={<TextWithPlural type="project" count={projects_count} />} />
            </A>
            <div tw="border-right[2px solid] border-gray-300 h-9" />
            <A href={`/space/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
              <Stats value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
            </A>
            <div tw="border-right[2px solid] border-gray-300 h-9" />
            <A href={`/space/${router.query.short_title}?tab=activities`} shallow noStyle scroll={false}>
              <Stats value={challenges_count} title={<TextWithPlural type="activity" count={challenges_count} />} />
            </A>
          </div>
          {/* heading with minimal infos, that sticks on top of screen, when you reached space logo */}
          <StickyHeading isSticky={isSticky} className="stickyHeading" tw="bg-white">
            <div tw="inline-flex items-center px-4 xl:px-0">
              <LogoImg src={space?.logo_url || 'images/jogl-logo.png'} alt={`${space?.title} logo`} />
              <H1 fontSize={['1.8rem', '2.18rem']} lineHeight="26px" pl="2">
                {(lang === 'fr' && space?.title_fr) || space?.title}
              </H1>
              <div tw="pl-5" className="actions">
                <ShareBtns type="space" specialObjId={id} />
              </div>
            </div>
          </StickyHeading>
        </div>
        <div tw="flex flex-wrap mt-2 gap-x-2 gap-y-2 mb-4 self-center w-full justify-center md:mb-0">
          {!is_owner && (status !== 'completed' || (status === 'completed' && is_member)) && (
            // show join button only if we are not owner, or if status is different from "completed" (+ prevent user to join space if the space is set as completed)
            <BtnJoin
              joinState={is_pending ? 'pending' : is_member}
              itemType="spaces"
              itemId={id}
              isPrivate
              count={members_count}
              showMembersModal={() =>
                router.push(`/space/${router.query.short_title}?tab=members`, undefined, { shallow: true })
              }
            />
          )}
          {is_member && <BtnFollow followState={has_followed} itemType="spaces" itemId={id} count={followers_count} />}
          <BtnStar itemType="spaces" itemId={id} hasStarred={has_saved} count={saves_count} />
        </div>
        <div tw="mb-0 pt-3 font-size[17.5px] text-align[justify] m-auto md:hidden">{short_description}</div>
      </div>
      {/* ------- grid separation --------- */}
      <div tw="pl-0 py-3 order-1 md:(order-2 pl-6)">
        <div tw="w-full flex-col justify-center pr-0 md:(justify-between pr-5)">
          <div tw="flex flex-wrap justify-center md:justify-start">
            <div tw="flex">
              <div tw="block md:hidden">
                <LogoImgComponent />
              </div>
              <div tw="flex flex-wrap items-center">
                <H1 fontSize={['2.18rem', '2.3rem', '2.5rem', '2.78rem']} pr={3} pb="2">
                  {(lang === 'fr' && title_fr) || title}
                </H1>
                {is_admin && (
                  <Link href={`/space/${short_title}/edit`} passHref>
                    <a tw="flex justify-center mr-4 pb-1">
                      <Edit size={18} title="Edit space" />
                      {t('entity.form.btnAdmin')}
                    </a>
                  </Link>
                )}
              </div>
            </div>
            <div tw="flex flex-wrap pb-2 items-center">
              <ShareBtns type="space" specialObjId={id} />
              {/* Social medias */}
              {/* {dataExternalLink && dataExternalLink?.length !== 0 && (
                    <div tw="flex flex-wrap gap-x-3 mx-6">
                      {[...dataExternalLink].map((link, i) => (
                        <a tw="items-center" key={i} href={link.url} target="_blank">
                          <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                        </a>
                      ))}
                    </div>
                  )} */}
              {/* display team members profile imgs */}
              {/* {renderTeam(members?.slice(0, 6), 'space', space?.short_title, members_count, 'internal')} */}
            </div>
          </div>
          {space.space_type && (
            <BasicChip tw="flex mx-auto self-center mt-3 md:(self-start m-0)">
              {t(`space.type.${space.space_type}`)}
            </BasicChip>
          )}
        </div>
        <div tw="mb-0 mt-7 font-size[17.5px] text-align[justify] hidden md:block">{short_description}</div>
      </div>
    </div>
  );
};

const Img = styled.img`
  ${[flexbox, space, display, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
`;

const LogoImg = styled.img`
  ${[flexbox, space, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
  width: 50px;
  height: 50px;
`;

const StickyHeading = styled.div`
  position: fixed;
  display: flex;
  height: 70px;
  top: ${(p) => (p.isSticky ? '64px' : '-1000px')};
  width: 100%;
  z-index: 9;
  border-top: 1px solid grey;
  left: 0;
  transition: top 333ms;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    height: 60px;
    .actions {
      display: none;
    }
    img {
      width: 40px;
      height: 40px;
    }
  }
  > div {
    max-width: 1280px;
    margin: 0 auto;
    width: 100%;
  }
`;
export default SpaceHeader2;
