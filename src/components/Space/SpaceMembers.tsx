import useTranslation from 'next-translate/useTranslation';
import React, { useState } from 'react';
import QuickSearchBar from 'components/Tools/QuickSearchBar';
// import useChallenges from 'hooks/useChallenges';
import Router from 'next/router';
import useUserData from 'hooks/useUserData';
// import styled from 'utils/styled';
// import Filters from '../Filters';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import SpinLoader from '../Tools/SpinLoader';
import UserCard from '../User/UserCard';
import useInfiniteLoading from 'hooks/useInfiniteLoading';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import FakeLink from '../primitives/FakeLink';
import Box from '../Box';
import H2 from 'components/primitives/H2';
import InviteMember from '../Tools/InviteMember';

const SpaceMembers = ({ spaceId, isMember, isAdmin }) => {
  const membersPerQuery = 24; // number of members we get per query calls (make it 3 to test locally)
  const [membersEndpoints, setMembersEndpoint] = useState(
    `/api/spaces/${spaceId}/people?items=${membersPerQuery}&sort=roles`
  );
  // const { dataChallenges, challengesError } = useChallenges('spaces', spaceId);
  // const [selectedChallengeFilterId, setSelectedChallengeFilterId] = useState(undefined);
  const { userData } = useUserData();
  const api = useApi();
  const { t } = useTranslation('common');
  const {
    data: dataMembers,
    response,
    error,
    size,
    setSize,
  } = useInfiniteLoading((index) => `${membersEndpoints}&page=${index + 1}`);

  const members = dataMembers ? [].concat(...dataMembers?.map((d) => d.people)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataMembers && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataMembers && typeof dataMembers[size - 1] === 'undefined');
  const isEmpty = dataMembers?.[0]?.length === 0;
  const isReachingEnd = isEmpty || members?.length === totalNumber;

  const affiliationRoute = `/api/users/${userData?.id}/affiliations/spaces/${spaceId}`;
  const { data: affiliationStatus } = useGet(affiliationRoute);
  const requestUserAffiliation = () => {
    api.post(affiliationRoute).then(() => alert('Your affiliation request is pending.'));
  };
  const removeAffiliation = (wasPending: boolean) => {
    api.delete(affiliationRoute).then(() => {
      alert(wasPending ? 'Your request has been cancelled.' : 'Your are not affiliated to the space anymore');
      Router.reload(window.location.pathname);
    });
  };

  // const onFilterChange = (e) => {
  //   const id = e.target.name;
  //   const itemsNb = members.length > membersPerQuery ? members.length : membersPerQuery;
  //   if (id) {
  //     // if we select one of the challenge
  //     setSelectedChallengeFilterId(Number(id));
  //     setMembersEndpoint(`/api/challenges/${id}/members?items=${itemsNb}`); // get members from selected challenge instead of space
  //     setHasLoadOnce(false); // set to false so it set new members of only selected challenge
  //   } else {
  //     // if we select "All challenges"
  //     setSelectedChallengeFilterId(undefined);
  //     setMembersEndpoint(`/api/programs/${spaceId}/members?items=${itemsNb}`); // default end point, getting default number of members from space api
  //   }
  // };

  // const OverflowGradient = styled.div`
  //   ${layout};
  //   width: 3rem;
  //   height: 100%;
  //   position: absolute;
  //   right: 0;
  //   ${(p) =>
  //     `background: linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`}
  // `;
  return (
    <>
      {!userData && ( // if user is not connected
        <Box spaceX={2} pb={4}>
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.members')}
          </A>
        </Box>
      )}
      <Box position="relative">
        {/* Filters of members by space/challenges */}
        {/* <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel="challenge.list_all"
            content={dataChallenges
              // filter to hide draft challenges
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={challengesError}
            errorMessage="Could not get challenges filters"
            selectedId={selectedChallengeFilterId}
          />
        </Box> */}
        {/* Members grid/list */}
        <span>
          {isMember && !affiliationStatus && <FakeLink onClick={requestUserAffiliation}>Request affiliation</FakeLink>}
          {isMember && affiliationStatus?.status === 'pending' && 'Your affiliation request is pending. '}
          {isMember && affiliationStatus?.status === 'accepted' && 'You are affiliated to this space. '}
          {isMember && affiliationStatus && (
            <FakeLink onClick={() => removeAffiliation(affiliationStatus?.status === 'pending')}>
              {affiliationStatus?.status === 'accepted' ? 'Remove my affiliation' : 'Cancel request'}
            </FakeLink>
          )}
        </span>
        {members && (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {
              // list
              members.length === 0 ? (
                <Loading />
              ) : (
                <Grid tw="py-4">
                  {members?.map((member, i) => (
                    <UserCard
                      key={i}
                      id={member.id}
                      firstName={member.first_name}
                      lastName={member.last_name}
                      nickName={member.nickname}
                      shortBio={member.short_bio}
                      skills={member.skills}
                      resources={member.ressources}
                      status={member.status}
                      lastActive={member.current_sign_in_at}
                      logoUrl={member.logo_url}
                      hasFollowed={member.has_followed}
                      affiliation={member.affiliation}
                      mutualCount={member.stats.mutual_count}
                      projectsCount={member.stats.projects_count}
                      followersCount={member.stats.followers_count}
                      spacesCount={member.stats.spaces_count}
                      role={member.relation}
                      showRelationNotRole
                    />
                  ))}
                </Grid>
              )
            }
            {/* show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page */}
            {totalNumber > membersPerQuery && size <= response?.[0].headers?.['total-pages'] && (
              <Box alignSelf="center" pt={4}>
                <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                  {isLoadingMore && <SpinLoader />}
                  {isLoadingMore
                    ? t('general.loading')
                    : !isReachingEnd
                    ? t('general.load')
                    : t('general.noMoreResults')}
                </Button>
              </Box>
            )}
          </>
        )}
        {/* Invite someone component */}
        {isAdmin && (
          <Box mt={8}>
            <H2>{t('member.invite.general')}</H2>
            <InviteMember itemType="spaces" itemId={spaceId} />
          </Box>
        )}
      </Box>
    </>
  );
};

export default SpaceMembers;
