import React, { useState, FC, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import Alert from '../Tools/Alert';
import Button from '../primitives/Button';
import Loading from '../Tools/Loading';
import SpinLoader from '../Tools/SpinLoader';
import { displayObjectDate } from 'utils/utils';

interface PropsModal {
  alreadyPresentEvents: any[];
  callBack: () => void;
  closeModal: () => void;
  serviceId: number;
}
export const SpaceAddEventsModal: FC<PropsModal> = ({ alreadyPresentEvents, callBack, closeModal, serviceId }) => {
  const { data: dataEvents, revalidate: revalidateEvents } = useGet(`/api/services/${serviceId}/flows/events`);
  const events = dataEvents?.data && Object.values(dataEvents?.data)?.[0];
  const [eventsIds, setEventsIds] = useState([]);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');

  // refetch events when modal is launched
  useEffect(() => {
    api.get(`api/services/${serviceId}`).then(() => {
      api.put(`/api/services/${serviceId}/flows/events`).then(() => {
        revalidateEvents();
      });
    });
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    const params = {
      opts: {
        ids: eventsIds,
      },
    };
    setSending(true);
    // Link this project to the challenge then mutate the cache of the projects from the parent prop
    await api.post(`/api/services/${serviceId}/flows/events`, params).catch(() => console.error(`Error`));
    // mutateEvents({ projects: [...alreadyPresentProjects, selectedProject] });
    callBack();
    setTimeout(() => {
      // close modal after 1.5sec
      closeModal();
    }, 1500);
  };

  const handleChange = (content) => {
    const tempEventsIds = content.target.checked
      ? [...eventsIds, content.target.value]
      : eventsIds.filter((event) => event !== content.target.value);
    setEventsIds(tempEventsIds);
  };

  return (
    <div>
      {!events ? (
        <Loading />
      ) : events?.length > 0 ? (
        <form tw="text-left">
          <div tw="text-left divide-x-0 border-0 divide-y divide-gray-400 divide-solid">
            {events
              // don't show events that were already added
              .filter((event) => !alreadyPresentEvents.includes(event.id))
              // reverse to show last events first
              // .reverse()
              .map((event, index) => {
                return (
                  <div className="form-check" key={index} tw="height[50px] pt-4">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      name="exampleRadios"
                      id={`event-${index}`}
                      value={event.id}
                      onChange={handleChange}
                    />
                    <label className="form-check-label" htmlFor={`event-${index}`}>
                      {event.name.text}{' '}
                      <span tw="text-gray-500">({displayObjectDate(event.start.local, 'LL', true)})</span>
                    </label>
                  </div>
                );
              })}
          </div>
          <div className="btnZone">
            <Button type="submit" disabled={eventsIds.length === 0 || sending} onClick={onSubmit} tw="mb-2">
              <>
                {sending && <SpinLoader />}
                {t('general.add')}
              </>
            </Button>
            {requestSent && <Alert type="success" message={t('attach.project.success')} />}
          </div>
        </form>
      ) : (
        <div className="noProject" style={{ textAlign: 'center' }}>
          {t('attach.project.noProject')}
        </div>
      )}
    </div>
  );
};
