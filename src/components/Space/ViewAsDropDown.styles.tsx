import isPropValid from '@emotion/is-prop-valid';
import { MenuButton, MenuList } from '@reach/menu-button';
import { typography } from 'styled-system';
import styled from 'utils/styled';

export const DropDownMenu = styled(MenuList)`
  background-color: white;
  border: 1px solid black;
  border-radius: ${(p) => p.theme.radii.default};
  box-shadow: ${(p) => p.theme.shadows.xl};
  border: 1px solid ${(p) => p.theme.colors.greys['200']};
  padding: 0px;
  font-size: 1rem;
  * + * {
    ${(p) => `border-top: 1px solid ${p.theme.colors.greys['200']}`};
  }
  a {
    cursor: pointer;
    transition: opacity 0.1s ease-in-out;
    &:hover {
      text-decoration: none;
    }
  }
`;

export const StyledMenuButton = styled(MenuButton, {
  shouldForwardProp: (prop) => isPropValid(prop),
})`
  ${typography};
  background: none;
  color: 'rgba(0, 0, 0, 0.7)';
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  display: inline-block;
  text-align: left;
  ${(p) => p.uppercase && 'text-transform: uppercase'};
  &:hover {
    color: black;
  }
`;
