/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import { useRouter } from 'next/router';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStarIcon from 'components/Tools/BtnStarIcon';
import { TextWithPlural } from 'utils/managePlurals';

interface Props {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  membersCount?: number;
  projectsCount?: number;
  needsCount?: number;
  has_saved?: boolean;
  banner_url: string;
  width?: string;
  source?: 'algolia' | 'api';
  cardFormat?: string;
  spaceType: string;
}
const SpaceCard = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  membersCount,
  projectsCount,
  needsCount,
  has_saved,
  banner_url = '/images/default/default-space.jpg',
  width,
  source,
  cardFormat,
  spaceType,
}: Props) => {
  const router = useRouter();
  const { locale } = router;
  const spaceUrl = `/space/${short_title}`;
  return (
    // <ObjectCard imgUrl={banner_url} href={spaceUrl} width={width} chip={spaceType}>
    <ObjectCard imgUrl={banner_url} href={spaceUrl} width={width}>
      {/* Title */}
      <div tw="inline-flex items-center md:h-16">
        <Link href={spaceUrl} passHref>
          <Title>
            <H2 tw="word-break[break-word] text-2xl line-clamp-2 items-center md:text-3xl">
              {(locale === 'fr' && title_fr) || title}
            </H2>
          </Title>
        </Link>
      </div>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <Hr tw="mt-2 pt-4" />
      {/* Description */}
      <div tw="line-clamp-6 flex-1 color[#343a40]">
        {(locale === 'fr' && short_description_fr) || short_description}
      </div>
      {/* Stats */}
      {cardFormat !== 'compact' && (
        <>
          <Hr tw="mt-5 pt-2" />
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
            <CardData value={needsCount} title={<TextWithPlural type="need" count={needsCount} />} />
            <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
            {/* <CardData value={clapsCount} title={<TextWithPlural type="clap" count={clapsCount}/>} /> */}
          </div>
        </>
      )}
      {/* Star icon */}
      {(has_saved !== undefined || source === 'algolia') && (
        <BtnStarIcon itemType="spaces" itemId={id} saveState={has_saved} source={source} />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-extrabold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default SpaceCard;
