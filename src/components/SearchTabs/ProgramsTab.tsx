import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const ProgramsTab: React.FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="Program"
        onSearchStateChange={setSearchState}
        refinements={[{ attribute: 'status', searchable: false, showMore: false }]}
        sortByItems={[
          { label: t('general.filter.newly_updated'), index: 'Program' },
          { label: t('general.filter.object.date2'), index: 'Program_id_des' },
          { label: t('general.filter.object.date1'), index: 'Program_id_asc' },
        ]}
      />
    </TabPanel>
  );
};
