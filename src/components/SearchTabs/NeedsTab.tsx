import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const NeedsTab: React.FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="Need"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'skills' },
          { attribute: 'ressources' },
          { attribute: 'project.title' },
          { attribute: 'status', searchable: false, showMore: false },
        ]}
        sortByItems={[
          { label: t('general.filter.newly_updated'), index: 'Need_updated_at' },
          { label: t('general.filter.object.date2'), index: 'Need_id_des' },
          { label: t('general.filter.object.date1'), index: 'Need_id_asc' },
          { label: t('general.filter.pop'), index: 'Need' },
          { label: t('general.star'), index: 'Need_saves_desc' },
          { label: t('general.member', { count: 1 }), index: 'Need_members' },
        ]}
      />
    </TabPanel>
  );
};
