import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const MembersTab: React.FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();
  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="User"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'skills' },
          { attribute: 'ressources' },
          { attribute: 'interests', searchable: false, limit: 17 },
        ]}
        sortByItems={[
          // {label: t('general.filter.newly_updated'),index: 'User_updated_at'},
          { label: t('general.filter.people.date2'), index: 'User_id_des' },
          { label: t('general.filter.pop'), index: 'User' },
          { label: t('general.filter.people.date1'), index: 'User_id_asc' },
          { label: t('general.filter.people.alpha1'), index: 'User_fname_asc' },
          { label: t('general.filter.people.alpha2'), index: 'User_fname_desc' },
        ]}
      />
    </TabPanel>
  );
};
