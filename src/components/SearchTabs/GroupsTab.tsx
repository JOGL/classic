import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const GroupsTab: React.FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="Community"
        onSearchStateChange={setSearchState}
        refinements={[{ attribute: 'skills' }, { attribute: 'ressources' }]}
        sortByItems={[
          { label: t('general.filter.newly_updated'), index: 'Community_updated_at' },
          { label: t('general.filter.object.date2'), index: 'Community_id_des' },
          { label: t('general.filter.pop'), index: 'Community' },
          { label: t('general.member', { count: 1 }), index: 'Community_members' },
          { label: t('general.star'), index: 'Community_saves_desc' },
          { label: t('general.filter.object.date1'), index: 'Community_id_asc' },
          { label: t('general.filter.object.alpha1'), index: 'Community_title_asc' },
          { label: t('general.filter.object.alpha2'), index: 'Community_title_desc' },
        ]}
      />
    </TabPanel>
  );
};
