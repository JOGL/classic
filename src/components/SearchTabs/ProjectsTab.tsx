import React from 'react';
import useTranslation from 'next-translate/useTranslation';
import Search from 'components/Search/Search';
import { useSearchStateContext } from 'contexts/searchStateContext';
import { TabPanel } from '@reach/tabs';

export const ProjectsTab: React.FC = () => {
  const { t } = useTranslation('common');
  const { index, searchState, setSearchState } = useSearchStateContext();

  return (
    <TabPanel id={index}>
      <Search
        searchState={searchState}
        index="Project"
        onSearchStateChange={setSearchState}
        refinements={[
          { attribute: 'reviews_count', searchable: false, showMore: false, type: 'toggle' },
          { attribute: 'status', searchable: false, showMore: false },
          { attribute: 'maturity', searchable: false, showMore: false },
          { attribute: 'skills' },
          { attribute: 'interests', searchable: false, limit: 17 },
          { attribute: 'programs.title', searchable: false },
          { attribute: 'challenges.title' },
        ]}
        sortByItems={[
          { label: t('general.filter.newly_updated'), index: 'Project_updated_at' },
          { label: t('general.filter.object.date2'), index: 'Project_id_des' },
          { label: t('general.filter.pop'), index: 'Project' },
          { label: t('general.star'), index: 'Project_saves_desc' },
          { label: t('general.need', { count: 2 }), index: 'Project_needs' },
          { label: t('general.member', { count: 2 }), index: 'Project_members' },
          { label: t('general.filter.object.date1'), index: 'Project_id_asc' },
          { label: t('general.filter.object.alpha1'), index: 'Project_title_asc' },
          { label: t('general.filter.object.alpha2'), index: 'Project_title_desc' },
        ]}
      />
    </TabPanel>
  );
};
