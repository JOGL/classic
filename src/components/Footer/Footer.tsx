import React from 'react';
import Link from 'next/link';
// import Image from 'next/image';
import useTranslation from 'next-translate/useTranslation';
import P from '../primitives/P';
import Image2 from '../Image2';
import Button from '../primitives/Button';
import { MediumSquare, Twitter, Facebook, Instagram, Linkedin, Gitlab } from '@emotion-icons/boxicons-logos';

// import "./Footer.scss";
const Footer = () => {
  const { t } = useTranslation('common');
  return (
    <footer id="footer">
      <div className="footer-top">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 col-sm-4 col-md-4 col-lg-2 footCol" style={{ marginTop: '0px' }}>
              <Image2 src="/images/logo_JOGL-02.png" alt="jogl logo rocket" quality={50} />
            </div>
            <div className="col-12 col-sm-8 col-lg-4 footCol">
              <h3>{t('footer.aboutJOGL')}</h3>
              <p tw="pb-3">{t('footer.JOGLdescription')}</p>
              <hr />
              <div tw="flex flex-col">
                {t('footer.donate.text')}
                {/* <a className="custom-dbox-popup" target="_blank" href="https://donorbox.org/donate-to-jogl">
                  <Button tw="mt-4">{t('footer.donate.btn')}</Button>
                </a> */}
                <Link href="/donate">
                  <a>
                    <Button tw="mt-4">{t('footer.donate.btn')}</Button>
                  </a>
                </Link>
              </div>
            </div>
            <div className="col-12 col-sm-4 col-md-4 col-lg-3 footer-contact footCol">
              <h3>{t('footer.general')}</h3>
              <p>
                <a href="https://jogl.io" target="_blank" rel="noopener">
                  {t('footer.aboutJOGL')}
                </a>
              </p>
              <p>
                <a href="https://jogl.tawk.help/" target="_blank" rel="noopener">
                  {t('faq.title')}
                </a>
              </p>
              <p>
                <Link href="/ethics-pledge">
                  <a>{t('footer.ethicsPledge')}</a>
                </Link>
              </p>
              <p>
                <Link href="/data">
                  <a>{t('footer.data')}</a>
                </Link>
              </p>
              <p>
                <Link href="/terms">
                  <a>{t('footer.termsConditions')}</a>
                </Link>
              </p>
              <p>
                <a href="https://gitlab.com/JOGL/JOGL" target="_blank" rel="noopener">
                  {t('footer.contribute')}
                </a>
              </p>
              <p>
                <a href="https://jogl.io/#jobs" target="_blank" rel="noopener">
                  {t('footer.jobs')}
                </a>
              </p>
            </div>
            <div className="col-12 col-sm-8 col-lg-3 footer-contact footCol">
              <h3>Contact</h3>
              <div className="d-flex flex-row">
                <p>Centre de Recherches Interdisciplinaires (CRI) - 8 bis rue Charles V, 75004, Paris</p>
              </div>
              <div className="contact">
                <a href="mailto:hello@jogl.io">
                  <Button>{t('footer.contactUs')}</Button>
                </a>
                <p className="press">
                  {t('footer.pressContact')}
                  <a href="mailto:press@jogl.io">press[at]jogl.io</a>
                </p>
              </div>
              <div tw="gap-3 flex flex-wrap mb-2">
                <a href="https://twitter.com/justonegiantlab" target="_blank" rel="noopener">
                  <Twitter size={30} title="Twitter icon" tw="text-gray-500 hover:color[#00aced]" />
                </a>
                <a href="https://www.facebook.com/justonegiantlab/" target="_blank" rel="noopener">
                  <Facebook size={30} title="Facebook icon" tw="text-gray-500 hover:color[#0077e2]" />
                </a>
                <a href="https://www.linkedin.com/company/jogl/about/" target="_blank" rel="noopener">
                  <Linkedin size={30} title="Linkedin icon" tw="text-gray-500 hover:color[#0277b5]" />
                </a>
                <a href="https://www.instagram.com/justonegiantlab/" target="_blank" rel="noopener">
                  <Instagram size={30} title="Instagram icon" tw="text-gray-500 hover:color[#c43670]" />
                </a>
                <a href="https://gitlab.com/JOGL/JOGL" target="_blank" rel="noopener">
                  <Gitlab size={30} title="Gitlab icon" tw="text-gray-500 hover:color[#fc6d26]" />
                </a>
                <a href="https://medium.com/justonegiantlab/latest" target="_blank" rel="noopener">
                  <MediumSquare size={30} title="medium icon" tw="text-gray-500 hover:text-black" />
                </a>
              </div>
              <P pt={4} fontSize="93%">
                {t('footer.bugOrRequest.text')}
                &nbsp;
                <a href="https://gitlab.com/JOGL/JOGL/-/issues/new" target="_blank" rel="noopener">
                  {t('footer.bugOrRequest.link')}
                </a>
              </P>
              <P fontSize="93%">
                {t('footer.whatNewChangelog.text')}
                &nbsp;
                <a
                  href="https://gitlab.com/JOGL/frontend-v0.1/-/blob/master/CHANGELOG.md"
                  target="_blank"
                  rel="noopener"
                >
                  {t('footer.whatNewChangelog.link')}
                </a>
              </P>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 col-sm-7 col-md-9 footer-bottom--copyright">
              <p>Just One Giant Lab - {new Date().getFullYear()} | v0.9.8.2</p>
              <p>{t('footer.CC')}</p>
            </div>
            <div className="col-12 col-sm-5 col-md-3 footer-bottom--right">
              <button type="button" className="btn btn-warning repport">
                <a href="mailto:support@jogl.io" target="_blank" rel="noopener">
                  {t('footer.help')}
                </a>
              </button>
              <a href="https://www.algolia.com/" target="_blank" rel="noopener">
                <Image2 src="/images/search-by-algolia.svg" alt="Search by algolia" width="168px" height="24px" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default React.memo(Footer);
