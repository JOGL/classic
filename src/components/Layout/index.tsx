import useTranslation from 'next-translate/useTranslation';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { ReactNode } from 'react';
import Footer from 'components/Footer/Footer';
import Header from 'components/Header/Header';

interface Props {
  title?: string;
  desc?: string;
  img?: string;
  className?: string;
  noHeaderFooter?: boolean;
  noIndex?: boolean;
  children?: ReactNode;
}
export default function Layout({
  title,
  desc = '',
  img,
  className,
  noHeaderFooter = false,
  noIndex = false,
  children,
}: Props) {
  const { t, lang } = useTranslation('common');
  const router = useRouter();
  const description = desc ? truncateString(desc, 150) : ''; // shorten object description to 150 characters maximum

  return (
    <>
      <Head>
        <title>{title || t('general.title')}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="theme-color" content="#ffffff" />
        <meta name="description" content={description || t('general.joglDesc')} />
        {/* Social media */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content={process.env.ADDRESS_FRONT + router.asPath} />
        <meta property="og:title" content={title || t('general.title')} />
        <meta property="og:image" content={img || `${process.env.ADDRESS_FRONT}/images/JOGL_banner_2021.png`} />
        <meta property="og:description" content={description || t('general.joglDesc')} />
        <meta property="og:site_name" content="JOGL - Just One Giant Lab" />
        <meta property="og:locale" content={lang} />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@justonegiantlab" />
        {/* if noIndex is true, don't index on search engines */}
        {noIndex && <meta name="robots" content="noindex,nofollow" />}
      </Head>
      {/* TODO implement user connection */}
      {!noHeaderFooter && <Header userConnected={false} />}
      <main className={className || 'main'} id="main">
        {children}
      </main>
      {!noHeaderFooter && <Footer />}
    </>
  );
}

const truncateString = (str, num) => {
  if (str.length <= num) {
    return str;
  }
  return str.slice(0, num) + '...';
};
