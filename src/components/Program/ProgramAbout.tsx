import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import React, { FC } from 'react';
import H2 from 'components/primitives/H2';
import styled from 'utils/styled';
import Box from '../Box';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import ShowBoards from '../Tools/ShowBoards';
import { displayObjectDate } from 'utils/utils';

interface Props {
  programId: number;
  enablers: string;
  description: string;
  launchDate: Date;
  status: string;
}

const ProgramAbout: FC<Props> = ({ programId, enablers, description, launchDate, status }) => {
  const { t } = useTranslation('common');

  return (
    <div tw="max-w-3xl">
      {/* program full description */}
      <InfoHtmlComponent content={description} />

      {/* supporters logos bloc */}
      <SupportersBloc display={['flex', undefined, undefined, undefined, 'none']} pt={6}>
        <H2>{t('program.supporters')}</H2>
        <InfoHtmlComponent content={enablers} />
        <a href="mailto:hello@jogl.io">{t('program.supporters_interested')}</a>
      </SupportersBloc>

      {/* display program dates info, and status */}
      <Box py={8}>
        <Box row>
          <Box pr={1}>
            <strong>{t('entity.info.launch_date')}:</strong>
          </Box>
          {launchDate ? displayObjectDate(launchDate, 'LL') : t('general.noDate')}
        </Box>
        <Box row>
          <Box pr={1}>
            <strong>{t('attach.status')}</strong>
          </Box>
          {t(`entity.info.status.${status}`)}
        </Box>
      </Box>
      {/* Program Boards */}
      <div tw="mt-4">
        <ShowBoards objectType="programs" objectId={programId} />
      </div>
    </div>
  );
};

const SupportersBloc = styled(Box)`
  .infoHtml img {
    max-width: 300px !important;
  }
`;

export default ProgramAbout;
