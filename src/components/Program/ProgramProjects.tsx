import useTranslation from 'next-translate/useTranslation';
import React, { useState } from 'react';
import { layout } from 'styled-system';
import useChallenges from 'hooks/useChallenges';
import useUserData from 'hooks/useUserData';
import { Project } from 'types';
import styled from 'utils/styled';
import Box from '../Box';
import Filters from '../Filters';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import P from '../primitives/P';
import ProjectCard from '../Project/ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';

const OverflowGradient = styled.div`
  ${layout};
  width: 3rem;
  height: 100%;
  position: absolute;
  right: 0;
  background: ${(p) =>
    `linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`};
`;

const ProgramProjects = ({ programId, customChalName }) => {
  const projectsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  const [projectsEndpoints, setProjectsEndpoint] = useState(
    `/api/programs/${programId}/projects?items=${projectsPerQuery}`
  );
  const { dataChallenges, challengesError } = useChallenges('programs', programId);
  const [selectedChallengeFilterId, setSelectedChallengeFilterId] = useState(undefined);
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const {
    data: dataProjects,
    response,
    error,
    size,
    setSize,
  } = useInfiniteLoading<{
    projects: Project[];
  }>((index) => `${projectsEndpoints}&page=${index + 1}`);

  const projects = dataProjects ? [].concat(...dataProjects?.map((d) => d.projects)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataProjects && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataProjects && typeof dataProjects[size - 1] === 'undefined');
  const isEmpty = dataProjects?.[0]?.length === 0;
  const isReachingEnd = isEmpty || projects?.length === totalNumber;

  const onFilterChange = (e) => {
    const id = e.target.name;
    const itemsNb = projects.length > projectsPerQuery ? projects.length : projectsPerQuery;
    if (id) {
      setSelectedChallengeFilterId(Number(id));
      setProjectsEndpoint(`/api/challenges/${id}/projects?items=${itemsNb}`);
    } else {
      setSelectedChallengeFilterId(undefined);
      setProjectsEndpoint(`/api/programs/${programId}/projects?items=${projectsPerQuery}`);
    }
    // TODO: maybe use mutate to update projects to show projects from the selected challenge only, without having to fetch the whole api from selected challenge
    // console.log(projects.filter(({ challenges }) =>Object.values(challenges).some(({ challenge_id }) => challenge_id === Number(id))));
  };

  return (
    <div>
      <Box>
        <P>{t('general.attached_projects_explanation')}</P>
        {!userData && ( // if user is not connected
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.project')}
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel="challenge.list_all"
            content={dataChallenges
              // filter to hide draft challenges
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!challengesError}
            errorMessage="Could not get challenges filters"
            selectedId={selectedChallengeFilterId}
            customWording={customChalName}
          />
        </Box>
        {!dataProjects ? (
          <Loading />
        ) : projects?.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Grid tw="py-4">
            {projects
              ?.filter(({ challenges }) =>
                // display only projects that have been accepted to the challenge (go through all project's challenges and return true if one of them has project_status === "accepted" )
                Object.values(challenges).some((challenge) => challenge.project_status === 'accepted')
              )
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  followersCount={project.followers_count}
                  savesCount={project.saves_count}
                  postsCount={project.posts_count}
                  has_saved={project.has_saved}
                  skills={project.skills}
                  banner_url={project.banner_url}
                  reviewsCount={project.reviews_count}
                />
              ))}
          </Grid>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          totalNumber > projectsPerQuery && size <= response?.[0].headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default ProgramProjects;
