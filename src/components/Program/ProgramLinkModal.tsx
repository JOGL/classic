import React, { useState, useMemo, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import Button from 'components/primitives/Button';
import Alert from 'components/Tools/Alert';
import Loading from '../Tools/Loading';
import SpinLoader from '../Tools/SpinLoader';
import NoResults from '../Tools/NoResults';

interface PropsModal {
  alreadyPresentPrograms: any[];
  objectId: number;
  callBack: () => void;
  closeModal: () => void;
}
export const ProgramLinkModal: FC<PropsModal> = ({ alreadyPresentPrograms, objectId, callBack, closeModal }) => {
  const { data: dataProgramsMine, error } = useGet('/api/programs/mine');
  const [selectedProgram, setSelectedProgram] = useState(null);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();
  const { t } = useTranslation('common');

  // Filter the programs that are already in this program so you don't add it twice!
  const filteredPrograms = useMemo(() => {
    if (dataProgramsMine) {
      return dataProgramsMine.filter((programMine) => {
        // Check if my program is found in alreadyPresentPrograms
        const isMyProgramAlreadyPresent = alreadyPresentPrograms.find((alreadyPresentProgram) => {
          return alreadyPresentProgram.id === programMine.id;
        });
        // We keep only the ones that are not present
        return !isMyProgramAlreadyPresent;
      });
    }
  }, [dataProgramsMine, alreadyPresentPrograms]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Link this program to the program then mutate the cache of the programs from the parent prop
    if ((selectedProgram as { id: number })?.id) {
      await api;
      api
        .post(`/api/programs/${(selectedProgram as { id: number }).id}/affiliations/spaces/${objectId}`)
        .catch(() =>
          console.error(`Could not affiliate space with id ${objectId} with program with id ${selectedProgram.id}`)
        );
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
      callBack();
      setTimeout(() => {
        // close modal after 1.5sec
        closeModal();
      }, 1500);
    }
  };
  const onProgramSelect = (e) => {
    setSelectedProgram(filteredPrograms.find((item) => item.id === parseInt(e.target.value)));
    setIsButtonDisabled(false);
  };

  return (
    <div>
      {!filteredPrograms ? (
        <Loading />
      ) : filteredPrograms && filteredPrograms.length > 0 ? (
        <form style={{ textAlign: 'left' }}>
          {filteredPrograms.map((program, index) => (
            <div className="form-check" key={index} style={{ height: '50px' }}>
              <input
                type="radio"
                className="form-check-input"
                name="exampleRadios"
                id={`program-${index}`}
                value={program.id}
                onChange={onProgramSelect}
              />
              <label className="form-check-label" htmlFor={`program-${index}`}>
                {program.title}
              </label>
            </div>
          ))}

          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-2">
              <>
                {sending && <SpinLoader />}
                {t('attach.challenge.btnSend')}
              </>
            </Button>
            {requestSent && <Alert type="success" message={t('attach.challenge.btnSendEndedProgram')} />}
          </div>
        </form>
      ) : (
        <div className="noProgram" style={{ textAlign: 'center' }}>
          <NoResults type="program" />
        </div>
      )}
    </div>
  );
};
