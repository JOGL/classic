import useTranslation from 'next-translate/useTranslation';
import React, { FC, ReactNode, useState } from 'react';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { Program } from 'types';
import Box from '../Box';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  program: Program;
  spaceId: number;
  callBack: () => void;
}
const ProgramAdminCard: FC<Props> = ({ program, spaceId, callBack }) => {
  const [sending, setSending] = useState<'remove' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const api = useApi();
  const { t } = useTranslation('common');
  const route = `/api/programs/${program.id}/affiliations/spaces/${spaceId}`;

  const removeProgram = (): void => {
    setSending('remove');
    api
      .delete(route)
      .then(() => {
        setSending(undefined);
        callBack();
      })
      .catch((err) => {
        console.error(err);
        setSending(undefined);
        setError(t('err-'));
      });
  };

  const bgLogo = {
    backgroundImage: `url(${program.logo_url_sm || '/images/default/default-program.jpg'})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (program) {
    return (
      <div>
        <Box flexDirection="row" justifyContent="space-between" key={program.id} tw="py-3">
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <Box>
              <A href={`/program/${program.short_title}`}>{program.title}</A>
            </Box>
          </Box>
          <Box row alignItems="center">
            <Button btnType="danger" disabled={sending === 'remove'} onClick={removeProgram} tw="ml-3">
              {sending === 'remove' && <SpinLoader />}
              {t('general.remove')}
            </Button>
            {error && <Alert type="danger" message={error} />}
          </Box>
        </Box>
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ProgramAdminCard;
