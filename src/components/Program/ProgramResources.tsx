import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Box from 'components/Box';
import H2 from 'components/primitives/H2';
import useGet from 'hooks/useGet';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

interface Props {
  programId: number;
}
const ProgramResources: FC<Props> = ({ programId }) => {
  const { data: resourcesList } = useGet(`/api/programs/${programId}/resources`);
  const { t } = useTranslation('common');
  return (
    <Box>
      {!resourcesList ? (
        <Loading />
      ) : resourcesList?.length === 0 ? (
        <NoResults type="resources" />
      ) : (
        <Box spaceY={7}>
          <H2 px={[3, 4]}>{t('program.resources.title')}</H2>
          {[...resourcesList].map((resource, i) => (
            <Box>
              <H2 px={[3, 4]} pb={2}>
                {resource.title}
              </H2>
              <Box bg="white" px={[3, 4]}>
                <InfoHtmlComponent content={resource.content} />
              </Box>
            </Box>
          ))}
        </Box>
      )}
    </Box>
  );
};

export default ProgramResources;
