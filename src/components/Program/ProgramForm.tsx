import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormWysiwygComponent from 'components/Tools/Forms/FormWysiwygComponent';
import FormDropdownComponent from 'components/Tools/Forms/FormDropdownComponent';
import FormTextAreaComponent from '../Tools/Forms/FormTextAreaComponent';
import Box from '../Box';
import { Program } from 'types';
import { useRouter } from 'next/router';
import { changesSavedConfAlert } from 'utils/utils';
import SpinLoader from '../Tools/SpinLoader';
import AllowPostingToAllToggle from '../Tools/AllowPostingToAllToggle';
// import "./ProgramForm.scss";

interface Props {
  mode: 'edit' | 'create';
  program: Program;
  sending: boolean;
  hasUpdated: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const ProgramForm: FC<Props> = ({ mode, program, sending = false, hasUpdated = false, handleChange, handleSubmit }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const urlBack = mode === 'edit' ? `/program/${program.short_title}` : '/';
  const textAction = mode === 'edit' ? 'Update' : 'Create';

  // show conf message when project has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to project page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));

  return (
    <form className="programForm">
      {(mode === 'edit' || mode === 'create') && (
        <>
          <FormDefaultComponent
            id="title"
            content={program.title}
            title={t('entity.info.title')}
            placeholder={t('program.form.title.placeholder')}
            onChange={handleChange}
            mandatory
          />
          <FormDefaultComponent
            id="title_fr"
            content={program.title_fr}
            title={t('entity.info.title_fr')}
            placeholder={t('program.form.title.placeholder')}
            onChange={handleChange}
          />
          <FormDefaultComponent
            id="short_title"
            content={program.short_title}
            title={t('entity.info.short_name')}
            placeholder={t('program.form.short_title.placeholder')}
            onChange={handleChange}
            prepend="#"
            mandatory
            pattern={/[A-Za-z0-9]/g}
          />

          <FormTextAreaComponent
            content={program.short_description}
            id="short_description"
            maxChar={500}
            onChange={handleChange}
            rows={3}
            title={t('entity.info.short_description')}
            placeholder={t('program.form.short_description.placeholder')}
            mandatory
          />
        </>
      )}
      {mode === 'edit' && (
        <>
          <FormTextAreaComponent
            content={program.short_description_fr}
            id="short_description_fr"
            maxChar={500}
            onChange={handleChange}
            rows={3}
            title={t('entity.info.short_description_fr')}
            placeholder={t('program.form.short_description.placeholder')}
          />
          <FormWysiwygComponent
            id="description"
            content={program.description}
            title={t('entity.info.description')}
            placeholder={t('program.form.description.placeholder')}
            onChange={handleChange}
            show
          />
          <FormWysiwygComponent
            id="description_fr"
            content={program.description_fr}
            title={t('entity.info.short_description_fr')}
            placeholder={t('program.form.description.placeholder')}
            onChange={handleChange}
          />
          <FormWysiwygComponent
            id="enablers"
            content={program.enablers}
            title={t('entity.info.enablers')}
            placeholder={t('program.form.enablers_placeholder')}
            onChange={handleChange}
          />
          <FormWysiwygComponent
            id="meeting_information"
            content={program.meeting_information}
            title={t('program.form.meeting_information')}
            placeholder={t('program.form.meeting_information')}
            onChange={handleChange}
          />
          <FormImgComponent
            id="banner_url"
            content={program.banner_url}
            title={t('program.info.banner_url')}
            imageUrl={program.banner_url}
            itemId={program.id}
            type="banner"
            itemType="programs"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChange}
            tooltipMessage={t('challenge.info.banner_url_tooltip')}
          />
          <FormImgComponent
            id="logo_url"
            content={program.logo_url}
            title={t('program.info.logo_url')}
            imageUrl={program.logo_url}
            itemId={program.id}
            type="avatar"
            itemType="programs"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChange}
          />
          <FormDropdownComponent
            id="status"
            content={program.status}
            title={t('entity.info.status.title')}
            options={['draft', 'soon', 'active', 'completed']}
            onChange={handleChange}
          />
          <FormDefaultComponent
            id="launch_date"
            content={program.launch_date ? program.launch_date.substr(0, 10) : undefined}
            title={t('entity.info.launch_date')}
            onChange={handleChange}
            type="date"
          />
          <FormDefaultComponent
            id="end_date"
            content={program.end_date ? program.end_date.substr(0, 10) : undefined}
            title={t('entity.info.end_date')}
            onChange={handleChange}
            type="date"
          />
          <FormDefaultComponent
            id="contact_email"
            content={program.contact_email}
            title={t('program.form.contact_email')}
            placeholder={t('program.form.contact_email_placeholder')}
            onChange={handleChange}
            mandatory
          />
          <FormDefaultComponent
            id="custom_challenge_name"
            content={program.custom_challenge_name}
            title={t('program.form.custom_challenge_name')}
            placeholder={t('general.challenges')}
            onChange={handleChange}
          />
          <AllowPostingToAllToggle feedId={program.feed_id} />
        </>
      )}
      {/* Form buttons */}
      <Box row justifyContent="center" mt={5} mb={3}>
        <Link href={urlBack}>
          <a>
            <button type="button" className="btn btn-outline-primary">
              {t('entity.form.back')}
            </button>
          </a>
        </Link>
        <button
          type="button"
          onClick={handleSubmit}
          className="btn btn-primary"
          disabled={sending}
          style={{ marginLeft: '10px' }}
        >
          {sending && <SpinLoader />}
          {t(`entity.form.btn${textAction}`)}
        </button>
      </Box>
    </form>
  );
};
export default ProgramForm;
