import React, { FC, memo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from 'components/primitives/H2';
import Accordion from '../Accordion';
import Box from '../Box';

interface Props {
  faqList: any;
}
const ProgramFaq: FC<Props> = ({ faqList }) => {
  const { t } = useTranslation('common');
  return (
    <Box spaceY={3}>
      <H2>{t('faq.fullTitle')}</H2>
      {faqList && (
        <Accordion
          values={faqList.documents.sort(function (a, b) {
            return a.id - b.id; // sort by id (asc)
          })}
          hasHtmlContent
        />
      )}
    </Box>
  );
};

export default memo(ProgramFaq);
