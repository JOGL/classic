/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC } from 'react';
import { display, flexbox, layout, space } from 'styled-system';
import Box from 'components/Box';
import H1 from 'components/primitives/H1';
import { Program } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import styled from 'utils/styled';
import { useTheme } from 'utils/theme';
import A from '../primitives/A';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
// import "./ProgramHeader.scss";

const Img = styled.img`
  ${[flexbox, space, display, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
  z-index: 1;
}
`;
interface Props {
  program: Program;
  lang: string;
}
const ProgramHeader: FC<Props> = ({ program, lang = 'en' }) => {
  const router = useRouter();
  const theme = useTheme();
  const { t } = useTranslation('common');

  const Stats = ({ value, title }) => (
    <div tw="justify-center items-center">
      <div>{value}</div>
      <div>{title}</div>
    </div>
  );

  const {
    needs_count,
    // status,
    members_count,
    logo_url,
    title,
    title_fr,
    projects_count,
  } = program;

  return (
    <Box width="100%" pt={4}>
      <Box flexDirection={['row', undefined, 'column']} justifyContent="center">
        <Img
          src={logo_url || 'images/jogl-logo.png'}
          style={{ objectFit: 'contain', backgroundColor: 'white' }}
          mr={4}
          mb={[undefined, undefined, 2]}
          width={['5rem', '7rem', '9rem', '10rem']}
          height={['5rem', '7rem', '9rem', '10rem']}
          alignSelf="center"
          alt="program logo"
        />
        <Box alignItems="center">
          <Box row alignItems="center" flexWrap="wrap" textAlign="center" justifyContent="center">
            <H1 pr="3" fontSize={['2.18rem', '2.3rem', '2.5rem', '2.78rem']} textAlign={['left', 'center']}>
              {/* display different field depending on language */}
              {(lang === 'fr' && title_fr) || title}
            </H1>
            {program.is_admin && (
              <Link href={`/program/${program.short_title}/edit`} passHref>
                <a tw="pr-4 cursor-pointer">
                  <Edit size={18} title="Edit program" />
                  {t('entity.form.btnAdmin')}
                </a>
              </Link>
            )}
          </Box>
          <Box pt="1" row alignSelf={['flex-start', 'center']}>
            <ShareBtns type="program" specialObjId={program.id} />
          </Box>
        </Box>
      </Box>
      <div tw="text-center items-center justify-center space-x-6 w-full inline-flex pb-2 pt-6 px-0">
        <A href={`/program/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
          <Stats value={projects_count} title={<TextWithPlural type="project" count={projects_count} />} />
        </A>
        <div tw="border-right[2px solid] border-gray-300 h-8" />
        <A href={`/program/${router.query.short_title}?tab=needs`} shallow noStyle scroll={false}>
          <Stats value={needs_count} title={<TextWithPlural type="need" count={needs_count} />} />
        </A>
        <div tw="border-right[2px solid] border-gray-300 h-8" />
        <A href={`/program/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
          <Stats value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
        </A>
      </div>
    </Box>
  );
};

export default ProgramHeader;
