import { AnyARecord } from 'node:dns';
import { FC } from 'react';
import styled from 'utils/styled';
import Box from '../Box';

// Hide checkbox visually but remain accessible to screen readers.
// Source: https://polished.js.org/docs/#hidevisually
const HiddenCheckbox = styled.input`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
  :focus + & {
    box-shadow: 0 0 0 3px pink;
  }
`;

const Label = styled.label`
  box-shadow: ${(p) => p.theme.shadows.light};
  border: ${(p) => (p.checked ? `2px solid ${p.theme.colors.darkBlue}` : '2px solid transparent')};
  border-radius: ${(p) => p.theme.radii.full};
  cursor: pointer;
  font-weight: bold;
  transition: border 0.3s ease-in-out, box-shadow 0.3s ease-in-out;
  white-space: nowrap;
  background: white;
  margin-top: 2px;
  color: ${(p) => (p.checked ? p.theme.colors.darkBlue : p.theme.colors.greys['600'])};
  :hover {
    border: 2px solid ${(p) => p.theme.colors.darkBlue};
    box-shadow: ${(p) => p.theme.shadows.md};
    color: ${(p) => p.theme.colors.darkBlue};
  }
  /* :focus-within {
    outline: -webkit-focus-ring-color auto 1px !important;
  } */
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    &:last-child {
      padding-right: 5rem;
    }
  }
`;

interface Props {
  checked: boolean;
  iconSrc: string;
  onChange: (e: any) => void;
  label: string;
  value: number;
}

const FilterCheckbox: FC<Props> = ({ checked, iconSrc, onChange, label, value }) => {
  return (
    <Label checked={checked}>
      <HiddenCheckbox name={value} type="checkbox" checked={checked} onChange={onChange} />
      <Box row spaceX={1} px={4} height={7} fontSize={1} justifyContent="center" alignItems="center">
        {iconSrc && (
          <Box width={6} height={6} justifyContent="center" alignItems="center" position="relative">
            <img src={iconSrc} width="100%" alt="Label icon" />
          </Box>
        )}
        {/* {typeof label === 'object' ? t(label.id) : label} */}
        {label}
      </Box>
    </Label>
  );
};

export default FilterCheckbox;
