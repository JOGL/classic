const BasicChip = (props) => (
  <span
    tw="inline-flex items-center px-3 py-1 rounded-full font-medium bg-gray-200 text-gray-800 w-[fit-content]"
    {...props}
  />
);

export default BasicChip;
