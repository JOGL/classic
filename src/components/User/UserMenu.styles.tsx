import styled from 'utils/styled';
import { MenuButton } from '@reach/menu-button';
import { DropDownMenu } from '../Header/Header.styles';

export const BtnUserMenu = styled(MenuButton)`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  box-shadow: 0 0 8px 3px rgba(0, 0, 0, 0.2);
  transition-duration: 200ms;
  background-size: cover;
  background-position: center;
  background-color: white;
  border: 3px solid white;
`;
export const Container = styled.div`
  min-width: 0 !important;

  .dropdown-toggle::after {
    display: none !important;
  }

  .btnUserMenu:hover {
    box-shadow: 0 0 8px 3px rgba(0, 0, 0, 0.4);
    transition-duration: 200ms;
  }

  .profileImg {
    width: 100%;
    padding: 3px;
    border-radius: 50%;
  }

  .signup {
    margin-right: 15px;
    font-weight: 600 !important;
    font-size: 17px;

    a {
      margin-right: 8px;
    }

    @media (min-width: 992px) {
      display: none;
    }

    @media (max-width: 400px) {
      display: none;
    }
  }

  .dropdown-item {
    svg {
      color: grey;
    }
  }
`;

export const UserDropDownMenu = styled(DropDownMenu)`
  a,
  div {
    padding: 6px 15px;
    @media (max-width: ${(p) => p.theme.breakpoints.lg}) {
      padding: 10px 13px;
    }
  }
  div[role='none'] {
    padding: 0 !important;
  }
`;
