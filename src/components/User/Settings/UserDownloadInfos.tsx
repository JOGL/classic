import withTranslation from 'next-translate/withTranslation';
import { Component } from 'react';
import { withApi } from 'contexts/apiContext';

class UserDownloadInfos extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    const { api, userId } = this.props;
    api
      .get(`/api/users/${userId}`)
      .then((res) => {
        const personalDatas = res.data;
        const data = `text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(personalDatas))}`;
        const link: HTMLElement = document.querySelector('.dlHiddenLink') as HTMLElement;
        link.href = `data:${data}`;
        link.download = 'data.json';
        link.innerHTML = 'download JSON';
        link.click();
      })
      .catch((err) => {
        console.error(`Couldn't GET user id=${userId}`, err);
      });
  }

  render() {
    const { t } = this.props.i18n;
    return (
      <div>
        <button type="button" className="btn btn-primary dlInfosBtn" onClick={this.handleClick}>
          {t('settings.account.dlinfos.text')}
        </button>
        <a href="#" className="dlHiddenLink" />
      </div>
    );
  }
}

export default withApi(withTranslation(UserDownloadInfos, 'common'));
