import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import React, { useContext, useState } from 'react';
import Box from 'components/Box';
import Button from 'components/primitives/Button';
import P from 'components/primitives/P';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { useModal } from 'contexts/modalContext';
import { UserContext } from 'contexts/UserProvider';
import { logEventToGA } from 'utils/analytics';

const UserDelete = ({ userId }) => {
  const { showModal, closeModal } = useModal();
  const { t } = useTranslation('common');
  const UserDeleteModal = () => {
    const api = useApi();
    const [reason, setReason] = useState('');
    const [errors, setErrors] = useState();
    const router = useRouter();
    const userContext = useContext(UserContext);
    const handleChange = (event) => {
      setReason(event.target.value);
    };

    const errorMessage = errors?.includes('err-') ? t(errors) : errors;

    const deleteAccount = (event) => {
      event.preventDefault();
      const headers = {
        Authorization: userContext.credentials.authorization,
      };
      if (reason) {
        // if user gave reason to delete account, send it to a JOGL admin via mail
        // @TODO find better way to manage this
        const param = {
          object: 'Account deletion explanation', // mail subject
          content: `The user ${userId} deleted their account with the following reason: ${reason}`, // mail content, with reason to leave message
        };
        // send it to user 2, which is JOGL admin user
        api
          .post('/api/users/2/send_email', param)
          .then(() => setReason('')) // reset reason
          .catch(() => setReason('')); // reset reason
      }
      // delete account
      api
        .delete(`/api/users`, { headers })
        .then(() => {
          // record event to Google Analytics
          logEventToGA('delete user', 'Delete', `[${userId}]`, { userId });
          userContext.logout(); // logout user
          closeModal(); // close modal
          router.push('/'); // redirect to homepage
        })
        // if errors, show them
        .catch((error) => setErrors(error.toString()));
    };
    return (
      <>
        {errors && <Alert type="danger" message={errorMessage} />}
        <P fontWeight="600" fonSize="1.1rem" mb={0}>
          {t('settings.account.delete.modal.message')}
        </P>
        <P fontSize=".9rem" fontStyle="italic">
          {t('settings.account.delete.modal.info')}
        </P>
        <P fontSize=".95rem" mb={0}>
          {t('settings.account.delete.modal.reason')}
        </P>
        <input type="text" className="form-control" id="reason" name="reason" onChange={handleChange} />
        <Box row spaceX={3} pt={3}>
          <Button btnType="danger" onClick={deleteAccount}>
            {t('general.yes')}
          </Button>
          <Button onClick={closeModal}>{t('general.no')}</Button>
        </Box>
      </>
    );
  };
  return (
    <Button
      onClick={() => {
        showModal({
          children: <UserDeleteModal />,
          title: t('settings.account.delete.btn'),
          maxWidth: '30rem',
        });
      }}
      btnType="danger"
    >
      {t('settings.account.delete.btn')}
    </Button>
  );
};
export default UserDelete;
