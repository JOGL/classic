import Axios from 'axios';
import React, { Fragment, useContext, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Box from 'components/Box';
import Grid from 'components/Grid';
import FormToggleComponent from 'components/Tools/Forms/FormToggleComponent';
import P from 'components/primitives/P';
import { useApi } from 'contexts/apiContext';
import { UserContext } from 'contexts/UserProvider';
import { useTheme } from '@emotion/react';
import { confAlert } from 'utils/utils';
import Loading from 'components/Tools/Loading';

const UserNotificationsSettings = () => {
  const [notificationsSettings, setNotificationsSettings] = useState();
  const api = useApi();
  const user = useContext(UserContext);
  const { t } = useTranslation('common');
  const theme = useTheme();

  useEffect(() => {
    const axiosSource = Axios.CancelToken.source();
    if (user) {
      const fetchSettings = async () => {
        const res = await api.get(`/api/notifications/settings`, { cancelToken: axiosSource.token }).catch((err) => {
          if (!Axios.isCancel(err)) {
            console.error("Couldn't GET settings", err);
          }
        });
        console.log(res?.data?.settings);
        res?.data?.settings && setNotificationsSettings(res.data.settings);
      };
      fetchSettings();
    }
    return () => {
      axiosSource.cancel();
    };
  }, [api, user]);

  const saveSettings = () => {
    if (notificationsSettings) {
      api
        .post(`/api/notifications/settings`, { settings: notificationsSettings })
        .then(() => confAlert.fire({ icon: 'success', title: t('entity.form.confModal.saved') })); // show confirmation alert
    }
  };

  const isDisabled = (category, delivery_method) => {
    if (notificationsSettings) {
      if (!notificationsSettings.enabled) {
        return true;
      } else {
        if (delivery_method) {
          if (category) {
            if (!notificationsSettings.categories[category].enabled) {
              return true;
            } else {
              if (!notificationsSettings.delivery_methods[delivery_method].enabled) {
                return true;
              }
            }
          }
        }
      }
      return false;
    }
  };

  const handleChange = (category, delivery_method, isDisabled) => {
    var tempSettings = { ...notificationsSettings };
    if (!isDisabled) {
      if (category) {
        if (delivery_method) {
          const isCategoryMailEnabled = tempSettings.categories[category].delivery_methods[delivery_method].enabled;
          tempSettings.categories[category].delivery_methods[delivery_method].enabled = !isCategoryMailEnabled;
        } else {
          const isCategoryNotifEnabled = tempSettings.categories[category].enabled;
          tempSettings.categories[category].enabled = !isCategoryNotifEnabled;
        }
      } else {
        if (delivery_method) {
          const isGlobalMailEnabled = tempSettings.delivery_methods[delivery_method].enabled;
          tempSettings.delivery_methods[delivery_method].enabled = !isGlobalMailEnabled;
        } else {
          const isGlobalNotifEnabled = tempSettings.enabled;
          tempSettings.enabled = !isGlobalNotifEnabled;
        }
      }
      setNotificationsSettings(tempSettings);
      saveSettings();
    }
  };

  const notifCategories = notificationsSettings?.categories;
  const isGeneralSiteNotifEnabled = notificationsSettings?.enabled;
  const isGeneralEmailNotifEnabled = notificationsSettings?.delivery_methods.email.enabled;

  return (
    <Box pt={4} pb={7}>
      <h3>{t('settings.notifications.name')}</h3>
      <P color={theme.colors.greys['700']}>{t('settings.notifications.details')}</P>

      <div tw="grid items-center pt-5 gridTemplateColumns[minmax(130px, 1fr) 85px 85px] sm:gridTemplateColumns[minmax(130px, 1fr) 105px 105px] md:gridTemplateColumns[385px 105px 105px]">
        {/* table header */}
        <Box fontWeight="bold" fontSize={3}>
          {t('settings.notifications.type')}
        </Box>
        <Box textAlign="right" fontWeight="bold" fontSize={3}>
          {t('settings.notifications.jogl')}
        </Box>
        <Box textAlign="right" fontWeight="bold" fontSize={3}>
          {t('settings.notifications.email')}
        </Box>

        {/* table first row (global settings) */}
        <Box pt={4} pb={5}>
          <Box fontWeight="600">{t('settings.notifications.all.title')}</Box>
          <Box color={theme.colors.greys['700']}>{t('settings.notifications.all.details')}</Box>
        </Box>
        <Box pt={4} pb={5}>
          <FormToggleComponent
            toggleType="notif"
            id="notifG"
            isChecked={isGeneralSiteNotifEnabled}
            color1="orange"
            onChange={() => handleChange(undefined, undefined, undefined)}
          />
        </Box>
        <Box pt={4} pb={5}>
          <FormToggleComponent
            toggleType="notif"
            id="mailG"
            isChecked={isGeneralEmailNotifEnabled}
            color1="orange"
            isDisabled={isDisabled(undefined, 'email')}
            onChange={() => handleChange(undefined, 'email', isDisabled(undefined, 'email'))}
          />
        </Box>

        {/* other table rows (one for each detailed notification setting) */}
        {notificationsSettings ? (
          Object.keys(notifCategories)
            // temporary remove "notification" category which are notif that don't fit in other categories (as we don't have for now)
            // also remove "space" category as it's not yet ready/functional + program/administration ones
            .filter(
              (category) =>
                category !== 'notification' &&
                category !== 'space' &&
                category !== 'administration' &&
                category !== 'program' &&
                category !== 'recsys'
            )
            .map((category, i) => {
              const isNotifChecked = notifCategories[category].enabled;
              const isMailChecked = notifCategories[category].delivery_methods.email.enabled;
              return (
                <Fragment key={i}>
                  <Box py={2} pr={[0, undefined, 4]}>
                    <Box fontWeight="600">{t(`settings.notifications.${category}.title`)}</Box>
                    <Box color={theme.colors.greys['700']}>{t(`settings.notifications.${category}.details`)}</Box>
                  </Box>
                  <FormToggleComponent
                    toggleType="notif"
                    id="notifC"
                    isChecked={isNotifChecked}
                    isDisabled={isDisabled(category, undefined)}
                    onChange={() => handleChange(category, undefined, isDisabled(category, undefined))}
                  />
                  <FormToggleComponent
                    toggleType="notif"
                    id="mailC"
                    isChecked={isMailChecked}
                    isDisabled={isDisabled(category, 'email')}
                    onChange={() => handleChange(category, 'email', isDisabled(category, 'email'))}
                  />
                </Fragment>
              );
            })
        ) : (
          <Loading />
        )}
        {/* Show only recsys settings */}
        {notificationsSettings ? (
          Object.keys(notifCategories)
            .filter((category) => category === 'recsys')
            .map((category, i) => {
              const isMailChecked = notifCategories[category].delivery_methods.email.enabled;
              return (
                <Fragment key={i}>
                  <Box py={2} pr={[0, undefined, 4]}>
                    <Box fontWeight="600">{t(`settings.notifications.${category}.title`)}</Box>
                    <Box color={theme.colors.greys['700']}>{t(`settings.notifications.${category}.details`)}</Box>
                  </Box>
                  <FormToggleComponent
                    toggleType="notif"
                    id="notifC"
                    isChecked={false}
                    isDisabled={true}
                    onChange={() => handleChange(category, undefined, isDisabled(category, undefined))}
                  />
                  <FormToggleComponent
                    toggleType="notif"
                    id="mailC"
                    isChecked={isMailChecked}
                    isDisabled={isDisabled(category, 'email')}
                    onChange={() => handleChange(category, 'email', isDisabled(category, 'email'))}
                  />
                </Fragment>
              );
            })
        ) : (
          <Loading />
        )}
      </div>
    </Box>
  );
};

export default UserNotificationsSettings;
