import ChallengeCard from '../Challenge/ChallengeCard';
// import CommunityList from '../Community/CommunityList';
import Grid from '../Grid';
import ProgramCard from '../Program/ProgramCard';
import ProjectList from '../Project/ProjectList';
import SpaceCard from '../Space/SpaceCard';
import Loading from '../Tools/Loading';
import useUserObjects from 'hooks/useUserObjects';

const UserCollection = ({ userId, userStats, userData, t }) => {
  const listProjects = userStats.projects_count !== 0 ? useUserObjects(userId, 'projects').data : [];
  // const listCommunities = userStats.communities_count !== 0 ? useUserObjects(userId, 'communities').data : [];
  const listChallenges = userStats.challenges_count !== 0 ? useUserObjects(userId, 'challenges').data : [];
  const listPrograms = userStats.programs_count !== 0 ? useUserObjects(userId, 'programs').data : [];
  const listSpaces = userStats.spaces_count !== 0 ? useUserObjects(userId, 'spaces').data : [];
  return (
    <>
      {/* load/show only if user has projects */}
      {userStats.projects_count !== 0 && (
        <>
          <h3>{t('user.profile.tab.projects')}</h3>
          {/* don't show draft projects to other users */}
          <ProjectList
            listProjects={
              userData && userId === userData.id
                ? listProjects
                : listProjects?.filter(({ status }) => status !== 'draft')
            }
          />
        </>
      )}
      {/* load/show only if user has groups */}
      {/* {userStats.communities_count !== 0 && (
        <>
          <div className="communityList" tw="mt-10">
            <h3>{t('user.profile.tab.communities')}</h3>
            <CommunityList listCommunities={listCommunities} />
          </div>
        </>
      )} */}
      {/* load/show only if user has challenges */}
      {userStats.challenges_count !== 0 && (
        <>
          <div className="communityList" style={{ marginTop: '2.6rem' }}>
            <h3>{t('general.challenges')}</h3>
            <Grid tw="pt-3">
              {!listChallenges ? (
                <Loading />
              ) : (
                listChallenges
                  ?.filter(({ status }) => status !== 'draft') // don't show draft challenges
                  .map((challenge, i) => (
                    <ChallengeCard
                      key={i}
                      id={challenge.id}
                      short_title={challenge.short_title}
                      title={challenge.title}
                      title_fr={challenge.title_fr}
                      short_description={challenge.short_description}
                      short_description_fr={challenge.short_description_fr}
                      membersCount={challenge.members_count}
                      needsCount={challenge.needs_count}
                      has_saved={challenge.has_saved}
                      clapsCount={challenge.claps_count}
                      status={challenge.status}
                      program={challenge.program}
                      space={challenge.space}
                      customType={challenge.custom_type}
                      projectsCount={challenge.projects_count}
                      banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                    />
                  ))
              )}
            </Grid>
          </div>
        </>
      )}
      {/* load/show only if user has programs */}
      {userStats.programs_count !== 0 && (
        <>
          <div className="communityList" style={{ marginTop: '2.6rem' }}>
            <h3>{t('general.programs')}</h3>
            <Grid tw="pt-3">
              {!listPrograms ? (
                <Loading />
              ) : (
                listPrograms
                  ?.filter(({ status }) => status !== 'draft') // don't show draft programs
                  .map((program, i) => (
                    <ProgramCard
                      key={i}
                      id={program.id}
                      short_title={program.short_title}
                      title={program.title}
                      title_fr={program.title_fr}
                      short_description={program.short_description}
                      short_description_fr={program.short_description_fr}
                      membersCount={program.members_count}
                      needsCount={program.needs_count}
                      has_saved={program.has_saved}
                      projectsCount={program.projects_count}
                      banner_url={program.banner_url || '/images/default/default-program.jpg'}
                    />
                  ))
              )}
            </Grid>
          </div>
        </>
      )}
      {/* load/show only if user has spaces */}
      {userStats.spaces_count !== 0 && (
        <>
          <div className="communityList" style={{ marginTop: '2.6rem' }}>
            <h3>{t('general.spaces')}</h3>
            <Grid tw="pt-3">
              {!listSpaces ? (
                <Loading />
              ) : (
                listSpaces
                  ?.filter(({ status }) => status !== 'draft') // don't show draft spaces
                  .map((space, i) => (
                    <SpaceCard
                      key={i}
                      id={space.id}
                      short_title={space.short_title}
                      title={space.title}
                      title_fr={space.title_fr}
                      short_description={space.short_description}
                      short_description_fr={space.short_description_fr}
                      membersCount={space.members_count}
                      needsCount={space.needs_count}
                      has_saved={space.has_saved}
                      clapsCount={space.claps_count}
                      projectsCount={space.projects_count}
                      spaceType={space.space_type}
                      banner_url={space.banner_url || '/images/default/default-space.jpg'}
                    />
                  ))
              )}
            </Grid>
          </div>
        </>
      )}
    </>
  );
};

export default UserCollection;
