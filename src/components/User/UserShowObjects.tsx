/* eslint-disable no-nested-ternary */
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import useTranslation from 'next-translate/useTranslation';
import React, { useEffect, useState } from 'react';
import { layout } from 'styled-system';
import CommunityList from 'components/Community/CommunityList';
import ProjectList from 'components/Project/ProjectList';
import Loading from 'components/Tools/Loading';
import styled from 'utils/styled';
import Box from '../Box';
import ChallengeCard from '../Challenge/ChallengeCard';
import Grid from '../Grid';
import NeedCard from '../Need/NeedCard';
import ProgramCard from '../Program/ProgramCard';
import SpaceCard from '../Space/SpaceCard';
import { TabListUser, TabUser } from '../Tabs/TabsStyles';
import NoResults from '../Tools/NoResults';
import UserCard from './UserCard';
// import "Components/User/UserProfile.scss";
// import "Components/User/UserFollowings.scss";

const OverflowGradient = styled.div`
  ${layout};
  width: 3rem;
  height: 100%;
  position: absolute;
  right: 0;
  background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 134.37%);
`;

export default function UserShowObjects({ list, type = 'following' }) {
  const [listChallenges, setListChallenges] = useState([]);
  // const [listCommunities, setListCommunities] = useState([]);
  const [listProjects, setListProjects] = useState([]);
  const [listUsers, setListUsers] = useState([]);
  const [listNeeds, setListNeeds] = useState([]);
  const [listPrograms, setListPrograms] = useState([]);
  const [listSpaces, setListSpaces] = useState([]);
  const [loading, setLoading] = useState(true);
  const [noResult, setNoResult] = useState(false);
  const [selectedType, setSelectedType] = useState('users');
  const { t } = useTranslation('common');

  useEffect(() => {
    setLoading(true);
    setNoResult(false);
    if (list) {
      setLoading(false);
      setListChallenges(list?.challenges);
      // setListCommunities(list?.communities);
      setListProjects(list?.projects);
      setListUsers(list?.users);
      setListNeeds(list?.needs);
      setListPrograms(list?.programs);
      setListSpaces(list?.spaces);
      if (
        list.challenges?.length === 0 &&
        // list.communities?.length === 0 &&
        list.projects?.length === 0 &&
        list.users?.length === 0 &&
        list.needs?.length === 0 &&
        list.programs?.length === 0 &&
        list.spaces?.length === 0
      ) {
        setNoResult(true);
      }
    }
  }, [list]);
  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    listUsers?.length > 0
      ? setSelectedType('users') // if user follows other users, add active class to this tab
      : listProjects?.length > 0
      ? setSelectedType('projects') // if user doesn't follow objects from previous tab, but from projects, add active class to this tab
      : // : listCommunities?.length > 0
      // ? setSelectedType('communities') // same as up for communities
      listChallenges?.length > 0
      ? setSelectedType('challenges') // same as up for challenges
      : listNeeds?.length > 0
      ? setSelectedType('needs') // same as up for needs
      : listPrograms?.length > 0
      ? setSelectedType('programs') // same as up for programs
      : listSpaces?.length > 0 && setSelectedType('spaces'); // else add active class to spaces tab
  }, [listChallenges, listUsers, listProjects, listNeeds, listPrograms, listSpaces]);

  const tabs = [
    { value: 'users', translationId: 'user.profile.tab.following.users', tabList: listUsers },
    { value: 'projects', translationId: 'user.profile.tab.following.projects', tabList: listProjects },
    { value: 'needs', translationId: 'user.profile.tab.following.needs', tabList: listNeeds },
    { value: 'challenges', translationId: 'user.profile.tab.following.challenges', tabList: listChallenges },
    { value: 'programs', translationId: 'general.programs', tabList: listPrograms },
    { value: 'spaces', translationId: 'general.spaces', tabList: listSpaces },
    // { value: 'communities', translationId: 'user.profile.tab.following.communities', tabList: listCommunities },
  ];

  return (
    <Loading active={loading} height="300px">
      {!noResult && (
        <Box position="relative" minHeight="70px">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Tabs defaultIndex={tabs.findIndex((tab) => tab.value === selectedType)}>
            <TabListUser tw="overflow-scroll flex-nowrap pr-4">
              <div>
                {tabs.map((item) => {
                  if (item.tabList?.length > 0) return <TabUser>{t(item.translationId)}</TabUser>;
                })}
              </div>
            </TabListUser>
            <TabPanels>
              {/* Users */}
              {listUsers?.length > 0 && (
                <TabPanel>
                  <Grid tw="py-2 gap-2 sm:(py-4 gap-4)">
                    {listUsers?.map((user, i) => (
                      <UserCard
                        key={i}
                        id={user.id}
                        firstName={user.first_name}
                        lastName={user.last_name}
                        nickName={user.nickname}
                        shortBio={user.short_bio}
                        logoUrl={user.logo_url}
                        hasFollowed={user.has_followed}
                        skills={user.skills}
                        affiliation={user.affiliation}
                        projectsCount={user.stats?.projects_count}
                        followersCount={user.stats?.followers_count}
                        spacesCount={user.stats?.spaces_count}
                        mutualCount={user.stats.mutual_count}
                      />
                    ))}
                  </Grid>
                </TabPanel>
              )}
              {/* Projects */}
              {listProjects?.length > 0 && (
                <TabPanel>
                  <ProjectList
                    listProjects={
                      // if we are in followings modal, don't show draft projects, else we are in the "my objects modal", so we can show draft projects
                      type === 'followings' ? listProjects?.filter(({ status }) => status !== 'draft') : listProjects
                    }
                  />
                </TabPanel>
              )}
              {/* Needs */}
              {listNeeds?.length > 0 && (
                <TabPanel>
                  <Grid tw="py-4">
                    {!listNeeds ? (
                      <Loading />
                    ) : (
                      listNeeds.map((need, i) => (
                        <NeedCard
                          key={i}
                          title={need.title}
                          project={need.project}
                          skills={need.skills}
                          resources={need.ressources}
                          hasSaved={need.has_saved}
                          id={need.id}
                          postsCount={need.posts_count}
                          publishedDate={need.created_at}
                          membersCount={need.members_count}
                          dueDate={need.end_date}
                          status={need.status}
                        />
                      ))
                    )}
                  </Grid>
                </TabPanel>
              )}
              {/* Challenges */}
              {listChallenges?.length > 0 && (
                <TabPanel>
                  <Grid tw="py-4">
                    {!listChallenges ? (
                      <Loading />
                    ) : (
                      listChallenges.map((challenge, i) => (
                        <ChallengeCard
                          key={i}
                          id={challenge.id}
                          short_title={challenge.short_title}
                          title={challenge.title}
                          title_fr={challenge.title_fr}
                          short_description={challenge.short_description}
                          short_description_fr={challenge.short_description_fr}
                          membersCount={challenge.members_count}
                          needsCount={challenge.needs_count}
                          has_saved={challenge.has_saved}
                          clapsCount={challenge.claps_count}
                          status={challenge.status}
                          program={challenge.program}
                          customType={challenge.custom_type}
                          space={challenge.space}
                          projectsCount={challenge.projects_count}
                          banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                        />
                      ))
                    )}
                  </Grid>
                </TabPanel>
              )}
              {/* Programs */}
              {listPrograms?.length > 0 && (
                <TabPanel>
                  <Grid tw="py-4">
                    {!listPrograms ? (
                      <Loading />
                    ) : (
                      listPrograms.map((program, i) => (
                        <ProgramCard
                          key={i}
                          id={program.id}
                          short_title={program.short_title}
                          title={program.title}
                          title_fr={program.title_fr}
                          short_description={program.short_description}
                          short_description_fr={program.short_description_fr}
                          membersCount={program.members_count}
                          needsCount={program.needs_count}
                          has_saved={program.has_saved}
                          projectsCount={program.projects_count}
                          banner_url={program.banner_url || '/images/default/default-program.jpg'}
                        />
                      ))
                    )}
                  </Grid>
                </TabPanel>
              )}
              {/* Spaces */}
              {listSpaces?.length > 0 && (
                <TabPanel>
                  <Grid tw="py-4">
                    {!listSpaces ? (
                      <Loading />
                    ) : (
                      listSpaces.map((space, i) => (
                        <SpaceCard
                          key={i}
                          id={space.id}
                          short_title={space.short_title}
                          title={space.title}
                          short_description={space.short_description}
                          membersCount={space.members_count}
                          needsCount={space.needs_count}
                          has_saved={space.has_saved}
                          projectsCount={space.projects_count}
                          spaceType={space.space_type}
                          banner_url={space.banner_url || '/images/default/default-space.jpg'}
                        />
                      ))
                    )}
                  </Grid>
                </TabPanel>
              )}
              {/* {listCommunities?.length > 0 && (
                <TabPanel>
                  <div className="communityList">
                    <CommunityList listCommunities={listCommunities} />
                  </div>
                </TabPanel>
              )} */}
              {noResult && <NoResults type={type} />}
            </TabPanels>
          </Tabs>
        </Box>
      )}
    </Loading>
  );
}
