import React, { useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
// import $ from "jquery";
import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import useUserData from 'hooks/useUserData';
import BtnFollow from '../Tools/BtnFollow';
import ListFollowers from '../Tools/ListFollowers';
import UserShowObjects from 'components/User/UserShowObjects';
import { useModal } from 'contexts/modalContext';
import { ContactForm } from '../Tools/ContactForm';
import useGet from 'hooks/useGet';
import { TextWithPlural } from 'utils/managePlurals';
import Box from '../Box';
import ReactGA from 'react-ga';
import Chips from '../Chip/Chips';
import { theme } from 'twin.macro';
import { PaperPlane } from '@emotion-icons/boxicons-regular';
// import "./UserHeader.scss";

const UserHeader = ({ user, setMyProfile }) => {
  useEffect(() => {
    $('.moreSkills').click(() => {
      // when click on the "+..." skills
      const aboutTab: HTMLElement = document.querySelector('[data-reach-tab-list] :nth-child(2)') as HTMLElement;
      aboutTab.click(); // force click on about tab
      const element = document.querySelector('.infoSkills'); // get skills section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to section
    });
    $('.moreResources').click(() => {
      // when click on the "+..." resources
      const aboutTab: HTMLElement = document.querySelector('[data-reach-tab-list] :nth-child(2)') as HTMLElement;
      aboutTab.click(); // force click on about tab
      const element = document.querySelector('.infoResources'); // get resources section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to section
    });
  });

  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const { userData } = useUserData();
  // prettier-ignore
  let {
    id, logo_url, skills, ressources, nickname, first_name, last_name, bio, short_bio,
    stats, can_contact, has_followed
  } = user;
  if (!logo_url) {
    logo_url = '/images/default/default-user.png';
  }
  const logoStyle = {
    backgroundImage: `url(${logo_url})`,
  };
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/users/${id}/links`);

  const openFollowersModal = (e) => {
    stats.followers_count && // open modal only if user has followers
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={id} itemType="users" />,
        title: t('entity.tab.followers'),
        maxWidth: '70rem',
      });
  };

  const openFollowingModal = (e) => {
    stats.following_count && // open modal only if user is following objects
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <FollowingModal userId={id} />,
        title: t('general.following_other'),
        maxWidth: '70rem',
      });
  };

  const showUserProfilePicture = () => {
    showModal({
      children: (
        <Box>
          <img src={logo_url} style={{ objectFit: 'contain' }} />
        </Box>
      ),
      showCloseButton: true,
      maxWidth: '30rem',
    });
  };

  useEffect(() => {
    if (userData && userData.id === id) setMyProfile(true);
  }, [userData]);

  const showSkillsAndResourcesChips = () => (
    <Box flexDirection={['column', 'row']} spaceX={[0, 5]}>
      {skills.length !== 0 && (
        <Box>
          <Box color={theme`colors.secondary`}>{t('user.profile.skills')}</Box>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              href: `/search/members/?refinementList[skills][0]=${skill}`,
            }))}
            overflowText="userSkill"
            type="skills"
            showCount={4}
          />
        </Box>
      )}
      {ressources.length !== 0 && (
        <Box>
          <Box color={theme`colors.secondary`}>{t('user.profile.resources')}</Box>
          <Chips
            data={ressources.map((resource) => ({
              title: resource,
              href: `/search/members/?refinementList[ressources][0]=${resource}`,
            }))}
            overflowText="resourceSkill"
            type="resources"
            showCount={4}
          />
        </Box>
      )}
    </Box>
  );

  return (
    <div className="userHeader--top row">
      <div className="col-lg-2 col-12 d-none d-lg-block">
        <div className="userImg" style={logoStyle} onClick={showUserProfilePicture} />
      </div>

      <div className="col-lg-10 col-12 userInfos">
        <div className="infoContainer">
          <div className="userSmallImg">
            <div style={logoStyle} onClick={showUserProfilePicture} />
          </div>
          <div className="firstRow">
            <div className="nameInfos">
              <h1 className="title">{`${first_name} ${last_name}`}</h1>
              <p className="nickname">{`@${nickname}`}</p>
            </div>
            <Box display={['none', 'flex']}>{showSkillsAndResourcesChips()}</Box>
          </div>
        </div>
        <div className="userStats" tw="my-5 sm:my-3 md:(mt-0 mb-4)">
          {/* show follow button if user is not connected, or if he is connected but not the user viewed */}
          {(!userData || (userData && userData.id !== id)) && (
            <BtnFollow followState={has_followed} itemType="users" itemId={id} count={stats.followers_count} />
          )}
          {/* show contact button if user is connected, that he's not the viewed user, and that viewed user wants to be contacted */}
          {userData && userData.id !== id && can_contact !== false && (
            <span tw="relative z-0 inline-flex shadow-sm rounded-md ml-3">
              <button
                type="button"
                tw="relative inline-flex items-center p-2 rounded-md border border-solid border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-100 focus:(z-10 outline-none ring-1 ring-primary border-primary)"
                onClick={() => {
                  ReactGA.modalview('/send-message');
                  showModal({
                    children: <ContactForm itemId={id} closeModal={closeModal} />,
                    title: t('user.contactModal.title', { userFullName: `${first_name} ${last_name}` }),
                  });
                }}
              >
                <PaperPlane size={18} title={t('user.btn.contact')} />
                {t('user.btn.contact')}
              </button>
            </span>
          )}
          <div>
            {userData && userData.id === id && (
              <span className="text" tabIndex={0} onClick={openFollowersModal} onKeyUp={openFollowersModal}>
                <strong>{stats.followers_count}</strong>&nbsp;
                <TextWithPlural type="follower" count={stats.followers_count} />
              </span>
            )}
            <span tw="ml-3" className="text" tabIndex={0} onClick={openFollowingModal} onKeyUp={openFollowingModal}>
              <strong>{stats.following_count || 0}</strong>&nbsp;
              <TextWithPlural type="following" count={stats.following_count} />
            </span>
          </div>
        </div>
        <p className="about">{short_bio || bio}</p>
        {/* user social medias */}
        {dataExternalLink && dataExternalLink?.length !== 0 && (
          <div tw="flex flex-wrap gap-x-4 mb-5 sm:gap-x-3">
            {[...dataExternalLink].map((link, i) => (
              <a tw="items-center self-center" key={i} href={link.url} target="_blank">
                <img tw="w-10 hover:opacity-80" src={link.icon_url} />
              </a>
            ))}
          </div>
        )}
        <Box display={['flex', 'none']}>{showSkillsAndResourcesChips()}</Box>
      </div>

      {userData && userData.id === id && (
        <Box id="editProfile" className="col-12 userActions" pt={[4, undefined, undefined, 0]}>
          <Link href={`/user/${id}/edit`}>
            <a>
              <Edit size={23} />
              {t('user.profile.edit.btnEdit')}
            </a>
          </Link>
        </Box>
      )}
    </div>
  );
};

const FollowingModal = ({ userId }) => {
  const { data: followings } = useGet(`/api/users/${userId}/following`);
  return <UserShowObjects list={followings} type="following" />;
};

export default UserHeader;
