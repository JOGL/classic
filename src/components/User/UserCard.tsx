import Link from 'next/link';
import React, { FC, memo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from 'components/primitives/H2';
// import { useModal } from 'contexts/modalContext';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import { DataSource } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import ObjectCard from 'components/Cards/ObjectCard';
import Chips from '../Chip/Chips';
import Title from '../primitives/Title';
import BtnFollow from '../Tools/BtnFollow';
import Image2 from '../Image2';
import tw, { css } from 'twin.macro';
import A from '../primitives/A';

interface Props {
  id: number;
  firstName: string;
  lastName: string;
  nickName?: string;
  shortBio: string;
  skills?: string[];
  resources?: string[];
  status?: string;
  lastActive?: string;
  logoUrl: string;
  hasFollowed?: boolean;
  source?: DataSource;
  role?: string;
  projectsCount?: number;
  followersCount?: number;
  spacesCount?: number;
  mutualCount?: number;
  isCompact?: boolean;
  affiliation?: string;
  showRelationNotRole?: boolean;
}
const UserCard: FC<Props> = ({
  id,
  firstName = 'First name',
  lastName = 'Last name',
  nickName = '',
  shortBio = '_ _',
  skills = [],
  resources = [],
  status,
  logoUrl = '/images/default/default-user.png',
  hasFollowed = false,
  lastActive,
  source,
  role,
  mutualCount,
  projectsCount,
  followersCount = 0,
  spacesCount = 0,
  isCompact = false,
  affiliation = '',
  showRelationNotRole = false,
}) => {
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  // const { showModal, closeModal } = useModal();
  const { data: affiliations } = useGet(`api/users/${id}/affiliations`);
  // all affiliation that are not pending
  const currentAffiliations = affiliations?.parents.filter((affiliate) => affiliate.status !== 'pending');

  const userUrl = `/user/${id}/${nickName}`;
  if (!isCompact) {
    return (
      <ObjectCard tw="justify-between">
        <div>
          <div tw="-mx-4 -mt-4 height[96px]">
            {/* Top shape */}
            {!role ? (
              <img src="/images/userCardShapeMember.svg" tw="w-full" loading="lazy" />
            ) : showRelationNotRole && role === 'member' ? (
              <img src="/images/userCardShapeMember2.svg" tw="w-full" loading="lazy" />
            ) : (
              <img
                src={`/images/userCardShape${role[0].toUpperCase() + role.slice(1)}.svg`}
                tw="w-full"
                loading="lazy"
              />
            )}
          </div>
          {role && (
            <div tw="absolute left-3 color[#4d4d4d] capitalize rounded top-3 bg-white bg-opacity-80 px-1 box-shadow[0px 4px 15px rgb(0 0 0 / 9%)]">
              {role}
            </div>
          )}
          {/* Profile img */}
          <A href={userUrl} noStyle>
            <div
              css={[
                tw`w-24 h-24 mx-auto mt-[-5.5rem]`,
                css`
                  img {
                    ${tw`(object-cover rounded-full w-24 h-24 border-white bg-white border-4 border-solid hover:border-width[2px])!`};
                  }
                `,
              ]}
            >
              <Image2 src={logoUrl} priority />
            </div>
          </A>
          {/* Name */}
          <Link href={userUrl} passHref>
            <Title>
              <H2 tw="word-break[break-word] text-2xl text-center my-3">
                {firstName} {lastName}
              </H2>
            </Title>
          </Link>
          {/* Stats */}
          <div tw="items-center justify-around space-x-2 flex flex-wrap">
            <CardData value={followersCount} title={<TextWithPlural type="follower" count={followersCount} />} />
            <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
            <CardData value={spacesCount} title={<TextWithPlural type="space" count={spacesCount} />} />
          </div>
          <Hr tw="mt-2 pt-3" />
          {/* Affiliation */}
          <div className="content" tw="inline-flex">
            {/* if user has affiliation, display them */}
            {affiliations?.parents.length !== 0 && currentAffiliations?.length !== 0 ? (
              currentAffiliations?.map((affiliate, i) => {
                return (
                  <div tw="inline-flex space-x-2 items-center" key={i}>
                    {/* <Image2 src="/images/logo.svg" alt="jogl logo rocket" quality={25} tw="w-3!" /> */}
                    <Image2 src={affiliate.logo_url_sm} alt="jogl logo rocket" quality={25} tw="w-4!" />
                    <Link href={`/${affiliate.parent_type.toLowerCase()}/${affiliate.short_title}`} passHref>
                      <a tw="text-black font-medium hover:(underline text-black)">{affiliate.title}</a>
                    </Link>
                  </div>
                );
              })
            ) : affiliation ? (
              <>
                <span tw="underline">{t('user.profile.affiliation')}</span>: {affiliation}
              </>
            ) : (
              '_ _'
            )}
          </div>
          <Hr tw="mt-2 pt-2" />
          {/* Bio */}
          <div tw="line-clamp-2 my-1 break-all h-[2.85rem]">{shortBio || '_ _'}</div>
          {(skills.length !== 0 || resources.length !== 0) && <Hr tw="mt-2 pt-2" />}
          {/* SKills & resources */}
          {skills.length !== 0 && (
            <>
              <p tw="text-gray-400 mb-0 text-sm">{t('user.profile.skills')}</p>
              <Chips
                data={skills.map((skill) => ({
                  title: skill,
                  href: `/search/members/?refinementList[skills][0]=${skill}`,
                }))}
                overflowLink={`/user/${id}`}
                type="skills"
                showCount={2}
                smallChips
              />
            </>
          )}
          {resources.length !== 0 && (
            <>
              <p tw="text-gray-400 mb-0 text-sm">{t('user.profile.resources')}</p>
              <Chips
                data={resources.map((resource) => ({
                  title: resource,
                  href: `/search/members/?refinementList[ressources][0]=${resource}`,
                }))}
                overflowLink={`/user/${id}`}
                type="resources"
                showCount={2}
                smallChips
              />
            </>
          )}
        </div>
        <div>
          <Hr tw="mt-2 pt-3" />
          {/* Shared connections */}
          {/* {mutualCount > 0 && userData?.id !== id && ( */}
          {mutualCount !== undefined && (
            <A href={userUrl} noStyle>
              <div tw="flex flex-col items-center text-sm mb-3">
                <div tw="inline-flex">
                  <div tw="rounded-full h-3 w-3 flex border-gray-600 border-solid border-2"></div>
                  <div tw="rounded-full h-3 w-3 flex border-gray-600 border-solid border-2 -ml-1.5"></div>
                </div>
                {userData?.id !== id ? mutualCount : '∞'} <TextWithPlural type="mutualConnection" count={mutualCount} />
              </div>
            </A>
          )}
          {/* Follow button */}
          {hasFollowed !== undefined && (
            <div tw="flex justify-center">
              <BtnFollow
                followState={hasFollowed}
                itemType="users"
                itemId={id}
                hasNoStat
                dataComesFromAlgolia={source === 'algolia'}
                isDisabled={userData?.id === id}
                tw="width[fit-content]"
              />
            </div>
          )}
        </div>
      </ObjectCard>
    );
  }
  // Compact card
  return (
    <div tw="items-center flex shadow-custom rounded-xl pr-3 h-[8rem] w-[300px]">
      {/* Left shape */}
      <img src="/images/userCardShapeLeft.svg" loading="lazy" tw="height[inherit] w-[54px]" />
      {/* Profile img */}
      <A href={userUrl} noStyle>
        <div
          css={[
            tw`w-16 h-16 ml-[-2.7rem]`,
            css`
              img {
                ${tw`(object-cover rounded-full w-16 h-16 border-white bg-white border-4 border-solid hover:opacity-90)!`};
              }
            `,
          ]}
        >
          <Image2 src={logoUrl} priority />
        </div>
      </A>
      <div tw="flex flex-col flex-1 ml-2 items-start">
        {/* Name and follow button */}
        <div tw="flex justify-between w-full">
          <Link href={userUrl} passHref>
            <Title>
              <H2 tw="word-break[break-word] text-2xl mr-3 text-left">
                {firstName} {lastName}
              </H2>
            </Title>
          </Link>
          {hasFollowed !== undefined && (
            <div tw="flex justify-center height[fit-content]">
              <BtnFollow
                followState={hasFollowed}
                itemType="users"
                itemId={id}
                hasNoStat
                dataComesFromAlgolia={source === 'algolia'}
                isDisabled={userData?.id === id}
                tw="width[fit-content] height[fit-content]"
              />
            </div>
          )}
        </div>
        {/* Bio */}
        <div tw="line-clamp-2 my-1 break-all">{shortBio || '_ _'}</div>
      </div>
    </div>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-extrabold font-size[16px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default memo(UserCard);
