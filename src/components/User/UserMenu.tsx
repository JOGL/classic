import { FileTrayFull } from '@emotion-icons/ionicons-sharp';
import { Star, HelpCircle, UserCircle, Cog } from '@emotion-icons/boxicons-solid';
import { SignOut } from '@emotion-icons/octicons/SignOut';
import { Menu, MenuItem, MenuLink } from '@reach/menu-button';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import React, { useContext, useEffect, useState } from 'react';
import { useModal } from 'contexts/modalContext';
import { UserContext } from 'contexts/UserProvider';
import useGet from 'hooks/useGet';
import Box from '../Box';
import ChallengeCard from '../Challenge/ChallengeCard';
// import CommunityList from '../Community/CommunityList';
import Grid from '../Grid';
import ProgramCard from '../Program/ProgramCard';
import ProjectList from '../Project/ProjectList';
import { TabListUser, TabUser } from '../Tabs/TabsStyles';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import { BtnUserMenu, Container, UserDropDownMenu } from './UserMenu.styles';
import UserShowObjects from './UserShowObjects';
import SpaceCard from '../Space/SpaceCard';

const UserMenu: React.FC = () => {
  const userContext = useContext(UserContext);
  const { showModal } = useModal();
  const user = userContext.userData;
  const { t } = useTranslation('common');

  if (user) {
    return (
      <Container>
        <Menu>
          <BtnUserMenu style={{ backgroundImage: `url(${user?.logo_url_sm})` }} />
          <UserDropDownMenu>
            <Link href={`/user/${user?.id}/${user?.nickname}`} passHref>
              <MenuLink to={`/user/${user?.id}/${user?.nickname}`}>
                <UserCircle size={20} title="User profile" />
                {t('menu.profile.profile')}
              </MenuLink>
            </Link>
            <MenuItem
              onSelect={() => {
                showModal({
                  children: <UsersObjects t={t} />,
                  title: t('user.objects.title'),
                  maxWidth: '70rem',
                });
              }}
            >
              <FileTrayFull size={20} title="My portfolio" />
              {t('user.objects.title')}
            </MenuItem>
            <MenuItem
              onSelect={() => {
                showModal({
                  children: <SavedObjectsModal />,
                  title: t('user.profile.starred_items'),
                  maxWidth: '70rem',
                });
              }}
            >
              <Star size={20} title="Starred items" />
              {t('user.profile.starred_items')}
            </MenuItem>
            <Link href={`/user/${user?.id}/edit?t=admin`} passHref>
              <MenuLink to={`/user/${user?.id}/edit?t=admin`}>
                <Cog size={20} title="Settings" />
                {t('menu.profile.settings')}
              </MenuLink>
            </Link>
            <MenuItem
              onSelect={() => {
                window.location.href = 'mailto:support@jogl.io';
              }}
            >
              <HelpCircle size={20} title="JOGL Support" />
              {t('menu.profile.help')}
            </MenuItem>
            <MenuItem onSelect={() => userContext.logout('/')}>
              <SignOut size={20} title="Log out" />
              {t('menu.profile.logout')}
            </MenuItem>
          </UserDropDownMenu>
        </Menu>
      </Container>
    );
  } else {
    return <Loading />;
  }
};

const SavedObjectsModal = () => {
  const { data: savedObjects } = useGet(`/api/users/saved_objects`);
  return <UserShowObjects list={savedObjects} type="star" />;
};

const UsersObjects = ({ t }) => {
  const { data: userProjects } = useGet(`/api/projects/mine`);
  // const { data: userGroups } = useGet(`/api/communities/mine`);
  const { data: userChallenges } = useGet(`/api/challenges/mine`);
  const { data: userPrograms } = useGet(`/api/programs/mine`);
  const { data: userSpaces } = useGet(`/api/spaces/mine`);
  const projectsAdmin = userProjects?.filter(({ is_admin }) => is_admin);
  // const groupsAdmin = userGroups?.filter(({ is_admin }) => is_admin);
  const challengesAdmin = userChallenges?.filter(({ is_admin }) => is_admin);
  const programsAdmin = userPrograms?.filter(({ is_admin }) => is_admin);
  const spacesAdmin = userSpaces?.filter(({ is_admin }) => is_admin);
  const [isAdminOfObjects, setIsAdminOfObjects] = useState(false);
  const [noResults, setNoResults] = useState(false);
  useEffect(() => {
    // set isAdminOfObjects to true if user is admin of at least one object of any type
    if (projectsAdmin && challengesAdmin && programsAdmin && spacesAdmin) {
      setIsAdminOfObjects(
        projectsAdmin?.length !== 0 ||
          // groupsAdmin?.length !== 0 ||
          challengesAdmin?.length !== 0 ||
          programsAdmin?.length !== 0 ||
          spacesAdmin?.length !== 0
      );
    }
    setNoResults(
      // if user has no object, set no result to true
      userProjects?.length === 0 &&
        // userGroups?.length === 0 &&
        userChallenges?.length === 0 &&
        userPrograms?.length === 0 &&
        userSpaces?.length === 0
    );
  }, [projectsAdmin, challengesAdmin, programsAdmin, spacesAdmin]);

  if (noResults) return <NoResults />;

  const tabs = [
    { value: 'adminOf', translationId: 'user.objects.adminOf' },
    { value: 'memberOf', translationId: 'user.objects.memberOf' },
  ];

  return (
    <section>
      <Box position="relative">
        <Tabs defaultIndex={1}>
          {isAdminOfObjects && ( // show the 2 different nav tabs only if user is admin of objects
            <TabListUser tw="flex-nowrap pr-4 h-16">
              {tabs.map(({ translationId }) => (
                <TabUser>{t(translationId)}</TabUser>
              ))}
            </TabListUser>
          )}
          {
            <TabPanels>
              <TabPanel>
                {/* Objects user is admin of */}
                <TabContent
                  projects={projectsAdmin}
                  // groups={groupsAdmin}
                  challenges={challengesAdmin}
                  programs={programsAdmin}
                  spaces={spacesAdmin}
                />
              </TabPanel>
              <TabPanel>
                {/* Objects user is member of */}
                <TabContent
                  projects={userProjects}
                  // groups={userGroups}
                  challenges={userChallenges}
                  programs={userPrograms}
                  spaces={userSpaces}
                />
              </TabPanel>
            </TabPanels>
          }
        </Tabs>
      </Box>
    </section>
  );
};

const TabContent = ({ projects, challenges, programs, spaces }) => {
  const { t } = useTranslation('common');
  return (
    <>
      {projects?.length !== 0 && ( // projects list
        <Box mb={4} pb={8} borderBottom="2px solid lightgrey">
          <h3>{t('user.profile.tab.projects')}</h3>
          <ProjectList listProjects={projects} />
        </Box>
      )}
      {/* {groups?.length !== 0 && ( // groups list
        <Box mb={4} pb={8} borderBottom="2px solid lightgrey">
          <h3>{t('user.profile.tab.communities')}</h3>
          <CommunityList listCommunities={groups} />
        </Box>
      )} */}
      {challenges?.length !== 0 && ( // challenges list
        <Box mb={4} pb={8} borderBottom="2px solid lightgrey">
          <h3>{t('general.challenges')}</h3>
          <Grid tw="py-4">
            {!challenges ? (
              <Loading />
            ) : (
              challenges?.map((challenge, i) => (
                <ChallengeCard
                  key={i}
                  id={challenge.id}
                  short_title={challenge.short_title}
                  title={challenge.title}
                  title_fr={challenge.title_fr}
                  short_description={challenge.short_description}
                  short_description_fr={challenge.short_description_fr}
                  membersCount={challenge.members_count}
                  needsCount={challenge.needs_count}
                  has_saved={challenge.has_saved}
                  clapsCount={challenge.claps_count}
                  status={challenge.status}
                  program={challenge.program}
                  space={challenge.space}
                  customType={challenge.custom_type}
                  projectsCount={challenge.projects_count}
                  banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                />
              ))
            )}
          </Grid>
        </Box>
      )}
      {programs?.length !== 0 && ( // programs list
        <Box mb={4} pb={8} borderBottom="2px solid lightgrey">
          <h3>{t('general.programs')}</h3>
          <Grid tw="py-4">
            {!programs ? (
              <Loading />
            ) : (
              programs.map((program, i) => (
                <ProgramCard
                  key={i}
                  id={program.id}
                  short_title={program.short_title}
                  title={program.title}
                  title_fr={program.title_fr}
                  short_description={program.short_description}
                  short_description_fr={program.short_description_fr}
                  membersCount={program.members_count}
                  needsCount={program.needs_count}
                  has_saved={program.has_saved}
                  projectsCount={program.projects_count}
                  banner_url={program.banner_url || '/images/default/default-program.jpg'}
                />
              ))
            )}
          </Grid>
        </Box>
      )}
      {spaces?.length !== 0 && ( // spaces list
        <>
          <h3>{t('general.spaces')}</h3>
          <Grid tw="py-4">
            {!spaces ? (
              <Loading />
            ) : (
              spaces.map((space, i) => (
                <SpaceCard
                  key={i}
                  id={space.id}
                  short_title={space.short_title}
                  title={space.title}
                  title_fr={space.title_fr}
                  short_description={space.short_description}
                  short_description_fr={space.short_description_fr}
                  membersCount={space.members_count}
                  needsCount={space.needs_count}
                  has_saved={space.has_saved}
                  clapsCount={space.claps_count}
                  projectsCount={space.projects_count}
                  spaceType={space.space_type}
                  banner_url={space.banner_url || '/images/default/default-space.jpg'}
                />
              ))
            )}
          </Grid>
        </>
      )}
    </>
  );
};

export default UserMenu;
