import React, { useState, useMemo, FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import useGet from 'hooks/useGet';
import Button from 'components/primitives/Button';
import Alert from 'components/Tools/Alert';
import Loading from '../Tools/Loading';
import { Challenge } from 'types';
import SpinLoader from '../Tools/SpinLoader';

interface PropsModal {
  alreadyPresentChallenges: Challenge[];
  spaceId: number;
  mutateChallenges: (data?: any, shouldRevalidate?: boolean) => Promise<any>;
  closeModal: () => void;
}
export const ChallengeLinkToSpace: FC<PropsModal> = ({
  alreadyPresentChallenges,
  spaceId,
  mutateChallenges,
  closeModal,
}) => {
  const { data: dataChallengesMine, error } = useGet('/api/challenges/mine');
  const [selectedChallenge, setSelectedChallenge] = useState(null);
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();
  const { t } = useTranslation('common');

  // Filter the challenges that are already in this challenge so you don't add it twice!
  // (+ challenges that already belong to a program or a space)
  const filteredChallenges = useMemo(() => {
    if (dataChallengesMine) {
      return dataChallengesMine
        .filter(({ program }) => program.id === -1) // check that does not belong to another program
        .filter(({ spaces }) => spaces.length === 0) // and space
        .filter((challengeMine) => {
          // Check if my challenge is found in alreadyPresentChallenges
          const isMyChallengeAlreadyPresent = alreadyPresentChallenges.find((alreadyPresentChallenge) => {
            return alreadyPresentChallenge.id === challengeMine.id;
          });
          // We keep only the ones that are not present
          return !isMyChallengeAlreadyPresent;
        });
    }
  }, [dataChallengesMine, alreadyPresentChallenges]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Affiliate this challenge to the space then mutate the cache of the challenges from the parent prop
    if ((selectedChallenge as { id: number })?.id) {
      await api
        .post(`/api/spaces/${spaceId}/affiliations/challenges/${selectedChallenge.id}`)
        .catch(() =>
          console.error(`Could not PUT/affiliate space n° ${spaceId} with challenge n° ${selectedChallenge.id}`)
        );
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
      mutateChallenges({ challenges: [...alreadyPresentChallenges, selectedChallenge] });
      // close modal after 3.5sec
      setTimeout(() => {
        closeModal();
      }, 3500);
    }
  };
  const onChallengeselect = (e) => {
    setSelectedChallenge(filteredChallenges.find((item) => item.id === parseInt(e.target.value)));
    setIsButtonDisabled(false);
  };

  return (
    <div>
      {!filteredChallenges ? (
        <Loading />
      ) : filteredChallenges && filteredChallenges.length > 0 ? (
        <form style={{ textAlign: 'left' }}>
          {filteredChallenges.map((challenge, index) => (
            <div className="form-check" key={index} style={{ height: '50px' }}>
              <input
                type="radio"
                className="form-check-input"
                name="exampleRadios"
                id={`challenge-${index}`}
                value={challenge.id}
                onChange={onChallengeselect}
              />
              <label className="form-check-label" htmlFor={`challenge-${index}`}>
                {challenge.title}
              </label>
            </div>
          ))}

          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} tw="mb-2">
              <>
                {sending && <SpinLoader />}
                {t('attach.challenge.btnSend')}
              </>
            </Button>
            {/* {requestSent && <Alert type="success" message={t('attach.challenge.btnSendEnded')} />} */}
            {requestSent && <Alert type="success" message="The challenge was affiliated" />}
          </div>
        </form>
      ) : (
        <div className="noChallenge" style={{ textAlign: 'center' }}>
          {t('attach.challenge.noChallenge')}
        </div>
      )}
    </div>
  );
};
