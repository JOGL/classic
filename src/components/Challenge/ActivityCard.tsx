/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import { Challenge } from 'types';

export type ChallengeType = Pick<
  Challenge,
  'title' | 'title_fr' | 'short_description' | 'short_description_fr' | 'short_title' | 'banner_url' | 'custom_type'
>;
const ActivityCard: FC<ChallengeType> = ({
  title,
  title_fr,
  short_title,
  link,
  short_description,
  short_description_fr,
  banner_url = '/images/default/default-challenge.jpg',
  custom_type,
}) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const activityUrl = short_title ? `/challenge/${short_title}` : link;
  const customType = custom_type === 'Challenge' ? t('challenge.title') : custom_type;

  return (
    // hrefNewTab indicates that clicking link on the objectCard banner will open it in new tab
    <ObjectCard imgUrl={banner_url} href={activityUrl} chip={customType} hrefNewTab={link}>
      {/* Title */}
      <Link href={activityUrl} passHref>
        <Title target={link && '_blank'}>
          <H2 tw="word-break[break-word] text-2xl">{(locale === 'fr' && title_fr) || title}</H2>
        </Title>
      </Link>
      {/* <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)] mt-2 pt-2" /> */}
      {/* Description */}
      <div tw="line-clamp-4 flex-1 mt-4">{(locale === 'fr' && short_description_fr) || short_description}</div>
    </ObjectCard>
  );
};

export default ActivityCard;
