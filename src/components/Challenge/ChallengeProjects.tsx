import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import useUserData from 'hooks/useUserData';
import { Project } from 'types';
import Box from '../Box';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import P from '../primitives/P';
import ProjectCard from '../Project/ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';

interface Props {
  challengeId: number;
}

const ChallengeProjects: FC<Props> = ({ challengeId }) => {
  const projectsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const {
    data: dataProjects,
    response,
    error,
    size,
    setSize,
  } = useInfiniteLoading<{
    projects: Project[];
  }>((index) => `/api/challenges/${challengeId}/projects?items=${projectsPerQuery}&page=${index + 1}`);

  const projects = dataProjects ? [].concat(...dataProjects?.map((d) => d.projects)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataProjects && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataProjects && typeof dataProjects[size - 1] === 'undefined');
  const isEmpty = dataProjects?.[0]?.length === 0;
  const isReachingEnd = isEmpty || projects?.length === totalNumber;

  return (
    <div>
      <Box>
        <P>{t('general.attached_projects_explanation')}</P>
        {!userData && ( // if user is not connected
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.project')}
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        {!dataProjects ? (
          <Loading />
        ) : projects?.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Grid tw="pb-4">
            {
              // don't display pending projects
              projects
                ?.filter(
                  ({ challenges }) =>
                    // find challenge project_status by accessing object with same challenge_id as the accessed challenge id
                    challenges.find((obj) => obj.challenge_id === challengeId).project_status !== 'pending'
                )
                .map((project, index) => (
                  <ProjectCard
                    key={index}
                    id={project.id}
                    title={project.title}
                    shortTitle={project.short_title}
                    short_description={project.short_description}
                    members_count={project.members_count}
                    needs_count={project.needs_count}
                    followersCount={project.followers_count}
                    savesCount={project.saves_count}
                    postsCount={project.posts_count}
                    has_saved={project.has_saved}
                    skills={project.skills}
                    banner_url={project.banner_url}
                    reviewsCount={project.reviews_count}
                  />
                ))
            }
          </Grid>
        )}

        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          totalNumber > projectsPerQuery && size <= response?.[0].headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default ChallengeProjects;
