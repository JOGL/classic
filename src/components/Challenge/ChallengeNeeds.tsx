import useTranslation from 'next-translate/useTranslation';
import useUserData from 'hooks/useUserData';
import { Need } from 'types';
import Box from '../Box';
import Grid from '../Grid';
import { FC } from 'react';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import Button from '../primitives/Button';
import P from '../primitives/P';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from 'hooks/useInfiniteLoading';

interface Props {
  challengeId: number;
}

const ChallengeNeeds: FC<Props> = ({ challengeId }) => {
  const needsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const {
    data: dataNeeds,
    response,
    error,
    size,
    setSize,
  } = useInfiniteLoading<{
    needs: Need[];
  }>((index) => `/api/challenges/${challengeId}/needs?items=${needsPerQuery}&page=${index + 1}`);

  const needs = dataNeeds ? [].concat(...dataNeeds?.map((d) => d.needs)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataNeeds && !error;
  const isLoadingMore = isLoadingInitialData || (size > 0 && dataNeeds && typeof dataNeeds[size - 1] === 'undefined');
  const isEmpty = dataNeeds?.[0]?.length === 0;
  const isReachingEnd = isEmpty || needs?.length === totalNumber;

  return (
    <div>
      <Box>
        <P>{t('general.attached_needs_explanation')}</P>
        {!userData && ( // if user is not connected
          <A href="/signIn">
            {t('header.signIn')} {t('program.signinCta.need')}
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        {!dataNeeds ? (
          <Loading />
        ) : needs?.length === 0 ? (
          <NoResults type="need" />
        ) : (
          <Grid tw="pb-4">
            {needs?.map((need, i) => (
              <NeedCard
                key={i}
                title={need.title}
                project={need.project}
                skills={need.skills}
                resources={need.ressources}
                hasSaved={need.has_saved}
                id={need.id}
                postsCount={need.posts_count}
                membersCount={need.members_count}
                publishedDate={need.created_at}
                dueDate={need.end_date}
                status={need.status}
              />
            ))}
          </Grid>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          totalNumber > needsPerQuery && size <= response?.[0].headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default ChallengeNeeds;
