import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC, Fragment, useContext } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H1 from 'components/primitives/H1';
import { useModal } from 'contexts/modalContext';
import { UserContext } from 'contexts/UserProvider';
import useGet from 'hooks/useGet';
import { Challenge, Project } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import styled from 'utils/styled';
import Box from '../Box';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import { ProjectLinkModal } from '../Project/ProjectLinkModal';
import BtnFollow from 'components/Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import BtnStar from '../Tools/BtnStar';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import { StatusCircle, statusToStep } from './ChallengeStatus';
import P from '../primitives/P';
import BasicChip from '../BasicChip';
// import "./ChallengeHeader.scss";

const StatusStep = styled(Box)`
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    transform: rotate(135deg) translateY(-5px) translateX(6px) !important;
  }
`;
interface Props {
  challenge: Challenge;
  lang: string;
}
const ChallengeHeader: FC<Props> = ({ challenge, lang = 'en' }) => {
  const { t } = useTranslation('common');
  const user = useContext(UserContext);
  const router = useRouter();
  const { showModal, closeModal } = useModal();
  const { data: projectsData, mutate: mutateProjects } = useGet<{
    projects: Project[];
  }>(`/api/challenges/${challenge.id}/projects`);

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const Stats = ({ value, title }) => (
    <div tw="justify-center items-center">
      <div>{value}</div>
      <div>{title}</div>
    </div>
  );

  const {
    has_followed,
    has_saved,
    id,
    is_member,
    is_owner,
    members_count,
    followers_count,
    projects_count,
    saves_count,
    needs_count,
    status,
    title,
    title_fr,
    program,
    space,
    short_description,
    short_description_fr,
    custom_type,
  } = challenge;

  // challenge can have different naming
  const customChalName =
    program.id !== -1 // if it's attached to a program
      ? program.custom_challenge_name
        ? program.custom_challenge_name.slice(0, -1) // have it being custom name chosen by program admin
        : t('challenge.title') // or generic "challenge" name
      : // else it's attached to a space, so use custom type defined by challenge admin
      custom_type === 'Challenge'
      ? t('challenge.title')
      : custom_type;

  // dynamically set challengeStatus to completed if date is over and admin forgot to set it as complete, else show normal status
  // const challengeStatus = final_date && getDaysLeft(final_date) === 0 ? 'completed' : status;
  const statusStep = statusToStep(status);
  return (
    <div className="challengeHeader">
      <Box px={[3, undefined, undefined, undefined, 0]} pb={5}>
        <Box alignItems="center" width={'full'} mb={[6, undefined, undefined, 8]} pt={5}>
          <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
            <H1 pr="2" fontSize={['2.55rem', undefined, '3rem']}>
              {(lang === 'fr' && title_fr) || title}
            </H1>
            {/* edit button */}
            {challenge.is_admin && (
              <Link href={`/challenge/${challenge.short_title}/edit`}>
                <a tw="flex justify-center pr-2 cursor-pointer">
                  <Edit size={23} title="Edit challenge" />
                  {t('entity.form.btnAdmin')}
                </a>
              </Link>
            )}
            <ShareBtns type="challenge" specialObjId={id} />
          </Box>
          <div tw="inline-flex space-x-2">
            <BasicChip tw="flex self-center mt-1 ml-4">{customChalName}</BasicChip>
          </div>
        </Box>
        <div tw="grid gridTemplateColumns[100%] lg:gridTemplateColumns[22% 50% 28%]">
          <Box
            row
            alignSelf="flex-start"
            alignItems="center"
            justifyContent="center"
            justifySelf="center"
            spaceX={4}
            flexWrap="wrap"
            order={[2, undefined, undefined, 1]}
            py={{ _: 4, md: 0 }}
          >
            {program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
              <Box row pb={3}>
                <strong>{t('entity.info.program_title')}&nbsp;</strong>
                <Link href={`/program/${program.short_title}`}>
                  <a>{program.title}</a>
                </Link>
              </Box>
            )}
            {space.id !== -1 && ( // if space id is !== -1 (meaning challenge is not attached to a space), display space name and link
              <Box row pb={3}>
                <strong>{t('space.title')}:&nbsp;</strong>
                <Link href={`/space/${space.short_title}`}>
                  <a>{space.title}</a>
                </Link>
              </Box>
            )}
            {status !== 'draft' && (
              <Box textAlign={['center', undefined, 'inherit']}>
                <Box
                  display="grid"
                  gridTemplateColumns={['27% 53%', undefined, undefined, '33% 53%']}
                  justifyContent="center"
                  alignItems="center"
                  justifyItems="center"
                  width="12rem"
                  margin={['auto', undefined, undefined, 'inherit']}
                  style={{ paddingBottom: '15px' }}
                >
                  <>
                    {/* where statusStep[0] = step number, and statusStep[1] = step color */}
                    <StatusCircle mr={2} step={statusStep[0]}>
                      <StatusStep>{statusStep[0]}/4</StatusStep>
                    </StatusCircle>
                    <Box color={statusStep[1]}>{t(`challenge.info.status_${status}`)}</Box>
                  </>
                </Box>
              </Box>
            )}
            {/* show submit button only for connected users, and only if status of challenge is accepting projects + challenge is attached to a program */}
            {user.isConnected && status === 'accepting' && (program.id !== -1 || space.id !== -1) && (
              <Button
                onClick={() => {
                  showModal({
                    children: (
                      <ProjectLinkModal
                        alreadyPresentProjects={projectsData?.projects}
                        challengeId={id}
                        programId={program.id}
                        spaceId={space.id}
                        mutateProjects={mutateProjects}
                        closeModal={closeModal}
                        isMember={is_member}
                        hasFollowed={has_followed}
                      />
                    ),
                    title: t('attach.project.title'),
                    maxWidth: '50rem',
                  });
                }}
              >
                {t('attach.project.title')}
              </Button>
            )}
            {/* <span tw="text-gray-500">{t('challenge.info.status_draft')}</span> */}
          </Box>
          <Box textAlign="center" px={4} order={[1, undefined, undefined, 2]}>
            <P fontSize="17.5px">{(lang === 'fr' && short_description_fr) || short_description}</P>
          </Box>
          <Box order={3} pt={[4, undefined, undefined, 0]}>
            <div tw="flex flex-wrap gap-x-2 gap-y-2 mb-6 self-center justify-center lg:self-end">
              {!is_owner && ( // show join button only if we are not owner of challenge
                <BtnJoin
                  joinState={is_member}
                  itemType="challenges"
                  itemId={id}
                  count={members_count}
                  showMembersModal={() =>
                    router.push(`/challenge/${router.query.short_title}?tab=members`, undefined, { shallow: true })
                  }
                />
              )}
              <BtnFollow
                followState={has_followed}
                itemType="challenges"
                itemId={challenge.id}
                count={followers_count}
              />
              <BtnStar itemType="challenges" itemId={id} hasStarred={has_saved} count={saves_count} />
            </div>
            <div tw="text-center items-center self-center justify-center space-x-6 w-full inline-flex pb-2 px-0 md:(px-4 mt-6)">
              <A href={`/challenge/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
                <Stats value={projects_count} title={<TextWithPlural type="project" count={projects_count} />} />
              </A>
              <div tw="border-right[2px solid] border-gray-300 h-8" />
              <A href={`/challenge/${router.query.short_title}?tab=needs`} shallow noStyle scroll={false}>
                <Stats value={needs_count} title={<TextWithPlural type="need" count={needs_count} />} />
              </A>
              <div tw="border-right[2px solid] border-gray-300 h-8" />
              <A href={`/challenge/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
                <Stats value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
              </A>
            </div>
          </Box>
        </div>
      </Box>
    </div>
  );
};
export default ChallengeHeader;
