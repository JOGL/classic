import useTranslation from 'next-translate/useTranslation';
import { ReactNode, useEffect, useState, FC } from 'react';
import { useRouter } from 'next/router';
import ChallengeForm from 'components/Challenge/ChallengeForm';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import useUserData from 'hooks/useUserData';
import Button from '../primitives/Button';

interface Props {
  isModal?: boolean;
  objectId?: number;
  objectType?: string;
  customChalName?: string;
  closeModal?: () => void;
  callBack?: () => void;
}

const ChallengeCreate: FC<Props> = ({ isModal, closeModal, objectId, objectType, customChalName, callBack }) => {
  const api = useApi();
  const router = useRouter();
  const { t } = useTranslation('common');
  const [error, setError] = useState<string | ReactNode>('');
  const [sending, setSending] = useState(false);
  const [hasCreated, setHasCreated] = useState(false);
  const [newChallenge, setNewChallenge] = useState({
    title: '',
    title_fr: '',
    short_title: '',
    short_description: '',
    short_description_fr: '',
    interests: [],
    skills: [],
    status: 'draft',
    creator_id: undefined,
    space_id: objectId,
    custom_type: 'Challenge',
  });
  const customChalNameSing = customChalName ? customChalName.slice(0, -1) : newChallenge.custom_type;

  const { userData } = useUserData();
  // canCreate condition not here, left to father elements
  useEffect(() => {
    if (userData) {
      handleChange('creator_id', userData.id);
    }
  }, [userData]);

  const handleChange = (key, content) => {
    setNewChallenge((prev) => ({ ...prev, [key]: content }));
    setError('');
  };

  const handleSubmit = () => {
    if (newChallenge.title !== '' && newChallenge.short_description !== '' && newChallenge.short_title !== '') {
      setSending(true);
      api
        // check if short_title already exists
        .get(`/api/challenges/exists/${newChallenge.short_title}`)
        // if it's not the case, then we create the challenge
        .then((res) => {
          if (res.data.data === 'short_title is available') {
            api
              .post('/api/challenges/', { challenge: newChallenge }) // create challenge
              .then((res) => {
                // if isModal (= if we created the challenge from a modal in an object edit page)
                if (isModal) {
                  objectType === 'programs' && api.put(`/api/${objectType}/${objectId}/challenges/${res.data.id}`); // link
                  setHasCreated(true);
                  // add challenge to the challenge list
                  setTimeout(() => callBack(), 500);
                }
                // redirect us to the challenge edit page, if we didn't create the challenge through modal
                !isModal && router.push(`/challenge/${res.data.short_title}/edit`);
              })
              .catch(() => setSending(false));
          }
        })
        // else, show error message
        .catch((err) => {
          setSending(false);
          if (err.response.data.data === 'short_title already exists') {
            setError(t('err-4006'));
          }
        });
    } else {
      // show error msg if title, short_desc or short_title are missing
      setError(t('challenge.create.error'));
    }
  };

  return (
    <>
      {!hasCreated && (
        <>
          <ChallengeForm
            challenge={newChallenge}
            handleChange={handleChange}
            handleSubmit={handleSubmit}
            mode="create"
            sending={sending}
            isModal={isModal}
          />
          {error !== '' && <Alert type="danger" message={error} />}
        </>
      )}
      {/* show this after challenge was created from within a modal */}
      {hasCreated && (
        <>
          <Alert type="success" message={t('challenge.created.confirm', { challenge_wording: customChalNameSing })} />
          <p tw="italic">{t('challenge.created.infoMsg')}</p>
          <div tw="flex space-x-2 mt-5">
            <Button onClick={() => router.push(`/challenge/${newChallenge.short_title}/edit`)}>
              {t('challenge.edit.short')}
            </Button>
            <Button btnType="secondary" onClick={closeModal}>
              {t('general.close')}
            </Button>
          </div>
        </>
      )}
    </>
  );
  // eslint-disable-next-line @rushstack/no-null
  return null;
};

export default ChallengeCreate;
