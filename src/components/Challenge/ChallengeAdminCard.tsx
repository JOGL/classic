import React, { FC, ReactNode, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Alert from 'components/Tools/Alert';
import { useApi } from 'contexts/apiContext';
import { Challenge } from 'types';
import A from '../primitives/A';
import Box from '../Box';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  challenge: Challenge;
  parentType: 'programs' | 'spaces';
  parentId: number;
  callBack: () => void;
  showChalType?: boolean;
}
const ChallengeAdminCard: FC<Props> = ({ challenge, parentType, parentId, callBack, showChalType = false }) => {
  const [sending, setSending] = useState<'remove' | 'accepted' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const [imgToDisplay, setImgToDisplay] = useState('/images/default/default-challenge.jpg');
  const api = useApi();
  const { t } = useTranslation('common');
  const route = `/api/${parentType}/${parentId}/challenges/${challenge.id}`;

  const onSuccess = () => {
    setSending(undefined);
    callBack();
  };

  const onError = (err) => {
    console.error(err);
    setSending(undefined);
    setError(t('err-'));
  };

  const acceptChallenge = () => {
    setSending('accepted');
    api
      .post(route, { status: 'accepted' })
      .then(() => onSuccess())
      .catch((err) => onError(err));
  };

  const removeChallenge = () => {
    setSending('remove');
    api
      .delete(route)
      .then(() => onSuccess())
      .catch((err) => onError(err));
  };

  const isChallengePending = false;

  useEffect(() => {
    challenge.logo_url && setImgToDisplay(challenge.logo_url);
  }, [challenge.logo_url]);

  const bgLogo = {
    backgroundImage: `url(${imgToDisplay})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (challenge) {
    return (
      <Box flexDirection={['column', 'row']} justifyContent="space-between" key={challenge.id} tw="py-3">
        <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
          <div style={{ width: '50px' }}>
            <div style={bgLogo} />
          </div>
          <Box>
            <span>
              <A href={`/challenge/${challenge.short_title}`}>{challenge.title}</A>
              {showChalType && <span tw="font-size[90%]">{` (${challenge.custom_type})`}</span>}
            </span>
            <Box fontSize="85%" pt={1}>
              {t('attach.status')}
              {t(`challenge.info.status_${challenge.status}`)}
            </Box>
          </Box>
        </Box>
        <Box row alignItems="center" justifyContent={['space-between', 'flex-end']}>
          <Box pr={8}>
            {t('attach.members')}
            {challenge.members_count}
          </Box>

          {isChallengePending ? ( // if challenge status is pending, display the accept/reject buttons
            <Box flexDirection={['column', 'row']}>
              <button
                type="button"
                className="btn btn-outline-success"
                style={{ marginBottom: '5px', marginLeft: '8px' }}
                disabled={sending === 'accepted'}
                onClick={acceptChallenge}
              >
                {sending === 'accepted' && <SpinLoader />}
                {t('general.accept')}
              </button>
              <button
                type="button"
                className="btn btn-outline-danger"
                style={{ marginBottom: '5px', marginLeft: '8px' }}
                disabled={sending === 'remove'}
                onClick={removeChallenge}
              >
                {sending === 'remove' && <SpinLoader />}
                {t('general.reject')}
              </button>
            </Box>
          ) : (
            <Button btnType="danger" disabled={sending === 'remove'} onClick={removeChallenge} tw="ml-3">
              {sending === 'remove' && <SpinLoader />}
              {t('general.remove')}
            </Button>
          )}
          {error && <Alert type="danger" message={error} />}
        </Box>
      </Box>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ChallengeAdminCard;
