import { useRouter } from 'next/router';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import PostDisplay from 'components/Feed/Posts/PostDisplay';
import H2 from 'components/primitives/H2';
import useGet from 'hooks/useGet';
import useUserData from 'hooks/useUserData';
import { Need, Post, Project } from 'types';
import Box from '../Box';
import Carousel from '../Carousel';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import ProjectCard from '../Project/ProjectCard';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

interface Props {
  challengeId: number;
  challengeFeedId: number;
  meetingInfo: string;
  isAdmin?: boolean;
  posts?: Post[];
}
const ChallengeHome: FC<Props> = ({ challengeId, challengeFeedId, meetingInfo, isAdmin = false, posts }) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const { userData } = useUserData();
  const { data: dataProjects } = useGet<{ projects: Project[] }>(`/api/challenges/${challengeId}/projects?items=7`);
  const { data: dataNeeds } = useGet<{ needs: Need[] }>(`/api/challenges/${challengeId}/needs?items=5`);

  return (
    <Box spaceY={3} position="relative">
      {/* <Box pb={5}>
        <P display={['none', undefined, 'block']}>{shortDescription}</P>
        <A href={`/challenge/${router.query.short_title}?tab=about`} shallow>
          {t('program.home.learnMore')}
        </A>
      </Box> */}
      {/* Projects section */}
      <Box pb={8}>
        <Box row justifyContent="space-between">
          <H2>{t('program.featuredProjects')}</H2>
          <A href={`/challenge/${router.query.short_title}?tab=projects`} shallow scroll={false}>
            {t('program.seeAll')}
          </A>
        </Box>
        {!dataProjects ? (
          <Loading />
        ) : dataProjects?.projects.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Carousel spaceX={12}>
            {[...dataProjects.projects]
              // don't display pending projects
              .filter(
                ({ challenges }) =>
                  // find challenge project_status by accessing object with same challenge_id as the accessed challenge id
                  challenges.find((obj) => obj.challenge_id === challengeId).project_status !== 'pending'
              )
              // display only seven last projects
              .splice(0, 7)
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  followersCount={project.followers_count}
                  savesCount={project.saves_count}
                  postsCount={project.posts_count}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  has_saved={project.has_saved}
                  skills={project.skills}
                  banner_url={project.banner_url}
                  reviewsCount={project.reviews_count}
                  width="18.4rem"
                />
              ))}
          </Carousel>
        )}
        <Box pt={4}>
          <A href="/project/create">{t('program.ideaProject')}</A>
        </Box>
      </Box>
      {/* Needs section */}
      <Box pb={8}>
        <Box row justifyContent="space-between">
          <H2>{t('program.latestNeeds')}</H2>
          <A href={`/challenge/${router.query.short_title}?tab=needs`} shallow scroll={false}>
            {t('program.seeAll')}
          </A>
        </Box>
        {!dataNeeds?.needs ? (
          <Loading />
        ) : dataNeeds?.needs?.length === 0 ? (
          <NoResults type="need" />
        ) : (
          <Carousel spaceX={12}>
            {[...dataNeeds?.needs].map((need, i) => (
              <NeedCard
                key={i}
                title={need.title}
                project={need.project}
                skills={need.skills}
                resources={need.ressources}
                hasSaved={need.has_saved}
                id={need.id}
                postsCount={need.posts_count}
                membersCount={need.members_count}
                publishedDate={need.created_at}
                dueDate={need.end_date}
                status={need.status}
                width="18.4rem"
              />
            ))}
          </Carousel>
        )}
        <Box pt={4}>
          <A href="/search/needs">{t('program.needsMatch')}</A>
        </Box>
      </Box>
      {/* Posts bloc */}
      <Box pb={8}>
        <Box row justifyContent="space-between">
          <H2>{t('program.announcements')}</H2>
          <A href={`/challenge/${router.query.short_title}?tab=news`} shallow scroll={false}>
            {t('program.seeAll')}
          </A>
        </Box>
        {!posts ? (
          <Loading />
        ) : posts?.length === 0 ? (
          <NoResults type="post" />
        ) : (
          <Carousel spaceX={12}>
            {[...posts].map((post, i) => (
              <PostDisplay
                post={post}
                key={i}
                feedId={challengeFeedId}
                user={userData}
                isAdmin={isAdmin}
                cardNoComments
                width="18.4rem"
              />
            ))}
          </Carousel>
        )}
      </Box>
      {/* Meeting info bloc */}
      {meetingInfo && (
        <Box
          display={['flex', undefined, undefined, undefined, 'none']}
          bg="white"
          shadow
          border="2px solid lightgrey"
          borderRadius="1rem"
          p={3}
        >
          <H2>{t('program.meeting_information')}</H2>
          <InfoHtmlComponent content={meetingInfo} />
        </Box>
      )}
    </Box>
  );
};
export default ChallengeHome;
