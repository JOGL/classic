import { FC, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
/** * Form objects ** */
import FormDefaultComponent from 'components/Tools/Forms/FormDefaultComponent';
import FormDropdownComponent from 'components/Tools/Forms/FormDropdownComponent';
import FormImgComponent from 'components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from 'components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from 'components/Tools/Forms/FormSkillsComponent';
import FormTextAreaComponent from 'components/Tools/Forms/FormTextAreaComponent';
import FormWysiwygComponent from 'components/Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from 'components/Tools/Forms/FormValidator';
import challengeFormRules from './challengeFormRules.json';
import Box from '../Box';
import { Challenge } from 'types';
import { changesSavedConfAlert } from 'utils/utils';
import { useRouter } from 'next/router';
import SpinLoader from '../Tools/SpinLoader';
import CreatableSelect from 'react-select/creatable';
import TitleInfo from '../Tools/TitleInfo';
import AllowPostingToAllToggle from '../Tools/AllowPostingToAllToggle';
/** * Images/Style ** */

interface Props {
  mode: 'edit' | 'create';
  challenge: Challenge;
  sending: boolean;
  hasUpdated: boolean;
  isModal?: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}

const ChallengeForm: FC<Props> = ({
  mode,
  challenge,
  sending = false,
  hasUpdated = false,
  isModal,
  handleChange,
  handleSubmit,
}) => {
  const { t } = useTranslation('common');
  const router = useRouter();
  const validator = new FormValidator(challengeFormRules);
  const [stateValidation, setStateValidation] = useState({});
  const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } =
    stateValidation || '';
  const urlBack = mode === 'edit' ? `/challenge/${challenge.short_title}` : '/search/challenges';

  // show conf message when project has been successfully saved/updated
  hasUpdated &&
    changesSavedConfAlert(t)
      .fire()
      // go back to project page if user clicked on conf button
      .then(({ isConfirmed }) => isConfirmed && router.push(urlBack));

  const handleChangeChallenge = (key, content) => {
    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = validator.validate(state);
    if (validation[key] !== undefined) {
      const newStateValidation = {};
      newStateValidation[`valid_${key}`] = validation[key];
      setStateValidation(newStateValidation);
    }
    /* Validators end */
    handleChange(key, content);
  };
  const handleChangeType = (key, content) => {
    handleChange(content.name, key.value);
  };

  const handleSubmitChallenge = () => {
    /* Validators control before submit */
    const validation = validator.validate(challenge);
    if (validation.isValid) {
      handleSubmit();
    } else {
      const newStateValidation = {};
      let firstError = true;
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 130; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          newStateValidation[`valid_${key}`] = validation[key];
        }
      });
      setStateValidation(newStateValidation);
    }
  };

  const renderFormBtns = () => {
    const textAction = mode === 'edit' ? 'Update' : 'Next';
    return (
      <Box row justifyContent="center" mt={9} mb={3}>
        {!isModal && (
          <Link href={urlBack}>
            <a>
              <button type="button" className="btn btn-outline-primary">
                {t('entity.form.back')}
              </button>
            </a>
          </Link>
        )}
        <button
          type="button"
          onClick={handleSubmitChallenge}
          className="btn btn-primary"
          disabled={sending}
          style={{ marginLeft: '10px' }}
        >
          {sending && <SpinLoader />}
          {t(`entity.form.btn${textAction}`)}
        </button>
      </Box>
    );
  };
  const challengeTypes = ['Challenge', 'Hackathon', 'Track'];
  const challengeTypesList = challengeTypes.map((chalType) => ({ value: chalType, label: chalType }));

  return (
    <form className="challengeForm">
      <FormDefaultComponent
        content={challenge.title}
        errorCodeMessage={valid_title ? valid_title.message : ''}
        id="title"
        isValid={valid_title ? !valid_title.isInvalid : undefined}
        mandatory
        onChange={handleChangeChallenge}
        title={t('entity.info.title')}
        placeholder={t('challenge.form.title_placeholder')}
      />
      {mode === 'edit' && (
        <FormDefaultComponent
          content={challenge.title_fr}
          id="title_fr"
          onChange={handleChangeChallenge}
          title={t('entity.info.title_fr')}
          placeholder={t('challenge.form.title_placeholder')}
        />
      )}
      <FormDefaultComponent
        content={challenge.short_title}
        errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
        id="short_title"
        isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
        onChange={handleChangeChallenge}
        mandatory
        pattern={/[A-Za-z0-9]/g}
        title={t('entity.info.short_name')}
        placeholder={t('challenge.form.short_title_placeholder')}
        prepend="#"
      />
      <FormTextAreaComponent
        content={challenge.short_description}
        errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
        id="short_description"
        isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
        mandatory
        maxChar={340}
        onChange={handleChangeChallenge}
        rows={5}
        title={t('entity.info.short_description')}
        placeholder={t('challenge.form.short_description_placeholder')}
      />
      <FormInterestsComponent content={challenge.interests} onChange={handleChangeChallenge} />
      <FormSkillsComponent
        content={challenge.skills}
        id="skills"
        type="challenge"
        onChange={handleChangeChallenge}
        placeholder={t('general.skills.placeholder')}
        title={t('entity.info.skills')}
      />
      {mode === 'edit' && (
        <>
          <FormTextAreaComponent
            content={challenge.short_description_fr}
            id="short_description_fr"
            maxChar={340}
            onChange={handleChangeChallenge}
            rows={3}
            title={t('entity.info.short_description_fr')}
            placeholder={t('challenge.form.short_description_placeholder')}
          />
          <FormWysiwygComponent
            id="description"
            title={t('entity.info.description')}
            placeholder={t('challenge.form.description_placeholder')}
            content={challenge.description}
            onChange={handleChangeChallenge}
            show
          />
          <FormWysiwygComponent
            id="description_fr"
            title={t('entity.info.description_fr')}
            placeholder={t('challenge.form.description_placeholder')}
            content={challenge.description_fr}
            onChange={handleChangeChallenge}
          />
          {/* <FormWysiwygComponent
              id="rules"
              title={t('entity.info.rules')}
              placeholder={t('challenge.form.rules.placeholder')}
              content={challenge.rules}
              onChange={handleChangeChallenge}
            />
            <FormWysiwygComponent
              id="rules_fr"
              title={t('entity.info.rules_fr')}
              placeholder={t('challenge.form.rules.placeholder')}
              content={challenge.rules_fr}
              onChange={handleChangeChallenge}
            /> */}
          {/* <FormWysiwygComponent
              id="faq"
              title={t('entity.info.faq')}
              placeholder={t('challenge.form.faq.placeholder')}
              content={challenge.faq}
              onChange={handleChangeChallenge}
            />
            <FormWysiwygComponent
              id="faq_fr"
              title={t('entity.info.faq_fr')}
              placeholder={t('challenge.form.faq.placeholder')}
              content={challenge.faq_fr}
              onChange={handleChangeChallenge}
            /> */}
          <FormDropdownComponent
            id="status"
            type="challenge"
            warningMsg={t('entity.info.status.dropDownDraftWarningMsg')}
            title={t('entity.info.status.title')}
            content={challenge.status}
            options={['draft', 'soon', 'accepting', 'evaluating', 'active', 'completed']}
            onChange={handleChangeChallenge}
          />
        </>
      )}
      <div tw="py-2">
        <TitleInfo title={t('challenge.info.custom_type.title')} />
        <label className="form-check-label">{t('challenge.info.custom_type.label')}</label>
        <CreatableSelect
          name="custom_type"
          options={challengeTypesList}
          value={{ value: challenge.custom_type, label: challenge.custom_type }}
          formatCreateLabel={(inputValue) => `${t('challenge.info.custom_type.use')} "${inputValue}"`}
          onChange={handleChangeType}
          noOptionsMessage={() => null}
          menuShouldScrollIntoView={true} // force scroll into view
        />
      </div>
      {mode === 'edit' && (
        <>
          <FormImgComponent
            type="banner"
            id="banner_url"
            imageUrl={challenge.banner_url}
            itemId={challenge.id}
            itemType="challenges"
            title={t('challenge.info.banner_url')}
            content={challenge.banner_url}
            defaultImg="/images/default/default-challenge.jpg"
            onChange={handleChangeChallenge}
            tooltipMessage={t('challenge.info.banner_url_tooltip')}
          />
          <FormImgComponent
            id="logo_url"
            content={challenge.logo_url}
            title={t('challenge.info.logo_url')}
            imageUrl={challenge.logo_url}
            itemId={challenge.id}
            type="avatar"
            itemType="challenges"
            defaultImg="/images/default/default-program.jpg"
            onChange={handleChangeChallenge}
          />
          <FormDefaultComponent
            id="launch_date"
            content={challenge.launch_date && challenge.launch_date.split('T')[0]}
            onChange={handleChangeChallenge}
            type="date"
            title={t('entity.info.launch_date')}
          />
          <FormDefaultComponent
            id="final_date"
            content={challenge.final_date && challenge.final_date.split('T')[0]}
            onChange={handleChangeChallenge}
            type="date"
            title={t('entity.info.final_date')}
          />
          <FormDefaultComponent
            id="end_date"
            content={challenge.end_date && challenge.end_date.split('T')[0]}
            onChange={handleChangeChallenge}
            type="date"
            title={t('entity.info.end_date')}
          />
          <AllowPostingToAllToggle feedId={challenge.feed_id} />
          {/* <FormDefaultComponent
              id="address"
              title={formatMessage({ id: 'general.address')}
              placeholder={formatMessage({
                id: 'general.address.placeholder',
            ',
              })}
              content={challenge.address}
              onChange={handleChangeChallenge}
            /> */}
          {/* <FormDefaultComponent
              id="city"
              title={formatMessage({ id: 'general.city')}
              placeholder={formatMessage({
                id: 'general.city.placeholder',
            ',
              })}
              content={challenge.city}
              onChange={handleChangeChallenge}
            />
            <FormDefaultComponent
              id="country"
              title={formatMessage({ id: 'general.country')}
              placeholder={formatMessage({
                id: 'general.country.placeholder',
            ',
              })}
              content={challenge.country}
              onChange={handleChangeChallenge}
            /> */}
        </>
      )}
      {renderFormBtns()}
    </form>
  );
};
export default ChallengeForm;
