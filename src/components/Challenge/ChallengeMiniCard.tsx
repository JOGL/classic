import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from 'contexts/apiContext';
import styled from 'utils/styled';
import { useTheme } from 'utils/theme';
import Box from '../Box';
import Card from 'components/Cards/Card';
import Button from '../primitives/Button';

interface Props {
  icon: string;
  title: string;
  shortTitle: string;
  status?: string;
  projectId?: number;
  projectChallengeId?: number;
  onChallengeDelete?: (id: number) => void;
  isEditCard?: boolean;
}

const ChallengeMiniCard: FC<Props> = ({
  icon,
  title,
  shortTitle,
  status = 'pending',
  projectChallengeId,
  projectId,
  onChallengeDelete,
  isEditCard = false,
}) => {
  const theme = useTheme();
  const api = useApi();
  const { t } = useTranslation('common');
  const cancelParticipation = () => {
    api
      .delete(`/api/challenges/${projectChallengeId}/projects/${projectId}`)
      .then(() => onChallengeDelete(projectChallengeId))
      .catch((err) => console.error(`Couldn't remove project of challenge with id=${projectChallengeId}`, err));
  };

  return (
    <CustomCard>
      <Box height="100%" width={['14rem']} justifyContent="space-between">
        <Link href={`/challenge/${shortTitle}`}>
          <a>
            <Box justifyContent="center" alignItems="center" spaceY={6}>
              {icon && (
                <img src={icon} width="100%" alt="Challenge Icon" style={{ objectFit: 'cover', maxHeight: '110px' }} />
              )}
              <Box fontFamily="secondary" fontSize={5} px={4}>
                <h3 tw="line-clamp-2 hover:underline">{title}</h3>
              </Box>
            </Box>
          </a>
        </Link>
        {status && isEditCard && (
          <Box row justifyContent="space-around" mt={3}>
            <Box
              textAlign="center"
              width="fit-content"
              py={1}
              px={2}
              color={status === 'pending' ? 'black' : 'white'}
              borderRadius={5}
              alignSelf="center"
              backgroundColor={status === 'pending' ? theme.colors.warnings['700'] : theme.colors.successes['500']}
            >
              {t(`challenge.acceptState.${status}`)}
            </Box>
            <Button btnType="danger" onClick={cancelParticipation}>
              {t('attach.remove')}
            </Button>
          </Box>
        )}
      </Box>
    </CustomCard>
  );
};

const CustomCard = styled(Card)`
  border: 1px solid lightgray;
  transition: box-shadow 0.3s ease-in-out;
  a {
    color: ${(p) => p.theme.colors.greys['800']};
    text-decoration: none;
  }
`;

export default ChallengeMiniCard;
