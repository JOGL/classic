/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import Box from 'components/Box';
import ObjectCard from 'components/Cards/ObjectCard';
import H2 from 'components/primitives/H2';
import Title from 'components/primitives/Title';
import BtnStarIcon from 'components/Tools/BtnStarIcon';
import { DataSource, Program, Space } from 'types';
import { TextWithPlural } from 'utils/managePlurals';
import { statusToStep } from './ChallengeStatus';
import ReactTooltip from 'react-tooltip';
import Image2 from 'components/Image2';

export type Challenge = {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  membersCount: number;
  projectsCount: number;
  program?: Pick<Program, 'id' | 'short_title' | 'title' | 'title_fr'>;
  space?: Pick<Space, 'id' | 'short_title' | 'title' | 'title_fr'>;
  needsCount: number;
  has_saved?: boolean;
  banner_url: string;
  status: string;
  customType?: string;
};
interface Props extends Challenge {
  width?: string;
  source?: DataSource;
}
const ChallengeCard: FC<Props> = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  membersCount,
  projectsCount,
  program,
  space,
  needsCount = 0,
  has_saved,
  banner_url = '/images/default/default-challenge.jpg',
  width,
  source,
  status,
  customType,
}) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const challengeUrl = `/challenge/${short_title}`;
  const statusStep = statusToStep(status);
  // const affSpace = (space && space[0] && space[0][0]) || [];

  return (
    <ObjectCard
      imgUrl={banner_url}
      href={challengeUrl}
      width={width}
      // customize the chip depending on conditions
      chip={customType !== undefined ? (customType !== '' ? customType : t('challenge.title')) : ''}
    >
      {/* Title */}
      <div tw="inline-flex items-center md:h-16">
        <Link href={challengeUrl} passHref>
          <Title>
            {/* <H2 tw="word-break[break-word] text-2xl flex items-center md:(text-3xl h-16)"> */}
            <H2 tw="word-break[break-word] text-2xl line-clamp-2 items-center md:text-3xl">
              {(locale === 'fr' && title_fr) || title}
            </H2>
          </Title>
        </Link>
      </div>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
        <span> Last active today </span> <span> Prototyping </span>
      </div> */}
      <Hr tw="mt-2 pt-2" />
      {/* Challenge's program */}
      {program &&
        program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
          <div tw="inline-flex space-x-2 items-center">
            <Image2 src="/images/logo.svg" alt="jogl logo rocket" quality={25} tw="w-3!" />
            <div>
              {t('entity.info.program_title')}
              &nbsp;
              <Link href={`/program/${program.short_title}`} passHref>
                <a tw="text-black font-medium hover:(underline text-black)">
                  {(locale === 'fr' && program.title_fr) || program.title}
                </a>
              </Link>
            </div>
          </div>
        )}
      {/* Challenge's space */}
      {space &&
        space.id !== -1 && ( // if space id is !== -1 (meaning challenge is not attached to a space), display space name and link
          <div tw="inline-flex space-x-2 mt-0 items-center">
            <Image2 src="/images/logo.svg" alt="jogl logo rocket" quality={25} tw="w-3!" />
            <div>
              {t('space.title')}:&nbsp;
              <Link href={`/space/${space.short_title}`} passHref>
                <a tw="text-black font-medium hover:(underline text-black)">
                  {(locale === 'fr' && space.title_fr) || space.title}
                </a>
              </Link>
            </div>
          </div>
        )}
      {/* Status */}
      <div tw="inline-flex space-x-2 items-center">
        <div tw="rounded-full h-2 w-2" style={{ backgroundColor: status !== 'draft' ? statusStep[1] : 'grey' }} />
        <Box
          style={{ color: status !== 'draft' ? statusStep[1] : 'grey' }}
          row
          width="fit-content"
          borderRadius={6}
          alignItems="center"
          data-tip={t('entity.info.status.title')}
          data-for="challengeCard_status"
        >
          <div tw="font-medium">{t(`challenge.info.status_${status}`)}</div>
        </Box>
        <ReactTooltip id="challengeCard_status" effect="solid" />
      </div>
      <Hr tw="mt-2 pt-6" />
      {/* Description */}
      <div tw="line-clamp-4 flex-1">{(locale === 'fr' && short_description_fr) || short_description}</div>
      <Hr tw="mt-5 pt-2" />
      {/* Stats */}
      <div tw="items-center justify-around space-x-2 flex flex-wrap">
        <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
        <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
        <CardData value={needsCount} title={<TextWithPlural type="need" count={needsCount} />} />
      </div>
      {/* Star icon */}
      {(has_saved !== undefined || source === 'algolia') && (
        <BtnStarIcon itemType="challenges" itemId={id} saveState={has_saved} source={source} />
      )}
    </ObjectCard>
  );
};

const CardData = ({ value, title }) => (
  <div tw="flex flex-col justify-center items-center">
    <div tw="font-extrabold font-size[17px] -mb-1">{value}</div>
    <div tw="text-gray-500 font-medium text-sm">{title}</div>
  </div>
);
const Hr = (props) => <div tw="-mx-4 border-0 border-t border-solid border-color[rgba(0, 0, 0, 0.07)]" {...props} />;

export default ChallengeCard;
