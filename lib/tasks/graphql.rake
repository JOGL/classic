require 'graphql/rake_task'

GraphQL::RakeTask.new(schema_name: 'BackendSchema', json_outfile: 'doc/graphql/schema.json')
