# frozen_string_literal: true

namespace :data_migrations do
  desc 'update all members of an object to also follow that object'
  task auto_follow: :environment do
    class CreateRelation < ApplicationRecord
      self.table_name = 'relations'

      belongs_to :user
      belongs_to :resource, polymorphic: true
    end

    [Program, Challenge, Community, Project, Need].each do |klass|
      message =
        ">>>>>>>>>>>>
now updating #{klass}
>>>>>>>>>>>>"
      Rails.logger.info(message)
      puts message
      klass.find_each do |instance|
        members_who_are_not_followers = instance.members.to_a - instance.followers
        members_who_are_not_followers.each do |non_follower_member|
          existing_relation = non_follower_member.owned_relations.find_by(resource: instance)
          if existing_relation&.follows?
            message = "User(#{non_follower_member.id}) already follows #{instance.class}(#{instance.id})"
          elsif existing_relation && !existing_relation.follows?
            existing_relation.update_column(:follows, true) # skip callbacks
            message = "User(#{non_follower_member.id}) relation updated to follow #{instance.class}(#{instance.id})"
          else
            relation = CreateRelation.new(user: non_follower_member, resource: instance, follows: true) # skips callbacks
            relation.save!
            # cannot use the following because callbacks to Algolia fail
            # non_follower_member.owned_relations.create(resource: instance, follows: true)
            message = "User(#{non_follower_member.id}) relation created to follow #{instance.class}(#{instance.id})"
          end
          Rails.logger.info(message)
          puts message
        end
      end
    end
  end
end
