# frozen_string_literal: true

Sentry.init do |config|
  config.environment = ENV['HEROKU_ENV'] if ENV['HEROKU_ENV']
  config.breadcrumbs_logger = [:active_support_logger, :http_logger]

  config.traces_sample_rate = 0.25
end
