# frozen_string_literal: true

require 'pagy/extras/array'
require 'pagy/extras/headers'
require 'pagy/extras/items'
require 'pagy/extras/overflow'

Pagy::VARS[:items_param] = :items
Pagy::VARS[:max_items]   = 500
Pagy::VARS[:overflow]    = :empty_page
Pagy::VARS[:items]       = 200
