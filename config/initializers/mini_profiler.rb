# frozen_string_literal: true

if Rails.env == 'development'
  Rack::MiniProfiler.config.storage_options = { url: ENV['REDIS_SERVER_URL'] }
  Rack::MiniProfiler.config.storage = Rack::MiniProfiler::RedisStore
end