# frozen_string_literal: true

require_relative 'boot'

require 'rails'

require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
require 'sprockets/railtie'
require 'rails/test_unit/railtie'
require 'rack/cors'

Bundler.require(*Rails.groups)

module Backend
  class Application < Rails::Application
    config.load_defaults 6.1

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*',
                 headers: :any,
                 methods: %i[get post options],
                 expose: %w[Authorization Current-Page Page-Items Total-Count Total-Pages]
      end
    end

    config.backend_url = "#{ENV['PROTOCOL']}://#{ENV['HOSTNAME']}:#{ENV['BACKEND_PORT']}"

    config.action_controller.asset_host = config.backend_url

    config.frontend_url = "#{ENV['PROTOCOL']}://#{ENV['HOSTNAME']}:#{ENV['FRONTEND_PORT']}"

    config.email = ENV['EMAIL'].presence || 'no-reply@jogl.io'

    config.paths.add 'app/helpers', eager_load: true
    config.paths.add 'app/lib', eager_load: true
    config.paths.add 'app/models/concerns', eager_load: true

    config.api_only = true

    config.active_job.queue_adapter = :sidekiq

    config.session_store :cookie_store, key: '_interslice_session'

    config.middleware.use ActionDispatch::Cookies
    config.middleware.use ActionDispatch::Session::CookieStore, config.session_options

    config.middleware.use Rack::MethodOverride
  end
end
