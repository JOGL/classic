# Contributing

JOGL is **100% open source**, and we fully welcome contributions! You can help us by **contributing to the code**, or translating the platform in your language.

We use the following technologies: [ReactJS](https://reactjs.org/), [NextJS](https://github.com/vercel/next.js/), Typescript, Algolia, Sass, AmazonS3, Ruby, GraphQL, Elastic Search... If you have experience in one or multiple of them, your help will be much appreciated!

When contributing to this repository, please try to focus on already planned issues / known bugs:

## Git Workflow

### General workflow

we think the **forking workflow** is more appropriate, as it allow open contributions from anyone without the need to explicitly add someone to the contributors of the reference repo. And it is the default in many open source projects.

### Branching workflow

We like the [feature branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) for its simplicity. The short lived branches reduce the time spent handling conflicts manually with git.

In this workflow commits are always made in a dedicated feature branch and never in the default branch (here `develop`).
Commits are only added to the default branch by merging a feature branch into it.

## Releases

The different environments are:

| branch    | auto deployed on | purpose                                                                  | frontend link                           |
| --------- | ---------------- | ------------------------------------------------------------------------ | --------------------------------------- | --- |
| `develop` | dev              | for the dev team and contributors and to do a quick first Breaking check | https://jogl-frontend-dev.herokuapp.com |     |
| `master`  | production       | live for users                                                           | https://app.jogl.io/                    |

So, the default branch is `develop`:

- merge requests are targeted towards `develop`
- feature branches are created from `develop`
- feature branches are rebased on top of `develop`:
  - during dev
  - **required**: just before the merge request is open
  - **required**: and after the merge request has been accepted before merging the feature branch into `develop`

Deployment to production is done by merging `develop` into `master`. (Restricted to the JOGL team for sanity purposes)

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

- Using welcoming and inclusive language
- Being respectful of differing viewpoints and experiences
- Gracefully accepting constructive criticism
- Focusing on what is best for the community
- Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

- The use of sexualized language or imagery and unwelcome sexual attention or
  advances
- Trolling, insulting/derogatory comments, and personal or political attacks
- Public or private harassment
- Publishing others' private information, such as a physical or electronic
  address, without explicit permission
- Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

# Contributors

Thank you to all the following users who have made contributions

- [@Xqua](https://gitlab.com/xqua)
- [@LucaH](https://gitlab.com/luknl)
- [@Luis](https://gitlab.com/kaaloo1)
- [@emak](https://gitlab.com/emak)
- [@koriroys](https://gitlab.com/koriroys)
- [@grimbsi](https://gitlab.com/grimbsi)
- [@Isalafont](https://gitlab.com/Isalafont)
- [@EliotM](https://gitlab.com/EliotM)
- [@janet-yu](https://gitlab.com/janet-yu)

## More information

For more detailed information on how to contribute, please read our [General contributing guidelines](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).
