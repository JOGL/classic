# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'PostsOpen', type: :request do
  it 'Shows a post' do
    @user = create(:confirmed_user)
    @feed = @user.feed
    @project = create(:project, creator_id: @user.id)
    @post_one = create(:post, user_id: @user.id, feed_id: @feed.id)
    @mention = create(:mention, post_id: @post_one.id, obj: @project, obj_match: "##{@project.short_title}")

    path '/api/posts/{id}' do
      get(summary: 'Get a post') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Feed ID'
        let(:id) { @post_one.id }
        response(200, description: 'Gets the content of a post') do
          expect(body['id']).to eq(@post_one.id)
          expect(body['content']).to eq(@post_one.content)
          expect(body['media']).to eq(@post_one.media)
          expect(body['creator']['user_id']).to eq(@user.id)
          expect(body['claps']).to eq(@post_one.claps)
          expect(body['mentions'].count).to eq(1)
        end
      end
    end
  end

  it "can't create or update a post" do
    path '/api/posts' do
      parameter :data,
                in: :body,
                required: true
      post(summary: 'Create a post') do
        let(:data) do
          {
            "post": {
              "content": 'This is the best',
              "media": 'https://mediaforpost.com/media2341',
              "feed_id": 1
            }
          }
        end
        response(401, description: 'Unauthorized')
      end
    end
  end
end

RSpec.describe 'PostsAuthorized', type: :request do
  it 'renders a public feed' do
    @user = create(:confirmed_user)
    @project = create(:project, creator_id: @user.id)
    @feed = @user.feed
    @post_one = create(:post, user_id: @user.id, feed_id: @feed.id)
    @feed.posts << @post_one
    @post_two = create(:post, user_id: @user.id, feed_id: @feed.id)
    @feed.posts << @post_two
    @mention = create(:mention, post_id: @post_one.id, obj: @project, obj_match: "##{@project.short_title}")
    @project.feed.posts << @post_one

    path '/api/feed/{id}' do
      parameter :id, in: :path, type: :integer, required: true, description: 'Feed ID'
      get(summary: 'Get a feed') do
        let(:id) { @feed.id }
        response(200, description: 'It gets the feed') do
          body = JSON(response.body)
          expect(body.count).to eq(2)
          expect(body[0]['id']).to eq(@post_one.id)
          expect(body[0]['content']).to eq(@post_one.content)
          expect(body[0]['media']).to eq(@post_one.media)
          expect(body[0]['creator']['user_id']).to eq(@user.id)
          expect(body[0]['claps']).to eq(@post_one.claps)
          expect(body[0]['mentions'].count).to eq(0)
          expect(body[1]['id']).to eq(@post_two.id)
          expect(body[1]['content']).to eq(@post_two.content)
          expect(body[1]['media']).to eq(@post_two.media)
          expect(body[1]['creator']['user_id']).to eq(@user.id)
          expect(body[1]['claps']).to eq(@post_two.claps)
          expect(body[1]['mentions'].count).to eq(1)
          expect(body[1]['mentions']['obj_id']).to eq(@mention.obj_id)
          expect(body[1]['mentions']['obj_type']).to eq(@mention.obj_type.downcase)
          expect(body[1]['mentions']['obj_match']).to eq(@mention.obj_match)
        end
      end
    end

    path '/api/feed' do
      get(summary: 'Get a feed') do
        response(401, description: 'Unauthorized')
      end
    end

    sign_in @user

    path '/api/feed' do
      get(summary: 'Get a feed') do
        response(200, description: 'It gets the feed') do
          body = JSON(response.body)
          expect(body.count).to eq(2)
          expect(body[0]['id']).to eq(@post_one.id)
          expect(body[0]['content']).to eq(@post_one.content)
          expect(body[0]['media']).to eq(@post_one.media)
          expect(body[0]['creator']['user_id']).to eq(@user.id)
          expect(body[0]['claps']).to eq(@post_one.claps)
          expect(body[0]['mentions'].count).to eq(0)
          expect(body[1]['id']).to eq(@post_two.id)
          expect(body[1]['content']).to eq(@post_two.content)
          expect(body[1]['media']).to eq(@post_two.media)
          expect(body[1]['creator']['user_id']).to eq(@user.id)
          expect(body[1]['claps']).to eq(@post_two.claps)
          expect(body[1]['mentions'].count).to eq(1)
          expect(body[1]['mentions']['obj_id']).to eq(@mention.obj_id)
          expect(body[1]['mentions']['obj_type']).to eq(@mention.obj_type)
          expect(body[1]['mentions']['obj_match']).to eq(@mention.obj_match)
        end
      end
    end
  end
end
