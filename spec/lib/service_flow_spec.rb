# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ServiceFlow do
  it 'throws an error for fetch_data' do
    service = create(:service)
    service_flow = described_class.new(service)

    expect { service_flow.fetch_data }.to raise_error(NotImplementedError)
  end

  it 'throws an error when loading data for an unknown service' do
    service = create(:service)
    service_flow = described_class.new(service)

    expect { service_flow.load_data }.to raise_error(ActiveRecord::StatementInvalid)
  end

  it 'throws an error for copy_data' do
    service = create(:service)
    service_flow = described_class.new(service)

    expect { service_flow.copy_data }.to raise_error(NotImplementedError)
  end
end
