# frozen_string_literal: true

require 'simplecov'
require 'webmock/rspec'

SimpleCov.start do
  add_filter %w[/spec/ /vendor/]
end

# WebMock.disable_net_connect!(allow_localhost: true)
WebMock.allow_net_connect!

RSpec.configure do |config|
  config.before(:suite) do
    Rails.application.load_seed
  end
  
  config.before(:each) do
    Sidekiq::Worker.clear_all
    stub_request(:any, /www.eventbriteapi.com/).to_rack(FakeEventbrite)
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end
