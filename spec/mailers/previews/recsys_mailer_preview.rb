# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/recsys_recommendation_mailer

class RecsysMailerPreview < ActionMailer::Preview
  def recsys_recommendation
    # RecsysMailer.with(user: User.first).recsys_recommendation
  end
end
