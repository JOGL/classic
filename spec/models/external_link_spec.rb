# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExternalLink, type: :model do
  it { should have_one_attached(:icon) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:url) }

  it 'has a valid factory' do
    external_link = build(:external_link)

    expect(external_link).to be_valid
  end
end
