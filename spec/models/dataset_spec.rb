# frozen_string_literal: true

require 'rails_helper'

require_relative 'shared_examples/mediumable'

RSpec.describe Dataset, type: :model do

  it_behaves_like 'a model that has many media', :dataset

  it { should belong_to(:author) }

  it { should have_many(:data) }
  it { should have_many(:sources) }
  it { should have_many(:tags) }

  it { should have_one(:license) }

  it { should validate_presence_of(:type) }
  it { should validate_presence_of(:url) }
end
