# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ServiceFlowData, type: :model do
  subject { FactoryBot.build(:service_flow_data) }

  it { is_expected.to belong_to(:service) }

  it { is_expected.to define_enum_for(:flow).with_values(%i[events]) }

  it { is_expected.to validate_presence_of(:data) }
  it { is_expected.to validate_presence_of(:flow) }
end
