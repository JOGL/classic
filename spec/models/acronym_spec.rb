# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Acronym, type: :model do
  it 'has a valid factory' do
    acronym = build(:acronym)

    expect(acronym).to be_valid
  end
end
