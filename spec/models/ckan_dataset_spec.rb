# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Datasets::CkanDataset, type: :model do
  it 'has a valid factory' do
    ckan_dataset = build(:ckan_dataset)

    expect(ckan_dataset).to be_valid
  end

  it 'rejects an invalid url' do
    expect do
      create(:ckan_dataset, url: FFaker::Internet.http_url)
    end.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Url is missing or does not match CKAN dataset format')
  end

  it 'is invalid without an author' do
    ckan_dataset = build(:ckan_dataset, author_id: nil)
    ckan_dataset.valid?

    expect(ckan_dataset.errors.full_messages).to include('Author must exist')
  end
end
