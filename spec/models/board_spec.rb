# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Board, type: :model do
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:title) }

  it 'has a valid factory' do
    board = build(:board)

    expect(board).to be_valid
  end

  it 'can add users' do
    board = create(:board)
    user = create(:user)

    expect do
      board.users << user
    end.to change(board.users, :count).by(1)

    expect(board.users.pluck(:id)).to include user.id
  end

  it 'can be attached to a program' do
    program = create(:program)
    board = create(:board)
    program.boards << board

    expect(program.boards.first.id).to be board.id

    expect(board.boardable.id).to be program.id
  end
end
