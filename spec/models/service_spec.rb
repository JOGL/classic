# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Service, type: :model do
  subject { FactoryBot.build(:service) }

  it { is_expected.to belong_to(:creator) }
  it { is_expected.to belong_to(:serviceable) }

  it { is_expected.to define_enum_for(:name).with_values(%i[eventbrite]) }

  it { is_expected.to validate_presence_of(:creator) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:token) }

  it { is_expected.to validate_uniqueness_of(:name).scoped_to(:serviceable_id, :serviceable_type).ignoring_case_sensitivity }
end
