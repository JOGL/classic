# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChallengesWorkgroup, type: :model do
  it { should belong_to(:challenge) }
  it { should belong_to(:workgroup) }
end
