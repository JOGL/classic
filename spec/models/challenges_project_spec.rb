# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChallengesProject, type: :model do
  it { should belong_to(:challenge) }
  it { should belong_to(:project) }

  it 'has a mutable project status' do
    challenge = create(:challenge)
    project = create(:project)
    challenges_project = ChallengesProject.new challenge: challenge, project: project
    challenges_project.accepted!

    expect(challenges_project.project_status).to eq('accepted')
  end

  describe 'notifications' do
    before(:each) do
      @challenge = create(:challenge, status: :active)
      @project = create(:project)
      @challenge.projects << @project
      @relation = ChallengesProject.find_by(challenge_id: @challenge.id, project_id: @project.id)
    end

    it "sends a notification when project_status changes from 'pending' to 'accepted'" do
      expect(@relation).to receive(:notif_pending_join_request_approved)
      @relation.accepted!
    end

    it "sends a notification to project admins when the relation is accepted" do
      expect do
        @relation.accepted!
      end.to change(Notification, :count).by(@project.admins.count)
    end
  end
end
