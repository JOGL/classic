# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  it { should belong_to(:user) }

  it { should have_and_belong_to_many(:feeds) }

  it { should have_many(:comments) }
  it { should have_many(:mentions) }

  it { should validate_presence_of(:content) }

  it 'should have valid factory' do
    expect(build(:post)).to be_valid
  end
end
