# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Datum, type: :model do
  it { should belong_to(:author) }
  it { should belong_to(:dataset) }

  it { should have_many(:datafields) }
  it { should have_many(:sources) }

  it { should have_one(:license) }

  it { should have_one_attached(:document) }
end
