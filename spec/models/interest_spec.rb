# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Interest, type: :model do
  it { should have_and_belong_to_many(:challenges) }
  it { should have_and_belong_to_many(:workgroups) }
  it { should have_and_belong_to_many(:projects) }
  it { should have_and_belong_to_many(:users) }
end
