# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersBoard, type: :model do
  it { should belong_to(:board) }
  it { should belong_to(:user) }
end
