# frozen_string_literal: true

require 'rails_helper'

require_relative 'shared_examples/affiliatable_child'
require_relative 'shared_examples/feedable'
require_relative 'shared_examples/mediumable'

RSpec.describe Challenge, type: :model do
  it_behaves_like 'a model that can have a feed', :challenge
  it_behaves_like 'a model that is an affilatable child', :challenge
  it_behaves_like 'a model that has many media', :challenge

  it { should have_and_belong_to_many(:interests) }
  it { should have_and_belong_to_many(:skills) }

  it { should have_one(:faq) }
  it { should have_one(:feed) }

  it 'creates an associated faq after creation' do
    challenge = Challenge.create

    expect(challenge.faq).to be_present
  end

  describe 'needs_count' do
    it 'counts needs from accepted projects only' do
      challenge = create(:challenge)
      project_one = create(:project)
      challenge.projects << project_one
      project_one.needs << create(:need)
      project_two = create(:project)
      challenge.projects << project_two
      project_two.needs << create(:need)
      challenge.challenges_projects.update_all(project_status: :accepted)
      project_three = create(:project)
      project_three.needs << create(:need)
      challenge.projects << project_three
      expect(challenge.needs_count).to eq(2)
    end
  end
  
  describe 'notifications' do
    before(:each) do
      @challenge = create(:challenge)
      @project = create(:project)
      @challenge.projects << @project
      @relation = ChallengesProject.find_by(challenge_id: @challenge.id, project_id: @project.id)
    end

    it 'sends a notification when a project is rejected' do
      @relation.pending!

      expect(@challenge).to receive(:notif_rejected_project)
      @challenge.projects.delete(Project.find(@project.id))
    end

    it 'sends a notification when a project is removed' do
      @relation.accepted!
      
      expect(@challenge).to receive(:notif_removed_project)
      @challenge.projects.delete(Project.find(@project.id))
    end
  end

  it 'responds to banner_url_sm' do
    expect(described_class.new).to respond_to(:banner_url_sm)
  end

  describe '#add_users_from_projects' do
    it "adds project users to the challenge's users" do
      challenge = create(:challenge)
      confirmed_project_users = []
      create_list(:project, 2).each do |project|
        user = create(:user)
        confirmed_project_users << user
        user.add_role(:member, project)
        challenge.projects << project
        challenge.challenges_projects.update(project_status: 'accepted')
      end

      unconfirmed_project = create(:project)
      unconfirmed_project_user = create(:user)
      unconfirmed_project_user.add_role(:member, unconfirmed_project)
      challenge.projects << unconfirmed_project

      challenge.add_users_from_projects

      # includes
      # 1 factory-created challenge creator (1x),
      # 1 factory-created creator per project (2x),
      # and 1 test-created user for each `accepted` challenge_project (2x)
      expect(challenge.users).to include(*confirmed_project_users)
      expect(challenge.users).to_not include(unconfirmed_project_user)
    end
  end

  describe '#projects_count' do
    it 'is the number of accepted projects' do
      challenge = create(:challenge)
      expect(challenge.projects_count).to eq(0)
      project = create(:project)
      challenge.projects << project
      challenge.challenges_projects.first.update(project_status: 'accepted')
      expect(challenge.projects_count).to eq(1)
    end
  end

  describe 'logo_url_sm' do
    before do
      @challenge = build(:challenge)
    end

    it 'picks a default avatar image' do
      expect(@challenge.logo_url_sm).to match(%r{http://localhost:3001/assets/default-avatar-\w*.png})
    end

    it 'calls default_logo_url if avatar.attachment is not available' do
      expect(@challenge).to receive(:default_logo_url).with(no_args).at_least(:once)
      @challenge.logo_url_sm
      @challenge.logo_url
    end

    it "uses the avatar image if it's present" do
      @challenge.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:avatar_variant_url).with(resize: '40x40^')
      @challenge.logo_url_sm
    end

    it "uses the avatar image if it's present" do
      @challenge.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:avatar_variant_url).with(resize: '200x200^')
      @challenge.logo_url
    end

    it "uses the avatar image if it's present" do
      @challenge.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:avatar_blob_url).at_least(:once)
      @challenge.logo_url_sm
    end
  end

  describe 'banner_url_sm' do
    before do
      @challenge = build(:challenge)
    end

    it 'picks a default banner image' do
      expect(@challenge.banner_url_sm).to match(%r{http://localhost:3001/assets/default-challenge-\w*.jpg})
    end

    it 'calls default_banner_url if banner.attachment is not available' do
      expect(@challenge).to receive(:default_banner_url).with(no_args)
      @challenge.banner_url
    end

    it "uses the banner image if it's present" do
      @challenge.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:banner_variant_url).with(resize: '100x100^')
      @challenge.banner_url_sm
    end

    it "uses the banner image if it's present" do
      @challenge.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:banner_variant_url).with(resize: '400x400^')
      @challenge.banner_url
    end

    it "uses the banner image if it's present" do
      @challenge.banner.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@challenge).to receive(:banner_blob_url)
      @challenge.banner_url
    end
  end
end
