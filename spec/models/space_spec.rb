# frozen_string_literal: true

require 'rails_helper'

require_relative 'shared_examples/affiliatable_parent'
require_relative 'shared_examples/feedable'
require_relative 'shared_examples/mediumable'

RSpec.describe Space, type: :model do
  it_behaves_like 'a model that can have a feed', :space
  it_behaves_like 'a model that is an affilatable parent', :space
  it_behaves_like 'a model that has many media', :space

  it { should belong_to(:creator) }

  it { should have_many(:challenges) }
  it { should have_many(:resources) }

  it { should have_many_attached(:documents) }

  it { should have_one(:faq) }

  it { should validate_presence_of(:title) }
  it { should validate_uniqueness_of(:short_title) }

  it 'has a valid factory' do
    space = create(:space)

    expect(space).to be_valid
  end

  describe 'counts' do
    let!(:space) { create(:space) }

    it 'has the correct project counts' do
      project1 = create(:project)
      project2 = create(:project)
      space.add_affiliate project1
      project2.add_affiliation_to space
      expect(space.projects_count).to eq(1)
    end

    it 'has the correct project counts' do
      project1 = create(:project)
      project2 = create(:project)
      need1 = create(:need, project: project1)
      need2 = create(:need, project: project2)
      space.add_affiliate project1
      project2.add_affiliation_to space
      expect(space.needs_count).to eq(1)
    end

    it 'counts only accepted challenges' do
      challenge1 = create(:challenge, status: 'draft')
      challenge2 = create(:challenge, status: 'accepting')
      challenge3 = create(:challenge, status: 'active')
      all_challenges = [challenge1, challenge2, challenge3]
      space.challenges << all_challenges
      expect(space.challenges_count).to eq 2
    end
  end
end
