# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NeedsSkill, type: :model do
  it { should belong_to(:need) }
  it { should belong_to(:skill) }
end
