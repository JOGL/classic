# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Affiliation, type: :model do
  it { is_expected.to belong_to(:affiliate) }
  it { is_expected.to belong_to(:parent) }

  it 'has a valid factory' do
    expect(build(:space)).to be_valid
  end

  it 'has a default status of pending' do
    affiliation = create(:affiliation)

    expect(affiliation.status).to eq('pending')
  end

  it 'has a mutable status of pending->accepted' do
    affiliation = create(:affiliation)
    affiliation.accepted!

    expect(affiliation.status).to eq('accepted')
  end

  it 'has a unique affiliation' do
    affiliation = create(:affiliation)

    expect do
      described_class.create(parent: affiliation.parent, affiliate: affiliation.affiliate)
    end.to change(described_class, :count).by(0)
  end

  describe 'notifications' do
    let(:challenge) { FactoryBot.create(:challenge) }
    let(:space) { FactoryBot.create(:space) }
    let(:user) { FactoryBot.create(:user) }
    let(:admins) do
      {
        challenge: FactoryBot.create(:user).add_role(:admin, challenge),
        space: FactoryBot.create(:user).add_role(:admin, space)
      }
    end
    let(:affiliation) { challenge.add_affiliation_to(space) }

    it 'sends a notification when an affiliation is pending' do
      affiliation

      expect(Notification.last).to have_attributes type: 'affiliation_pending', object_type: 'Space'
    end

    it 'does not send a notification when affiliation is not pending' do
      expect { space.add_affiliate(challenge) }.not_to change(Notification, :count)
    end

    it 'sends a notification when an affiliation is accepted' do
      affiliation.accepted!

      expect(Notification.last).to have_attributes type: 'affiliation_accepted', object_type: 'Challenge'
    end

    it 'sends a notification when an user affiliation is accepted' do
      affiliation = user.add_affiliation_to(space)
      affiliation.accepted!

      expect(Notification.last).to have_attributes type: 'affiliation_accepted', object_type: 'User'
    end

    it 'sends a notification when an affiliation is rejected' do
      affiliation

      space.remove_affiliate(challenge)

      expect(Notification.last).to have_attributes type: 'affiliation_rejected', object_type: 'Challenge'
    end

    it 'sends a notification when an user affiliation is rejected' do
      user.add_affiliation_to(space)

      space.remove_affiliate(user)

      expect(Notification.last).to have_attributes type: 'affiliation_rejected', object_type: 'User'
    end

    it 'sends a notification when an affiliation is destroyed' do
      space.add_affiliate(challenge)

      space.remove_affiliate(challenge)

      expect(Notification.last).to have_attributes type: 'affiliation_destroyed', object_type: 'Challenge'
    end

    it 'sends a notification when an user affiliation is destroyed' do
      space.add_affiliate(user)

      space.remove_affiliate(user)

      expect(Notification.last).to have_attributes type: 'affiliation_destroyed', object_type: 'User'
    end
  end
end
