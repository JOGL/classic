# frozen_string_literal: true

require 'rails_helper'

require_relative 'shared_examples/feedable'
require_relative 'shared_examples/mediumable'

RSpec.describe Need, type: :model do
  it_behaves_like 'a model that can have a feed', :need
  it_behaves_like 'a model that has many media', :need

  it { should belong_to(:project) }
  it { should belong_to(:user) }

  it { should have_many_attached(:documents) }

  it { should have_one(:feed) }

  it { should validate_presence_of(:content) }
  it { should validate_presence_of(:title) }

  it 'has a valid factory' do
    need = build(:need)

    expect(need).to be_valid
  end
end
