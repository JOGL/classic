require 'rails_helper'

RSpec.shared_examples 'a model that is an affilatable parent' do |factory|
  let(:parent) { build(factory) }
  let(:child) { build(:affiliate) }

  describe 'affiliatable parent' do
    it { should have_many(:affiliations) }

    it 'can be add an affiliation to a parent' do

      expect do 
        parent.add_affiliate(child)
      end.to change { Affiliation.count }.by(1)
    end

    it 'can remove an affiliation to a child' do
      parent.add_affiliate(child)

      expect do
        parent.remove_affiliate(child)
      end.to change { Affiliation.count }.by(-1)
    end
    
  end
end