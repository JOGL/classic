require 'rails_helper'

RSpec.shared_examples 'a model that is an affilatable child' do |factory|
  let(:child) { build(factory) }

  describe 'affiliatable child' do
    it { should have_many(:affiliations) }

    it 'can be affiliate to a parent' do
      parent = build(:parent)

      expect do 
        child.add_affiliation_to(parent)
      end.to change { Affiliation.count }.by(1)
    end

    it 'can remove its affiliation to a parent' do
      parent = build(:parent)
      child.add_affiliation_to(parent)

      expect do
        child.remove_affiliation_to(parent)
      end.to change { Affiliation.count }.by(-1)
    end
    
  end
end
