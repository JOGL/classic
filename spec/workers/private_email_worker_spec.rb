# frozen_string_literal: true

require 'rails_helper'
# require 'sidekiq/testing'

RSpec.describe PrivateEmailWorker, type: :worker do
  describe 'testing worker' do
    # it "should respond to #perform" do
    #   expect(PrivateMailer.send_private_email(from, to, object, content)).to respond_to(:perform)
    # end

    # describe "perform" do
    #   before do
    #     Sidekiq::Worker.clear_all
    #   end
    # end

    it 'should sends from right email' do
      from_user = create(:user)
      to_user = create(:user)
      object = nil
      content = 'skjhgsalkjf'
      mail = PrivateMailer.send_private_email(from_user, to_user, object, content)
      expect(mail.to).to eq [to_user.email]
    end

    it 'should renders the subject' do
      from_user = create(:user)
      to_user = create(:user)
      object = "A subject"
      content = 'skjhgsalkjf'
      mail = PrivateMailer.send_private_email(from_user, to_user, object, content)
      expect(mail.subject).to match object
    end
  end
end
