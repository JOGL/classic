# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/affiliation'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/linkable'
require_relative 'shared_examples/mediumable'
require_relative 'shared_examples/memberable'
require_relative 'shared_examples/private_object_membership'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/reviewable'
require_relative 'shared_examples/saveable'

RSpec.describe Api::ProjectsController, type: :controller do
  context 'Examples' do
    describe 'project relations' do
      it_behaves_like 'an object with followable', :project
      it_behaves_like 'an object with saveable', :project
      it_behaves_like 'an object with reviewable', :project
    end

    describe 'Project' do
      it_behaves_like 'an object with affiliations', :project
      it_behaves_like 'an object with links', :project
      it_behaves_like 'an object with media', :project
      it_behaves_like 'an object with members', :project
      it_behaves_like 'a private object with members', :project
      it_behaves_like 'an object with recommendations', :project
    end
  end

  context 'Project Specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @project = create(:project, creator_id: @user.id)
    end

    describe '#index' do
      it 'should return list of project' do
        get :index
        project = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(project.count).to eq 1
      end
    end

    describe '#create' do
      context 'duplicate short title error' do
        it 'should not creates a project' do
          post :create, params: { project: @project.attributes }
          expect(response.status).to eq 422
        end
      end

      context 'creating project with logged in user' do
        it 'should create a project' do
          # @project.short_title = FFaker::Name.first_name
          # post :create, params: { project: @project.attributes.merge(title: @project.short_title) }
          @new_project = build(:project, creator_id: @user.id)
          post :create, params: { project: @new_project.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Project', targetable_node_id: json_response['id'], relation_type: 'is_author_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Project', targetable_node_id: json_response['id'], relation_type: 'is_member_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Project', targetable_node_id: json_response['id'], relation_type: 'is_admin_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Project', targetable_node_id: json_response['id'], relation_type: 'is_owner_of')).to exist
        end
      end

      context 'creating project without logged in user' do
        it 'should not create a project' do
          sign_out @user
          @project.short_title = FFaker::Name.first_name
          post :create, params: { project: @project.attributes }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#show' do
      it 'should return project' do
        @challenge = create(:challenge)
        @challenge2 = create(:challenge)

        # attach project to this challenge
        @challenge.projects << @project
        ChallengesProject.find_by(challenge: @challenge, project: @project).accepted!

        @challenge2.projects << @project
        ChallengesProject.find_by(challenge: @challenge2, project: @project).pending!

        get :show, params: { id: @project.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        # it pulls the challenge_project status onto challenge response
        # also, transforming it from 'accept' to 'accepted' at request of frontend team
        expect(json_response['challenges'].length).to eq(2)
        expect(json_response['challenges'][0]['project_status']).to eq('accepted')
        expect(json_response['challenges'][1]['project_status']).to eq('pending')

        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @project.class.name, targetable_node_id: @project.id, relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      context 'Updating project with logged in user and permissons' do
        it 'should update a project' do
          @user.add_role :admin, @project
          put :update, params: { project: { description: 'New descriptioniwqe', title: 'New title1' }, id: @project.id }
          expect(response.status).to eq 200
        end
      end

      context 'User not authorized for updating project' do
        it 'should throw forbidden error' do
          @user.remove_role :admin, @project
          @user.remove_role :owner, @project
          put :update, params: { project: { description: 'New description' }, id: @project.id }
          expect(response.status).to eq 403
        end
      end

      context 'updating project without loggin in user' do
        it 'should throw Unauthorized error' do
          sign_out @user
          put :update, params: { project: { description: 'New description' }, id: @project.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#destroy' do
      context 'Deleting project without loggin in user' do
        it 'Should not Delete the project' do
          sign_out @user
          delete :destroy, params: { id: @project.id }
          expect(response.status).to eq 401
        end
      end

      context 'Deleting project with loggin in user' do
        it 'Should Delete the project' do
          delete :destroy, params: { id: @project.id }
          expect(response.status).to eq 200
        end
      end
    end

    describe 'Banner Upload' do
      it 'should allow a PNG image' do
        @user.add_role :admin, @program
        png = fixture_file_upload('/authorized.png', 'image/png')
        post :upload_banner, params: { id: @project.id, banner: png }
        expect(response).to be_successful
      end

      it 'should not allow a TIFF image' do
        @user.add_role :admin, @program
        tiff = fixture_file_upload('/unauthorized.tiff', 'image/tiff')
        post :upload_banner, params: { id: @project.id, banner: tiff }
        expect(response.status).to eq(422)
      end
    end
  end
end
