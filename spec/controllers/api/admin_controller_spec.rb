# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::AdminController, type: :controller do
  context "is JOGL admin" do
    before do
      @user = create(:confirmed_user)
      @user.add_role :admin
      sign_in @user
    end

    describe '#users_with_skills' do
      before do
        @user1 = create(:confirmed_user)
        @user2 = create(:confirmed_user)
        @user3 = create(:confirmed_user)
        @user4 = create(:confirmed_user)
        @skills = [@user1.skills.first.id, @user2.skills.first.id, @user3.skills.first.id]
      end

      it "should render as json" do
        get :users_with_skills, params: {skills: @skills}, :format => :json
        expect(response.content_type).to include "application/json"
        expect(response).to have_http_status :ok
      end

      it "should render as csv" do
        get :users_with_skills, params: {skills: @skills}, :format => :csv
        expect(response.content_type).to include "text/csv"
        expect(response).to have_http_status :ok
      end

      it "Returns the users" do
        get :users_with_skills, params: {skills: @skills}, :format => :json
        json_response = JSON.parse(response.body)
        expect(json_response["users"].count).to eq(3)
        expect([@user1.id, @user2.id, @user3.id]).to include(json_response["users"][0]["id"])
        expect([@user1.id, @user2.id, @user3.id]).to include(json_response["users"][1]["id"])
        expect([@user1.id, @user2.id, @user3.id]).to include(json_response["users"][2]["id"])
      end

      it "Users are unique" do
        @user2.skills << @user1.skills.first
        get :users_with_skills, params: {skills: @skills}, :format => :json
        json_response = JSON.parse(response.body)
        expect(json_response["users"].count).to eq(3)
      end
    end
  end

  context "is Program admin get users with skills" do
    before do
      @program = create(:program)
      @user = @program.users.first
      sign_in @user
    end

    describe "#users_with_skills" do
      before do
        @user1 = create(:confirmed_user)
        @user2 = create(:confirmed_user)
        @user3 = create(:confirmed_user)
        @user4 = create(:confirmed_user)
        @skills = [@user1.skills.first.id, @user2.skills.first.id, @user3.skills.first.id]
      end

      it "should render as json" do
        get :users_with_skills, params: {skills: @skills}, :format => :json
        expect(response.content_type).to include "application/json"
        expect(response).to have_http_status :ok
      end

      it "should render as csv" do
        get :users_with_skills, params: {skills: @skills}, :format => :csv
        expect(response.content_type).to include "text/csv"
        expect(response).to have_http_status :ok
      end

      it "Returns the users" do
        get :users_with_skills, params: {skills: @skills}, :format => :json
        json_response = JSON.parse(response.body)
        expect(json_response["users"].count).to eq(3)
        expect([@user1.id, @user2.id, @user3.id]).to include(json_response["users"][0]["id"])
        expect([@user1.id, @user2.id, @user3.id]).to include(json_response["users"][1]["id"])
        expect([@user1.id, @user2.id, @user3.id]).to include(json_response["users"][2]["id"])
      end

      it "Users are unique" do
        @user2.skills << @user1.skills.first
        get :users_with_skills, params: {skills: @skills}, :format => :json
        json_response = JSON.parse(response.body)
        expect(json_response["users"].count).to eq(3)
      end
    end
  end

  context "is not an admin get users with skills" do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe "#users_with_skills" do
      before do
        @user1 = create(:confirmed_user)
        @user2 = create(:confirmed_user)
        @user3 = create(:confirmed_user)
        @user4 = create(:confirmed_user)
        @skills = [@user1.skills.first.id, @user2.skills.first.id, @user3.skills.first.id]
      end

      it "should be forbidden" do
        get :users_with_skills, params: {skills: @skills}, :format => :json
        expect(response).to have_http_status :forbidden
      end
    end
  end

  context "is not logged in get users with skills" do
    describe "#users_with_skills" do
      before do
        @user1 = create(:confirmed_user)
        @user2 = create(:confirmed_user)
        @user3 = create(:confirmed_user)
        @user4 = create(:confirmed_user)
        @skills = [@user1.skills.first.id, @user2.skills.first.id, @user3.skills.first.id]
      end

      it "should be unauthorized" do
        get :users_with_skills, params: {skills: @skills}, :format => :json
        expect(response).to have_http_status :unauthorized
      end
    end
  end
end
