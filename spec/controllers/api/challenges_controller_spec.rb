# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/affiliation'
require_relative 'shared_examples/faqable'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/linkable'
require_relative 'shared_examples/mediumable'
require_relative 'shared_examples/memberable'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/saveable'
require_relative 'shared_examples/skillable'

RSpec.describe Api::ChallengesController, type: :controller do
  context 'Examples' do
    describe 'challenge relations' do
      it_behaves_like 'an object with followable', :challenge
      it_behaves_like 'an object with saveable', :challenge
    end

    describe 'challenge mixins' do
      it_behaves_like 'an object with affiliations', :challenge
      it_behaves_like 'an object with an faq', :challenge
      it_behaves_like 'an object with links', :challenge
      it_behaves_like 'an object with media', :challenge
      it_behaves_like 'an object with members', :challenge
      it_behaves_like 'an object with recommendations', :challenge
      it_behaves_like 'an object with skills', :challenge
    end
  end

  describe 'object with sub object members' do
    describe '#members_list' do
      before do
        @megan = create(:confirmed_user, first_name: 'Megan', last_name: 'Strant')
        @challenge = create(:challenge)
        @challenge.admins.first.update(first_name: 'Ralph')
        @megan.add_role :admin, @challenge
        sign_in @megan
        @user2 = create(:confirmed_user, first_name: 'Betty')
        @user2.add_role :member, @challenge
      end

      it "should get the members of the #{described_class} including owners/admins/members of the project" do
        project = create(:project)
        project.admins.first.update(first_name: 'Trina')
        @challenge.projects << project
        roberta = create(:confirmed_user, first_name: 'Roberta')
        roberta.add_role(:member, project)
        michael = create(:confirmed_user, first_name: 'Michael')
        michael.add_role(:pending, project)
        owner = create(:confirmed_user, first_name: 'Owner')
        owner.add_role(:owner, project)
        admin = create(:confirmed_user, first_name: 'Admin')
        admin.add_role(:admin, project)

        get :members_list, params: { id: @challenge.id }

        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['members'].count).to eq(7)
        expect(json_response['members'].map { |m| m['first_name'] }).to_not include('Michael')
        expect(json_response['members'].map { |m| m['first_name'] }).to eq(%w[Megan Ralph Betty Trina Roberta Owner Admin])
      end

      it "should not return users not members of the #{described_class}" do
        get :members_list, params: { id: @challenge.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['members'].count).to eq 3
      end

      it "should paginate the members of the #{described_class}" do
        get :members_list, params: { id: @challenge.id, items: 1, page: 1 }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['members'].count).to eq 1
      end

      xit 'should order the returned users correctly' do
        get :members_list, params: { id: @challenge.id, f: true }
        json_response = JSON.parse(response.body)['members']
        expect(json_response.count).to eq(3)

        expect(json_response[0]['owner']).to eq(true)
        expect(json_response[1]['admin']).to eq(true)
        expect(json_response[2]['member']).to eq(true)
      end
    end
  end

  describe '#index_needs' do
    context 'we can get the list of needs attached to a program' do
      it 'should return the list of needs from projects accepted to the challenge' do
        megan = create(:confirmed_user, first_name: 'Megan')
        challenge = create(:challenge)
        # attach some needs to an accepted project
        project1 = create(:project, creator: megan)
        challenge.projects << project1
        project1.needs << create(:need, project: project1, user: megan)
        project1.needs << create(:need, project: project1, user: megan)
        challenge.challenges_projects.update_all(project_status: :accepted)

        # attach some needs to a non accepted project
        project2 = create(:project, creator: megan)
        challenge.projects << project2
        project2.needs << create(:need, project: project2, user: megan)
        project2.needs << create(:need, project: project2, user: megan)

        get :index_needs, params: { id: challenge.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response['needs'].count).to eq 2
      end
    end
  end

  describe '#index_projects' do
    context 'we can get the list of projects attached to a challenge' do
      it 'should return the list of projects' do
        megan = create(:confirmed_user, first_name: 'Megan')
        challenge = create(:challenge)
        project1 = create(:project, creator: megan)
        project2 = create(:project, creator: megan)

        challenge.projects << project1 << project2
        get :index_projects, params: { id: challenge.id }
        expect(response.status).to eq 200
        json_response = JSON.parse(response.body)
        expect(json_response['projects'].count).to eq 2
      end
    end
  end

  context 'Challenge admin' do
    before(:each) do
      @user = create(:confirmed_user)
      sign_in @user
      @challenge = create(:challenge)
      @user.add_role :member, @challenge
      @user.add_role :admin, @challenge
      @user.add_role :owner, @challenge
    end

    describe 'can manage projects' do
      before(:each) do
        @project = create(:project)
        @challenge.projects << @project
        @challenge.challenges_projects.find_by(project: @project).pending!
      end

      it 'and change the status' do
        post :set_project_status, params: { id: @challenge.id, project_id: @project.id, status: 'accepted' }
        JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@challenge.challenges_projects.find_by(project_id: @project.id).accepted?).to be_truthy
      end

      it 'and can remove a project' do
        delete :remove, params: { id: @challenge.id, project_id: @project.id }
        JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(@challenge.projects.count).to eq 0
      end
    end
  end

  context 'Project owner' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @challenge = create(:challenge)
      @project = create(:project, creator_id: @user.id)
    end

    it 'can attach its project' do
      put :attach, params: { id: @challenge.id, project_id: @project.id }
      JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@challenge.projects.count).to eq 1
      expect(@challenge.challenges_projects.find_by(project_id: @project.id).pending?).to be_truthy
      expect(RecsysDatum.where(sourceable_node_type: @project.class.name, sourceable_node_id: @project.id, targetable_node_type: @challenge.class.name, targetable_node_id: @challenge.id, relation_type: 'is_part_of')).to exist
    end

    it 'attach to challenge triggers a notification' do
      expect do
        put :attach, params: { id: @challenge.id, project_id: @project.id }
      end.to change(Notification, :count).by(1)
      notif = Notification.where(object_type: @project.class.name, object_id: @project.id, type: 'pending_project').first
      expect(notif).to be_truthy
      expect(notif.category).to match('administration')
      expect(notif.target).to be == @challenge.users.first
      expect(notif.metadata[:challenge].id).to be(@challenge.id)
    end

    it 'can remove its project' do
      @challenge.projects << @project
      delete :remove, params: { id: @challenge.id, project_id: @project.id }
      JSON.parse(response.body)
      expect(response.status).to eq 200
      expect(@challenge.projects.count).to eq 0
      expect(RecsysDatum.where(sourceable_node_type: @project.class.name, sourceable_node_id: @project.id, targetable_node_type: @challenge.class.name, targetable_node_id: @challenge.id, relation_type: 'is_part_of')).not_to exist
    end

    describe 'can change a challenge status' do
      before do
        @user.add_role :member, @challenge
        @user.add_role :admin, @challenge
      end

      it "from 'draft' to 'soon' which triggers notifications" do
        patch :update, params: { id: @challenge.id, challenge: { status: 'soon' } }
        @challenge.reload

        expect(response.status).to eq 200
      end

      it "from 'soon' to 'active' which triggers notifications" do
        @challenge.soon!
        patch :update, params: { id: @challenge.id, challenge: { status: 'active' } }
        @challenge.reload

        expect(response.status).to eq 200
      end

      it "from 'active' to 'completed' without a program" do
        @challenge.active!
        patch :update, params: { id: @challenge.id, challenge: { status: 'completed' } }
        @challenge.reload

        expect(response.status).to eq 200
      end

      it "from 'active' to 'completed' in a program" do
        @challenge.active!
        @program = create(:program)
        @program.challenges << @challenge
        patch :update, params: { id: @challenge.id, challenge: { status: 'completed' } }
        @challenge.reload

        expect(response.status).to eq 200
      end
    end

    describe 'except if it is not an admin' do
      before do
        @user.add_role :member, @challenge
      end

      it 'if not admin' do
        patch :update, params: { id: @challenge.id, challenge: { status: 'completed' } }
        expect(response.status).to eq 403
      end
    end
  end

  context "is program admin" do
    before do
      @program = create(:program)
      @user = @program.users.first
      @challenge = build(:challenge)
      sign_in @user
    end

    describe '#create' do
      it 'is permitted if the current user is an admin of a program' do
        expect do
          post :create, params: {challenge: @challenge.attributes}
        end.to change(Challenge, :count).by 1
        expect(response).to have_http_status :created
      end
    end
  end

  context "is space admin" do
    before do
      @space = create(:space)
      @user = @space.users.first
      @challenge = build(:challenge)
      sign_in @user
    end

    describe '#create' do
      it 'is permitted if the current user is an admin of a program' do
        expect do
          post :create, params: {challenge: @challenge.attributes}
        end.to change(Challenge, :count).by 1
        expect(response).to have_http_status :created
      end
    end
  end

  context "is not program or space admin" do
    before do
      @user = create(:confirmed_user)
      @challenge = build(:challenge)
      sign_in @user
    end

    describe '#create' do
      it 'is not permitted' do
        expect do
          post :create, params: {challenge: @challenge.attributes}
        end.to change(Challenge, :count).by 0
        expect(response).to have_http_status :forbidden
      end
    end
  end


  describe '#destroy' do
    it 'works if current user is owner of challenge and only user on challenge' do
      challenge = create(:challenge)
      jim = challenge.roles.first.users.first
      sign_in jim

      expect do
        delete :destroy, params: { id: challenge.id }
      end.to change { Challenge.count }.by(-1)
    end

    it 'archives the challenge if it is not a test challenge' do
      challenge = create(:challenge)
      owner = challenge.roles.where(name: :owner).first.users.first
      jim = create(:confirmed_user)
      jim.add_role(:member, challenge)
      sign_in owner

      delete :destroy, params: { id: challenge.id }
      challenge.reload

      expect(challenge).to be_archived
    end

    it 'rejects if current user not owner' do
      challenge = create(:challenge)
      unauthorized_user = create(:confirmed_user)
      sign_in unauthorized_user

      expect do
        delete :destroy, params: { id: challenge.id }
      end.to change { Challenge.count }.by(0)

      expect(response).to be_unauthorized
    end
  end
end
