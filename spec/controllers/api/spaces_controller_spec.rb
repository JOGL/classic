# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/affiliated'
require_relative 'shared_examples/faqable'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/linkable'
require_relative 'shared_examples/mediumable'
require_relative 'shared_examples/memberable'
require_relative 'shared_examples/private_object_membership'
require_relative 'shared_examples/resourceable'
require_relative 'shared_examples/saveable'
require_relative 'shared_examples/serviceable'

RSpec.describe Api::SpacesController, type: :controller do
  context 'Shared Examples' do
    describe 'Space relations' do
      it_behaves_like 'an object with followable', :space
      it_behaves_like 'an object with saveable', :space
    end

    describe 'Space' do
      it_behaves_like 'with affiliated', :space
      it_behaves_like 'an object with an faq', :space
      it_behaves_like 'an object with links', :space
      it_behaves_like 'an object with media', :space
      it_behaves_like 'an object with members', :space
      it_behaves_like 'a private object with members', :space
      it_behaves_like 'an object with resources', :space
      it_behaves_like 'with serviceable', :space
    end
  end

  before do
    @space = create(:space)
  end

  context 'Controller specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe '#index' do
      it 'should return list of spaces' do
        get :index
        expect(response).to have_http_status :ok
      end
    end

    describe '#my_spaces' do
      context 'getting users spaces with logged in user' do
        it 'should return list of users spaces' do
          get :my_spaces
          expect(response).to have_http_status :ok
        end
      end

      context 'getting users spaces without logged in user' do
        it 'should return list of users spaces' do
          sign_out @user
          get :my_spaces
          expect(response).to have_http_status :unauthorized
        end
      end
    end

    describe '#create' do
      context 'without logged in user' do
        it "Return Unauthorized because user's not signed in" do
          sign_out @user
          post :create, params: { space: @space.attributes }
          expect(response.status).to eq 401
        end
      end

      context 'creating space with unapproved user' do
        it 'returns forbidden' do
          @space.short_title = FFaker::Name.first_name
          post :create, params: { space: @space.attributes }
          expect(response.status).to eq 403
        end
      end

      context 'creating space with approved user' do
        it 'creates a space' do
          space = build(:space)
          @user.add_role :space_creator
          post :create, params: { space: space.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(json_response['data']).to eq 'Success'
        end

        it 'can edit the space settings' do
          space = build(:space)
          @user.add_role :space_creator
          space_params = space.attributes.merge(selected_tabs: { 'enablers' => true, 'faqs' => true })
          space_params.delete('settings')
          post :create, params: { space: space_params }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(json_response['data']).to eq 'Success'

          get :show, params: { id: json_response['id'] }
          json_response = JSON.parse(response.body)
          expect(json_response['available_tabs']).to eq({ 'challenges' => true, 'enablers' => true, 'faqs' => true, 'programs' => true, 'resources' => true })
          expect(json_response['selected_tabs']).to eq({ 'enablers' => 'true', 'faqs' => 'true' })
        end
      end
    end

    describe '#show' do
      it 'should return space' do
        get :show, params: { id: @space.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Space', targetable_node_id: json_response['id'], relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      context 'Updating space' do
        it 'Updates a space' do
          @user.add_role :admin, @space
          put :update, params: { space: { description: 'New description' }, id: @space.id }
          expect(response.status).to eq 200
        end

        it "from 'draft' to 'soon' status which triggers notifications" do
          @user.add_role :member, @space
          @user.add_role :admin, @space
          @space.users << @user
          expect do
            put :update, params: { space: { status: 'soon' }, id: @space.id }
          end#.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          # notif = Notification.where(object_type: @space.class.name, object_id: @space.id, type: 'soon_space')
          # expect(notif.count).to eq(User.count)
          # expect(notif.first.category).to match('space')
        end

        it "from 'soon' to 'active' status which triggers notifications" do
          @space.soon!
          @user.add_role :member, @space
          @user.add_role :admin, @space
          @space.users << @user
          expect do
            put :update, params: { space: { status: 'active' }, id: @space.id }
          end#.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          # notif = Notification.where(object_type: @space.class.name, object_id: @space.id, type: 'start_space')
          # expect(notif.count).to eq(User.count)
          # expect(notif.first.category).to match('space')
        end

        it "from 'active' to 'completed' status which triggers notifications" do
          @space.active!
          @user.add_role :member, @space
          @user.add_role :admin, @space
          @space.users << @user
          expect do
            put :update, params: { space: { status: 'completed' }, id: @space.id }
            expect(response.status).to eq 200
          end#.to change(Notification, :count).by(User.all.count)
          # notif = Notification.where(object_type: @space.class.name, object_id: @space.id, type: 'end_space')
          # expect(notif.count).to eq(User.count)
          # expect(notif.first.category).to match('space')
        end
      end

      context 'User not logged in' do
        it "Should throw Unauthorized error and won't update the space" do
          sign_out @user
          put :update, params: { space: { description: 'New description' }, id: @space.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#destroy' do
      context 'User not logged in' do
        it 'Should Not Delete the space' do
          sign_out @user
          delete :destroy, params: { id: @space.id }
          expect(response.status).to eq 401
        end
      end

      context 'User logged in' do
        it 'Should Not Delete the space' do
          delete :destroy, params: { id: @space.id }
          expect(response.status).to eq 200
        end
      end
    end

    describe '#index_challenges' do
      context 'we can get the list of challenges attached to a space' do
        it 'should return the list of challenges' do
          challenge1 = create(:challenge, space: @space)
          challenge2 = create(:challenge, space: @space)
          get :index_challenges, params: { id: @space.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['challenges'].count).to eq 2
        end
      end
    end

    describe '#index_projects' do
      context 'we can get the list of projects attached to a space' do
        it 'should return the list of projects' do
          challenge1 = create(:challenge)
          challenge2 = create(:challenge)
          project1 = create(:project, creator_id: @user.id)
          project2 = create(:project, creator_id: @user.id)
          project3 = create(:project, creator_id: @user.id)
          @space.add_affiliate project1
          @space.add_affiliate project2
          @space.add_affiliate project3

          get :index_projects, params: { id: @space.id }
          expect(response.status).to eq 200
          json_response = JSON.parse(response.body)
          expect(json_response['projects'].count).to eq 3
        end
      end
    end

    describe '#index_needs' do
      before(:each) do
        @project1 = create(:project, creator_id: @user.id)
        @project2 = create(:project, creator_id: @user.id)
        @project3 = create(:project, creator_id: @user.id)
        @project1.needs << create(:need, project_id: @project1.id, user_id: @user.id)
        @project2.needs << create(:need, project_id: @project2.id, user_id: @user.id)
        @project3.needs << create(:need, project_id: @project3.id, user_id: @user.id)
        @space.add_affiliate @project1
        @space.add_affiliate @project2
        @space.add_affiliate @project3
      end
      context 'we can get the list of needs attached to a space' do
        it 'should return the list of needs of accepted affiliation' do
          get :index_needs, params: { id: @space.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['needs'].count).to eq 3
        end

        it 'should exclude the needs of pending affiliation' do
          Affiliation.last.pending!

          get :index_needs, params: { id: @space.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['needs'].count).to eq 2
        end
      end
    end

    describe 'Banner Upload' do
      it 'should allow a PNG image' do
        @user.add_role :admin, @space
        png = fixture_file_upload('/authorized.png', 'image/png')
        post :upload_banner, params: { id: @space.id, banner: png }
        expect(response).to be_successful
      end

      it 'should not allow a TIFF image' do
        @user.add_role :admin, @space
        tiff = fixture_file_upload('/unauthorized.tiff', 'image/tiff')
        post :upload_banner, params: { id: @space.id, banner: tiff }
        expect(response.status).to eq(422)
      end
    end
  end
end
