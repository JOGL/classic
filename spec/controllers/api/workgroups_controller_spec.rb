# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/linkable'
require_relative 'shared_examples/mediumable'
require_relative 'shared_examples/memberable'
require_relative 'shared_examples/private_object_membership'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/saveable'

RSpec.describe Api::WorkgroupsController, type: :controller do
  context 'Examples' do
    describe 'workgroup relations' do
      it_behaves_like 'an object with followable', :workgroup
      it_behaves_like 'an object with saveable', :workgroup
    end

    describe 'Workgroup' do
      it_behaves_like 'an object with members', :workgroup
      it_behaves_like 'a private object with members', :workgroup
      it_behaves_like 'an object with recommendations', :workgroup
      it_behaves_like 'an object with links', :workgroup
      it_behaves_like 'an object with media', :workgroup
    end
  end

  context 'Workgroups specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @workgroup = create(:workgroup)
      # Workgroup.create(
      #   title: FFaker::Movie.title,
      #   short_title: FFaker::Internet.user_name,
      #   logo_url: FFaker::Avatar.image,
      #   description: FFaker::DizzleIpsum.paragraphs,
      #   short_description: FFaker::DizzleIpsum.paragraph,
      #   latitude: rand(-90.000000000...90.000000000),
      #   longitude: rand(-180.000000000...180.000000000),
      #   status: 0,
      #   is_private: true,
      #   creator_id: @user.id
      # )
    end

    describe '#index' do
      it 'should return list of workgroup' do
        get :index
        expect(response).to have_http_status :ok
      end
    end

    describe '#my workgroups' do
      context 'Fetching My communites with logged in user' do
        it 'should return list of Users workgroup' do
          get :my_workgroups
          expect(response).to have_http_status :ok
        end
      end

      context 'Fetching My communites without logged in user' do
        it 'should return list of Users workgroup' do
          sign_out @user
          get :my_workgroups
          expect(response).to have_http_status :unauthorized
        end
      end
    end

    describe '#create' do
      context 'duplicate short title error' do
        it 'creates a workgroup' do
          post :create, params: { workgroup: @workgroup.attributes }
          expect(response.status).to eq 422
        end
      end

      context 'creating workgroup with logged in user' do
        it 'creates a workgroup' do
          @workgroup.short_title = FFaker::Name.first_name
          post :create, params: { workgroup: @workgroup.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Workgroup', targetable_node_id: json_response['id'], relation_type: 'is_author_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Workgroup', targetable_node_id: json_response['id'], relation_type: 'is_member_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Workgroup', targetable_node_id: json_response['id'], relation_type: 'is_admin_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Workgroup', targetable_node_id: json_response['id'], relation_type: 'is_owner_of')).to exist
        end
      end

      context 'creating workgroup without logged in user' do
        it 'return unauthorized error' do
          @workgroup.short_title = FFaker::Name.first_name
          sign_out @user
          post :create, params: { workgroup: @workgroup.attributes }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#show' do
      it 'should return workgroup' do
        get :show, params: { id: @workgroup.id }
        expect(response.status).to eq 200
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @workgroup.class.name, targetable_node_id: @workgroup.id, relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      context 'Updating workgroup with logged in user and with admin role' do
        it 'Updates a workgroup' do
          @user.add_role :admin, @workgroup
          put :update, params: { workgroup: { description: 'New Description' }, id: @workgroup.id }
          expect(response.status).to eq 200
        end
      end

      context 'User cant update because user is not an admin' do
        it 'Should throw Forbidden error' do
          put :update, params: { workgroup: { description: 'New Description' }, id: @workgroup.id }
          expect(response.status).to eq 403
        end
      end

      context 'without logged in user' do
        it 'Should throw unauthorized error' do
          sign_out @user
          put :update, params: { workgroup: { description: 'New Description' }, id: @workgroup.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#destroy' do
      context 'with logged in user' do
        it 'Should Delete the workgroup' do
          delete :destroy, params: { id: @workgroup.id }
          expect(response.status).to eq 200
        end
      end

      context 'without logged in user' do
        it 'Should not delete the workgroup' do
          sign_out @user
          delete :destroy, params: { id: @workgroup.id }
          expect(response.status).to eq 401
        end
      end
    end
  end
end
