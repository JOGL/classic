# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/clappable'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/saveable'

RSpec.describe Api::PostsController, type: :controller do
  context 'Examples' do
    describe 'post relations' do
      it_behaves_like 'an object with clappable', :post
      it_behaves_like 'an object with saveable', :post
    end

    describe 'Post' do
      it_behaves_like 'an object with recommendations', :post
    end
  end

  context 'Post specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @project = create(:project, creator_id: @user.id)
      @user.add_role :admin, @project
      @user.can_contact = true
      @need = create(:need, project_id: @project.id, user_id: @user.id)
      @feed = create(:feed, feedable: @need)
      @post = create(:post, feed_id: @feed.id, user_id: @user.id)
    end

    describe '#create' do
      context 'Unauthorized' do
        it "Should Return Unauthorized error because user's not signed in" do
          sign_out @user
          post :create, params: { post: @post.attributes }
          expect(response.status).to eq 401
        end
      end

      context 'Authorized' do
        it 'fails to create a post' do
          post :create, params: { post: { feed_id: @post.feed_id } }
          expect(response.status_message).to eq('Unprocessable Entity')
        end

        it 'creates a post' do
          post :create, params: { post: @post.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(json_response['data']).to eq 'Your post has been published'
          post = Post.find(json_response['id'])
          expect(post.from).to eq(@need)
          expect(RecsysDatum.where(sourceable_node: @user, targetable_node: post, relation_type: 'is_author_of')).to exist
          expect(RecsysDatum.where(sourceable_node: @user, targetable_node: post.from, relation_type: 'has_posted_on')).to exist
        end

        it 'creates a post with a user mention' do
          target = create(:confirmed_user)
          attrs = @post.attributes
          attrs[:mentions] = []
          attrs[:mentions] << {
            obj_type: 'user',
            obj_id: target.id,
            obj_match: target.first_name
          }
          expect do
            post :create, params: { post: attrs }
          end.to change(Mention, :count).by(1)
        end

        it 'creates a post with a user mention and notifies the user' do
          target = create(:confirmed_user)
          attrs = @post.attributes
          attrs[:mentions] = []
          attrs[:mentions] << {
            obj_type: 'user',
            obj_id: target.id,
            obj_match: target.first_name
          }
          expect do
            post :create, params: { post: attrs }
          end.to change(Notification, :count).by(1)
        end

        it 'creates a post with a project mention' do
          target = create(:project)
          attrs = @post.attributes
          attrs[:mentions] = []
          attrs[:mentions] << {
            obj_type: 'project',
            obj_id: target.id,
            obj_match: target.short_title
          }
          expect do
            post :create, params: { post: attrs }
          end.to change(Mention, :count).by(1)
        end

        it 'creates a post with a project mention and notifies admins' do
          target = create(:project)
          attrs = @post.attributes
          attrs[:mentions] = []
          attrs[:mentions] << {
            obj_type: 'project',
            obj_id: target.id,
            obj_match: target.short_title
          }
          expect do
            post :create, params: { post: attrs }
          end.to change(Notification, :count).by(User.with_role(:admin, target).count)
        end

        it 'creates a post and notifies object followers' do
          need_follower = create(:user, first_name: 'Need', last_name: 'Follower')
          need_follower.owned_relations.create(follows: true, resource: @need)

          expect do
            post :create, params: { post: @post.attributes }
          end.to change(Notification, :count).by(1)
          expect(response.status).to eq 201
          json_response = JSON.parse(response.body)
          post = Post.find(json_response['id'])
          notif = Notification.where(category: 'feed', target: need_follower, object: post, type: 'new_post').first
          expect(notif).to be_present
        end

        it "creates a post on the user's profile feed and triggers a notification" do
          user = create(:confirmed_user)
          sign_in user
          feed = user.feed
          follower = create(:confirmed_user)
          follower.owned_relations.create(resource: user, follows: true)

          my_post = create(:post, feed: feed, user: user, from: user)
          expect do
            post :create, params: { post: my_post.attributes }
          end.to change(Notification, :count).by(1)

          expect(response.status).to eq 201

          notif = Notification.where(object: user, type: 'new_post_on_profile_feed').first
          expect(notif).to be_truthy
          expect(notif.category).to match('feed')
          expect(notif.target).to eq(follower)
        end

        it 'cross-posts to the project feed if posting to a need feed' do
          jim = create(:confirmed_user, first_name: 'Jim', last_name: 'Jiro')
          sign_in(jim)
          need_admin = create(:user, first_name: 'Need', last_name: 'Admin')
          need = create(:need, title: 'Now Hiring', user: need_admin)
          need_feed = need.feed
          need_follower = create(:user, first_name: 'Need', last_name: 'Follower')
          need_follower.owned_relations.create(follows: true, resource: need)
          project = need.project
          project_follower = create(:user, first_name: 'Project', last_name: 'Follower')
          project_follower.owned_relations.create(follows: true, resource: project)
          project_admin = project.admins.first
          project_admin.update(first_name: 'Project', last_name: 'Admin')

          my_post = create(:post, feed: need_feed, user: jim, from: need)
          expect do
            post :create, params: { post: my_post.attributes }
          end.to change(Notification, :count).by(4)

          people_notified = Notification.last(4).map(&:target)
          expect(people_notified).to include(need_follower, need_admin, project_follower, project_admin)
          expect(people_notified).to_not include(jim)
        end

        it 'validates you are an admin before allowing post to a program' do
          program = create(:program)
          jimmy = program.users.first
          jimmy.remove_role(:member, program)
          sign_in jimmy

          post :create, params: { post: {
            feed_id: program.feed.id,
            user_id: jimmy.id,
            content: 'my awesome post'
          } }

          expect(response).to be_successful
          expect(JSON.parse(response.body)['data']).to eq('Your post has been published')
        end
      end
    end

    describe '#show' do
      it 'should return post' do
        get :show, params: { id: @post.id }
        expect(response.status).to eq 200
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @post, relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      context 'Updating post' do
        it 'Updates a post' do
          put :update, params: { post: { content: 'New content' }, id: @post.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq 'Post has been updated'
        end
      end
      context 'User not logged in' do
        it 'Will throw error' do
          sign_out @user
          put :update, params: { post: { content: 'New content' }, id: @post.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#destroy' do
      it 'Should Delete the post' do
        delete :destroy, params: { id: @post.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response['data']).to eq 'Your post has been deleted'
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @post, relation_type: 'is_author_of')).not_to exist
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @post, relation_type: 'has_posted_on')).not_to exist
      end

      it "Doesn't allow random user B to delete a post of user A" do
        user_b = create(:confirmed_user)
        sign_in user_b

        delete :destroy, params: { id: @post.id }

        expect(response.status).to eq(403) # forbidden
      end

      it 'Allows admin user B to delete a post of user A' do
        user_b = create(:confirmed_user)
        user_b.add_role(:admin, @post.feed.feedable)
        sign_in user_b

        delete :destroy, params: { id: @post.id }

        expect(response.status).to eq(200)
      end

      context 'User not logged in' do
        it 'Should not Delete the post' do
          sign_out @user
          delete :destroy, params: { id: @post.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#comment' do
      context 'with logged in user' do
        it 'create comment' do
          post :comment, params: { id: @post.id, comment: { content: 'New Comment' } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(json_response['data']).to eq 'Your comment has been published'
          expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @post, relation_type: 'has_commented_on')).to exist
          expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @post.comments.first, relation_type: 'is_author_of')).to exist
          expect(RecsysDatum.where(sourceable_node: @post.comments.first, targetable_node: @post, relation_type: 'is_comment_of')).to exist
        end

        it 'creates a comment and only triggers a notification for the post participants who are not the commenter' do
          other_user = create(:confirmed_user)
          another_user = create(:confirmed_user)
          # create a comment on the post by someone other than the post creator so we have more than one post participant
          create(:comment, post: @post, user: other_user)
          create(:comment, post: @post, user: another_user)

          # signed_in user comments on their own post, should only notify the other people who commented on the post
          expect do
            post :comment, params: { id: @post.id, comment: { content: 'New comment' } }
          end.to change(Notification, :count).by(2)
          other_notif = Notification.find_by(target: other_user, object: @post.comments.last, type: 'new_comment')
          another_notif = Notification.find_by(target: another_user, object: @post.comments.last, type: 'new_comment')
          expect(other_notif).to be_truthy
          expect(other_notif.category).to match('comment')
          expect(other_notif.target).to eq(other_user)
          expect(other_notif.metadata[:commentor].id).to be(@user.id)
          expect(another_notif).to be_truthy
          expect(another_notif.category).to match('comment')
          expect(another_notif.target).to eq(another_user)
          expect(another_notif.metadata[:commentor].id).to be(@user.id)
        end
      end

      context 'without logged in user' do
        it 'should not create a comment on a post' do
          sign_out @user
          post :comment, params: { id: @post.id, comment: { content: 'New Comment' } }
          expect(response.status).to eq 401
        end
      end

      context 'with logged in user' do
        before(:each) do
          post :comment, params: { id: @post.id, comment: { content: 'New Comment' } }
        end

        it 'get a post with comments' do
          get :show, params: { id: @post.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['id']).to eq @post.id
          expect(json_response['comments'].count).to eq 1
        end

        it 'get a post with comments' do
          sign_out @user
          get :show, params: { id: @post.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['id']).to eq @post.id
          expect(json_response['comments'].count).to eq 1
        end
      end

      context 'with logged in user' do
        it 'should update comment' do
          comment = create(:comment, post: @post, user: @user)
          @post.comments << comment
          patch :comment, params: { id: @post.id, comment: { content: 'New Comment content' }, comment_id: comment.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq 'Your post has been updated'
        end
      end

      context 'without logged in user' do
        it 'should not update comment' do
          comment = create(:comment, post: @post, user: @user)
          @post.comments << comment
          sign_out @user
          patch :comment, params: { id: @post.id, comment: { content: 'New Comment content' }, comment_id: comment.id }
          expect(response.status).to eq 401
        end
      end

      context 'logged in user' do
        it 'should remove a comment from a post' do
          comment = create(:comment, post: @post, user: @user)
          @post.comments << comment
          delete :comment, params: { id: @post.id, comment_id: comment.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq 'Comment deleted'
        end
      end

      context 'without logged in user' do
        it 'should not remove a comment from a post' do
          comment = create(:comment, post: @post, user: @user)
          @post.comments << comment
          sign_out @user
          delete :comment, params: { id: @post.id, comment_id: comment.id }
          expect(response.status).to eq 401
        end
      end
    end
  end
end
