# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/saveable'
require_relative 'shared_examples/affiliation'

RSpec.describe Api::UsersController, type: :controller do
  context 'Examples' do
    describe 'user relations' do
      it_behaves_like 'an object with followable', :user
      it_behaves_like 'an object with saveable', :user
    end

    describe 'User mixins' do
      it_behaves_like 'an object with affiliations', :confirmed_user
      it_behaves_like 'an object with recommendations', :confirmed_user
    end
  end

  # need to create some custom specs because User model is not resourcified, e.g.
  # a user cannot have admin privileges to another user,
  # basically, only a user can edit that user's external links
  describe 'linkable for user' do
    before do
      @user = create(:confirmed_user)
    end

    context 'signed in' do
      before do
        sign_in @user
      end

      it 'can get the links' do
        links = create_list(:external_link, 3)
        links.each { |link| @user.external_links << link }

        get :index_link, params: { id: @user.id }

        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq 3
        expect(json_response.pluck('id')).to include(*links.map(&:id))
      end

      it 'can add a link' do
        icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
        expect do
          post :create_link, params: { id: @user.id, url: 'http://something.com', name: 'custom', icon: icon }
        end.to change(ExternalLink, :count).by(1)
        expect(response).to have_http_status :created
        json_response = JSON.parse(response.body)
        expect(json_response['url']).to match 'http://something.com'
        expect(json_response['icon_url']).not_to be_falsey
      end

      it 'can update a link' do
        icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
        link = create(:external_link)
        @user.external_links << link
        patch :update_link, params: { id: @user.id, link_id: link.id, url: 'http://something.com', name: 'custom', icon: icon }
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response['url']).to match 'http://something.com'
        expect(json_response['icon_url']).not_to be_falsey
      end

      it 'can destroy a link' do
        link = create(:external_link)
        @user.external_links << link
        expect do
          delete :destroy_link, params: { id: @user.id, link_id: link.id }
        end.to change(ExternalLink, :count).by(-1)
        expect(response).to have_http_status :ok
      end
    end

    context 'not signed in' do
      it 'can get the links' do
        links = create_list(:external_link, 3)
        links.each { |link| @user.external_links << link }

        get :index_link, params: { id: @user.id }

        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq 3
        expect(json_response.pluck('id')).to include(*links.map(&:id))
      end

      it 'cannot add a link' do
        icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
        expect do
          post :create_link, params: { id: @user.id, url: 'http://something.com', name: 'custom', icon: icon }
        end.to change(ExternalLink, :count).by(0)
        expect(response).to have_http_status :unauthorized
      end

      it 'cannot update a link' do
        icon = Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png')
        link = create(:external_link)
        @user.external_links << link
        patch :update_link, params: { id: @user.id, link_id: link.id, url: 'http://something.com', name: 'custom', icon: icon }
        expect(response).to have_http_status :unauthorized
      end

      it 'cannot destroy a link' do
        link = create(:external_link)
        @user.external_links << link
        expect do
          delete :destroy_link, params: { id: @user.id, link_id: link.id }
        end.to change(ExternalLink, :count).by(0)
        expect(response).to have_http_status :unauthorized
      end
    end
  end

  context 'not signed in' do
    it 'show response does not include private field' do
      user = create(:confirmed_user)
      get :show, params: { id: user.id }
      json_response = JSON.parse(response.body)
      expect(json_response['id']).to eq(user.id)
      expect(json_response.keys.include?('gender')).to eq(false)
      expect(json_response.keys.include?('birth_date')).to eq(false)
    end
  end

  context 'Users Specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe '#index' do
      it 'should return list of users' do
        get :index
        users = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(users.count).to eq 1
      end
    end

    describe '#create' do
      context 'user not exists' do
        let!(:user_params) do
          user = build(:user)
          {
            email: user.email,
            password: '12345678',
            nickname: user.nickname
          }
        end

        it 'gives error password confirmation does not match' do
          post :create, params: { user: user_params }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 422
          expect(json_response['error']).to eq('Password confirmation does not match the password')
        end

        it 'should create the user if password_confirmation matched' do
          post :create, params: { user: user_params.merge(password_confirmation: '12345678') }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(json_response['msg']).to eq('Thank you for signing up, please confirm your email address to continue')
        end

        it 'allows birth_date and gender fields' do
          user_params.merge!(
            first_name: 'jim',
            password_confirmation: '12345678',
            birth_date: Date.today - 20.years,
            gender: 'male'
          )
          expect do
            post :create, params: { user: user_params }
          end.to change { User.count }.by(1)
          sign_out(@user)
          jim = User.last
          jim.confirm
          sign_in(jim)

          get :show, params: { id: jim.id }
          json_response = JSON.parse(response.body)
          expect(json_response['age']).to eq(20)
          expect(json_response['gender']).to eq('male')
        end
      end

      context 'user already exists' do
        let!(:user_params) do
          {
            email: @user.email,
            password: '12345678',
            password_confirmation: '12345678',
            nickname: @user.nickname
          }
        end

        it 'gives error user already exists' do
          post :create, params: { user: user_params }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 422
          expect(json_response['error']).to eq('User already exists')
        end
      end
    end

    describe '#show' do
      it 'should return user' do
        users = create_list(:confirmed_user, 4)
        users.each do |user_to_follow|
          @user.owned_relations.create(resource: user_to_follow, follows: true)
        end
        get :show, params: { id: @user.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response['stats']['following_count']).to eq(4)
        expect(json_response['age']).to be_present
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @user, relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      let!(:user) { create(:user) }
      let!(:skill) { create(:skill) }
      context 'update with interest' do
        it 'should not update if interest is missing' do
          expect do
            put :update, params: { id: @user.id, user: { nickname: 'test12', interests: [1923] } }
          end.to change { @user.interests.count }.by(0)
        end

        it 'update if interest present in database' do
          put :update, params: { id: @user.id, user: { nickname: 'test12', interests: [3] } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq 'User updated'
        end
      end

      context 'update with skill' do
        it 'should update with skill name' do
          put :update, params: { id: @user.id, user: { nickname: 'test12', skills: ['Big Data'] } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq 'User updated'
          expect(@user.skills.count).to eq 1
        end
      end

      context 'update with ressource' do
        it 'should update with ressource name' do
          put :update, params: { id: @user.id, user: { nickname: 'test12', ressources: ['3D printers'] } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq 'User updated'
          @user.reload
          expect(@user.ressources.count).to eq 1
          expect(@user.ressources.first.ressource_name).to match('3d printer')
        end
      end

      context 'update nickname' do
        it 'gives error if nickname already taken' do
          put :update, params: { id: @user.id, user: { nickname: user.nickname } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 403
          expect(json_response['data']).to eq 'Nickname is already taken'
        end

        it 'should update user' do
          put :update, params: { id: @user.id, user: { nickname: 'test12' } }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq 'User updated'
          expect(User.find(@user.id).nickname).to eq 'test12'
        end
      end
    end

    describe '#destroy' do
      it 'should updated the status of user' do
        @user.add_role(:member)
        @user.add_role(:something)
        delete :destroy
        JSON.parse(response.body)
        expect(response.status).to eq 200
        @user.reload
        expect(@user.roles.size).to eq(0)
        expect(@user.archived?).to be_truthy
      end
    end

    describe '#mutual' do
      before do
        @user1 = create(:confirmed_user)
        @user2 = create(:confirmed_user)
        @user3 = create(:confirmed_user)
        @other = create(:confirmed_user)
        @user.owned_relations.create(resource: @user1, follows: true)
        @user.owned_relations.create(resource: @user2, follows: true)
        @user.owned_relations.create(resource: @user3, follows: true)
        @other.owned_relations.create(resource: @user1, follows: true)
        @other.owned_relations.create(resource: @user3, follows: true)
      end

      it 'returns the mutual followers between two users' do
        get :mutual, params: { id: @other.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(json_response.count).to eq 2
        expect(json_response.pluck('id')).to include(@user1.id)
        expect(json_response.pluck('id')).to include(@user3.id)
        expect(json_response.pluck('id')).not_to include(@user2.id)
      end
    end

    describe '#saved' do
      before do
        @project = create(:project, creator_id: @user.id)
        @workgroup = create(:workgroup, creator_id: @user.id)
        @need = create(:need, project_id: @project.id, user_id: @user.id)
        @challenge = create(:challenge)
        @user2 = create(:confirmed_user)
        @user3 = create(:confirmed_user)
        @user.owned_relations.create(resource: @project, saved: true)
        @user.owned_relations.create(resource: @workgroup, saved: true)
        @user.owned_relations.create(resource: @need, saved: true)
        @user.owned_relations.create(resource: @challenge, saved: true)
        @user.owned_relations.create(resource: @user2, saved: true)
        @user.owned_relations.create(resource: @user3, saved: true)
      end

      it 'should get the collection of saved items' do
        get :saved_objects
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['projects'].count).to eq 1
        expect(json_response['projects'][0]['id']).to eq @project.id

        expect(json_response['needs'].count).to eq 1
        expect(json_response['needs'][0]['id']).to eq @need.id

        expect(json_response['users'].count).to eq 2
        expect(json_response['users'][0]['id']).to eq @user2.id
        expect(json_response['users'][1]['id']).to eq @user3.id

        expect(json_response['challenges'].count).to eq 1
        expect(json_response['challenges'][0]['id']).to eq @challenge.id

        expect(json_response['workgroups'].count).to eq 1
        expect(json_response['workgroups'][0]['id']).to eq @workgroup.id
      end
    end

    describe 'Avatar Upload' do
      it 'should allow a PNG image' do
        @user.add_role :admin, @program
        png = fixture_file_upload('/authorized.png', 'image/png')
        post :upload_avatar, params: { id: @user.id, avatar: png }
        expect(response).to be_successful
      end

      it 'should not allow a TIFF image' do
        @user.add_role :admin, @program
        tiff = fixture_file_upload('/unauthorized.tiff', 'image/tiff')
        post :upload_avatar, params: { id: @user.id, avatar: tiff }
        expect(response.status).to eq(422)
      end
    end

    describe '#send_private_email' do
      it 'does stuff' do
        @user.update(direct_message_count: 20)
        post :send_private_email, params: { id: @user.id, content: 'whatevs' }
        expect(response).to be_forbidden
      end
    end
  end
end
