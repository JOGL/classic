# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with skills' do |factory|
  before do
    @object = build(factory)
  end

  context 'Signed in' do
    before(:each) do
      @user = create(:confirmed_user, first_name: 'Jimmy', last_name: 'Friedman')
      sign_in @user
    end

    describe '#create' do
      before do
        @user.add_role :admin if factory == :challenge
      end

      it "creates a #{factory} with skills" do
        params = {}
        params[factory] = @object.attributes
        params[factory].delete('faq')
        params[factory][:skills] = ['Juggling', 'Dancing', 'Computer Hacking']
        post :create, params: params
        expect(response).to be_successful
        expect(JSON.parse(response.body)['skills']).to eq(['Juggling', 'Dancing', 'Computer hacking'])
      end
    end
  end

  context 'Not Signed In' do
    before(:each) do
      @user = create(:confirmed_user)
      @user2 = create(:confirmed_user)
      @user2.add_role :member, @object
    end
  end
end
