# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'a private object with members' do |factory|

  context 'Signed in' do
    before(:each) do
      @object = create(factory, is_private: true, title: 'Super Duper')
      @user = create(:confirmed_user, first_name: 'Keller', last_name: 'Wells')
      sign_in @user
    end
    describe '#join' do
      it 'should trigger two notifications' do
        expect do
          put :join, params: { id: @object.id }
        end.to change { Notification.count }.by(2).and change { @object.users.count }.by(1)
      end

      it 'should trigger a pending_member notification' do
        put :join, params: { id: @object.id }
        notif = Notification.find_by(object: @object, type: 'pending_member')

        expect(notif).to be_truthy
        expect(notif.category).to match('administration')
        expect(notif.target).to eq(@object.users.first)
        expect(notif.subject_line).to match(/Keller Wells requested to join your .+: Super Duper/)
      end

      it 'then trigger a pending_join_request notification' do
        put :join, params: { id: @object.id }
        second_notif = Notification.find_by(object: @object, type: 'pending_join_request')

        expect(second_notif).to be_truthy
        expect(second_notif.category).to match('membership')
        expect(second_notif.target).to eq(@user)
        expect(second_notif.subject_line).to match(/\AYour request to join .* ".*" is pending\Z/)
      end
    end

    describe 'user can be approved and join private object' do
        
      it 'user have a pending role of private object' do
        expect do
          put :join, params: { id: @object.id }
        end.to change { Notification.count }.by(2).and change { @object.users.count }.by(1)

        expect(@user.has_role?(:pending, @object)).to be true
      end

      it 'should update user to a member role' do
        sign_out @user
        sign_in @object.creator
        notif = Notification.find_by(type: 'pending_join_request_approved')

        expect do
          post :update_member, params: { id: @object.id, user_id: @user.id, new_role: 'member' }
        end.to change { Notification.count }.by(1)

        notif = Notification.find_by(type: 'pending_join_request_approved')

        expect(@user.has_role?(:member, @object)).to be true
        expect(notif.target).to eq(@user)
        expect(notif.object).to eq(@object)
        expect(notif.subject_line).to match(/Your request to join the .+ "Super Duper" was approved!/)
      end
    end
  end

    describe '#members_list' do
    it 'should include owners/admins/members and not include pending_members' do
      object = create(factory, is_private: true, title: 'Super Duper', creator: create(:user, first_name: 'Bob'))
      pending_user = create(:confirmed_user, first_name: 'Pending')
      pending_user.add_role :pending, object
      admin = create(:confirmed_user, first_name: 'Admin')
      admin.add_role(:admin, object)
      member = create(:confirmed_user, first_name: 'Member')
      member.add_role(:member, object)

      get :members_list, params: { id: object.id }

      json_response = JSON.parse(response.body)['members']
      expect(json_response.count).to eq(3)
      expect(json_response.map { |m| m['first_name'] }).to_not include('Pending')
      expect(json_response.map { |m| m['first_name'] }).to include('Bob', 'Admin', 'Member')
    end

    it 'should include pending_members if param is present' do
      object = create(factory, is_private: true, title: 'Super Duper', creator: create(:user, first_name: 'Bob'))
      pending_user = create(:confirmed_user, first_name: 'Pending')
      pending_user.add_role :pending, object
      admin = create(:confirmed_user, first_name: 'Admin')
      admin.add_role(:admin, object)
      member = create(:confirmed_user, first_name: 'Member')
      member.add_role(:member, object)

      get :members_list, params: { id: object.id, status: 'pending' }

      json_response = JSON.parse(response.body)['members']
      expect(json_response.count).to eq(1)
      expect(json_response.map { |m| m['first_name'] }).to include('Pending')
      expect(json_response.map { |m| m['first_name'] }).to_not include('Bob', 'Admin', 'Member')
    end
  end

  describe '#update_member' do
    it 'should accept a pending_member to become a member' do
      object = create(factory, is_private: true, title: 'Super Duper')
      keller = create(:confirmed_user, first_name: 'Keller', last_name: 'Wells')
      sign_in keller
      keller.add_role :member, object
      keller.add_role :admin, object
      keller.add_role :owner, object

      penn = create(:confirmed_user, first_name: 'Penn', last_name: 'Jillette')
      penn.add_role :pending, object

      post :update_member, params: { id: object.id, user_id: penn.id, new_role: :member }

      expect(penn.has_role?(:member, object)).to eq(true)
      expect(penn.has_role?(:pending, object)).to eq(false)
    end
  end
end
