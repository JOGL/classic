# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'with affiliated' do |factory|
  let(:parent) { create(factory) }
  let(:affiliate) { create(:affiliate) }
  let(:affiliate_type) { affiliate.class.name.pluralize.downcase }
  let(:user) { create(:confirmed_user) }

  describe '#affiliated_index' do
    it 'returns affiliated children' do
      parent.add_affiliate affiliate

      get :affiliated_index, params: { id: parent.id, affiliate_type: affiliate_type }
      json_response = JSON.parse(response.body)

      expect(json_response['affiliates'][affiliate_type].first).to include('affiliate_id' => affiliate.id, 'status' => 'accepted')
    end

    it 'returns an empty list if there are no affiliated children' do
      get :affiliated_index, params: { id: parent.id, affiliate_type: affiliate_type }
      json_response = JSON.parse(response.body)

      expect(json_response['affiliates'][affiliate_type].count).to eq 0
    end
  end

  describe '#affiliated_stats' do
    it 'returns the correct counts based on affiliate types' do
      parent.add_affiliate affiliate
      parent.add_affiliate user

      get :affiliated_stats, params: { id: parent.id }
      json_response = JSON.parse(response.body)

      expect(json_response).to include('total' => 2, affiliate.class.name => 1, user.class.name => 1)
    end

    it 'excludes pending affiliations from the count' do
      affiliate.add_affiliation_to parent

      get :affiliated_stats, params: { id: parent.id }
      json_response = JSON.parse(response.body)

      expect(json_response[affiliate.class.name]).to be_nil
    end
  end

  describe '#affiliated_create' do
    it 'returns unauthorized if not signed in' do
      post :affiliated_create, params: { id: parent.id, affiliate_type: affiliate.class.name, affiliate_id: affiliate.id }

      expect(response).to have_http_status :unauthorized
    end
  end

  context 'when signed in' do
    before do
      sign_in user
    end

    describe '#affiliated_create' do
      it 'returns forbidden if current user is not the parent admin' do
        post :affiliated_create, params: { id: parent.id, affiliate_type: affiliate.class.name, affiliate_id: affiliate.id }

        expect(response).to have_http_status :forbidden
      end

      it 'returns not found if the affiliate type does not exist' do
        user.add_role(:admin, parent)

        post :affiliated_create, params: { id: parent.id, affiliate_type: 'foo', affiliate_id: affiliate.id }

        expect(response).to have_http_status :not_found
      end

      it 'creates an affiliation' do
        user.add_role(:admin, parent)

        post :affiliated_create, params: { id: parent.id, affiliate_type: affiliate.class.name, affiliate_id: affiliate.id }

        expect(Affiliation.find_by(affiliate: affiliate, parent: parent)).not_to be_nil
      end
    end
  end
end
