# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with clappable' do |factory|
  before do
    @object = create(factory)
  end

  context 'Signed in' do
    before(:each) do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe '#clap' do
      it "should clap the #{described_class}" do
        put :clap, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@object.clappers.count).to eq 1
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'has_clapped')).to exist
      end

      it 'should trigger a notification' do
        expect do
          put :clap, params: { id: @object.id }
        end.to change(Notification, :count).by(1)
        notification = Notification.last
        expect(notification.type).to match('new_clap')
        expect(notification.category).to match('clap')
        expect(notification.object).to eq @object
        if @object.instance_of?(User)
          expect(notification.target).to eq @object
        else
          expect(notification.target).to eq @object.users.first
        end
        expect(notification.metadata[:clapper].id).to be(@user.id)
      end
    end

    describe '#unclap' do
      before do
        @relation = @user.owned_relations.create(resource: @object, has_clapped: true)
        @user.add_edge(@object, 'has_clapped')
      end

      it "should unclap the #{described_class}" do
        expect(@object.clappers.count).to eq 1
        delete :clap, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@object.clappers.count).to eq 0
        expect(RecsysDatum.where(sourceable_node: @user, targetable_node: @object, relation_type: 'has_clapped')).not_to exist
      end
    end

    describe '#is_clap' do
      before do
        @relation = @user.owned_relations.create(resource: @object, has_clapped: true)
      end

      it 'should answer true' do
        get :clap, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_clapped']).to eq true
      end

      it 'should answer false' do
        @relation.has_clapped = false
        @relation.save
        get :clap, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_clapped']).to eq false
      end

      it 'should answer no_found' do
        @relation.delete
        get :clap, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_clapped']).to eq false
      end
    end

    describe '#clappers' do
      before do
        @relation = @user.owned_relations.create(resource: @object, has_clapped: true)
        @user.update(can_contact: true) if @object.instance_of?(Post)
      end

      it "should get the clappers of the #{described_class}" do
        get :clappers, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['clappers'].count).to eq 1
        expect(json_response['clappers'][0]['id']).to eq @user.id
        expect(json_response['clappers'][0]['nickname']).to eq @user.nickname
        if @object.instance_of?(Post)
          expect(json_response['clappers'][0]['projects_count']).to eq @user.projects_count
          expect(json_response['clappers'][0]['can_contact']).to eq @user.can_contact
        end
      end

      it 'should render deleted if the user has been deleted' do
        @user.archived!
        get :clappers, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['clappers'].count).to eq 1
        expect(json_response['clappers'][0]['id']).to eq(-1)
      end
    end
  end

  # Testing the Auth methods

  context 'Not Signed In' do
    it 'should require auth' do
      put :clap, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
      expect(@object.clappers.count).to eq 0
    end

    it 'should require auth' do
      delete :clap, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
      expect(@object.clappers.count).to eq 0
    end

    it 'should require auth' do
      get :clap, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end
  end
end
