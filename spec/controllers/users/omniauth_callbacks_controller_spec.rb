# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Users::OmniauthCallbacksController, type: :controller do
  include Devise::Test::ControllerHelpers

  describe 'eventbrite' do
    before do
      @space = create(:space)

      @request.env['devise.mapping'] = Devise.mappings[:user]

      @request.env['omniauth.params'] = {
        'serviceable_id' => @space.id,
        'serviceable_type' => 'space',
        'user_id' => @space.creator.id
      }

      @request.env['omniauth.auth'] = { 'credentials' => { 'token' => 'FOOBAR' } }
    end

    it 'creates a new service with the given token' do
      post :eventbrite

      service = Service.find_by(serviceable_id: @space.id, serviceable_type: 'Space')

      expect(service.token).to eq('FOOBAR')
    end

    it 'updates the service token' do
      @request.env['omniauth.auth'] = { 'credentials' => { 'token' => 'BARBAZ' } }

      post :eventbrite

      service = Service.find_by(serviceable_id: @space.id, serviceable_type: 'Space')

      expect(service.token).to eq('BARBAZ')
    end

    it 'returns 404 when a request is not serviceable?' do
      @request.env['omniauth.params'] = {}

      post :eventbrite

      expect(response).to have_http_status(:not_found)
    end
  end
end
