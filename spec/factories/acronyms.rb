# frozen_string_literal: true

FactoryBot.define do
  factory :acronym do
    acronym_name { FFaker::UnitEnglish.length_abbr }
    probable_skill_name { FFaker::Name.name }
    probability { rand }
    is_in_parenthesis { [true, false].sample }
    count { rand(150) }
  end
end
