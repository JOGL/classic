# frozen_string_literal: true

FactoryBot.define do
  factory :project, aliases: %i[ affiliate mediumable ] do
    creator

    title { FFaker::Movie.unique.title }
    short_title { FFaker::Internet.unique.user_name }
    logo_url { FFaker::Avatar.image }
    description { FFaker::DizzleIpsum.paragraph }
    short_description { FFaker::DizzleIpsum.paragraph }

    latitude { rand(-90.000000000...90.000000000) }
    longitude { rand(-180.000000000...180.000000000) }

    # Feed.create({project_id: self.id, object_type:'project'})

    status { 2 }

    after :create do |project|
      skills = create_list(:skill, 5)
      skills.map do |skill|
        project.skills << skill # has_many
      end
      project.creator.add_role :member, project
      project.creator.add_role :admin, project
      project.creator.add_role :owner, project
    end
  end

  factory :public_project, parent: :project do
    is_private { false }
  end

  factory :archived_project, parent: :project do
    status { 1 }
  end

  factory :private_project, parent: :project do
    is_private { true }
  end
end
