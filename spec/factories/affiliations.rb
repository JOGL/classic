# frozen_string_literal: true

FactoryBot.define do
  factory :affiliation do
    affiliate
    parent
  end
end
