# frozen_string_literal: true

FactoryBot.define do
  factory :dataset do
    title { FFaker::Movie.title }
    description { FFaker::DizzleIpsum.paragraph }
    type { 'Datasets::CustomDataset' }
    url { FFaker::Internet.http_url }
    author
  end
end
