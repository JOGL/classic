# frozen_string_literal: true

FactoryBot.define do

  factory :space, aliases: %i[ parent ] do
    creator
    description { 'MyString' }
    title { FFaker::Movie.unique.title }
    short_title { FFaker::Internet.unique.user_name }
    short_description { FFaker::Movie.title }

    after :create do |space|
      space.creator.add_role :member, space
      space.creator.add_role :admin, space
      space.creator.add_role :owner, space
    end
  end
end
