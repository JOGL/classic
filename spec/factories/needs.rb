# frozen_string_literal: true

FactoryBot.define do
  factory :need do
    project
    user
    title { FFaker::Name.first_name }
    content { FFaker::DizzleIpsum.paragraph }
    status { 0 }

    after :create do |need|
      skills = create_list(:skill, 5)
      skills.map do |skill|
        need.skills << skill # has_many
      end
      ressources = create_list(:ressource, 5)
      ressources.map do |ressource|
        need.ressources << ressource # has_many
      end
      need.user.add_role :member, need
      need.user.add_role :admin, need
      need.user.add_role :owner, need
    end
  end
end
