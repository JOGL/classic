# frozen_string_literal: true

FactoryBot.define do
  factory :service_flow_data do
    service { create(:service) }
    flow { ServiceFlowData.flows.keys.first }
    data { JSON.generate(foo: 'bar') }
  end
end
