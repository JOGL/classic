# frozen_string_literal: true

FactoryBot.define do
  factory :activity do
    id { [create(:service).id, FFaker::Guid.guid] }
    category { Activity.categories.keys.first }
    name { FFaker::Book.title }
    description { FFaker::Book.description }
    start { FFaker::Time.datetime }
    add_attribute(:end) { FFaker::Time.datetime }
    location { FFaker::Address.street_address }
    latitude { FFaker::Geolocation.lat }
    longitude { FFaker::Geolocation.lng }
    url { FFaker::Internet.http_url }
  end
end
