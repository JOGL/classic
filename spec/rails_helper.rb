# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'

require 'spec_helper'
require File.expand_path('../config/environment', __dir__)
abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'rspec/rails'
require 'rake'
require 'factory_bot_rails'
Dir[Rails.root.join('spec/support/**/*.rb')].sort.each { |f| require f }

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # Run each example within a transaction.
  config.use_transactional_fixtures = true

  # [Devise Test helpers](https://github.com/heartcombo/devise#test-helpers).
  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include Devise::Test::IntegrationHelpers, type: :request

  # [Bullet](https://github.com/flyerhzm/bullet) helps find N+1 queries
  if Bullet.enable?
    config.before(:each) { Bullet.start_request }
    config.after(:each)  { Bullet.end_request }
  end

  # Filter rails lines from backtraces. [See more](https://relishapp.com/rspec/rspec-rails/docs/backtrace-filtering)
  config.filter_rails_from_backtrace!

  # [Shoulda Matchers](https://github.com/thoughtbot/shoulda-matchers) provide one-liner tests.
  config.include Shoulda::Matchers::ActiveModel, type: :model
  config.include Shoulda::Matchers::ActiveRecord, type: :model

  DatabaseCleaner.url_allowlist = [%r{^postgres://jogl:password}]

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
    Rails.application.load_tasks
    Rake::Task['db:seed:interests'].invoke
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end

  config.backtrace_inclusion_patterns = [/app|spec/]
end

RSpec::Sidekiq.configure do |config|
  config.warn_when_jobs_not_processed_by_sidekiq = false
end
