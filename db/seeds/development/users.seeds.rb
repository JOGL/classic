# frozen_string_literal: true

after do
  return unless User.count.zero?

  first_name = FFaker::Name.first_name
  last_name = FFaker::Name.last_name
  nickname = FFaker::Name.first_name
  domain = FFaker::Internet.domain_name
  email = "#{first_name}.#{last_name}@#{domain}"
  password = (1..2).collect { |_| FFaker::DizzleIpsum.word.gsub(/\s+/, '').downcase }.join

  user = User.create!(first_name: first_name,
                      last_name: last_name,
                      nickname: nickname,
                      email: email,
                      password: password,
                      password_confirmation: password,
                      confirmed_at: DateTime.now,
                      email_confirmed: true)

  puts "USER #{user.id} created in #{Rails.env} environment with (email) #{user.email} (password) #{user.password}"
end
