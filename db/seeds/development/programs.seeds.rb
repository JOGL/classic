# frozen_string_literal: true

after 'development:users' do
  program = Program.create!(title: FFaker::Book.unique.title)

  User.first.add_role(:admin, program)
  User.first.add_role(:member, program)
  User.first.add_role(:owner, program)
end
