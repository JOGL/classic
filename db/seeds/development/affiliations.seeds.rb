# frozen_string_literal: true

after 'development:spaces', 'development:projects' do
  Space.first.add_affiliate(Project.second)
end
