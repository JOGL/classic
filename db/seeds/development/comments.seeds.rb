# frozen_string_literal: true

after 'development:users', 'development:posts', 'development:mentions' do
  Comment.create!(user_id: User.first.id, post_id: Post.first.id, content: FFaker::Book.title)
end
