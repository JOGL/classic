# frozen_string_literal: true

after 'development:users', 'development:projects' do
  space = Space.create!(creator: User.first, title: FFaker::Book.title, short_title: FFaker::Book.title, status: 'active')

  space.projects << Project.first

  User.first.add_role(:admin, space)
end
