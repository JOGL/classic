# frozen_string_literal: true

after 'development:users' do
  3.times do
    project = Project.create!(creator: User.first, title: FFaker::Book.title, short_title: FFaker::Book.title)
    User.first.add_role(:admin, project)
  end
end
