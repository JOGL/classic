# frozen_string_literal: true

after 'development:users', 'development:spaces' do
  service = Service.new(creator: User.first, name: Service.names.keys.first, token: FFaker::Guid.guid.to_s.upcase)

  Space.first.services << service
end