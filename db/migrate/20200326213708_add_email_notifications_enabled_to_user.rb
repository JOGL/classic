# frozen_string_literal: true

class AddEmailNotificationsEnabledToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :email_notifications_enabled, :boolean, default: true
  end
end
