class FixFaq < ActiveRecord::Migration[5.2]
  def up
    Program.all.each do |program|
      faq = Faq.create(faqable: program)
      program.faq = faq
      faq.save
      program.save
    end
  end

  def down
    Program.all.each do |program|
      faq = program.faq
      faq.destroy
    end
  end
end
