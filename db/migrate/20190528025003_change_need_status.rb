# frozen_string_literal: true

class ChangeNeedStatus < ActiveRecord::Migration[5.2]
  def change
    change_column :needs, :status, 'integer USING CAST(status AS integer)'
  end
end
