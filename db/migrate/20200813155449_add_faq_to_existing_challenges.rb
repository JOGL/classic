# frozen_string_literal: true

class AddFaqToExistingChallenges < ActiveRecord::Migration[5.2]
  def change
    Challenge.all.each(&:create_faq)
  end
end
