# frozen_string_literal: true

class CreateLanguageConverters < ActiveRecord::Migration[5.2]
  def change
    create_table :language_converters do |t|
      t.string :key_name
      t.string :content
      t.string :language
      t.string :languagable_type
      t.bigint :languagable_id
      t.timestamps
    end
  end
end
