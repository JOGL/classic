# frozen_string_literal: true

class AddPostFrom < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :from_object, :string
    add_column :posts, :from_id, :bigint
    add_column :posts, :from_name, :string
  end
end
