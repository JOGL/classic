class Nullerror < ActiveRecord::Migration[6.1]
  def change
    change_column_null :recsys_skills_referential, :created_at, false, Time.now
    change_column_null :recsys_skills_referential, :updated_at, false, Time.now
  end
end
