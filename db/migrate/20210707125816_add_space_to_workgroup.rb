class AddSpaceToWorkgroup < ActiveRecord::Migration[6.1]
  def change
    add_column :workgroups, :space_id, :int
  end
end
