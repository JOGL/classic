# frozen_string_literal: true

class FixProgramStatusType < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :status, :active_status
    add_column :users, :status, :string

    rename_column :programs, :status, :old_status
    add_column :programs, :status, :integer

    Program.all.each do |program|
      program.status = %w[draft soon active completed].index(program.old_status)
      program.save!
    end

    remove_column :programs, :old_status
  end
end
