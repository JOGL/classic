class AddCustomChallengeNameToSpaces < ActiveRecord::Migration[6.1]
  def change
    add_column :spaces, :custom_challenge_name, :string
  end
end
