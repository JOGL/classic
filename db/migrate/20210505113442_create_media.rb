class CreateMedia < ActiveRecord::Migration[6.1]
  def change
    create_table :media do |t|
      t.string :title
      t.string :alt_text
      t.timestamps
    end
    add_reference :media, :mediumable, polymorphic: true, index: true
  end
end
