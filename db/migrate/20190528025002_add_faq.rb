# frozen_string_literal: true

class AddFaq < ActiveRecord::Migration[5.2]
  def change
    add_column :programs, :faq, :string
    add_column :programs, :enablers, :string
    add_column :programs, :ressources, :string
    add_column :programs, :short_description, :string
  end
end
