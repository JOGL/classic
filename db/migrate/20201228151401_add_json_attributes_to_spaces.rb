class AddJsonAttributesToSpaces < ActiveRecord::Migration[5.2]
  def change
    add_column :spaces, :json_attributes, :jsonb
  end
end
