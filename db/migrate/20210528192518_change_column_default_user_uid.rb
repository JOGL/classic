class ChangeColumnDefaultUserUid < ActiveRecord::Migration[6.1]
  def change
    change_column_default :users, :uid, from: '', to: ->{ 'uuid_generate_v1()' }
  end
end
