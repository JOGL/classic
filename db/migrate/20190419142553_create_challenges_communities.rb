# frozen_string_literal: true

class CreateChallengesCommunities < ActiveRecord::Migration[5.2]
  def change
    create_table :challenges_communities do |t|
      t.belongs_to :challenge, index: true
      t.belongs_to :community, index: true
      t.string :status
      t.timestamps
    end
  end
end
