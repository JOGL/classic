# frozen_string_literal: true

class CreateCustomField < ActiveRecord::Migration[5.2]
  def change
    create_table :customfields do |t|
      t.references :resource, polymorphic: true
      t.string 'description'
      t.string 'name'
      t.string 'field_type'
      t.boolean 'optional'
    end

    create_table :customdata do |t|
      t.belongs_to :customfield, index: true
      t.belongs_to :user, index: true
      t.string 'value'
    end

    add_index :customdata, %i[customfield_id user_id], unique: true, name: 'by_user_and_customfield'
  end
end
