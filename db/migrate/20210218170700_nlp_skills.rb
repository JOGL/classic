class NlpSkills < ActiveRecord::Migration[6.1]
  def change
    # Add missing columns for NLP
    add_column :skills, :enriched_skill_name, :string
    add_column :skills, :stemmed_skill_name, :string
    add_column :skills, :is_linkedin_skill, :boolean
    add_column :skills, :count, :integer
    add_column :skills, :created_at, :timestamp
    add_column :skills, :updated_at, :timestamp
  end
end
