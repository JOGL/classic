# frozen_string_literal: true

class AddNeedLoc < ActiveRecord::Migration[5.2]
  def change
    add_column :needs, :latitude, :float
    add_column :needs, :longitude, :float
    add_column :needs, :address, :string
    add_column :needs, :city, :string
    add_column :needs, :country, :string
  end
end
