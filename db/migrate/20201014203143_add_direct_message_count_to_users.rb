# frozen_string_literal: true

class AddDirectMessageCountToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :direct_message_count, :integer, default: 0
  end
end
