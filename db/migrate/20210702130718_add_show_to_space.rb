class AddShowToSpace < ActiveRecord::Migration[6.1]
  def change
    add_column :spaces, :show_associated_projects, :bool
    add_column :spaces, :show_associated_users, :bool
  end
end
