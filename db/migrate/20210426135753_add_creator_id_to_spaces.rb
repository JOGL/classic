class AddCreatorIdToSpaces < ActiveRecord::Migration[6.1]
  def up
    add_column :spaces, :creator_id, :integer

    Space.all.each do |space|
      space.creator = space.owners.first
      space.save
    end
  end

  def down
    remove_column :spaces, :creator_id
  end
end