# frozen_string_literal: true

class MigrateUsersToHaveDefaultNotifAndEmailSettings < ActiveRecord::Migration[5.2]
  DEFAULT_HASHIE = Hashie::Mash.new(
    {
      'enabled' => true,
      'categories' => {
        'notification' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'follow' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'membership' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'feed' => {
          'enabled' => true,
          'delivery_methods' => { 'email' => { 'enabled' => true } }
        },
        'comment' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'administration' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'clap' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'mention' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'program' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        },
        'space' => {
          'enabled' => true,
          'delivery_methods' => {
            'email' => {
              'enabled' => true
            }
          }
        }
      },
      'delivery_methods' => {
        'email' => {
          'enabled' => true
        }
      }
    }
  )

  def up
    # there are 14 users in production with this invalid settings hash
    # so let's set them to default settings
    User.where(settings: Hashie::Mash.new({ 'delivery_methods' => { 'email' => {} } })).update_all(settings: DEFAULT_HASHIE)

    # after the above fix
    # there are 79 users in prod that don't have null email/notifications settings
    users = User.where.not(settings: {})

    # there are 15 users with {"enabled"=>false, ...}
    # which we can reduce to { "enabled"=>false }
    # because that means notification creation and email sending are both off
    users
      .select { |user| user.settings.enabled == false }
      .each { |user| user.update_column(:settings, Hashie::Mash.new({ 'enabled' => false })) }

    users.reload

    # there are now 64 users don't have null settings

    # let's find the users that have invalid settings of some sort
    # after some digging, they all match the pattern:
    # {"categories"=>{"notification"=>{"delivery_methods"=>{"email"=>{}}}
    # which I can get to with some special Hashie::Mash method chaining
    users
      .select { |user| user.settings.categories!.notification!.delivery_methods!.email == {} }
      .each { |user| user.update_column(:settings, DEFAULT_HASHIE) }

    # the rest of the users in the users collection I can leave alone because
    # they have valid email/notification configs

    # for the last step, there are 4925 users in
    # production with null email/notifications settings
    # let's update them all to have the default settings
    User.where(settings: {}).update_all(settings: DEFAULT_HASHIE)
  end

  def down
    # this will wipe original settings
    User.update_all(settings: {})
  end
end
