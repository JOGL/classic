class ChangeProjectStatusDefaultToChallengesProject < ActiveRecord::Migration[6.1]
  def change
    change_column_default :challenges_projects, :project_status, from: nil, to: 0
  end
end
