# frozen_string_literal: true

class FixRessource < ActiveRecord::Migration[5.2]
  def change
    create_table :ressourceships do |t|
      t.string :ressourceable_type
      t.bigint :ressourceable_id
      t.bigint :ressource_id
      t.timestamps
    end
  end
end
