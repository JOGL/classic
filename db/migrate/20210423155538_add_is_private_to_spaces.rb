class AddIsPrivateToSpaces < ActiveRecord::Migration[6.1]
  def change
    add_column :spaces, :is_private, :boolean, default: true
  end
end
