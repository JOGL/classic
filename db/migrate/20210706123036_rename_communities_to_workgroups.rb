class RenameCommunitiesToWorkgroups < ActiveRecord::Migration[6.1]
  def change
    rename_table :challenges_communities, :challenges_workgroups
    rename_column :challenges_workgroups, :community_id, :workgroup_id
    
    rename_table :communities, :workgroups
    
    rename_table :communities_community_tags, :workgroup_tags_workgroups
    rename_column :workgroup_tags_workgroups, :community_id, :workgroup_id
    rename_column :workgroup_tags_workgroups, :community_tag_id, :workgroup_tag_id
    
    rename_table :communities_interests, :interests_workgroups
    rename_column :interests_workgroups, :community_id, :workgroup_id
    
    rename_table :communities_skills, :skills_workgroups
    rename_column :skills_workgroups, :community_id, :workgroup_id
    
    rename_table :community_tags, :workgroup_tags
  end

  def up
    drop_table :interests_communities
  end
  
  def data
    Affiliation.where(affiliate_type: 'Community').each do |affiliation|
      affiliation.affiliate_type = 'Workgroup'
      affiliation.save!
    end

    Affiliation.where(parent_type: 'Community').each do |affiliation|
      affiliation.parent_type = 'Workgroup'
      affiliation.save!
    end
  end

  def rollback
    Affiliation.where(affiliate_type: 'Workgroup').each do |affiliation|
      affiliation.affiliate_type = 'Community'
      affiliation.save!
    end

    Affiliation.where(parent_type: 'Workgroup').each do |affiliation|
      affiliation.parent_type = 'Community'
      affiliation.save!
    end
  end
end
