class NotificationBackfill < ActiveRecord::Migration[6.1]
  def data
    Notification.where("subject_line = '' AND created_at >= :this_month", this_month: Time.now - 30.days).each do |notification|
      return if notification.object.nil?
      
      notification.cta_link = notification.object.frontend_link

      notification.subject_line = subject_line(notification)

      notification.save!
    end
  end

  private

  def subject_line(notification)
    object = notification.object
    metadata = notification.metadata
    type = notification.type
    
    args = {
      scope: 'notifications',
      default: "PLEASE DEFINE a translation for #{I18n.locale}.notifications.#{type}",
      object_type: object.class.to_s.downcase
    }
    
    # communities are called "groups" on the frontend
    args[:object_type] = 'group' if object.class == Community
    if metadata.key?(:author_id)
      if (author = User.find_by(id: metadata[:author_id]))
        args.merge!(author_first_name: author.first_name)
        args.merge!(author_last_name: author.last_name)
      else
        args.merge!(author_first_name: 'DELETED')
        args.merge!(author_last_name: 'USER')
      end
    end
    args.merge!(object_title: object.title) if object.respond_to?(:title)
    args.merge!(project_title: object.project.title) if object.respond_to?(:project)
    # ick
    if object.present? && object.respond_to?(:feed) && object&.feed&.feedable&.class&.name != 'User'
      args.merge!(feedable_title: object&.feed&.feedable&.title)
      args.merge!(feedable_type: object&.feed&.feedable&.class&.name&.downcase)
    else
      args.merge!(feedable_title: '-DELETED-')
      args.merge!(feedable_type: '-DELETED-')
    end

    args.merge!(metadata)
    
    I18n.load_path = Dir[Rails.root.join('config/locales/old.yml')]
    I18n.available_locales = [:en]
    I18n.default_locale = :en
    
    I18n.t(type, **args)
  end
end
