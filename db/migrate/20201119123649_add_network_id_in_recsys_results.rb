class AddNetworkIdInRecsysResults < ActiveRecord::Migration[5.2]
  def change
    add_column :recsys_results, :network_id, :string
  end
end
