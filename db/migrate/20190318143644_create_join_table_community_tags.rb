# frozen_string_literal: true

class CreateJoinTableCommunityTags < ActiveRecord::Migration[5.2]
  def change
    create_table :communities_community_tags, id: false do |t|
      t.integer :community_tag_id
      t.integer :community_id
    end
  end
end
