class ChallengeCustomType < ActiveRecord::Migration[6.1]
  def change
    add_column :challenges, :custom_type, :string, default: "Challenge"
  end
end
