# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_07_07_131515) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "acronyms", force: :cascade do |t|
    t.string "acronym_name"
    t.string "probable_skill_name"
    t.float "probability"
    t.boolean "is_in_parenthesis"
    t.integer "count"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activities", id: false, force: :cascade do |t|
    t.bigint "service_id", null: false
    t.string "id", null: false
    t.integer "category", null: false
    t.string "name", null: false
    t.text "description"
    t.datetime "start"
    t.datetime "end"
    t.string "location"
    t.float "latitude"
    t.float "longitude"
    t.string "url"
    t.datetime "created_at", precision: 6, default: -> { "now()" }, null: false
    t.datetime "updated_at", precision: 6, default: -> { "now()" }, null: false
    t.string "logo"
    t.index ["service_id", "id"], name: "index_activities_on_service_id_and_id", unique: true
    t.index ["service_id"], name: "index_activities_on_service_id"
  end

  create_table "activities_skills", force: :cascade do |t|
    t.bigint "activity_id"
    t.bigint "skill_id"
    t.index ["activity_id"], name: "index_activities_skills_on_activity_id"
    t.index ["skill_id"], name: "index_activities_skills_on_skill_id"
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "affiliations", force: :cascade do |t|
    t.string "affiliate_type"
    t.bigint "affiliate_id"
    t.string "parent_type"
    t.bigint "parent_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status", default: 0
    t.index ["affiliate_id", "affiliate_type", "parent_id", "parent_type"], name: "unique_affiliateable_index", unique: true
    t.index ["affiliate_type", "affiliate_id"], name: "index_affiliations_on_affiliate"
    t.index ["parent_type", "parent_id"], name: "index_affiliations_on_parent"
  end

  create_table "authorships", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "authorable_id"
    t.string "authorable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "badges_sashes", force: :cascade do |t|
    t.integer "badge_id"
    t.integer "sash_id"
    t.boolean "notified_user", default: false
    t.datetime "created_at"
    t.index ["badge_id", "sash_id"], name: "index_badges_sashes_on_badge_id_and_sash_id"
    t.index ["badge_id"], name: "index_badges_sashes_on_badge_id"
    t.index ["sash_id"], name: "index_badges_sashes_on_sash_id"
  end

  create_table "boards", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.bigint "boardable_id"
    t.string "boardable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "challenges", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "banner_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "short_description"
    t.string "country"
    t.string "city"
    t.string "address"
    t.string "logo_url"
    t.string "rules"
    t.float "latitude"
    t.float "longitude"
    t.integer "status", default: 0, null: false
    t.bigint "program_id"
    t.datetime "launch_date"
    t.datetime "end_date"
    t.datetime "final_date"
    t.string "faq"
    t.string "title_fr"
    t.string "description_fr"
    t.string "short_description_fr"
    t.string "rules_fr"
    t.string "faq_fr"
    t.string "short_title"
    t.bigint "space_id"
    t.string "custom_type", default: "Challenge"
    t.index ["program_id"], name: "index_challenges_on_program_id"
    t.index ["short_title"], name: "index_challenges_on_short_title"
  end

  create_table "challenges_interests", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "interest_id"
  end

  create_table "challenges_projects", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "project_status", default: 0
    t.index ["challenge_id"], name: "index_challenges_projects_on_challenge_id"
    t.index ["project_id"], name: "index_challenges_projects_on_project_id"
  end

  create_table "challenges_skills", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "skill_id"
  end

  create_table "challenges_workgroups", force: :cascade do |t|
    t.bigint "challenge_id"
    t.bigint "workgroup_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["challenge_id"], name: "index_challenges_workgroups_on_challenge_id"
    t.index ["workgroup_id"], name: "index_challenges_workgroups_on_workgroup_id"
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "post_id"
    t.bigint "user_id"
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_comments_on_post_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "customdata", force: :cascade do |t|
    t.bigint "customfield_id"
    t.bigint "user_id"
    t.string "value"
    t.index ["customfield_id", "user_id"], name: "by_user_and_customfield", unique: true
    t.index ["customfield_id"], name: "index_customdata_on_customfield_id"
    t.index ["user_id"], name: "index_customdata_on_user_id"
  end

  create_table "customfields", force: :cascade do |t|
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "description"
    t.string "name"
    t.string "field_type"
    t.boolean "optional"
    t.index ["resource_type", "resource_id"], name: "index_customfields_on_resource"
  end

  create_table "data", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "author_id"
    t.string "url"
    t.string "format"
    t.string "filename"
    t.bigint "dataset_id"
    t.string "provider_id"
    t.string "provider_version"
    t.datetime "provider_created_at"
    t.datetime "provider_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "datafields", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.string "description"
    t.string "format"
    t.string "unit"
    t.bigint "datum_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "datasets", force: :cascade do |t|
    t.string "title"
    t.string "short_title"
    t.string "description"
    t.string "author_id"
    t.string "url"
    t.string "provider"
    t.string "provider_id"
    t.string "provider_type"
    t.string "provider_url"
    t.string "provider_author"
    t.string "provider_authoremail"
    t.string "provider_version"
    t.datetime "provider_created_at"
    t.datetime "provider_updated_at"
    t.string "type", null: false
    t.bigint "datasetable_id"
    t.string "datasetable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string "title"
    t.string "content"
    t.bigint "documentable_id"
    t.string "documentable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position"
  end

  create_table "enablers", force: :cascade do |t|
    t.string "description"
    t.string "name"
    t.string "organisation_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "external_links", force: :cascade do |t|
    t.string "linkable_type"
    t.bigint "linkable_id"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
  end

  create_table "externalhooks", force: :cascade do |t|
    t.string "hookable_type"
    t.bigint "hookable_id"
    t.string "hook_type"
    t.boolean "trigger_need"
    t.boolean "trigger_post"
    t.boolean "trigger_member"
    t.boolean "trigger_project"
    t.index ["hookable_type", "hookable_id"], name: "index_externalhooks_on_hookable"
  end

  create_table "faqs", force: :cascade do |t|
    t.bigint "faqable_id"
    t.string "faqable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feeds", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "feedable_type"
    t.integer "feedable_id"
    t.boolean "allow_posting_to_all", default: true
    t.index ["feedable_type", "feedable_id"], name: "index_feeds_on_feedable_type_and_feedable_id"
  end

  create_table "feeds_posts", id: false, force: :cascade do |t|
    t.bigint "feed_id"
    t.bigint "post_id"
    t.index ["feed_id", "post_id"], name: "by_feed_and_post", unique: true
    t.index ["feed_id"], name: "index_feeds_posts_on_feed_id"
    t.index ["post_id"], name: "index_feeds_posts_on_post_id"
  end

  create_table "interests", force: :cascade do |t|
    t.string "interest_name"
  end

  create_table "interests_communities", id: false, force: :cascade do |t|
    t.integer "communities_id"
    t.integer "interest_id"
  end

  create_table "interests_projects", id: false, force: :cascade do |t|
    t.integer "project_id"
    t.integer "interest_id"
  end

  create_table "interests_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "interest_id"
  end

  create_table "interests_workgroups", force: :cascade do |t|
    t.integer "workgroup_id"
    t.integer "interest_id"
  end

  create_table "jwt_denylist", force: :cascade do |t|
    t.string "jti", null: false
    t.datetime "exp", null: false
    t.index ["jti"], name: "index_jwt_denylist_on_jti"
  end

  create_table "language_converters", force: :cascade do |t|
    t.string "key_name"
    t.string "content"
    t.string "language"
    t.string "languagable_type"
    t.bigint "languagable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "licenses", force: :cascade do |t|
    t.string "title"
    t.string "short_title"
    t.string "version"
    t.string "attr"
    t.string "logo_url"
    t.string "url"
    t.string "url_legal"
    t.bigint "licenseable_id"
    t.string "licenseable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "media", force: :cascade do |t|
    t.string "title"
    t.string "alt_text"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "mediumable_type"
    t.bigint "mediumable_id"
    t.index ["mediumable_type", "mediumable_id"], name: "index_media_on_mediumable"
  end

  create_table "mentions", force: :cascade do |t|
    t.bigint "post_id"
    t.string "obj_type"
    t.integer "obj_id"
    t.string "obj_match"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["obj_id", "obj_type", "post_id"], name: "unique_mention_index", unique: true
    t.index ["post_id"], name: "index_mentions_on_post_id"
  end

  create_table "merit_actions", force: :cascade do |t|
    t.integer "user_id"
    t.string "action_method"
    t.integer "action_value"
    t.boolean "had_errors", default: false
    t.string "target_model"
    t.integer "target_id"
    t.text "target_data"
    t.boolean "processed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "merit_activity_logs", force: :cascade do |t|
    t.integer "action_id"
    t.string "related_change_type"
    t.integer "related_change_id"
    t.string "description"
    t.datetime "created_at"
  end

  create_table "merit_score_points", force: :cascade do |t|
    t.bigint "score_id"
    t.integer "num_points", default: 0
    t.string "log"
    t.datetime "created_at"
    t.index ["score_id"], name: "index_merit_score_points_on_score_id"
  end

  create_table "merit_scores", force: :cascade do |t|
    t.bigint "sash_id"
    t.string "category", default: "default"
    t.index ["sash_id"], name: "index_merit_scores_on_sash_id"
  end

  create_table "needs", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "user_id"
    t.string "title"
    t.string "content"
    t.integer "status"
    t.datetime "end_date"
    t.boolean "is_urgent", default: false
    t.float "latitude"
    t.float "longitude"
    t.string "address"
    t.string "city"
    t.string "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["project_id"], name: "index_needs_on_project_id"
    t.index ["user_id"], name: "index_needs_on_user_id"
  end

  create_table "needs_projects", force: :cascade do |t|
    t.bigint "need_id"
    t.bigint "project_id"
    t.index ["need_id"], name: "index_needs_projects_on_need_id"
    t.index ["project_id"], name: "index_needs_projects_on_project_id"
  end

  create_table "needs_skills", force: :cascade do |t|
    t.bigint "need_id"
    t.bigint "skill_id"
    t.index ["need_id"], name: "index_needs_skills_on_need_id"
    t.index ["skill_id"], name: "index_needs_skills_on_skill_id"
  end

  create_table "networks", force: :cascade do |t|
    t.jsonb "json_network"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notification_settings_subscriptions", force: :cascade do |t|
    t.string "subscriber_type"
    t.bigint "subscriber_id"
    t.string "subscribable_type"
    t.bigint "subscribable_id"
    t.text "settings"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscribable_type", "subscribable_id"], name: "idx_subscriptions_subscribable_type_subscribable_id"
    t.index ["subscriber_type", "subscriber_id"], name: "idx_subscriptions_subscriber_type_subscriber_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "target_type"
    t.bigint "target_id"
    t.string "object_type"
    t.bigint "object_id"
    t.boolean "read", default: false, null: false
    t.text "metadata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.bigint "subscription_id"
    t.string "category"
    t.string "cta_link"
    t.string "subject_line", default: "", null: false
    t.index ["object_type", "object_id"], name: "index_notifications_on_object"
    t.index ["read"], name: "index_notifications_on_read"
    t.index ["target_type", "target_id"], name: "index_notifications_on_target"
  end

  create_table "posts", force: :cascade do |t|
    t.bigint "feed_id"
    t.bigint "user_id"
    t.string "content"
    t.string "media"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_need"
    t.string "from_object"
    t.bigint "from_id"
    t.string "from_name"
    t.string "from_image"
    t.string "from_need_project_id"
    t.integer "category", default: 0
    t.index ["feed_id"], name: "index_posts_on_feed_id"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "programs", force: :cascade do |t|
    t.string "description"
    t.string "title"
    t.datetime "launch_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "short_title"
    t.string "faq"
    t.string "enablers"
    t.string "ressources"
    t.string "short_description"
    t.string "title_fr"
    t.string "short_title_fr"
    t.string "description_fr"
    t.string "short_description_fr"
    t.string "faq_fr"
    t.string "enablers_fr"
    t.integer "status", default: 0, null: false
    t.string "contact_email"
    t.string "meeting_information"
    t.string "custom_challenge_name"
    t.index ["short_title"], name: "index_programs_on_short_title"
    t.index ["status"], name: "index_programs_on_status"
  end

  create_table "projects", force: :cascade do |t|
    t.string "title"
    t.string "short_title"
    t.string "logo_url"
    t.string "description"
    t.string "short_description"
    t.integer "creator_id"
    t.integer "state"
    t.float "latitude"
    t.float "longitude"
    t.integer "feed_id"
    t.integer "status", default: 0
    t.string "short_name"
    t.boolean "is_private"
    t.string "banner_url"
    t.string "address"
    t.string "city"
    t.string "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "desc_elevator_pitch"
    t.string "desc_problem_statement"
    t.string "desc_objectives"
    t.string "desc_state_art"
    t.string "desc_progress"
    t.string "desc_stakeholder"
    t.string "desc_impact_strat"
    t.string "desc_ethical_statement"
    t.string "desc_sustainability_scalability"
    t.string "desc_communication_strat"
    t.string "desc_funding"
    t.string "desc_contributing"
    t.integer "sash_id"
    t.integer "level", default: 0
    t.boolean "is_looking_for_collaborators", default: false
    t.integer "maturity"
    t.boolean "is_reviewed", default: false
    t.string "grant_info"
    t.integer "space_id"
    t.integer "workgroup_id"
    t.index ["short_title"], name: "index_projects_on_short_title"
  end

  create_table "projects_skills", id: false, force: :cascade do |t|
    t.integer "project_id"
    t.integer "skill_id"
  end

  create_table "recsys_data", force: :cascade do |t|
    t.string "sourceable_node_type"
    t.bigint "sourceable_node_id"
    t.string "targetable_node_type"
    t.bigint "targetable_node_id"
    t.string "relation_type"
    t.float "value", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recsys_results", force: :cascade do |t|
    t.string "sourceable_node_type"
    t.bigint "sourceable_node_id"
    t.string "targetable_node_type"
    t.bigint "targetable_node_id"
    t.string "relation_type"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uuid", default: "uuid_generate_v4()"
    t.string "network_id"
    t.index ["network_id"], name: "index_recsys_results_on_network_id"
    t.index ["uuid"], name: "index_recsys_results_on_uuid"
  end

  create_table "recsys_skills_referential", force: :cascade do |t|
    t.string "skill_name"
    t.string "clean_skill_name"
    t.string "enriched_skill_name"
    t.string "stemmed_skill_name"
    t.string "lg_skill"
    t.boolean "is_linkedin_skill"
    t.integer "count"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "relations", force: :cascade do |t|
    t.bigint "user_id"
    t.string "resource_type"
    t.integer "resource_id"
    t.boolean "has_clapped"
    t.boolean "follows"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "saved"
    t.boolean "reviewed", default: false
    t.index ["resource_type", "resource_id"], name: "index_relations_on_resource_type_and_resource_id"
    t.index ["user_id"], name: "index_relations_on_user_id"
  end

  create_table "ressources", force: :cascade do |t|
    t.string "ressourceable_type"
    t.bigint "ressourceable_id"
    t.string "ressource_name"
    t.index ["ressourceable_type", "ressourceable_id"], name: "index_ressources_on_ressourceable"
  end

  create_table "ressourceships", force: :cascade do |t|
    t.string "ressourceable_type"
    t.bigint "ressourceable_id"
    t.bigint "ressource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource"
  end

  create_table "sashes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_flow_data", force: :cascade do |t|
    t.integer "service_id", null: false
    t.integer "flow", null: false
    t.jsonb "data", default: "{}", null: false
    t.datetime "created_at", default: -> { "now()" }, null: false
    t.datetime "updated_at", default: -> { "now()" }, null: false
    t.index ["data"], name: "index_service_flow_data_on_data", using: :gin
    t.index ["service_id", "flow"], name: "unique_service_flow_data_index", unique: true
  end

  create_table "services", force: :cascade do |t|
    t.integer "name", default: 0, null: false
    t.bigint "creator_id", null: false
    t.bigint "serviceable_id", null: false
    t.string "serviceable_type", null: false
    t.string "token", null: false
    t.datetime "created_at", default: -> { "now()" }, null: false
    t.datetime "updated_at", default: -> { "now()" }, null: false
    t.index ["name", "serviceable_id", "serviceable_type"], name: "unique_serviceable_index", unique: true
  end

  create_table "skills", force: :cascade do |t|
    t.string "skill_name"
    t.string "clean_skill_name"
    t.string "enriched_skill_name"
    t.string "stemmed_skill_name"
    t.boolean "is_linkedin_skill"
    t.integer "count"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skills_users", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "skill_id"
  end

  create_table "skills_workgroups", force: :cascade do |t|
    t.integer "workgroup_id"
    t.integer "skill_id"
  end

  create_table "slackhooks", force: :cascade do |t|
    t.bigint "externalhook_id"
    t.string "hook_url"
    t.string "channel"
    t.string "username"
    t.index ["externalhook_id"], name: "index_slackhooks_on_externalhook_id"
  end

  create_table "sources", force: :cascade do |t|
    t.string "title"
    t.string "url"
    t.bigint "sourceable_id"
    t.string "sourceable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "spaces", force: :cascade do |t|
    t.string "description"
    t.string "title"
    t.datetime "launch_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "short_title"
    t.string "faq"
    t.string "enablers"
    t.string "ressources"
    t.string "short_description"
    t.string "title_fr"
    t.string "short_title_fr"
    t.string "description_fr"
    t.string "short_description_fr"
    t.string "faq_fr"
    t.string "enablers_fr"
    t.integer "status", default: 0, null: false
    t.string "contact_email"
    t.string "meeting_information"
    t.boolean "show_featured_programs", default: false
    t.boolean "show_featured_challenges", default: false
    t.boolean "show_featured_projects", default: false
    t.boolean "show_featured_needs", default: false
    t.string "home_header", default: ""
    t.string "home_info", default: ""
    t.jsonb "json_attributes"
    t.text "code_of_conduct"
    t.text "onboarding_steps"
    t.string "custom_challenge_name"
    t.integer "space_type", default: 0
    t.boolean "is_private", default: true
    t.integer "creator_id"
    t.boolean "show_associated_projects"
    t.boolean "show_associated_users"
    t.index ["short_title"], name: "index_spaces_on_short_title"
    t.index ["status"], name: "index_spaces_on_status"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.bigint "tagable_id"
    t.string "tagable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "nickname"
    t.integer "age"
    t.string "social_cat"
    t.string "profession"
    t.string "country"
    t.string "city"
    t.string "address"
    t.string "phone_number"
    t.string "bio"
    t.boolean "allow_password_change"
    t.integer "feed_id"
    t.boolean "email_confirmed", default: false
    t.string "confirm_token"
    t.datetime "locked_at"
    t.string "provider", default: "email", null: false
    t.string "uid", default: -> { "uuid_generate_v1()" }, null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "active_status"
    t.float "latitude"
    t.float "longitude"
    t.string "banner_url"
    t.string "affiliation"
    t.string "category"
    t.boolean "mail_newsletter"
    t.boolean "mail_weekly"
    t.string "short_bio"
    t.boolean "can_contact"
    t.string "ip"
    t.integer "sash_id"
    t.integer "level", default: 0
    t.text "settings"
    t.string "status"
    t.string "job_title"
    t.date "birth_date"
    t.string "gender"
    t.integer "direct_message_count", default: 0
    t.datetime "last_active_at", default: -> { "CURRENT_TIMESTAMP" }
    t.index ["confirm_token"], name: "index_users_on_confirm_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "users_boards", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "board_id"
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type"
    t.string "{:null=>false}"
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "workgroup_tags", force: :cascade do |t|
    t.string "tag_name"
  end

  create_table "workgroup_tags_workgroups", id: false, force: :cascade do |t|
    t.integer "workgroup_tag_id"
    t.integer "workgroup_id"
  end

  create_table "workgroups", force: :cascade do |t|
    t.string "title"
    t.string "short_title"
    t.string "banner_url"
    t.string "short_description"
    t.boolean "is_private"
    t.integer "creator_id"
    t.integer "feed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.string "logo_url"
    t.string "description"
    t.float "latitude"
    t.float "longitude"
    t.string "address"
    t.string "city"
    t.string "country"
    t.integer "space_id"
    t.index ["short_title"], name: "index_workgroups_on_short_title"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "service_flow_data", "services"
end
