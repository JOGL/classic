# JOGL

Gitlab [![pipeline status](https://gitlab.com/JOGL/backend-v0.1/badges/develop/pipeline.svg)](https://gitlab.com/JOGL/backend-v0.1/-/commits/develop)
[![coverage report](https://gitlab.com/JOGL/backend-v0.1/badges/develop/coverage.svg)](https://gitlab.com/JOGL/backend-v0.1/-/commits/develop)

Skylight [![View performance data on Skylight](https://badges.skylight.io/typical/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/problem/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/rpm/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/status/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)

<hr />

JOGL is a **100%** open-source application built with [Ruby on Rails](https://rubyonrails.org/) and [Next.js](https://nextjs.org/).

We fully welcome contributions to the code!
Feel free to browse through the [issues](https://gitlab.com/JOGL/JOGL/-/issues).
If you want to contribute, fork from the branch `develop` and merge request to the branch `develop`!

For more detailed information, please read our guide to [contributing](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).

## DEVELOP

JOGL relies on various [environment variables](https://github.com/bkeepers/dotenv/) in order to operate correctly.
We suggest creating a `.env` file with same variables as our [.env.example](.env.example).

You will need to sign up for a free [Algolia](https://www.algolia.com/) account in order to fulfill all of the credentials above.

### LANGUAGES

We are currently using Ruby [3.0.x](https://www.ruby-lang.org/en/downloads/) and suggest using a tool like [rbenv](https://github.com/rbenv/rbenv#installation) to manage Ruby versions.

We use Node [14.x](https://nodejs.org/en/download/) for a few dependencies and suggest using a tool like [nvm](https://github.com/nvm-sh/nvm) to manage Node versions.

### DEPENDENCIES

After you have cloned the repository, install all of the project dependencies:

```sh
%> bundle install
%> npm install
```

### EXTERNAL SERVICES

JOGL relies on a few external services in order to function.

#### DOCKER COMPOSE

First, make sure you have [docker-compose](https://docs.docker.com/compose/install/) installed.
Then create a `docker-compose.yml` file at the root directory and copy paste the following content in it:

```yaml
version: '3'
services:

  redis:
    image: redis
    ports:
      - "6379:6379"

  maildev:
    image: djfarrelly/maildev
    ports:
      - "1025:25"
      - "1080:80"

  postgress:
    image: postgres
    volumes:
      - ./tmp/db:/var/lib/postgresql/data
    environment:
      POSTGRES_PASSWORD: password
      POSTGRES_DB: jogl_development
      POSTGRES_USER: jogl
      PGDATA: /tmp
    ports:
      - "5432:5432"
```

Finally, launch all the dependencies with `docker-compose up -d`. 
When you need to close the containers you can exit them with `docker-compose down` 

#### MANUALLY

You can also install dependencies manually.

##### POSTGRES

You can install [PostgreSQL](https://www.postgresql.org/) on your operating system or via Docker:

```sh
%> docker run --name postgres -e POSTGRES_USER=jogl -e POSTGRES_PASSWORD=password -p 5432:5432 -d jogl_development
```

A *jogl_development* database will be created along with a *jogl* super user.
If you already have Postgres installed, you will need to create a user with the ability to create/drop databases:

```sql
CREATE ROLE jogl LOGIN SUPERUSER PASSWORD 'password';
```

If you ever need to access Postgres, you can use [psql](http://postgresguide.com/utilities/psql.html).

```sh
%> psql -h localhost -d jogl_development -U jogl
# OR #
%> docker exec -it postgres psql -d jogl_development -U jogl
```

##### REDIS

You can install [Redis](https://redis.io/) on your operating system or via Docker:

```sh
%> docker run --name redis -p 6379:6379 -d redis
```

If you need to access Redis, you can use [redis-cli](https://redis.io/topics/rediscli).

```sh
%> redis-cli
# OR #
%> docker exec -it redis redis-cli
```

##### MAILDEV

You can install [MailDev](https://github.com/maildev/maildev) on your operating system or via Docker:

```sh
%> docker run --name maildev -p 1080:80 -p 1025:25 -d maildev/maildev
```

Visit [http://localhost:1080](http://localhost:1080) in order to view the captured SMTP traffic.

## RUN

```sh
%> bundle exec rails s --port 3001
# If you are running on an external machine don't forget to bind server:
%> bundle exec rails s --binding 0.0.0.0 --port 3001

```

Run [Sidekiq](https://sidekiq.org/) workers:

```sh
%> bundle exec sidekiq -C ./config/sidekiq.yml
```

## DOCUMENT

We use [yard](https://github.com/lsegal/yard/) for documenting our codebase.

```sh
%> bundle exec yardoc -m markdown -M redcarpet app/**/*.rb
```

We use [graphdoc](https://github.com/2fd/graphdoc) for documenting our GraphQL schema.

```sh
%> bundle exec rails graphql:schema:json
%> npx graphdoc -s doc/graphql/schema.json -o doc/graphql/schema
```

## TEST

We use [RSpec](https://rspec.info/).

```sh
%> bundle exec rspec
```

Run tests in parallel using [parallel_tests](https://github.com/grosser/parallel_tests):

```sh
%> bundle exec rake parallel:setup
%> bundle exec rake parallel:spec
```

## LINT

Check used gems for any known security vulnerabilities:

```sh
%> bundle exec bundler-audit
```

We use [RuboCop](https://github.com/rubocop/rubocop) for checking code quality and formatting.
See [.rubocop.yml](.rubocop.yml) for our configuration.

```sh
%> bundle exec rubocop
```

You can format a single file (or directory):

```sh
%> bundle exec rubocop --fix-layout file.rb 
```
