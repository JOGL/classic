module.exports = {
  // presets: ['next/babel', '@emotion/babel-preset-css-prop'],
  // presets: ['next/babel', '@emotion/babel-preset-css-prop'],
  //   presets: [
  //     'next/babel',
  //     [
  //       '@emotion/babel-preset-css-prop',
  //       {
  //         runtime: 'automatic',
  //       },
  //     ],
  //   ],
  //   plugins: ['babel-plugin-macros'],
  // };

  presets: [
    [
      'next/babel',
      {
        'preset-react': {
          runtime: 'automatic',
          importSource: '@emotion/react',
        },
      },
    ],
  ],
  plugins: ['@emotion/babel-plugin', 'babel-plugin-twin', 'babel-plugin-macros'],
};
