const nextTranslate = require('next-translate');

/* eslint-disable no-param-reassign */
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

// const withCSS = require('@zeit/next-css');

const nextConfig = {
  typescript: {
    // !! WARNING !!
    // Dangerously allow production builds to successfully complete even if your project has type errors.
    ignoreBuildErrors: true,
  },
  // manage env variables
  env: {
    ALGOLIA_APP_ID: process.env.ALGOLIA_APP_ID,
    ALGOLIA_TOKEN: process.env.ALGOLIA_TOKEN,
    BACKEND_PORT: process.env.BACKEND_PORT,
    FRONTEND_PORT: process.env.FRONTEND_PORT,
    GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
    GOOGLE_ANALYTICS4_ID: process.env.GOOGLE_ANALYTICS4_ID,
    HOSTNAME: process.env.HOSTNAME,
    TAGMANAGER_ARGS_ID: process.env.TAGMANAGER_ARGS_ID,
    HOTJAR_ID: process.env.HOTJAR_ID,
    PROTOCOL: process.env.PROTOCOL,
    SPACES_ALLOWLIST: process.env.SPACES_ALLOWLIST,
  },
  // image optimization config (https://nextjs.org/docs/basic-features/image-optimization)
  images: {
    deviceSizes: [480, 768, 1280],
    domains: ['development', 'localhost', 'jogl-backend-dev.herokuapp.com', 'jogl-backend.herokuapp.com', 'img.evbuc.com'],
  },
  // manage redirections
  redirects() {
    return [
      { source: '/projects', destination: '/search/projects', permanent: true },
      { source: '/needs', destination: '/search/needs', permanent: true },
      { source: '/people', destination: '/search/members', permanent: true },
      { source: '/communities', destination: '/search/groups', permanent: true },
      { source: '/challenges', destination: '/search/challenges', permanent: true },
    ];
  },
  webpack: (config, { isServer }) => {
    // Fixes npm packages that depend on `fs` and `module` modules
    if (!isServer) {
      // config.resolve.fallback.fs = false;
      // config.resolve.fallback.module = false;
      config.node = { fs: 'empty', module: 'empty' };
    }
    return config;
  },
  // future: {
  //   webpack5: true,
  // },
};

module.exports = nextTranslate(withBundleAnalyzer(nextConfig));
