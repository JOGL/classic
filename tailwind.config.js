const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    extend: {
      colors: {
        primary: '#2987cd',
        secondary: '#6c757d',
        danger: '#dc3545',
        lightBlue: '#F1F4F8',
        grey: '#C5CED5',
        pink: '#DE3E96',
        orange: '#F0712C',
        lightGrey: '#fafafa80',
        darkBlue: '#4F4BFF',
        violet: '#643CC6',
      },
      boxShadow: {
        custom: '0px 4px 30px rgba(0, 0, 0, 0.09)',
      },
      screens: {
        xs: '475px',
        ...defaultTheme.screens,
      },
    },
  },
  plugins: [require('@tailwindcss/line-clamp')],
};
