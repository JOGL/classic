const { createServer } = require('http');
const { parse } = require('url');
const next = require('next');

const port = parseInt(process.env.PORT, 10) || 3000;
const host = process.env.HOST || 'localhost';
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  createServer((req, res) => {
    // Not sure this is needed
    if (
      process.env.REACT_APP_NODE_ENV === 'production' &&
      req.headers['x-forwarded-proto'] === 'http' &&
      process.env.FRONTEND_URL
    ) {
      res.writeHead(302, {
        Location: `${process.env.FRONTEND_URL}${req.url}`,
      });
      res.end();
      return;
    }
    // End - Not sure this is needed
    const parsedUrl = parse(req.url, true);
    handle(req, res, parsedUrl);
  }).listen(port, host, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://${host}:${port}`);
  });
});
